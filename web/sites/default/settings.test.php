<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Test environment settings.
 *
 * Ideally there should not be any need for these, as the environment should be
 * identical to production.
 */

$config['system.logging']['error_level'] = 'verbose';

// Ignore trusted host patterns on test.
$settings['trusted_host_patterns'] = [];

// Stage file proxy gets images from production environment
if (getenv('PRODUCTION_DOMAIN')) {
  $config['stage_file_proxy.settings']['origin'] = getenv('PRODUCTION_DOMAIN');
}

// By definition, we can't use recaptcha successfully on automated tests.
$config['captcha.captcha_point.user_register_form']['status'] = FALSE;
$config['captcha.captcha_point.user_pass']['status'] = FALSE;
$config['captcha.captcha_point.user_login_form']['status'] = FALSE;
$config['captcha.captcha_point.simplenews_subscriptions_block_footer']['status'] = FALSE;

// Disable the antibot for forms during tests.
$config['antibot.settings']['form_ids'] = ['placeholder'];
