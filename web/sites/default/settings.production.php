<?php


// Allow emails to be sent
$config['maillog.settings']['send'] = TRUE;

// Query real discourse instance
$config['discourse_sso.settings']['discourse_server'] = 'https://sanghaspace.ahs.org.uk';

// Use live mode
$config['commerce_payment.commerce_payment_gateway.shrimala_trust_stripe']['configuration']['mode'] = 'live';

$settings['ahs_workflow_youtube_audio_request_url'] = NULL;
if (getenv("AHS_WORKFLOW_YOUTUBE_AUDIO_REQUEST_URL")) {
  $settings['ahs_workflow_youtube_audio_request_url'] = getenv("AHS_WORKFLOW_YOUTUBE_AUDIO_REQUEST_URL");
}

// These settings are null by default so that they log errors if not set on production.
$settings['ahs_workflow_youtube_audio_request_url'] = NULL;
if (getenv("AHS_WORKFLOW_YOUTUBE_AUDIO_REQUEST_URL")) {
  $settings['ahs_workflow_youtube_audio_request_url'] = getenv("AHS_WORKFLOW_YOUTUBE_AUDIO_REQUEST_URL");
}
$settings['ahs_workflow_youtube_transcript_request_url'] = NULL;
if (getenv("AHS_WORKFLOW_TRANSCRIPT_REQUEST_URL")) {
  $settings['ahs_workflow_transcript_request_url'] = getenv("AHS_WORKFLOW_TRANSCRIPT_REQUEST_URL");
}

