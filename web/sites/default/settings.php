<?php
/**
 * @file
 * AHS settings.php file for Drupal 8.
 */

// Default Drupal 8 settings.
//
// These are already explained with detailed comments in Drupal's
// default.settings.php file.
//
// See https://api.drupal.org/api/drupal/sites!default!default.settings.php/8
$settings['config_sync_directory'] = '../config/sync';
$settings['update_free_access'] = FALSE;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// The hash_salt should be a unique random value for each application.
// If left unset, the settings.platformsh.php file will attempt to provide one.
// You can also provide a specific value here if you prefer and it will be used
// instead. In most cases it's best to leave this blank on Platform.sh. You
// can configure a separate hash_salt in your settings.local.php file for
// local development.
$settings['hash_salt'] = 'WOidihLLuDn2rJdIpUsVB2eOY94BsyXFnTmahN09f5VQemJ6SnEIlis62L83CbNK7gedUG3XKA';

/*if (isset($_ENV["PLATFORM_ROUTES"])) {
  // Set base url, needed by simplenews module
  $main_route_url = 'http://{default}/';
  $routes = json_decode(base64_decode($_ENV['PLATFORM_ROUTES']),true);
  foreach ($routes as $route_url => $route_info) {
    if ($route_info["original_url"] == $main_route_url) {
      $base_url = $route_url;
      break;
    }
  }
  $base_url = rtrim($base_url,'/');
}*/

//$settings['trusted_host_patterns'] = [
//  '^.+\.ahs\.org\.uk$',
//  '^.+\.devahs\.co\.uk$',
//  ];

// Settings for different Platform.sh environments
if (isset($_ENV["PLATFORM_ENVIRONMENT"])) {
/*  // We're on platform.sh
  // Fetch the Platform.sh environment variables.
  $platformVariables = json_decode(base64_decode($_ENV['PLATFORM_VARIABLES']), TRUE);
  if ($_ENV['PLATFORM_ENVIRONMENT']==='master') {
    //We're on platform.sh master
    //$settings['config_readonly'] = TRUE;
    $dropboxPath = "Live";
  } else {
    // We're on a platform.sh dev environment
    $dropboxPath = "Dev";
    if (file_exists(__DIR__ . '/settings.platformdev.php')) {
      include __DIR__ . '/settings.platformdev.php';
    }
  }
  // Build the Flysystem scheme using credentials configure on Platform.sh.
  $schemes = [
    'dropboxwebarchive' => [
      'driver' => 'dropbox',
      'config' => [
        'token' => $platformVariables['DROPBOX_TOKEN'],
        'client_id' => $platformVariables['DROPBOX_CLIENT'],
        'prefix' => $dropboxPath,
      ],
    ]
  ];*/

} else {

  // We're not on Platform.sh; maybe Travis, maybe local.
/*  if (isset($_ENV['TRAVIS'])) {
    //We're on Travis
    $webRoot = $_ENV['TRAVIS_BUILD_DIR'];
  } else {
    //We're on local
    $webRoot= $_SERVER['DOCUMENT_ROOT'];
  }

  // Build the Flysystem scheme, using local storage instead of dropbox.
  $schemes = [
    'dropboxwebarchive' => [
      'driver' => 'local',
      'config' => [
        'root' => $webRoot . '/web/sites/default/files/testfiles',
      ],
	]
  ];*/

}

$schemes = [
  'dropboxwebarchive' => [
    'driver' => 'local',
    'config' => [
      'root' => 'web/sites/default/files/testfiles',
    ],
  ],
];
// Passing the Flysystem schemes to Drupal
$settings['flysystem'] = $schemes;

// Tweak the wording to reflect login by email.
$settings['locale_custom_strings_en'][''] = [
    'Unrecognized username or password. <a href=":password">Forgot your password?</a>' =>
      'Unrecognised email or password. <a href=":password">Forgot your password?</a>',
];


// Set API keys (probably only on production)
// overriding the test or dummy keys stored in config.
if (getenv("AHS_SENDGRID_APIKEY")) {
  $config['swiftmailer.transport']['smtp_credentials']['swiftmailer']['password'] = getenv("AHS_SENDGRID_APIKEY");
  $config['symfony_mailer.mailer_transport.smtp']['configuration']['pass'] = getenv("AHS_SENDGRID_APIKEY");
}
if (getenv("AHS_STRIPE_LIVE_PUBLISHABLEKEY")) {
  $config['commerce_payment.commerce_payment_gateway.shrimala_trust_stripe']['configuration']['publishable_key'] = getenv("AHS_STRIPE_LIVE_PUBLISHABLEKEY");
}
if (getenv("AHS_STRIPE_LIVE_SECRETKEY")) {
  $config['commerce_payment.commerce_payment_gateway.shrimala_trust_stripe']['configuration']['secret_key'] = getenv("AHS_STRIPE_LIVE_SECRETKEY");
}
if (getenv("AHS_XERO_AHSDRUPAL_CLIENT_SECRET")) {
  $config['xero.settings']['oauth']['consumer_secret'] = getenv("AHS_XERO_AHSDRUPAL_CLIENT_SECRET");
}
if (getenv("AHS_TINYPNG_PAID_APIKEY")) {
  // Tinypng account: owner at ahs.org.uk
  $config['imageapi_optimize.pipeline.tinypng']['processors']['be54b601-c774-4992-9f45-69e9ad54d952']['data']['api_key'] = getenv("AHS_TINYPNG_PAID_APIKEY");
}
if (getenv("AHS_ARCHIVES_STORAGE_ACCOUNT_KEY")) {
  $settings['s3fs.access_key'] = 'FlIOeJjKeP3IDcAwiy10Srpc';
  $settings['s3fs.secret_key'] = getenv("AHS_ARCHIVES_STORAGE_ACCOUNT_KEY");
}
if (getenv("AHS_RECAPTCHA_SITE_KEY")) {
  $config['recaptcha.settings']['site_key'] = getenv("AHS_RECAPTCHA_SITE_KEY");
}
if (getenv("AHS_RECAPTCHA_SECRET_KEY")) {
  $config['recaptcha.settings']['secret_key'] = getenv("AHS_RECAPTCHA_SECRET_KEY");
}
if (getenv("AHS_RECAPTCHA_V3_SITE_KEY")) {
  $config['recaptcha_v3.settings']['site_key'] = getenv("AHS_RECAPTCHA_V3_SITE_KEY");
}
if (getenv("AHS_RECAPTCHA_V3_SECRET_KEY")) {
  $config['recaptcha_v3.settings']['secret_key'] = getenv("AHS_RECAPTCHA_V3_SECRET_KEY");
}
if (getenv("AHS_DISCOURSE_API_KEY")) {
  $config['discourse_sso.settings']['api_key'] = getenv("AHS_DISCOURSE_API_KEY");
}
if (getenv("AHS_DISCOURSE_SSO_SECRET")) {
  $config['discourse_sso.settings']['discourse_sso_secret'] = getenv("AHS_DISCOURSE_SSO_SECRET");
}
$settings['ahs_workflow_youtube_audio_request_url'] = 'https://test.org/audio';
$settings['ahs_workflow_transcript_request_url'] = 'https://test.org/transcript';

// Keep test files separated from the real archives.
$testFolder = 'dev';
if (getenv('DRUPAL_ENVIRONMENT')) {
  // Keep test files separated between test jobs.
  $testFolder = getenv('DRUPAL_ENVIRONMENT');
  if (getenv('ENVIRONMENT_LABEL')) {
    $testFolder = "$testFolder/" . getenv('ENVIRONMENT_LABEL');
  }
}
$config['s3fs.settings']['root_folder'] = $testFolder;

// By default, do not use dev config and do use production config.
$config['config_split.config_split.development']['status'] = FALSE;
$config['config_split.config_split.production']['status'] = TRUE;

// Database settings.
$databases['default']['default'] = [
  'database' =>  getenv('DB_NAME'),
  'username' => getenv('DB_USER'),
  'password' => getenv('DB_PASS'),
  'host' => getenv('DB_HOST'),
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];

$settings['twig_sandbox_whitelisted_methods'] = [
  // Allow EntityInterface::uuid and Url::setAbsolute.
  // See https://www.drupal.org/project/drupal/issues/2907810
  'id',
  'label',
  'bundle',
  'get',
  '__toString',
  'toString',
  'setAbsolute',
  'uuid',
];

$settings['twig_sandbox_allowed_methods'] = [
  // Allow EntityInterface::uuid and Url::setAbsolute.
  // See https://www.drupal.org/project/drupal/issues/2907810
  'id',
  'label',
  'bundle',
  'get',
  '__toString',
  'toString',
  'setAbsolute',
  'uuid',
];

//$settings['php_storage']['twig']['directory'] = '/tmp';

/*if (getenv('MEMCACHED_HOST')) {
  $settings['memcache']['servers'] = [getenv('MEMCACHED_HOST') . ':11211' => 'default'];
	$settings['cache']['bins']['bootstrap'] = 'cache.backend.memcache';
  $settings['cache']['bins']['discovery'] = 'cache.backend.memcache';
  $settings['cache']['bins']['config'] = 'cache.backend.memcache';
  $settings['cache']['default'] = 'cache.backend.memcache';
}*/

// Explicitly disable memcache as it started erroring without this
$settings['memcache']['servers'] = [];
$settings['memcache']['bins'] = [];

// Specify the s3fs hostname to use. See web/modules/ahs/ahs_archives/src/StreamWrapper/S3fsStream.php
$settings['ahs_archives.s3fs_host'] = 'https://ahsarchivesstandard-fchcf6fsajhxfza6.z01.azurefd.net';

// Specify js/css path.
#$settings['file_assets_path'] = 'sites/default/files/optimized';

if (getenv('DRUPAL_ENVIRONMENT')) {
    // Development settings.
    if (file_exists($app_root . '/' . $site_path . '/settings.' . getenv('DRUPAL_ENVIRONMENT') . '.php')) {
      include $app_root . '/' . $site_path . '/settings.' . getenv('DRUPAL_ENVIRONMENT') . '.php';
    }
    if (file_exists($app_root . '/' . $site_path . '/services.' . getenv('DRUPAL_ENVIRONMENT') . '.yml')) {
      $settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.' . getenv('DRUPAL_ENVIRONMENT') . '.yml';
    }
}

// Lando settings.
if (getenv('LANDO')) {
  if (file_exists($app_root . '/' . $site_path . '/settings.lando.php')) {
    include $app_root . '/' . $site_path . '/settings.lando.php';
  }
}

// Include for settings managed by ddev.
$ddev_settings = dirname(__FILE__) . '/settings.ddev.php';
if (getenv('IS_DDEV_PROJECT') == 'true' && is_readable($ddev_settings)) {
  require $ddev_settings;
}

// Local settings. These come last so that they can override anything.
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

// Automatic Platform.sh settings.
if (file_exists($app_root . '/' . $site_path . '/settings.platformsh.php')) {
  include $app_root . '/' . $site_path . '/settings.platformsh.php';
}

