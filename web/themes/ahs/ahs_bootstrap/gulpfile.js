// The streaming build system.
var gulp = require('gulp');

// Define gulp-load-plugins.
var $ = require('gulp-load-plugins')();

// Gulp plugin for sass.
var sass = require('gulp-sass');

// Prefix CSS.
var autoprefixer = require('gulp-autoprefixer');

// Prevent pipe breaking caused by errors from gulp plugins.
var plumber = require('gulp-plumber');

// Live CSS Reload & Browser Syncing.
var browserSync = require('browser-sync').create();

// Rename files
var rename = require("gulp-rename");

// Shows scss file in browser inspect tools rather than the compiled css file
var sourcemaps = require('gulp-sourcemaps');

// Allows you to use glob syntax in imports (i.e. @import "dir/*.sass"). Use as a custom importer for node-sass.
var importer = require('node-sass-globbing');

// Gulp download utility
var download = require("gulp-download");

// Gulp unzipping utility
var decompress = require('gulp-decompress');

// Default settings.
var config = {
  scss: './scss/*.scss', // The top level scss files that include all other scss.
  watch: './scss/**', // All scss files that should trigger a recompile.
  css: './css', // The path to the generated css files.
  node_modules: './node_modules', // The path to the node modules directory.
  drupal: './drupal-bootstrap-styles'
};


var url = 'https://github.com/unicorn-fail/drupal-bootstrap-styles/archive/master.zip';

gulp.task('drupal-bootstrap-styles', function () {
  return download(url)
      .pipe(decompress())
      .pipe(gulp.dest(src.drupal));
});


// Compiles scss into css.
gulp.task('scss-compile', function () {
       return gulp.src(config.scss)
        .pipe(plumber())
         // Start making a source map, so that the browser can show a developer which scss file is responsible for a css rule.
        .pipe(sourcemaps.init({loadMaps: true})) //Not sure if loadMaps is a good idea.
         // Compile the scss to css.
        .pipe(sass({
            importer: importer,
            // Include some useful scss from external node packages.
            includePaths: [
                config.node_modules + '/breakpoint-sass/stylesheets/',
                config.node_modules + '/compass-mixins/lib/'
            ],
            // Tweak output styling to make things easier for developers.
            outputStyle: 'expanded',
            sourceComments: true,
            indentWidth: 2
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('maps'))
         // Write out the generated css.
        .pipe(gulp.dest(config.css))
         // Push the css to the browser.
        .pipe(browserSync.stream());
});

// Watch the scss files and trigger a recompile if they change.
gulp.task('watch', function () {
    // Setup a browsersync server.
    browserSync.init({
      proxy: 'http://appserver_nginx',
      socket: {
        domain: 'https://bs.ahs.lndo.site',
        port: 80
      },
      open: false,
      logLevel: "debug",
      logConnections: true,
    });
    // Trigger the scss-compile task whenever any file in the defined location changes.
    gulp.watch(config.watch, gulp.series('scss-compile'));
});

gulp.task('default', gulp.series('scss-compile'));
