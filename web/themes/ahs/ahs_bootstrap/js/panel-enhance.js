(function ($, Drupal) {
  Drupal.behaviors.ahsPanelEnhance = {
    attach: function (context, settings) {
      // Add icons for open/close.
      $(once("panelEnhance", ".panel:not(.always-expanded) .panel-heading h4")).prepend('<i class="fa fa-caret-right"></i><i class="fa fa-caret-down"></i>');

      // Enhance each initially collapsed panel
      $(context).find(once('ahsPanelEnhance', '.panel')).each(function () {
        // Add class on panel container marking initial collapsed state
        if (!$(this).hasClass('always-expanded')) {
          $(this).addClass('collapsed');
        }

        // Listen for the panel being collapsed
        $(this).on('hidden.bs.collapse', function (e) {
          // Toggle class on panel container marking collapsed state
          $(this).addClass('collapsed');
        });

        var id = $(this).attr('id');
        var hashId = '#' + id;

        if (id) {
          // Toggle panels open when deep-linked.
          if (location.hash === hashId && $(this).hasClass('collapsed')) {
            $(this).removeClass('collapsed');
            $(this).find('.collapse').addClass('in');
          }

          // Listen for the panel expansion starting.
          $(this).on('show.bs.collapse', function (e) {

            // Set hash in url
            if (history.pushState) {
              history.pushState(null, null, hashId);
            }
            else {
              location.hash = hashId;
            }

            // Hide siblings. Use #accordion if it exists, or the selector specified by attribute if not.
            parent = $(this).closest('.ahs-bootstrap-panel-parent');
            if (parent.get().length === 0) {
              var parentSelector = $(this).attr('data-ahs-bootstrap-panel-parent');
              if (parentSelector) {
                parent = $(this).closest(parentSelector);
              }
            }
            if (parent.get().length > 0) {
              var actives = parent.find('.panel:not(' + hashId + ') .panel-collapse');
              actives.each(function (index, element) {
                $(element).collapse('hide');
              });
            }
          });
        }

        // Listen for the panel expansion finished
        $(this).on('shown.bs.collapse', function (e) {
          // Toggle class on panel container marking collapsed state
          $(this).removeClass('collapsed');

          if (id) {
            // Scroll element to top
            //var marginTop = parseInt($(this).css('marginTop'));
            //$.scrollTo(hashId, {
            //  duration: 250,
            //  offset: marginTop * -2,
            //});

          }

          // When expanded, check visibility every 5 seconds
          var intervalId = setInterval( function() {
            // If all of the panel is no longer in viewport
            if (!($(e.currentTarget).visible( true, true ))) {
              // Collapse the panel
              $(e.currentTarget).find('.panel-collapse').collapse('hide');
              // Stop further checking
              clearInterval(intervalId);
            }
          }, 5000 );
        });
      });
    }
  };
})(jQuery, Drupal);
