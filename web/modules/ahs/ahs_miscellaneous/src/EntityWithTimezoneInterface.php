<?php

namespace Drupal\ahs_miscellaneous;

/**
 * Interface for entities that have fields with timezone information.
 */
interface EntityWithTimezoneInterface {

  /**
   * Gets the timezone for this entity.
   *
   * @return string
   *   The timezone string (e.g. 'UTC', 'America/New_York').
   */
  public function getTimezone(): string;

  /**
   * Gets the field IDs that should have timezone information.
   *
   * @return string[]
   *   Array of field IDs that should have timezone information.
   */
  public function getFieldsWithTimezone(): array;

  /**
   * Whether the entity will be attended from multiple timezones.
   *
   * @return bool
   *   Whether the entity will be attended from multiple timezones.
   */
  public function willBeAttendedFromMultipleTimezones(): bool;

}