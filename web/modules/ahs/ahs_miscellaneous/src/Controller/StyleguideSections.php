<?php

namespace Drupal\ahs_miscellaneous\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Styleguide sections controller.
 */
class StyleguideSections extends ControllerBase {

  /**
   * The render service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a StyleguideSections object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Render a single paragraph item (Styleguide section).
   *
   * @param int $paragraphId
   *   Paragraph id.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function section($paragraphId) {
    if (empty($paragraphId)) {
      $output = "No paragraph id supplied.";
    }
    else {
      $entity_type = 'paragraph';
      $view_builder = $this->entityTypeManager()
        ->getViewBuilder($entity_type);
      $paragraph = $this->entityTypeManager()
        ->getStorage($entity_type)
        ->load($paragraphId);
      if (empty($paragraph)) {
        $output = "Paragraph . " . $paragraphId . " not found.";
      }
      else {
        $build = $view_builder->view($paragraph, 'full');
        $output = $this->renderer->render($build);
      }
    }
    return [
      '#type' => 'markup',
      '#markup' => $output,
    ];

  }

}
