<?php

namespace Drupal\ahs_miscellaneous;

use Drupal\Component\Utility\Html;

/**
 * Prepares arrays using configurable logic that adapts to array size.
 */
class SmartListPreparer {

  /**
   * The settings. Defaults to "Item1 & others".
   *
   * @var array
   */
  protected array $settings = [
    'max' => 2,
    'omit_after' => 1,
    'omitted' => ' & others',
    'normal_separator' => ', ',
    'final_separator' => ' & ',
    'prefix' => '',
    'suffix' => '',
  ];

  public function setSettings(array $settings): SmartListPreparer {
    foreach ($settings as $name => $value) {
      $this->setSetting($name, $value);
    }
    return $this;
  }

  public function setSetting($name, $value): SmartListPreparer {
    if (array_key_exists($name, $this->settings)) {
      $this->settings[$name] = $value;
    }
    return $this;
  }

  public function getSettings(): array {
    return $this->settings;
  }

  public function getSetting($name) {
    if (!array_key_exists($name, $this->settings)) {
      throw new \InvalidArgumentException("'{$name}' is not a valid setting.");
    }
    return $this->settings[$name];
  }

  public function implode(iterable $elements): string {
    $prepared = $this->prepare($elements);
    $strings = array_map('strval', $prepared);
    return implode('', $strings);
  }

  public function prepare(iterable $elements): array {
    $inline = [];
    if (empty($elements)) {
      return $inline;
    }

    if (!is_countable($elements)) {
      throw new \InvalidArgumentException("List elements must be countable.");
    }

    if (!empty($this->getSetting('prefix'))) {
      $inline[] = $this->getSetting('prefix');
    }

    $maxSetting = empty($this->getSetting('max')) ? count($elements) : $this->getSetting('max');
    $max = min(count($elements), $maxSetting);
    if (count($elements) > $maxSetting) {
      $omit_after = $this->getSetting('omit_after');
      $final = 0;
    }
    else {
      $omit_after = 0;
      $final = count($elements) - 1;
    }
    $current = 1;
    foreach ($elements as $element) {
      $inline[] = $element;
      if ($current == $omit_after) {
        $inline[] = Html::escape($this->getSetting('omitted'));
        break;
      }
      elseif ($current == $final) {
        $inline[] = Html::escape($this->getSetting('final_separator'));
      }
      elseif ($current < $max) {
        $inline[] = Html::escape($this->getSetting('normal_separator'));
      }
      elseif ($current == $max) {
        break;
      }
      $current++;
    }

    if (!empty($this->getSetting('suffix'))) {
      $inline[] = $this->getSetting('suffix');
    }
    return $inline;
  }

}
