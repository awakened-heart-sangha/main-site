<?php

namespace Drupal\ahs_miscellaneous;

use Drupal\Core\State\StateInterface;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorInterface;
use Drupal\symfony_mailer\Processor\EmailProcessorTrait;

/**
 * Tracks sent emails for testing.
 */
class TestSymfonyMailer implements EmailProcessorInterface {

  use EmailProcessorTrait;

  const STATE_KEY = 'system.test_mail_collector';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The emails that have been sent.
   *
   * @var \Drupal\symfony_mailer\EmailInterface[]
   */
  protected $emails = [];

  /**
   * Constructs the MailerTestService.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * Post-render function.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email.
   */
  public function postRender(EmailInterface $email) {
    $email->setTransportDsn('null://default');
  }

  /**
   * Post-send function.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email.
   */
  public function postSend(EmailInterface $email) {
    $from = '';
    if (!empty($email->getAddress('from'))) {
      $address = $email->getAddress('from');
      $from = !empty($address[0]) ? $address[0]->getEmail() : '';
    }

    $to = '';
    if (!empty($email->getAddress('to'))) {
      $address = $email->getAddress('to');
      $to = !empty($address[0]) ? $address[0]->getEmail() : '';
    }

    $reply = '';
    if (!empty($email->getAddress('reply-to'))) {
      $address = $email->getAddress('reply-to');
      $reply = !empty($address[0]) ? $address[0]->getEmail() : '';
    }

    $body = '';
    if (!empty($email->getHtmlBody())) {
      $body = $email->getHtmlBody();
    }

    $this->emails[] = [
      'subject' => \Drupal::token()->replace($email->getSubject()),
      'body' => $body,
      'plain' => $email->getTextBody(),
      'from' => $from,
      'to' => $to,
      'reply-to' => $reply,
      'timestamp' => \Drupal::time()->getRequestTime(),
    ];

    $this->state->set(self::STATE_KEY, $this->emails);
  }

}
