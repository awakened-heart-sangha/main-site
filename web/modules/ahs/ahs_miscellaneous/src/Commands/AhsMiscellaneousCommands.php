<?php

namespace Drupal\ahs_miscellaneous\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class AhsMiscellaneousCommands extends DrushCommands {

  /**
   * The module installer service.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * The key value service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyvalue;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * AhsMiscellaneousCommands constructor.
   */
  public function __construct(ModuleInstallerInterface $module_installer, KeyValueFactoryInterface $key_value, ConfigFactoryInterface $config) {
    parent::__construct();
    $this->moduleInstaller = $module_installer;
    $this->keyvalue = $key_value;
    $this->config = $config;
  }

  /**
   * Switch installation profile.
   *
   * @param string $new
   *   The machine name of the desired profile.
   *
   * @usage ahs_miscellaneous-switchProfile profile
   *   Switch to the specified installation profile.
   *
   * @command ahs_miscellaneous:switchProfile
   */
  public function switchProfile($new) {
    $old = \Drupal::installProfile();
    $this->output()->writeln('Current profile: ' . $old);
    $this->output()->writeln('Attempting to switch profile to ' . $new);

    $this->output()->writeln('Editing configuration');
    $this->config->getEditable('core.extension')
      ->set('profile', $new)
      ->save();
    $this->output()->writeln('Flushing cache');
    drupal_flush_all_caches();

    $this->output()->writeln('Installing ' . $new);
    $this->moduleInstaller->install([$new]);
    $this->output()->writeln('Uninstalling ' . $old);
    $this->moduleInstaller->uninstall([$old]);

    $this->output()->writeln('Removing ' . $old . ' from system.schema');
    $sc = $this->keyvalue->get('system.schema');
    if ($weight = $sc->get($old)) {
      $sc->delete($old);
    }
    $this->output()->writeln('Putting ' . $new . ' into system.schema');
    $sc->set($new, $weight);
    $this->output()->writeln('Flushing cache');
    drupal_flush_all_caches();

    $this->output()->writeln('Profile now: ' . \Drupal::installProfile());
    $this->logger()->success(dt('Achievement unlocked.'));
  }

}
