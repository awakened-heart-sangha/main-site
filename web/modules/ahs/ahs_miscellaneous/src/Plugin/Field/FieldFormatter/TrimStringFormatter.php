<?php

namespace Drupal\ahs_miscellaneous\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'basic_string' formatter.
 *
 * @FieldFormatter(
 *   id = "ahs_trim_basic_string",
 *   label = @Translation("Plain text - trimmed of excess spacing"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *   }
 * )
 */
class TrimStringFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ value|nl2br }}',
        '#context' => ['value' => $this->cleanString($item->value)],
      ];
    }

    return $elements;
  }

  protected function cleanString($string) {
    // Replace multiple whitespaces with single space, excluding newlines.
    $string = preg_replace('/[ \t\r\0\x0B]+/', ' ', $string);
    // Replace newline + any whitespace (excluding newlines) + newline with single newline
    $string = preg_replace('/\n[ \t\r\0\x0B]*\n/', "\n", $string);
    // Collapse multiple line breaks into a single line break.
    $string = preg_replace('/(\r\n?|\n){2,}/', "\n", $string);
    // Trim whitespaces from beginning and end.
    $string = trim($string);
    return $string;
  }

}
