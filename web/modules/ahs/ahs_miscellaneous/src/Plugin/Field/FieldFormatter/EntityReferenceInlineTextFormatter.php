<?php

namespace Drupal\ahs_miscellaneous\Plugin\Field\FieldFormatter;

use Drupal\ahs_miscellaneous\SmartListPreparer;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Formatter that displays entity references as inline text.
 *
 * @FieldFormatter(
 *   id = "ahs_miscellaneous_entity_reference_inline_text",
 *   label = @Translation("Inline text"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceInlineTextFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'max' => 2,
      'omit_after' => 1,
      'omitted' => ' & others',
      'normal_separator' => ', ',
      'final_separator' => ' & ',
      'prefix' => '',
      'suffix' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['max'] = [
      '#title' => $this->t('Maximum number of values to display'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('max'),
      '#description' => $this->t('Enter zero to not limit the number of displayed values.'),
    ];
    $elements['omit_after'] = [
      '#title' => $this->t('If max is exceeded, omit values after'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $this->getSetting('omit_after'),
    ];
    $elements['omitted'] = [
      '#title' => $this->t('What to show in place of omitted values if max is exceeded'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('omitted'),
    ];
    $elements['normal_separator'] = [
      '#title' => $this->t('Normal separator'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('normal_separator'),
    ];
    $elements['final_separator'] = [
      '#title' => $this->t('Final separator'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('final_separator'),
    ];
    $elements['prefix'] = [
      '#title' => $this->t('Prefix'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('prefix'),
    ];
    $elements['suffix'] = [
      '#title' => $this->t('Suffix'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('suffix'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $max = empty($this->getSetting('max')) ? 2 : $this->getSetting('max');
    $summary[] = "Inline e.g. '" . $this->getExample($max) . "' or '" . $this->getExample($max + 1) . "'";
    return $summary;
  }

  /**
   * Helper to get example.
   */
  protected function getExample($count) {
    $elements = $this->getInlineElements(range(1, $count));
    return Html::decodeEntities(implode('', $elements));
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $parentElements = parent::viewElements($items, $langcode);
    $inlineElements = $this->getInlineElements($parentElements);

    foreach ($inlineElements as $key => $inlineElement) {
      if (!is_array($inlineElement)) {
        $inlineElements[$key] = ['#markup' => $inlineElement];
      }
    }
    return [0 => $inlineElements];
  }

  /**
   * Helper to get inline elements.
   */
  protected function getInlineElements($elements) {
    $preparer = new SmartListPreparer();
    $preparer->setSettings($this->getSettings());
    return $preparer->prepare($elements);
  }

}
