<?php

namespace Drupal\ahs_miscellaneous\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form_preview\Plugin\Field\FieldWidget\InlineEntityFormPreview;

/**
 * Extends the preview inline widget to prevent adding.
 *
 * @FieldWidget(
 *   id = "ahs_miscellaneous_inline_entity_form_preview_single",
 *   label = @Translation("Inline entity form - Preview single"),
 *   multiple_values = true,
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   }
 * )
 */
class InlineEntityFormPreviewSingle extends InlineEntityFormPreview {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (isset($element['actions']['ief_add'])) {
      unset($element['actions']['ief_add']);
    }

    return $element;
  }

}
