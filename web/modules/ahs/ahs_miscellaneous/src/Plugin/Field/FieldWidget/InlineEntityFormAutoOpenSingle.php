<?php

namespace Drupal\ahs_miscellaneous\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;
use Drupal\inline_entity_form\TranslationHelper;

/**
 * Extends the preview inline widget to prevent adding items even if
 * storage allows it
 *
 * @FieldWidget(
 *   id = "ahs_miscellaneous_inline_entity_form_auto_open_single",
 *   label = @Translation("Inline entity form - Auto-open single"),
 *   multiple_values = true,
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   }
 * )
 */
class InlineEntityFormAutoOpenSingle extends InlineEntityFormComplex {
  /**
   * The field items.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected $items;

  /**
   * {@inheritdoc}
   */
  protected function prepareFormState(FormStateInterface $form_state, FieldItemListInterface $items, $translating = FALSE) {
    $widget_state = $form_state->get(['inline_entity_form', $this->iefId]);
    if (empty($widget_state)) {
      $widget_state = [
        'instance' => $this->fieldDefinition,
        'form' => NULL,
        'delete' => [],
        'entities' => [],
      ];
      // Store $items entities in the widget state, for further manipulation.
      foreach ($items->referencedEntities() as $delta => $entity) {
        // Display the entity in the correct translation.
        if ($translating) {
          $entity = TranslationHelper::prepareEntity($entity, $form_state);
        }
        $widget_state['entities'][$delta] = [
          'entity' => $entity,
          'weight' => $delta,
          'form' => 'edit',
          'needs_save' => $entity->isNew(),
        ];
      }
      $form_state->set(['inline_entity_form', $this->iefId], $widget_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    // Store the field items, so that ::canAddNew() called in parent::formMultipleElements() can access them.
    $this->items = $items;
    $elements = parent::form($items, $form, $form_state, $get_delta);
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    //if (isset($element['actions']['ief_add'])) {
    //  unset($element['actions']['ief_add']);
   // }
    $element['entities']['#table_fields'] = [];
    $element['#title_display'] = 'invisible';
    $element['#attributes']['class'][] = 'auto-open-edit-form';
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function buildEntityFormActions(array $element) {
    $element = parent::buildEntityFormActions($element);
    if (isset($element['actions'])) {
      unset($element['actions']);
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCreateBundles() {
    // We override getCreateBundles() not canAddNew() because the latter is not used everywhere it should be.
    if ($this->items->isEmpty()) {
      return parent::getCreateBundles();
    }
    return [];
  }

}
