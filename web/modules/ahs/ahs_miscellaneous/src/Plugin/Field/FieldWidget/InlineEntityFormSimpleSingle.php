<?php

namespace Drupal\ahs_miscellaneous\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormSimple;

/**
 * Extends the simple inline widget to prevent adding items.
 *
 * @FieldWidget(
 *   id = "ahs_miscellaneous_inline_entity_form_simple_single",
 *   label = @Translation("Inline entity form - Simple single"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   },
 *   multiple_values = false
 * )
 */
class InlineEntityFormSimpleSingle extends InlineEntityFormSimple {

  /**
   * Returns all field items.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected $items;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'add_one' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['add_one'] = [
      '#title' => $this->t('Add one if empty.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('add_one'),
      '#description' => $this->t('If unchecked, this widget will only allow modifying existing items, not adding any.'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $add = empty($this->getSetting('add_one')) ? "None" : 'One';
    $summary[] = "Add: " . $add;
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    // Store the field items, so that ::canAddNew() called in
    // parent::formMultipleElements() can access them.
    $this->items = $items;
    return parent::formMultipleElements($items, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // As this widget can be configured to not support adding items,
    // it can support any number of bundles.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function canAddNew() {
    if ($this->items->isEmpty() && !empty($this->getSetting('add_one'))) {
      return parent::canAddNew();
    }
    return FALSE;
  }

}
