<?php

namespace Drupal\ahs_miscellaneous\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\field\EntityField;

/**
 * A views entity field plugin that allows skipping field access checks.
 * 
 * This does not completely work, as sometimes view rendering still goes
 * through views/Entity/Render/EntityFieldRenderer, EntityViewDisplay,
 * FieldItemList and EntityAccessHControlHAndler.
 */
class EntityFieldWithAccessOption extends EntityField {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    if ($this->options['ahs_miscellaneous_skip_access']) {
      return TRUE;
    }
    else {
      return parent::access($account);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['ahs_miscellaneous_skip_access'] = [
      'default' => FALSE,
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['ahs_miscellaneous_skip_access'] = [
      '#title' => $this->t('Skip field access checks'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['ahs_miscellaneous_skip_access'],
    ];
  }

}
