<?php

namespace Drupal\ahs_miscellaneous\Plugin\views\field;

use Drupal\views\Plugin\views\field\Custom;

/**
 * A handler to provide a field extending custom by global token replacement.
 *
 * Copied from https://drupal.stackexchange.com/questions/259534/what-is-the-replacement-pattern-in-rewrite-ouput-for-current-user-id
 * Original motivation was to show different order
 * links depending on current user.
 */
class CustomWithGlobalTokens extends Custom {

  /**
   * {@inheritdoc}
   */
  protected function renderAltered($alter, $tokens) {
    return $this->viewsTokenReplace($this->globalTokenReplace($alter['text']), $tokens);
  }

}
