<?php

namespace Drupal\ahs_miscellaneous\Plugin\DateAugmenter;

use Drupal\ahs_miscellaneous\EntityWithTimezoneInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\date_augmenter\DateAugmenter\DateAugmenterPluginBase;
use Drupal\date_augmenter\Plugin\PluginFormTrait;

/**
 * Date Augmenter plugin to append timezone information.
 *
 * @DateAugmenter(
 *   id = "ahs_append_timezone",
 *   label = @Translation("Append timezone information"),
 *   description = @Translation("Appends js timezone format to a date."),
 *   weight = 0
 * )
 */
class AppendTimezone extends DateAugmenterPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * {@inheritdoc}
   */
  public function augmentOutput(array &$output, DrupalDateTime $start, DrupalDateTime $end = NULL, array $options = []) {
    $settings = $options['settings'] ?? $this->getConfiguration();

    // Timezone is not always relevant, e.g. for in-person only events.
    if (isset($options['entity']) && $options['entity'] instanceof EntityWithTimezoneInterface) {
      if (!$options['entity']->willBeAttendedFromMultipleTimezones()) {
        if ($settings['hide_if_not_attended_from_multiple_timezones']) {
          return;
        }
      }
    }

    $timezone = $options['timezone'] ?? $options['entity']->getTimezone() ?? \Drupal::config('system.date')->get('timezone.default');

    // Create a wrapper for the existing output and timezone
    $new_output = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['ahs-date--with-timezone'],
        'data-ahs-timezone' => $timezone ?? '',
      ],
      'content' => [
        'date' => $output,
        'timezone' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => [
            'class' => ['ahs-date--timezone'],
          ],
        ],
      ],
    ];

    if ($settings['show_only_if_different']) {
      $new_output['#attributes']['data-ahs-show-only-if-different'] = '';
    }

    // Only attach the library if we have a timezone
    if ($timezone) {
      $new_output['#attached'] = [
        'library' => [
          'ahs_miscellaneous/timezone',
        ],
      ];
    }

    $output = $new_output;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'show_only_if_different' => FALSE,
      'hide_if_not_attended_from_multiple_timezones' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFields(array $form, ?array $settings, $field_definition) {
    $settings = empty($settings) ? $this->defaultConfiguration() : $settings + $this->defaultConfiguration();

    $form['show_only_if_different'] = [
      '#title' => $this->t('Show only if browser timezone differs'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_only_if_different'],
      '#description' => $this->t('Only show the timezone if the browser timezone is different from the content timezone.'),
    ];

    $form['hide_if_not_attended_from_multiple_timezones'] = [
      '#title' => $this->t('Hide if not attended from multiple timezones'),
      '#type' => 'checkbox',
      '#default_value' => $settings['hide_if_not_attended_from_multiple_timezones'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $this->configurationFields($form, $this->configuration, NULL);
  }

}
