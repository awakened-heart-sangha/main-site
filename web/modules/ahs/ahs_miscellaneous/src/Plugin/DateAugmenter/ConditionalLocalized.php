<?php

namespace Drupal\ahs_miscellaneous\Plugin\DateAugmenter;

use Drupal\ahs_miscellaneous\EntityWithTimezoneInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\date_augmenter\DateAugmenter\DateAugmenterPluginBase;
use Drupal\date_augmenter\Plugin\PluginFormTrait;

/**
 * Date Augmenter plugin to conditionally show localized time.
 *
 * @DateAugmenter(
 *   id = "ahs_conditional_localized",
 *   label = @Translation("Show localized time only if relevant"),
 *   description = @Translation("Shows localized time only when event will be attended from multiple timezones."),
 *   weight = 20
 * )
 */
class ConditionalLocalized extends DateAugmenterPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * {@inheritdoc}
   */
  public function augmentOutput(array &$output, DrupalDateTime $start, DrupalDateTime $end = NULL, array $options = []) {
    // Skip if no entity or not the right type
    if (!isset($options['entity']) || !$options['entity'] instanceof EntityWithTimezoneInterface) {
      return;
    }

    // Remove localize class if the event won't be attended from multiple timezones.
    if (!$options['entity']->willBeAttendedFromMultipleTimezones()) {
      $this->removeClassRecursively($output, 'smart-date--localize');
    }
  }

  /**
   * Recursively removes a class from all levels of a render array.
   *
   * @param array &$array
   *   The render array to process.
   * @param string $class
   *   The class to remove.
   */
  protected function removeClassRecursively(array &$array, $class) {
    if (isset($array['#attributes']['class']) && is_array($array['#attributes']['class'])) {
      if (($key = array_search($class, $array['#attributes']['class'])) !== FALSE) {
        unset($array['#attributes']['class'][$key]);
      }
    }

    foreach ($array as &$value) {
      if (is_array($value)) {
        $this->removeClassRecursively($value, $class);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }
  /**
   * Create configuration fields for the plugin form, or injected directly.
   *
   * @param array $form
   *   The form array.
   * @param array $settings
   *   The setting to use as defaults.
   * @param mixed $field_definition
   *   A parameter to define the field being modified. Likely FieldConfig.
   *
   * @return array
   *   The updated form array.
   */
  public function configurationFields(array $form, ?array $settings, $field_definition) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->configurationFields($form, $this->configuration);
    return $form;
  }

}