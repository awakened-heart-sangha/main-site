<?php

namespace Drupal\ahs_miscellaneous\Plugin\DateAugmenter;

use Drupal\ahs_miscellaneous\EntityWithTimezoneInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\date_augmenter\DateAugmenter\DateAugmenterPluginBase;
use Drupal\date_augmenter\Plugin\PluginFormTrait;
use Drupal\smart_date\Entity\SmartDateFormat;
use Drupal\smart_date\SmartDateTrait;

/**
 * Date Augmenter plugin to inject Registration links.
 *
 * @DateAugmenter(
 *   id = "ahs_append_localized",
 *   label = @Translation("Append an additonal date when using localized"),
 *   description = @Translation("Appends the localized date to the date in the entity or field timezone."),
 *   weight = 10
 * )
 */
class AppendLocalized extends DateAugmenterPluginBase implements PluginFormInterface {

  use PluginFormTrait;
  use SmartDateTrait;

  /**
   * Builds and returns a render array for the task.
   *
   * @param array $output
   *   The existing render array, to be augmented, passed by reference.
   * @param Drupal\Core\Datetime\DrupalDateTime $start
   *   The object which contains the start time.
   * @param Drupal\Core\Datetime\DrupalDateTime $end
   *   The optionalobject which contains the end time.
   * @param array $options
   *   An array of options to further guide output.
   */
  public function augmentOutput(array &$output, DrupalDateTime $start, DrupalDateTime $end = NULL, array $options = []) {
    if (isset($options['entity']) && $options['entity'] instanceof EntityWithTimezoneInterface) {
      // Localized timezone is not always relevant, e.g. for in-person only events.
      if(!$options['entity']->willBeAttendedFromMultipleTimezones()) {
        return;
      };
    }

    $settings = $options['settings'] ?? $this->getConfiguration();

    $format_storage = \Drupal::entityTypeManager()->getStorage('smart_date_format');
    $format = $format_storage->load($settings['unlocalized_format']);
    $format_settings = $format->getOptions();
    $unlocalized = static::formatSmartDate($start->getTimestamp(), $end->getTimestamp(), $format_settings, $options['timezone'] ?? NULL);

    $localized = $output;
    $timezone = $options['timezone'] ?? $options['entity']->getTimezone() ?? \Drupal::config('system.date')->get('timezone.default');
    $tag = $settings['inline'] ? 'span' : 'div';

    // Check if the input has timezone information from AppendTimezone
    $needs_timezone = FALSE;
    if (isset($output['#attributes']['class']) && in_array('ahs-date--with-timezone', $output['#attributes']['class'])) {
      $needs_timezone = TRUE;
      unset($output['#attributes']['class']['ahs-date--with-timezone']);
    }

    $structure = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      'times' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => [
          'class' => ['ahs-date--times'],
        ],
      ],
    ];


    $new_output = [
      'unlocalized' => $structure,
      'localized' => [
        '#type' => 'html_tag',
        '#tag' => $tag,
        '#attributes' => [
          'class' => ['ahs-date--localized'],
        ],
        'prefix' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $settings['prefix'],
        ],
        'content' => $structure,
        'suffix' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $settings['suffix'],
        ],
      ],
    ];

    if ($needs_timezone) {
      $new_output['unlocalized']['timezone'] = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => [
          'class' => ['ahs-date--timezone'],
        ],
      ];
      $new_output['unlocalized']['#attributes']['class'][] = 'ahs-date--with-timezone';
      $new_output['localized']['#attributes']['class'][] = 'ahs-date--with-timezone';
    }

    // Check if the localized output has data-ahs-show-only-if-different attribute
    if (isset($localized['#attributes']['data-ahs-show-only-if-different'])) {
      $new_output['unlocalized']['#attributes']['data-ahs-show-only-if-different'] = '';
    }

    $new_output['unlocalized']['#tag'] = $tag;
    $new_output['unlocalized']['#attributes']['class'][] = 'ahs-date--unlocalized';
    if ($timezone) {
      $new_output['unlocalized']['#attributes']['data-ahs-timezone'] = $timezone;
    }
    $new_output['unlocalized']['times']['content'] = $unlocalized;

    // Need to use flex display to avoid extra white space after/before prefix/suffix.
    $new_output['localized']['#attributes']['style'] = 'visibility: hidden; display: inline-flex;';
    $new_output['localized']['content']['times']['content'] = $localized;

    if ($settings['inline']) {
      $new_output['unlocalized']['#attributes']['style'] = 'display:inline-block; margin-right: 1em;';
    }

    // Only attach the library if we have a timezone
    if ($timezone) {
      $new_output['#attached'] = [
        'library' => [
          'ahs_miscellaneous/timezone',
        ],
      ];
    }

    if (empty($settings['prefix'])) {
      unset($new_output['localized']['prefix']);
    }
    if (empty($settings['suffix'])) {
      unset($new_output['localized']['suffix']);
    }

    $output = $new_output;
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'unlocalized_format' => 'default',
      'prefix' => '(',
      'suffix' => ')',
      'inline' => FALSE,
    ];
  }

  /**
   * Create configuration fields for the plugin form, or injected directly.
   *
   * @param array $form
   *   The form array.
   * @param array $settings
   *   The setting to use as defaults.
   * @param mixed $field_definition
   *   A parameter to define the field being modified. Likely FieldConfig.
   *
   * @return array
   *   The updated form array.
   */
  public function configurationFields(array $form, ?array $settings, $field_definition) {
    $settings = empty($settings) ? $this->defaultConfiguration() : $settings + $this->defaultConfiguration();


    // Ask the user to choose a Smart Date Format.
    $formatOptions = [];

    $smartDateFormats = \Drupal::entityTypeManager()
      ->getStorage('smart_date_format')
      ->loadMultiple();

    foreach ($smartDateFormats as $type => $format) {
      if ($format instanceof SmartDateFormat) {
        $formatted = static::formatSmartDate(time(), time() + 3600, $format->getOptions(), NULL, 'string');
        $formatOptions[$type] = $format->label() . ' (' . $formatted . ')';
      }
    }

    $form['unlocalized_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Unlocalized Format'),
      '#description' => $this->t('Choose which display configuration to use.'),
      '#default_value' => $settings['unlocalized_format'],
      '#options' => $formatOptions,
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#description' => $this->t('The prefix to display before the localized date.'),
      '#default_value' => $settings['prefix'],
    ];

    $form['suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix'),
      '#description' => $this->t('The suffix to display after the localized date.'),
      '#default_value' => $settings['suffix'],
    ];

    $form['inline'] = [
      '#title' => $this->t('Show localized date inline'),
      '#type' => 'checkbox',
      '#default_value' => $settings['inline'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->configurationFields($form, $this->configuration);
    return $form;
  }

}
