<?php

namespace Drupal\ahs_miscellaneous\Plugin\Notifier;

use Drupal\message_notify\Exception\MessageNotifyException;
use Drupal\message_notify\Plugin\Notifier\Email;

/**
 * Email notifier.
 *
 * @Notifier(
 *   id = "ahs_email",
 *   title = @Translation("AHS Email"),
 *   description = @Translation("Send messages via email, optionally specifying a reply address."),
 *   viewModes = {
 *     "mail_subject",
 *     "mail_body"
 *   }
 * )
 */
class AhsEmail extends Email {

  /**
   * {@inheritdoc}
   */
  public function deliver(array $output = []) {
    /** @var \Drupal\user\UserInterface $account */
    $account = $this->message->getOwner();

    if (!$this->configuration['mail'] && !$account->id()) {
      // The message has no owner and no mail was passed. This will cause an
      // exception, we just make sure it's a clear one.
      throw new MessageNotifyException('It is not possible to send a Message for an anonymous owner. You may set an owner using ::setOwner() or pass a "mail" to the $options array.');
    }

    $mail = $this->configuration['mail'] ?: $account->getEmail();

    if (!$this->configuration['language override']) {
      $language = $account->getPreferredLangcode();
    }
    else {
      $language = $this->message->language()->getId();
    }

    // The subject in an email can't be with HTML, so strip it.
    $output['mail_subject'] = trim(strip_tags($output['mail_subject']));
    $output['mail_body'] = $output['mail_body'];

    // Pass the message entity along to hook_drupal_mail().
    $output['message_entity'] = $this->message;

    $result = $this->mailManager->mail(
      'message_notify',
      $this->message->getTemplate()->id(),
      $mail,
      $language,
      $output,
      // This is the only line in the plugin that departs from the parent.
      ($this->configuration['reply'] ?: NULL)
    );

    return $result['result'];
  }

}
