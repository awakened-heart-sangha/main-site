<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

/**
 * Updates all media thumbnails.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0001_update_thumbnails_after_migration_to_core_media(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityQuery('media')
      ->accessCheck(TRUE)
      ->condition('bundle', 'youtube')
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      \Drupal::queue('media_entity_thumbnail')
        ->createItem(['id' => $id]);
      \Drupal::logger('ahs_miscellaneous')
        ->notice("Queued thumbail update for media entity " . $id);
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error queuing thumbail update for media entity " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'media thumbnails update queued');
}

/**
 * Updates all media thumbnails.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0002_cleanup_old_video_thumbnails(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityQuery('file')
      ->accessCheck(TRUE)
      ->condition('uri', 'public://video_thumbnails', 'STARTS_WITH')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('file');
      $file = $storage->load($id);
      $file->delete();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error deleting managed file " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'old video thumbnails deleted');
}

/**
 * Updates all media thumbnails.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0003_new_audio_thumbnail(&$sandbox) {
  $storage = \Drupal::entityTypeManager()->getStorage('file');
  $file = $storage->load(1479);
  $file->setFileUri('public://media-icons/generic/audio.png');
  $file->save();
}

/**
 * Updates session leaders to be user references.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0004_update_session_leader_to_user(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('node')->getQuery()
      ->condition('type', 'session')
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $session = $storage->load($id);
      $leaders = $session->get('field_leader');
      foreach ($leaders as &$leader) {
        $leaderNode = $storage->load($leader->target_id);
        $uid = $leaderNode->get('field_user')->target_id;
        $leader->target_id = $uid;
      }
      $session->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error updating leader for session " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'session leaders updated');
}

/**
 * Convert comment format to basic_html.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0005_full_html_comment(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('comment')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('comment_body.format', 'full_html')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('comment');
      $entity = $storage->load($id);
      $entity->comment_body->format = 'basic_html';
      $entity->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error updating comment " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'comment formats updated');
}

/**
 * Convert group description format to basic_html.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0006_full_html_group(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('group')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('field_description.format', 'full_html')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('group');
      $entity = $storage->load($id);
      $entity->field_description->format = 'advanced_html';
      $entity->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error updating group " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'group formats updated');
}

/**
 * Convert node body format to basic_html.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0007_full_html_node(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('node')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('body.format', 'full_html')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $entity = $storage->load($id);
      $entity->body->format = 'advanced_html';
      $entity->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error updating node " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'node formats updated');
}

/**
 * Convert node body format to basic_html.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0008_full_html_para(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('paragraph')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('field_text.format', 'full_html')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('paragraph');
      $entity = $storage->load($id);
      $entity->field_text->format = 'advanced_html';
      $entity->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error updating para " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'para formats updated');
}

/**
 * Convert block format to basic_html.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_miscellaneous_deploy_0009_full_html_block(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('block_content')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('body.format', 'full_html')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage('block_content');
      $entity = $storage->load($id);
      $entity->body->format = 'advanced_html';
      $entity->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_miscellaneous')
        ->error("Error updating block " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'block formats updated');
}
