(function ($, Drupal) {
  'use strict';

  function getFormattedTimezone(date, timeZone = null) {
    // Return empty string if timeZone is null, undefined, or empty string
    if (!timeZone) {
      return '';
    }
    const formatter = new Intl.DateTimeFormat(document.documentElement.lang, {
      timeZone: timeZone,
      timeZoneName: 'shortGeneric'
    });
    const parts = formatter.formatToParts(date);
    const timezonePart = parts.find(part => part.type === 'timeZoneName');
    return timezonePart ? timezonePart.value
      .replace(/ Time$/, ' time')
      .replace(/^United Kingdom time$/, 'UK time') : '';
  }

  Drupal.behaviors.ahsTimezone = {
    // Set higher weight to run after Smart Date
    weight: 100,

    attach: function (context, settings) {
      // Get browser's timezone
      const browserTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

      // Show localized appended date if the browser is in a different timezone.
      once('ahsShowLocalized', '.ahs-date--localized', context).forEach(function (element) {
        // Find the previous sibling which should be the unlocalized element
        const unlocalizedElement = element.previousElementSibling;
        if (!unlocalizedElement || !unlocalizedElement.classList.contains('ahs-date--unlocalized')) return;

        // Localized version is only shown if the browser is in a different timezone.
        const dataTimezone = unlocalizedElement.getAttribute('data-ahs-timezone');
        if (dataTimezone && dataTimezone !== browserTimezone) {
          // Show the localized version by removing the inline style
          element.style.removeProperty('visibility');
        }
      });

      // Append js timezones.
      once('ahsTimezone', '.ahs-date--with-timezone', context).forEach(function (element) {
        const dataTimezone = element.getAttribute('data-ahs-timezone');
        if (!dataTimezone) return;

        const timezoneSpan = element.querySelector('.ahs-date--timezone');
        if (!timezoneSpan) return;

        // Get any time elements to use their date if available
        const timeElement = element.querySelector('time');
        const date = timeElement && timeElement.dateTime ? new Date(timeElement.dateTime) : new Date();

        // For localized elements, use browser timezone instead of data-timezone
        const isLocalized = element.querySelector('.smart-date--localize');
        const effectiveTimezone = isLocalized ? browserTimezone : dataTimezone;

        // Only show timezone if configured to always show or if it differs when show_only_if_different is enabled
        if (element.hasAttribute('data-ahs-show-only-if-different')) {
          if (dataTimezone !== browserTimezone) {
            timezoneSpan.textContent = getFormattedTimezone(date, effectiveTimezone);
          }
        } else {
          timezoneSpan.textContent = getFormattedTimezone(date, effectiveTimezone);
        }
      });
    }
  };

})(jQuery, Drupal);