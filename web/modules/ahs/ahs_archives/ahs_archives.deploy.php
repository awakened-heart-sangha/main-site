<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\group\Entity\GroupRelationshipType;

/**
 * Updates managed files to s3 from dropbox.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_archives_deploy_0004_update_audio_to_s3(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('file')
      ->getQuery()
      ->condition('uri', 'dropboxwebarchive://recordings/audio/', 'STARTS_WITH')
      ->accessCheck(FALSE)
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $file = \Drupal::entityTypeManager()->getStorage('file')->load($id);
      $path = substr($file->getFileUri(), strlen('dropboxwebarchive://recordings/audio/'));
      $basename = \Drupal::service('file_system')->basename($file->getFileUri());

      // Catch '2011/2011-12-03 0000 Mandala Q+A 1.mp3' and
      // '2011/2011-12-04 0000 Mandala Q+A 2.mp3'.
      $path = str_replace('+', '&', $path);
      $basename = str_replace('+', '&', $basename);

      $file->setFileUri("s3://audio/" . $path);
      $file->setFilename($basename);
      $file->origname->value = $basename;
      $file->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_archives')
        ->error("Error changing file uri " . $id . " " . $file->getFileUri() . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'file uris modified');
}

/**
 * Updates audio media to use field_audio not field_dropbox.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_archives_deploy_0005_update_media_to_audio_field(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('media')
      ->getQuery()
      ->condition('bundle', 'audio')
      ->accessCheck(FALSE)
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $media = \Drupal::entityTypeManager()->getStorage('media')->load($id);
      $media->get('field_audio')->setValue($media->get('field_dropbox')->getValue());
      $media->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_archives')
        ->error("Error settings field_audio on media " . $id . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'media field_audio set');
}

/**
 * Resaves sessions to trigger media and file renaming.
 *
 * @see ahs_archives_node_update()
 *
 * Implements hook_deploy_NAME().
 */
function ahs_archives_deploy_0006_update_file_names_from_session_titles(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('node')
      ->getQuery()
      ->condition('type', 'session')
      ->accessCheck(FALSE)
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $session = \Drupal::entityTypeManager()->getStorage('node')->load($id);
      $session->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_archives')
        ->error("Error resaving session " . $id . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'session resaved');
}

/**
 * Implements hook_deploy_NAME().
 */
function ahs_archives_deploy_0007_uninstall_group_media(&$sandbox) {
  $storage = \Drupal::entityTypeManager()->getStorage('group_content');
  $query = $storage->getQuery()->accessCheck(FALSE);
  $group_content_types = array_merge(array_keys(GroupRelationshipType::loadByPluginId('group_media:audio')), array_keys(GroupRelationshipType::loadByPluginId('group_media:youtube')));
  $groupMediaIds = $query
    ->condition('type', $group_content_types, 'IN')
    ->accessCheck(FALSE)
    ->execute();
  $contents = $storage->loadMultiple($groupMediaIds);
  $storage->delete($contents);
}

/**
 * Create transcripts for all media.
 *
 * @see ahs_archives_node_update()
 *
 * Implements hook_deploy_NAME().
 */
function ahs_archives_deploy_0008_create_transcript_nodes(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('media')
      ->getQuery()
      ->condition('bundle', ['youtube', 'audio'], 'IN')
      ->accessCheck(FALSE)
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $media = \Drupal::entityTypeManager()->getStorage('media')->load($id);
      ahs_archives_media_insert($media);
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_archives')
        ->error("Error creating media transcript " . $id . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'transcripts created');
}

/**
 * Get whisper transcripts from Kevin for all media.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_archives_deploy_0009_get_kevin_transcripts(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('media')
      ->getQuery()
      ->condition('bundle', ['youtube', 'audio'], 'IN')
      ->accessCheck(FALSE)
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $payload = ['media_id' => $id];
      $job = Job::create('ahs_kevin_transcript_import', $payload);
      if ($job instanceof Job) {
        $q = Queue::load('default');
        $q->enqueueJob($job);
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_archives')
        ->error("Error creating kevin import job " . $id . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'kevin transcript jobs created');
}