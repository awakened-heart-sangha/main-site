<?php

namespace Drupal\ahs_archives\Entity;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\node\Entity\Node;
use Drupal\ahs_miscellaneous\EntityWithTimezoneInterface;

class NodeSession extends Node implements EntityWithTimezoneInterface {

  /**
   * The primary group for the session.
   *
   * @var AhsGroup|null
   */
  protected ?AhsGroup $primaryGroup = NULL;

  /**
   * {@inheritdoc}
   */
  public function getTimezone(): string {
    // First try to get timezone from primary group
    if ($primaryGroup = $this->getPrimaryGroup()) {
      return $primaryGroup->getTimezone();
    }

    // Fallback to site default
    return \Drupal::config('system.date')->get('timezone.default');
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsWithTimezone(): array {
    return ['field_datetime'];
  }

  /**
   * {@inheritdoc}
   */
  public function willBeAttendedFromMultipleTimezones(): bool {
    if ($primaryGroup = $this->getPrimaryGroup()) {
      return $primaryGroup->willBeAttendedFromMultipleTimezones();
    }
    return FALSE;
  }

  /**
   * Get the primary group.
   *
   * This is the group that the session is group content for,
   * with the field_primary set to TRUE on the group relationship.
   * There should only be one such group per session, even if the
   * session is group content in multiple groups.
   *
   * @return AhsGroup|null
   *   The primary group for the session, if there is one.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPrimaryGroup(): ?AhsGroup {
    if (!isset($this->primaryGroup)) {
      $contents = \Drupal::entityTypeManager()
        ->getStorage('group_content')
        ->loadByEntity($this);
      foreach ($contents as $content) {
        $plugin_id = $content->getPlugin()->getPluginId();
        if ($plugin_id === "group_node:session" && $content->hasField('field_primary')) {
          if (!$content->get('field_primary')->isEmpty() && $content->get('field_primary')->value == TRUE) {
            $this->primaryGroup = $content->getGroup();
            break;
          }
        }
      }
    }
    return $this->primaryGroup;
  }

}
