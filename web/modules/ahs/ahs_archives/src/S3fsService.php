<?php

namespace Drupal\ahs_archives;

use Drupal\s3fs\S3fsService as parentS3fsService;

/**
 * Modifies S3fsService service to use setupTempTable().
 */
class S3fsService extends parentS3fsService {

  /**
   * {@inheritdoc}
   */
  public function refreshCache(array $config) {
    $is_refreshed = \Drupal::state()->get('ahs_archives.s3fs_is_refreshed');
    if (is_null($is_refreshed)) {
      \Drupal::state()->set('ahs_archives.s3fs_is_refreshed', TRUE);
      parent::refreshCache($config);
    }

    \Drupal::state()->delete('ahs_archives.s3fs_is_refreshed');
  }

}
