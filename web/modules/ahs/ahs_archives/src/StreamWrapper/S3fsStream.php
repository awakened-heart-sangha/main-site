<?php

namespace Drupal\ahs_archives\StreamWrapper;

use Drupal\Core\Site\Settings;
use Drupal\s3fs\StreamWrapper\S3fsStream as parentStreamWrapper;

/**
 * Modifies S3 classes to use a different host.
 *
 * We use Flexify running on an Azure VM as an S3 API proxy for
 * Azure blob storage. Flexify seems to require presigned URLs to be
 * presigned using the ip/hostname that Flexify recognises itself as having.
 * However, in order to allow https access (necessary to prevent
 * the user's browser from issuing insecure content warnings) we need to
 * use a proxy like Azure Front Door. so we need the S3 url to be presigned
 * using the VM's ip/hostname that Flexify expects, but
 * the url's hostname needs to be that of the Front Door.
 *
 * This may relate to https://www.drupal.org/project/s3fs/issues/3278421.
 */
class S3fsStream extends parentStreamWrapper {

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    $url   = parent::getExternalUrl();
    $parts = parse_url($url);
    $path  = $parts['path'] ?? '';
    $query = empty($parts['query']) ? '' : ('?' . $parts['query']);
    $host  = Settings::get('ahs_archives.s3fs_host');
    $url   = $host . $path . $query;
    return $url;
  }

}
