<?php

namespace Drupal\ahs_archives\EventSubscriber;

use Drupal\advancedqueue\Job;
use Drupal\ahs_archives\TranscriptDispatcher;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A TranscriptRequestSubscriber that handles transcipts being requested.
 *
 * The state 'requested' can be arrived at manually using the 'request'
 * transition, or programatically using the 'queue' transition. A
 * programatic request should simply queue the request; a manual request should
 * try to dispatch it immediately falling back to queuing if that fails.
 */
class TranscriptRequestSubscriber implements EventSubscriberInterface {

  /**
   * The transcript dispatcher.
   *
   * @var \Drupal\ahs_archives\TranscriptDispatcher
   */
  protected $transcriptDispatcher;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TranscriptRequestSubscriber object.
   *
   * @param \Drupal\ahs_archives\TranscriptDispatcher $transcript_dispatcher
   *   The transcript dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(TranscriptDispatcher $transcript_dispatcher, EntityTypeManagerInterface $entity_type_manager) {
    $this->transcriptDispatcher = $transcript_dispatcher;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['ahs_archives_transcription.request.pre_transition'] = 'onRequest';
    $events['ahs_archives_transcription.retry.pre_transition'] = 'onRequest';
    $events['ahs_archives_transcription.post_transition'] = 'onRequested';
    return $events;
  }

  /**
   * Dispatch a request immediately if manually requested.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onRequest(WorkflowTransitionEvent $event) {
    $result = $this->transcriptDispatcher->dispatch($event->getEntity());

    // If successful, update the state so that
    // it doesn't get queued for dispatch.
    if ($result) {
      $event->getEntity()->set('field_state', 'transcribing');
    }
  }

  /**
   * Queue a transcription request if it gets to the requested state.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onRequested(WorkflowTransitionEvent $event) {
    /** @var \Drupal\state_machine\Plugin\Workflow\WorkflowTransition $transition */
    $transition = $event->getTransition();
    /** @var \Drupal\state_machine\Plugin\Workflow\WorkflowState $to_state */
    $to_state = $transition->getToState();
    $to_state_id = $to_state->getId();
    if ($to_state_id === 'requested') {
      $queue_storage = $this->entityTypeManager
        ->getStorage('advancedqueue_queue');
      /** @var \Drupal\advancedqueue\Entity\QueueInterface $recurring_queue */
      $queue = $queue_storage->load('transcription_request');
      $job = Job::create('ahs_archives_transcription_request', [
        'transcript_id' => $event->getEntity()->id(),
      ]);
      $queue->enqueueJob($job);
    }
  }

}
