<?php

namespace Drupal\ahs_archives\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that a session 's title and date form a unique label.
 */
class AhsArchivesSessionLabelValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRoute;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentRoute = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if (!isset($entity) || $entity->bundle() !== 'session' || empty($entity->getTitle())) {
      return;
    }

    $group = NULL;
    if ($entity->id()) {
      $group_content_storage = $this->entityTypeManager->getStorage('group_content');

      $group_content_query = $group_content_storage->getQuery();
      $group_content_query->accessCheck(FALSE);
      $group_content_query->condition('type', 'group_node-session', 'CONTAINS');
      $group_content_query->condition('entity_id', $entity->id());

      if ($group_content_ids = $group_content_query->execute()) {
        $group_content_id = reset($group_content_ids);

        /** @var \Drupal\group\Entity\GroupRelationshipInterface $group_content */
        $group_content = $group_content_storage->load($group_content_id);
        $group = $group_content->getGroup();
      }
    }

    if (!$group) {
      foreach($this->currentRoute->getParameters() as $key => $parameter) {
        if ('group' == $key) {
          if ($parameter instanceof GroupInterface) {
            $group = $parameter;
          }
          elseif (is_numeric($parameter)) {
            $group = $this->entityTypeManager->getStorage('group')->load($parameter);
          }
        }
      }
    }

    if (!$group) {
      return;
    }

    $session_ids = [];
    /** @var \Drupal\group\Entity\GroupRelationshipInterface $content */
    foreach($group->getRelationships('group_node:session') as $content) {
      if ($content->hasField('field_primary') && $content->get('field_primary')->getString()) {
        $node = $content->getEntity();

        if ($entity->id() && $entity->id() == $node->id()) {
          continue;
        }

        $session_ids[$node->id()] = $node->id();
      }
    }

    if (!$session_ids) {
      return;
    }

    // Sanitisation happens in presave, so after validation. But we have to
    // consider the impact of sanitisation when searching for duplicate titles.
    $sanitisedTitle = _ahs_archives_file_name_sanitise($entity->getTitle());

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('title', $sanitisedTitle);
    $query->condition($entity->getEntityType()->getKey('bundle'), 'session');
    $query->condition($entity->getEntityType()->getKey('id'), $session_ids, 'IN');

    if (!$entity->get('field_datetime')->isEmpty() && $entity->get('field_datetime')->date) {
      $date = $entity->get('field_datetime')->date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      $query->condition('field_datetime', $date);
    }
    else {
      $query->notExists('field_datetime');
    }

    $duplicate_exists = (bool) $query
      ->range(0, 1)
      ->count()
      ->execute();

    if ($duplicate_exists) {
      $date = !$entity->get('field_datetime')->isEmpty() ? $entity->get('field_datetime')->value : 'unspecified';
      $this->context->addViolation($constraint->message, [
        '%title' => $sanitisedTitle,
        '%date' => $date,
      ]);
    }
  }

}
