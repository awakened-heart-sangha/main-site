<?php

namespace Drupal\ahs_archives\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a session's date and title are unique.
 *
 * @Constraint(
 *   id = "AhsArchivesSessionLabelUnique",
 *   label = @Translation("Session date and title form a unique label", context = "Validation")
 * )
 */
class AhsArchivesSessionLabelUnique extends Constraint {

  /**
   * The error message.
   *
   * @var string
   */
  public $message = "A session with the title '%title' and date '%date' already exists.";

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return '\Drupal\ahs_archives\Plugin\Validation\Constraint\AhsArchivesSessionLabelValidator';
  }

}
