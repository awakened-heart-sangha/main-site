<?php

namespace Drupal\ahs_archives\Plugin\LinkExtractor;

use Drupal\linkchecker\Plugin\LinkExtractorBase;

/**
 * Extracts link from string field.
 *
 * @LinkExtractor(
 *   id = "ahs_archives_string_extractor",
 *   label = @Translation("Text field extractor"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class StringLinkExtractor extends LinkExtractorBase {

  /**
   * {@inheritdoc}
   */
  protected function extractUrlFromField(array $value) {
    return empty($value['value']) ? [] : [$value['value']];
  }

}
