<?php

namespace Drupal\ahs_archives\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;

/**
 * Extra field display plugin displaying information about whether audio is derived.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_archives_audio_derived",
 *   label = @Translation("Is derived"),
 *   description = @Translation("Displays whether audio is derived."),
 *   bundles = {
 *     "media.audio",
 *   }
 * )
 */
class AudioDerived extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $media) {
    $build = [];
    if ($media->get('field_derived')->value) {
      $pending = $media->get('field_audio')->isEmpty() ? " (pending)" : "";
      $build = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => "Extracted automatically from youtube" . $pending,
      ];
    }
    return $build;
  }

}
