<?php

namespace Drupal\ahs_archives\Plugin\EntityBrowser\Widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_browser\WidgetBase;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An entity browser widget that can select files from the S3 file system.
 *
 * Managed file entities are created for the selected files, if they do not
 * exist already.
 *
 * @EntityBrowserWidget(
 *   id = "ahs_s3fs_entity_browser_view",
 *   label = @Translation("AHS S3 file system view"),
 *   description = @Translation("Browses files from the S3 file system."),
 *   auto_select = FALSE
 * )
 */
class S3fsEntityBrowserS3View extends WidgetBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The S3 file system service.
   *
   * @var \Drupal\s3fs\S3fsServiceInterface
   */
  protected $s3fs;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // See https://www.previousnext.com.au/blog/safely-extending-drupal-8-plugin-classes-without-fear-of-constructor-changes.
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->s3fs = $container->get('s3fs');
    $instance->currentUser = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->database = $container->get('database');
    $instance->config = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory'),
      '#description' => $this->t('Directory within S3 bucket to browse.'),
      '#default_value' => $this->configuration['directory'],
    ];

    $form['multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Select multiple files'),
      '#description' => $this->t('Multiple file selection will only be possible if the source field allows more than one value.'),
      '#default_value' => $this->configuration['multiple'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'directory' => '',
      'multiple' => TRUE,
      'submit_text' => $this->t('Add selected files'),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);

    $extensions = $form_state->get(['entity_browser', 'validators', 'file', 'validators', 'file_validate_extensions']);
    $extensions = is_array($extensions) && !empty($extensions[0]) ? $extensions[0] : $extensions;
    $extensions = is_string($extensions) ? explode(" ", $extensions) : [];
    $s3_files = $this->getFiles($this->configuration['directory'], $extensions);

    $type = 'radios';
    $default_value = NULL;
    $field_cardinality = $form_state->get(['entity_browser', 'validators', 'cardinality', 'cardinality']);
    if ($field_cardinality != 1 && $this->configuration['multiple']) {
      $type = 'checkboxes';
      $default_value = [];
    }

    if (!empty($s3_files)) {
      $form['files'] = [
        '#type' => $type,
        '#options' => $s3_files,
        '#default_value' => $form_state->getUserInput()['files'] ?? $default_value,
      ];
    }
    else {
      $form['files'] = [
        '#markup' => $this->t('No files available.'),
      ];
    }

    return $form;
  }

  /**
   * Get s3 files to select from.
   *
   * @param string $directory
   *   The directory within the s3 bucket to limit files to.
   * @param array $extensions
   *   The file extensions to limit files to.
   *
   * @return array
   *   An array of pretty file names, keyed by the s3fs path.
   */
  protected function getFiles($directory = '', $extensions = []) {
    $prefix = trim("s3://$directory", "/") . '/';

    // Refresh s3 file info from remote storage.
    $s3fs_config = $this->config->get('s3fs.settings')->get();
    $this->s3fs->refreshCache($s3fs_config);

    // Get s3 files within specified directoryas an array.
    $query = $this->database->select('s3fs_file', 's');
    $query->fields('s', ['uri']);
    $query->condition('uri', $query->escapeLike($prefix) . '%', 'LIKE');
    $queried = $query->execute()->fetchAllAssoc('uri');

    $files = [];
    foreach ($queried as $path => $result) {
      // Objects that end in / are directories not files.
      if ($path[strlen($path) - 1] == '/') {
        continue;
      }

      // Only include the allowed extensions.
      $fileParts = explode(".", $path);
      $extension = end($fileParts);
      if (!empty($extensions) && !in_array(strtolower($extension), $extensions)) {
        continue;
      }

      // Only display the path within the specified folder.
      $display = substr($path, strlen($prefix));

      // Paths are easier to read with spacing between folders.
      $display = str_replace("/", "  /  ", $display);

      $files[$path] = $display;
    }
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array &$form, FormStateInterface $form_state) {
    $files = $this->getSelectedFiles($form_state);
    if (isset($form_state->getUserInput()['files']) && empty($files)) {
      $form_state->setError($form['widget'], $this->t('Please select a file.'));
    }

    // If there weren't any errors set, run the normal validators.
    if (empty($form_state->getErrors())) {
      parent::validate($form, $form_state);
    }
  }

  /**
   * Get the selected s3 files from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   An array of s3fs file paths.
   */
  protected function getSelectedFiles(FormStateInterface $form_state) {
    $files = [];
    $user_input = $form_state->getUserInput();
    if (isset($user_input['files'])) {
      if (is_array($user_input['files'])) {
        $files = array_filter($user_input['files']);
      }
      else {
        $files = [$user_input['files']];
      }
    }
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $files = $this->getSelectedFiles($form_state);
    $prepared = [];
    foreach ($files as $uri) {
      // Check if file is already managed by Drupal.
      $managedFiles = $this->entityTypeManager
        ->getStorage('file')
        ->loadByProperties(['uri' => $uri]);
      $managedFile = reset($managedFiles);
      if (!$managedFile) {
        // Save the file in Drupal.
        $managedFile = File::create([
          'uri' => $uri,
          'uid' => $this->currentUser->id(),
        ]);
      }
      $prepared[] = $managedFile;
    }
    return $prepared;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    $files = $this->prepareEntities($form, $form_state);
    array_walk(
      $files,
      function (FileInterface $file) {
        $file->setPermanent();
        $file->save();
      }
    );
    $this->selectEntities($files, $form_state);
  }

}
