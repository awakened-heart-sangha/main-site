<?php

namespace Drupal\ahs_archives\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed field for session transcripts.
 */
class TranscriptsItem extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->getEntity();

    $readable_media = [];
    $verbatim_media = [];

    if (!$node->get('field_media')->isEmpty()) {
      foreach ($node->get('field_media') as $item) {
        $media = $item->entity;
        if ($media) {
          if ($media->bundle() === 'youtube' && $media->hasField('field_transcript')) {
            if ($media->get('field_transcript')->first()?->getReadableTranscript()) {
              $readable_media[] = $media;
              break;
            }
            if ($media->get('field_transcript')->first()?->getVerbatimTranscript()) {
              $verbatim_media[] = $media;
            }
          }
          elseif ($media->bundle() === 'audio' && $media->hasField('field_transcript')) {
            if ((!$media->hasField('field_derived') || empty($media->get('field_derived')?->value))) {
              if ($media->get('field_transcript')->first()?->getReadableTranscript()) {
                $readable_media[] = $media;
              }
              if ($media->get('field_transcript')->first()?->getVerbatimTranscript()) {
                $verbatim_media[] = $media;
              }
            }
          }
        }
      }
    }

    $transcripts = $readable_media ? $readable_media : $verbatim_media;

    foreach ($transcripts as $delta => $media) {
      $this->list[$delta] = $this->createItem($delta, $media);
    }
  }

}
