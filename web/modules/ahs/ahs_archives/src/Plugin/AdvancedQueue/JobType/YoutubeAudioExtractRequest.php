<?php

namespace Drupal\ahs_archives\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\ahs_miscellaneous\SmartListPreparer;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Site\Settings;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\node\Entity\Node;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

/**
 * Defines a job type for extracting audio from youtube video.
 *
 * @AdvancedQueueJobType(
 *   id = "ahs_archives_youtube_audio_extract",
 *   label = @Translation("Send audio extraction request"),
 *   max_retries = 3,
 *   retry_delay = 3600,
 * )
 */
class YoutubeAudioExtractRequest extends JobTypeBase {

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    // Load the session.
    $payload = $job->getPayload();
    if (!isset($payload['node_id'])) {
      return JobResult::failure("Missing session id");
    }
    $session = Node::load($payload['node_id']);
    if (!$session) {
      return JobResult::failure("Could not load session " . $payload['node_id']);
    }

    // Consider the existing session media.
    foreach ($session->get('field_media') as $mediaItem) {
      $media = $mediaItem->entity;
      if (!empty($media) && $media->bundle() === 'youtube') {
        $youtubeMedia = $media;
      }
      // If there is an audio, derived or not, don't derive a new one.
      if (!empty($media) && $media->bundle() === 'audio') {
        return JobResult::success("An audio already existed for session " . $payload['node_id']);
      }
    }
    // If the session has no youtube media, nothing to do.
    if (empty($youtubeMedia)) {
      return JobResult::failure("No youtube media existed for session " . $payload['node_id']);
    }

    $youtube_url = $youtubeMedia->getSource()->getSourceFieldValue($youtubeMedia);
    if (!$youtube_url) {
      return JobResult::failure('No url on youtube media ' . $youtubeMedia->id());
    }

    // All necessary conditions to extract audio are satisifed.
    // No audio media exists, so create one.
    $audioMedia = Media::create([
      'bundle' => 'audio',
      'status' => FALSE,
      'field_derived' => $youtubeMedia->id(),
    ]);
    $audioMedia->save();
    $session->get('field_media')->appendItem($audioMedia);
    $session->save();

    // Prepare the date.
    $date_value = $session->get('field_datetime')->date;
    $date_string = $date_value ? $date_value->format('Y-m-d') : '';

    // Prepare the leader or leaders.
    $leaders = [];
    foreach($session->get('field_leader')->referencedEntities() as $leader_entity) {
      $leaders[] = $leader_entity->getDisplayName();
    }
    $preparer = new SmartListPreparer();
    $leader = $preparer->implode($leaders);

    // Post the remote request.
    $workflow_url = Settings::get('ahs_workflow_youtube_audio_request_url');
    if (!$workflow_url) {
      return JobResult::failure('Missing ahs_workflow_youtube_audio_request_url setting.');
    }
    try {
      $client = \Drupal::service('http_client');
      // $client->post($workflow_url, [
      //   RequestOptions::JSON => [
      //     'youtube_url' => $youtube_url,
      //     'audio_uuid' => $audioMedia->uuid(),
      //     'leader' => $leader,
      //     'date' => $date_string,
      //     'title' => $session->getTitle(),
      //     'group' => $session->getPrimaryGroup()?->label() ?? '',
      //   ],
      // ]);

      // Rate-limit calls to the pipedream API.
      // Sleeps for 0.2 seconds.
      // @see https://pipedream.com/docs/limits#qps-queries-per-second
      usleep(200000);
      return JobResult::success();

    } catch (RequestException $e) {
      \Drupal::logger('ahs_archives')->error('Request to ' . $workflow_url . ' failed: ' . $e->getMessage());
      $audioMedia->delete();
      return JobResult::failure('Request failed: ' . $e->getMessage());
    }
  }

}
