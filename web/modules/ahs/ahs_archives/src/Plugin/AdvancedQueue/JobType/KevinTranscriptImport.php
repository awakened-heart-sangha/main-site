<?php

namespace Drupal\ahs_archives\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A one time job to load transcripts from Kevin for all media.
 *
 * @AdvancedQueueJobType(
 *   id = "ahs_kevin_transcript_import",
 *   label = @Translation("Send transcription request"),
 * )
 */
class KevinTranscriptImport extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The transcript dispatcher.
   *
   * @var \Drupal\ahs_archives\TranscriptDispatcher
   */
  protected $transcriptDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // The base class has no create() so can't use parent::create().
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->logger = $container->get('logger.factory')->get('ahs_archives');
    $instance->transcriptDispatcher = $container->get('ahs_archives.transcript_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    $media_id = $job->getPayload()['media_id'];
    /** @var \Drupal\user\UserInterface $user */
    $media = $this->entityTypeManager->getStorage('media')->load($media_id);
    if (!$media) {
      return JobResult::failure("Media $media_id not found.");
    }

    $media_uuid = $media->uuid();
    $api_key = getenv('KEVIN_API_KEY');
    $url = "https://ahs-videos.nw.r.appspot.com/api/transcript/$api_key/json/$media_uuid";

    try {
      $client = \Drupal::httpClient();
      $response = $client->get($url);
      $json_payload = (string) $response->getBody();
      $json_payload = str_replace('"segments":[', '"words":[', $json_payload);

      // Decode JSON to ensure it's valid
      $decoded_payload = json_decode($json_payload, TRUE);
      if (json_last_error() !== JSON_ERROR_NONE) {
        throw new \Exception('Invalid JSON received from API');
      }

      // Save the JSON payload to the media entity
      $media->set('field_transcript', $json_payload);
      $media->save();

      $result = TRUE;
    } catch (\Exception $e) {
      if ($e instanceof \GuzzleHttp\Exception\ClientException || $e instanceof \GuzzleHttp\Exception\ServerException) {
        $statusCode = $e->getResponse()->getStatusCode();
        if ($statusCode == 404 || $statusCode == 500) {
          $this->logger->warning('Ignoring @code error for media @id: @error', [
            '@code' => $statusCode,
            '@id' => $media_id,
            '@error' => $e->getMessage(),
          ]);
          return JobResult::success("Transcript request for media $media_id completed with ignored @code error.", ['@code' => $statusCode]);
        }
      }
      $this->logger->error('Error fetching or saving transcript for media @id: @error', [
        '@id' => $media_id,
        '@error' => $e->getMessage(),
      ]);
      return JobResult::failure("Failed to fetch or save transcript: " . $e->getMessage());
    }

    
    if ($result) {
      return JobResult::success("Transcript for media $media_id fetched and saved successfully.");
    }
    return JobResult::failure("Transcript request for media $media_id failed.");
  }

}
