<?php

namespace Drupal\ahs_archives\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the job type for Stripe customer data synchronization.
 *
 * @AdvancedQueueJobType(
 *   id = "ahs_archives_transcription_request",
 *   label = @Translation("Send transcription request"),
 * )
 */
class TranscriptRequest extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The transcript dispatcher.
   *
   * @var \Drupal\ahs_archives\TranscriptDispatcher
   */
  protected $transcriptDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // The base class has no create() so can't use parent::create().
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->logger = $container->get('logger.factory')->get('ahs_archives');
    $instance->transcriptDispatcher = $container->get('ahs_archives.transcript_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    $transcript_id = $job->getPayload()['transcript_id'];
    /** @var \Drupal\user\UserInterface $user */
    $transcript = $this->entityTypeManager->getStorage('user')->load($transcript_id);
    $result = $this->transcriptDispatcher->dispatch($transcript);
    if ($result) {
      return JobResult::success("Transcript $transcript_id requested.");
    }
    return JobResult::failure("Transcript request $transcript_id failed.");
  }

}
