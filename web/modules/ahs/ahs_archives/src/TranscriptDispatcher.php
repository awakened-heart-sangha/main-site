<?php

namespace Drupal\ahs_archives;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\node\NodeInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

/**
 * Class TranscriptDispatcher. Helper to dispatch transcript.
 */
class TranscriptDispatcher {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The ahs_archives logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TranscriptDispatcher object.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, FileUrlGeneratorInterface $file_url_generator, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('ahs_archives');
    $this->fileUrlGenerator = $file_url_generator;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Dispatch a transcript request.
   *
   * @param \Drupal\node\NodeInterface $transcript
   *   The empty transcript node to transcribe.
   *
   * @return bool
   *   Whether the transcription request was dispatched successfully.
   */
  public function dispatch(NodeInterface $transcript): bool {
    $media = $transcript->get('field_recording')->entity;
    $value = $media->getSource()->getSourceFieldValue($media);
    if ($media->bundle() === 'audio') {
      $file = $this->entityTypeManager->getStorage('file')->load($value);
      $uri = $file->getFileUri();
      $value = $this->fileUrlGenerator->generateAbsoluteString($uri);
    }

    try {
      $workflow_url = Settings::get('ahs_workflow_transcript_request_url');
      if ($workflow_url) {
        $this->httpClient->post($workflow_url, [
          RequestOptions::JSON => [
            'url' => $value,
            'uuid' => $transcript->uuid(),
          ],
        ]);
        $this->logger->notice('Dispatch transcript ' . $transcript->id() . ' with recording ' . $value);
        return TRUE;
      }
      $this->logger->warning('Missing ahs_workflow_transcript_request_url setting.');
      return FALSE;
    }
    catch (RequestException $e) {
      $this->logger->warning('Error calling ' . $workflow_url . ' to dispatch transcript ' . $transcript->id());
      return FALSE;
    }
  }

}
