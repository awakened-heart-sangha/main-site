<?php

namespace Drupal\ahs_archives\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * A computed field item list for a session's primary group.
 */
class PrimaryGroupItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  public function computeValue() {
    // No value will exist if the entity has not been created, so exit early.
    if ($this->getEntity()->isNew()) {
      return NULL;
    }

    // @see \Drupal\ahs_archives\Entity\NodeSession::getPrimaryGroup().
    $group = $this->getEntity()->getPrimaryGroup();

    $this->list = [];
    if ($group) {
      $this->list[0] = $this->createItem(0, [
        'target_id' => $group->id(),
      ]);
      ;
    }
  }

}
