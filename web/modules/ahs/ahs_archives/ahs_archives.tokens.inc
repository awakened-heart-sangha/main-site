<?php

/**
 * @file
 * Contains ahs_archives.tokens.inc.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_token_info().
 */
function ahs_archives_token_info() {
  $info['tokens']['media']['derived'] = [
    'name' => t("Derived"),
    'description' => t("Either 'derived/' or an empty string, depending on whether media is original or derived."),
  ];
  $info['tokens']['media']['year'] = [
    'name' => t("Year"),
    'description' => t("Year, extracted from start of media name"),
  ];
  $info['tokens']['media']['raw_name'] = [
    'name' => t("Name (raw)"),
    'description' => t("Name, without special characters escaped"),
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function ahs_archives_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'media') {
    foreach ($tokens as $name => $original) {
      // Find the desired token by name.
      switch ($name) {
        case 'derived':
          $derived = $data['media']->hasField('field_derived') && !$data['media']->get('field_derived')->isEmpty();
          $replacements[$original] = $derived ? 'derived/' : '';
          break;

        case 'year':
          $name = $data['media']->getName();
          $start = substr($name, 0, 4);
          $year = is_numeric($start) ? $start : 'miscellaneous';
          $replacements[$original] = $year;
          break;

        case 'raw_name':
          $name = $data['media']->getName();
          $replacements[$original] = Markup::create($name);
          break;
      }
    }
  }
  return $replacements;
}
