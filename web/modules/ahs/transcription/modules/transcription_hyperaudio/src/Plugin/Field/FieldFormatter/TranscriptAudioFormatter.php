<?php

namespace Drupal\transcription_hyperaudio\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\file\FileInterface;

/**
 * Plugin implementation of the 'transcript_audio' formatter.
 *
 * @FieldFormatter(
 *   id = "transcription_hyperaudio_audio",
 *   label = @Translation("Transcript Audio Formatter"),
 *   field_types = {
 *     "transcript"
 *   }
 * )
 */
class TranscriptAudioFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => TRUE,
      'autoplay' => FALSE,
      'loop' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'controls' => [
        '#title' => $this->t('Show playback controls'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('controls'),
      ],
      'autoplay' => [
        '#title' => $this->t('Autoplay'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('autoplay'),
      ],
      'loop' => [
        '#title' => $this->t('Loop'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('loop'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Playback controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('visible') : $this->t('hidden')]);
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('yes') : $this->t('no')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\media\MediaInterface $media */
    $media = $items->getParent()->getValue();
    // Get source field.
    $name = $media->getSource()->getSourceFieldDefinition($media->bundle->entity)->getName();
    $file = $media->get($name)->entity;

    $files = [];
    if ($file instanceof FileInterface) {
      $source_attributes = new Attribute();
      $source_attributes
        ->setAttribute('src', $file->createFileUrl())
        ->setAttribute('type', $file->getMimeType());
      $files[] = [
        'file' => $file,
        'source_attributes' => $source_attributes,
      ];
    }

    $attributes = $this->prepareAttributes();
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'transcription_audio',
        '#transcript' => Json::decode($item->value),
        '#files' => $files,
        '#attributes' => $attributes,
      ];
    }

    return $elements;
  }

  /**
   * Prepare the attributes according to the settings.
   *
   * @return \Drupal\Core\Template\Attribute
   *   Container with all the attributes for the HTML tag.
   */
  protected function prepareAttributes() {
    $attributes = new Attribute();

    foreach (['controls', 'autoplay', 'loop'] as $attribute) {
      if ($this->getSetting($attribute)) {
        $attributes->setAttribute($attribute, $attribute);
      }
    }

    return $attributes;
  }

}
