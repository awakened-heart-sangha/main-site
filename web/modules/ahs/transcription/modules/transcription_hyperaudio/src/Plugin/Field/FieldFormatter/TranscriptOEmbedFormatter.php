<?php

namespace Drupal\transcription_hyperaudio\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media\IFrameUrlHelper;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\OEmbed\ResourceFetcherInterface;
use Drupal\media\OEmbed\UrlResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'transcription_hyperaudio_oembed' formatter.
 *
 * @FieldFormatter(
 *   id = "transcription_hyperaudio_oembed",
 *   label = @Translation("Transcript OEmbed Formatter"),
 *   field_types = {
 *     "transcript"
 *   }
 * )
 */
class TranscriptOEmbedFormatter extends FormatterBase {

  /**
   * The oEmbed resource fetcher.
   *
   * @var \Drupal\media\OEmbed\ResourceFetcherInterface
   */
  protected $resourceFetcher;

  /**
   * The oEmbed URL resolver service.
   *
   * @var \Drupal\media\OEmbed\UrlResolverInterface
   */
  protected $urlResolver;

  /**
   * The iFrame URL helper service.
   *
   * @var \Drupal\media\IFrameUrlHelper
   */
  protected $iFrameUrlHelper;

  /**
   * Constructs an TranscriptFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin ID for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\media\OEmbed\ResourceFetcherInterface $resource_fetcher
   *   The oEmbed resource fetcher service.
   * @param \Drupal\media\OEmbed\UrlResolverInterface $url_resolver
   *   The oEmbed URL resolver service.
   * @param \Drupal\media\IFrameUrlHelper $iframe_url_helper
   *   The iFrame URL helper service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ResourceFetcherInterface $resource_fetcher, UrlResolverInterface $url_resolver, IFrameUrlHelper $iframe_url_helper) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->resourceFetcher = $resource_fetcher;
    $this->urlResolver = $url_resolver;
    $this->iFrameUrlHelper = $iframe_url_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('media.oembed.resource_fetcher'),
      $container->get('media.oembed.url_resolver'),
      $container->get('media.oembed.iframe_url_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => 0,
      'height' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state) + [
      'width' => [
        '#type' => 'number',
        '#title' => $this->t('Width'),
        '#default_value' => $this->getSetting('width'),
        '#size' => 5,
        '#maxlength' => 5,
        '#field_suffix' => $this->t('pixels'),
        '#min' => 0,
      ],
      'height' => [
        '#type' => 'number',
        '#title' => $this->t('Height'),
        '#default_value' => $this->getSetting('height'),
        '#size' => 5,
        '#maxlength' => 5,
        '#field_suffix' => $this->t('pixels'),
        '#min' => 0,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting('width') && $this->getSetting('height')) {
      $summary[] = $this->t('Maximum size: %width x %height pixels', [
        '%width' => $this->getSetting('width'),
        '%height' => $this->getSetting('height'),
      ]);
    }
    elseif ($this->getSetting('width')) {
      $summary[] = $this->t('Maximum width: %width pixels', [
        '%width' => $this->getSetting('width'),
      ]);
    }
    elseif ($this->getSetting('height')) {
      $summary[] = $this->t('Maximum height: %height pixels', [
        '%height' => $this->getSetting('height'),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $width = $this->getSetting('width');
    $height = $this->getSetting('height');

    /** @var \Drupal\media\MediaInterface $media */
    $media = $items->getParent()->getValue();
    // Get source field.
    $name = $media->getSource()->getSourceFieldDefinition($media->bundle->entity)->getName();
    $path = $media->get($name)->getString();

    // Check if it's a Youtube link to use the default embed iframe.
    preg_match('/^https?:\/\/(www\.)?((?!.*list=)youtube\.com\/watch\?.*v=|youtu\.be\/)(?<id>[0-9A-Za-z_-]*)/', $path, $matches);

    if (isset($matches['id'])) {
      $url = "https://www.youtube.com/embed/{$matches['id']}?enablejsapi=1";
    }
    else {
      $url = Url::fromRoute('media.oembed_iframe', [], [
        'absolute' => TRUE,
        'query' => [
          'url' => $path,
          'max_width' => $width,
          'max_height' => $height,
          'hash' => $this->iFrameUrlHelper->getHash($path, $width, $height),
        ],
      ])->toString();
    }

    $resource = NULL;
    try {
      $resource_url = $this->urlResolver->getResourceUrl($path, $width, $height);
      $resource = $this->resourceFetcher->fetchResource($resource_url);
    }
    catch (ResourceException $exception) {
    }

    foreach ($items as $delta => $item) {
      $elements[] = [
        '#theme' => 'transcription_oembed',
        '#transcript' => Json::decode($item->value),
        '#url' => $url,
        '#width' => $resource && $resource->getWidth() ? $resource->getWidth() : $width,
        '#height' =>  $resource && $resource->getHeight() ? $resource->getHeight() : $height,
        '#attached' => [
          'library' => [
            'media/oembed.formatter',
          ],
        ],
      ];
    }

    return $elements;
  }

}
