<?php

namespace Drupal\transcription\Plugin\search_api\processor;

use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Item\ItemInterface;
use Drupal\transcription\Plugin\Field\FieldType\TranscriptFieldInterface;

/**
 * @SearchApiProcessor(
 *   id = "transcription_transcript_field",
 *   label = @Translation("Transcript field processor"),
 *   description = @Translation("Processes transcript fields for indexing."),
 *   stages = {
 *     "preprocess_index" = 0,
 *   },
 * )
 */
class TranscriptFieldProcessor extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function preprocessIndexItems(array $items) {
    foreach ($items as $item) {
      $this->processItemFields($item);
    }
  }

  /**
   * Process the fields of a single item.
   */
  protected function processItemFields(ItemInterface $item) {
    $fields = $item->getFields();
    $entity = $item->getOriginalObject()?->getValue();

    foreach ($fields as $field) {
      $field_name = $field->getPropertyPath();
      if ($entity && $entity->hasField($field_name)) {
        $field_definition = $entity->getFieldDefinition($field_name);
        $field_type = $field_definition->getFieldStorageDefinition()->getType();
        $field_type_definition = \Drupal::service('plugin.manager.field.field_type')->getDefinition($field_type);
        
        if (isset($field_type_definition['class']) && is_subclass_of($field_type_definition['class'], TranscriptFieldInterface::class)) {
          $values = [];
          foreach($entity->get($field_name) as $item) {
            $values[] = $item->getTranscript()->getText();            
          }
          $field->setValues($values ?? ['']);
        }
      }
    }
  }
}