<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\ParagraphsInterface;

/**
 * Represents an ordered list of transcription paragraphs.
 *
 * @DataType(
 *   id = "transcription_paragraphs_list",
 *   label = @Translation("Transcription paragraphs list"),
 *   definition_class = "\Drupal\Core\TypedData\ListDataDefinition"
 * )
 */
class ParagraphsList extends TranscriptionItemList implements ParagraphsInterface {

  /**
   * {@inheritdoc}
   */
  public function getText($separator = NULL) {
    $separator = $separator ?? "\n\n";
    return parent::getText($separator);
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphs() {
    return $this->getListElements('transcription_paragraph', ParagraphsInterface::class, 'getParagraphs');
  }

}
