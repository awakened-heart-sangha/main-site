<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

abstract class TranscriptionItemBase extends Map implements TranscriptionItemInterface {}
