<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\SectionsInterface;
use Drupal\transcription\Transcription\TranscriptInterface;

/**
 * Transcription transcript type.
 *
 * @DataType(
 *   id = "transcription_transcript",
 *   label = @Translation("Transcript"),
 *   definition_class = "\Drupal\transcription\TypedData\Definition\TranscriptDefinition",
 *   list_class = "\Drupal\transcription\Plugin\DataType\SectionsList"
 * )
 */
class Transcript extends Section implements TranscriptInterface {}
