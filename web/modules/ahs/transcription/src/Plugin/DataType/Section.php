<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\ParagraphsInterface;
use Drupal\transcription\Transcription\SectionInterface;
use Drupal\transcription\Transcription\SectionsInterface;

/**
 * Transcription Section type.
 *
 * @DataType(
 *   id = "transcription_section",
 *   label = @Translation("Section"),
 *   definition_class = "\Drupal\transcription\TypedData\Definition\SectionDefinition",
 *   list_class = "\Drupal\transcription\Plugin\DataType\SectionsList"
 * )
 */
class Section extends Paragraph implements SectionInterface {

  /**
   * {@inheritdoc}
   */  
  public function getParagraphs() {
    return $this->getTranscriptionData(ParagraphsInterface::class, 'getParagraphs');
  }

  /**
   * {@inheritdoc}
   */  
  public function getSections() {
    return $this->getTranscriptionData(SectionsInterface::class, 'getSections');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->getString();
  }

}
