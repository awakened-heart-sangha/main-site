<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\ParagraphInterface;
use Drupal\transcription\Transcription\SentencesInterface;

/**
 * Transcription paragraph type.
 *
 * @DataType(
 *   id = "transcription_paragraph",
 *   label = @Translation("Paragraph"),
 *   definition_class = "\Drupal\transcription\TypedData\Definition\ParagraphDefinition",
 *   list_class = "\Drupal\transcription\Plugin\DataType\ParagraphsList"
 * )
 */
class Paragraph extends Sentence implements ParagraphInterface {

  /**
   * {@inheritdoc}
   */  
  public function getSentences() {
    return $this->getTranscriptionData(SentencesInterface::class, 'getSentences');
  }

}
