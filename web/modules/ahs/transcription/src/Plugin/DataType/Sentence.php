<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\SegmentsInterface;
use Drupal\transcription\Transcription\SentenceInterface;

/**
 * Transcription sentence type.
 *
 * @DataType(
 *   id = "transcription_sentence",
 *   label = @Translation("Sentence"),
 *   definition_class = "\Drupal\transcription\TypedData\Definition\SentenceDefinition",
 *   list_class = "\Drupal\transcription\Plugin\DataType\TranscriptionItemList"
 * )
 */
class Sentence extends Segment implements SentenceInterface { 

  /**
   * {@inheritdoc}
   */  
  public function getSegments() {
    return $this->getTranscriptionData(SegmentsInterface::class, 'getSegments');
  }

}
