<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\SectionsInterface;

/**
 * Represents an ordered list of transcription paragraphs.
 *
 * @DataType(
 *   id = "transcription_paragraphs_list",
 *   label = @Translation("Transcription paragraphs list"),
 *   definition_class = "\Drupal\Core\TypedData\ListDataDefinition"
 * )
 */
class SectionsList extends ParagraphsList implements SectionsInterface {

  /**
   * {@inheritdoc}
   */
  public function getSections() {
    return $this->getListElements('transcription_section', SectionsInterface::class, 'getSections');
  }

}
