<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\SegmentInterface;
use Drupal\transcription\Transcription\TextInterface;
use Drupal\transcription\Transcription\WordsInterface;

/**
 * Transcription segment type.
 *
 * @DataType(
 *   id = "transcription_segment",
 *   label = @Translation("Segment"),
 *   definition_class = "\Drupal\transcription\TypedData\Definition\SegmentDefinition",
 *   list_class = "\Drupal\transcription\Plugin\DataType\SegmentsList"
 * )
 */
class Segment extends Word implements SegmentInterface {

  /**
   * {@inheritdoc}
   */  
  public function getText() {
    /** @var \Drupal\transcription\Transcription\TranscriptionComplexItemInterface|\Drupal\transcription\Transcription\TranscriptionItemList */
    $text = $this->get('text')->getValue();
    if (empty($text)) {
      $text = $this->getTranscriptionData(TextInterface::class, 'getText');
    }
    return $text;
  }

  /**
   * {@inheritdoc}
   */  
  public function getWords() {
    return $this->getTranscriptionData(WordsInterface::class, 'getWords');
  }

}
