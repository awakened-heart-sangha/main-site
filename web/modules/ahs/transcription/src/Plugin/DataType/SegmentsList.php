<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\SegmentsInterface;

/**
 * Represents an ordered list of transcription segments.
 *
 * @DataType(
 *   id = "transcription_segments_list",
 *   label = @Translation("Transcription segments list"),
 *   definition_class = "\Drupal\Core\TypedData\ListDataDefinition"
 * )
 */
class SegmentsList extends TranscriptionItemList implements SegmentsInterface {

  /**
   * {@inheritdoc}
   */
  public function getSegments() {
    return $this->getListElements('transcription_segment', SegmentsInterface::class, 'getSegments');
  }

}
