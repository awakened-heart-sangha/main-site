<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\TextInterface;
use Drupal\transcription\Transcription\WordInterface;

/**
 * Transcription word type.
 *
 * @DataType(
 *   id = "transcription_word",
 *   label = @Translation("Word"),
 *   definition_class = "\Drupal\transcription\TypedData\Definition\WordDefinition",
 *   list_class = "\Drupal\transcription\Plugin\DataType\TranscriptionItemList"
 * )
 */
class Word extends TranscriptionComplexItemBase implements WordInterface {

  /**
   * {@inheritdoc}
   */  
  public function getText() {
    return trim($this->get('text')->getString());
  }

}
