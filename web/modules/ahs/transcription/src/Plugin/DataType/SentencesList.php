<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\transcription\Transcription\SentencesInterface;

/**
 * Represents an ordered list of transcription sentences.
 *
 * @DataType(
 *   id = "transcription_sentences_list",
 *   label = @Translation("Transcription sentences list"),
 *   definition_class = "\Drupal\Core\TypedData\ListDataDefinition"
 * )
 */
class SentencesList extends TranscriptionItemList implements SentencesInterface {

  /**
   * {@inheritdoc}
   */
  public function getSentences() {
    return $this->getListElements('transcription_sentence', SentencesInterface::class, 'getSentences');
  }

}
