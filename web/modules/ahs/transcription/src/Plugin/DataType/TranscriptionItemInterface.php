<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\Core\TypedData\ComplexDataInterface;

/**
 * Represents a basic Transcription data type.
 *
 * @see \Drupal\transcription\Plugin\DataType\TranscriptionItemBase
 */
interface TranscriptionItemInterface extends ComplexDataInterface {

  /**
   * Present the typed data as a render array for rendering.
   *
   * @param array $options
   *   (optional) An array of options. Defaults to an empty array.
   *
   * @return array
   *   A render array.
   */
  public function render($options = []);

}
