<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\transcription\Transcription\TextInterface;
use Drupal\transcription\Transcription\WordsInterface;

/**
 * Represents an ordered list of transcription items.
 *
 * @DataType(
 *   id = "transcription_list",
 *   label = @Translation("Transcription list"),
 *   definition_class = "\Drupal\Core\TypedData\ListDataDefinition"
 * )
 */
class TranscriptionItemList extends ItemList implements TextInterface, WordsInterface {

  /**
   * {@inheritdoc}
   */  
  public function getText($separator = ' ') {
    $strings = [];
    /** @var \Drupal\transcription\Transcription\TranscriptionComplexItemInterface $item */
    foreach ($this->list as $item) {
      $strings[] = $item->getText();
    }
    // Remove any empty strings resulting from empty items.
    return implode($separator, array_filter($strings, 'mb_strlen'));
  }

  /**
   * {@inheritdoc}
   */
  public function getWords() {
    return $this->getListElements('transcription_word', WordsInterface::class, 'getWords');
  }

    /**
   * {@inheritdoc}
   */
  protected function getListElements($data_type, $interface, $method) {
    if ($this->getItemDefinition()->getDataType() === $data_type) {
      return $this->getValue();
    }
    $sections = [];
    foreach ($this->list as $item) {
      if ($item instanceof $interface) {
        $sections[] = $item->{$method}();
      }
    }
    return $sections;
  }

}
