<?php

namespace Drupal\transcription\Plugin\DataType;

use Drupal\Component\Utility\Html;
use Drupal\transcription\Transcription\TextInterface;

/**
 * Transcription base type for all complex transcription types to inherit.
 */
abstract class TranscriptionComplexItemBase extends TranscriptionItemBase implements TranscriptionComplexItemInterface, TextInterface {

  /**
   * The label for a list of items.
   *
   * @var string
   */
  static public $label_list;

  /**
   * The names of the properties holding the transcription units,
   * in order of decreasing granularity.
   *
   * @var array
   */
  static public $units = [
    'text',
    'words',
    'segments',
    'sentences',
    'paragraphs',
    'sections',
  ];

  /**
   * {@inheritdoc}
   */
  public static function getListLabel() {
    return static::$label_list;
  }

  /**
   * {@inheritdoc}
   */
  public function render($options = []) {
    $type = Html::getClass(substr($this->getPluginId(), strlen('transcription_')));
    return [
      '#theme' => $this->getPluginId(),
      '#' . $type => $this,
      '#options' => $options,
      '#attributes' => [
        'class' => ['transcription-unit', 'transcription-unit--' . $type],
      ],
    ];
  }

  /**
   * Get transcription data from the most granular unit possible.
   *
   * @param string $interface
   *   The interface that the data must implement.
   * @param string $method
   *   The method to call on the item or list of items to get the data.
   *
   * @return \Drupal\transcription\Transcription\TranscriptionUnitInterface|string|null
   *   The transcription data, or NULL if no data is found.
   */
  protected function getTranscriptionData($interface, $method) {
    $data = NULL;
    foreach(static::$units as $unit) {
      if (isset($this->definition->getPropertyDefinitions()[$unit])) {
        /** @var \Drupal\transcription\Transcription\TranscriptionComplexItemInterface|\Drupal\transcription\Transcription\TranscriptionItemList */
        $value = $this->get($unit);
        if ($value instanceof $interface) {
          $data = $value->{$method}();
          if (!empty($data)) {
            break;
          }
        }
      }
    }
    return $data;
  }

}
