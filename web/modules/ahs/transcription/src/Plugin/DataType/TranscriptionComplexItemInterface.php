<?php

namespace Drupal\transcription\Plugin\DataType;

/**
 * Describes methods useful for Xero API integration on complex data types.
 *
 * Complex data types refers to a top-level data type accessible from the API.
 *
 * @see \Drupal\xero\Plugin\DataType\XeroComplexItemBase
 */
interface TranscriptionComplexItemInterface extends TranscriptionItemInterface {}
