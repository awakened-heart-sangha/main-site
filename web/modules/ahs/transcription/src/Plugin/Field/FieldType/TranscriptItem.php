<?php

namespace Drupal\transcription\Plugin\Field\FieldType;

use Drupal\transcription\Plugin\Field\FieldType\TranscriptFieldInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\json_field\Plugin\Field\FieldType\NativeBinaryJsonItem;
use Drupal\transcription\Transcription\TranscriptInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

/**
 * Plugin implementation of the 'transcript' field type.
 *
 * @FieldType(
 *   id = "transcript",
 *   label = @Translation("Transcript"),
 *   description = @Translation("Stores transcription data in a json format."),
 *   category = @Translation("Data"),
 *   default_widget = "json_textarea",
 *   default_formatter = "transcript",
 *   constraints = {"valid_json" = {}}*
 * )
 */
class TranscriptItem extends NativeBinaryJsonItem implements TranscriptFieldInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['edited'] = $schema['columns']['value'];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')->setLabel(t('JSONB trancript, verbatim.'))
      ->setDescription(t('JSONB representation of the verbatim transcript.'));
    $properties['readable'] = DataDefinition::create('string')->setLabel(t('JSONB transcript, edited'))
      ->setDescription(t('JSONB representation of the transcript edited for readability.'));
    $properties['transcript'] = DataDefinition::create('any')
      ->setLabel(t('Transcript'))
      ->setDescription(t('The transcript object, edited if available otherwise verbatim.'))
      ->setComputed(TRUE)
      ->setClass(TranscriptComputed::class)
      ->setSetting('source', 'value');
    $properties['transcript_verbatim'] = DataDefinition::create('any')
      ->setLabel(t('Verbatim transcript'))
      ->setDescription(t('The verbatim transcript object.'))
      ->setComputed(TRUE)
      ->setClass(TranscriptComputed::class)
      ->setSetting('source', 'value');
    $properties['transcript_readable'] = DataDefinition::create('any')
      ->setLabel(t('Readable transcript'))
      ->setDescription(t('The readable transcript object.'))
      ->setComputed(TRUE)
      ->setClass(TranscriptComputed::class)
      ->setSetting('source', 'readable');
    $properties['text'] = DataDefinition::create('string')
      ->setLabel(t('Transcript text'))
      ->setDescription(t('The transcript text, edited if available otherwise verbatim.'))
      ->setComputed(TRUE)
      ->setClass(TranscriptComputed::class)
      ->setSetting('text', TRUE);
    $properties['text_verbatim'] = DataDefinition::create('string')
      ->setLabel(t('Verbatim transcript text'))
      ->setDescription(t('The verbatim transcript text.'))
      ->setComputed(TRUE)
      ->setClass(TranscriptComputed::class)
      ->setSetting('source', 'value')
      ->setSetting('text', TRUE);
    $properties['text_readable'] = DataDefinition::create('string')
      ->setLabel(t('Readable transcript text'))
      ->setDescription(t('The readable transcript text.'))
      ->setComputed(TRUE)
      ->setClass(TranscriptComputed::class)
      ->setSetting('source', 'readable')
      ->setSetting('text', TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function __set($name, $value) {
    // Don't normalize the transcript passed as a typed value,
    // (which the parent method does) but leave it to setValue() to serialize.
    $this->set($name, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Allow setting the transcript as an object.
    if ($values instanceof TypedDataInterface && $values instanceof TranscriptInterface) {
      $values = $this->serializeTranscript($values);
      parent::setValue($values, $notify);
      return;
    }
    if (is_array($values)) {
      foreach ($values as $key => $value) {
        if (in_array($key, ['value', 'edited']) && $value instanceof TypedDataInterface && $value instanceof TranscriptInterface) {
          $values[$key] = $this->serializeTranscript($value);
        }
      }
    }
    parent::setValue($values, $notify);
  }

  /**
   * Serializes a TranscriptInterface object.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $transcript
   *   The transcript object to serialize.
   *
   * @return string
   *   The serialized transcript data.
   */
  protected function serializeTranscript(TypedDataInterface $transcript): string {
    $serializer = \Drupal::service('serializer');
    $context = [
      'plugin_id' => $transcript->getDataDefinition()->getDataType(),
      AbstractObjectNormalizer::SKIP_NULL_VALUES => TRUE,
    ];
    return $serializer->serialize($transcript, 'json', $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getTranscript(): ?TranscriptInterface {
    return $this->getReadableTranscript() ?? $this->getVerbatimTranscript();
  }

  /**
   * {@inheritdoc}
   */
  public function getVerbatimTranscript(): ?TranscriptInterface {
    return $this->transcript;
  }

  /**
   * {@inheritdoc}
   */
  public function getReadableTranscript(): ?TranscriptInterface {
    return $this->transcript_readable;
  }

}
