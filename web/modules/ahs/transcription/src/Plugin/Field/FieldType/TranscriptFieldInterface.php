<?php

namespace Drupal\transcription\Plugin\Field\FieldType;

use Drupal\transcription\Transcription\TranscriptInterface;

/**
 * Interface for transcript field types.
 */
interface TranscriptFieldInterface {

  /**
   * Gets the transcript (readable if available, otherwise verbatim).
   *
   * @return \Drupal\transcription\Transcription\TranscriptInterface|null
   *   The transcript or null if not available.
   */
  public function getTranscript(): ?TranscriptInterface;

  /**
   * Gets the verbatim transcript.
   *
   * @return \Drupal\transcription\Transcription\TranscriptInterface|null
   *   The verbatim transcript or null if not available.
   */
  public function getVerbatimTranscript(): ?TranscriptInterface;

  /**
   * Gets the readable transcript.
   *
   * This is the transcript edited for readability.
   *
   * @return \Drupal\transcription\Transcription\TranscriptInterface|null
   *   The readable transcript or null if not available.
   */
  public function getReadableTranscript(): ?TranscriptInterface;

}
