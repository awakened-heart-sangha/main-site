<?php

namespace Drupal\transcription\Plugin\Field\FieldType;

use Drupal\Core\TypedData\TypedData;
use Drupal\Core\TypedData\TypedDataTrait;

/**
 * A computed property for transcripts on transcript field items.
 */
class TranscriptComputed extends TypedData {

  use TypedDataTrait;

  /**
   * Cached transcript.
   *
   * @var \Drupal\transcription\Transcription\TranscriptInterface|null
   */
  protected $transcript = NULL;

  /**
   * Cached transcript text.
   *
   * @var string|null
   */
  protected $text = NULL;

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if (is_null($this->transcript)) {
      $this->transcript = $this->getTranscript();
    }

    $text = $this->definition->getSetting('text');
    if ($text) {
      $this->text = $this->transcript->getText();
      return $this->text;
    }

    return $this->transcript;
  }

  protected function getTranscript() {
    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $this->getParent();
    $source = $this->definition->getSetting('source');
    $value = NULL;
    if (empty($source)) {
      $readable = $item->readable;
      if (!empty($readable) && is_string($readable)) {
        $value = $readable;
      }
    }
    else {
      $value = $item->{$source};
    }

    if (empty($value) || !is_string($value)) {
      return NULL;
    }
    try {
      $transcript = \Drupal::service('serializer')->deserialize(
        $value,
        $this->getTypedDataManager()->getDefinition('transcription_transcript')['class'],
        'json',
        ['plugin_id' => 'transcription_transcript']
      );
      return $transcript;
    }
    catch (\Exception $e) {
      \Drupal::logger('transcription')->error("Error deserializing json from $source property into transcript typed data: " . $e->getMessage());
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $text = $this->definition->getSetting('text');
    if ($text) {
      $this->text = $value;
    }
    else {
      $this->transcript = $value;
    }
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }

}
