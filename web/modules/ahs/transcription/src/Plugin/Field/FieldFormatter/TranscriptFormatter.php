<?php

namespace Drupal\transcription\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'transcript' formatter.
 *
 * @FieldFormatter(
 *   id = "transcript",
 *   label = @Translation("Transcript Formatter"),
 *   field_types = {
 *     "transcript"
 *   }
 * )
 */
class TranscriptFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $transcript = $item->getTranscript();
      if ($transcript) {
        $elements[$delta] = $transcript->render();
      }
    }
    return $elements;
  }

}
