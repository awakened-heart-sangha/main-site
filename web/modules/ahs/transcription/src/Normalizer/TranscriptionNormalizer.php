<?php

namespace Drupal\transcription\Normalizer;

use Drupal\Core\TypedData\ListDataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\serialization\Normalizer\ComplexDataNormalizer;
use Drupal\transcription\Plugin\DataType\TranscriptionComplexItemInterface;
use Drupal\transcription\TypedData\Definition\TranscriptionDefinitionInterface;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Implement denormalization for Xero complex data.
 */
class TranscriptionNormalizer extends ComplexDataNormalizer implements DenormalizerInterface {

  /**
   * Typed Data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * Initialization method.
   *
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed_data.manager service.
   */
  public function __construct(TypedDataManagerInterface $typed_data_manager) {
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      TranscriptionComplexItemInterface::class => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {
    // The context array requires the Transcription data type to be known. If not, then
    // cannot do anything. This is consistent with Entity normalization.
    if (!isset($context['plugin_id']) || empty($context['plugin_id'])) {
      throw new UnexpectedValueException('Plugin id parameter must be included in context.');
    }

    //$plural_name = $class::getXeroProperty('plural_name');
    //if (isset($data[$plural_name])) {
     // return $this->denormalizeList($data[$plural_name], $class, $format, $context);
    //}

    return $this->denormalizeItem($data, $class, $format, $context);
  }

  /**
   * Denormalizes a list of Xero items.
   *
   * @param mixed $data
   *   The list data.
   * @param string $class
   *   The expected class to instantiate the list items to.
   * @param string $format
   *   Format the given data was extracted from.
   * @param array $context
   *   Options available to the denormalizer.
   *
   * @return object
   *   A typed data list object.
   */
  protected function denormalizeList($data, $class, $format, array $context) {
    $list_definition = $this->typedDataManager->createListDataDefinition($context['plugin_id']);
    /** @var $definition \Drupal\Core\TypedData\ComplexDataDefinitionInterface */
    $definition = $this->typedDataManager->createDataDefinition($context['plugin_id']);
    // Typed Data Manager's createListDataDefinition method is dumb and creates
    // lists of "any" by default so it needs to be set. DrupalWTF.
    $list_definition->setItemDefinition($definition);

    // Create an empty list and then populate each item.
    /** @var $items \Drupal\Core\TypedData\Plugin\DataType\ItemList */
    $items = $this->typedDataManager->create($list_definition, []);
    foreach ($data as $index => $item_data) {
      $item = $this->denormalizeItem($item_data, $class, $format, $context);
      // Set the value, not the Typed Data object. Might have performance
      // concerns about this.
      $items->offsetSet(NULL, $item->getValue());
    }

    return $items;

  }

  /**
   * Denormalizes a transcription item.
   *
   * @param mixed $data
   *   The item data.
   * @param string $class
   *   The expected transcription class to instantiate.
   * @param string $format
   *   Format the given data was extracted from.
   * @param array $context
   *   Options available to the denormalizer.
   *
   * @return \Drupal\xero\TypedData\TranscriptionComplexItemInterface
   *   A transcription unit object.
   */
  protected function denormalizeItem($data, $class, $format, array $context) {
    /** @var $definition \Drupal\Core\TypedData\ComplexDataDefinitionInterface */
    $definition = $this->typedDataManager->createDataDefinition($context['plugin_id']);

    /** @var $item \Drupal\Core\TypedData\ComplexDataInterface */
    $item = $this->typedDataManager->create($definition, []);

    // Go through each property definition.
    foreach ($definition->getPropertyDefinitions() as $prop => $prop_definition) {
      if (isset($data[$prop])) {
        if ($prop_definition instanceof TranscriptionDefinitionInterface) {
          $prop_data = $this->denormalizeItem(
            $data[$prop],
            $prop_definition->getClass(),
            $format,
            ['plugin_id' => $prop_definition->getDataType()]
          );
          $item->set($prop, $prop_data->getValue(), TRUE);
        }
        elseif ($prop_definition instanceof ListDataDefinitionInterface) {
          $prop_data = $this->denormalizeList(
            $data[$prop],
            $prop_definition->getItemDefinition()->getClass(),
            $format,
            ['plugin_id' => $prop_definition->getItemDefinition()->getDataType()]
          );
          $item->set($prop, $prop_data->getValue(), TRUE);
        }
        else {
          // Otherwise set the property directly.
          $value = $data[$prop];
          $item->set($prop, $value, TRUE);
        }
      }
    }

    return $item;
  }

}
