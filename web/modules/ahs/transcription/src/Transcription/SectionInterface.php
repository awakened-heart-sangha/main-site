<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription section unit.
 *
 * @see \Drupal\transcription\Plugin\DataType\Section
 */
interface SectionInterface extends SectionsInterface, ParagraphsInterface, SentencesInterface, SegmentsInterface, WordsInterface, UnitInterface {

  /**
   * Gets the title of the section.
   *
   * @return string|null
   *   The title of the section, or null if no title is set.
   */
  public function getTitle();

}
