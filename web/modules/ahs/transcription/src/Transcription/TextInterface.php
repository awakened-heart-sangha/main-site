<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription unit that can provide its text.
 */
interface TextInterface {

  /**
   * Get the transcribed text.
   *
   * @return string
   *   The text.
   */
  public function getText();

}
