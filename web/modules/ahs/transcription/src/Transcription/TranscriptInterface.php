<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription transcript unit.
 *
 * @see \Drupal\transcription\Plugin\DataType\Transcript
 */
interface TranscriptInterface extends SectionsInterface, ParagraphsInterface, SentencesInterface, SegmentsInterface, WordsInterface, UnitInterface {}
