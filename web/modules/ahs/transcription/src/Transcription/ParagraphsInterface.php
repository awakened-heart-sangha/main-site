<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription unit with paragraphs.
 *
 * @see \Drupal\transcription\Plugin\DataType\Paragraph
 * @see \Drupal\transcription\Transcription\ParagraphInterface
 */
interface ParagraphsInterface {

  /**
   * Get the paragraphs as an array.
   *
   * @return \Drupal\transcription\Plugin\DataType\Paragraph[]
   *   A array of sections.
   */
  public function getParagraphs();

}
