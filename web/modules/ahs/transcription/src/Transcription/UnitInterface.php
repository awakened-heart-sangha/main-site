<?php

namespace Drupal\transcription\Transcription;

/**
 * Represents a basic unit of transcription.
 *
 * @see \Drupal\transcription\Plugin\DataType\TranscriptionItemBase
 */
interface UnitInterface extends TextInterface {}
