<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription word unit.
 *
 * @see \Drupal\transcription\Plugin\DataType\Word
 */
interface WordInterface extends UnitInterface {}
