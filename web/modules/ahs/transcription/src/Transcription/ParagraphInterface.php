<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription paragraph unit.
 *
 * @see \Drupal\transcription\Plugin\DataType\Paragraph
 */
interface ParagraphInterface extends SentencesInterface, SegmentsInterface, WordsInterface, UnitInterface {}
