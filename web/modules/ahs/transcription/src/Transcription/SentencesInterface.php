<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription unit with sentences.
 *
 * @see \Drupal\transcription\Plugin\DataType\Sentence
 * @see \Drupal\transcription\Transcription\SentenceInterface
 */
interface SentencesInterface {

  /**
   * Get the sentences as an array.
   *
   * @return \Drupal\transcription\Plugin\DataType\Sentence[]
   *   A array of sentences.
   */
  public function getSentences();

}
