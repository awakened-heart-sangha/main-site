<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription segment unit.
 *
 * @see \Drupal\transcription\Plugin\DataType\Segment
 */
interface SegmentInterface extends WordsInterface, UnitInterface {}
