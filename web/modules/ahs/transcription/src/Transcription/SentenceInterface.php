<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription sentence unit.
 *
 * @see \Drupal\transcription\Plugin\DataType\Sentence
 */
interface SentenceInterface extends SegmentsInterface, WordsInterface, UnitInterface {}
