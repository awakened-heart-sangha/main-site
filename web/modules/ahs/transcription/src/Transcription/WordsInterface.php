<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription unit with words.
 *
 * @see \Drupal\transcription\Plugin\DataType\Word
 * @see \Drupal\transcription\Transcription\WordInterface
 */
interface WordsInterface {

  /**
   * Get the words as an array.
   *
   * @return \Drupal\transcription\Plugin\DataType\Word[]
   *   A array of words.
   */
  public function getWords();

}
