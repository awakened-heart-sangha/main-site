<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription unit with sections.
 *
 * @see \Drupal\transcription\Plugin\DataType\Section
 * @see \Drupal\transcription\Transcription\SectionInterface
 */
interface SectionsInterface {

  /**
   * Get the sections as an array.
   *
   * @return \Drupal\transcription\Plugin\DataType\Section[]
   *   A array of sections.
   */
  public function getSections();

}
