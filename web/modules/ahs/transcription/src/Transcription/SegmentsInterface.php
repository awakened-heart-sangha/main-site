<?php

namespace Drupal\transcription\Transcription;

/**
 * An interface for a transcription unit with segments.
 *
 * @see \Drupal\transcription\Plugin\DataType\Segment
 * @see \Drupal\transcription\Transcription\SegmentInterface
 */
interface SegmentsInterface {

  /**
   * Get the segments as an array.
   *
   * @return \Drupal\transcription\Plugin\DataType\Segment[]
   *   A array of segments.
   */
  public function getSegments();

}
