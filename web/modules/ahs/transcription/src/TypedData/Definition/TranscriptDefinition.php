<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\DataDefinition;

/**
 * Transcript data definition
 */
class TranscriptDefinition extends SectionDefinition {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = parent::getPropertyDefinitions();
      $info['date'] = DataDefinition::create('timestamp')->setLabel('Date');
      $this->propertyDefinitions = $info;
    }
    return $this->propertyDefinitions;
  }

}
