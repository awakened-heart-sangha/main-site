<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;

/**
 * Transcription section data definition.
 * 
 * A section is a flexible unit that may represent a turn of speech
 * by a speaker in a conversation, or a semantic chapter within a longer
 * speech.
 */
class SectionDefinition extends TranscriptionUnitDefinitionBase {
  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = parent::getPropertyDefinitions();
      $info['title'] = DataDefinition::create('string')->setLabel('Title');
      //$info['summaries'] = ListDataDefinition::create('transcription_summary')->setLabel('Summaries');
      $info['sections'] = ListDataDefinition::create('transcription_section')->setLabel('Sections');
      $info['paragraphs'] = ListDataDefinition::create('transcription_paragraph')->setLabel('Paragraphs');
      $info['sentences'] = ListDataDefinition::create('transcription_sentence')->setLabel('Sentences');
      $info['segments'] = ListDataDefinition::create('transcription_segment')->setLabel('Segments');
      $info['words'] = ListDataDefinition::create('transcription_word')->setLabel('Words');
      $this->propertyDefinitions = $info;
    }
    return $this->propertyDefinitions;
  }
}
