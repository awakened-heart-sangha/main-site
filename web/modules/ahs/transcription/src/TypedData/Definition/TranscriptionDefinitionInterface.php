<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\ComplexDataDefinitionInterface;

interface TranscriptionDefinitionInterface extends ComplexDataDefinitionInterface {}
