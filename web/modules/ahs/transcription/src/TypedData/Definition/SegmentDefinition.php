<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Transcription segment data definition.
 * 
 * Segments are a unit of transcription used by automated transcription
 * systems. They may cut across paragraph or sentence boundaries.
 */
class SegmentDefinition extends TranscriptionUnitDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = parent::getPropertyDefinitions();
      $info['confidence'] = DataDefinition::create('float')->setLabel('Confidence');
      $info['id'] = DataDefinition::create('integer')->setLabel('Id');
      $info['seek'] = DataDefinition::create('integer')->setLabel('Seek offset of segment');
      $info['tokens'] = DataDefinition::create('list')->setLabel('Tokens');
      $info['temperature'] = DataDefinition::create('float')->setLabel('Temperature');
      $info['avg_logprob'] = DataDefinition::create('float')->setLabel('Average log probability');
      $info['compression_ratio'] = DataDefinition::create('float')->setLabel('Compression ratio');
      $info['words'] = ListDataDefinition::create('transcription_word')->setLabel('Words');
      $this->propertyDefinitions = $info;
    }
    return $this->propertyDefinitions;
  }

}
