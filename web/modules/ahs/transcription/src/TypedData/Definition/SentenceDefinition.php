<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;

/**
 * Transcription sentence data definition.
 * 
 * Sentences are a gramatical unit of text.
 */
class SentenceDefinition extends TranscriptionUnitDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = parent::getPropertyDefinitions();
      $info['confidence'] = DataDefinition::create('float')->setLabel('Confidence');
      $info['segments'] = ListDataDefinition::create('transcription_segment')->setLabel('Segments');
      $info['words'] = ListDataDefinition::create('transcription_word')->setLabel('Words');
      $this->propertyDefinitions = $info;
    }
    return $this->propertyDefinitions;
  }

}
