<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\DataDefinition;

/**
 * Transcription word data definition
 */
class WordDefinition extends TranscriptionUnitDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = parent::getPropertyDefinitions();
      $info['confidence'] = DataDefinition::create('float')->setLabel('Confidence');
      $info['word'] = DataDefinition::create('string')->setLabel('Word (without punctuation)');
      $this->propertyDefinitions = $info;
    }
    return $this->propertyDefinitions;
  }

}
