<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\ListDataDefinition;

/**
 * Transcription paragraph data definition.
 * 
 * A paragraph is a semantic unit of text that must not cut across
 * sentence boundaries.
 */
class ParagraphDefinition extends TranscriptionUnitDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = parent::getPropertyDefinitions();
      $info['sentences'] = ListDataDefinition::create('transcription_sentence')->setLabel('Sentences');
      $info['segments'] = ListDataDefinition::create('transcription_segment')->setLabel('Segments');
      $info['words'] = ListDataDefinition::create('transcription_word')->setLabel('Words');
      $this->propertyDefinitions = $info;
    }
    return $this->propertyDefinitions;
  }

}
