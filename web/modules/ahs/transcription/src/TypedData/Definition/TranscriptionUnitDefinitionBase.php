<?php

namespace Drupal\transcription\TypedData\Definition;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ComplexDataDefinitionBase;

/**
 * Base class for a unit of transcription.
 */
class TranscriptionUnitDefinitionBase extends ComplexDataDefinitionBase implements TranscriptionDefinitionInterface {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info['speaker_id'] = DataDefinition::create('string')->setLabel('Speaker ID');
      $info['speaker_label'] = DataDefinition::create('string')->setLabel('Speaker label');
      $info['speaker_confidence'] = DataDefinition::create('float')->setLabel('Speaker confidence');
      $info['start'] = DataDefinition::create('float')->setLabel('Start of audio (in seconds)');
      $info['end'] = DataDefinition::create('float')->setLabel('End of audio (in seconds)');
      $info['language'] = DataDefinition::create('timestamp')->setLabel('Date');
      $info['duration'] = DataDefinition::create('float')->setLabel('Duration of audio (in seconds)');
      $info['text'] = DataDefinition::create('string')->setLabel('Transcribed text');
      $this->propertyDefinitions = $info;
    }
    return $this->propertyDefinitions;
  }

}
