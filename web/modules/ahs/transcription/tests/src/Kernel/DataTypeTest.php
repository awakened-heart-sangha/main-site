<?php

namespace Drupal\Tests\transcription\Kernel\Plugin\DataType;

use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests typed data for transcription data types.
 *
 * @group transcription
 */
class DataTypeTest extends KernelTestBase {

  use TypedDataTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['serialization', 'transcription'];

  /**
   * Tests converting normalized array data into typed data.
   *
   * @dataProvider normalizedDataProvider
   */
  public function testNormalizedToTypedData($plugin_id, $data, $expected) {
    $typedDataManager = $this->getTypedDataManager();
    $definition = $typedDataManager->createDataDefinition($plugin_id);
    $typedData = $typedDataManager->create($definition, $data);
    $class = get_class($typedData);
    foreach($expected as $method => $value) {
      $method = 'get' . $method;
      $this->assertEquals($value, $typedData->{$method}(), "Unexpected value for $method on $class");
    }
  }

  /**
   * Data provider for testNormalizedToTypedData.
   */
  public function normalizedDataProvider() {
    return [
      'non transcription data type' => [
        'plugin_id' => 'string',
        'data' => 'Hello World',
        'expected' => [
          'Value' => 'Hello World',
        ],
      ],
      'single word to word data type' => [
        'plugin_id' => 'transcription_word',
        'data' => ['text' => 'Hello!'],
        'expected' => [
          'Value' => ['text' => 'Hello!'],
          'Text' => 'Hello!',
        ],
      ],
      'multiple words to segment data type text' => [
        'plugin_id' => 'transcription_segment',
        'data' => ['text' => 'Hello world!'],
        'expected' => [
          'Value' => ['text' => 'Hello world!'],
          'Text' => 'Hello world!',
        ],
      ],
      'multiple words to segment data type words' => [
        'plugin_id' => 'transcription_segment',
        'data' => ['words' => [
          ['text' => 'Hello'],
          ['text' => 'world!'],
        ]],
        'expected' => [
          'Value' => ['words' => [
            ['text' => 'Hello'],
            ['text' => 'world!'],
          ]],
          'Text' => 'Hello world!',
        ],
      ],
      'transcript' => [
        'plugin_id' => 'transcription_transcript',
        'data' => ['sections' => [
          ['sections' => [
            ['paragraphs' => [
              ['text' => 'OK.'],
              ['sentences' => [
                ['segments' => [
                  ['words' => [
                    ['text' => 'Hello'],
                    ['text' => 'world!'],
                  ]],
                ]],
              ]],
            ]],
          ]],
        ]],
        'expected' => [
          'Text' => "OK.\n\nHello world!",
        ],
      ],
    ];
  }
}