<?php

namespace Drupal\Tests\transcription\Kernel;

use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\transcription\Transcription\TranscriptInterface;

/**
 * Tests the TranscriptItem field type and TranscriptFormatter.
 *
 * @group transcription
 */
class FieldTest extends KernelTestBase {

  protected static $modules = [
    'system',
    'field',
    'text',
    'user',
    'entity_test',
    'json_field',
    'serialization',
    'transcription',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
    $this->installConfig(['field']);

    // Create a TranscriptItem field.
    FieldStorageConfig::create([
      'field_name' => 'field_transcript',
      'entity_type' => 'entity_test',
      'type' => 'transcript',
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_transcript',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'label' => 'Transcript',
    ])->save();

    // After creating the field, set the default formatter
    \Drupal::service('entity_display.repository')
      ->getViewDisplay('entity_test', 'entity_test', 'default')
      ->setComponent('field_transcript', [
        'type' => 'transcript', // Make sure this formatter exists
        'weight' => 1,
      ])
      ->save();
  }

  /**
   * Tests setting a transcript object on the field.
   */
  public function testTranscriptObject() {
    // Create a typed data Transcript object.
    $typedDataManager = $this->container->get('typed_data_manager');
    $definition = $typedDataManager->createDataDefinition('transcription_transcript');
    $transcript = $typedDataManager->create($definition, [
      'speakers' => [
        ['id' => 1, 'name' => 'Speaker 1'],
        ['id' => 2, 'name' => 'Speaker 2'],
      ],
      'segments' => [
        ['speaker' => 1, 'start' => 0, 'end' => 5, 'text' => 'Hello, how are you?'],
        ['speaker' => 2, 'start' => 5, 'end' => 10, 'text' => 'Fine, thank you.'],
      ],
    ]);

    $this->assertInstanceOf(TypedDataInterface::class, $transcript);
    $this->assertInstanceOf(TranscriptInterface::class, $transcript);

    // Create an entity with the transcript field.
    $entity = EntityTest::create([
      'name' => 'Test Entity',
      'field_transcript' => ['value' => $transcript],
    ]);
    $entity->save();

    $this->assertNotNull($entity->get('field_transcript')->value, "Value property should be set.");
    $this->assertNotNull($entity->get('field_transcript')->transcript, "Computed transcript property should be set.");
    $this->assertSame("Hello, how are you? Fine, thank you.", $entity->get('field_transcript')->transcript->getText());
    $this->assertSame($entity->get('field_transcript')->transcript, $entity->get('field_transcript')->first()->getTranscript());
    //$this->assertSame($entity->get('field_transcript')->transcript, $entity->get('field_transcript')->getTranscript());

    // Before rendering, ensure the field is visible in the view mode
    $display = \Drupal::service('entity_display.repository')
      ->getViewDisplay('entity_test', 'entity_test', 'default');
    $this->assertNotEmpty($display->getComponent('field_transcript'), 'Transcript field should be visible in default view mode');

    // Render the entity
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('entity_test');
    $build = $view_builder->view($entity, 'default'); // Specify 'default' view mode
    $output = \Drupal::service('renderer')->renderRoot($build);

    // Assert that the transcript is rendered correctly.
    $this->assertStringContainsString('Hello, how are you?', (string) $output);
    $this->assertStringContainsString('Fine, thank you.', (string) $output);

    // @todo test setting raw json string
  }

  /**
   * Tests setting a json string on the field.
   */
  public function testTranscriptJson() {
    $json = '{"date":"2024-03-12","length":5442.55,"words":[{"end":19.68,"speaker":"Fred Bloggs","start":4.94,"text":"Hello, how are you?"},{"end":27.94,"start":19.68,"text":"Fine, thank you."}]}';
    $this->assertIsArray(json_decode($json, TRUE));

    // Create an entity with the transcript field.
    $entity = EntityTest::create([
      'name' => 'Test Entity',
      'field_transcript' => ['value' => $json],
    ]);
    $entity->save();

    $this->assertNotNull($entity->get('field_transcript')->value, "Value property should be set.");
    $this->assertNotNull($entity->get('field_transcript')->transcript, "Computed transcript property should be set.");
    $this->assertSame("Hello, how are you? Fine, thank you.", $entity->get('field_transcript')->transcript->getText());
    $this->assertSame($entity->get('field_transcript')->transcript, $entity->get('field_transcript')->first()->getTranscript());
    //$this->assertSame($entity->get('field_transcript')->transcript, $entity->get('field_transcript')->getTranscript());

    // Before rendering, ensure the field is visible in the view mode
    $display = \Drupal::service('entity_display.repository')
      ->getViewDisplay('entity_test', 'entity_test', 'default');
    $this->assertNotEmpty($display->getComponent('field_transcript'), 'Transcript field should be visible in default view mode');

    // Render the entity
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('entity_test');
    $build = $view_builder->view($entity, 'default'); // Specify 'default' view mode
    $output = \Drupal::service('renderer')->renderRoot($build);

    // Assert that the transcript is rendered correctly.
    $this->assertStringContainsString('Hello, how are you?', (string) $output);
    $this->assertStringContainsString('Fine, thank you.', (string) $output);

    // @todo test setting raw json string
  }

  
}