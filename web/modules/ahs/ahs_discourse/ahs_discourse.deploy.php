<?php

/**
 * @file
 * Deploy functions for the AHS discourse module.
 */

use Drupal\advancedqueue\Job;
use Drupal\user\Entity\User;

/**
 * Syncs all members with Discourse.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_discourse_deploy_0004_initial_member_sync(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->condition('roles', 'member')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      $queue_storage = \Drupal::entityTypeManager()
        ->getStorage('advancedqueue_queue');
      /** @var \Drupal\advancedqueue\Entity\QueueInterface $recurring_queue */
      $discourseSsoSyncQueue = $queue_storage->load('ahs_discourse_sso_sync');
      $discourseSsoSyncJob = Job::create('ahs_discourse_sso_sync', [
        'user_id' => $user->id(),
      ]);
      $discourseSsoSyncQueue->enqueueJob($discourseSsoSyncJob);
      \Drupal::logger('ahs_discourse')->notice("Queued for initial member sync: " . $id . " " . $user->label());
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_discourse')
        ->error("Error queuing member for discourse sync " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'members queued for discourse sync');
}

/**
 * Resave all members to fix username and sync with Discourse.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_discourse_deploy_0005_resync_rebuild_usernames(&$sandbox) {
  $jobsLoader = function () {
    $query = \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE);
    $or = $query->orConditionGroup()
      ->condition('roles', 'member')
      ->exists('name');
    $query->condition($or);
    return $query->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      $user->save();
      \Drupal::logger('ahs_discourse')->notice("Resaved user: " . $id . " " . $user->label());
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_discourse')
        ->error("Error resaving user for discourse sync " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users saved for discourse sync');
}

/**
 * Implements hook_deploy_NAME().
 */
function ahs_discourse_deploy_0006_owner(&$sandbox) {
  try {
    $user = User::load(1);
    \Drupal::service('ahs_discourse.sso_sync')->syncSso($user, TRUE);
    \Drupal::logger('ahs_discourse')->notice("Owner synced");
  }
  catch (\Throwable $e) {
    \Drupal::logger('ahs_discourse')
      ->error("Error syncing owner: " . $e->getMessage());
  }
}
