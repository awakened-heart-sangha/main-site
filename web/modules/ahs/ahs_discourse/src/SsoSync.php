<?php

namespace Drupal\ahs_discourse;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\discourse_sso\SingleSignOnBase;
use Drupal\user\UserInterface;
use GuzzleHttp\ClientInterface;

/**
 * Provides SSO sync functionality for discourse.
 *
 * @see https://meta.discourse.org/t/sync-discourseconnect-user-data-with-the-sync-sso-route/84398
 */
class SsoSync extends SingleSignOnBase {

  /**
   * Discourse client.
   *
   * @var DiscourseClient
   */
  protected $discourseClient;

  /**
   * Secret value from the config.
   *
   * @var string
   */
  protected $discourseSecret;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * SingleSignOnBase constructor.
   */
  public function __construct(ClientInterface $http_client, ConfigFactory $config_factory, DiscourseClient $discourse_client, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($http_client, $config_factory);
    $config = $config_factory->get('discourse_sso.settings');
    $this->discourseSecret = $config->get('discourse_sso_secret');
    $this->discourseClient = $discourse_client;
    $this->loggerFactory = $logger_factory->get('ahs_discourse');
  }

  /**
   * Sync a Drupal user with a Discourse SSO record.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to sync.
   * @param bool $create
   *   (optional) Whether the synced user should be created if
   *   it does not exist on discourse.
   */
  public function syncSso(UserInterface $user, $create = NULL): void {
    if (is_null($create)) {
      $create = $this->shouldCreate($user);
    }
    if (!$create && empty($this->getSyncedDiscourseUser($user))) {
      return;
    }

    $ssoParams = [];
    $this->buildSsoParameters($ssoParams, $user, $create);
    $sso_payload = base64_encode(http_build_query($ssoParams));
    $sso_sig = hash_hmac('sha256', $sso_payload, $this->discourseSecret);
    $data = [
      'sso' => $sso_payload,
      'sig' => $sso_sig,
    ];
    $this->discourseClient->post('/admin/users/sync_sso', $data);
  }

  /**
   * Helper to check if we need to create a new user.
   */
  protected function shouldCreate(UserInterface $user) {
    if ($user->hasRole('member')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Build an array of SSO parameters to sync a user with discourse.
   *
   * This is called from both ::sync and
   * ahs_discourse_discourse_sso_parameters_alter().
   *
   * @param array $params
   *   An array of SSO params, to modify.
   * @param \Drupal\user\UserInterface $user
   *   The user being synced.
   * @param bool $create
   *   Whether the synced user should be created
   *   if it does not exist on discourse.
   */
  public function buildSsoParameters(array &$params, UserInterface $user = NULL, $create = TRUE) {
    $user = $user ?? \Drupal::entityTypeManager()->getStorage('user')->load($params['external_id']);
    $picture_url = '';
    if ($user->hasField('user_picture') && !$user->get('user_picture')->isEmpty()) {
      $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('user_picture');
      $uri = $user->get('user_picture')->entity->getFileUri();
      $picture_url = $style->buildUrl($uri);
    }

    $params['external_id'] = $user->id();
    $params['name'] = (string) $user->getDisplayName();
    // Remove the picture on Discourse if it has been removed on Drupal.
    $params['avatar_url'] = $picture_url;
    $params['avatar_force_update'] = TRUE;

    $params['username'] = $user->getDiscourseUsername();
    $params['email'] = $user->getEmail();

    // Give AHS members tl1, because the 'members' group on
    // Sanghaspace grants that automatically.
    // These parameters take a comma-seperated string.
    if ($user->hasRole('member')) {
      $params['add_groups'] = 'members';
    }
    else {
      $params['remove_groups'] = 'members';
    }

    $this->loggerFactory->debug("Preparing to sync " . $user->id . " " . $user->label() . ": \n\n" . print_r($params, TRUE));
  }

  /**
   * Helper to get user by id.
   */
  public function getSyncedDiscourseUser(UserInterface $user) {
    $url = '/users/by-external/' . $user->id() . '.json';
    $response = $this->discourseClient->get($url);
    return $response['user'] ?? NULL;
  }

}
