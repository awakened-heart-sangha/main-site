<?php

namespace Drupal\ahs_discourse;

/**
 * Propagate a user logout in Drupal to the associated Discourse forum.
 *
 * Cf. https://meta.discourse.org/t/discourse-sso-logout/28509/21
 */
class Logout extends DiscourseBase {

  /**
   * Helper to logout user.
   */
  public function logout($uid): void {
    if ($discourse_user = $this->getDiscourseUserByExternalId($uid)) {
      $discourse_id = $discourse_user['id'];
      $url = '/admin/users/' . $discourse_id . '/log_out';
      $this->client->post($url);
    }
  }

  /**
   * Helper to get user by id.
   */
  protected function getDiscourseUserByExternalId($uid, $method = 'GET') {
    $url = '/users/by-external/' . $uid . '.json';
    $response = $this->client->get($url);
    return $response['user'] ?? NULL;
  }

}
