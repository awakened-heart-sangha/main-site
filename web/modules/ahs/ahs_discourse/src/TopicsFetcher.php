<?php

namespace Drupal\ahs_discourse;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 * Fetch topics from Discourse.
 */
class TopicsFetcher extends DiscourseBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TopicsFetcher object.
   */
  public function __construct(DiscourseClient $discourse_client, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($discourse_client);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Helper to get topics.
   */
  public function getTopicsByCategory($categoryId, $parameters = []): array {
    $url = '/c/' . $categoryId . '.json?';
    foreach ($parameters as $name => $value) {
      $url .= $name . '=' . urlencode($value) . '&';
    }
    $response = $this->client->get($url);
    $topics = $response['topic_list']['topics'] ?? [];
    $users = $response['users'] ?? [];

    // Key by id.
    $topics = array_column($topics, NULL, 'id');
    $users = array_column($users, NULL, 'id');

    // Store any poster usernames as part of the topic posters data.
    foreach ($topics as &$topic) {
      foreach ($topic['posters'] as &$poster) {
        if (isset($poster['user_id']) && isset($users[$poster['user_id']]['username'])) {
          $poster['username'] = $users[$poster['user_id']]['username'];
        }
      }
    }

    return $topics;
  }

  /**
   * Helper to sync data.
   */
  public function syncHelpWanted() {
    $topics = $this->getTopicsByCategory(7,
      [
        'solved' => 'no',
        'status' => 'open',
      ]);

    $storage = $this->entityTypeManager->getStorage('node');
    $nodes = $storage->loadByProperties(['type' => 'discourse_topic']);

    // Update or delete existing nodes.
    foreach ($nodes as $node) {
      $id = $node->get('field_discourse_id')->value;
      if (isset($topics[$id])) {
        $this->updateNode($node, $topics[$id]);
        unset($topics[$id]);
      }
      else {
        $node->delete();
      }
    }

    // Create new nodes if needed.
    foreach ($topics as $topic) {
      $node = $storage->create(['type' => 'discourse_topic']);
      $this->updateNode($node, $topic);
    }

  }

  /**
   * Helper to update node.
   */
  protected function updateNode(NodeInterface $node, array $topic) {
    // Strip ellipsis  from end of excerpt.
    $ellipsis = '&hellip;';
    if (substr($topic['excerpt'], -strlen($ellipsis)) === $ellipsis) {
      $topic['excerpt'] = substr($topic['excerpt'], 0, -strlen($ellipsis));
    }
    // Only include first para.
    $topic['excerpt'] = strtok($topic['excerpt'], "\n");
    // Discard partial sentences.
    $lastPeriod = strrpos($topic['excerpt'], '. ');
    if ($lastPeriod) {
      $topic['excerpt'] = substr($topic['excerpt'], 0, $lastPeriod + 1);
    }
    // Minimize saving so as to avoid busting caches.
    $needsSave = FALSE;
    if ($node->get('title')->value != $topic['title'] ||
      $node->get('field_discourse_excerpt')->value != $topic['excerpt'] ||
      $node->get('changed')->value != strtotime($topic['last_posted_at'])
    ) {
      $needsSave = TRUE;
    }
    if ($needsSave) {
      $node->set('field_discourse_id', $topic['id']);
      $node->set('title', $topic['title']);
      $node->set('field_discourse_excerpt', $topic['excerpt']);
      $node->set('changed', strtotime($topic['last_posted_at']));
      // Technically date and first poster can be changed
      // on Discourse although unlikely.
      $node->set('created', strtotime($topic['created_at']));
      if (isset($topic['posters'][0])) {
        $uid = $this->getDrupalUserId($topic['posters'][0]);
        $node->set('uid', $uid);
      }
      // @todo tags?
      $node->save();
    }
  }

  /**
   * Helper to get user id.
   */
  protected function getDrupalUserId($poster) {
    // @todo we could store these on the user to minimise the number of requests, even if we didn't have it for everyone.
    // If the username is available for this poster,
    // then look up Drupal user by that.
    if (isset($poster['username'])) {
      $storage = $this->entityTypeManager->getStorage('user');
      $users = $storage->loadByProperties(['name' => $poster['username']]);
      if (!empty($users)) {
        $user = reset($users);
        return $user->id();
      }
    }
    // If no user can be found by username,
    // request the Drupal id from Discourse.
    $url = '/admin/users/' . $poster['user_id'] . '.json';
    $response = $this->client->get($url);
    $externalId = $response['single_sign_on_record']['external_id'] ?? NULL;
    return $externalId;
  }

}
