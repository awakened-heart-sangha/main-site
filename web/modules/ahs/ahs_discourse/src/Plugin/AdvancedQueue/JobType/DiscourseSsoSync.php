<?php

namespace Drupal\ahs_discourse\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the job type for Discourse SSO user data synchronization.
 *
 * @AdvancedQueueJobType(
 *   id = "ahs_discourse_sso_sync",
 *   label = @Translation("Send sso user updates to Discourse"),
 *   allow_duplicates = FALSE
 * )
 */
class DiscourseSsoSync extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Discourse SSO sync service.
   *
   * @var \Drupal\ahs_discourse\SsoSync
   */
  protected $discourseSsoSync;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // The base class has no create() so can't use parent::create().
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->discourseSsoSync = $container->get('ahs_discourse.sso_sync');
    $instance->logger = $container->get('logger.factory')->get('ahs_discourse');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    $userId = $job->getPayload()['user_id'];
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($userId);

    if (empty($user)) {
      $result = JobResult::success($this->t('No user @user found', ['@user' => $userId]));
    }
    else {
      try {
        $create = $job->getPayload()['create'] ?? NULL;
        $this->discourseSsoSync->syncSso($user, $create);
        $result = JobResult::success($this->t('Discourse data updated for user @user', ['@user' => $user->label()]));
      }
      catch (\Throwable $e) {
        $message = $this->t('Error when syncing user @user: @error', [
          '@user' => $user->label(),
          '@error' => $e->getMessage(),
        ]);
        $result = JobResult::failure($message);
        $message = $this->t('Error when syncing user @user: @error \n@trace', [
          '@user' => $user->label(),
          '@error' => $e->getMessage(),
          '@trace' => $e->getTraceAsString(),
        ]);
        $this->logger->error($message);
      }
    }
    return $result;
  }

}
