<?php

namespace Drupal\ahs_discourse;

/**
 * A base class for any Discourse api operation.
 */
class DiscourseBase {

  /**
   * Discourse client.
   *
   * @var \Drupal\ahs_discourse\DiscourseClient
   */
  protected $client;

  /**
   * DiscourseBase constructor.
   *
   * @param \Drupal\ahs_discourse\DiscourseClient $discourse_client
   *   Discourse client.
   */
  public function __construct(DiscourseClient $discourse_client) {
    $this->client = $discourse_client;
  }

}
