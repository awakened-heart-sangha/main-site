<?php

namespace Drupal\ahs_discourse;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Error;
use Drupal\discourse_sso\SingleSignOnBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Provides SSO sync functionality for discourse.
 *
 * @see https://meta.discourse.org/t/sync-discourseconnect-user-data-with-the-sync-sso-route/84398
 */
class DiscourseClient extends SingleSignOnBase {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * SingleSignOnBase constructor.
   */
  public function __construct(ClientInterface $http_client, ConfigFactory $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($http_client, $config_factory);

    $this->loggerFactory = $logger_factory->get('ahs_discourse');
  }

  /**
   * Get data from discourse.
   *
   * @param string $path
   *   The discourse path to query, excluding the domain.
   */
  public function get($path) {
    try {
      $response = $this->doRequest('GET', $this->url . $path, $this->getDefaultParameter());
      if ($response instanceof ResponseInterface && $response->getStatusCode() === 200) {
        $json = json_decode($response->getBody(), TRUE);
        return $json;
      }

      $code = $body = '';
      if ($response instanceof ResponseInterface) {
        $code = $response->getStatusCode();
        $body = $response->getBody();
      }

      $this->loggerFactory->warning($code . " status when getting from discourse on $path. \n\n" . print_r(json_decode($body, TRUE), TRUE));
    }
    catch (GuzzleException $e) {
      $logger = \Drupal::logger('ahs_discourse');
      Error::logException($logger, $e, $e->getMessage());
    }
  }

  /**
   * Put data to discourse.
   *
   * @param string $path
   *   The discourse path to query, excluding the domain.
   * @param array $data
   *   An array of data to put.
   */
  public function put($path, $data = NULL) {
    $this->send('PUT', $path, $data);
  }

  /**
   * Post data to discourse.
   *
   * @param string $path
   *   The discourse path to query, excluding the domain.
   * @param array $data
   *   An array of data to post.
   */
  public function post($path, $data = NULL) {
    $this->send('POST', $path, $data);
  }

  /**
   * Send data from discourse.
   *
   * @param string $method
   *   'PUT' or 'POST'.
   * @param string $path
   *   The discourse path to query, excluding the domain.
   * @param array $data
   *   An array of data to send.
   */
  protected function send($method, $path, $data = NULL) {
    $parameters = [
      'json' => $data,
    ] + $this->getDefaultParameter();
    try {
      $response = $this->doRequest($method, $this->url . $path, $parameters);
      if ($response instanceof ResponseInterface && $response->getStatusCode() === 200) {
        $json = json_decode($response->getBody(), TRUE);
        $this->loggerFactory->debug("Sent SSO data to discourse. Response: \n\n" . print_r($json, TRUE));
        return $json;
      }

      $code = $body = '';
      if ($response instanceof ResponseInterface) {
        $code = $response->getStatusCode();
        $body = $response->getBody();
      }

      $this->loggerFactory->warning($code . " status when sending data to discourse on $path. \n\n" . print_r(json_decode($body, TRUE), TRUE));
    }
    catch (GuzzleException $e) {
      $logger = \Drupal::logger('ahs_discourse');
      Error::logException($logger, $e, $e->getMessage());
    }
  }

  /**
   * Try a request, waiting until rate limits reset.
   */
  protected function doRequest($method, $url, $parameters) {
    // Retry 10 times, sleeping 10 seconds between,
    // until we get something other than a rate limit.
    $response = NULL;
    for ($i = 0; $i < 10; $i++) {
      try {
        $response = $this->client->request($method, $url, $parameters);
        if ($response->getStatusCode() !== 429) {
          return $response;
        }
        sleep(10);
      }
      catch (\Exception $e) {}
    }
    return $response;
  }

  /**
   * Helper to get default URL parameters.
   */
  protected function getDefaultParameter($name = NULL) {
    $parameters = [
      RequestOptions::HEADERS => [
        'Api-Key' => $this->api_key,
        'Api-Username' => $this->api_username,
      ],
      RequestOptions::HTTP_ERRORS => $this->http_errors,
    ];

    return $parameters;
  }

}
