<?php

/**
 * @file
 * Contains ahs_discourse.module.
 */

/**
 * Alter the parameters for the Discourse SSO.
 *
 * @param array $parameters
 *   The parameters to be altered.
 *
 * @see \Drupal\discourse_sso\Controller\DiscourseSsoController::validate
 */

use Drupal\advancedqueue\Job;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Implements hook_discourse_sso_parameters_alter().
 */
function ahs_discourse_discourse_sso_parameters_alter(array &$parameters) {
  Drupal::service('ahs_discourse.sso_sync')->buildSsoParameters($parameters);
}

/**
 * Queue updated users for syncing to discourse .
 *
 * @param \Drupal\user\UserInterface $user
 *   The updated user.
 */
function ahs_discourse_user_update(UserInterface $user) {
  $queue_storage = \Drupal::entityTypeManager()
    ->getStorage('advancedqueue_queue');
  /** @var \Drupal\advancedqueue\Entity\QueueInterface $recurring_queue */
  $discourseSsoSyncQueue = $queue_storage->load('ahs_discourse_sso_sync');
  $discourseSsoSyncJob = Job::create('ahs_discourse_sso_sync', [
    'user_id' => $user->id(),
  ]);
  try {
    $discourseSsoSyncQueue->enqueueJob($discourseSsoSyncJob);
  }
  catch (\Drupal\advancedqueue\Exception\DuplicateJobException $e) {}
}

/**
 * Implements hook_user_logout().
 */
function ahs_discourse_user_logout(AccountInterface $account) {
  $uid = $account->id();
  if ($uid > 0) {
    $service = \Drupal::service('ahs_discourse.logout');
    $service->logout($uid);
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function ahs_discourse_module_implements_alter(&$implementations, $hook) {
  if ($hook === 'user_logout') {
    if (isset($implementations['discourse_sso'])) {
      unset($implementations['discourse_sso']);
    }
  }
}

/**
 * Implements hook_cron().
 */
function ahs_discourse_cron() {
  try {
    \Drupal::service('ahs_discourse.topics_fetcher')->syncHelpWanted();
  }
  catch (\Throwable $e) {
    \Drupal::logger('ahs_discourse')->error('Error executing ahs_discourse cron. ' . $e->getMessage());
  }
}
