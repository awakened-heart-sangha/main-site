<?php

namespace Drupal\ahs_admin\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for 403 page.
 *
 * @Block(
 *   id = "ahs_admin_access_denied",
 *   admin_label = @Translation("AHS admin: Access denied"),
 * )
 */
class AhsAdminAccessDenied extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<div>' . $this->t('You are not authorized to access this page.') . '</div>',
    ];
  }

}
