<?php

namespace Drupal\ahs_admin\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for 404 page.
 *
 * @Block(
 *   id = "ahs_admin_page_not_found",
 *   admin_label = @Translation("AHS admin: Page not found"),
 * )
 */
class AhsAdminPageNotFound extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<div>' . $this->t('The requested page could not be found.') . '</div>',
    ];
  }

}
