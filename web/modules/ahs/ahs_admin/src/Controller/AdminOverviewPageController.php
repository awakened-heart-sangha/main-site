<?php

namespace Drupal\ahs_admin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminOverviewPageController. Controller to render menu.
 *
 * @package Drupal\ahs_admin\Controller
 */
class AdminOverviewPageController extends ControllerBase {

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Returns the menu.link_tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MenuLinkTreeInterface $menu_link_tree) {
    $this->entityTypeManager = $entity_type_manager;
    $this->menuTree = $menu_link_tree;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('menu.link_tree')
    );
  }

  /**
   * Route callback to build the menu.
   */
  public function render() {
    $menu_name = 'main-navigation-authenticated';
    $adminLinkPluginId = 'menu_link_content:a5028a1c-5e4a-4fb3-b0d7-0eac6bd63796';
    $parameters = new MenuTreeParameters();
    $parameters->setRoot($adminLinkPluginId)->excludeRoot()->onlyEnabledLinks();
    $tree = $this->menuTree->load($menu_name, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);
    $build = $this->menuTree->build($tree);
    $build['#theme'] = 'menu';
    return $build;
  }

}
