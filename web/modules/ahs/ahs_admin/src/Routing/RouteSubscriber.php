<?php

namespace Drupal\ahs_admin\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber. Alter routes from other modules.
 *
 * @package Drupal\ahs_admin\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('system.admin')) {
      $route->setDefault('_controller', '\Drupal\ahs_admin\Controller\AdminOverviewPageController::render');
    }
    if ($route = $collection->get('filter.tips_all')) {
      $route->setRequirements(['_access' => 'FALSE']);
    }
    if ($route = $collection->get('filter.tips')) {
      $route->setRequirements(['_access' => 'FALSE']);
    }
  }

}
