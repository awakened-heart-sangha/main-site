<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

use Drupal\advancedqueue\Job;

/**
 * Retry failed Xero sync install jobs.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_accounting_deploy_0001_retry_failed_xero_sync_user_contact_install_jobs(&$sandbox) {
  $database = \Drupal::database();
  $query = $database->update('advancedqueue');
  $query->fields(
    [
      'state' => Job::STATE_QUEUED,
      'num_retries' => 0,
    ]
  );
  $query->condition('state', Job::STATE_FAILURE);
  $query->execute();
}
