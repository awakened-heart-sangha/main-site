<?php

namespace Drupal\ahs_accounting\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\user\UserInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for redirecting to user info on external accounting services.
 */
class UserRemoteController extends ControllerBase {

  /**
   * Redirect to the user on Xero.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account for which a personal contact form should be generated.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   Redirect to Xero
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Exception is thrown when user tries to access a contact form for a
   *   user who does not have an email address configured.
   */
  public function xero(UserInterface $user) {
    $syncField = $user->get('xero_sync');
    if (!$syncField->isEmpty() && $contactID = $syncField->guid) {
      $xeroUrl = static::getXeroContactUrl($contactID);
      return new TrustedRedirectResponse($xeroUrl);
    }
    throw new NotFoundHttpException();
  }

  /**
   * Helper to get Xero URL.
   */
  public static function getXeroContactUrl($contactID) {
    $shortcode = "!sWlHQ";
    $xeroUrl = "https://go.xero.com/organisationlogin/default.aspx?shortcode=$shortcode&redirecturl=/Contacts/View.aspx?contactID=$contactID";
    return $xeroUrl;
  }

  /**
   * Redirect to the user on Stripe.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account for which a personal contact form should be generated.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   Redirect to Xero
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Exception is thrown when user tries to access a contact form for a
   *   user who does not have an email address configured.
   */
  public function stripe(UserInterface $user) {
    $remote_ids = $user->get('commerce_remote_id');
    $remote_id = $remote_ids->getByProvider('shrimala_trust_stripe|live') ?? $remote_ids->getByProvider('shrimala_trust_stripe|test');
    if (!$remote_id) {
      throw new NotFoundHttpException();
    }
    $stripeUrl = "https://dashboard.stripe.com/customers/$remote_id";
    return new TrustedRedirectResponse($stripeUrl);
  }

}
