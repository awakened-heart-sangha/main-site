<?php

namespace Drupal\ahs_accounting\EventSubscriber;

use Drupal\user\UserInterface;
use Drupal\xero_sync\Event\XeroSyncAppropriateEvent;
use Drupal\xero_sync\Event\XeroSyncEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Restrict Xero Contact creation to members.
 *
 * We don't want to create a Xero contact for every user, because we plan to
 * use users for email list subscribers.
 */
class SyncIsAppropriateSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      XeroSyncEvents::SYNC_IS_APPROPRIATE . '.user' => 'createMembers',
    ];
    return $events;
  }

  /**
   * Don't automatically create Xero Contacts for non-members.
   *
   * @param \Drupal\xero_sync\Event\XeroSyncAppropriateEvent $event
   *   The Xero Sync event.
   */
  public function createMembers(XeroSyncAppropriateEvent $event) {
    if ($event->getEntityType() === 'user' && $event->getEntity() instanceof UserInterface) {
      $user = $event->getEntity();
      if (!$user->hasRole('member')) {
        $event->setFlag('create', FALSE);
      }
    }
  }

}
