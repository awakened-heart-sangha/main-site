<?php

namespace Drupal\ahs_accounting\EventSubscriber;

use Drupal\commerce_xero\EventSubscriber\OrderSubscriber as parentSubscriber;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Decorates the Commerce Xero Order subscriber.
 *
 * It uses to prevent processing of recurring invoices.
 *
 * Acts on workflow state event from commerce payment.
 */
class CommerceXeroOrderSubscriber extends parentSubscriber {

  /**
   * Decorated event subscriber.
   *
   * @var \Drupal\commerce_xero\EventSubscriber\OrderSubscriber
   */
  protected $inner;

  /**
   * {@inheritdoc}
   */
  public function __construct(parentSubscriber $commerce_xero_order_subscriber) {
    $this->inner = $commerce_xero_order_subscriber;
  }

  /**
   * Don't post recurring invoices to Xero.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The commerce order transition.
   */
  public function onPaymentReceived(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $event->getEntity();
    $order = $payment->getOrder();
    if ($order && $order->bundle() == 'recurring') {
      return;
    }
    $this->inner->onPaymentReceived($event);
  }

}
