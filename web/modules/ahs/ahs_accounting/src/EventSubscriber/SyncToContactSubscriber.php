<?php

namespace Drupal\ahs_accounting\EventSubscriber;

use Drupal\user\UserInterface;
use Drupal\xero_sync\Event\XeroSyncEvents;
use Drupal\xero_sync\Event\XeroSyncPropertiesEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Specify information that should be set on the Xero contact.
 */
class SyncToContactSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      XeroSyncEvents::SYNC_TO_ITEM_FROM_ENTITY . '.user' => 'specifyContact',
    ];

    return $events;
  }

  /**
   * Specify first and last name on the Xero contact.
   *
   * @param \Drupal\xero_sync\Event\XeroSyncPropertiesEvent $event
   *   The Xero Sync event.
   */
  public function specifyContact(XeroSyncPropertiesEvent $event) {
    // This is just paranoia, as we're subscribing to a user event we
    // should be able to trust there's a user entity.
    $hasUser = $event->getEntityType() === 'user' && ($event->getEntity() instanceof UserInterface);
    $hasContact = $event->getItemType() === 'xero_contact';
    if ($hasUser && $hasContact) {
      $user = $event->getEntity();
      if ($user->hasField('about_profiles') && $profile = $user->get('about_profiles')->entity) {
        if ($nameField = $profile->get('field_name')->get(0)) {
          if ($given = $nameField->given) {
            $event->setItemProperty('FirstName', $given);
          }
          if ($family = $nameField->family) {
            $event->setItemProperty('LastName', $family);
          }
        }
      }
    }
  }

}
