<?php

namespace Drupal\ahs_accounting\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access based on whether the user has a Stripe customer.
 *
 * Used to show a user local task redirecting to the customer on stripe.com.
 */
class StripeCustomerAccessCheck implements AccessInterface {

  /**
   * Checks user has a Stripe customer to access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    if (!$route->hasRequirement('_ahs_accounting_has_stripe_customer')) {
      return AccessResult::neutral();
    }
    $needsStripeCustomer = filter_var($route->getRequirement('_ahs_accounting_has_stripe_customer'), FILTER_VALIDATE_BOOLEAN);
    $user = $route_match->getParameters()->get('user');
    if ($user instanceof UserInterface) {
      $remote_ids = $user->get('commerce_remote_id');
      $remote_id = $remote_ids->getByProvider('shrimala_trust_stripe|live') ?? $remote_ids->getByProvider('shrimala_trust_stripe|test');
      if ($needsStripeCustomer && $remote_id) {
        return AccessResult::allowed()->addCacheableDependency($user);
      }
    }
    return AccessResult::neutral();
  }

}
