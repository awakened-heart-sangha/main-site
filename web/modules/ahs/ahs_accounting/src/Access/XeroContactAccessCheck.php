<?php

namespace Drupal\ahs_accounting\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access based on whether the user has a Xero contact.
 *
 * Used to show a user local task redirecting to the contact on xero.com.
 */
class XeroContactAccessCheck implements AccessInterface {

  /**
   * Checks user has a synced Xero contact to access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    if (!$route->hasRequirement('_ahs_accounting_has_xero_contact')) {
      return AccessResult::neutral();
    }
    $needsXeroContact = filter_var($route->getRequirement('_ahs_accounting_has_xero_contact'), FILTER_VALIDATE_BOOLEAN);
    $user = $route_match->getParameters()->get('user');
    if ($user instanceof UserInterface) {
      $syncField = $user->get('xero_sync');
      if ($needsXeroContact && !$syncField->isEmpty() && $syncField->guid) {
        return AccessResult::allowed()->addCacheableDependency($user);
      }
    }
    return AccessResult::neutral();
  }

}
