<?php

namespace Drupal\ahs_accounting\Plugin\CommerceXero\processor;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\commerce_xero\Plugin\CommerceXero\CommerceXeroProcessorPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sets better dates on the invoice.
 *
 * This is a process plugin, not immediate,
 * to make sure the payment has a completed time.
 *
 * @CommerceXeroProcessor(
 *   id = "ahs_accounting_invoice_dates",
 *   label = @Translation("Set better dates on the Xero invoice"),
 *   types = {},
 *   execution = "process",
 *   required = FALSE
 * )
 */
class AhsInvoiceDates extends CommerceXeroProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Drupal time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The Drupal date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function process(PaymentInterface $payment, ComplexDataInterface &$data, CommerceXeroStrategyInterface $strategy) {
    // Replace the dates with better ones.
    $time = $payment->getCompletedTime() ?? $payment->getAuthorizedTime() ?? $this->time->getCurrentTime();
    $order = $payment->getOrder();
    if ($order->hasField('billing_period')) {
      $billing_period = $order->get('billing_period')->first()->toBillingPeriod();
      $time = $billing_period->getEndDate()->getTimestamp();
    }
    $date = $this->dateFormatter->format($time, 'custom', 'Y-m-d');
    $data->set('Date', $date);
    $data->set('DueDate', $date);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // This should be parent::create(). But the base class has no create().
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->time = $container->get('datetime.time');
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

}
