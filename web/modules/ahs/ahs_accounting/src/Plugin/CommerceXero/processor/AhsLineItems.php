<?php

namespace Drupal\ahs_accounting\Plugin\CommerceXero\processor;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\commerce_xero\Plugin\CommerceXero\CommerceXeroProcessorPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sets line items that have Xero account identified from the product.
 *
 * This is a process plugin, not immediate, because it needs to call
 * the Xero api to get the account code.
 *
 * @CommerceXeroProcessor(
 *   id = "ahs_accounting_line_items",
 *   label = @Translation("Set line items with Xero account identified from product"),
 *   types = {},
 *   execution = "process",
 *   required = FALSE
 * )
 */
class AhsLineItems extends CommerceXeroProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Xero item manager service.
   *
   * @var \Drupal\xero\XeroItemManagerInterface
   */
  protected $itemManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function process(PaymentInterface $payment, ComplexDataInterface &$data, CommerceXeroStrategyInterface $strategy = NULL): bool {
    // Replace the line items with new ones that use a more specific account.
    $order = $payment->getOrder();
    if (is_null($order)) {
      throw new \Exception('Order is not specified on payment ' . $payment->id());
    }
    $replacementLineItems = [];
    foreach ($order->getItems() as $orderItem) {
      $replacementLineItems[] = [
        'Description' => $this->describeOrderItem($orderItem),
        'Quantity' => $orderItem->getQuantity(),
        'UnitAmount' => $orderItem->getUnitPrice()->getNumber(),
        'AccountCode' => $this->getOrderItemAccountCode($orderItem, $strategy),
        // Specify no VAT.By default the invoice type adds VAT based on account.
        'TaxType' => 'NONE',
        'TaxAmount' => 0.00,
      ];
    }
    $data->set('LineItems', $replacementLineItems);
    return TRUE;
  }

  /**
   * Helper to get an account code.
   */
  protected function getOrderItemAccountCode(OrderItemInterface $orderItem, CommerceXeroStrategyInterface $strategy) {
    // Use the configured account code as the default.
    $accountCode = $strategy->get('revenue_account');

    $message = "Purchased entity is null or not product variation";
    $this->logger->error('Could not get Xero account code for order item ' . $orderItem->id() . " $message.");
    return $accountCode;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // This should be parent::create(). But the base class has no create().
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->itemManager = $container->get('xero.item_manager');
    $instance->logger = $container->get('logger.factory')->get('ahs_accounting');
    return $instance;
  }

  /**
   * Prepare a description of an order item.
   *
   * Used in Xero and Stripe reports. There is  a 500 character limit.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $item
   *   The order to describe.
   *
   * @return string
   *   Order description.
   */
  protected function describeOrderItem(OrderItemInterface $item) {
    try {
      // Prepare a description of the order item if it is a single order item.
      $itemDescriptionArr = [];
      if ($purchased = $item->getPurchasedEntity()) {
        $itemDescriptionArr[] = $purchased->getSku();
      }
      if ($item->hasField('field_group') && $item->get('field_group')->entity instanceof GroupInterface) {
        $group = $item->get('field_group')->entity;
        $itemDescriptionArr[] = sprintf('%s %s (%s)', $group->bundle(), $group->id(), $group->label());
      }
      if ($item->hasField('field_notes') && !$item->get('field_notes')->isEmpty()) {
        $itemDescriptionArr[] = $item->field_notes->value;
      }
      $itemDescription = implode(' ', $itemDescriptionArr);

      // We don't know the true limit, but keep it to 250 for sanity.
      return substr($itemDescription, 0, 250);
    }
    catch (\Throwable $e) {
      $this->logger->error('Error creating description for order item' . $item->id() . '. ' . $e->getMessage());
    }

    return '';
  }

}
