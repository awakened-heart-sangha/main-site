<?php

namespace Drupal\ahs_commerce;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Converts user uuids for upcasting user objects.
 *
 * @code
 * ahs_commerce.route:
 *   path: example/{user}
 *   defaults:
 *     _controller: '\Drupal\ahs_commerce\Controller\AhsCommerceController::build'
 *   requirements:
 *     _access: 'TRUE'
 *   options:
 *     parameters:
 *       user:
 *        type: entity:user:uuid
 * @endcode
 *
 * @see \Drupal\Core\ParamConverter\EntityConverter
 */
class EntityUserUuidParamConverter implements ParamConverterInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityUserUuidParamConverter.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    // Return NULL if record not found to trigger 404 HTTP error.
    $user = NULL;
    $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['uuid' => $value]) ?: NULL;
    if ($users) {
      $user = reset($users);
    }
    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return !empty($definition['type']) && $definition['type'] == 'entity:user:uuid';
  }

}
