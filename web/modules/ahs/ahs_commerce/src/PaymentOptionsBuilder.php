<?php

namespace Drupal\ahs_commerce;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentOptionsBuilder as CommercePaymentOptionsBuilder;
use Drupal\commerce_payment\PaymentOptionsBuilderInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class PaymentOptionsBuilder. Builds payment options.
 */
class PaymentOptionsBuilder extends CommercePaymentOptionsBuilder {

  /**
   * Decorated payment options builder service object.
   *
   * @var \Drupal\commerce_payment\PaymentOptionsBuilder
   */
  protected $inner;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(PaymentOptionsBuilderInterface $payment_options_builder, AccountProxyInterface $current_user) {
    $this->inner = $payment_options_builder;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptions(OrderInterface $order, array $payment_gateways = []) {
    // We reset customer ID to anonymous temporarily
    // This is to ensure that any anon users who give
    // the email ID of an existing user don't get presented
    // with the payment menthods of that user.
    $customerId = $order->getCustomerId();
    if (!empty($customerId)) {
      if ($this->currentUser->id() != $customerId) {
        // No need to save the order, as the removal of
        // the customer is just temporary.
        $order->setCustomerId(0);
      }
    }

    $options = $this->inner->buildOptions($order, $payment_gateways);

    $unique = [];
    foreach ($options as $key => $option) {
      if (in_array($option->getLabel(), $unique)) {
        unset($options[$key]);
      }
      else {
        $unique[] = $option->getLabel();
      }
    }

    // Reset back the customer ID.
    if (!empty($customerId) && $customerId != $order->getCustomerId()) {
      $order->setCustomerId($customerId);
    }

    return $options;
  }

}
