<?php

namespace Drupal\ahs_commerce\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the redirect to the checkout form page.
 */
class AhsCommerceRetryController extends ControllerBase {

  /**
   * Builds and processes the form provided by the order's checkout flow.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect.
   */
  public function formPage(RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $step_id = $route_match->getParameter('step');

    // Redirect to the new route.
    $url = Url::fromRoute('commerce_recurring.payment_retry', [
      'commerce_order' => $order->id(),
      'step' => $step_id,
    ]);
    return new RedirectResponse($url->toString(TRUE)->getGeneratedUrl());
  }

}
