<?php

namespace Drupal\ahs_commerce\Controller;

use Drupal\commerce_checkout\Controller\CheckoutController as parentcheckoutController;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the checkout form page, with additional logging.
 */
class CheckoutController extends parentCheckoutController {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function formPage(RouteMatchInterface $route_match) {
    $order = $route_match->getParameter('commerce_order');
    $requested_step_id = $route_match->getParameter('step');
    $step_id = $this->checkoutOrderManager->getCheckoutStepId($order, $requested_step_id);

    // Log when each step is loaded.
    /** @var \Drupal\commerce_log\CommerceLogStorageInterface $logStorage */
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
    $message = "Loading checkout step '$step_id'.";
    if (empty($requested_step_id)) {
      $message = "Loading checkout step '$step_id' as default step.";
    }
    elseif ($requested_step_id != $step_id) {
      $message = "Loading checkout step '$step_id' although step '$requested_step_id' was requested.";
    }
    $log = $logStorage->generate($order, 'ahs_commerce_checkout_progress', ['message' => $message]);
    $log->save();

    return parent::formPage($route_match);
  }

}
