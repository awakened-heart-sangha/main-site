<?php

namespace Drupal\ahs_commerce\Controller;

use Drupal\commerce_cart\CartSession;
use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_recurring\Controller\RecurringPaymentRetryController as RecurringPaymentRetryControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the checkout form page.
 */
class RecurringPaymentRetryController extends RecurringPaymentRetryControllerBase {

  /**
   * The cart session.
   *
   * @var \Drupal\commerce_cart\CartSessionInterface
   */
  protected $cartSession;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->cartSession = $container->get('commerce_cart.cart_session');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->messenger = $container->get('messenger');
    $instance->logger = $container->get('logger.factory')->get('ahs_commerce');

    return $instance;
  }

  /**
   * Builds and processes the form provided by the order's checkout flow.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The render form.
   */
  public function formPage(RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');

    $requested_step_id = $route_match->getParameter('step');
    $step_id = $this->checkoutOrderManager->getCheckoutStepId($order, $requested_step_id);

    // Log when each step is loaded.
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
    $message = "Loading retry checkout step '$step_id'.";
    if ($requested_step_id != $step_id) {
      $message = "Redirecting to retry checkout step '$step_id' although step '$requested_step_id' was requested.";
    }
    $logStorage->generate($order, 'ahs_commerce_checkout_progress', ['message' => $message])->save();

    if ($requested_step_id != $step_id) {
      $url = Url::fromRoute('commerce_recurring.payment_retry', [
        'commerce_order' => $order->id(),
        'step' => $step_id,
      ]);
      return new RedirectResponse($url->toString(TRUE)->getGeneratedUrl());
    }

    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $this->checkoutOrderManager->getCheckoutFlow($order);
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    return $this->formBuilder->getForm($checkout_flow_plugin, $step_id);
  }

  /**
   * Checks access for the form page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(RouteMatchInterface $route_match, AccountInterface $account) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');

    if (!in_array('mark_paid', array_keys($order->getState()->getTransitions()))) {
      if ($order->getState()->getId() === 'completed') {
        // Only show the message if we are sure the order belongs to this user.
        $anonymousCanAccessPaymentInformation = $account->isAnonymous() &&
          in_array($order->id(), $this->cartSession->getCartIds(CartSessionInterface::COMPLETED));
        $authenticatedCanAccessPaymentInformation = $account->isAuthenticated() && $order->getCustomerId() === $account->id();

        if ($anonymousCanAccessPaymentInformation || $authenticatedCanAccessPaymentInformation) {
          $requiredUserIntervention = ($order->bundle() == 'recurring' && ($order->get('checkout_step')->value === 'complete'));
          if ($requiredUserIntervention) {
            $this->messenger
              ->addMessage($this->t('You have already successfully renewed this membership donation. Thankyou for your contribution.'));
          }
          else {
            $this->messenger->addMessage($this->t("It looks like your membership donation has been successfully processed, so there's no need for you to do anything more. Thankyou for your contribution."));
          }
        }
      }
      elseif ($order->getState()->getId() === 'canceled') {
        $this->messenger
          ->addMessage($this->t('This membership donation has been cancelled. There is no need for you to do anything more at this time.'));
        $this->logger
          ->addMessage($this->t('An attempt was made to setup a retry for cancelled order @order', ['@order' => $order->id()]));
      }
      return AccessResult::forbidden()->addCacheableDependency($order);
    }

    // This is a hack for allowing anonymous checkouts,
    // exposing payment methods. As its only the 4 last digits,
    // we can assume this risk. But for payment process to work,
    // we need to ensure there's a cart session with
    // the current order active.
    if ($account->isAnonymous()) {
      $this->cartSession->addCartId($order->id(), CartSession::ACTIVE);
    }

    // The user can checkout only their own non-empty orders.
    if ($account->isAuthenticated()) {
      $customer_check = $account->id() == $order->getCustomerId();
    }
    else {
      // We allow access, as the checkout flow should force login for an
      // anonymous user and afterwards we would check the order is owned by them.
      $customer_check = TRUE;
    }

    return AccessResult::allowedIf($customer_check)
      ->andIf(AccessResult::allowedIf($order->hasItems()))
      ->andIf(AccessResult::allowedIfHasPermission($account, 'access checkout'))
      ->addCacheableDependency($account)
      ->addCacheableDependency($order);
  }

}
