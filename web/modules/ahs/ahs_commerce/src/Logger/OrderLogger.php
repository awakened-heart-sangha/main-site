<?php

namespace Drupal\ahs_commerce\Logger;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Routing\RouteMatchInterface;
use Psr\Log\LoggerInterface;

/**
 * Copy site logs to commerce log on order routes.
 */
class OrderLogger implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ahs_commerce logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * Constructs an OrderLogger object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, LogMessageParserInterface $parser) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('ahs_commerce');
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    try {
      $ignoredChannels = [
        // Don't try to log errors while creating logs, in case of circularity.
        'commerce_log',
        // ahs_commerce_payment_gateway channel are guaranteed
        // to already have commerce logs.
        'ahs_commerce_payment_gateway',
      ];
      if (!in_array($context['channel'], $ignoredChannels)) {
        $order = $this->routeMatch->getParameter('commerce_order');
        if ($order instanceof OrderInterface && $order->getState()->getId() !== 'completed') {
          // Ignore routes to do with viewing or administering the order,
          // as these can throw irrelevant warnings and notices.
          // We only really care about checkouts and payments.
          $ignoredRoutes = [
            'entity.commerce_order.canonical',
            'entity.commerce_order.user_view',
            "entity.commerce_order.edit_form",
            "entity.commerce_order.delete_form",
            "entity.commerce_order.duplicate_form",
            "entity.commerce_order.resend_receipt_form",
          ];
          if (!in_array($this->routeMatch->getRouteName(), $ignoredRoutes)) {
            // Ensure variable substitution using FormattableMarkup
            $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
            $message = new \Drupal\Component\Render\FormattableMarkup($message, $message_placeholders);

            // Remove the call trace added
            // by ahs_miscellaneous_log_thrown_error.
            if ($context['channel'] === 'ahs_commerce') {
              $message = substr($message, 0, strpos($message, 'Trace:'));
            }

            $variables = [
              'channel' => $context['channel'],
              'level' => strtolower(RfcLogLevel::getLevels()[$level]),
              'message' => (string) $message,
            ];
            /** @var \Drupal\commerce_log\CommerceLogStorageInterface $logStorage */
            $logStorage = $this->entityTypeManager->getStorage('commerce_log');
            $logStorage->generate($order, 'ahs_commerce_order_logger', $variables)->save();
          }
        }
      }
    }
    catch (\Throwable $e) {
      $this->logger->error('Error creating commerce log for Stripe error.' . $e->getMessage());
    }
  }

}