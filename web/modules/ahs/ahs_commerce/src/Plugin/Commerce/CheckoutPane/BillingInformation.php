<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\BillingInformation as CommerceBillingInformation;

/**
 * Provides the billing information pane.
 *
 * The pane provided by commerce_checkout is made
 * undiscoverable by commerce_payment.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_billing_information",
 *   label = @Translation("Your name & address"),
 *   default_step = "about",
 *   wrapper_element = "fieldset",
 * )
 */
class BillingInformation extends CommerceBillingInformation {}
