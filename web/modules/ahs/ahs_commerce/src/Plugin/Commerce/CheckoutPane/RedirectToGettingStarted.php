<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the redirection pane.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_redirect_to_getting_started",
 *   label = @Translation("Redirect to Getting Started"),
 * )
 */
class RedirectToGettingStarted extends CheckoutPaneBase {

  /**
   * The group membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoader
   */
  protected $membershipLoader;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->membershipLoader = $container->get('group.membership_loader');
    $instance->logger = $container->get('logger.factory')->get('ahs_commerce');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $group = NULL;
    if ($this->order->getCustomer()) {
      $memberships = $this->membershipLoader->loadByUser($this->order->getCustomer());
      foreach ($memberships as $membership) {
        $group = $membership->getGroup();
        if ($group->bundle() === 'course' && $group->get('field_template')->target_id == 592) {
          break;
        }
      }
      if (!$group) {
        $url = Url::fromRoute('<front>');
        $this->logger->error('Could not find a Getting Started course');
      }
      else {
        $url = $group->toUrl();
      }
      throw new NeedsRedirectException($url->toString(TRUE)->getGeneratedUrl());
    }
  }

}
