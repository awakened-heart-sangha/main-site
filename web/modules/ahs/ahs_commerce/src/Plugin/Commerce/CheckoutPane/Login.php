<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\email_registration\Plugin\Commerce\CheckoutPane\EmailRegistrationLogin;

/**
 * Require coupled users to login, but not decoupled users.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_login",
 *   label = @Translation("Login"),
 *   default_step = "login",
 * )
 */
class Login extends EmailRegistrationLogin {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'decoupled_skip_login' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $summary = parent::buildConfigurationSummary() . '<br>';
    if (!empty($this->configuration['decoupled_skip_login'])) {
      $summary .= $this->t('Decoupled users skip login: Yes');
    }
    else {
      $summary .= $this->t('Decoupled users skip login: No');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['decoupled_skip_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Decoupled users skip login'),
      '#description' => $this->t('If the order customer is a decoupled user, or the order email is that of a decoupled user, skip this login pane.'),
      '#default_value' => $this->configuration['decoupled_skip_login'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['decoupled_skip_login'] = !empty($values['decoupled_skip_login']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    // Identify the user who may need to log in,
    // based on order customer or email.
    $user = NULL;
    if (!empty($this->order->getCustomerId())) {
      $user = $this->order->getCustomer();
    }
    elseif (!empty($this->order->getEmail())) {
      $user = user_load_by_mail($this->order->getEmail());
    }

    // Skip login for decoupled users, because they're incapable of logging in.
    if ($user && $user->isDecoupled() && !$user->isAnonymous()) {
      // We don't set decoupled users as order customers yet,
      // so as to avoid exposing previously stored payment methods without
      // proper authentication.
      return FALSE;
    }

    // Everyone else must login, unless someone is already logged in.
    return $this->currentUser->isAnonymous();
  }

}
