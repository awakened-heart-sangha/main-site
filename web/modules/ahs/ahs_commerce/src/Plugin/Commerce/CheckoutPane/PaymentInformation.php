<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentInformation as parentPaymentInformation;

/**
 * Customising the payment information pane.
 *
 * Disabling this pane will automatically disable the payment process pane,
 * since they are always used together. Developers subclassing this pane
 * should use hook_commerce_checkout_pane_info_alter(array &$panes) to
 * point $panes['payment_information']['class'] to the new child class.
 */
class PaymentInformation extends parentPaymentInformation {

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if (!$this->order->getTotalPrice() || $this->order->isPaid() || $this->order->getTotalPrice()->isZero()) {
      return FALSE;
    }
    return TRUE;
  }

}
