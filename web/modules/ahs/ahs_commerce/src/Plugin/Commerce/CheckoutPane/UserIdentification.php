<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\ContactInformation;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Ensures that the order has an email adequate to identify a user.
 *
 * If the create_user setting is enabled, a new user if created if
 * no existing user has this email. This assumes a module like
 * decoupled_auth is in operation so it is both possible
 * and OK UX to create users from an email address alone.
 *
 * If the order has an email, we try to find a user with that email or
 * create one. If the order has no email or creation would fail, we ask
 * the user to provide an email.
 * When the user provides an email, we try again to find or create a user.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_user_identification_by_email",
 *   label = @Translation("Identify user by email"),
 *   default_step = "email",
 * )
 */
class UserIdentification extends ContactInformation {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->logger = $container->get('logger.factory')->get('ahs_commerce');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'create_user' => FALSE,
      'set_decoupled_customer' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $summary = parent::buildconfigurationSummary() . '<br>';
    if (!empty($this->configuration['create_user'])) {
      $summary .= $this->t('Create user: Yes');
    }
    else {
      $summary .= $this->t('Create user: No');
    }
    $summary .= '<br>';
    if (!empty($this->configuration['set_decoupled_customer'])) {
      $summary .= $this->t('Set customer: Yes');
    }
    else {
      $summary .= $this->t('Set customer: No');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['create_user'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create user'),
      '#description' => $this->t('Create a new user with the email address provided for this order, if no existing user has this email.'),
      '#default_value' => $this->configuration['create_user'],
    ];

    $form['set_decoupled_customer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set customer'),
      '#description' => $this->t('Set an identified user as the customer (without requiring login) if the user does not yet have a username & password.'),
      '#default_value' => $this->configuration['set_decoupled_customer'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['create_user'] = !empty($values['create_user']);
      $this->configuration['set_decoupled_customer'] = !empty($values['set_decoupled_customer']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    $id = $this->order->getCustomerId();
    $hasNoCustomer = empty($id) || $id === '0';
    return $hasNoCustomer;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Try to find a user, creating one if necessary,
    // and redirect if successful.
    $email = $this->order->getEmail();
    if ($user = $this->identifyUserByEmail($email)) {
      $this->prepareCustomer($user);
      $next_step_id = $this->getNextVisibleStepId();
      if ($next_step_id) {
        $this->checkoutFlow->redirectToStep($next_step_id);
      }
      $this->logger->warning('Could not find visible checkout steps after ' . $this->getStepId());
      return;
    }

    // If we failed to find or create a user with the supplied email,
    // ask the user to revise it.
    if (!empty($email)) {
      $this->messenger()->addError($this->t('We were unable to find or create a user with this email.'));
    }
    return parent::buildPaneForm($pane_form, $form_state, $complete_form);
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::validatePaneForm($pane_form, $form_state, $complete_form);
    $values = $form_state->getValue($pane_form['#parents']);
    $user = $this->identifyUserByEmail($values['email']);
    if (!$user) {
      $form_state->setError($pane_form, $this->t('We were unable to find or create a user with this email.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::submitPaneForm($pane_form, $form_state, $complete_form);
    $values = $form_state->getValue($pane_form['#parents']);
    $user = $this->identifyUserByEmail($values['email']);
    if ($user) {
      $this->prepareCustomer($user);
    }
    else {
      $this->logger->warning('Could not identify user when user identification pane submitted');
    }
  }

  /**
   * Load an existing user by an email address, or creating a new user.
   *
   * @param string $email
   *   The email address of the user.
   *
   * @return \Drupal\user\UserInterface|false
   *   The identified user, or false is no existing user was found.
   */
  protected function loadUserByEmail($email) {
    if (empty($email)) {
      return FALSE;
    }

    $users = $this->entityTypeManager
      ->getStorage('user')
      ->loadByProperties(['mail' => $email]);
    return $users ? reset($users) : FALSE;
  }

  /**
   * Identify a user by an email address, or creating a new user.
   *
   * @param string $email
   *   The email address of the user.
   *
   * @return \Drupal\user\UserInterface|false
   *   The identified but unsaved user, or false is no existing
   *   user was found and creation fails.
   */
  protected function identifyUserByEmail($email) {
    $user = $this->loadUserByEmail($email);

    // If a user with that email can't be found, create one.
    if (!$user && $email) {
      $user = $this->createUserFromEmail($email);
    }

    if ($user) {
      return $user;
    }
    return FALSE;
  }

  /**
   * Create a user from an email address.
   *
   * @param string $email
   *   The email address of the user.
   *
   * @return \Drupal\user\UserInterface|false
   *   The newly created but unsaved user, or false is creation fails.
   */
  protected function createUserFromEmail($email) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->create([
      'mail' => $email,
      'status' => TRUE,
    ]);

    // Return the user if we can create one successfully.
    /** @var \Drupal\Core\Entity\EntityConstraintViolationListInterface $violations */
    $violations = $user->validate();
    if ($violations->count() === 0) {
      return $user;
    }

    if ($this->configuration['create_user']) {
      $this->logger->warning("Could not create user automatically because of user entity constraint violations:\n" . (string) $violations);
    }
    return FALSE;
  }

  /**
   * Prepares the user to be the order customer.
   *
   * Optionally save the user if it is newly created.
   *
   * @param \Drupal\user\UserInterface $user
   *   User entity.
   */
  protected function prepareCustomer($user) {
    if ($user->isNew() && $this->configuration['create_user']) {
      $user->save();
    }
    $this->order->setCustomerId($user->id());
  }

  /**
   * Get the id of the next visible step.
   *
   * We have to check visibility ourselves because
   * $this->checkoutFlow->getVisibleSteps() uses a cache that can
   * be stale. In particular the user creation in this pane
   * changes the visibilty of the login pane.
   */
  protected function getNextVisibleStepId() {
    $step = $this->getStepId();
    while ($step = $this->checkoutFlow->getNextStepId($step)) {
      if (!empty($this->checkoutFlow->getVisiblePanes($step))) {
        return $step;
      }
    }
    return NULL;
  }

}
