<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the redirection pane.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_redirection",
 *   label = @Translation("Redirect"),
 * )
 */
class Redirection extends CheckoutPaneBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'destination' => 'internal:/',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    return $this->configuration['destination'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect to'),
      '#description' => $this->t("URI of the destination. For example 'internal:/search' or 'http://google.com'. This will be ignored if the order data has a 'redirect_on_completion_uri' property."),
      '#default_value' => $this->configuration['destination'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['destination'] = $values['destination'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    if ($this->order->getData('redirect_on_completion_messages')) {
      foreach ($this->order->getData('redirect_on_completion_messages') as $message) {
        if (!empty($message)) {
          $this->messenger->addMessage($message);
        }
      }
    }

    $uri = $this->configuration['destination'];
    if ($this->order->getData('redirect_on_completion_uri')) {
      $uri = $this->order->getData('redirect_on_completion_uri');
    }
    if (substr($uri, 0, 1) === '/') {
      $uri = 'internal:' . $uri;
    }
    throw new NeedsRedirectException(Url::fromUri($uri)->toString(TRUE)->getGeneratedUrl());
  }

}
