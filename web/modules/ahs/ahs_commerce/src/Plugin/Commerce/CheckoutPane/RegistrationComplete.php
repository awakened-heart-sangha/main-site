<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the registration complete pane.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_registration_complete",
 *   label = @Translation("AHS registration complete"),
 * )
 */
class RegistrationComplete extends CheckoutPaneBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $variation = $this->order->getItems()[0]->getPurchasedEntity();

    $handler = $this->entityTypeManager->getHandler('registration', 'host_entity');
    $host = $handler->createHostEntity($variation);

    $confirmation_message = $host->getSetting('confirmation');
    $confirmation_message = empty($confirmation_message) ? 'Thankyou for registering.' : $confirmation_message;
    $this->messenger->addMessage($confirmation_message);

    $redirect = $host->getSetting('confirmation_redirect');
    if ($redirect) {
      // Custom redirect in the settings.
      // Check for external first.
      if (UrlHelper::isExternal($redirect)) {
        // To be considered external, the URL helper checks
        // for dangerous protocols, so the redirect must be safe.
        // Sanitize and use it.
        $redirect = Html::escape($redirect);
        $redirect = Url::fromUri($redirect);
      }
      else {
        // Potentially unsafe URL. Try for an internal redirect.
        $redirect = UrlHelper::stripDangerousProtocols($redirect);
        $redirect = Html::escape($redirect);
        $redirect = Url::fromUserInput($redirect);
      }
    }
    else {
      $group = $variation->getGroup();
      $redirect = $group ? $group->toUrl() : Url::fromUri('<fornt>');
    }
    throw new NeedsRedirectException($redirect->toString(TRUE)->getGeneratedUrl());

  }

}
