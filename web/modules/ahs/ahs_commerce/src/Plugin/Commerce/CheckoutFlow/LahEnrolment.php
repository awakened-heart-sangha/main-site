<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutFlow;

/**
 * Provides a multistep checkout flow for AHS LAH enrolment.
 *
 * @CommerceCheckoutFlow(
 *   id = "ahs_commerce_lah_enrolment",
 *   label = "AHS LAH Enrolment",
 * )
 */
class LahEnrolment extends AhsCheckoutBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    $default = parent::getSteps();
    return [
      'email' => $default['identification'],
      'login' => $default['login'],
      'about' => $default['pre_payment_information'],
      'payment_information' => $default['payment_information'],
      'payment_review' => [
        'next_label' => 'Donate now',
      ] + $default['payment_review'],
      'payment' => [
        'next_label' => 'Donate now',
      ] + $default['payment'],
      'gift_aid' => $default['gift_aid'],
      'complete' => $default['post_payment_information'],
      'finish' => [
        'label' => 'Get started',
      ] + $default['complete'],
    ];
  }

}
