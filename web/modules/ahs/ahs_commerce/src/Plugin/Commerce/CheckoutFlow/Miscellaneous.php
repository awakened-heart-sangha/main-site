<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutFlow;

/**
 * Provides a multistep checkout flow for AHS misc donations & payments.
 *
 * @CommerceCheckoutFlow(
 *   id = "ahs_commerce_miscellaneous",
 *   label = "AHS Miscellaneous donations & payments",
 * )
 */
class Miscellaneous extends AhsCheckoutBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    $default = parent::getSteps();
    return [
      'identification' => $default['identification'],
      'login' => $default['login'],
      'payment_information' => $default['payment_information'],
      'payment_review' => $default['payment_review'],
      'payment' => $default['payment'],
      'gift_aid' => $default['gift_aid'],
      'complete' => $default['complete'],
    ];
  }

}
