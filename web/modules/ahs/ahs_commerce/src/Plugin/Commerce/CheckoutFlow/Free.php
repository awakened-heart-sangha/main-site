<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutFlow;

/**
 * Provides a multistep checkout flow for AHS free order.
 *
 * @CommerceCheckoutFlow(
 *   id = "ahs_commerce_free",
 *   label = "AHS Free",
 * )
 */
class Free extends AhsCheckoutBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    $default = parent::getSteps();
    return [
      'payment_information' => [
        'label' => 'Your request',
      ] + $default['payment_information'],
      'complete' => [
        'next_label' => 'Confirm',
      ] + $default['complete'],
    ];
  }

}
