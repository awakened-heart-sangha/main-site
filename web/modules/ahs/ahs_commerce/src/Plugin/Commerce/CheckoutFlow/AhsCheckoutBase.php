<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\CheckoutPaneManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;
use Drupal\commerce_order\OrderAssignmentInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Base class for a checkout that supports multiple steps after payment.
 */
class AhsCheckoutBase extends CheckoutFlowWithPanesBase {

  /**
   * The order assignment.
   *
   * @var \Drupal\commerce_order\OrderAssignmentInterface
   */
  protected $orderAssignment;

  /**
   * Constructs a new CheckoutFlowWithPanesBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pane_id
   *   The plugin_id for the plugin instance.
   * @param mixed $pane_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\commerce_checkout\CheckoutPaneManager $pane_manager
   *   The checkout pane manager.
   * @param \Drupal\commerce_order\OrderAssignmentInterface $order_assignment
   *   The order assignment.
   */
  public function __construct(array $configuration, $pane_id, $pane_definition, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, RouteMatchInterface $route_match, CheckoutPaneManager $pane_manager, OrderAssignmentInterface $order_assignment) {
    parent::__construct($configuration, $pane_id, $pane_definition, $entity_type_manager, $event_dispatcher, $route_match, $pane_manager);
    $this->orderAssignment = $order_assignment;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pane_id, $pane_definition) {
    return new static(
      $configuration,
      $pane_id,
      $pane_definition,
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'),
      $container->get('current_route_match'),
      $container->get('plugin.manager.commerce_checkout_pane'),
      $container->get('commerce_order.order_assignment')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    return [
      'identification' => [
        'label' => $this->t('Your email'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => FALSE,
      ],
      'login' => [
        'label' => $this->t('Login'),
        'next_label' => $this->t('Continue to next step'),
        'has_sidebar' => FALSE,
      ],
      'pre_payment_information' => [
        'label' => $this->t('About you'),
        'next_label' => $this->t('Continue to next step'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => FALSE,
      ],
      'payment_information' => [
        'label' => $this->t('Payment information'),
        'next_label' => $this->t('Continue to next step'),
        'has_sidebar' => TRUE,
      ],
      'payment_review' => [
        'label' => $this->t('Reviewing...'),
        'next_label' => $this->t('Pay now'),
        'has_sidebar' => FALSE,
      ],
      'payment' => [
        'label' => $this->t('Payment'),
        'next_label' => $this->t('Pay now'),
        'has_sidebar' => FALSE,
        'hidden' => TRUE,
      ],
      'gift_aid' => [
        'label' => $this->t('Gift Aid'),
        'next_label' => $this->t('Continue to next step'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => FALSE,
      ],
      'post_payment_information' => [
        'label' => $this->t('About you'),
        'next_label' => $this->t('Continue to next step'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => FALSE,
      ],
      'complete' => [
        'label' => $this->t('Thankyou'),
        'next_label' => $this->t('Finish'),
        'has_sidebar' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    foreach (Element::children($actions) as $action_id) {
      $actions[$action_id]['#attributes']['class'][] = Html::cleanCssIdentifier($form['#step_id'] . '--' . $action_id);
    }
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function onStepChange($step_id) {
    /** @var \Drupal\commerce_log\CommerceLogStorageInterface $logStorage */
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');

    // We take action on any step after payment, because we can't be sure which
    // is the first step after payment because any step may be
    // empty of panes and so skipped.
    if ($this->isStepAfterPayment($step_id)) {
      // Set a user as the order customer if there is one
      // who matches the order email.
      if (empty($this->order->getCustomerId()) && $email = $this->order->getEmail()) {
        $logStorage->generate($this->order, 'ahs_commerce_checkout_progress', ['message' => "Trying to assign customer based on email $email."])->save();
        $user_storage = \Drupal::entityTypeManager()->getStorage('user');
        $users = $user_storage->loadByProperties(['mail' => $email]);
        $user = reset($users);
        if ($user) {
          $this->orderAssignment->assign($this->order, $user);
        }
      }
      // Place the order.
      if ($this->order->getState()->getId() === 'draft') {
        $logStorage->generate($this->order, 'ahs_commerce_checkout_progress', ['message' => "Placing draft order."])->save();
        $transition = $this->order->getState()->getWorkflow()->getTransition('place');
        $this->order->getState()->applyTransition($transition);
      }
    }
    if ($step_id == 'payment') {
      $this->order->lock();
    }
    else {
      $this->order->unlock();
    }

    /** @var \Drupal\commerce_log\CommerceLogStorageInterface $logStorage */
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
    $logStorage->generate($this->order, 'ahs_commerce_checkout_progress', ['message' => "Preparing to change to step '$step_id'."])->save();
  }

  /**
   * Determine if a step is after the payment step in the checkout flow.
   *
   * @param string $step_id
   *   The step ID.
   *
   * @return bool
   *   TRUE if the step is after payment, FALSE otherwise.
   */
  protected function isStepAfterPayment($step_id) {
    $steps = array_keys($this->getSteps());
    $paymentStepIndex = array_search('payment', $steps);
    $currentStepIndex = array_search($step_id, $steps);
    return ($currentStepIndex > $paymentStepIndex);
  }

  /**
   * {@inheritdoc}
   */
  public function redirectToStep($step_id) {
    // Log when moving on to the next step.
    /** @var \Drupal\commerce_log\CommerceLogStorageInterface $logStorage */
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
    $logStorage->generate($this->order, 'ahs_commerce_checkout_progress', ['message' => "Preparing to redirect to checkout step '$step_id'."])->save();
    try {
      parent::redirectToStep($step_id);
    }
    catch (NeedsRedirectException $e) {
      $logStorage->generate($this->order, 'ahs_commerce_checkout_progress', ['message' => "Order saved. Now redirecting to checkout step '$step_id'."])->save();
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStepId($requested_step_id = NULL) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->order;

    $available_step_ids = array_keys($this->getVisibleSteps());

    // Get the step stored order on the order.
    $selected_step_id = $order->get('checkout_step')->value;
    $selected_step_id = $selected_step_id ?: reset($available_step_ids);

    // Return the step stored on the order if a different step is not requested.
    if (empty($requested_step_id) || $requested_step_id == $selected_step_id) {
      return $selected_step_id;
    }

    // Return the requested step if it is before the stored step.
    if (in_array($requested_step_id, $available_step_ids)) {
      $requested_step_index = array_search($requested_step_id, $available_step_ids);
      $payment_step_index = array_search('payment', $available_step_ids);
      // Don't allow access to payment or before unless order is a draft.
      if ($requested_step_index <= $payment_step_index && $order->getState()->value != 'draft') {
        return $selected_step_id;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $step_id = NULL) {
    $visible_panes = array_keys($this->getVisiblePanes($step_id));
    $panes_message = empty($visible_panes) ? 'no visible panes' : 'visible panes: ' . implode(', ', $visible_panes) . '';
    $message = "Building form for step '$step_id' with $panes_message.";
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
    /** @var \Drupal\commerce_log\CommerceLogStorageInterface $logStorage */
    $logStorage->generate($this->order, 'ahs_commerce_checkout_progress', ['message' => $message])->save();
    return parent::buildForm($form, $form_state, $step_id);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Log when step is submitted.
    $step_id = $form['#step_id'];
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
    /** @var \Drupal\commerce_log\CommerceLogStorageInterface $logStorage */
    $logStorage->generate($this->order, 'ahs_commerce_checkout_progress', ['message' => "Submitting checkout step '$step_id'."])->save();
    parent::submitForm($form, $form_state);
  }

}
