<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\CheckoutFlow;

/**
 * Provides a multistep checkout flow for AHS membership.
 *
 * @CommerceCheckoutFlow(
 *   id = "ahs_commerce_membership",
 *   label = "AHS Membership",
 * )
 */
class MonthlyMembership extends AhsCheckoutBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    $default = parent::getSteps();
    return [
      'identification' => $default['identification'],
      'login' => $default['login'],
      'payment_information' => $default['payment_information'],
      'payment_review' => [
        'next_label' => 'Donate now',
      ] + $default['payment_review'],
      'payment' => [
        'next_label' => 'Donate now',
      ] + $default['payment'],
      'gift_aid' => $default['gift_aid'],
      'complete' => $default['complete'],
    ];
  }

}
