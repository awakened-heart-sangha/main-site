<?php

namespace Drupal\ahs_commerce\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_stripe\Plugin\Commerce\PaymentGateway\Stripe as StripeBase;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\group\Entity\GroupInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\CardException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the AHS Stripe payment gateway.
 *
 * It simply logs payment failures.
 *
 * @CommercePaymentGateway(
 *   id = "ahs_commerce_stripe",
 *   label = "AHS Stripe",
 *   display_label = "Stripe",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_stripe\PluginForm\Stripe\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   js_library = "commerce_stripe/form",
 *   requires_billing_information = FALSE,
 * )
 */
class Stripe extends StripeBase {

  /**
   * The log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * Route match for the current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->routeMatch = $container->get('current_route_match');
    $instance->logStorage = $container->get('entity_type.manager')->getStorage('commerce_log');

    return $instance;
  }

  /**
   * Helper to try gateway method.
   */
  protected function tryGatewayMethod($method, $args, OrderInterface $order = NULL) {
    try {
      return parent::{$method}(...$args);
    }
    // Commerce log and rethrow any errors.
    catch (\Throwable $e) {
      $this->handleThrowable($e, $method, $order);
    }
  }

  /**
   * Helper to handle errors.
   */
  protected function handleThrowable(\Throwable $e, $method, OrderInterface $order = NULL) {
    $message = get_class($this) . "::$method() threw ";
    $severity = RfcLogLevel::ERROR;
    if (!$order) {
      // createPaymentMethod is called in the checkout but is not passed
      // the order, so we have to get it form the route.
      $order = $this->routeMatch->getParameter('commerce_order');
    }
    if ($order instanceof OrderInterface) {
      // Create a generic commerce order log.
      $variables = [
        'gateway_id' => $this->getPluginId(),
        'method' => $method,
        'message' => $e->getMessage() . ' ' . $e->getTraceAsString(),
        'throwable' => get_class($e),
        'uid' => $order->getCustomerId() ?? \Drupal::currentUser()->id(),
      ];
      $this->logStorage->generate($order, 'ahs_commerce_payment_gateway_error', $variables)->save();

      // Create a commerce order log for any Stripe error.
      $previous = $e->getPrevious();
      if ($previous && $previous instanceof ApiErrorException) {
        $variables = [
          'message' => $previous->getMessage(),
          'stripe_code' => $previous->getStripeCode(),
        ];
        if ($previous instanceof CardException) {
          $variables['decline_code'] = $previous->getDeclineCode();
          $severity = RfcLogLevel::WARNING;
        }
        $this->logStorage->generate($order, 'ahs_commerce_stripe_api_error', $variables)->save();
      }

      $message = "Order #" . $order->id() . ": $message";
    }

    // Create a site log of the exception with enhanced detail.
    // We use 'ahs_commerce_payment_gateway' so that the OrderLogger can
    // skip it and not create a commerce log that duplicates the ones
    // created above. This should be a warning, as card declines exceptions
    // are not actual code errors.
    ahs_miscellaneous_log_thrown_error('ahs_commerce_payment_gateway', $e, $severity, $message);

    // Rethrow the exception to continue normal handling of it by commerce.
    throw $e;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->tryGatewayMethod('createPayment', [$payment, $capture], $payment->getOrder());
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $this->tryGatewayMethod('deletePaymentMethod', [$payment_method], NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $this->tryGatewayMethod('createPaymentMethod', [$payment_method, $payment_details], NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function canCapturePayment(PaymentInterface $payment) {
    $this->tryGatewayMethod('canCapturePayment', [$payment], $payment->getOrder());
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->tryGatewayMethod('capturePayment', [$payment, $amount], $payment->getOrder());
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    $this->tryGatewayMethod('canRefundPayment', [$payment], $payment->getOrder());
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->tryGatewayMethod('refundPayment', [$payment, $amount], $payment->getOrder());
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->tryGatewayMethod('voidPayment', [$payment], $payment->getOrder());
  }

  /**
   * {@inheritdoc}
   */
  protected function assertPaymentMethod(PaymentMethodInterface $payment_method = NULL) {
    if (empty($payment_method)) {
      throw new \InvalidArgumentException('The provided payment has no payment method referenced.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentIntent(OrderInterface $order, $intent_attributes = [], PaymentInterface $payment = NULL) {
    try {
      return $this->doCreatePaymentIntent($order, $intent_attributes, $payment);
    }
    catch (\Throwable $e) {
      $this->handleThrowable($e, 'createPaymentIntent', $order);
    }

    return parent::createPaymentIntent($order, $intent_attributes, $payment);
  }

  /**
   * Helper to create a new payment.
   */
  protected function doCreatePaymentIntent(OrderInterface $order, array $intent_params = [], $payment = NULL) {
    $description = $this->describeOrder($order);
    if ($description) {
      $intent_params['description'] = $description;
    }

    // Extra protection against duplicate payments.
    if ($order->isPaid() || $payment && $payment->isCompleted()) {
      throw new \Exception('Attempted to create a payment attempt on order ' . $order->id() . ' that was already paid.');
    }

    return parent::createPaymentIntent($order, $intent_params, $payment);
  }

  /**
   * Prepare a description of an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to describe.
   *
   * @return string
   *   Description of the order.
   */
  protected function describeOrder(OrderInterface $order) {
    try {
      // Prepare a description of the order item if it is a single order item.
      $itemDescriptionArr = [];
      $items = $order->getItems();
      if (count($items) === 1) {
        $item = $items[0];
        if ($purchased = $item->getPurchasedEntity()) {
          $itemDescriptionArr[] = $purchased->getSku();
        }
        $group = NULL;
        if ($item->hasField('field_group') && $item->get('field_group')->entity instanceof GroupInterface) {
          $group = $item->get('field_group')->entity;
        }
        // Try to get group from the product.
        elseif ($item->hasPurchasedEntity()) {
          $purchased_entity = $item->getPurchasedEntity();
          if ('commerce_product_variation' == $purchased_entity->getEntityTypeId() && $product = $purchased_entity->getProduct()) {
            if ($product->hasField('field_group') && $product->get('field_group')->entity instanceof GroupInterface) {
              $group = $product->get('field_group')->entity;
            }
          }
        }

        if ($group) {
          $itemDescriptionArr[] = sprintf('%s %s (%s)', $group->bundle(), $group->id(), $group->label());
        }

        if ($item->hasField('field_notes') && !$item->get('field_notes')->isEmpty()) {
          $itemDescriptionArr[] = $item->field_notes->value;
        }
      }
      $itemDescription = implode(' ', $itemDescriptionArr);

      // Limit to 500 for sanity.
      return substr(sprintf('DC#%s%s', $order->id(), $itemDescription), 0, 500);
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_commerce')->error('Error creating description for Stripe payment intent on order ' . $order->id() . '. ' . $e->getMessage());
    }

    return '';
  }

}
