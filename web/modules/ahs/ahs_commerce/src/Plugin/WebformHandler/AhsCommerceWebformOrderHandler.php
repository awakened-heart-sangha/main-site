<?php

namespace Drupal\ahs_commerce\Plugin\WebformHandler;

use Drupal\commerce_price\Price;
use Drupal\commerce_webform_order\Plugin\WebformHandler\CommerceWebformOrderHandler;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Creates a commerce order with a webform submission.
 *
 * This AHS version allows the order item amount to be zero,
 * instead of defaulting to the product variation price.
 *
 * @WebformHandler(
 *   id = "ahs_commerce_webform_order_allow_zero",
 *   label = @Translation("AHS Commerce Webform Order Handler (allows zero)"),
 *   category = @Translation("Commerce"),
 *   description = @Translation("Creates a commerce order with a webform submission, allowing a zero amount."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   conditions = TRUE,
 *   tokens = TRUE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class AhsCommerceWebformOrderHandler extends CommerceWebformOrderHandler {

  /**
   * Helper to prepare data.
   */
  protected function prepareData(WebformSubmissionInterface $webform_submission) {
    // Override the parent method's replacement of zero amount
    // with product variation price.
    $prepared_data = parent::prepareData($webform_submission);
    $original_data = $this->replaceConfiguration($webform_submission);
    $original_amount = $original_data['order_item']['amount'];
    if (is_numeric($original_amount) && $original_amount == 0) {
      $prepared_data['price'] = new Price($original_amount, $prepared_data['price']->getCurrencyCode());
    }
    return $prepared_data;
  }

}
