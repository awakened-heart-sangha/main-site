<?php

namespace Drupal\ahs_commerce\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Stripe\Customer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the job type for Stripe customer data synchronization.
 *
 * @AdvancedQueueJobType(
 *   id = "ahs_commerce_stripe_customer_update",
 *   label = @Translation("Send customer data updates to Stripe"),
 * )
 */
class StripeCustomerUpdate extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // The base class has no create() so can't use parent::create().
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->logger = $container->get('logger.factory')->get('ahs_commerce');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    $userId = $job->getPayload()['user_id'];
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($userId);

    if ($user != NULL) {
      $remote_ids = $user->get('commerce_remote_id');
      $remote_id = $remote_ids->getByProvider('shrimala_trust_stripe|live') ?? $remote_ids->getByProvider('shrimala_trust_stripe|test');

      if ($remote_id) {
        try {
          // Instantiating the gateway ensures the Stripe connection is setup.
          $gateway = $this->entityTypeManager->getStorage('commerce_payment_gateway')->load('shrimala_trust_stripe');
          $gateway->getPlugin();

          $stripeCustomer = Customer::retrieve($remote_id);
          $stripeCustomer->email = $user->getEmail();
          $stripeCustomer->name = $user->label();
          $stripeCustomer->description = sprintf('%s (%s)', $user->label(), $user->id());
          $stripeCustomer->save();
          $result = JobResult::success($this->t('Data updated for user @user', ['@user' => $user->label()]));
        }
        catch (\Throwable $e) {
          $message = $this->t('Error when updating user @user stripe data: @error', [
            '@user' => $user->label(),
            '@error' => $e->getMessage(),
          ]);
          $result = JobResult::failure($message);
          $message = $this->t('Error when updating user @user stripe data: @error \n@trace', [
            '@user' => $user->label(),
            '@error' => $e->getMessage(),
            '@trace' => $e->getTraceAsString(),
          ]);
          $this->logger->error($message);
        }
      }
      else {
        $result = JobResult::success($this->t('No Stripe customer id for user @user', ['@user' => $user->label()]));
      }
    }
    else {
      $result = JobResult::success($this->t('No user @user found', ['@user' => $userId]));
    }
    return $result;
  }

}
