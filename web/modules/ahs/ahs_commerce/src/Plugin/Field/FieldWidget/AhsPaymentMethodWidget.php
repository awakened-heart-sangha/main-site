<?php

namespace Drupal\ahs_commerce\Plugin\Field\FieldWidget;

use Drupal\commerce_recurring\Plugin\Field\FieldWidget\PaymentMethodWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'commerce_recurring_payment_method' widget.
 */
class AhsPaymentMethodWidget extends PaymentMethodWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $default = NULL;

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription */
    if ($subscription = $items->getEntity()) {
      $default = $subscription->getPaymentMethodId();
    }

    if (!empty($element['target_id']['#options'])) {
      $options = &$element['target_id']['#options'];

      $unique = [];
      // Force add default option to the option list.
      if (isset($default, $options[$default])) {
        $unique[] = $options[$default];
      }

      foreach ($options as $key => $option) {
        if (isset($default) && $key == $default) {
          continue;
        }

        if (in_array($option, $unique)) {
          unset($options[$key]);
        }
        else {
          $unique[] = $option;
        }
      }
    }

    return $element;
  }

}
