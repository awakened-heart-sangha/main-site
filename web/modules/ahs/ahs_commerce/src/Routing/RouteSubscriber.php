<?php

namespace Drupal\ahs_commerce\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // onAlterRoutes() is a method in
    // RouteSubscriberBase, wrapping alterRoutes().
    $events[RoutingEvents::ALTER] = 'onAlterRoutes';
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Use our custom checkout controller that has additional logging.
    if ($route = $collection->get('commerce_checkout.form')) {
      $route->setDefault('_controller', '\Drupal\ahs_commerce\Controller\CheckoutController::formPage');
    }

    // Allow to retry/confirm payments as anonymous.
    // We change the user parameter so it's an uuid.
    if ($route = $collection->get('commerce_recurring.payment_retry')) {
      $route->setDefault('_controller', '\Drupal\ahs_commerce\Controller\RecurringPaymentRetryController::formPage');
      $route->setRequirement('_custom_access', '\Drupal\ahs_commerce\Controller\RecurringPaymentRetryController::checkAccess');
    }

    // Adjust title for subscription customer form.
    if ($route = $collection->get('entity.commerce_subscription.customer_edit_form')) {
      $route->setDefault('_title_callback', '\Drupal\ahs_commerce\Controller\SubscriptionController::title');
    }
  }

}
