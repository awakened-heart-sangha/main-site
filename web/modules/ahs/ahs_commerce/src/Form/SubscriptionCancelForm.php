<?php

namespace Drupal\ahs_commerce\Form;

use Drupal\commerce_recurring\Form\SubscriptionCancelForm as parentCancelForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a confirmation form for cancelling a subscription.
 *
 * @internal
 */
class SubscriptionCancelForm extends parentCancelForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['cancel_option']['scheduled']['#disabled'] = TRUE;
    $form['cancel_option']['#default_value'] = 'now';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $subscription = $this->entity;
    $customer = $subscription->getCustomer();
    if ($customer) {
      $form_state->setRedirect('view.user_subscriptions.membership_page', [
        'user' => $customer->id(),
      ]);
    }
  }

}
