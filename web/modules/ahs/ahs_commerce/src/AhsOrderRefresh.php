<?php

namespace Drupal\ahs_commerce;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderRefresh;

/**
 * Modifies the implementation for order refresh.
 *
 * If there is a non-anonymous order customer, the parent::refresh()
 * syncs the order email with the customer email. We don't want to do
 * this, because we are using alternative_user_emails and decoupled_auth
 * so the email the user has given at checkout is a better guide to their
 * preferred order email than the email stored on the customer.
 */
class AhsOrderRefresh extends OrderRefresh {

  /**
   * {@inheritdoc}
   */
  public function refresh(OrderInterface $order) {
    $email = $order->getEmail();
    parent::refresh($order);
    if ($email) {
      $order->setEmail($email);
    }
  }

}
