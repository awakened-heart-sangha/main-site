<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\ahs_training\Entity\TrainingRecord;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\message\Entity\Message;
use Drupal\message_notify\MessageNotifier;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PlacedMaterialsOrder. Provides subscriber for orders.
 */
class PlacedMaterialsOrder implements EventSubscriberInterface {

  /**
   * The message notify.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotify;

  /**
   * Constructs a new PlacedMaterialsOrder.
   */
  public function __construct(MessageNotifier $message_notify) {
    $this->messageNotify = $message_notify;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => ['recordMaterialsExperiences', 200],
      'commerce_order.fulfill.post_transition' => ['notifyStudent', 300],
    ];
    return $events;
  }

  /**
   * Record training experiences based on orders of materials.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function recordMaterialsExperiences(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if ($order->bundle() == 'recurring') {
      return;
    }
    if (!$customer = $order->getCustomer()) {
      return;
    }
    $items = $order->getItems();
    foreach ($items as $item) {
      $purchased = $item->getPurchasedEntity();
      if ($purchased instanceof ProductVariationInterface) {
        $product = $purchased->getProduct();
        if ($product && $product->bundle() === 'ahs_course_materials') {
          $experienceIds = array_column($product->get('field_experiences_given')->getValue(), 'target_id');
          $fields = [
            'uid' => $customer->id(),
            'participant_type' => TrainingRecord::STUDENT_TERM_ID,
            'effective_time' => $order->getPlacedTime(),
            'note' => "From materials order " . $order->getOrderNumber(),
          ];
          TrainingRecord::createMultiple($experienceIds, $fields);
        }
      }
    }

  }

  /**
   * Notify student a book order has shipped.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function notifyStudent(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if ($order->bundle() !== 'ahs_free') {
      return;
    }
    if (!$customer = $order->getCustomer()) {
      return;
    }
    $message = Message::create([
      'template' => 'books_shipped',
    ]);
    $message->set('field_order', $order);
    $message->save();
    $this->messageNotify->send($message, ['mail' => $customer->getEmail(), 'reply' => 'support@ahs.org.uk'], 'ahs_email');

  }

}
