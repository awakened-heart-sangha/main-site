<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\Resolver\ChainStoreResolverInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Error;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\message\Entity\Message;
use Drupal\message_notify\MessageNotifier;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PlacedLahEnrolmentOrder. Provides subscriber for orders.
 */
class PlacedLahEnrolmentOrder implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The message notify.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotify;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * The chain store resolver service.
   *
   * @var \Drupal\commerce_store\Resolver\ChainStoreResolverInterface
   */
  protected $chainStore;

  /**
   * Constructs a new EventSubscriber.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, MessageNotifier $message_notify, LoggerChannelFactoryInterface $logger_factory, TimeInterface $time_service, ChainStoreResolverInterface $chain_store) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->messageNotify = $message_notify;
    $this->loggerFactory = $logger_factory->get('ahs_commerce');
    $this->timeService = $time_service;
    $this->chainStore = $chain_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => ['processOrder', 1],
    ];

    return $events;
  }

  /**
   * Notify to the customer about the new account.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function processOrder(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if ($order->bundle() === 'ahs_lah_enrolment') {
      $customer = $order->getCustomer();
      if ($customer && !$customer->isAnonymous()) {
        // Errors in these steps should not be fatal.
        try {
          $this->notifyDecoupledCustomer($customer);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

        try {
          $this->addCustomerToDhbGroup($customer);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

        try {
          $this->updateAboutProfile($customer, $order);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

        try {
          $this->acknowledgeStudent($customer, $order);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

        try {
          $this->notifyStudentSupport($customer, $order);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

        try {
          $this->orderMaterials($customer, $order);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

        try {
          $this->addToGettingStartedGroup($customer);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

        try {
          $this->makeMember($customer);
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('ahs_commerce');
          Error::logException($logger, $e);
        }

      }
    }
  }

  /**
   * Notify a decoupled user about the user account.
   *
   * We treat all decoupled users as if they're newly created, even
   * if they're not.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The enrolled user, the enrolment order customer.
   */
  public function notifyDecoupledCustomer(UserInterface $customer) {
    if ($customer->isDecoupled()) {
      // _user_mail_notify('register_admin_created', $customer);
    }
  }

  /**
   * Add user to DHB group.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The enrolled user, the enrolment order customer.
   */
  public function addCustomerToDhbGroup(UserInterface $customer) {
    $properties = ['type' => 'special', 'label' => 'Discovering the Heart of Buddhism'];
    $groupStorage = $this->entityTypeManager->getStorage('group');
    $groups = $groupStorage->loadByProperties($properties);
    // Get the first special group with the right title.
    $group = reset($groups);
    try {
      $group->addMember($customer);
    }
    catch (\Exception $e) {
      $logger = \Drupal::logger('ahs_commerce');
      Error::logException($logger, $e);
    }
  }

  /**
   * Update customer's about profile.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The enrolled user, the enrolment order customer.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The enrolment order.
   */
  public function updateAboutProfile(UserInterface $customer, OrderInterface $order) {
    $billingProfile = $order->getBillingProfile();
    if ($billingProfile instanceof ProfileInterface) {
      // Get the customer's about profile, or create it
      // if it does not yet exist.
      $profile_storage = $this->entityTypeManager->getStorage('profile');
      $aboutProfile = $profile_storage->loadByUser($customer, 'about');
      $needsSave = FALSE;
      if (!$aboutProfile instanceof ProfileInterface) {
        $aboutProfile = Profile::create(['type' => 'about', 'uid' => $customer->id()]);
        $aboutProfile->setPublished();
        $needsSave = TRUE;
      }

      // Update the address.
      if (!$billingProfile->get('address')->isEmpty()) {
        $address = $billingProfile->get('address')[0];
        $aboutProfile->field_name[0] = [
          'given' => $address->getGivenName(),
          'family' => $address->getFamilyName(),
        ];
        $needsSave = TRUE;
      }

      // Update the phone number.
      if (!$billingProfile->get('field_telephone')->isEmpty()) {
        $phone = $billingProfile->get('field_telephone')[0]->value;
        if (substr($phone, 0, 2) === '07') {
          $aboutProfile->set('field_telephone_mobile', $phone);
        }
        else {
          $aboutProfile->set('field_telephone_home', $phone);
        }
        $needsSave = TRUE;
      }

      // Save the about profile if it is new or updated.
      if ($needsSave) {
        $aboutProfile->save();
      }
    }
  }

  /**
   * Send an acknowledgement email to the new student.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The enrolled user, the enrolment order customer.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The enrolment order.
   */
  public function acknowledgeStudent(UserInterface $customer, OrderInterface $order) {
    $message = Message::create([
      'template' => 'enrolment_to_student',
    ]);
    $message->set('field_student', $customer);
    $message->set('field_order', $order);
    $message->set('arguments', [
      '@one-time-login-url' => [
        'callback' => 'user_pass_reset_url',
        'arguments' => [$customer, []],
      ],
    ]);
    $message->save();
    $this->messageNotify->send($message, ['mail' => $customer->getEmail(), 'reply' => 'support@ahs.org.uk'], 'ahs_email');
  }

  /**
   * Notify student support about the enrolment.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The enrolled user, the enrolment order customer.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The enrolment order.
   */
  public function notifyStudentSupport(UserInterface $customer, OrderInterface $order) {
    $message = Message::create([
      'template' => 'enrolment_to_support',
    ]);
    $message->set('field_student', $customer);
    $message->set('field_order', $order);
    $message->save();
    $this->messageNotify->send($message, ['mail' => 'support@ahs.org.uk']);
  }

  /**
   * Order initial materials for the enrollee.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The enrolled user, the enrolment order customer.
   * @param \Drupal\commerce_order\Entity\OrderInterface $enrolmentOrder
   *   The enrolment order.
   */
  public function orderMaterials(UserInterface $customer, OrderInterface $enrolmentOrder) {
    $initialMaterialsSkus = ['book_hom', 'book_ocs', 'book_dhb_course_companion'];
    $items = [];
    $productVariationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');
    foreach ($initialMaterialsSkus as $sku) {
      $variation = $productVariationStorage->loadBySku($sku);
      if (!$variation) {
        $this->loggerFactory->error("Could not load variation with sku '$sku'");
      }
      if ($variation && !$this->hasUserOrderedMaterial($customer, $variation)) {
        $orderItem = OrderItem::create([
          'type' => 'default',
          'quantity' => 1,
          'unit_price' => new Price(0, 'GBP'),
          'purchased_entity' => $variation,
          'overridden_unit_price' => TRUE,
        ]);
        $orderItem->save();
        $items[] = $orderItem;
      }
    }

    if (!empty($items)) {
      $materialsOrder = Order::create([
        'type' => 'ahs_free',
      ]);
      $materialsOrder->setStore($this->chainStore->resolve());
      $materialsOrder->setCustomer($customer);
      $materialsOrder->setItems($items);
      if ($enrolmentOrder->getBillingProfile()) {
        $profile = $enrolmentOrder->getBillingProfile();
        $profile = clone $profile;
        $materialsOrder->setBillingProfile($profile);
      }
      $materialsOrder->save();
      $materialsOrder->getState()->applyTransitionById('place');
      $materialsOrder->save();
    }
  }

  /**
   * Helper to check for material.
   */
  protected function hasUserOrderedMaterial(UserInterface $user, PurchasableEntityInterface $material) {
    $query = $this->database->select('commerce_order', 'co');
    $query->leftjoin('commerce_order_item', 'coi', 'co.order_id = coi.order_id');
    $query->fields('co', ['order_id']);
    $query->condition('co.state', ['draft', 'canceled'], 'NOT IN');
    $query->condition('co.uid', $user->id());
    $query->condition('coi.purchased_entity', $material->id());
    $result = $query->distinct()->countQuery()->execute()->fetchField();
    $hasMaterial = ((int) $result) > 0;
    return $hasMaterial;
  }

  /**
   * Create a 'Getting Started' course for the student and add them to it.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The enrolled user, the enrolment order customer.
   */
  public function addToGettingStartedGroup(UserInterface $customer) {
    // This logic is all copied from GroupCloneForm,
    // as unfortunately entity_clone does not have an API.
    $template = $this->entityTypeManager->getStorage('group')->load(592);
    if (!$template) {
      $this->loggerFactory->error("Could not load 'Getting started' template 592");
      return;
    }
    $course = $this->entityTypeManager->getStorage('group')->create(['type' => 'course']);
    $excludedFields = ['id', 'uuid', 'type', 'revision_id'];
    $sourceFields = $template->getFields(FALSE);
    foreach ($sourceFields as $fieldId => $fieldItems) {
      if (!in_array($fieldId, $excludedFields) && $course->hasField($fieldId)) {
        $course->set($fieldId, $fieldItems->getValue());
      }
    }

    $course->set('field_private', TRUE);
    $course->set('field_self_paced', TRUE);
    $course->set('field_registration_open', FALSE);
    $course->set('status', TRUE);
    $course->set('field_template', $template);

    // Set start and end dates.
    $timestamp = $this->timeService->getRequestTime();
    $start = DrupalDateTime::createFromTimestamp($timestamp);
    $days = ((int) $template->get('field_expected_weeks')->value * 7);
    $interval = new \DateInterval("P{$days}D");
    $end = clone $start;
    $end = $end->add($interval);
    $value = [
      'value' => $start->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $end->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
    ];
    $course->set('field_dates', $value);

    // Specify that the template content should be cloned recursively.
    $properties = [];
    foreach ($template->get('field_content')->referencedEntities() as $para) {
      $properties['recursive']['group.course_template.field_content']['references'][$para->id()] = [
        'clone' => TRUE,
        'target_entity_type_id' => 'paragraph',
        'target_bundle' => $para->bundle(),
      ];
    }

    // Add the customer to the course.
    $properties['uids'] = [$customer->id()];
    $properties['no_suffix'] = (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting();

    $entity_clone_handler = $this->entityTypeManager->getHandler('group', 'entity_clone');
    $course = $entity_clone_handler->cloneEntity($template, $course, $properties);
  }

  /**
   * Assume the user should be a member.
   *
   */
  public function makeMember(UserInterface $customer) {
    $customer->addRole('member');
    $customer->save();
  }

}
