<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\commerce_payment\Event\PaymentEvent;
use Drupal\commerce_payment\Event\PaymentEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PaymentEventLogSubscriber. Provides subscriber for payments.
 *
 * This is copy and pasted from www.drupal.org/project/commerce/issues/2845321
 * but with the log template names prefixed with ahs_commerce.
 *
 * A PAYMENT_CREATE log was also added to record the initiation
 * of a payment attempt.
 */
class PaymentEventLogSubscriber implements EventSubscriberInterface {

  /**
   * The log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * Constructs a new PaymentEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->logStorage = $entity_type_manager->getStorage('commerce_log');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      PaymentEvents::PAYMENT_CREATE => ['onPaymentCreate', -100],
      PaymentEvents::PAYMENT_INSERT => ['onPaymentAdd', -100],
      PaymentEvents::PAYMENT_UPDATE => ['onPaymentUpdate', -100],
      PaymentEvents::PAYMENT_PREDELETE => ['onPaymentDelete', -100],
    ];
    return $events;
  }

  /**
   * Creates a log when a payment is initiated.
   *
   * @param \Drupal\commerce_payment\Event\PaymentEvent $event
   *   The payment event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onPaymentCreate(PaymentEvent $event) {
    $payment = $event->getPayment();
    $properties = [];
    if (!is_null($payment->getAmount())) {
      $properties['amount'] = $payment->getAmount();
    }
    if ($payment->getOrder()) {
      $this->logStorage->generate($payment->getOrder(), 'ahs_commerce_payment_created', $properties)
        ->save();
    }
  }

  /**
   * Creates a log when a payment is added.
   *
   * @param \Drupal\commerce_payment\Event\PaymentEvent $event
   *   The payment event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onPaymentAdd(PaymentEvent $event) {
    $payment = $event->getPayment();
    if ($order = $payment->getOrder()) {
      $this->logStorage->generate($order, 'ahs_commerce_payment_added', [
        'id' => $payment->id(),
        'remote_id' => $payment->getRemoteId(),
        'amount' => $payment->getAmount(),
        'state' => $payment->getState()->getLabel(),
      ])->save();
    }
  }

  /**
   * Creates a log when a payment is updated.
   *
   * @param \Drupal\commerce_payment\Event\PaymentEvent $event
   *   The payment event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onPaymentUpdate(PaymentEvent $event) {
    $payment = $event->getPayment();
    if ($order = $payment->getOrder()) {
      $this->logStorage->generate($order, 'ahs_commerce_payment_updated', [
        'id' => $payment->id(),
        'remote_id' => $payment->getRemoteId(),
        'amount' => $payment->getBalance(),
        'state' => $payment->getState()->getLabel(),
      ])->save();
    }
  }

  /**
   * Creates a log when a payment is deleted.
   *
   * @param \Drupal\commerce_payment\Event\PaymentEvent $event
   *   The payment event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onPaymentDelete(PaymentEvent $event) {
    $payment = $event->getPayment();
    // Capture payment method information for deleted payment.
    $payment_method_label = NULL;
    if ($payment_method = $payment->getPaymentMethod()) {
      $payment_method_label = $payment_method->label();
    }
    if ($order = $payment->getOrder()) {
      $this->logStorage->generate($order, 'ahs_commerce_payment_deleted', [
        'id' => $payment->id(),
        'remote_id' => $payment->getRemoteId(),
        'amount' => $payment->getBalance(),
        'payment_method_label' => $payment_method_label,
      ])->save();
    }
  }

}
