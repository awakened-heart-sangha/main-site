<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\Error;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PlacedMembershipOrder. Provides subscriber for orders.
 */
class PlacedMembershipOrder implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * Constructs a new EventSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time_service
   *   The time interface.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TimeInterface $time_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->timeService = $time_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => ['startMembership', 100],
    ];
    return $events;
  }

  /**
   * Grant the member role if a placed order contains the membershp product.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function startMembership(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if ($order->bundle() == 'recurring') {
      return;
    }

    // Errors should not be fatal.
    try {
      $customer = $order->getCustomer();
      if ($customer && !$customer->isAnonymous()) {
        if ($this->isMembershipOrder($order)) {
          $customer->addRole('member');
          $customer->save();
        }
      }
    }
    catch (\Exception $e) {
      $logger = \Drupal::logger('ahs_commerce');
      Error::logException($logger, $e);
    }
  }

  /**
   * See if an order contains the membership product.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  protected function isMembershipOrder(OrderInterface $order) {
    // Get the membership product.
    $sku = 'ahs_membership_monthly';
    $variationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');
    $variations = $variationStorage->loadByProperties(['sku' => $sku]);
    // There should only be 1 variation with this sku.
    $variation = reset($variations);
    if (!$variation) {
      return;
    }
    $membershipProductId = $variation->getProductId();

    // See if the order contains the membership product.
    $items = $order->getItems();
    foreach ($items as $item) {
      $purchased = $item->getPurchasedEntity();
      if ($purchased instanceof ProductVariationInterface) {
        $productId = $purchased->getProductId();
        if ($productId === $membershipProductId) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
