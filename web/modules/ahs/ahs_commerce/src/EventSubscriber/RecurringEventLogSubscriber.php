<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\advancedqueue\Event\AdvancedQueueEvents;
use Drupal\advancedqueue\Event\JobEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Log the results of cmmerce recurring queue jobs.
 */
class RecurringEventLogSubscriber implements EventSubscriberInterface {

  /**
   * The log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * The log storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RecurringEventLogSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logStorage = $entity_type_manager->getStorage('commerce_log');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      AdvancedQueueEvents::POST_PROCESS => ['onPostProcess'],
    ];
    return $events;
  }

  /**
   * Creates a log when after a commerce recurring job is processed.
   *
   * @param \Drupal\advancedqueue\Event\JobEvent $event
   *   The job event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onPostProcess(JobEvent $event) {
    $job = $event->getJob();
    if ($job->getType() !== 'commerce_recurring_order_close') {
      return;
    }

    $payload = $job->getPayload();
    try {
      $order = $this->entityTypeManager
        ->getStorage('commerce_order')
        ->load($payload['order_id']);
      if (empty($order)) {
        return;
      }
    }
    catch (\Throwable $e) {
      return;
    }

    $state = $job->getState();
    $properties = [
      'state' => ucfirst($state),
      'attempt' => $job->getNumRetries() + 1,
    ];
    if (!empty($job->getMessage())) {
      $properties['message'] = $job->getMessage();
    }

    $this->logStorage->generate($order, 'ahs_commerce_recurring_post_job', $properties)->save();
  }

}
