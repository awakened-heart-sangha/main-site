<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\commerce_recurring\Event\PaymentDeclinedEvent;
use Drupal\commerce_recurring\Event\RecurringEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\message\Entity\Message;
use Drupal\message_notify\MessageNotifier;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides subscriber for payments.
 */
class RecurringPaymentDeclinedDunningSubscriber implements EventSubscriberInterface {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * The message notify.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotify;

  /**
   * Constructs a new RecurringPaymentDeclinedDunningSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\message_notify\MessageNotifier $message_notify
   *   The message notifier.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessageNotifier $message_notify) {
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->logStorage = $entity_type_manager->getStorage('commerce_log');
    $this->messageNotify = $message_notify;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      RecurringEvents::PAYMENT_DECLINED => ['onRecurringPaymentDeclined'],
    ];
    return $events;
  }

  /**
   * Creates a log when a recurring payment is declined.
   *
   * @param \Drupal\commerce_recurring\Event\PaymentDeclinedEvent $event
   *   The payment event.
   */
  public function onRecurringPaymentDeclined(PaymentDeclinedEvent $event) {
    $order = $event->getOrder();
    // Don't dunn cancelled orders.
    if ($order->getState()->getId() !== 'needs_payment') {
      return;
    }

    $message = Message::create([
      'template' => 'subscription_payment_dunning',
    ]);
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->userStorage->load($order->getCustomerId());
    $retry_url = Url::fromRoute('commerce_recurring.payment_retry', [
      'commerce_order' => $order->id(),
    ], ['absolute' => TRUE])->toString(TRUE)->getGeneratedUrl();

    $message->set('field_order', $order);
    $message->set('field_update_payment_method_link', [
      'title' => 'Payment method link',
      'uri' => $retry_url,
    ]);
    $message->save();
    $this->messageNotify->send($message, ['mail' => $user->getEmail()], 'email');

    $properties = [
      'email' => $user->getEmail(),
      'url' => $retry_url,
    ];
    $this->logStorage->generate($order, 'ahs_commerce_recurring_dunning_sent', $properties)->save();
  }

}
