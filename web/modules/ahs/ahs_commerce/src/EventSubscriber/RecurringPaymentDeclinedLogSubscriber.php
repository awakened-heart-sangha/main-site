<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\commerce_recurring\Event\PaymentDeclinedEvent;
use Drupal\commerce_recurring\Event\RecurringEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides subscriber for payments.
 */
class RecurringPaymentDeclinedLogSubscriber implements EventSubscriberInterface {

  /**
   * The log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * Constructs a new PaymentEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->logStorage = $entity_type_manager->getStorage('commerce_log');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      RecurringEvents::PAYMENT_DECLINED => ['onRecurringPaymentDeclined'],
    ];
    return $events;
  }

  /**
   * Creates a log when a recurring payment is declined.
   *
   * @param \Drupal\commerce_recurring\Event\PaymentDeclinedEvent $event
   *   The payment event.
   */
  public function onRecurringPaymentDeclined(PaymentDeclinedEvent $event) {
    $this->logStorage->generate($event->getOrder(), 'ahs_commerce_recurring_payment_declined', [
      'attempt' => $event->getNumRetries() + 1,
      'max_attempts' => $event->getMaxRetries() + 1,
      'retry_days' => $event->getRetryDays(),
    ])->save();
  }

}
