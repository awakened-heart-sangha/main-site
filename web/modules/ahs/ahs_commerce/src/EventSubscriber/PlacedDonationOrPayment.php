<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Error;
use Drupal\group\Entity\Group;
use Drupal\message\Entity\Message;
use Drupal\message_notify\MessageNotifier;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PlacedMiscDonationPayment. Provides subscriber for orders.
 * 
 * This sends thankyou for miscellaneous and registration donations & payments.
 */
class PlacedDonationOrPayment implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The message notify.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotify;

  /**
   * Constructs a new PlacedMiscDonationPayment.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, MessageNotifier $message_notify) {
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $logger_factory->get('ahs_commerce');
    $this->messageNotify = $message_notify;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => 'sendThankyouMessage',
    ];

    return $events;
  }

  /**
   * Send the customer a thank you message.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function sendThankyouMessage(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

    if (!in_array($order->bundle(), ['ahs_miscellaneous', 'ahs_registration'])) {
      return;
    }

    if (is_null($order->getTotalPaid()) || $order->getTotalPaid()->isZero()) {
      return;
    }

    // Errors should not be fatal.
    try {
      $items = $order->getItems();
      $item = $items['0'];
      $sku = $item->getPurchasedEntity()->getSku();

      $group = NULL;
      if ($item->hasField('field_group') and $gid = $item->get('field_group')->getString()) {
        $group = Group::load($gid);
      }

      if ($sku === 'misc_donation_miscellaneous') {
        $this->sendMiscDonationThankyou($order);
      }
      elseif ($sku === 'misc_payment_event' || strpos($sku, 'reg_payment') === 0) {
        $this->sendEventThankyou('misc_event_payment_thankyou', $order, $group);
      }
      elseif (
        $sku === 'misc_donation_event_hermitage' ||
        $sku === 'misc_donation_event_online' ||
        $sku === 'misc_donation_course' ||
        strpos($sku, 'reg_donation') === 0
      ) {
        $this->sendEventThankyou('event_donation_thankyou', $order, $group);
      }
    }
    catch (\Exception $e) {
      $logger = \Drupal::logger('ahs_commerce');
      Error::logException($logger, $e);
    }
  }

  /**
   * Send thankyou message to customer who made a donation.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The miscellaneous order.
   */
  public function sendMiscDonationThankyou(OrderInterface $order) {
    $message = Message::create([
      'template' => 'misc_donation_thankyou',
    ]);
    $user = $order->getCustomer();
    $email = $user && !$user->isAnonymous() && $user->getEmail() ? $user->getEmail() : $order->getEmail();
    if (!$email) {
      $this->loggerFactory->warning('No customer email available for order ' . $order->id());
    }
    else {
      $items = $order->getItems();
      $item = $items['0'];
      $message->set('field_order', $order);
      $message->set('field_student', $user);
      if ($item->hasField('field_notes')) {
        if ($notes = $item->get('field_notes')->getString()) {
          $message->set('field_order_notes', $notes);
        }
      }
      $message->save();
      $this->messageNotify->send($message, ['mail' => $email], 'email');
    }
  }

  /**
   * Send thankyou message to customer who made a payment.
   *
   * @param string $template_id
   *   The message template ID.   * 
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The enrolment order.
   * @param \Drupal\group\Entity\Group $group
   *   The enrolment group.
   */
  public function sendEventThankyou($template_id, OrderInterface $order, $group) {
    $message = Message::create([
      'template' => $template_id,
    ]);
    $user = $order->getCustomer();
    $email = $user && !$user->isAnonymous() && $user->getEmail() ? $user->getEmail() : $order->getEmail();
    if (!$email) {
      $this->loggerFactory->warning('No customer email available for order ' . $order->id());
    }
    else {
      $items = $order->getItems();
      $item = $items['0'];
      $message->set('field_group', $group);
      $message->set('field_order', $order);
      $message->set('field_student', $user);
      if ($item->hasField('field_notes')) {
        if ($notes = $item->get('field_notes')->getString()) {
          $message->set('field_order_notes', $notes);
        }
      }
      $message->save();
      $this->messageNotify->send($message, ['mail' => $user->getEmail()], 'email');
    }
  }

}
