<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OrderTransitionLogSubscriber. Provides subscriber for orders.
 */
class OrderTransitionLogSubscriber implements EventSubscriberInterface {

  /**
   * The log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * The log storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OrderTransitionLogSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logStorage = $entity_type_manager->getStorage('commerce_log');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = 'onPlace';
    $events['commerce_order.cancel.post_transition'] = 'onCancel';
    $events['commerce_order.mark_paid.post_transition'] = 'onMarkPaid';
    $events['commerce_order.mark_failed.post_transition'] = 'onMarkFailed';
    return $events;
  }

  /**
   * Log order placed.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onPlace(WorkflowTransitionEvent $event) {
    $this->logTransition($event, 'placed');
  }

  /**
   * Log order cancelled.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onCancel(WorkflowTransitionEvent $event) {
    $this->logTransition($event, 'cancelled');
  }

  /**
   * Log order marked paid.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onMarkPaid(WorkflowTransitionEvent $event) {
    $this->logTransition($event, 'marked as paid');
  }

  /**
   * Log order marked failed.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onMarkFailed(WorkflowTransitionEvent $event) {
    $this->logTransition($event, 'marked as failed');
  }

  /**
   * Helper to add log.
   */
  public function logTransition(WorkflowTransitionEvent $event, $transition) {
    $properties = [
      'transition' => $transition,
    ];
    $this->logStorage->generate($event->getEntity(), 'ahs_commerce_order_transition', $properties)->save();
  }

}
