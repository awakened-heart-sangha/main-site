<?php

namespace Drupal\ahs_commerce\EventSubscriber;

use Drupal\commerce_stripe\Event\PaymentIntentEvent;
use Drupal\commerce_stripe\Event\StripeEvents;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class StripeTransactionData. Provides subscriber for payments.
 */
class StripeTransactionData implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EventSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      StripeEvents::PAYMENT_INTENT_CREATE => 'paymentIntent',
    ];
  }

  /**
   * Set metadata and description on stripe charges.
   *
   * @param \Drupal\commerce_stripe\Event\PaymentIntentEvent $event
   *   The payment intent event.
   */
  public function paymentIntent(PaymentIntentEvent $event) {
    $order = $event->getOrder();
    $intent_attributes = $event->getIntentAttributes();

    // Prepare metadata about the order item.
    // There is  a 500 character limit.
    $itemsMetadata = [];
    if ($order->hasItems()) {
      $items = $order->getItems();
      foreach ($items as $item) {
        $itemMetadata = [];
        $itemMetadata['id'] = $item->id();
        if ($purchased = $item->getPurchasedEntity()) {
          $purchased = $item->getPurchasedEntity();
          if ($purchased) {
            $itemMetadata['sku'] = $purchased->getSku();
          }
          /*          if ($purchased instanceof ProductVariationInterface) {
          $product = $purchased->getProduct();
          if ($product) {
          $itemMetadata['product_label'] = $product->label();
          $itemMetadata['product_id'] = $product->id();
          $itemMetadata['product_bundle'] = $product->bundle();
          }
          }*/
        }
        if ($item->hasField('field_notes')) {
          $itemMetadata['notes'] = $item->field_notes->value;
        }
        if ($item->hasField('field_group') && !$item->get('field_group')->isEmpty()) {
          $itemMetadata['group'] = $item->field_group->target_id;
        }
        // Else try to get group from the product.
        elseif ($item->hasPurchasedEntity()) {
          $purchased_entity = $item->getPurchasedEntity();
          if ('commerce_product_variation' == $purchased_entity->getEntityTypeId() && $product = $purchased_entity->getProduct()) {
            if ($product->hasField('field_group') && !$product->get('field_group')->isEmpty()) {
              $itemMetadata['group'] = $product->get('field_group')->target_id;
            }
          }
        }
        $itemsMetadata[] = $itemMetadata;
      }
    }

    // Set the Stripe metadata.
    $intent_attributes['metadata']['drupal_commerce_order_id'] = $order->id();
    // Avoid breaking the payment if there are a lot of items.
    // Stripe has a 500 character limit.
    $jsonItemsMetadata = Json::encode($itemsMetadata);
    if (strlen($jsonItemsMetadata) < 500) {
      $intent_attributes['metadata']['drupal_commerce_order_items'] = $jsonItemsMetadata;
    }

    $event->setIntentAttributes($intent_attributes);
  }

}
