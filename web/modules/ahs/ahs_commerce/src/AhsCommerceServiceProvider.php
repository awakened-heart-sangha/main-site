<?php

namespace Drupal\ahs_commerce;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the checkout order manager service.
 */
class AhsCommerceServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // We handle the dunning messages ourselves at
    // ahs_commerce.recurring_payment_declined_dunning_subscriber.
    $container->removeDefinition('commerce_recurring.event_subscriber.dunning_subscriber');
  }

}
