<?php

namespace Drupal\ahs_commerce;

use Drupal\commerce_payment\PaymentMethodListBuilder as ParentListBuilder;

/**
 * Don't allow ordinary users to manage their payment methods after checkout.
 *
 * Defines the list builder for payment methods.
 */
class PaymentMethodListBuilder extends ParentListBuilder {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    if (\Drupal::currentUser()->hasPermission('administer commerce_payment_method')) {
      $route = \Drupal::service('current_route_match');
      $user = $route->getParameter('user');
      $build['activity'] = [
        '#type' => 'view',
        '#name' => 'commerce_activity',
        '#display_id' => 'payment_method_history',
        '#arguments' => [$user->id(), 'user'],
        '#embed' => TRUE,
        '#title' => $this->t('Payment method history'),
      ];
    }
    return $build;
  }

}
