<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

/**
 * Change payment gateway mode to live on commerce payments.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_commerce_deploy_0001_change_payment_mode(&$sandbox) {
  \Drupal::database()->update('commerce_payment')
    ->fields(['payment_gateway_mode' => 'live'])
    ->execute();
  \Drupal::database()->update('commerce_payment_method')
    ->fields(['payment_gateway_mode' => 'live'])
    ->execute();
}

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

/**
 * Change payment gateway mode to live on users.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_commerce_deploy_0002_change_customer_mode(&$sandbox) {
  \Drupal::database()->update('user__commerce_remote_id')
    ->fields(['commerce_remote_id_provider' => 'shrimala_trust_stripe|live'])
    ->execute();
}
