<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Create registration variations for coming events.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_registration_deploy_0001_create_variations(&$sandbox) {
  $jobsLoader = function () {
    $groupQuery = \Drupal::entityTypeManager()->getStorage('group')->getQuery()->accessCheck(FALSE);
    $groupQuery->condition('type', 'event');
    $groupQuery->condition('field_registration_open', TRUE);
    $groupQuery->condition('status', TRUE);
    $time = \Drupal::time()->getCurrentTime();
    $date = DrupalDateTime::createFromTimestamp($time);
    $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $date = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $groupQuery->condition('field_dates.end_value', $date, '>');
    return $groupQuery->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);
      $handler = \Drupal::entityTypeManager()->getHandler('registration', 'host_entity');

      $online_variation = ProductVariation::create([
        'type' => 'ahs_registration',
        'field_registration_type' => 'online',
        'field_group' => $group,
        'title' => 'Online',
        'price' => new Price(0, 'GBP'),
      ]);
      $online_variation->save();
      $host = $handler->createHostEntity($online_variation);
      $settings = $host->getSettings();
      $open = (bool) $group->get('field_registration_open')->value;
      $settings->set('status', $open);
      if (!$group->get('field_registration_deadline')->isEmpty()) {
        $deadline = $group->get('field_registration_deadline')->value;
        $settings->set('close', $deadline);
      }
      $settings->save();
      $variations = [$online_variation];

      if ($group->get('field_venue')->target_id == 2139) {
        $hermitage_variation = ProductVariation::create([
          'type' => 'ahs_registration',
          'field_registration_type' => 'in_person_hermitage',
          'title' => 'In-person',
          'price' => new Price(0, 'GBP'),
        ]);
        $hermitage_variation->save();
        $host = $handler->createHostEntity($hermitage_variation);
        $settings = $host->getSettings();
        $open = (bool) $group->get('field_registration_open')->value;
        $settings->set('status', $open);
        if (!$group->get('field_registration_deadline')->isEmpty()) {
          $deadline = $group->get('field_registration_deadline')->value;
          $settings->set('close', $deadline);
        }
        $settings->save();
        $variations[] = $hermitage_variation;
      }

      $group->set('field_registration_variations', $variations);
      $group->save();

    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_registration')
        ->error("Error creating registration variations " . $id . " " . $group->label() . " : " . $e->getMessage() . ' ' . $e->getTraceAsString());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'groups variations created');
}

/**
 * Fill in the registration_system field.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_registration_deploy_0002_registration_system(&$sandbox) {
  $jobsLoader = function () {
    $groupQuery = \Drupal::entityTypeManager()->getStorage('group')->getQuery()->accessCheck(FALSE);
    $groupQuery->condition('type', 'event');
    return $groupQuery->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);
      $group->set('field_registration_system', 'manual');
      if (!$group->get('field_registration_normal')->isEmpty() && $group->get('field_registration_normal')->value) {
        $group->set('field_registration_system', 'group');
      }
      $group->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_registration')
        ->error("Error setting default registration system " . $id . " " . $group->label() . " : " . $e->getMessage() . ' ' . $e->getTraceAsString());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'groups registration system set');
}

/**
 * Resave registration products to update variation titles.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_registration_deploy_0003_resave_registration_products(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('commerce_product')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'ahs_registration')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $product = \Drupal::entityTypeManager()->getStorage('commerce_product')->load($id);
      $product->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_registration')
        ->error("Error resaving product " . $id . " " . $product->label() . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'products resaved');
}

/**
 * Resave registrations to update field_group.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_registration_deploy_0004_resave_registrations(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('registration')
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $registration = \Drupal::entityTypeManager()->getStorage('registration')->load($id);
      $registration->save();
    }
    catch (\Throwable $e) {
      $label = $registration ? $registration->label() : 'cannot load';
      \Drupal::logger('ahs_registration')
        ->error("Error resaving registration " . $id . " " . $label . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'registrations resaved');
}

/**
 * Resave registrations to handle nones.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_registration_deploy_0005_resave_registrations(&$sandbox) {
  $jobsLoader = function() {
    return \Drupal::entityTypeManager()->getStorage('registration')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'in_person_hermitage')
      ->execute();
  };

  $jobProcessor = function($id) {
    try {
      $registration = \Drupal::entityTypeManager()->getStorage('registration')->load($id);
      $registration->save();
    }
    catch (\Throwable $e) {
      $label = $registration ? $registration->label() : 'cannot load';
      \Drupal::logger('ahs_registration')
        ->error("Error resaving registration " . $id . " " . $label . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'registrations resaved');
}


/**
 * Resave profiles to handle nones.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_registration_deploy_0006_resave_profiles(&$sandbox) {
  $jobsLoader = function() {
    return \Drupal::entityTypeManager()->getStorage('profile')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'in_person_participation')
      ->execute();
  };

  $jobProcessor = function($id) {
    try {
      $registration = \Drupal::entityTypeManager()->getStorage('profile')->load($id);
      $registration->save();
    }
    catch (\Throwable $e) {
      $label = $registration ? $registration->label() : 'cannot load';
      \Drupal::logger('ahs_registration')
        ->error("Error resaving profile " . $id . " " . $label . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'profiles resaved');
}

/**
 * Provide default value for payment type in legacy ahs_registration product variations.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_registration_deploy_0007_set_donation_suggested(&$sandbox) {
  $jobsLoader = function() {
    return \Drupal::entityTypeManager()->getStorage('commerce_product_variation')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'ahs_registration')
      ->execute();
  };

  $jobProcessor = function($id) {
    try {
      $variation = \Drupal::entityTypeManager()->getStorage('commerce_product_variation')->load($id);
      $variation->set('field_payment_type', 'donation');
      $variation->save();
    }
    catch (\Throwable $e) {
      $label = $variation ? $variation->label() : 'cannot load';
      \Drupal::logger('ahs_registration')
        ->error("Error resaving product variation " . $id . " " . $label . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'product variations updated');
}
