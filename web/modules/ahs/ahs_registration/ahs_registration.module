<?php

/**
 * @file
 * Contains ahs_registration.module.
 */

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\ahs_registration\Entity\AhsRegistrationVariation;
use Drupal\ahs_registration\EntityClone\ProductEntityClone;
use Drupal\ahs_registration\EntityClone\ProductVariationEntityClone;
use Drupal\ahs_registration\EntityClone\ProductVariationEntityCloneForm;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_clone\EntityClone\Content\ContentEntityCloneFormBase;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupRelationship;
use Drupal\registration\Entity\Registration;
use Drupal\registration\Entity\RegistrationInterface;
use Drupal\registration\Entity\RegistrationSettings;
use Drupal\user\Entity\User;

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 */
function ahs_registration_inline_entity_form_entity_form_alter(&$entity_form, &$form_state) {
  // Showing registration settings in IEF on product variation entity.
  // Fill in required backreference fields to parent (host)
  // on registration settings.
  if ($entity_form['#entity_type'] === 'registration_settings') {
    // The entity_id and entity_type_id is required, but the parent id
    // is not yet known on a new parent, so use a temporary value until
    // overridden by ahs_registration_commerce_product_variation_insert().
    // Use a random value to avoid collision with other
    // similarly created entities, and add a large number to avoid
    // collision with real permanent ids.
    if ($entity_form['#entity']->get('entity_type_id')->isEmpty()) {
      $entity_form['#entity']->set('entity_type_id', 'commerce_product_variation');
    }
    if ($entity_form['#entity']->get('entity_id')->isEmpty()) {
      $entity_form['#entity']->set('entity_id', 10000000 + mt_rand());
    }
  }

  // Showing registration variations in IEF on group entity.
  // Fill in  backreference to group on registration variations.
  if ($entity_form['#entity_type'] === 'commerce_product_variation') {
    if ($entity_form['#entity']->bundle() === 'ahs_registration') {
      $parent_entity = $form_state->getFormObject()->getEntity();
      // A backreference is only possible if the group is not new.
      if ($parent_entity->getEntityTypeId() === 'group' && !$parent_entity->isNew()) {
        $entity_form['#entity']->set('field_group', $parent_entity->id());
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function ahs_registration_registration_presave(RegistrationInterface $registration) {
  // When staff create registrations not using checkout,
  // they are always complete.
  if ($registration->getState()->id() == 'pending' && $registration->get('order_id')->isEmpty()) {
    $registration->set('state', 'complete');
    $registration->set('completed', \Drupal::time()->getRequestTime());
  }
  // Store a reference to the host group on the registration.
  // This is a shortcut for the group->product->product_variation host chain.
  if ($registration->hasField('field_group') && $host = $registration->getHostEntity()->getEntity()) {
    if ($host instanceof AhsRegistrationVariation) {
      $registration->set('field_group', $host->getGroup());
    }
  }
  _ahs_registration_handle_nones(['field_life_situations', 'field_health_information'], $registration);
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function ahs_registration_group_insert(AhsGroup $group) {
  _ahs_registration_product_group_backreference($group);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function ahs_registration_group_update(AhsGroup $group) {
  _ahs_registration_product_group_backreference($group);
}

/**
 * Provides a helper to add references between product and group.
 */
function _ahs_registration_product_group_backreference(AhsGroup $group) {
  if ($group->hasField('field_product')) {

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    foreach ($group->get('field_product')->referencedEntities() as $product) {
      if ($product->hasField('field_group')) {
        $group_id = $product->get('field_group')->getValue();
        if (!$group_id || $group->id() != $group_id[0]['target_id']) {
          $product->set('field_group', $group->id());
          $product->save();
        }
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function ahs_registration_profile_presave(\Drupal\profile\Entity\ProfileInterface $profile) {
  _ahs_registration_handle_nones(['field_food_allergies', 'field_food_intolerances', 'field_food_preferences'], $profile);
}

/**
 * Treat certain text values as NULLs.
 *
 * Intended to be called from hook_ENTITY_TYPE_presave().
 *
 * @param array $field_names
 *   Names of fields to receive this treatment. Must be single-value.
 * @param $entity
 *   The entity.
 */
function _ahs_registration_handle_nones(array $field_names, $entity) {
  foreach($field_names as $field_name) {
    if ($entity->hasField($field_name)) {
      // Specify strings to treat as NULL.
      $null_equivalents = ['none', 'na', 'nothing', 'no', 'not', 'vegetarian', 'veggie', 'any', 'anything', 'all', 'unrestricted'];
      // Remove punctuation, whitespace, and convert to lowercase in a UTF-8-aware manner
      $field_value = $entity->get($field_name)->value ?? '';
      $field_value = mb_strtolower(preg_replace("/\W|\s/u", "", $field_value), 'UTF-8');
      if (in_array($field_value, $null_equivalents, TRUE)) {
        $entity->set($field_name, NULL);
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_create() for registration_settings entity.
 */
function ahs_registration_registration_settings_create(RegistrationSettings $settings) {
  // Default to enabled for new registration settings sets.
  $settings->set('status', TRUE);
}

/**
 * Implements hook_entity_type_build().
 */
function ahs_registration_entity_type_build(array &$entity_types) {
  /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */

  if (isset($entity_types['registration_settings'])) {
    $settings_type = $entity_types['registration_settings'];
    // A bug in entity_clone requires us to specify the form handler
    // if we set a custom clone handler.
    // @see https://www.drupal.org/project/entity_clone/issues/3212991
    $settings_type->setHandlerClass('entity_clone_form', ContentEntityCloneFormBase::class);
  }

  if (isset($entity_types['commerce_product_variation'])) {
    $variation_type = $entity_types['commerce_product_variation'];
    $variation_type->setHandlerClass('entity_clone', ProductVariationEntityClone::class);
    $variation_type->setHandlerClass('entity_clone_form', ProductVariationEntityCloneForm::class);
  }

  if (isset($entity_types['commerce_product'])) {
    $product_type = $entity_types['commerce_product'];
    $product_type->setHandlerClass('entity_clone', ProductEntityClone::class);
    $product_type->setHandlerClass('entity_clone_form', ContentEntityCloneFormBase::class);
  }
}

/**
 * Implements hook_entity_bundle_info_alter().
 */
function ahs_registration_entity_bundle_info_alter(array &$bundles): void {
  if (isset($bundles['commerce_product_variation']['ahs_registration'])) {
    $bundles['commerce_product_variation']['ahs_registration']['class'] = AhsRegistrationVariation::class;
  }
}

/**
 * Implements hook_webform_options_alter().
 *
 * Converts a suggested donation amount into a series of donations options.
 */
function ahs_registration_webform_options_alter(array &$options, array &$element): void {
  $id = $element['#webform_id'] ?? NULL;
  if (!in_array($id, ['register--donation_amount', 'register--payment_amount'])) {
    return;
  }

  // Show no options if no price specified or price zero.
  $oldOptions = $options;
  $options = [];
  $element['#default_value'] = '_other_';

  $variation = \Drupal::service('plugin.manager.webform.source_entity')->getSourceEntity('webform');
  if (!($variation instanceof ProductVariationInterface &&
    $variation->bundle() === 'ahs_registration')) {
    return;
  }

  if ($id === 'register--donation_amount') {
    if (!$variation->getPrice() || $variation->getPrice()->isZero()) {
      return;
    }

    $amount = (int) $variation->getPrice()->getNumber();
    $options = [];
    foreach ($oldOptions as $key => $text) {
      $proportion = ((int) $key) / 100;
      $newAmount = round(($amount * $proportion) / 5) * 5;
      $options[$newAmount] = '£' . $newAmount . '  ' . $text;
    }
    // The first option has the highest value.
    $element['#default_value'] = array_key_first($options);
  }


  if ($id === 'register--payment_amount') {
    $options = [];
    if ($variation->getListPrice() && !$variation->getListPrice()->isZero()) {
      $options[$variation->getListPrice()->getNumber()] = 'Full amount £' . Calculator::trim($variation->getListPrice()->getNumber());
      $element['#default_value'] = $variation->getListPrice()->getNumber();
      if ($variation->getPrice() && !$variation->getPrice()->isZero() && $variation->getListPrice()->getNumber() > $variation->getPrice()->getNumber()) {
        $options[$variation->getPrice()->getNumber()] = 'Deposit £' . Calculator::trim($variation->getPrice()->getNumber());
      }
    }
    elseif ($variation->getPrice() && !$variation->getPrice()->isZero()) {
      $options[$variation->getPrice()->getNumber()] = 'Deposit £' . Calculator::trim($variation->getPrice()->getNumber());
      $element['#default_value'] = $variation->getPrice()->getNumber();
    }
  }

}

/**
 * Implements hook_cron().
 */
function ahs_registration_cron() {
  // Delete confidential information 30 days after departure.
  $date_threshold = new DateTime();
  $date_threshold->modify('-30 days');
  $formatted_date = $date_threshold->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

  $fields = ['field_health_information', 'field_life_situations'];
  foreach ($fields as $field) {
    // Find registrations for finished events.
    $query = \Drupal::entityTypeManager()->getStorage('registration')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('field_departure', $formatted_date, '<=')
      ->exists($field)
      ->condition($field, '', '<>');
    $registration_ids = $query->execute();
    $registrations = Registration::loadMultiple($registration_ids);

    // Wipe confidential information for those registrations.
    foreach ($registrations as $registration) {
      // We can wipe both fields at once,
      // to reduce the number of entity load/saves.
      $registration->set('field_health_information', NULL);
      $registration->set('field_life_situations', NULL);
      $registration->save();
    }
  }
}

/**
 * Implements hook_group_content_delete().
 *
 * Cancels registrations when a member is removed from a group.
 */
function ahs_registration_group_content_delete(GroupRelationship $entity) {
  if ($entity->getPluginId() === 'group_membership') {
    if ($entity->getGroup() && $entity->getEntity()) {
      $user = User::load($entity->getEntity()->id());
      $registrations = $entity->getGroup()->getRegistrations($user);
      foreach ($registrations as $registration) {
        if (!$registration->isCanceled()) {
          $registration->set('state', 'canceled');
          $registration->save();
        }
      }
    }
  }
}

/**
 * Stores the registered group on registration order items.
 *
 * Implements hook_ENTITY_TYPE_presave().
 */
function ahs_registration_commerce_order_item_presave(OrderItemInterface $order_item) {
  if ($order_item->bundle() === 'ahs_registration' && $order_item->get('field_group')->isEmpty()) {
    $group = $order_item->getPurchasedEntity()->getGroup();
    $order_item->set('field_group', $group);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ahs_registration_form_registration_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Display details about the event at the top.
  /** @var \Drupal\registration\Entity\RegistrationInterface $registration */
  $registration = $form_state->getFormObject()->getEntity();
  /** @var \Drupal\registration\HostEntityInterface $host */
  $host = $registration->getHostEntity();
  $view_builder = \Drupal::entityTypeManager()->getViewBuilder($host->getEntity()->getEntityTypeId());
  $form['host'] = $view_builder->view($host->getEntity(), 'summary');
  $form['host']['#weight'] = -10;

  // Don't allow changing the user for an existing registration.
  if (!$registration->isNew() && $registration->getUser() && $registration->getUser()->isAuthenticated()) {
    $form['user_uid']['widget']['#disabled'] = TRUE;
    $form['user_uid']['widget'][0]['target_id']['#description'] = '';
    $form['anon_mail']['#access'] = FALSE;

    // Find the 'who_is_registering' element which gets an id appended
    // to it for unknown reasons.
    foreach ($form as $key => $value) {
      if (strpos($key, 'who_is_registering') === 0) {
        $who_is = $key;
        break;
      }
    }
    $form[$who_is]['#access'] = FALSE;
  }

  if (isset($form['state'])) {
    $form['state']['widget'][0]['#type'] = 'radios';
    $form['state']['#group'] = NULL;
  }
}

/**
 * Implements hook_entity_base_field_info_alter().
 */
function ahs_registration_entity_base_field_info_alter(&$fields, EntityTypeInterface $entity_type) {
  // Tweak registration fields.
  if ($entity_type->id() == 'registration') {
    if (isset($fields['author_uid']) && !empty($fields['author_uid'])) {
      $fields['author_uid']->setLabel('Registered by');
    }
  }
}

/**
 * Implements hook_entity_field_access().
 */
function ahs_registration_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, $items = NULL) {
  $sensitive_fields = [
    'field_life_situations',
    'field_health_information',
  ];
  $field_name = $field_definition->getName();
  if ($field_definition->getTargetEntityTypeId() === 'registration' && in_array($field_name, $sensitive_fields)) {
    // Get the registration entity from the field items.
    $registration = $items ? $items->getEntity() : NULL;

    if ($registration) {
      // The field is only sensitive if it contains information,
      // not if it's blank.
      if (!$registration->get($field_name)->isEmpty()) {
        // Check if the registration is for the current user.
        if ($registration->getUserId() === $account->id()) {
          return AccessResult::allowed()->addCacheContexts(['user'])->addCacheableDependency($registration);
        }

        if ($registration->hasField('field_group')) {
          // Get the group reference.
          /** @var \Drupal\ahs_groups\Entity\AhsGroup $group */
          if ($group = $registration->field_group->entity) {
            return GroupAccessResult::forbiddenIf(!$group->hasPermission('view group member sensitive information', $account))->addCacheableDependency($registration);
          }
        }
      }
    }
  }
  // Default access for other fields and entities.
  return AccessResult::neutral();
}

/**
 * Implements hook_ENTITY_TYPE_access().
 */
//function ahs_registration_registration_access(RegistrationInterface $entity, $op, AccountInterface $account) {
//  if (in_array($op, ['view', 'update', 'change_host'])) {
//    $product_variation = $entity->getHostEntity()->getEntity();
//
//    if ($product_variation instanceof ProductVariationInterface) {
//      /** @var \Drupal\group\Entity\GroupInterface $group */
//      $group = $product_variation->getGroup();
//
//      if ($group && $group->hasPermission('administer members', $account)) {
//        return AccessResult::allowed();
//      }
//    }
//  }
//}

/**
 * Don't allow deleting registration products that have active registrations.
 *
 * Implements hook_ENTITY_TYPE_access().
 */
function ahs_registration_commerce_product_access(ProductInterface $product, $op, AccountInterface $account) {
  if ($op == 'delete' && $variations = $product->getVariationIds()) {
    $registration_storage = \Drupal::entityTypeManager()->getStorage('registration');

    $query = $registration_storage->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('entity_type_id', 'commerce_product_variation');
    $query->condition('entity_id', $variations, 'IN');
    // It's fine to delete hosts for canceled registrations.
    $query->condition('state', 'complete');

    if ($query->execute()) {
      return AccessResult::forbidden();
    }
  }

  return AccessResult::neutral();
}

/**
 * Implements hook_field_widget_info_alter().
 *
 * Swaps the default registration type widget with a custom one from this module.
 */
function ahs_registration_field_widget_info_alter(array &$info) {
  if (isset($info['registration_type'])) {
    $info['registration_type']['class'] = \Drupal\ahs_registration\Plugin\Field\FieldWidget\RegistrationTypeWidget::class;
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 */
function ahs_miscellaneous_inline_entity_form_entity_form_alter(array &$entity_form, FormStateInterface $form_state) {
  if ('commerce_product' == $entity_form['#entity_type'] &&
    'ahs_registration' == $entity_form['#bundle'] &&
    !empty($entity_form['variations']['widget']['entities'][0])
  ) {
    // Remove the add form if the widget already has a variation.
    unset($entity_form['variations']['widget']['form']);
  }

  if ('commerce_product_variation' === $entity_form['#entity_type'] &&
    'ahs_registration' === $entity_form['#bundle']
  ) {
    $entity_form['list_price']['widget'][0]['#description'] = 'Full price to be paid eventually if payment; leave blank if donation.';
    $entity_form['price']['widget'][0]['#description'] = 'Deposit to be paid now if payment; medium suggested amount if donation.';
  }
}

/**
 * Implements hook_ENTITY_TYPE_access() for registration entity type.
 */
function ahs_registration_registration_access(RegistrationInterface $registration, $op, AccountInterface $account) {
  if ($group = $registration?->getHostEntity()?->getEntity()?->getGroup()) {
    if ($op === 'view') {
      return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'view group registration')->addCacheableDependency($registration);
    }
    elseif ($op === 'update') {
      $result = GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'edit group registration')->addCacheableDependency($registration);
      return $result;
    }
    elseif ($op === 'delete') {
      return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'delete group registration')->addCacheableDependency($registration);
    }
    elseif ($op === 'change_host') {
      return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'change host group registration')->addCacheableDependency($registration);
    }
  }
  return AccessResult::neutral();
}
