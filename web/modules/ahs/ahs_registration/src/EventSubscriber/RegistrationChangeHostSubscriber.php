<?php

namespace Drupal\ahs_registration\EventSubscriber;

use Drupal\registration_change_host\Event\RegistrationChangeHostEvents;
use Drupal\registration_change_host\Event\RegistrationChangeHostPossibleHostsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides a registration change host event subscriber.
 */
class RegistrationChangeHostSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      RegistrationChangeHostEvents::REGISTRATION_CHANGE_HOST_POSSIBLE_HOSTS => 'getPossibleHosts',
    ];
  }

  /**
   * Add possible hosts.
   *
   * @param \Drupal\registration_change_host\Event\RegistrationChangeHostPossibleHostsEvent $event
   *   The registration change host event.
   */
  public function getPossibleHosts(RegistrationChangeHostPossibleHostsEvent $event) {
    $registration = $event->getRegistration();
    $set = $event->getPossibleHostsSet();
    /** @var \Drupal\ahs_registration\Entity\AhsRegistrationVariation $product_variation */
    $product_variation = $registration->getHostEntity()->getEntity();

    /** @var \Drupal\ahs_groups\Entity\AhsGroup $group */
    $group = $product_variation->getGroup();
    if ($group && $group->hasField('field_product')) {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      foreach ($group->get('field_product')->referencedEntities() as $product) {
        foreach ($product->getVariations() as $variation) {
          $possible_host = $set->buildNewPossibleHost($variation);
          $set->addHost($possible_host);
        }
        $set->addCacheableDependency($product);
      }
      $set->addCacheableDependency($group);
    }
  }

}
