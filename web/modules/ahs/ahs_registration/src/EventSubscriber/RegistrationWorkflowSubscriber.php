<?php

namespace Drupal\ahs_registration\EventSubscriber;

use Drupal\ahs_registration\Entity\AhsRegistrationVariation;
use Drupal\ahs_training\Entity\TrainingRecord;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\group\Entity\GroupMembership;
use Drupal\message\Entity\Message;
use Drupal\message_notify\MessageNotifier;
use Drupal\registration\Event\RegistrationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides base class for registration workflow.
 */
class RegistrationWorkflowSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ahs_commerce logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The message notify.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotify;

  /**
   * Constructs a RegistrationInsertSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\message_notify\MessageNotifier $message_notify
   *   The message notify service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, MessageNotifier $message_notify) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('ahs_registration');
    $this->messageNotify = $message_notify;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'registration.registration.insert' => ['onInsert', 100],
      'registration.registration.update' => ['onUpdate', 100],
    ];
    return $events;
  }

  /**
   * Helper to get transition subscriptions.
   */
  protected function getWorkflowTransitionSubscriptions() {
    return [
      'complete' => [],
      'hold' => [],
      'cancel' => [
        'removeGroupMember',
      ],
    ];
  }

  /**
   * Helper to get state subscriptions.
   */
  protected function getWorkflowStateSubscriptions() {
    return [
      'pending' => [],
      'held' => [],
      'complete' => [
        // The complete transition does capture movement from canceled
        // to complete, or registrations created complete.
        'addGroupMember',
      ],
      'canceled' => [],
    ];
  }

  /**
   * React to registration being created.
   *
   * @param \Drupal\registration\Event\RegistrationEvent $event
   *   The registration event.
   */
  public function onInsert(RegistrationEvent $event) {
    $registration = $event->getRegistration();
    $to_state = $registration->getState()->id();

    // Repeat addGroupMember logic whenever registration is saved, in case sync failed.
    if ($to_state === 'complete') {
      $this->addGroupMember($event);
      $this->sendConfirmationEmail($event);
    }
  }

  /**
   * React to registration being updated.
   *
   * @param \Drupal\registration\Event\RegistrationEvent $event
   *   The registration event.
   */
  public function onUpdate(RegistrationEvent $event) {
    $registration = $event->getRegistration();
    $workflow = $event->getRegistration()->getWorkflow();
    $workflow_plugin = $workflow->getTypePlugin();
    $from_state = $registration->original->getState()->id();
    $to_state = $registration->getState()->id();

    // Repeat addGroupMember logic whenever registration is saved, in case sync failed.
    if ($to_state === 'complete') {
      $this->addGroupMember($event);
    }
    if ($from_state !== $to_state) {
      $transition = $workflow_plugin->getTransitionFromStateToState($from_state, $to_state);
      if ($transition->id() === 'cancel') {
        $this->removeGroupMember($event);
      }
      if ($to_state === 'complete') {
        $this->sendConfirmationEmail($event);
      }
    }
  }

  /**
   * Add group member for group registration.
   *
   * @param \Drupal\registration\Event\RegistrationEvent $event
   *   The registration event.
   */
  public function addGroupMember(RegistrationEvent $event) {
    $registration = $event->getRegistration();
    $host = $registration->getHostEntity();
    $variation = $host->getEntity();

    if ($variation->getEntityTypeId() === 'commerce_product_variation') {
      $group = $variation->getGroup();

      if (!$group) {
        $this->logger->error('No group specified on ahs_registration product variation ' . $variation->id());
        throw new \Exception("A group should be specified on the ahs_registration product variation.");
      }

      $user = $registration->getUser();
      if (!$user) {
        $this->logger->error('No user specified on registration ' . $registration->id());
      }

      // Add user as group member or update existing group membership.
      $studentTermId = TrainingRecord::STUDENT_TERM_ID;
      $observerTermId = TrainingRecord::OBSERVER_TERM_ID;
      if ($membership = GroupMembership::loadSingle($group, $user)) {
        // Leave mentors as mentors, but upgrade observers to students.
        $participantTypeItemList = $membership->get('field_participant_type');
        if ($participantTypeItemList->isEmpty() || $participantTypeItemList->target_id == $observerTermId) {
          $participantTypeItemList->setValue([['target_id' => $studentTermId]]);
        }
        $membership->set('field_registration', $registration);
        $membership->save();
      }
      else {
        $group->addMember($user, [
          'field_registration' => $registration,
          'field_participant_type' => $studentTermId,
        ]);
      }
    }
  }

  /**
   * Remove group member when registration cancelled.
   *
   * @param \Drupal\registration\Event\RegistrationEvent $event
   *   The registration event.
   */
  public function removeGroupMember(RegistrationEvent $event) {
    $registration = $event->getRegistration();
    $host = $registration->getHostEntity();
    $variation = $host->getEntity();
    if ($variation->getEntityTypeId() === 'commerce_product_variation') {
      $group = $variation->getGroup();

      if (!$group) {
        $this->logger->error('No group specified on ahs_registration product variation ' . $variation->id());
        throw new \Exception("A group should be specified on the ahs_registration product variation.");
      }
      $user = $registration->getUser();
      if (!$user) {
        $this->logger->error('No user specified on registration ' . $registration->id());
      }
      else {
        /** @var \Drupal\group\GroupMembership $membership */
        if ($membership = $group->getMember($user)) {
          /** @var \Drupal\group\Entity\GroupRelationshipInterface $relationship */
          $relationship = $membership->getGroupRelationship();
          $relationship->delete();
        }
      }
    }
  }

  /**
   * Send confirmation email when registered.
   *
   * @param \Drupal\registration\Event\RegistrationEvent $event
   *   The registration event.
   */
  public function sendConfirmationEmail(RegistrationEvent $event) {
    $registration = $event->getRegistration();
    $host = $registration->getHostEntity();
    $variation = $host->getEntity();

    if ($variation instanceof AhsRegistrationVariation) {
      $group = $variation->getGroup();
      $user = $registration->getUser();
      if (!$user) {
        $this->logger->error('No user specified on registration ' . $registration->id());
      }
      else {
        $message = Message::create([
          'template' => 'event_registration_confirmation',
        ]);
        $message->set('field_group', $group);
        $message->set('arguments', [
          '@group-absolute-url' => $group->toUrl('canonical', ['absolute' => TRUE])->toString(TRUE)->getGeneratedUrl(),
        ]);
        $message->save();
        $this->messageNotify->send($message, ['mail' => $user->getEmail(), 'reply' => 'events@ahs.org.uk'], 'email');
      }
    }
  }

}
