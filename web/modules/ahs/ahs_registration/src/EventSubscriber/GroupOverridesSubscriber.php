<?php

namespace Drupal\ahs_registration\EventSubscriber;

use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Entity\GroupInterface;
use Drupal\registration\Event\RegistrationDataAlterEvent;
use Drupal\registration\Event\RegistrationEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Allows group admins to override registration settings.
 */
class GroupOverridesSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * Constructs a new GroupOverridesSubscriber.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   */
  public function __construct(AccountProxy $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * Alters whether a host entity is enabled for registration.
   *
   * @param \Drupal\registration\Event\RegistrationDataAlterEvent $event
   *   The registration data alter event.
   */
  public function alterEnabled(RegistrationDataAlterEvent $event) {
    $enabled = (bool) $event->getData();
    if (!$enabled) {
      $enabled = TRUE;
      $context = $event->getContext();
      $errors = $context['errors'];
      $host_entity = $context['host_entity'];

      if ($host_entity?->getEntity()?->getGroup() instanceof GroupInterface) {
        $group = $host_entity->getEntity()->getGroup();
        // Ignore specific errors if the user is a group admin.
        $errors_to_ignore = ['status', 'capacity', 'waitlist_capacity', 'open', 'close'];
        if (array_intersect($errors_to_ignore, array_keys($errors))) {
          if ($group->hasPermission('edit group registration', $this->currentUser)) {
            $errors = array_diff_key($errors, array_flip($errors_to_ignore));
          }
        }
      }

      // Respect other modules that may have added their own errors.
      if (!empty($errors)) {
        $enabled = FALSE;
      }

      // Update the event.
      $event
        ->setData($enabled)
        ->setErrors($errors);
    }
  }

  /**
   * Alters the state set by a registration wait list presave.
   *
   * @param \Drupal\registration\Event\RegistrationDataAlterEvent $event
   *   The registration data alter event.
   */
  public function alterWaitListState(RegistrationDataAlterEvent $event) {
    $state = (string) $event->getData();
    $context = $event->getContext();

    // If an existing registration is about to be wait listed, see if an
    // override can place the registration in the originally desired active
    // state instead.
    if ($state == 'waitlist') {
      $registration = $context['registration'];
      $host_entity = $context['host_entity'];
      if ($host_entity->getEntity() instanceof GroupInterface) {
        $group = $host_entity->getEntity();
        if (!$registration->isNew() && $registration->getState()->isActive()) {
          if ($group->hasPermission('edit group registration', $this->currentUser)) {
            $event->setData($registration->getState()->id());
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      RegistrationEvents::REGISTRATION_ALTER_ENABLED => 'alterEnabled',
      'registration_waitlist.registration.presave' => 'alterWaitListState',
    ];
  }

}
