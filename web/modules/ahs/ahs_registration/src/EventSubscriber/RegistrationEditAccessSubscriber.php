<?php

namespace Drupal\ahs_registration\EventSubscriber;

use Drupal\ahs_registration\Entity\AhsRegistrationVariation;
use Drupal\Core\Session\AccountProxy;
use Drupal\registration\Entity\RegistrationInterface;
use Drupal\registration\Event\RegistrationDataAlterEvent;
use Drupal\registration\Event\RegistrationEvents;
use Drupal\registration\HostEntityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides a registration data alter event subscriber that influences registration editablility.
 */
class RegistrationEditAccessSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * Constructs a new GroupOverridesSubscriber.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   */
  public function __construct(AccountProxy $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * Deny access to registrations on finished groups but otherwise allow.
   *
   * @param \Drupal\registration\Event\RegistrationDataAlterEvent $event
   *   The registration data alter event.
   */
  public function alterHostIsEditableValidation(RegistrationDataAlterEvent $event) {
    $context = $event->getContext();
    if ($context['pipeline_id'] === 'editable_registration') {
      assert($context['host_entity'] instanceof HostEntityInterface);
      assert($context['host_entity']->getEntity() instanceof AhsRegistrationVariation);
      /** @var \Drupal\ahs_groups\Entity\AhsGroup $group */
      $group = $context['host_entity']->getEntity()->getGroup();
      $result = $event->getData();
      if (!$group->isFinished()) {
        $result->removeViolationWithCode('close');
        $result->removeViolationWithCode('status');
      }
      else {
        assert($context['registration'] instanceof RegistrationInterface);
        $registration = $context['registration'];
        $access = $registration->access('administer', $this->currentUser, TRUE);
        if (!$access->isAllowed()) {
          $result->addViolation('The %label @type is finished.', [
            '%label' => $group->label(),
            '@type' => $group->bundle()
          ], NULL, NULL, NULL, 'finished', t("Finished."));
        }
        $result->addCacheableDependency($access);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      RegistrationEvents::REGISTRATION_ALTER_VALIDATION_RESULT => 'alterHostIsEditableValidation',
    ];
  }

}
