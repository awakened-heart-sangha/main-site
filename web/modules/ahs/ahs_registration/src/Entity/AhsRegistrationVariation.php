<?php

namespace Drupal\ahs_registration\Entity;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Component\Utility\Html;

/**
 * A commerce product variation type that does not depend on products.
 *
 * Class AhsRegistrationVariation.
 *
 * @package Drupal\ahs_registration\Entity
 */
class AhsRegistrationVariation extends ProductVariation {

  /**
   * Helper to get referenced group entity.
   */
  public function getGroup() {
    $product = $this->getProduct();

    $group = NULL;
    if ($product && $product->hasField('field_group')) {
      $group = $product->get('field_group')->entity;
    }

    return $group;
  }

  /**
   * Get this product variation wrapped as a registration host entity.
   *
   * @return \Drupal\registration\HostEntityInterface
   */
  public function getRegistrationHost() {
    $handler = \Drupal::entityTypeManager()->getHandler('registration', 'host_entity');
    return $handler->createHostEntity($this);
  }

  /**
   * Get the registration settings entity.
   *
   * @return \Drupal\registration\Entity\RegistrationSettings|null
   */
  public function getRegistrationSettings() {
    return $this->getRegistrationHost()->getSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    // Create a default sku. Must be done after other preSave(), in order to have
    // the title set.
    $name = strtolower(Html::cleanCssIdentifier(mb_substr($this->getTitle(), 0)));
    $this->set('sku', 'reg_' . $this->get('field_payment_type')->value . '_' . $name);
  }

}
