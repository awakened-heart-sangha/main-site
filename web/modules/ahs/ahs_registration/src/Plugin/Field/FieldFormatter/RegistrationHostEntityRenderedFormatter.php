<?php

namespace Drupal\ahs_registration\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'registration_host_entity_rendered' formatter.
 *
 * An adaptation of the core EntityReferenceEntityFormatter.
 * Only for use with a computed host entity item field.
 * See the comments in the HostEntityItem class for more information.
 *
 * @FieldFormatter(
 *   id = "registration_host_entity_rendered",
 *   label = @Translation("Rendered host entity"),
 *   description = @Translation("Display the host entity for a registration, rendered in a view mode."),
 *   field_types = {
 *     "registration_host_entity"
 *   }
 * )
 *
 * @see \Drupal\registration\Plugin\Field\FieldType\HostEntityItem
 * @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter
 */
class RegistrationHostEntityRenderedFormatter extends FormatterBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'view_mode' => 'default',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements['view_mode'] = [
      '#title' => $this->t('View mode'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('view_mode'),
      '#description' => $this->t("For any host entity type that does not have this view mode, the default view mode will be used."),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $view_mode = $this->getSetting('view_mode');
    $summary[] = $this->t('Rendered as @mode', ['@mode' => $view_mode]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $view_mode = $this->getSetting('view_mode');

    foreach ($items as $delta => $item) {
      /** @var \Drupal\registration\HostEntityInterface $host_entity */
      $entity = $item->get('entity')->getValue();
      if (is_null($entity)) {
        continue;
      }
      // Get the translated entity if it has one. Let the host entity handler
      // do the heavy lifting since the entity type may not be translatable
      // and calling translation functions on it would throw exceptions.
      $handler = $this->entityTypeManager->getHandler('registration', 'host_entity');
      $host_entity = $handler->createHostEntity($entity, $langcode);
      $entity = $host_entity->getEntity();

      $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
      $elements[$delta] = $view_builder->view($entity, $view_mode, $entity->language()->getId());

      // Add a resource attribute to set the mapping property's value to the
      // entity's url. Since we don't know what the markup of the entity will
      // be, we shouldn't rely on it for structured data such as RDFa.
      if (!empty($items[$delta]->_attributes) && !$entity->isNew() && $entity->hasLinkTemplate('canonical')) {
        $items[$delta]->_attributes += ['resource' => $entity->toUrl()->toString()];
      }
    }

    return $elements;
  }

}
