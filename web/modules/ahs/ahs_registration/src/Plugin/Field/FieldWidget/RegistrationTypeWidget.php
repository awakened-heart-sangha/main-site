<?php

namespace Drupal\ahs_registration\Plugin\Field\FieldWidget;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\registration\Plugin\Field\FieldWidget\RegistrationTypeWidget as parentWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of the 'registration_type' widget.
 * 
 * Swapped in by ahs_registration.module so that it is used in the 
 * 'inline_entity_form_settings' widget too.
 */
class RegistrationTypeWidget extends parentWidget {

  /**
   * {@inheritdoc}
   */
  protected function getRegistrationTypeOptions(): array {
    $options = parent::getRegistrationTypeOptions();
    $options[''] = $this->t('-- Select type --');
    return $options;
  }

}
