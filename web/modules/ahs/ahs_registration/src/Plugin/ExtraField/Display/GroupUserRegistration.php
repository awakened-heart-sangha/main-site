<?php

namespace Drupal\ahs_registration\Plugin\ExtraField\Display;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\ahs_groups\Plugin\ExtraField\Display\GroupUserBase;
use Drupal\ahs_training\Entity\TrainingRecord;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\ahs_groups\Plugin\ExtraField\Display\GroupUserRegistration as AhsGroupGroupUserRegistration;

/**
 * Extra field display plugin displaying information about group sessions.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_registration_group_user_registration",
 *   label = @Translation("User's registration"),
 *   description = @Translation("Displays user's registration for group."),
 *   bundles = {
 *     "group.*",
 *   }
 * )
 */
class GroupUserRegistration extends GroupUserBase {

  /**
   * {@inheritdoc}
   */
  protected static function build(AhsGroup $group, UserInterface $user) {
    if ('registration' !== $group->getRegistrationSystem()) {
      $build = static::buildNonRegistrationSystem($group, $user);
    }
    else {
      $build = static::buildRegistrationSystem($group, $user);
    }
    return $build;
  }

  protected static function buildNonRegistrationSystem(AhsGroup $group, UserInterface $user) {
    /** @var \Drupal\group\GroupMembership $membership */
    $membership = $group->getMember($user);

    if ($membership) {
      /** @var \Drupal\group\Entity\GroupRelationshipInterface $relationship */
      $relationship = $membership->getGroupRelationship();

      if ($relationship->hasField('field_participant_type') &&
        TrainingRecord::OBSERVER_TERM_ID == $relationship->get('field_participant_type')->getString()
      ) {
        return [
          '#markup' => '<div>' . t('You are not a registered participant, but you have been given access as an observer.') . '</div>',
        ];
      }

      $owner = $relationship->getOwner();
      $registrar = '';
      if ($owner && $owner->id() != $user->id()) {
        $registrar = ' ' . t('by @name', ['@name' => $owner->getDisplayName()]);
      }

      $created = $relationship->get('created')->getValue();
      $date = \Drupal::service('date.formatter')->format($created[0]['value']);
      return [
        '#markup' => '<div>' . t('Registered on @date', ['@date' => $date]) . $registrar . '</div>',
      ];
    }
  }

  protected static function buildRegistrationSystem(AhsGroup $group, UserInterface $user) {
    $build = [];

    /** @var \Drupal\group\GroupMembership $membership */
    $membership = $group->getMember($user);

    // Find current registration for this group.
    /** @var \Drupal\registration\Entity\RegistrationInterface $registration */
    $registrations = $group->getRegistrations($user);

    $active_registration = FALSE;
    foreach ($registrations as $registration) {
      /** @var \Drupal\workflows\StateInterface $state */
      $state = $registration->getState();
      $registration_type = $registration->getType();
      $active_states = array_keys($registration_type->getActiveStates());
      $active_registration = in_array($state->id(), $active_states);
      if ($active_registration) {
        $view_builder = \Drupal::entityTypeManager()->getViewBuilder('registration');
        $build['complete'] = $view_builder->view($registration, 'summary');

        if ($registration->access('update')) {
          $button = $registration->toLink(t('Update registration'), 'edit-form')->toRenderable();
          $button['#attributes']['class'] = ['btn', 'btn-primary'];
          $button['#url']->setOption('query', ['destination' => $group->toUrl()->toString()]);
          $build['update'] = $button;
        }
        // Only show one active registration. They should be sorted by completion date
        // so the first one should be completed if there is one.
        break;
      }
      elseif ('canceled' == $state->id()) {
        $build['canceled'] = [
          '#markup' => '<div>' . t('Your registration was canceled.') . '</div>',
        ];
      }

      if ('complete' == $state->id()) {
        $view_builder = \Drupal::entityTypeManager()->getViewBuilder('registration');
        $build['complete'] = $view_builder->view($registration, 'summary');
      }

      if ($registration->access('update') && 'canceled' !== $state->id()) {
        $url =  Url::fromRoute('entity.registration.edit_form', ['registration' => $registration->id()]);
        $button = Link::fromTextAndUrl(t('Update registration'), $url)->toRenderable();

        $button['#attributes']['class'] = ['btn', 'btn-primary'];
        $button['#url']->setOption('query', ['destination' => $group->toUrl()->toString()]);
        $build['update'] = $button;
      }
    }

    // Ask unregistered group members to complete their registrations.
    // Group leaders are automatically placed in a state like this, with a membership but not registration.
    // This normally should not happen for other group participants,
    // but bugs or staff errors might cause it.
    if ($membership && !$active_registration) {
      $build['incomplete'] = [
        '#markup' => '<div>' . t('Your registration is incomplete.') . '</div>',
      ];

      // Add buttons to register properly.
      $build['buttons'] = AhsGroupGroupUserRegistration::buildRegistrationCommerceRegistration($group, $user);

      // Add button to cancel instead of registering.
      /** @var \Drupal\group\Entity\GroupRelationshipInterface $relationship */
      $relationship = $membership->getGroupRelationship();
      if ($relationship->access('delete')) {
        $button = $relationship->toLink(t('Cancel'), 'delete-form')->toRenderable();
        $button['#attributes']['class'] = ['btn', 'btn-primary'];
        $build['membership'] = $button;
      }
    }

    return $build;
  }

}
