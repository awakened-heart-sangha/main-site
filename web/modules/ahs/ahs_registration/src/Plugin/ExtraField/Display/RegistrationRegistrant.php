<?php

namespace Drupal\ahs_registration\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;

/**
 * Extra field display plugin displaying information about a user as registrant.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_registration_registration_registrant",
 *   label = @Translation("Registrant"),
 *   description = @Translation("Displays additional information about a registrant."),
 *   bundles = {
 *     "registration.*",
 *   }
 * )
 */
class RegistrationRegistrant extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $registration) {
    $build = [];
    $user = $registration->getUser();
    if ($user) {
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder('user');
      $build['registrant_view'] = $view_builder->view($user, 'registrant');
    }
    return $build;
  }

}
