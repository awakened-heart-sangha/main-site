<?php

namespace Drupal\ahs_registration\Plugin\Commerce\CheckoutFlow;

use Drupal\ahs_commerce\Plugin\Commerce\CheckoutFlow\AhsCheckoutBase;

/**
 * Provides a multistep checkout flow for AHS event and course registrations.
 *
 * @CommerceCheckoutFlow(
 *   id = "ahs_registration_registration",
 *   label = "AHS Event & course registration",
 * )
 */
class Registration extends AhsCheckoutBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    $default = parent::getSteps();
    $default['post_payment_information']['label'] = 'Registration';
    return [
      'login' => $default['login'],
      'payment_information' => $default['payment_information'],
      'payment_review' => $default['payment_review'],
      'payment' => $default['payment'],
      'gift_aid' => $default['gift_aid'],
      'post_payment_information' => $default['post_payment_information'],
      'complete' => $default['complete'],
    ];

  }

}
