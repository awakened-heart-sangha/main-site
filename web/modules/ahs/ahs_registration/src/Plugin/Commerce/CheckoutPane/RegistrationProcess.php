<?php

namespace Drupal\ahs_registration\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_registration\Plugin\Commerce\CheckoutPane\RegistrationProcess as parentRegistrationProcess;

/**
 * Adds an additional registration process pane.
 *
 * In order not to create an unnecessary checkout step, and allowing for orders
 * that may or may not have an amount of zero and so may or may not
 * require payment, we need a copy of this plugin so that it can be placed on
 * two different steps.
 *
 * If the registrations are created by the first instance,
 * the second instance does nothing.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_registration_process",
 *   label = @Translation("AHS Registration process (for payment information step)"),
 *   default_step = "payment_information",
 *   wrapper_element = "container",
 * )
 */
class RegistrationProcess extends parentRegistrationProcess {

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    // Hide the pane if the PaymentInformation pane is not visible.
    $payment_info_pane = $this->checkoutFlow->getPane('payment_information');
    if ($this->getStepId() === 'payment_information' && !$payment_info_pane->isVisible()) {
      return FALSE;
    }
    return TRUE;
  }

}
