<?php

namespace Drupal\ahs_registration\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collect the user's name, if not previously named.
 *
 * @see \Drupal\commerce_registration\Plugin\Commerce\CheckoutPane\RegistrationProcess
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_registration_user_name",
 *   label = @Translation("AHS user name"),
 * )
 */
class UserName extends CheckoutPaneBase {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->logger = $container->get('logger.factory')->get('ahs_registration');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    $user = $this->order->getCustomer();
    if (!$user || $user->isAnonymous() || !$profile = $user->get('about_profiles')->entity) {
      return FALSE;
    }
    return $profile->get('field_name')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    $user = $this->order->getCustomer();
    $profile = $user->get('about_profiles')->entity;
    $form_display = EntityFormDisplay::collectRenderDisplay($profile, 'default');
    $form_display->buildForm($profile, $pane_form, $form_state);
    foreach (array_keys($pane_form) as $key) {
      if ($key !== 'field_name') {
        unset($pane_form[$key]);
      }
    }
    if (isset($pane_form['field_name'])) {
      $pane_form['field_name']['widget']['#required'] = TRUE;
      $pane_form['field_name']['widget'][0]['#required'] = TRUE;
    }
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $user = $this->order->getCustomer();
    $profile = $user->get('about_profiles')->entity;
    $values = $form_state->getValue($pane_form['#parents'])['field_name'][0];
    $profile->set('field_name', $values);
    $profile->save();
    parent::submitPaneForm($pane_form, $form_state, $complete_form);
  }

}
