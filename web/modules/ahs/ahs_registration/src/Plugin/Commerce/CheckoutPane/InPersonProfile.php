<?php

namespace Drupal\ahs_registration\Plugin\Commerce\CheckoutPane;

/**
 * Provides a checkout pane for updating the in-person profile.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_in_person_profile",
 *   label = @Translation("In-person attendance"),
 *   wrapper_element = "fieldset",
 * )
 */
class InPersonProfile extends ProfileBase {

  /**
   * The name of the profile type.
   *
   * @var string
   */
  protected string $profileType = 'in_person_participation';

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    return parent::isVisible() && $this->hasRegistrationOfType('in_person_hermitage');
  }

}
