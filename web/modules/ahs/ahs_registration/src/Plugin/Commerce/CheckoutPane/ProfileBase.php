<?php

namespace Drupal\ahs_registration\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for checkout panes for updating profiles.
 */
class ProfileBase extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * The name of the profile type.
   *
   * @var string
   */
  protected string $profileType;

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    // This type of pane needs an identifiable user.
    if (!$this->order->getCustomer() || $this->order->getCustomer()->isAnonymous()) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $profile = $this->getProfile();
    // Sometimes we set a customer id based on email, without login,
    // so we can't assume the current user is the customer.
    // We don't show existing profile values without checking access,
    // but we do always override the existing profile values with
    // entered values, regardless of access.
    if (!$profile->isNew() && !$profile->access('view')) {
      return $pane_form;
    }
    $form_display = EntityFormDisplay::collectRenderDisplay($profile, 'checkout');
    $form_display->buildForm($profile, $pane_form, $form_state);

    // Cache based on access, which will depend per user.
    $pane_form['#cache']['contexts'][] = 'user';
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::validatePaneForm($pane_form, $form_state, $complete_form);
    $profile = $this->getProfile();

    if (!$profile->isNew() && !$profile->access('view')) {
      return;
    }

    $form_display = EntityFormDisplay::collectRenderDisplay($profile, 'checkout');
    $form_display->extractFormValues($profile, $pane_form, $form_state);
    $form_display->validateFormValues($profile, $pane_form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::submitPaneForm($pane_form, $form_state, $complete_form);
    $profile = $this->getProfile();
    $form_display = EntityFormDisplay::collectRenderDisplay($profile, 'checkout');
    $form_display->extractFormValues($profile, $pane_form, $form_state);
    $profile->save();
  }

  /**
   * Helper to get profile.
   */
  protected function getProfile() {
    $customer_id = $this->order->getCustomerId();
    $properties = [
      'type' => $this->profileType,
      'uid' => $customer_id,
    ];
    $profile_storage = $this->entityTypeManager->getStorage('profile');
    $profiles = $profile_storage->loadByProperties($properties);
    $profile = reset($profiles);
    if (!$profile) {
      $profile = $profile_storage->create($properties);
    }
    return $profile;
  }

  /**
   * Helper to check registration type.
   */
  protected function hasRegistrationOfType(string $registration_type) {
    // We can't rely on already created and saved registrations,
    // as these may only be saved on the same step and so not
    // yet exist when isVisible() is called on this pane.
    foreach ($this->order->getItems() as $item) {
      $registration = $this->getItemRegistration($item);
      if ($registration && $registration->bundle() === $registration_type) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Helper to get registration item.
   */
  protected function getItemRegistration(OrderItemInterface $item) {
    $variation = $item->getPurchasedEntity();
    $handler = $this->entityTypeManager->getHandler('registration', 'host_entity');
    $host_entity = $handler->createHostEntity($variation);
    if ($host_entity->isConfiguredForRegistration()) {
      $registration = $host_entity->createRegistration();
      return $registration;
    }
  }

}
