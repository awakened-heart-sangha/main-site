<?php

namespace Drupal\ahs_registration\Plugin\Commerce\CheckoutPane;

/**
 * Provides a checkout pane for updating the retreat profile.
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_background_profile",
 *   label = @Translation("Background information"),
 *   wrapper_element = "fieldset",
 * )
 */
class BackgroundProfile extends ProfileBase {

  /**
   * The name of the profile type.
   *
   * @var string
   */
  protected string $profileType = 'background';

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    return parent::isVisible() && $this->isRetreatRegistration();
  }

  /**
   * Helper to check registration.
   */
  protected function isRetreatRegistration() {
    foreach ($this->order->getItems() as $item) {
      $variation = $item->getPurchasedEntity();
      if ($variation->bundle() === 'ahs_registration') {
        $group = $variation->getGroup();

        if ($group && $group->bundle() === 'event') {
          if ($group->get('field_dates')->start_date && $group->get('field_dates')->end_date) {
            $interval = $group->get('field_dates')->start_date->diff($group->get('field_dates')->end_date);
            // Anything that spans 5 days is a retreat.
            if ($interval->days > 5) {
              return TRUE;
            }
            // Anything that involves an overnight stay is a retreat.
            if (($interval->days > 0) && $registration = $this->getItemRegistration($item)) {
              if ($registration->bundle() === 'in_person_hermitage') {
                return TRUE;
              }
            }
          }
        }
      }
    }
    return FALSE;
  }

}
