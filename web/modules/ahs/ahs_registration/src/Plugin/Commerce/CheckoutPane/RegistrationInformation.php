<?php

namespace Drupal\ahs_registration\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_registration\Plugin\Commerce\CheckoutPane\RegistrationInformation as parentRegistrationInformation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A customised version of the registration information pane.
 *
 * This needs to go after the registration_process pane in the same flow,
 * but is visible only if there is additional information about the
 * registration that needs capturing from the user.
 *
 * @see \Drupal\commerce_registration\Plugin\Commerce\CheckoutPane\RegistrationProcess
 *
 * @CommerceCheckoutPane(
 *   id = "ahs_registration_information",
 *   label = @Translation("AHS registration information"),
 * )
 */
class RegistrationInformation extends parentRegistrationInformation {

  /**
   * Date time formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL): \Drupal\commerce_registration\Plugin\Commerce\CheckoutPane\RegistrationInformation {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    // Track the subform paths for use later in this method.
    $subform_paths = [];
    foreach ($this->order->getItems() as $item) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
      $variation = $item->getPurchasedEntity();
      $handler = $this->entityTypeManager->getHandler('registration', 'host_entity');
      $host_entity = $handler->createHostEntity($variation);
      if ($host_entity->isConfiguredForRegistration()) {
        $group = $variation->getGroup();

        $quantity = (int) $item->getQuantity();
        for ($index = 1; $index <= $quantity; $index++) {
          $subform_paths[] = $this->getPaneSubformPath($host_entity->getEntity(), $index);
        }
      }
    }

    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);

    // Modify fields, and determine if this step is needed at all.
    $empty = TRUE;
    foreach ($subform_paths as $subform_path) {
      $element =& $pane_form;
      foreach ($subform_path as $path_element) {
        $element =& $element[$path_element];
      }

      $fields =& $element['registration'];

      // Help users by adding event start and end dates to arrival &
      // departure field descriptions.
      if ($group) {
        if (isset($fields['field_arrival'])) {
          if ($startDate = $group->get('field_dates')->start_date) {
            $description = (string) $fields['field_arrival']['widget'][0]['#description'] ?? '';
            $date = $this->dateFormatter->format($startDate->getTimestamp(), 'long');
            $append = 'The event starts at ' . $date;
            $fields['field_arrival']['widget'][0]['#description'] = Markup::create($description . ' ' . $append);
          }
        }
        if (isset($fields['field_departure'])) {
          if ($endDate = $group->get('field_dates')->end_date) {
            $description = (string) $fields['field_departure']['widget'][0]['#description'] ?? '';
            $date = $this->dateFormatter->format($endDate->getTimestamp(), 'long');
            $append = 'The event ends at ' . $date;
            $fields['field_departure']['widget'][0]['#description'] = Markup::create($description . ' ' . $append);
          }
        }
      }

      // Suppress fields for staff registering other people
      // added by RegisterForm.
      $fields['who_is_registering']['#access'] = FALSE;
      $fields['who_message']['#access'] = FALSE;
      $fields['state']['#access'] = FALSE;
      $fields['user_uid']['#access'] = FALSE;
      $fields['anon_mail']['#access'] = FALSE;

      // Show this step only if there is a visible field
      // Ignore properties and hidden fields.
      $visible_fields = Element::getVisibleChildren($fields);
      if (!empty($visible_fields)) {
        $empty = FALSE;
      }
      // If there are no visible fields, hide the title.
      else {
        $element['registration']['#access'] = FALSE;
      }
    }
    if ($empty) {
      // Don't redirect if there are other visible panes on same step.
      $panes = $this->checkoutFlow->getVisiblePanes($this->getStepId());
      if (count(array_diff_key($panes, [$this->getId() => NULL, 'registration_process' => NULL])) === 0) {
        $this->checkoutFlow->redirectToStep($this->checkoutFlow->getNextStepId($this->getStepId()));
      }
    }

    return $pane_form;

  }

}
