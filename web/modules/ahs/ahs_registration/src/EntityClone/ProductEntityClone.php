<?php

namespace Drupal\ahs_registration\EntityClone;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_clone\EntityClone\Content\ContentEntityCloneBase;

/**
 * An entity clone handler for commerce products.
 */
class ProductEntityClone extends ContentEntityCloneBase {

  /**
   * {@inheritdoc}
   */
  protected function fieldIsClonable(FieldDefinitionInterface $field_definition) {
    // Only allow specific group entity reference fields to be cloned.
    // The parent implementation will clone other reference fields like
    // field_group even if we have unset them on the clone.
    return in_array($field_definition->getName(), ['variations'], TRUE);
  }

  /**
   * {@inheritdoc}
   */
  protected function cloneReferencedEntities(FieldItemListInterface $field, $field_definition, array $properties, array &$already_cloned) {
    $referenced_entities = [];
    foreach ($field as $value) {
      // Check if we're not dealing with an entity
      // that has been deleted in the meantime.
      if (!$referenced_entity = $value->get('entity')->getTarget()) {
        continue;
      }

      /** @var \Drupal\Core\Entity\ContentEntityInterface $referenced_entity */
      $referenced_entity = $value->get('entity')->getTarget()->getValue();

      $cloned_reference = $referenced_entity->createDuplicate();
      /** @var \Drupal\entity_clone\EntityClone\EntityCloneInterface $entity_clone_handler */
      $entity_clone_handler = $this->entityTypeManager->getHandler($referenced_entity->getEntityTypeId(), 'entity_clone');
      $cloned_reference->set('product_id', NULL);
      $entity_clone_handler->cloneEntity($referenced_entity, $cloned_reference, [
        'no_suffix' => (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting(),
      ]);

      $referenced_entities[] = $cloned_reference;
    }

    return $referenced_entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function setClonedEntityLabel(EntityInterface $original_entity, EntityInterface $cloned_entity, array $properties) {
    $properties['no_suffix'] = (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting();
    parent::setClonedEntityLabel($original_entity, $cloned_entity, $properties);
  }

}
