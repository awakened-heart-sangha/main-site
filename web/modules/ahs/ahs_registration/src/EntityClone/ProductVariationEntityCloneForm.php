<?php

namespace Drupal\ahs_registration\EntityClone;

use Drupal\entity_clone\EntityClone\Content\ContentEntityCloneFormBase;

/**
 * Entity clone form handler for groups.
 */
class ProductVariationEntityCloneForm extends ContentEntityCloneFormBase {

  /**
   * {@inheritdoc}
   */
  protected function shouldRecurse($entity_type_id) {
    // Don't recurse into references to groups, to prevent unnecessary
    // detections of circular references on backreferences from registration
    // variations to group, which prevent child variation cloning. Necessary
    // because ContentEntityCloneFormBase does not consider
    // ContentEntityCloneBase::fieldIsClonable().
    // @see ContentEntityCloneFormBase::getRecursiveFormElement()
    if ($entity_type_id === 'group') {
      return FALSE;
    }
    return parent::shouldRecurse($entity_type_id);
  }

}
