<?php

namespace Drupal\ahs_registration\EntityClone;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_clone\EntityClone\Content\ContentEntityCloneBase;

/**
 * An entity clone handler for commerce product variations.
 */
class ProductVariationEntityClone extends ContentEntityCloneBase {

  /**
   * Determines if a field is clonable.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return bool
   *   TRUE if the field is clonable; FALSE otherwise.
   */
  protected function fieldIsClonable(FieldDefinitionInterface $field_definition) {
    // Only allow specific entity reference fields to be cloned.
    // The parent implementation will clone other reference fields like
    // field_group even if we have unset them on the clone.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function cloneReferencedEntities(FieldItemListInterface $field, $field_definition, array $properties, array &$already_cloned) {
    $referenced_entities = [];
    foreach ($field as $value) {
      // Check if we're not dealing with an entity
      // that has been deleted in the meantime.
      if (!$referenced_entity = $value->get('entity')->getTarget()) {
        continue;
      }

      /** @var \Drupal\Core\Entity\ContentEntityInterface $referenced_entity */
      $referenced_entity = $value->get('entity')->getTarget()->getValue();

      $cloned_reference = $referenced_entity->createDuplicate();
      /** @var \Drupal\entity_clone\EntityClone\EntityCloneInterface $entity_clone_handler */
      $entity_clone_handler = $this->entityTypeManager->getHandler($referenced_entity->getEntityTypeId(), 'entity_clone');
      $entity_clone_handler->cloneEntity($referenced_entity, $cloned_reference, [
        'no_suffix' => (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting(),
      ]);

      $referenced_entities[] = $cloned_reference;
    }

    return $referenced_entities;
  }

  /**
   * {@inheritdoc}
   */
  public function cloneEntity(EntityInterface $entity, EntityInterface $cloned_entity, array $properties = [], array &$already_cloned = []) {
    $cloned_entity = parent::cloneEntity($entity, $cloned_entity, [
      'no_suffix' => (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting(),
    ]);

    $registration_storage = \Drupal::entityTypeManager()->getStorage('registration_settings');

    $registration_query = $registration_storage->getQuery();
    $registration_query->accessCheck(FALSE);
    $registration_query->condition('entity_type_id', 'commerce_product_variation');
    $registration_query->condition('entity_id', $entity->id());

    if ($results = $registration_query->execute()) {
      $registration_id = reset($results);
      $registration = $registration_storage->load($registration_id);

      $duplicate = $registration->createDuplicate();
      $duplicate->set('entity_id', $cloned_entity->id());

      // Disable registration by default.
      $duplicate->set('status', FALSE);

      // Don't clone dates.
      $duplicate->set('open', NULL);
      $duplicate->set('close', NULL);
      $duplicate->set('reminder_date', NULL);

      $duplicate->save();
    }

    return $cloned_entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function setClonedEntityLabel(EntityInterface $original_entity, EntityInterface $cloned_entity, array $properties) {
    $properties['no_suffix'] = (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting();
    parent::setClonedEntityLabel($original_entity, $cloned_entity, $properties);
  }

}
