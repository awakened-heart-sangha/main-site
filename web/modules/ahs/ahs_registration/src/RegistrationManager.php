<?php

namespace Drupal\ahs_registration;

use Drupal\registration\Entity\RegistrationInterface;
use Drupal\registration\Entity\RegistrationSettings;
use Drupal\registration\RegistrationManager as parentRegistrationManager;

/**
 * Decorate RegistrationManager to only allow self-registration.
 */
class RegistrationManager extends parentRegistrationManager {

  /**
   * {@inheritdoc}
   */
  public function getRegistrantOptions(RegistrationInterface $registration, RegistrationSettings $settings): array {
    // Only allow self-registration.
    $options = parent::getRegistrantOptions($registration, $settings);
    return array_intersect_key($options, array_flip([RegistrationInterface::REGISTRATION_REGISTRANT_TYPE_ME]));
  }

}
