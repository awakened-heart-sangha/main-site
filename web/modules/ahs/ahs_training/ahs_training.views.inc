<?php

/**
 * @file
 * Contains ahs_training\ahs_training.views.inc.
 */

/**
 * Implements hook_views_data_alter().
 */
function ahs_training_views_data_alter(array &$data) {
  $data['groups']['ahs_training_groups_for_user'] = [
    'help' => t('Possible groups for a user'),
    'real field' => 'title',
    'argument' => [
      'title' => t('Possible groups for a user'),
      'id' => 'ahs_training_groups_for_user',
    ],
    'filter' => [
      'title' => t('Possible groups for a user'),
      'field' => 'id',
      'id' => 'ahs_training_groups_for_user',
    ],

  ];

  $data['users_field_data']['ahs_training_users_for_group'] = [
    'help' => t('Possible users for a group'),
    'real field' => 'uid',
    'argument' => [
      'title' => t('Possible users for a group'),
      'id' => 'ahs_training_users_for_group',
    ],
  ];
}

/**
 * Implements hook_views_data().
 */
function ahs_training_views_data() {
  $data['views']['ahs_training_group_eligibility'] = [
    'title' => t('Group eligibility'),
    'help' => t('Displays information about eligibility and suggestion status for a group.'),
    'area' => [
      'id' => 'ahs_training_group_eligibility',
    ],
  ];
  return $data;
}
