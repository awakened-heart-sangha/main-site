The Training module handles the following:

1. Recording of completed events
2. Conditions to autocomplete events when other events are completed
3. Conditions for events that are a prerequisite of other events
4. Logic to complete and auto complete trainings


## Services

There are two services:
1. ahs_training.user_experience_evaluator: records training completions and confirms if a user has completed a training
2. ahs_training.completion: auto completes trainings, handles prerequisites and logic to complete from groups

## ahs_training.user_experience_evaluator service
### Record trainings

Use the record method on the ahs_training.record service.

`\Drupal::service(‘ahs_training.record’)->record($user_id, $group_id, $training_id);`

### Has a user completed a training

Use the hasExperience method to check if a user has completed a training experience.

`\Drupal::service(‘ahs_training.record’)->hasExperience($training_experience_id, $user_id)`

You can optionally pass in a group ID to also check if a user has completed a training from a specific group.

You can also check if a user has completed multiple trainings:

`\Drupal::service(‘ahs_training.record’)->hasAllExperiences($training_experience_ids, $user_id)`

$training_ids is an array of IDs.

## ahs_training.completion service

### Auto complete trainings

A combination of trainings can complete another training. E.g. completing A & B can auto complete C. These are stored as
TrainingCondition entities and referenced from a Training entity.

The auto completion logic is when a TrainingRecord has completed and is called from TrainingRecord entity class postSave() method.
This will be triggered when ever a TrainingRecord entity is created (when `Drupal::service(‘ahs_training.record’)->record()` is called).

### Auto completion bubbling

An event that has been auto completed by the completion of other trainings might itself be listed as available to autocomplete other trainings.

For example: A & C auto completes C. C then auto completes D.

### Recording from group
Use the recordFromGroup() method on the ahs_training.completion service to record from a group.

`\Drupal::service(‘ahs_training.completion’)->recordFromGroup($members, $group, $training = NULL)`

This can be called from a group entity update hook.

In the future, $training will be optional. If it is not passed as an argument, the method will look up the training that
is referenced with the group (to be completed).

### Eligibility conditions

Use the areTrainingEligibilityConditionsMet() method on the ahs_training.completion service to check if a user has met the eligibility conditions
for a training.

`\Drupal::service(‘ahs_training.completion’)->areTrainingEligibilityConditionsMet($groupId, $user)`

Eligibility conditions are stored as TrainingCondition entities and referenced from a group.

This service optionally takes a third parameter of $type. By default, this is set to 'eligibility'. The other allowed type
is 'suggestion'. This is used by the suggestion views filter (`GroupContentMembershipFilter.php`) when checking is a
user meets the suggestion conditions.

## Suggestion filters
Use the `ahs_training_groups_for_user` argument filter in a view to return suggested groups for a user. The user can
be supplied by a contextual argument. If there is no argument, the current logged-in user can be used. This is set in View settings.

Currently used in Suggestions View. See Contextual Filters.

Twig Tweak can be used to pass the user ID to the view and display as a block. Example: `{{ drupal_view('suggestions', 'block_1', $user_id) }}`
