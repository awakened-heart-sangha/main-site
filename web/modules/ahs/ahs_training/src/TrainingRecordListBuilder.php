<?php

namespace Drupal\ahs_training;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Training records entities.
 *
 * @ingroup ahs_training
 */
class TrainingRecordListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Training records ID');
    $header['group'] = $this->t('Group');
    $header['experience'] = $this->t('Experience');
    $header['uid'] = $this->t('User');
    $header['effective_time'] = $this->t('Expected Completion date');
    $header['note'] = $this->t('Note');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $user = $entity->get('uid')->entity;
    $group = $entity->get('group')->entity;
    $experience = $entity->get('experience')->entity;
    $note = $entity->get('note')->value;

    $effective_time = '';
    if ($effective_time_date = $entity->get('effective_time')->getValue()) {
      $effective_time = reset($effective_time_date)['value'];
      $effective_time = date("Y-m-d", (int) $effective_time);
    }

    /** @var \Drupal\ahs_training\Entity\TrainingRecord $entity */
    $row['id'] = Link::createFromRoute(
      $entity->label(),
      'entity.training_record.edit_form',
      ['training_record' => $entity->id()]
    );
    $row['group'] = $group ? $group->label() : '';
    $row['experience'] = $experience ? $experience->label() : '';
    $row['uid'] = $user ? $user->label() : '';
    $row['effective_time'] = $effective_time;
    $row['note'] = $note[0];

    return $row + parent::buildRow($entity);
  }

}
