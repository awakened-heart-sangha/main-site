<?php

namespace Drupal\ahs_training\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\node\Entity\Node;

/**
 * Defines the Training condition entity.
 *
 * @ingroup ahs_training
 *
 * @ContentEntityType(
 *   id = "training_condition",
 *   label = @Translation("Training condition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\ahs_training\Entity\TrainingConditionViewsData",
 *     "access" = "Drupal\ahs_training\TrainingConditionAccessControlHandler",
 *   },
 *   base_table = "training_condition",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {},
 *   field_ui_base_route = "training_condition.settings"
 * )
 */
class TrainingCondition extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return empty($this->get('name')->value) ? $this->calculateLabel() : $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * Get an array of experience ids.
   *
   * User must have ids in order to meet this condition.
   *
   * @return array
   *   An array of node ids of the 'training_experience' node type.
   */
  public function getExperienceIds() {
    return array_column($this->get('experiences')->getValue(), 'target_id');
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->setName($this->calculateLabel());
  }

  /**
   * {@inheritdoc}
   */
  protected function calculateLabel() {
    $label = NULL;
    $eids = $this->getExperienceIds();

    if (!empty($eids)) {
      $experiences = Node::loadMultiple($eids);
      foreach ($experiences as $experience) {
        $labelParts[] = $experience->label();
      }
      if ($labelParts && is_array($labelParts)) {
        $label = implode(' & ', $labelParts);
      }
    }
    if (empty($label)) {
      $label = "No experiences specified.";
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('A convenient description for this condition.'))
      ->setDefaultValue('')
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'type' => 'string',
      ]);

    $fields['experiences'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Training Experiences'))
      ->setDescription(t('The training experiences that a user must have to meet this condition.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type','node')
      ->setSetting('handler_settings', ['target_bundles' => ['training_experience' => 'training_experience']])
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'select2_entity_reference',
        'weight' => 5,
        'settings' => [],
      ])
      ->setRequired(TRUE);

    return $fields;
  }

}
