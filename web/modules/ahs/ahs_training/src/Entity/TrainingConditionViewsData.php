<?php

namespace Drupal\ahs_training\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Training conditions entities.
 */
class TrainingConditionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
