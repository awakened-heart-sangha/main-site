<?php

namespace Drupal\ahs_training\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\group\GroupMembership;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Training records entity.
 *
 * @ingroup ahs_training
 *
 * @ContentEntityType(
 *   id = "training_record",
 *   label = @Translation("Training record"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ahs_training\TrainingRecordListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\ahs_training\Entity\TrainingRecordViewsData",
 *     "form" = {
 *       "default" = "Drupal\ahs_training\Form\TrainingRecordForm",
 *       "add" = "Drupal\ahs_training\Form\TrainingRecordForm",
 *       "edit" = "Drupal\ahs_training\Form\TrainingRecordForm",
 *       "delete" = "Drupal\ahs_training\Form\TrainingRecordDeleteForm",
 *     },
 *     "access" = "Drupal\ahs_training\TrainingRecordAccessControlHandler",
 *   },
 *   base_table = "training_record",
 *   admin_permission = "administer training records",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "owner_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/training/training_record/{training_record}",
 *     "add-form" = "/admin/training/training_record/add",
 *     "edit-form" = "/admin/training/training_record/{training_record}/edit",
 *     "delete-form" = "/admin/training/training_record/{training_record}/delete",
 *     "collection" = "/admin/training/training_records",
 *   },
 *   field_ui_base_route = "training_record.settings"
 * )
 */
class TrainingRecord extends ContentEntityBase implements EntityChangedInterface, EntityOwnerInterface {

  // The ids of the taxonomy terms on the group membership field_partipant_type.
  const STUDENT_TERM_ID = 69;
  const MENTOR_TERM_ID = 70;
  const OBSERVER_TERM_ID = 71;

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    // During record creation by TrainingRecordForm uid
    // and experience can be null.
    if ($this->get('uid')->target_id && $this->get('experience')->target_id) {
      return $this->get('uid')->entity->label() . ": " . $this->get('experience')->entity->label();
    }
    return 'Creating';
  }

  /**
   * {@inheritdoc}
   */
  public function isCascaded() {
    return (bool) $this->get('is_cascaded')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEffectiveTime() {
    // NULL or 0 become TRUE, meaning 'always effective'.
    return empty($this->get('effective_time')->value) ? TRUE : $this->get('effective_time')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getExperienceId() {
    return $this->get('experience')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Update cascading records, if this is not a cascading record.
    if (!$this->isCascaded()) {
      // Store this record in the cache, so it is there for
      // the cascading records updater to use.
      \Drupal::service('ahs_training.user_experience_evaluator')->clearUserExperienceCache($this->getUserId());
      \Drupal::service('ahs_training.cascading_records_updater')->updateForUser($this->getUserId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    // The parent method invalidates the entity cache, so don't rebuild
    // the cascade until after that.
    parent::postDelete($storage, $entities);

    // If any deleted entity is non-cascaded, then rebuild the cascade once.
    $needsCascadeUpdate = FALSE;
    foreach ($entities as $entity) {
      if (!$entity->isCascaded()) {
        $needsCascadeUpdate = TRUE;
      }
    }
    if ($needsCascadeUpdate) {
      \Drupal::service('ahs_training.user_experience_evaluator')->clearUserExperienceCache($entity->getUserId());
      \Drupal::service('ahs_training.cascading_records_updater')->updateForUser($entity->getUserId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The user who is recording as having an experience.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ]);

    $fields['group'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Group'))
      ->setDescription(t('The group that gave the experience to the user.'))
      ->setSetting('target_type', 'group')
      ->setSetting('handler', 'default');

    $fields['experience'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Training Experience'))
      ->setDescription(t('The training experience that has been completed.'))
      ->setSetting('target_type','node')
      ->setSetting('handler_settings', ['target_bundles' => ['training_experience' => 'training_experience']])
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 10,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ]);

    $fields['effective_time'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Effective date'))
      ->setDescription(t('The date this experience is expected to be completed.'))
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 15,
      ]);

    $fields['participant_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Participant type'))
      ->setDescription(t('The type of participant'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['participant_type' => 'participant_type']])
      ->setDefaultValue([0 => ['target_id' => self::STUDENT_TERM_ID]])
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 20,
        'settings' => [],
      ]);

    $fields['note'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Note'))
      ->setDescription(t('An explanation of why this record was created.'))
      ->setDisplayOptions('form', [
        'type' => 'text_plain',
        'weight' => 25,
      ]);

    $fields['owner_id']->setDisplayConfigurable('view', TRUE)
      ->setLabel(t('Creator'))
      ->setDescription(t('The user who created this training record.'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 30,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ]);

    $fields['is_cascaded'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is cascaded'))
      ->setDescription(t('Whether the record is generated automatically based on other records.'))
      ->setDefaultValue(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the record was last edited.'));

    return $fields;
  }

  /**
   * Create training records from a group membership.
   *
   * @param \Drupal\group\GroupMembership $membership
   *   The group membership entity that records the.
   * @param array|null $experienceIds
   *   (optional) An array of ids of training experiences to record.
   * @param bool $mayExist
   *   Whether there may already be a record for this group member.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createFromGroupMembership(GroupMembership $membership, array $experienceIds = NULL, $mayExist = TRUE) {
    $group = $membership->getGroup();
    $user = $membership->getUser();
    $group_content = $membership->getGroupRelationship();

    // If no experiences are specified, use all that the group gives.
    if (empty($experienceIds) && $group->hasField('field_experiences_given')) {
      $experienceIds = array_column($group->get('field_experiences_given')->getValue(), 'target_id');
    }
    if (empty($experienceIds)) {
      return;
    }
    // Don't give out the same experience multiple times.
    $experienceIds = array_unique($experienceIds);

    // Only create records for students and mentors.
    $participantType = NULL;
    if ($group_content->hasField('field_participant_type')) {
      // For group types other than special only mentor and student types
      // are recorded.
      $recordedTypeIds = [static::STUDENT_TERM_ID, static::MENTOR_TERM_ID];
      if ($group->hasField('field_participant_types')) {
        $recordedTypeIds = array_column($group->get('field_participant_types')->getValue(), 'target_id');
      }
      $typeId = $group_content->get('field_participant_type')->target_id;
      if (!is_null($typeId) && !in_array($typeId, $recordedTypeIds)) {
        return;
      }
      $participantType = $group_content->get('field_participant_type')->entity;
    }

    $effectiveDate = NULL;
    if ($group->hasField('field_dates')) {
      $effectiveDate = $group->get('field_dates')->end_date;
      if (empty($effectiveDate)) {
        $effectiveDate = $group->get('field_dates')->start_date;
      }
    }
    $effectiveTimestamp = $effectiveDate instanceof DrupalDateTime ? $effectiveDate->getTimestamp() : $group_content->getCreatedTime();

    $fields = [
      'uid' => $user->id(),
      'effective_time' => $effectiveTimestamp,
      'group' => $group->id(),
      'participant_type' => $participantType->id(),
    ];
    static::createMultiple($experienceIds, $fields, $mayExist);
  }

  /**
   * Create multiple training records.
   *
   * @param array $experienceIds
   *   An array of experience ids to create records for.
   * @param array $fields
   *   An array of field values for each record, keyed by field name.
   * @param bool $mayExist
   *   Whether the record may already exist.
   */
  public static function createMultiple(array $experienceIds, array $fields, $mayExist = TRUE) {
    $fields['effective_time'] = $fields['effective_time'] ?? \Drupal::time()->getRequestTime();

    // Create a record for each experience.
    $storage = \Drupal::entityTypeManager()->getStorage('training_record');
    foreach ($experienceIds as $experienceId) {
      // Create a record but don't save it yet.
      $fields['experience'] = $experienceId;
      $record = $storage->create($fields);
      // Save the record if there is not already one.
      if (!$mayExist) {
        $record->save();
      }
      else {
        $storage = \Drupal::entityTypeManager()->getStorage('training_record');
        $properties = array_intersect_key($fields, array_flip(['uid', 'experience', 'group']));
        $records = $storage->loadByProperties($properties);
        if (empty($records)) {
          $record->save();
        }
      }
    }
  }

}
