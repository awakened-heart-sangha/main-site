<?php

namespace Drupal\ahs_training;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class TrainingRecord.
 *
 * Records a training.
 *
 * @package Drupal\ahs_training
 */
class UserExperienceEvaluator {

  /**
   * A static cache of user experiences for each user.
   *
   * The keys are user ids, the values are arrays where the keys are experience
   * ids and the values are the earliest effective date.
   *
   * @var array
   */
  protected $userExperiences = [];

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new UserExperienceEvaluator object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Has a user completed a set of trainings.
   */
  public function hasAllExperiences(array $experienceIds, $uid, DrupalDateTime $date = NULL, $returnTimestamp = FALSE) {
    $latestDate = 0;
    foreach (array_count_values($experienceIds) as $experienceId => $experienceCount) {
      $hasExperience = $this->hasExperience($experienceId, $uid, $date, $experienceCount, $returnTimestamp);
      if (!$hasExperience) {
        return FALSE;
      }
      // If $hasExperience === TRUE then $latestDate never needs updating.
      if ($hasExperience !== TRUE) {
        $latestDate = max($latestDate, $hasExperience);
      }
    }
    $result = $returnTimestamp && $latestDate > 0 ? $latestDate : TRUE;
    return $result;
  }

  /**
   * Check if a user has completed a single training.
   *
   * @return bool
   *   Whether or not the user has the experience at the date.
   */
  public function hasExperience($experienceId, $uid, DrupalDateTime $date = NULL, $experienceCount = 1, $returnTimestamp = FALSE) {
    // Make sure that anonymous user don't have experiences.
    if ($uid == 0) {
      return FALSE;
    }

    $experiences = $this->getUserExperiences($uid);

    // If the user doesn't have a record of that experience.
    if (!isset($experiences[$experienceId][$experienceCount])) {
      return FALSE;
    }

    // If we need to check the effective date and it's not yet effective.
    if ($date) {
      $timestamp = $date->getTimestamp();
      // If the date the experience becomes effective is later than the date
      // we're evaluating for.
      if ($experiences[$experienceId][$experienceCount] > $timestamp) {
        return FALSE;
      }
    }

    // If the record has an effective date, and we need to return it.
    if ($returnTimestamp && $experiences[$experienceId][$experienceCount] > 0) {
      return $experiences[$experienceId][$experienceCount];
    }

    // The user has a record, and we don't care about the date.
    return TRUE;
  }

  /**
   * Checks if a set of conditions have been completed by a user.
   *
   * @param array $conditions
   *   An array of conditions.
   * @param int $uid
   *   The user id.
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   The date at which the condition should be effective.
   * @param bool $returnTimestamp
   *   Whether to return TRUE or a timestamp if a condition is met.
   *
   * @return mixed
   *   FALSE if the user does not meet any of the conditions. If the user does
   *   meet any of them then depending on $returnTimestamp either a boolean TRUE
   *   or an integer timestamp indicating th earliest date the conditions was
   *   met.
   */
  public function isAnyConditionMetByUser(array $conditions, $uid, DrupalDateTime $date = NULL, $returnTimestamp = FALSE) {
    $timestamp = NULL;
    foreach ($conditions as $condition) {
      $experienceIds = $condition->getExperienceIds();
      if (empty($experienceIds)) {
        continue;
      }
      // Return truthy if user has a record for any of the conditions.
      if ($hasExperiences = $this->hasAllExperiences($experienceIds, $uid, $date, $returnTimestamp)) {
        // Return true if the experience is always effective or
        // we don't want to return the effective time.
        if (!$returnTimestamp || $hasExperiences === TRUE) {
          return TRUE;
        }
        else {
          // Keep track of the earliest date any condition
          // that will become effective.
          $timestamp = min($timestamp ?? $hasExperiences, $hasExperiences);
        }
      }
    }

    // If timestamp is not null, then we must have passed a condition but want
    // to return the earliest effective time after comparing all conditions.
    if (!is_null($timestamp)) {
      return $timestamp;
    }

    return FALSE;
  }

  /**
   * Get a user's experiences and their effective dates.
   *
   * This uses a static cache for performance when evaluating the user for many
   * different experiences.
   *
   * @param int $uid
   *   The user id.
   *
   * @return array
   *   An array keyed by experience id, with the values being the earliest
   *   effective date of the experience.
   */
  protected function getUserExperiences($uid) {
    if (!isset($this->userExperiences[$uid])) {
      $this->userExperiences[$uid] = [];
      $query = $this->database->select('training_record', 'tr');
      $query->condition('tr.uid', $uid);
      $query->addField('tr', 'experience');
      $query->addField('tr', 'effective_time');
      // Get all experience records as an array.
      $records = $query->execute()->fetchAll();
      foreach ($records as $record) {
        $effective = (int) $record->effective_time ?? TRUE;
        $this->updateUserExperienceCache($uid, $record->experience, $effective);
      }
    }
    return $this->userExperiences[$uid];
  }

  /**
   * Update the user experience cache.
   *
   * @param int $uid
   *   The user id.
   * @param int $experienceId
   *   The experience id.
   * @param bool|int $effective
   *   The timestamp when the experience will become effective. Can also be TRUE
   *   meaning always.
   */
  public function updateUserExperienceCache($uid, $experienceId, $effective) {
    // Load the user's experiences from storage if they're not already loaded.
    $cache = $this->getUserExperiences($uid);
    $timestamps = $cache[$experienceId] ?? [];

    // Create an array of experience timestamps. So
    // $this->userExperiences[$uid][$experienceId][2] will tell you the earliest
    // time when the user has at least 2 instances of a certain experience.
    $timestamps[] = ($effective === TRUE) ? 0 : $effective;
    sort($timestamps);
    $keys = range(1, count($timestamps));
    $updated = array_combine($keys, $timestamps);
    $this->userExperiences[$uid][$experienceId] = $updated;
  }

  /**
   * Helper to clear cache values.
   */
  public function clearUserExperienceCache($uid) {
    unset($this->userExperiences[$uid]);
  }

}
