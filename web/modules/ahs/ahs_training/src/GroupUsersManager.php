<?php

namespace Drupal\ahs_training;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\group\Entity\GroupInterface;

/**
 * Identifies possible users for groups.
 *
 * @package Drupal\ahs_training
 */
class GroupUsersManager {

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new GroupUsersManager object.
   */
  public function __construct(Connection $database, TimeInterface $time) {
    $this->database = $database;
    $this->time = $time;
  }

  /**
   * Helper to get users with experiences.
   */
  public function getUsersWithExperience($experienceId, $date = NULL, $experienceCount = 1, $isCascaded = NULL) {
    $query = $this->database->select('training_record', 'tr');
    $query->addField('tr', 'uid');
    if ($date) {
      // Convert the date to unix time in seconds.
      $timestamp = $date->getTimestamp();
      // The record's expected completion should either be in the past or unset.
      $orCondition = $query->orConditionGroup();
      $orCondition->isNull('tr.effective_time')
        ->condition('tr.effective_time', $timestamp, "<=");
      $query->condition($orCondition);
    }
    $query->condition('tr.experience', $experienceId);
    if (!is_null($isCascaded)) {
      $query->condition('tr.is_cascaded', $isCascaded);
    }
    if (!empty($experienceCount) && $experienceCount > 1) {
      $query->groupBy("tr.uid");
      $query->having('COUNT(uid) >= :matches', [':matches' => $experienceCount]);
    }
    // Get unique participants as an array.
    $result = $query->execute()->fetchAll(\PDO::FETCH_COLUMN | \PDO::FETCH_UNIQUE, 0);
    assert(is_array($result));
    return $result;
  }

  /**
   * Helper to get users with all experiences.
   */
  public function getUsersWithAllExperiences(array $experienceIds, DrupalDateTime $date = NULL) {
    if (empty($experienceIds)) {
      return NULL;
    }
    $usersForExperience = [];
    // If an experience is specified multiple times, require the user to have
    // had it recorded multiple times.
    foreach (array_count_values($experienceIds) as $experienceId => $experienceCount) {
      $usersForExperience[] = $this->getUsersWithExperience($experienceId, $date, $experienceCount);
    }
    // Discard users who do not have all the experiences.
    return count($usersForExperience) > 1 ? array_intersect(...$usersForExperience) : $usersForExperience[0];
  }

  /**
   * Get users who meet at least one of a set of conditions.
   *
   * @return bool|array
   *   Return TRUE if there are no conditions, or an array of user ids.
   */
  public function getUsersMeetingConditions(array $conditions, DrupalDateTime $date = NULL) {
    if (empty($conditions)) {
      return TRUE;
    }
    $users = [];
    foreach ($conditions as $condition) {
      // Gets IDs of the Training Experiences.
      $experienceIds = $condition->getExperienceIds();
      $users[] = $this->getUsersWithAllExperiences($experienceIds, $date) ?? [];
    }
    return array_merge(...$users);
  }

  /**
   * Get ids of users who are members of a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   Group entity.
   */
  protected function getGroupMemberUserIds(GroupInterface $group) {
    $uids = [];
    foreach ($group->getMembers() as $membership) {
      $uids[] = $membership->getGroupRelationship()->get('entity_id')->target_id;
    }
    return $uids;
  }

  /**
   * Get list of participants who are eligible to be suggested a group.
   *
   * @return bool|array
   *   TRUE if suggested to everyone, or an array of user ids.
   */
  public function getEligibleUsersForGroup(AhsGroup $group, $excludeCurrent = TRUE) {
    // If there are no eligibility conditions, return TRUE to indicate
    // everyone is eligible.
    $conditions = $group->get('field_eligibility_conditions')->referencedEntities();
    if (empty($conditions)) {
      return TRUE;
    }

    $eligibleUsers = $this->getUsersMeetingConditions($conditions, $group->getConditionsEvaluationDate());
    assert(is_array($eligibleUsers));

    if ($excludeCurrent) {
      $currentUsers = $this->getGroupMemberUserIds($group);
      $eligibleUsers = array_diff($eligibleUsers, $currentUsers);
    }
    assert(is_array($eligibleUsers));
    return $eligibleUsers;
  }

  /**
   * Get list of users who should be suggested a group.
   *
   * @return bool|array
   *   TRUE if suggested to everyone,
   *   FALSE if to no one, or an array of user ids.
   */
  public function getSuggestedUsersForGroup(AhsGroup $group, $excludeCurrent = TRUE) {
    // Groups must be explicitly suggested to be suggested to anyone.
    // This doesn't use ::isSuggestable() because that considers group availablity as well,
    // and we want this to work for past groups too so it needs to ignore group availability. 
    if (!$group->isSuggested()) {
      return FALSE;
    }

    // No need to compute and exclude current members from those eligible, as we
    // will exclude them from those suggested.
    $eligibleUsers = $this->getEligibleUsersForGroup($group, FALSE);

    // If there are no explicit suggestion conditions, and it is eligible
    // to everyone return TRUE to indicate it is suggested to everyone.
    $conditions = $group->get('field_suggestion_conditions')->referencedEntities();
    if (empty($conditions) && $eligibleUsers === TRUE) {
      return TRUE;
    }
    if ($eligibleUsers === FALSE) {
      // This shouldn't occur.
      return FALSE;
    }

    $suggestedUsers = $this->getUsersMeetingConditions($conditions, $group->getConditionsEvaluationDate());

    // Only eligible users can be suggested.
    if (is_array($eligibleUsers)) {
      if (is_array($suggestedUsers)) {
        $suggestedUsers = array_intersect($suggestedUsers, $eligibleUsers);
      }
      elseif ($suggestedUsers === TRUE) {
        $suggestedUsers = $eligibleUsers;
      }
      else {
        // This shouldn't occur.
        return FALSE;
      }
    }

    if ($excludeCurrent) {
      $currentUsers = $this->getGroupMemberUserIds($group);
      $suggestedUsers = array_diff($suggestedUsers, $currentUsers);
    }

    // Exclude users who already have all the experiences of this group.
    $experienceIds = array_column($group->get('field_experiences_given')->getValue(), 'target_id');
    // Regardless of effective date of the experience.
    $experiencedUsers = $this->getUsersWithAllExperiences($experienceIds) ?? [];
    $suggestedUsers = array_diff($suggestedUsers, $experiencedUsers);

    return $suggestedUsers;
  }

}
