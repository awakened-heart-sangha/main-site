<?php

namespace Drupal\ahs_training;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Training conditions entity.
 *
 * @see \Drupal\ahs_training\Entity\TrainingCondition.
 */
class TrainingConditionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ahs_training\Entity\TrainingCondition $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view training conditions');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'administer training conditions');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'administer training conditions');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer training conditions');
  }

}
