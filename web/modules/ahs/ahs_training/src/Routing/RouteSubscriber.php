<?php

namespace Drupal\ahs_training\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.taxonomy_term.canonical')) {
      $route->setDefault('_controller', '\Drupal\ahs_training\Controller\TaxonomyTermViewPageController::handle');
    }

    if ($route = $collection->get('view.training_experience_records.page_1')) {
      $route->addOptions([
        'parameters' => [
          'node' => [
            'type' => 'entity:node',
            'bundle' => [
              'training_experience',
            ],
          ],
        ],
      ]);
      // From D9.2 this requirement can be dropped as it will be covered
      // by the 'bundle' option defined above.
      // See https://www.drupal.org/node/3155569
      $route->addRequirements(['_entity_bundles' => 'node:training_experience']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();

    // Come after views.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -180];

    return $events;
  }

}
