<?php

namespace Drupal\ahs_training\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Training records edit forms.
 *
 * @ingroup ahs_training
 */
class TrainingRecordForm extends ContentEntityForm {

  /**
   * The current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Drupal time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->currentRequest = $container->get('request_stack')->getCurrentRequest();
    $form->entityTypeManager = $container->get('entity_type.manager');
    $form->messenger = $container->get('messenger');
    $form->time = $container->get('datetime.time');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if ($this->entity->isNew()) {
      // Default the effective date to today.
      $timestamp = $this->time->getRequestTime();
      $datetime = DrupalDateTime::createFromTimestamp($timestamp);
      $form['effective_time']['widget']['0']['value']['#default_value'] = $datetime;

      // This form can create multiple training records, for multiple
      // users and multiple experiences.
      $form['uid']['#access'] = FALSE;
      $form['uids'] = [
        '#type' => 'select2',
        '#title' => 'Users',
        '#multiple' => TRUE,
        '#autocomplete' => TRUE,
        '#target_type' => 'user',
        '#description' => $this->t('The users who will be recorded as having the training experience.'),
      ];
      $uid = $this->currentRequest->query->get('uid');
      if (is_numeric($uid) && $user = $this->entityTypeManager->getStorage('user')->load($uid)) {
        $form['uids']['#default_value'] = $user->id();
      }

      $form['experience']['#access'] = FALSE;
      $form['experiences'] = [
        '#type' => 'select2',
        '#title' => 'Experiences',
        '#multiple' => TRUE,
        '#autocomplete' => TRUE,
        '#target_type' => 'node',
        '#selection_settings' => ['target_bundles' => ['training_experience']],
        '#description' => $this->t('The training experiences that should be recorded.'),
      ];
      $experienceId = $this->currentRequest->query->get('experience');
      if (is_numeric($experienceId) && $node = $this->entityTypeManager->getStorage('node')->load($experienceId)) {
        if ($node->bundle() === 'training_experience') {
          $form['experiences']['#default_value'] = $node->id();
        }
      }
    }

    // Time is unnecessary detail, date-only is fine.
    $form['effective_time']['widget']['0']['value']['#date_date_element'] = 'date';
    $form['effective_time']['widget']['0']['value']['#date_time_element'] = 'none';
    $form['effective_time']['widget']['0']['value']['#description'] = $this->t('The date this experience is expected to be completed. Format: dd/mm/yyyy');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if ($this->entity->isNew()) {
      $uids = $form_state->getValues()['uids'];
      $experienceIds = $form_state->getValues()['experiences'];
      $entityTypeManager = $this->entityTypeManager;
      $recordStorage = $entityTypeManager->getStorage('training_record');
      $count = 0;
      foreach ($uids as $uid) {
        foreach ($experienceIds as $experienceId) {
          $count++;
          $record = $recordStorage->create();
          $this->copyFormValuesToEntity($record, $form, $form_state);
          $record->set('uid', $uid);
          $record->set('experience', $experienceId);
          $record->save();
        }
      }
      $this->messenger->addMessage("Created $count training records");
      return SAVED_NEW;
    }

    $status = parent::save($form, $form_state);
    $this->messenger->addMessage("Updated the training record");
    return $status;
  }

}
