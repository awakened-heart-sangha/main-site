<?php

namespace Drupal\ahs_training\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Training records entities.
 *
 * @ingroup ahs_training
 */
class TrainingRecordDeleteForm extends ContentEntityDeleteForm {


}
