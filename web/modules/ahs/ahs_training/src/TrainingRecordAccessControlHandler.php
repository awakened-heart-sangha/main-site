<?php

namespace Drupal\ahs_training;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Training records entity.
 *
 * @see \Drupal\ahs_training\Entity\TrainingRecord.
 */
class TrainingRecordAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ahs_training\Entity\TrainingRecordInterface $entity */
    switch ($operation) {
      case 'view':
        if ($entity->getUserId() != $account->id()) {
          return AccessResult::allowedIfHasPermission($account, 'view any training records');
        }
        return AccessResult::allowedIfHasPermission($account, 'view own training records');

      case 'update':
        if ($entity->isCascaded()) {
          return AccessResult::forbidden('Cascaded records cannot be updated by users.');
        }
        return AccessResult::allowedIfHasPermission($account, 'administer training records');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'administer training records');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer training records');
  }

}
