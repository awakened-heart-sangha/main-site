<?php

namespace Drupal\ahs_training\Plugin\views_add_button;

use Drupal\Core\Url;
use Drupal\views_add_button\Plugin\views_add_button\ViewsAddButtonDefault;

/**
 * Views Add Button plugin that uses the entity type's add-form link.
 *
 * @ViewsAddButton(
 *   id = "ahs_training_add_form",
 *   label = @Translation("ViewsAddButtonAddForm"),
 *   target_entity = ""
 * )
 */
class ViewsAddButtonAddForm extends ViewsAddButtonDefault {

  /**
   * Plugin description.
   *
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t("Views Add Button URL Generator for entity types that have an 'add-form' link defined.");
  }

  /**
   * Generate the add button URL.
   *
   * @param string $entity_type
   *   Entity type ID.
   * @param string $bundle
   *   Bundle ID.
   * @param array $options
   *   Array of options to be passed to the Url object.
   * @param string $context
   *   Module-specific context string.
   *
   * @return \Drupal\Core\Url
   *   Url object which is used to construct the add button link.
   */
  public static function generateUrl($entity_type, $bundle, array $options, $context = '') {
    $u = \Drupal::entityTypeManager()->getDefinition($entity_type)->getLinkTemplate('add-form');
    // Create URL from the data above.
    return Url::fromUserInput($u, $options);
  }

}
