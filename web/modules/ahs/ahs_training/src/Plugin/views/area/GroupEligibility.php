<?php

namespace Drupal\ahs_training\Plugin\views\area;

use Drupal\ahs_groups\UserGroupsManager;
use Drupal\ahs_training\UserExperienceEvaluator;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\node\Entity\Node;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a group eligibility area handler.
 *
 * Used to diagnose eligibility & suggestion.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("ahs_training_group_eligibility")
 */
class GroupEligibility extends AreaPluginBase {

  /**
   * The group storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $groupStorage;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $userStorage;

  /**
   * The user groups manager service.
   *
   * @var \Drupal\ahs_groups\UserGroupsManager
   */
  protected $userGroupsManager;

  /**
   * The user experience evaluator service.
   *
   * @var \Drupal\ahs_training\UserExperienceEvaluator
   */
  protected $userExperienceEvaluator;

  /**
   * Constructs a new OrderTotal instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\ahs_groups\UserGroupsManager $user_groups_manager
   *   The user groups manager.
   * @param \Drupal\ahs_training\UserExperienceEvaluator $user_experience_evaluator
   *   The user experience evaluator.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, UserGroupsManager $user_groups_manager, UserExperienceEvaluator $user_experience_evaluator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->groupStorage = $entity_type_manager->getStorage('group');
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->userGroupsManager = $user_groups_manager;
    $this->userExperienceEvaluator = $user_experience_evaluator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('ahs_groups.user_groups_manager'),
      $container->get('ahs_training.user_experience_evaluator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['suggested'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['suggested'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include suggestion'),
      '#default_value' => $this->options['suggested'],
      '#description' => $this->t('Whether to include diagnostics for suggestion as well as eligibility.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $group = NULL;
    foreach ($this->view->argument as $argument) {
      if (in_array(gettype($argument->getValue()), ['string', 'integer'])) {
        /**@var \Drupal\ahs_groups\Entity\AhsGroup $group */
        $group = $this->groupStorage->load($argument->getValue());
        break;
      }
    }
    if (!$group) {
      return [];
    }

    // If (!$empty || !empty($this->options['empty'])) {}.
    $bundleLabel = $group->type->entity->label();

    $build = ['#type' => 'container'];
    $build['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => ($this->options['suggested'] ? "Suggestion" : "Eligibility") . " diagnostics for $bundleLabel " . $group->id(),
    ];
    $table = ['#type' => 'table'];

    $input = $this->view->getExposedInput();
    if ($input && isset($input['mail'])) {
      $users = $this->userStorage->loadByProperties(['mail' => $input['mail']]);
      if (empty($users)) {
        $this->messenger()->addError('No user found with email ' . $input['mail']);
        return [];
      }
      $user = reset($users);
      if ($user) {
        $userLabel = $user->label();
        $uid = $user->id();
        $userLink = '<a href="' . "/user/$uid/participation/experiences" . '">' . "$userLabel ($uid)</a>";

        if ($this->options['suggested']) {
          $table['is_suggested_to_user'] = $this->getBooleanMarkup("$bundleLabel suggested to user", $group->isSuggestedToUser($user));
          $table['has_user_already_engaged'] = $this->getBooleanMarkup("$bundleLabel not already engaged with by user", !$group->hasUserAlreadyEngaged($user));
          $table['has_user_already_experiences_given'] = $this->getBooleanMarkup("$bundleLabel experiences not already had by user", !$group->hasUserAlreadyExperiencesGiven($user));
          $table['are_suggestion_conditions_met_by_user'] = $this->getBooleanMarkup("$bundleLabel suggestion conditions met by user", $group->areSuggestionConditionsMetByUser($user));
          $table['is_registration_available_to_user'] = $this->getBooleanMarkup("$bundleLabel is registerable to user", $group->isRegistrationAvailableToUser($user));
        }

        $table['are_eligibility_conditions_met_by_user'] = $this->getBooleanMarkup("$bundleLabel eligibility conditions met by user", $group->areEligibilityConditionsMetByUser($user));
        $table['available_to_user'] = $this->getBooleanMarkup("$bundleLabel available to user $userLink ?", $group->isAvailableToUser($user));
        if ($group->bundle() !== 'course_template') {
          $table['group_member'] = $this->getBooleanMarkup("Not already $bundleLabel participant", !$this->userGroupsManager->isUserGroupMember($user->id(), $group));
        }

        $eligibilityTable = $this->getConditionsDiagnosticsTable('Eligibility', $group->get('field_eligibility_conditions')->referencedEntities(), $group, $user);
        if ($this->options['suggested']) {
          $suggestionTable = $this->getConditionsDiagnosticsTable('Suggestion', $group->get('field_suggestion_conditions')->referencedEntities(), $group, $user);
        }
      }
    }

    if ($this->options['suggested']) {
      $table['suggested'] = $this->getBooleanMarkup("$bundleLabel suggested", $group->isSuggested());
      $table['is_registration_available'] = $this->getBooleanMarkup("$bundleLabel registration available", $group->isRegistrationAvailable());
      $table['registration_deadline_passed'] = $this->getBooleanMarkup("$bundleLabel registration deadline not passed", !$group->hasRegistrationDeadlinePassed());
      $table['is_registration_open'] = $this->getBooleanMarkup("$bundleLabel registration open", $group->isRegistrationOpen());
      $table['suggestable'] = $this->getBooleanMarkup("$bundleLabel suggestable", $group->isSuggestable());
      $table['not_started'] = $this->getBooleanMarkup("$bundleLabel not started", !$group->isStarted());
    }
    $table['available'] = $this->getBooleanMarkup("$bundleLabel is available", $group->isAvailable());
    $table['finished'] = $this->getBooleanMarkup("$bundleLabel not finished", !$group->isFinished());
    $table['private'] = $this->getBooleanMarkup("$bundleLabel is not private", !$group->isPrivate());
    $table['published'] = $this->getBooleanMarkup("$bundleLabel published", $group->isPublished());


    $build['basic'] = $table;
    $build['eligibility'] = $eligibilityTable ?? [];
    $build['suggestion'] = $suggestionTable ?? [];

    return $build;
  }

  /**
   * Helper to get markup.
   */
  protected function getBooleanMarkup($question, $answer) {
    return [
      'answer' => [
        '#markup' => is_null($answer) ? '' : Markup::create($answer ? 'Yes' : '<span style="color:red;">No</span>'),
      ],
      'question' => [
        '#markup' => is_null($answer) || $answer ? $question : Markup::create('<span style="color:red;">' . $question . '</span>'),
      ],
    ];
  }

  /**
   * Helper to get conditions.
   */
  protected function getConditionsDiagnosticsTable($type, $conditions, $group, $user) {
    $table = [
      '#type' => 'table',
      '#header' => ['', $type],
    ];
    if (empty($conditions)) {
      $table['no_conditions_specified'] = $this->getBooleanMarkup("-- No conditions specified", NULL);
    }
    else {
      $date = $group->getConditionsEvaluationDate();
      $dateString = 'on ' . $date->format('Y-m-d');
      $table['any_condition_met_by_user'] = $this->getBooleanMarkup("-- Any conditions met by user $dateString", $this->userExperienceEvaluator->isAnyConditionMetByUser($conditions, $user->id(), $date));
      foreach ($conditions as $condition) {
        $hasAllExperiences = $this->userExperienceEvaluator->hasAllExperiences($condition->getExperienceIds(), $user->id(), $date);
        $table[$condition->id()] = $this->getBooleanMarkup("-- -- User has experiences " . $condition->label() . " " . $dateString, $hasAllExperiences);
        if (!$hasAllExperiences) {
          foreach ($condition->getExperienceIds() as $experienceId) {
            $experience = Node::load($experienceId);
            $hasExperience = $this->userExperienceEvaluator->hasExperience($experienceId, $user->id(), $date);
            $table[$condition->id() . ' ' . $experienceId] = $this->getBooleanMarkup("-- -- -- User has experience " . $experience->label() . " " . $dateString, $hasExperience);
          }
        }
      }
    }
    return $table;

  }

}