<?php

namespace Drupal\ahs_training\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides filtering of users to those eligible/suggested for a given group.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("ahs_training_users_for_group")
 */
class UsersForGroup extends ArgumentPluginBase {

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User groups.
   *
   * @var \Drupal\ahs_training\GroupUsersManager
   */
  protected $groupUsers;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->groupUsers = $container->get('ahs_training.group_users_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['exclude_members'] = ['default' => TRUE];
    $options['suggested'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['exclude_members'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude existing group members'),
      '#default_value' => !empty($this->options['exclude_members']),
    ];
    $form['suggested'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude users the group is not suggested to'),
      '#description' => $this->t('If not selected, all users eligible for the group will be included.'),
      '#default_value' => !empty($this->options['suggested']),
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $group = Group::load($this->argument);

    // Identify specific users for this group.
    if (empty($this->options['suggested'])) {
      $users = $this->groupUsers->getEligibleUsersForGroup($group, !empty($this->options['exclude_members']));
    }
    else {
      $users = $this->groupUsers->getSuggestedUsersForGroup($group, !empty($this->options['exclude_members']));
    }

    // The service will return FALSE or TRUE in some circumstances. In
    // this case, and if there are no specified users, we supply an impossible
    // user id in order to force no results.
    $users = !is_array($users) || empty($users) ? [-10] : $users;
    $this->view->query->addWhere('AND', 'users_field_data.uid', $users, 'IN');
  }

  /**
   * Override the behavior of title(). Get the title of the group.
   */
  public function title() {
    $groups = $this->entityTypeManager->getStorage('group')->loadMultiple([(int) $this->argument]);
    $group = reset($groups);
    return $group->label();
  }

}
