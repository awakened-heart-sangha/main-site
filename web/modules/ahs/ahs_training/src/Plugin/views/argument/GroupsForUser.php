<?php

namespace Drupal\ahs_training\Plugin\views\argument;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides contextual filtering of groups eligible/suggested for a user.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("ahs_training_groups_for_user")
 */
class GroupsForUser extends ArgumentPluginBase {

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User groups.
   *
   * @var \Drupal\ahs_groups\UserGroupsManager
   */
  protected $groupUsers;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->groupUsers = $container->get('ahs_groups.user_groups_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['suggested'] = ['default' => FALSE];
    $options['add_memberships'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['suggested'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude groups not suggested to the user'),
      '#default_value' => !empty($this->options['suggested']),
    ];

    $form['add_memberships'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add groups the user is already a participant in'),
      '#default_value' => !empty($this->options['add_memberships']),
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $userGroupsManager = $this->groupUsers;
    $user = User::load($this->argument);
    if (!empty($this->options['suggested'])) {
      $groups = $userGroupsManager->getSuggestedGroupsForUser($user);
    }
    else {
      $groups = $userGroupsManager->getAvailableGroupsForUser($user);
    }

    // This relies on the view to filter out past groups if unwanted.
    if (!empty($this->options['add_memberships'])) {
      $participatingGroups = $userGroupsManager->getParticipatingGroupsForUser($user);
      $groups = $groups + $participatingGroups;
    }

    // If there are no groups, supply an impossible
    // group id to force no results.
    $groups = empty($groups) ? [-10] : $groups;
    $this->view->query->addWhere('AND', 'groups_field_data.id', array_keys($groups), 'IN');
  }

  /**
   * Override the behavior of title(). Get the name of the user.
   */
  public function title() {
    $users = $this->entityTypeManager->getStorage('user')->loadMultiple([(int) $this->argument]);
    $user = reset($users);
    return $user->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // This won't be adequate to allow other users to view someone else's
    // eligible/suggested groups. In displays that allow this, caching should
    // be turned off in the views UI.
    $contexts = parent::getCacheContexts();
    $contexts = Cache::mergeContexts($contexts, ['user.group_permissions']);
    return $contexts;
  }

}
