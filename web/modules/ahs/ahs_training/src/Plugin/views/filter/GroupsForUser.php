<?php

namespace Drupal\ahs_training\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides filtering of groups to those eligible/suggested for current user.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("ahs_training_groups_for_user")
 */
class GroupsForUser extends FilterPluginBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The user group manager.
   *
   * @var \Drupal\ahs_groups\UserGroupsManager
   */
  protected $userGroup;

  /**
   * The current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->currentUser = $container->get('current_user');
    $instance->userGroup = $container->get('ahs_groups.user_groups_manager');
    $instance->currentRequest = $container->get('request_stack')->getCurrentRequest();
    return $instance;
  }

  /**
   * Exposed filter options.
   *
   * @var bool
   */
  protected $alwaysMultiple = TRUE;

  /**
   * No operators.
   */
  public function operatorOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    if (empty($this->value) || $this->value === 'all') {
      return;
    }
    $userGroupsManager = $this->userGroup;
    $user = User::load($this->currentUser->id());
    if ($this->value === 'suggested') {
      $groups = $userGroupsManager->getSuggestedGroupsForUser($user);
    }
    elseif ($this->value === 'eligible') {
      $groups = $userGroupsManager->getAvailableGroupsForUser($user);
    }
    // If there are no groups, supply an impossible
    // group id to force no results.
    $groups = empty($groups) ? [-10] : $groups;
    $this->view->query->addWhere('AND', 'groups_field_data.id', array_keys($groups), 'IN');
  }

  /**
   * Provide a simple textfield for equality.
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $options = [
      'suggested' => 'Suggested for me',
      'eligible' => 'Eligible for me',
      'all' => 'Any',
    ];
    if ($this->currentUser->isAnonymous()) {
      $options = [
        'suggested' => 'Suggested for everyone',
        'eligible' => 'Eligible for everyone',
        'all' => 'Any',
      ];
    }
    $form['value'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => empty($this->value) ? 'suggested' : $this->value,
    ];
    if ($this->currentUser->isAnonymous()) {
      $url = Url::fromRoute('user.login', [], ['query' => ['destination' => $this->currentRequest->getRequestUri()]])->toString();
      $form['value']['#description'] = '<a href="' . $url . '">Login</a> to get personalised suggestions.';
    }
  }

}
