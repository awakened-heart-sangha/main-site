<?php

namespace Drupal\ahs_training\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\ahs_training\CascadingRecordsUpdater;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a queue job to update cascading records for a user.
 *
 * @AdvancedQueueJobType(
 *   id = "ahs_training_update_cascading_records_for_user",
 *   label = @Translation("Update cascading records for user"),
 * )
 */
class UpdateCascadingRecordsForUser extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cascading records updater service.
   *
   * @var \Drupal\ahs_training\CascadingRecordsUpdater
   */
  protected $cascadingRecordsUpdater;

  /**
   * Constructs a new object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\ahs_training\CascadingRecordsUpdater $cascading_records_updater
   *   The cascading records updater.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, CascadingRecordsUpdater $cascading_records_updater) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->cascadingRecordsUpdater = $cascading_records_updater;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('ahs_training.cascading_records_updater')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    $uid = $job->getPayload()['uid'];
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($uid);

    // The user could have been deleted.
    if (!$user) {
      return JobResult::failure($this->t('No user @user found', ['@user' => $uid]));
    }
    $this->cascadingRecordsUpdater->updateForUser($uid);
    return JobResult::success($this->t('Cascading experience records updated for @user', ['@user' => $user->label()]));
  }

}
