<?php

namespace Drupal\ahs_training\Controller;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Routing\ViewPageController;

/**
 * Provides view controller for the taxonomy terms.
 *
 * In Drupal 8 the taxonomy term pages are views. By default the View
 * “taxonomy_term” is used if a taxonomy term is requested (/taxonomy/term/TID).
 * This view can be adjusted to fit your needs. But sometimes you like to
 * change the output / view only one some terms or for all terms in some
 * vocabulary.
 *
 * Currently there is no easy way to do that. Because always this one view is
 * used and all changes inside the view has an impact on all terms. And if more
 * views are on the system path /taxonomy/term/{taxonomy_term}, also only one is
 * used for all terms. No way to say, please use this view for the particular
 * vocabulary otherwise this view.
 *
 * The approach used here is to use a routeSubscriber to intercept the canonical
 * route and send it to this alternative View controller.
 *
 * Copied from https://wiki.cbeier.net/en/webworking/cms/drupal/drupal8/snippets/use_different_views_for_various_vocabularies
 */
class TaxonomyTermViewPageController extends ViewPageController {

  /**
   * {@inheritdoc}
   */
  public function handle($view_id, $display_id, RouteMatchInterface $route_match) {
    // Drupal Defaults.
    $view_id = 'taxonomy_term';
    $display_id = 'page_1';

    // Entity of the requested term.
    $term = $route_match->getParameter('taxonomy_term');

    // Get the vid (vocabulary machine name) of the current term.
    $vid = $term->get('vid')->target_id;

    // Use a non-standard view for the trainings vocabulary.
    if ($vid === 'trainings') {
      // Change view.
      $view_id = 'trainings';
    }

    return parent::handle($view_id, $display_id, $route_match);
  }

}
