<?php

namespace Drupal\ahs_training\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\group\Entity\GroupInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Paragraph Pages controller.
 */
class ParagraphPagesController extends ControllerBase {

  /**
   * The render service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * List of links to course units.
   */
  public function units(GroupInterface $group) {
    $units = $group->field_unit->getValue();
    $links = [];
    foreach ($units as $unit) {
      $p = Paragraph::load($unit['target_id']);
      $id = array_column($p->id->getValue(), 'value');
      $title = array_column($p->field_title->getValue(), 'value');

      // Drupal core saves an empty paragraph if no paragraph is added
      // to a group. Therefore, only include items with titles.
      if (!empty($title)) {
        $links[] = Link::createFromRoute($title[0], 'ahs_training.unit_pages', ['pid' => $id[0]]);
      }
    }

    if (empty($links)) {
      $content = [
        '#type' => 'markup',
        '#markup' => 'This group does not have any units. Edit the group to add them.',
      ];
    }
    else {
      $content = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => 'Course units',
        '#items' => $links,
      ];
    }

    return $content;

  }

  /**
   * Render a single paragraph item (Course Unit).
   */
  public function renderParagraph($pid) {
    $entity_type = 'paragraph';
    $view_builder = $this->entityTypeManager()->getViewBuilder($entity_type);
    $entity = $this->entityTypeManager()->getStorage($entity_type)->load($pid);
    $build = $view_builder->view($entity, 'full');

    return [
      '#type' => 'markup',
      '#markup' => $this->renderer->render($build),
    ];
  }

  /**
   * Check access to a course unit.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Access results.
   */
  public function access() {
    return AccessResult::allowedIf($this->hasMentorGrantedAccess() && $this->hasMemberCompletedPreviousUnit());
  }

  /**
   * Stub for logic to stop participants from jumping ahead.
   */
  private function hasMemberCompletedPreviousUnit() {
    // Idea: use flag module. Participants could flag
    // that they have completed each unit.
    // Don't allow access if they did not flag the previous unit.
    return TRUE;
  }

  /**
   * Stub for logic to allow restrictions.
   *
   * It based on whether mentor has allowed this participant access.
   */
  private function hasMentorGrantedAccess() {
    // Idea: use fields on group membership object
    // (/admin/group/content/manage/course-group_membership/fields)
    // Checkbox for each Unit. Mentor can tick which
    // Units a member has access to.
    return TRUE;
  }

}
