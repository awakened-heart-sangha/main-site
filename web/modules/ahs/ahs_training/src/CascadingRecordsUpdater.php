<?php

namespace Drupal\ahs_training;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 * Update cascading records that are granted on the basis of other records.
 *
 * @package Drupal\ahs_training
 */
class CascadingRecordsUpdater {

  /**
   * An array of nodes of the 'training_experience' type that can be cascaded.
   *
   * @var array
   */
  protected static $cascadableExperiences;

  /**
   * The user experience evaluator service.
   *
   * @var \Drupal\ahs_training\UserExperienceEvaluator
   */
  protected $userExperienceEvaluator;

  /**
   * The group users manager service.
   *
   * @var \Drupal\ahs_training\GroupUsersManager
   */
  protected $groupUsersManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new CascadingRecordsUpdater object.
   */
  public function __construct(UserExperienceEvaluator $user_experience_evaluator, GroupUsersManager $group_users_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->userExperienceEvaluator = $user_experience_evaluator;
    $this->groupUsersManager = $group_users_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Update the cascading experiences for a user.
   *
   * This determines what cascading experiences the user should have,
   * and on that basis then deletes any cascaded records that the user
   * should no longer have and creates new ones if needed.
   *
   * @param int $uid
   *   A user id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateForUser($uid) {
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    if (!$user) {
      return;
    }
    // Iterate over cascadable experiences build an array of cascaded
    // experiences for the user, carrying on  until there is no more change.
    $cascadedExperiences = $this->getCascadingExperiencesForUser($uid);

    // CRUD new training records as needed for the cascaded experiences.
    $storedExperienceIds = [];
    $storage = $this->entityTypeManager->getStorage('training_record');
    $storedCascadedRecords = $storage->loadByProperties([
      'uid' => $uid,
      'is_cascaded' => TRUE,
    ]);

    // Iterate over existing cascading  records, comparing with new calculation.
    foreach ($storedCascadedRecords as $record) {
      $storedExperienceId = $record->getExperienceId();
      $storedExperienceIds[] = $storedExperienceId;

      // Delete cascaded records that are no longer appropriate.
      if (!in_array($storedExperienceId, array_keys($cascadedExperiences))) {
        $record->delete();
      }

      // Update the effective date where necessary.
      elseif ($cascadedExperiences[$storedExperienceId] !== $record->getEffectiveTime()) {
        $record->set('effective_time', $cascadedExperiences[$storedExperienceId]);
        $record->save();
      }
    }

    // Iterate over new cascading experiences, checking for any where a record
    // needs to be created.
    foreach ($cascadedExperiences as $experienceId => $effectiveTimestamp) {
      if (!in_array($experienceId, $storedExperienceIds)) {
        $record = $storage->create([
          'uid' => $uid,
          'experience' => $experienceId,
          'effective_time' => $effectiveTimestamp,
          'note' => 'Given automatically from other experiences',
          'is_cascaded' => TRUE,
        ]);
        $record->save();
      }
    }
  }

  /**
   * Initiate an updating of the cascading records for all users.
   *
   * @param \Drupal\node\NodeInterface $experience
   *   Node entity.
   */
  public function updateForExperience(NodeInterface $experience) {
    // This method is only called if an experience has changed in some way,
    // so we will need to reset the cascadable experiences static cache.
    self::$cascadableExperiences = NULL;

    // Get users effected by the cascade of this experience.
    $usersHavingExperienceAlready = $this->groupUsersManager->getUsersWithExperience($experience->id(), NULL, 1, TRUE);
    $usersMeetingConditions = $this->groupUsersManager->getUsersMeetingConditions($experience->get('field_automatic_conditions')->referencedEntities());
    if (!is_array($usersMeetingConditions)) {
      return;
    }
    $uids = array_merge($usersHavingExperienceAlready, $usersMeetingConditions);

    // Queue an update job for each affected user.
    $queue = $this->getCascadingQueue();
    foreach ($uids as $uid) {
      $job = Job::create('ahs_training_update_cascading_records_for_user', [
        'uid' => $uid,
      ]);
      $queue->enqueueJob($job);
    }
  }

  /**
   * Helper to get experiences.
   */
  protected function getCascadableExperiences() {
    if (is_null(self::$cascadableExperiences)) {
      $storage = $this->entityTypeManager->getStorage('node');
      $storage->resetCache();
      $experienceIds = $storage
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('type', 'training_experience')
        ->exists('field_automatic_conditions')
        ->execute();
      self::$cascadableExperiences = $storage->loadMultiple($experienceIds);
    }
    return self::$cascadableExperiences;
  }

  /**
   * Helper to get experiences for user.
   */
  public function getCascadingExperiencesForUser($uid) {
    $cascadedExperiences = [];
    $cascadableExperiences = $this->getCascadableExperiences();
    $updated = TRUE;
    // Keep iterating over cascadable experiences until the cascade stabilises.
    while ($updated) {
      $updated = FALSE;
      foreach ($cascadableExperiences as $experience) {
        $conditions = $experience->get('field_automatic_conditions')->referencedEntities();
        // $hasExperience could be a boolean or a timestamp
        $hasExperience = $this->userExperienceEvaluator->isAnyConditionMetByUser($conditions, $uid, NULL, TRUE);
        // Implement the cascade immediately in the cache, even though it's not
        // yet true in the database.
        $this->userExperienceEvaluator->updateUserExperienceCache($uid, $experience->id(), $hasExperience);
        $newEffectiveTimestamp = ($hasExperience === TRUE) ? 0 : $hasExperience;
        $updateExperience = $hasExperience && (!isset($cascadedExperiences[$experience->id()]) || $cascadedExperiences[$experience->id()] > $newEffectiveTimestamp);
        if ($updateExperience) {
          $updated = TRUE;
          $cascadedExperiences[$experience->id()] = $newEffectiveTimestamp;
        }
      }
    }
    return $cascadedExperiences;
  }

  /**
   * Get an advanced queue, or create if if it doesn't exist.
   *
   * @return \Drupal\advancedqueue\Entity\QueueInterface|null
   *   The specified advanced queue, or NULL if Advanced Queue is not installed.
   */
  protected function getCascadingQueue() {
    if (($queue = Queue::load('ahs_training_update_cascading_records_for_user')) == NULL) {
      // Add new queue.
      $data = [
        'id' => 'ahs_training_update_cascading_records_for_user',
        'label' => 'Update cascading training records for user',
        'backend' => 'database',
        'backend_configuration' => ['lease_time' => 60],
        'processor' => 'cron',
        'processing_time' => 60,
        'locked' => TRUE,
      ];
      $queue = Queue::create($data);
      $queue->save();
    }
    return $queue;
  }

}
