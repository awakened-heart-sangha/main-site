<?php

/**
 * @file
 * Contains commerce_claim_gift_aid.module.
 */

use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_entity_base_field_info_alter().
 */
function commerce_claim_gift_aid_entity_base_field_info_alter(array &$fields, EntityTypeInterface $entity_type) {

  if ($entity_type->id() == 'commerce_order') {
    $info = [
      'commerce_order' => t('Order has gift aid'),
    ];

    // Adds a gift aid field onto orders.
    $field_name = 'gift_aid';
    $fields[$field_name] = BaseFieldDefinition::create('boolean')
      ->setName($field_name)
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel($info[$entity_type->id()])
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValue(FALSE);
  }

  if ($entity_type->id() == 'commerce_product') {
    $info = [
      'commerce_product' => t('This is a donation eligible for gift aid'),
    ];

    // Adds a gift aid field onto products.
    $field_name = 'gift_aid';
    $fields[$field_name] = BaseFieldDefinition::create('boolean')
      ->setName($field_name)
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel($info[$entity_type->id()])
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValue(FALSE);
  }

  if ($entity_type->id() == 'commerce_store') {

    // Adds a gift aid enabled field onto stores.
    $field_name = 'gift_aid_enabled';
    $fields[$field_name] = BaseFieldDefinition::create('boolean')
      ->setName($field_name)
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Enable gift aid customizations for this store'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValue(FALSE);

    // Adds a gift aid text field onto stores.
    $field_name = 'gift_aid_text';
    $fields[$field_name] = BaseFieldDefinition::create('text_long')
      ->setName($field_name)
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Gift aid text'))
      ->setDescription(t('Enter some text which will appear on the order checkout pane'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE);

    // Charity Author field.
    $field_name = 'gift_aid_charity';
    $fields[$field_name] = BaseFieldDefinition::create('entity_reference')
      ->setName($field_name)
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Default charity'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'gift_aid_charity')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'settings' => ['link' => FALSE],
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Declaration type field.
    $field_name = 'gift_aid_declaration_type';
    $fields[$field_name] = BaseFieldDefinition::create('entity_reference')
      ->setName($field_name)
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Default declaration type'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'gift_aid_declaration_type')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'settings' => ['link' => FALSE],
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for 'commerce_order_item_type_form'.
 */
function commerce_claim_gift_aid_form_commerce_order_item_type_form_alter(array &$form, FormStateInterface $form_state) {
  /** @var \Drupal\commerce_order\Entity\OrderItemType $order_item_type */
  $order_item_type = $form_state->getFormObject()->getEntity();
  $form['gift_aid'] = [
    '#type' => 'radios',
    '#title' => t('Order item is eligible for gift aid?'),
    '#weight' => 10,
    '#default_value' => $order_item_type->getThirdPartySetting('commerce_claim_gift_aid', 'gift_aid'),
    '#options' => \Drupal::service('commerce_claim_gift_aid.order_item_eligibility')->getOptions(),
  ];
  $form['#entity_builders'][] = 'commerce_claim_gift_aid_add';
}

/**
 * Implements hook_form_FORM_ID_alter() for 'commerce_product_form'.
 */
function commerce_claim_gift_aid_form_commerce_product_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();
  $entity = $form_object->getEntity();
  $product_type = ProductType::load($entity->bundle());
  $product_variation_type = ProductVariationType::load($product_type->getVariationTypeId());
  $order_item_type = OrderItemType::load($product_variation_type->getOrderItemTypeId());
  $form['gift_aid']['#access'] = Drupal::service('commerce_claim_gift_aid.order_item_eligibility')->availableAsGift($order_item_type);
}

/**
 * Handler for commerce_gift_aid_form_commerce_order_item_type_form_alter().
 *
 * @param string $entity_type
 *   The entity type as a string.
 * @param \Drupal\commerce_order\Entity\OrderItemType $commerce_order_item_type
 *   The config entity OrderItemType.
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function commerce_claim_gift_aid_add($entity_type, OrderItemType $commerce_order_item_type, array &$form, FormStateInterface $form_state) {
  if ($entity_type == 'commerce_order_item_type') {
    $gift_aid = $form_state->getValue(['gift_aid']);
    $commerce_order_item_type->setThirdPartySetting('commerce_claim_gift_aid', 'gift_aid', $gift_aid);
    $commerce_order_item_type->save();
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for 'commerce_store_form'.
 */
function commerce_claim_gift_aid_form_commerce_store_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  // Add fieldset - container for gift aid fields.
  $form['commerce_claim_gift_aid'] = [
    '#type' => 'details',
    '#title' => t('Commerce claim gift aid'),
    '#open' => TRUE,
    '#weight' => 99,
  ];
  // @todo description is not shown.
  $form['gift_aid_text']['#description'] = t('You can view further on what you should include in your gift aid text <a target="_blank" href="@link">here</a>', [
    '@link' => 'https://www.gov.uk/claim-gift-aid',
  ]);

  // Add #states support.
  $gift_aid_enabled_checked = [
    'visible' => [
      'input[name="gift_aid_enabled[value]"]' => ['checked' => TRUE],
    ],
  ];
  $form['gift_aid_text']['#states'] = $gift_aid_enabled_checked;
  $form['gift_aid_charity']['#states'] = $gift_aid_enabled_checked;
  $form['gift_aid_declaration_type']['#states'] = $gift_aid_enabled_checked;

  // Adapt weights.
  $form['gift_aid_charity']['#weight'] = 4;
  $form['gift_aid_declaration_type']['#weight'] = 5;

  // Move fields to fieldset.
  $form['commerce_claim_gift_aid']['gift_aid_enabled'] = $form['gift_aid_enabled'];
  $form['commerce_claim_gift_aid']['gift_aid_text'] = $form['gift_aid_text'];
  $form['commerce_claim_gift_aid']['gift_aid_charity'] = $form['gift_aid_charity'];
  $form['commerce_claim_gift_aid']['gift_aid_declaration_type'] = $form['gift_aid_declaration_type'];
  unset($form['gift_aid_enabled']);
  unset($form['gift_aid_text']);
  unset($form['gift_aid_charity']);
  unset($form['gift_aid_declaration_type']);
}
