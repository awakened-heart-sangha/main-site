<?php

namespace Drupal\commerce_claim_gift_aid\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_claim_gift_aid\Services\OrderItemEligibility;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_store\Entity\Store;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the gift aid information pane.
 *
 * @CommerceCheckoutPane(
 *   id = "claim_gift_aid",
 *   label = @Translation("Gift Aid Declaration"),
 *   default_step = "review",
 *   wrapper_element = "fieldset",
 * )
 */
class ClaimGiftAid extends CheckoutPaneBase {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The order item eligibility service.
   *
   * @var \Drupal\commerce_claim_gift_aid\Services\OrderItemEligibility
   */
  protected $orderItemEligibility;

  /**
   * Constructs a new ClaimGiftAid object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, Token $token, OrderItemEligibility $order_item) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
    $this->token = $token;
    $this->orderItemEligibility = $order_item;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('commerce_claim_gift_aid.order_item_eligibility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Obtain gift aid text.
    $store = $this->order->getStore();
    $charity = $store->gift_aid_charity->entity;
    $gift_aid_text = $store->get('gift_aid_text')->getValue()[0]['value'];
    $gift_aid_text = $this->token->replace($gift_aid_text, [
      'gift_aid_charity' => $charity,
    ]);

    // Form.
    $pane_form['gift_aid_declaration'] = [
      '#type' => 'checkbox',
      '#title' => $gift_aid_text,
      '#default_value' => $this->order->get('gift_aid')->value,
    ];

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValues();
    $gift_aid = !empty($values['claim_gift_aid']['gift_aid_declaration']) ? 1 : 0;
    // Set the claim_gift_aid value on the order as true or false.
    $this->order->set('gift_aid', $gift_aid);
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    // Only show the gift aid pane if the order item type is eligible for
    // gift aid.
    $any_gift_aid_items = FALSE;
    $order_items = $this->order->getItems();
    foreach ($order_items as $item) {
      $config_entity = OrderItemType::load($item->bundle());
      $product = Product::load($item->purchased_entity->target_id);

      // Check if the store uses gift_aid.
      $order_id = $item->get('order_id')->getValue()[0]['target_id'];
      $order = Order::load($order_id);
      $store_id = $order->get('store_id')->getValue()[0]['target_id'];
      $store = Store::load($store_id);
      $store_gift_aid_enabled = $store->get('gift_aid_enabled')->getValue()[0]['value'];

      if ($store_gift_aid_enabled && $this->orderItemEligibility
        ->isEligible($config_entity, $product)
      ) {
        $any_gift_aid_items = TRUE;
        break;
      }
    }
    return $any_gift_aid_items;
  }

}
