<?php

namespace Drupal\commerce_claim_gift_aid\Plugin\GiftAidDeclarer;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerDefault;

/**
 * Provides a Order Gift Aid Declarer.
 *
 * The plugin use the Commerce Order.
 *
 * @GiftAidDeclarer(
 *   id = "gift_aid_declarer_order",
 *   label = @Translation("Gift Aid Declarer Order"),
 * )
 */
class GiftAidDeclarerOrder extends GiftAidDeclarerDefault implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];

    // Configure the Declarer field.
    $fields['declarer'] = clone $base_field_definitions['declarer'];
    $fields['declarer']->setSettings([
      'target_type' => 'commerce_order',
      'handler' => 'default:commerce_order',
      'handler_settings' => [
        'include_anonymous' => FALSE,
        'target_bundles' => NULL,
        'filter' => [
          'type' => '_none',
        ],
        'sort' => [
          'field' => '_none',
        ],
        'auto_create' => FALSE,
      ],
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getLegalName() {
    /** @var \Drupal\commerce_order\Entity\Order $declarer */
    $declarer = $this->getDeclarer();
    return 'Order number ' . $declarer->getOrderNumber();
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxAddress() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getMail() {
    /** @var \Drupal\commerce_order\Entity\Order $declarer */
    $declarer = $this->getDeclarer();
    return $declarer->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function viewDeclarer() {
    $declaration = $this->getDeclaration();
    $declarer = $this->getDeclarer();

    // No return when user doesn't have an access to view declarer.
    if (FALSE === $declarer->access('view')) {
      return NULL;
    }

    // Build render wrapper.
    $output = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'declarer-view',
          'declarer-view--declaration--' . $declaration->bundle(),
          'declarer-view--plugin--' . $this->getPluginId(),
        ],
      ],
      '#cache' => [
        'tags' => $declarer->getCacheTags(),
        'contexts' => $declarer->getCacheContexts(),
        'max-age' => $declarer->getCacheMaxAge(),
      ],
    ];

    $output['fields'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        ['#markup' => $this->t('Declarer Name: @name', ['@name' => $this->getLegalName()])],
        ['#markup' => $this->t('Declarer Mail: @mail', ['@mail' => $this->getMail()])],
      ],
    ];

    return $output;
  }

}
