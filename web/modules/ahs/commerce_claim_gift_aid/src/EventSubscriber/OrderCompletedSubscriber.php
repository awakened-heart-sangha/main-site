<?php

namespace Drupal\commerce_claim_gift_aid\EventSubscriber;

use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OrderCompletedSubscriber. Provides base class for order subscriber.
 */
class OrderCompletedSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new OrderCompletedSubscriber object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   */
  public function __construct(MessengerInterface $messenger, TranslationInterface $string_translation) {
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      OrderEvents::ORDER_UPDATE => ['onOrderUpdate'],
    ];
    return $events;
  }

  /**
   * Places the order after it has been fully paid through an off-site gateway.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The event.
   */
  public function onOrderUpdate(OrderEvent $event) {
    $order = $event->getOrder();
    $order_state_completed = $order->getState()->value == 'completed';
    $gift_aid_checked = $order->gift_aid->value;
    if ($order_state_completed && $gift_aid_checked) {
      $store = $order->getStore();
      $charity = $store->gift_aid_charity->entity;
      $declaration_type = $store->gift_aid_declaration_type->entity;
      $declaration = gift_aid_declaration_create($charity, $declaration_type, $order);
      $this->messenger->addMessage($this->t('A new declaration @id was created.', [
        '@id' => $declaration->id(),
      ]));
    }
  }

}
