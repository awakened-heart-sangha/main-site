<?php

namespace Drupal\commerce_claim_gift_aid\Services;

use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Tests if an order item is gift aid eligible.
 */
class OrderItemEligibility {
  use StringTranslationTrait;

  /**
   * Gift aid options.
   *
   * @var array
   */
  private $options = [];

  /**
   * Constructs a new OrderItemEligibility object.
   */
  public function __construct() {
    $this->options = [
      1 => $this->t('Always'),
      2 => $this->t('Sometimes'),
      0 => $this->t('Never'),
    ];
  }

  /**
   * Get options.
   *
   * @return array
   *   Options array.
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * Checks if product is available as gift.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemType $commerce_order_item_type
   *   The config entity OrderItemType.
   *
   * @return bool
   *   TRUE if the product is available as gift, FALSE otherwise.
   */
  public function availableAsGift(OrderItemType $commerce_order_item_type) {
    if ($commerce_order_item_type instanceof OrderItemType) {
      // Show product 'This is a donation eligible for gift aid' field when
      // 'Order item is eligible for gift aid' commerce order field has status
      // 'Sometimes'.
      if ($commerce_order_item_type->getThirdPartySetting('commerce_claim_gift_aid', 'gift_aid') == 2) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Checks if order is eligible.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemType $commerce_order_item_type
   *   The config entity OrderItemType.
   * @param \Drupal\commerce_product\Entity\Product $product
   *   The product.
   *
   * @return bool
   *   TRUE if the order is eligible, FALSE otherwise.
   */
  public function isEligible(OrderItemType $commerce_order_item_type, Product $product) {
    $gift_aid = $commerce_order_item_type->getThirdPartySetting('commerce_claim_gift_aid', 'gift_aid');

    if ($commerce_order_item_type instanceof OrderItemType) {
      // Show access to make declaration (checkbox on checkout panel) for order
      // when 'Order item is eligible for gift aid' has status 'Always'.
      if ($gift_aid == 1) {
        return TRUE;
      }

      // Show access to make declaration (checkbox on checkout panel) for order
      // when 'Order item is eligible for gift aid' has status 'Sometimes' and
      // product has checked 'This is a donation eligible for gift aid'
      // (gift_aid) field.
      if ($gift_aid == 2 && $product->gift_aid->value) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
