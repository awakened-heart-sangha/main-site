<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\MiscDonation;

/**
 * @group ClonedDatabaseRequired
 * @group js
 */
class MiscDonationFromPageTest extends MiscDonationTestBase {

  protected $donationFormUrl = "donations-and-payments";
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Misc donation from specific page
    In order for people to be ale to use the misc donations form
    It needs to be on a page somewhere

    Scenario: Misc donation from page
      When I submit a miscellaneous form
      And I provide my email
      Then a miscellaneous order is started for me

FEATURE;

  // Don't mess with the webform and products as we're on the production db.
  protected function setUpWebform() {}

}

