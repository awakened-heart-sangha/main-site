<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\MiscDonation;

use Drupal\profile\Entity\ProfileType;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * @group js
 */
class MiscDonationUserTest extends MiscDonationTestBase {

  protected $anonymousProfileCount;
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Misc donation checkout with payment
    In order for us to know who is making donations
    They need to be tracked against users

    Scenario: New users can donate
      When I submit a miscellaneous form
      And I provide my email
      Then a miscellaneous order is started for me
      And a new user should have been created for me
      When I complete the miscellaneous payment steps
      Then the order should be in the paid state
      And my user should be the customer on the miscellaneous order
      And a payment should be received from me
      And I should have the right profiles

    Scenario: Existing decoupled users can donate
      Given I am already a user but without a password
      When I submit a miscellaneous form
      And I provide my email
      Then a miscellaneous order is started for me
      When I complete the miscellaneous payment steps
      Then the order should be in the paid state
      And my user should be the customer on the miscellaneous order
      And a payment should be received from me
      And I should have the right profiles

    Scenario: Existing decoupled users are recognised by alternative email
      Given I am already a user but without a password
      And I have an alternative email
      When I submit a miscellaneous form
      And I provide my alternative email
      Then a miscellaneous order is started with my alternative email
      When I complete the miscellaneous payment steps
      Then the order should be in the paid state
      And my user should be the customer on the miscellaneous order
      And a payment should be received from me
      And I should have the right profiles

    Scenario: Existing coupled users who are already logged in can donate
      Given I am already a user with a password
      And I am logged in
      When I submit a miscellaneous form
      Then a miscellaneous order is started for me
      And my user should be the customer on the miscellaneous order
      When I complete the miscellaneous payment steps
      Then the order should be in the paid state
      And a payment should be received from me
      And I should have the right profiles

    Scenario: Existing coupled users who are not logged in can donate
      Given I am already a user with a password
      When I submit a miscellaneous form
      And I provide my email
      Then a miscellaneous order is started for me
      And I should not be required to login
      And my user should be the customer on the miscellaneous order
      When I complete the miscellaneous payment steps
      Then the order should be in the paid state
      And a payment should be received from me
      And I should have the right profiles

    Scenario: Users can donate repeatedly
      When I submit a miscellaneous form
      And I provide my email
      Then a miscellaneous order is started for me
      And a new user should have been created for me
      When I complete the miscellaneous payment steps
      Then the order should be in the paid state
      And my user should be the customer on the miscellaneous order
      And a payment should be received from me
      And I should have the right profiles
      When I submit a miscellaneous form
      And I provide my email
      Then a miscellaneous order is started for me
      When I complete the miscellaneous payment steps
      Then the order should be in the paid state
      And my user should be the customer on the miscellaneous order
      And a payment should be received from me
      And I should have the right profiles

    Scenario Outline: Donations have correct amounts
      When I submit a miscellaneous form specifying "<amount>"
      And I provide my email
      Then a miscellaneous order is started for me
      When I complete the miscellaneous payment steps
      Then a payment of "<amount>" should be received from me
      And I should have the right profiles

      Examples:
        | amount |
        | 30     |
        | 50     |

FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->anonymousProfileCount = $this->getAnonymousProfileCount();
  }

  /**
   * @Then a payment should be received from me
   * @Then a payment of :amount should be received from me
   */
  public function aDonationShouldBeReceivedFromMe($amount = NULL) {
    $amount = is_null($amount) ? $this->amount : $amount;
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertOrderPayment($order, $amount);
  }

  /**
   * @Then my user should be the customer on the miscellaneous order
   */
  public function myUserShouldBeTheCustomerOnTheMiscellaneousOrder() {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertGreaterThan(0, $order->getCustomerId(), "The order should have a customer.");
    $this->assertSame($this->getMe()->id(), $order->getCustomerId(), "The order should have the specificied user as the customer.");
  }

  /**
   * @Then there should not yet be a customer on the miscellaneous order
   */
  public function thereShouldNotYetBeACustomerOnTheMiscellaneousOrder() {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertEmpty($order->getCustomerId(), "The order should not yet have a customer.");
  }

  /**
   * @When I provide my alternative email
   */
  public function iProvideMyAlternativeEmail() {
    $this->assertSession()->pageTextContains('Email');
    $formValues = [
      'mail' => $this->getMyAlternativeEmail(TRUE),
    ];
    $this->submitForm($formValues, 'Continue to next step');
  }

  /**
   * @Then a miscellaneous order is started with my alternative email
   */
  public function aMiscellaneousOrderIsStartedWithMyAlternativeEmail() {
    $this->orderId = $this->assertOrderByEmail(
      $this->getMyAlternativeEmail(),
      'ahs_miscellaneous',
      'ahs_miscellaneous',
      $this->amount,
      [$this->miscDonationSku],
      'draft')
      ->id();
  }

  /**
   * @Then I should have the right profiles
   */
  public function iShouldHaveTheRightProfiles() {
    $profile_storage = \Drupal::entityTypeManager()->getStorage('profile');
    $user = $this->loadEntityByProperties('user', ['mail' => $this->getMyEmail()]);
    $this->assertUserProfiles($user);

    $newAnonymousProfiles = $this->getAnonymousProfileCount() - $this->anonymousProfileCount;
    $this->assertSame(0, $newAnonymousProfiles, "No new anonymous profiles should have been created.");
  }

  protected function getAnonymousProfileCount() {
    $profile_storage = \Drupal::entityTypeManager()->getStorage('profile');
    $anon = User::getAnonymousUser();
    $profileTypes = ProfileType::loadMultiple();
    $count = 0;
    foreach($profileTypes as $profileType) {
      $profiles = $profile_storage->loadMultipleByUser($anon, $profileType->getEntityTypeId());
      $count += count($profiles);
    }
    return $count;
  }

}

