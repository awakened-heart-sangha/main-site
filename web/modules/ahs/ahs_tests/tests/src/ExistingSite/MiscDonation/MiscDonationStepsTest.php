<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\MiscDonation;

/**
 * @group js
 */
class MiscDonationStepsTest extends MiscDonationTestBase {

  protected static $feature = <<<'FEATURE'
Feature: Misc donation steps
    In order for students to make donations easily
    The checkout needs to have precise steps

    Scenario: All the donation steps are correct
      When I submit a miscellaneous form
      Then I should be on the Email step of the checkout
      And I should not see a Go Back link
      And I should not see the order summary
      When I provide my email
      Then I should be on the payment information step of the checkout
      And the button should say "Pay now"
      And I should not see a billing profile form
      And I should not see the order summary
      When I submit my credit card details
      And I should not see a Go Back link
      And I should be on the Thankyou step of the checkout

    Scenario: All the donation steps are correct with 3DS and card is accepted
      When I submit a miscellaneous form
      Then I should be on the Email step of the checkout
      And I should not see a Go Back link
      And I should not see the order summary
      When I provide my email
      Then I should be on the payment information step of the checkout
      And the button should say "Pay now"
      And I should not see a billing profile form
      And I should not see the order summary
      When I submit my accepted credit card details that require 3DS
      Then I should see the 3DS review step
      #And I authorize the 3DS payment
      #And I should not see a Go Back link
      #And I should be on the Thankyou step of the checkout

    Scenario: All the donation steps are correct with 3DS and card is declined
      When I submit a miscellaneous form
      Then I should be on the Email step of the checkout
      And I should not see a Go Back link
      And I should not see the order summary
      When I provide my email
      Then I should be on the payment information step of the checkout
      And the button should say "Pay now"
      And I should not see a billing profile form
      And I should not see the order summary
      When I submit my declined credit card details that require 3DS
      Then I should see the 3DS review step
      #And I reject the 3DS payment
      #And I should not see a Go Back link
      #And I should be on the Thankyou step of the checkout

FEATURE;

  /**
   * @Then I should see the 3DS review step
   */
  public function iShouldSeeThe3DSReviewStep() {
    $this->assertSession()->pageTextContains('Reviewing...');

    $frameId = $this->assertWaitForFrameStartingWith('__privateStripeFrame');
    $this->getSession()->switchToIFrame($frameId);
    sleep(1);
  }

  /**
   * Waits for a frame to become available and then switches to it.
   *
   * @param string $name
   *   The frame name.
   * @param int $wait
   *   The wait time, in seconds.
   *
   * @return bool
   *   Returns TRUE if operation succeeds.
   *
   * @throws \Exception
   */
  public function assertWaitForFrameStartingWith($name, $wait = 20) {
    $last_exception = NULL;
    $stopTime = time() + $wait;
    while (time() < $stopTime) {
      try {
        $element = $this->assertSession()->elementExists('xpath', "//iframe[starts-with(@id,'$name') or starts-with(@name, '$name')]");
        return $element->getAttribute('id');
      }
      catch (\Exception $e) {
        // If the frame has not been found, keep waiting.
        $last_exception = $e;
      }
      usleep(250000);
    }
    throw $last_exception;
  }

  /**
   * @Then I reject the 3DS payment
   */
  public function iRejectThe3DSPayment() {
    // ID of the button is: test-source-fail-3ds
    // $this->getSession()->getPage()->pressButton('Fail');
    $this->getSession()->getPage()->pressButton('test-source-fail-3ds');

    $this->getSession()->switchToIFrame();
  }

  /**
   * @Then I authorize the 3DS payment
   */
  public function iAuthorizeThe3DSPayment() {
    // ID of the button is: test-source-authorize-3ds
    // $this->getSession()->getPage()->pressButton('Complete');
    $this->getSession()->getPage()->pressButton('test-source-authorize-3ds');
    $this->getSession()->switchToIFrame();
  }

  /**
   * @Then I should not see a Go Back link
   */
  public function iShouldNotSeeABackLink() {
    $this->assertSession()->pageTextNotContains('Go back');
  }

  /**
   * @Then I should not see the order summary
   */
  public function iShouldNotSeeTheOrderSummary() {
    $this->assertSession()->pageTextNotContains('Order Summary');
  }

  /**
   * @Then I should be on the Email step of the checkout
   */
  public function iShouldBeOnTheEmailStep() {
    $this->assertSession()->pageTextContains('Email');
  }

  /**
   * @Then the button should say :text
   */
  public function theButtonShouldSay($text) {
    $this->assertSession()->pageTextContains($text);
  }

}

