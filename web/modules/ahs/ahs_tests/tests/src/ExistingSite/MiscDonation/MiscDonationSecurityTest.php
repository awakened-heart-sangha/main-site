<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\MiscDonation;

use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

/**
 * @group js
 */
class MiscDonationSecurityTest extends MiscDonationTestBase {

  use UserTrait;

  protected static $feature = <<<'FEATURE'
Feature: Checkout should not reveal payment methods without login
    In order for payment methods to be secure
    They should not be show to people who are not logged in

    Scenario: Returning users can see payment methods if logged in
      Given I am already a user with a password
      And I am logged in
      And I have a stored payment method
      When I submit a miscellaneous form
      Then I should be on the payment information step of the checkout
      And I should see an existing payment method

    Scenario: Payment methods for existing decoupled users are secure against anonymous users
      Given an existing user without a password but with a stored payment method
      When I submit a miscellaneous form
      And I provide the email of the existing user
      Then I should be on the payment information step of the checkout
      And I should not see an existing payment method

    Scenario: Payment methods for existing decoupled users are secure against users who log in
      Given an existing user without a password but with a stored payment method
      When I submit a miscellaneous form
      And I provide the email of the existing user
      And I log in by form as a different user
      Then I cannot return to the checkout

FEATURE;

  protected $existingUser;

  /**
   * @Given an existing user without a password but with a stored payment method
   */
  public function anExistingUserWithoutAPasswordButWithAStoredPaymentMethod() {
    $user = $this->createDecoupledUser();
    $this->existingUser = $user;
    $this->createStripePaymentMethodForUser($user);
  }

  /**
   * @Given an existing user with a password and a stored payment method
   */
  public function anExistingUserWithAPasswordAndAStoredPaymentMethod() {
    $user = $this->createUser();
    $email = $user->setEmail($this->randomEmail());
    $user->save();
    $this->existingUser = $user;
    $this->createStripePaymentMethodForUser($user);
  }

  /**
   * @Given I have a stored payment method
   */
  public function iHaveAStoredPaymentMethod() {
    $this->createStripePaymentMethodForUser($this->getMe());
  }

  /**
   * @When I provide the email of the existing user
   */
  public function iProvideTheEmailOfTheExistingUser() {
    $this->assertSession()->pageTextContains('Email');
    $formValues = [
      'mail' => $this->existingUser->getEmail(),
    ];
    $this->submitForm($formValues, 'Continue to next step');
    $order = $this->loadEntityByProperties('commerce_order', ['mail' => $this->existingUser->getEmail()]);
    $this->assertNotEmpty($order, "An order with the specified email should exist.");
  }

  /**
   * @When I reach the payment information step
   */
  public function whenIReachThePaymentInformationStep() {
    $this->iProvideMyEmail();
    $this->iShouldBeOnThePaymentInformationStepOfTheCheckout();
  }

  /**
   * @Then I should not see an existing payment method
   */
  public function iShouldNotSeeAnExistingPaymentMethod() {
    $this->assertSession()->pageTextNotContains('Payment method');
    $this->assertSession()->pageTextNotContains('ending in');
  }

  /**
   * @Then I should see an existing payment method
   */
  public function iShouldSeeAnExistingPaymentMethod() {
    $this->assertSession()->pageTextContains('Payment method');
    $this->assertSession()->pageTextContains('ending in');
  }

  /**
   * @When I log in by form as a different user
   */
  public function iLoginByFormAsADifferentUser() {
    // We don't use the normal login method from UserTrait as that relies on
    // drupalLogin() which messes with the session and stops the cart from being
    // carried over.

    // Make sure we log in as a new user not an existing one.
    $this->forgetMe();
    $this->coupleMe();

    $edit = array(
      'name' => $this->getMe()
        ->getAccountName(),
      'pass' => $this->getMe()->passRaw,
    );
    $this->drupalGet('user/login');
    $this->submitForm($edit, t('Log in'));
    $this->assertSession()->pageTextNotContains('Log in');
    $this->assertSession()->pageTextContains($this->getMe()->getDisplayName());
  }

  /**
   * @Then I cannot return to the checkout
   */
  public function iCannotReturnToTheCheckout() {
    $order = $this->loadEntityByProperties('commerce_order', ['mail' => $this->existingUser->getEmail()]);
    $this->assertNotEmpty($order, "An order with the specified email should exist.");
    $this->drupalGet('/checkout/' . $order->id() . '/payment_information');
    $this->assertSession()->pageTextNotContains('Payment information');
  }

}

