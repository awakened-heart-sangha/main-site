<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\MiscDonation;

use Drupal\Tests\ahs_tests\Traits\CommerceTrait;

/**
 * @group js
 */
class MiscDonationDeclinedTest extends MiscDonationTestBase {

  protected static $feature = <<<'FEATURE'
Feature: Misc donation declined
    In order for administrators to understand failed payments
    Payment errors need to be logged

    Scenario: Declined payments are logged
      When I submit a miscellaneous form
      And I provide my email
      # This step crashes the tests for unknown reasons
      # And I submit a credit card that will be declined
      #Then I should be on the payment information step of the checkout
      # And there should be a declined activity log

FEATURE;

  /**
   * @When I submit a credit card that will be declined
   */
  public function iSubmitACreditCardThatWillBeDeclined() {
    $this->submitCreditCardDetails('Pay now', CommerceTrait::$CARD_BASIC_DECLINED);
  }

  /**
   * @Then there should be a declined activity log
   */
  public function iShouldNotSeeTheOrderSummary() {
    $exceptionActivityLog = $this->loadEntityByProperties('commerce_log', [
      'source_entity_type' => 'commerce_order',
      'source_entity_id' => $this->orderId,
      'template_id' => 'ahs_commerce_stripe_exception'
    ]);
    $this->assertNotEmpty($exceptionActivityLog, "There should be an ahs_commerce_stripe_exception activity log");
  }

}

