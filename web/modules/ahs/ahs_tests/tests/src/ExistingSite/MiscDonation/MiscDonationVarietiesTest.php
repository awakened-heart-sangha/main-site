<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\MiscDonation;

use Behat\Gherkin\Node\TableNode;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\commerce_order\Entity\Order;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;

/**
 * @group NewDatabaseRequiredWithTrueTime
 * @group js
 */
class MiscDonationVarietiesTest extends MiscDonationTestBase {

  protected $storeId;

  protected static $feature = <<<'FEATURE'

Feature: Misc donation consequences
    In order for a good donation & payment experience
    Different types of donation and payment need to be treated differently

    Scenario: A miscellaneous donation
     When I go to the miscellaneous form
     Then none of the additional fields are visible
     When I select the option "misc_donation_miscellaneous" on the miscellaneous form
     #Then the additional field "notes_donation" becomes visible
     When I enter a note into the "notes_donation" field
     And I complete the miscellaneous form
     And I provide my email
     Then a miscellaneous order is started for me
     And the note is stored on the order item
     When I complete the miscellaneous payment steps
     Then a thankyou email is sent to me
     And the thankyou email mentions the note

    Scenario Outline: A donation or payment for an event
     Given a set of groups
     When I go to the miscellaneous form
     Then none of the additional fields are visible
     When I select the option "<sku>" on the miscellaneous form
     #Then the additional field "<field>" becomes visible
     And "<field>" lists only groups of the group type "<group_type>"
     And "<field>" does not list groups that ended in the far past
     And "<field>" lists groups requesting "<requesting>" that ended recently
     And "<field>" lists groups requesting "<requesting>" that are happening now
     And "<field>" lists groups requesting "<requesting>" that start soon
     And only for event payments are groups ending in the far future listed in "<field>"
     When I select the group of type "<group_type>" requesting "<requesting>" that is happening now on "<field>"
     And only for event payments I enter a note into the "notes" field
     And I complete the miscellaneous form
     And I provide my email
     Then a miscellaneous order is started for me
     And the group I selected is stored on the order item
     And only for event payments the note is stored on the order item
     When I complete the miscellaneous payment steps
     Then a thankyou email is sent to me
     And only for event payments the thankyou email mentions the group
     And only for event payments a notification email is sent to the event booking contacts
     And the notification email mentions the group when appropriate
     And the notification email mentions the note when appropriate

    Examples:
      | sku                           | field                 | group_type | requesting |
      | misc_donation_event_hermitage | event_donation        | Event      | donation   |
      | misc_payment_event            | event_payment         | Event      | payment    |
      | misc_donation_event_online    | event_donation_online | Event      | donation   |
      | misc_donation_course          | course_donation       | Course     | donation   |


FEATURE;

  // The selected group
  protected $group;

  // The note entered into the form.
  protected $note = "Ut non tortor vel est mollis congue.";

  protected $fields = [
    'event_donation',
    'event_payment',
    'event_donation_online',
    'course_donation',
    'notes',
    'notes_donation',
    ];

  // The corresponding HTML wrapper id or class per field
  // field_name => id or class
  // This is currently not used see assertVisibleFields().
  protected $field_wrappers = [
      'event_donation' => '#edit-event-donation--wrapper',
      'event_payment' => '#edit-event-payment--wrapper',
      'event_donation_online' => '#edit-event-donation-online--wrapper',
      'course_donation' => '#edit-course-donation--wrapper',
      'notes' => '.form-item-notes',
      'notes_donation' => '.form-item-notes-donation'
    ];

  protected function setUp(): void {
    parent::setUp();
    $this->ensureProductBySku('misc_donation_event_hermitage');
    //$this->ensureProductBySku('misc_donation_event');
    $this->ensureProductBySku('misc_donation_event_online');
    $this->ensureProductBySku('misc_donation_course');
    $this->ensureProductBySku('misc_donation_visit_hermitage');
    $this->ensureProductBySku('misc_payment_event');
  }

  /**
   * @Given a set of groups
   */
  public function aSetOfGroups() {

    // Possible times for events.
    $month = 60 * 60 * 24 * 30;
    $times = [
      // very far in future
      [
        'label' => 'very_far_future',
        'field_dates' => [
          'value' =>  new DrupalDateTime("+36 months"),
        ],
      ],
      // event quite far in future
      [
        'label' => 'far_future',
        'type' => 'event',
        'field_dates' => [
          'value' => new DrupalDateTime("+5 months"),
        ],
      ],
      // soon
      [
        'label' => 'soon',
        'field_dates' => [
          'value' => new DrupalDateTime("+14 days"),
        ],
      ],
      // now
      [
        'label' => 'now',
        'field_dates' => [
          'value' => new DrupalDateTime("-14 days"),
          'end_value' => new DrupalDateTime("+14 days"),
        ],
      ],
      // recent
      [
        'label' => 'recent',
        'field_dates' => [
          'value' => new DrupalDateTime("-14 days"),
        ],
        'field_donations' => FALSE,
      ],
      // long finished
      [
        'label' => 'far_past',
        'field_dates' => [
          'value' => new DrupalDateTime("-3 months"),
        ],
      ],
    ];

    // Complete date ranges
    foreach ($times as &$time) {
      if (!isset($time['field_dates']['end_value'])) {
        $time['field_dates']['end_value'] = $time['field_dates']['value']->modify("+1 day");
      }
    }

    // Combine different times with other group values
    $groups = [];
    foreach ($times as $time) {
      $dateRepetition = 0;
      // Payments only, donation only, or both
      foreach (['pay', 'don', 'both'] as $ask) {
        foreach (['event', 'course'] as $type) {
          if ($type == 'course' && $ask === 'pay') {
            continue;
          }
          $group = $time;
          $group['status'] = TRUE;
          $group['field_dates']['value'] = $group['field_dates']['value']->modify("+" . $dateRepetition . ' minutes');
          $dateRepetition = $dateRepetition + 1;
          if (!isset($time['field_dates']['end_value'])) {
            $group['field_dates']['end_value'] = $group['field_dates']['value']->modify("+1 day");
          }
          $group['field_dates']['value'] = $group['field_dates']['value']->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
          $group['field_dates']['end_value'] = $group['field_dates']['end_value']->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
          $group['type'] = $type;
          $group['label'] = $type. '_' . $ask . '_' . $group['label'];
          $group['status'] = TRUE;
          $group['field_payments'] = ($ask !== 'don');
          $group['field_donations'] = ($ask !== 'pay');
          $group['field_online'] = TRUE;
          $groups[] = $group;
        }
      }
    }


    // Create the actual group entities.
    $groupStorage = $this->entityTypeManager->getStorage('group');
    foreach ($groups as $group) {
      $groupEntity = $this->loadEntityByProperties('group', $group);
      if (empty($groupEntity)) {
        $groupEntity = $groupStorage->create($group);
        $groupEntity->enforceIsNew();
        $groupEntity->save();
        $this->markEntityForCleanup($groupEntity);
      }
    }
  }

  /**
   * @When I go to the miscellaneous form
   */
  public function iGoToTheMiscellaneousForm() {
    $this->drupalGet("/". $this->donationFormUrl);
    $this->assertSession()->addressEquals($this->donationFormUrl);
    $this->assertSession()->pageTextContains('What would you like to contribute');
  }

  /**
   * Currently this method is skipped.
   * TODO. Find a working solution for this.
   * @param array $visibleFields
   *
   * @throws \Behat\Mink\Exception\ElementHtmlException
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertVisibleFields(array $visibleFields) {
    foreach ($this->fields as $field) {

      if (in_array($field, $visibleFields)) {
        $this->assertSession()
          ->assertVisibleInViewport(
            'css',
            $this->field_wrappers[$field],
            FALSE,
            sprintf('Additional field "%s" should be visible.', $field)
          );
      }
      else {
        $this->assertSession()
          ->elementAttributeContains(
            'css',
            $this->field_wrappers[$field],
            'class',
            'js-webform-states-hidden'
          );
      }
    }
  }

  /**
   * @Then none of the additional fields are visible
   */
  public function noneOfTheAdditionalFieldsAreVisible() {
    $this->assertVisibleFields([]);
  }

  /**
   * @When I select the option :sku on the miscellaneous form
   * $this->selectRadio throws new Exception if not found.
   */
  public function iSelectTheOptionOnTheMiscellaneousForm($sku) {
    $this->miscDonationSku = $sku;
    $this->selectRadio('sku', $sku);
  }

  /**
   * @Then the additional field :field becomes visible
   */
  public function theAdditionalFieldBecomesVisible($field) {
    $this->assertVisibleFields([$field]);
  }

  /**
   * Check that ALL group options (radios) meet the requirements given.
   *
   * @param $field - The field containing the group options (radios) we are testing.
   * @param $partialTitle - A string to check the existence of in a group option title.
   * @param bool $mode - Are we checking for the existence or non-existence of a particular group option.
   */
  protected function assertGroupOptions($field, $partialTitle, $mode = TRUE) {
    $radios = $this->getRadioOptions($field);
    foreach ($radios as $value => $label) {
      if ($mode == TRUE) {
        $this->assertStringContainsString($partialTitle, $label, sprintf("The event title '%s' should contain '%s'", $label, $partialTitle), FALSE);
      }
      else {
        $this->assertFalse(stripos($label,$partialTitle), sprintf("The event title '%s' should not contain '%s'", $label, $partialTitle));
      }
    }
  }

  /**
   * Check whether a particular group option (radio) exists or not.
   * The different between this method and assertGroupOptions() is that this will not throw an exception for non-matches
   * Whilst assertGroupOptions() expects all group options found to match the parameters provided.
   *
   * @param $field - The field containing the group options (radios) we are testing.
   * @param $time - (i.e 'now', 'soon', 'recent')
   * @param $requesting - (don' or 'pay')
   * @param bool $mode - Should we check for the existence or non-existence of a particular group option.
   */
  protected function assertGroupOptionExists($field, $time, $requesting, $mode = TRUE) {
    $radios = $this->getRadioOptions($field);
    $count = 0;
    foreach ($radios as $value => $label) {
      if (stripos($label, $time) !== FALSE AND stripos($label, $requesting) !== FALSE) {
        $count ++;
      }
    }
    if ($mode == TRUE) {
      $this->assertGreaterThan(
        0,
        $count,
        sprintf("There should be a group option for the field '%s' with time '%s' and request '%s'", $field, $time, $requesting)
      );
    }
    elseif ($mode == FALSE) {
      $this->assertEquals(
        0,
        $count,
        sprintf("There should not be any instances of this group option for the field '%s' with time '%s' and request '%s'", $field, $time, $requesting)
      );
    }
  }

  /**
   *
   * Relay the correct 'requesting' value to the assertGroupOptionExists() method.
   * depending on product selected - i.e. 'donation' or 'payment'.
   *
   * @param $field - The field containing the group options (radios) we are testing.
   * @param $time - (i.e 'now', 'soon', 'recent')
   * @param $requesting - (i.e. 'donation' or 'payment')
   */
  protected function assertGroupOptionsRequesting($field, $time, $requesting) {
    if ($requesting == 'donation') {
      $this->assertGroupOptionExists($field, $time, 'don');
      $this->assertGroupOptionExists($field, $time, 'both');
    }
    elseif ($requesting === 'payment') {
      $this->assertGroupOptionExists($field, $time, 'pay');
      $this->assertGroupOptionExists($field, $time, 'both');
    }
  }

  /**
   * @Then :field lists only groups of the group type :type
   */
  public function listsOnlyGroupsOfTheGroupType2($field, $type) {
    $this->assertGroupOptions($field, $type);
  }

  /**
   * @Then :field does not list groups that ended in the far past
   */
  public function doesNotListGroupsThatEndedInTheFarPast($field) {
    $this->assertGroupOptions($field, 'far_past', FALSE);
  }

  /**
   * @Then :field lists groups requesting :requesting that ended recently
   */
  public function listsGroupsRequestingThatEndedRecently($field, $requesting) {
      $this->assertGroupOptionsRequesting($field, 'recent', $requesting);
  }

  /**
   * @Then :field lists groups requesting :requesting that are happening now
   */
  public function listsGroupsRequestingThatAreHappeningNow($field, $requesting) {
    $this->assertGroupOptionsRequesting($field, 'now', $requesting);
  }

  /**
   * @Then :field lists groups requesting :requesting that start soon
   */
  public function listsGroupsRequestingThatStartSoon($field, $requesting) {
    $this->assertGroupOptionsRequesting($field, 'soon', $requesting);
  }

  /**
   * @Then only for event payments are groups ending in the far future listed in :field
   */
  public function onlyForEventPaymentsAreGroupsEndingInTheFarFutureListedIn($field) {
    if ($this->miscDonationSku === 'misc_payment_event') {
      $this->assertGroupOptionsRequesting($field, 'far_future', 'payment');
    }
    else {
      $this->assertGroupOptions($field, 'far_future', FALSE);
    }
  }

  /**
   * @When I select the group of type :group_type requesting :requesting that is happening now on :field
   */
  public function iSelectTheGroupOfTypeRequestingThatIsHappeningNowOn($group, $requesting, $field) {

    $req = ($requesting == 'payment') ? 'pay' : 'don';
    $radios = $this->getRadioOptions($field);

    foreach ($radios as $value => $label) {
      if (stripos($label, 'now') !== FALSE AND stripos($label, $group) !== FALSE AND stripos($label, $req) !== FALSE) {
        $this->selectRadio($field, $value);
        $this->group = $value;
        return;
      }
    }
    throw new \Exception("There should be an option for a group '%s' of type '%s' that is happening now", $group, $requesting);
  }

  /**
   * @When I complete the miscellaneous form
   */
  public function iCompleteTheMiscellaneousForm() {

    // An arbitrary integer for the amount field.
    $this->amount = '1111';

    $this->waitToPassHoneypot();

    $page = $this->getCurrentPage();
    // Enter amount into amount field
    $page->fillField('How much would you like to give?', $this->amount);
    // Press submit :) and hope for the best
    $page->findButton('OK')->press();
    // Form submits successfully
    $this->assertSession()->pageTextNotContains('What would you like to contribute');
  }

  /**
   * @Then a thankyou email is sent to me
   */
  public function aThankyouEmailIsSentToMe() {
    $mails = $this->getMail(['to' => $this->getMyEmail()]);
    $this->assertCount(1, $mails, 'A thank you email should be sent to the customer');
  }

  /**
   * @Then the thankyou email mentions the note
   */
  public function theThankyouEmailMentionsTheNote() {
    $mails = $this->getMail(['to' => $this->getMyEmail()]);
    $properties = [
      'body' => [
        $this->note
      ],
    ];
    $this->assertTrue($this->matchesMail($mails['0'], $properties), "The order note should be present on the thank you email");
  }

  /**
   * @Then the note is stored on the order item
   */
  public function theNoteIsStoredOnTheOrderItem($mode = TRUE) {
    $order = $this->getOrder($this->orderId);
    $items = $order->getItems();
    $item = $items['0'];
    if ($mode === TRUE) {
      $this->assertNotEmpty($item->get('field_notes')->getString(), 'The order note should be present on the order');
      $this->assertEquals($item->get('field_notes')->getString(), $this->note, 'The order note should match what was entered into the form');
    }
    elseif ($mode === FALSE) {
      $this->assertEmpty($item->get('field_notes')->getString(), 'Only event payments should contain an order note');
    }
  }

  /**
   * @When only for event payments I enter a note into the :arg1 field
   */
  public function onlyForEventPaymentsIEnterANoteIntoTheField($field) {
    if ($this->miscDonationSku === 'misc_payment_event') {
      $this->iEnterANoteIntoTheField($field);
    }
  }

  /**
   * @When I enter a note into the :field field
   */
  public function iEnterANoteIntoTheField($field) {
    $page = $this->getCurrentPage();
    $page->fillField($field, $this->note);
  }

  /**
   * @Then the group I selected is stored on the order item
   */
  public function theGroupISelectedIsStoredOnTheOrderItem() {
    $order = Order::load($this->orderId);
    $this->assertTrue($order->hasItems(), 'The order should have an order item');
    $items = $order->getItems();
    $item = $items['0'];

    $this->assertInstanceOf(
        GroupInterface::class,
        $item->get('field_group')->entity,
        'There should be a group associated with this order'
      );

    // One is string, the other integer.
    $this->assertEquals(
        $this->group,
        $item->get('field_group')->getString(),
        'The group on the order should match the group selected'
      );
  }

  /**
   * @Then only for event payments the note is stored on the order item
   */
  public function onlyForEventPaymentsTheNoteIsStoredOnTheOrderItem() {
    if ($this->miscDonationSku == 'misc_payment_event') {
      $this->theNoteIsStoredOnTheOrderItem();
    }
    else {
      $this->theNoteIsStoredOnTheOrderItem(FALSE);
    }
  }

  /**
   * @Then only for event payments the thankyou email mentions the group
   */
  public function onlyForEventPaymentsTheThankyouEmailMentionsTheGroup() {
    if ($this->miscDonationSku == 'misc_payment_event') {
      $mails = $this->getMail(['to' => $this->getMyEmail()]);
      $group = Group::load($this->group);
      $properties = [
        'body' => [
          $group->label()
        ],
      ];
      $this->assertTrue($this->matchesMail($mails['0'], $properties), "The group should be mentioned in the thank you email");
    }
  }

  /**
   * @Then only for event payments a notification email is sent to the event booking contacts
   */
  public function onlyForEventPaymentsANotificationEmailIsSentToTheEventBookingContacts() {
    // Get event booking contacts
    // We need to create some dummy contacts with the test?
    if ($this->miscDonationSku == 'misc_payment_event') {
      // Check if email is only sent to them for event payments
    }
    else {

    }
  }

  /**
   * @Then the notification email mentions the group when appropriate
   */
  public function theNotificationEmailMentionsTheGroupWhenAppropriate() {

  }

  /**
   * @Then the notification email mentions the note when appropriate
   */
  public function theNotificationEmailMentionsTheNoteWhenAppropriate() {

  }


}
