<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\MiscDonation;

use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\EntityTrait;
use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\WebformTrait;
use Drupal\Tests\ahs_tests\Traits\FormTrait;

class MiscDonationTestBase extends ExistingSiteJsBehatBase {

  use UserDefinitions;
  use CommerceDefinitions;
  use EntityTrait;
  use CommerceTrait;
  use MailCollectionTrait;
  use WebformTrait;
  use FormTrait;

  protected $webFormHandlerSettings;

  protected $amount;

  protected $orderId;

  protected $donationFormUrl;

  protected $webFormSettings = [];

  protected $currency = 'GBP';

  protected $miscDonationSku = 'misc_donation_miscellaneous';

  protected $name = [
    'given_name' => 'Fred',
    'family_name' => 'bloggs',
  ];

  protected $address = [
    'address_line1' => '70 Happy Row',
    'locality' => 'London',
    'postal_code' => 'SE17 3FG',
  ];

  protected $phone = '020 7856 7324';

  protected $inVisualRegressionHide = ["div.checkout-complete"];

  protected function setUp(): void {
    parent::setUp();

    $this->startMailCollection();
    $this->setUpWebform();
  }

  protected function setUpWebform() {
    $originalWebform = $this->entityTypeManager->getStorage('webform')->load('miscellaneous_donations_payments');
    $this->assertNotEmpty($originalWebform, "A misc donations webform should exist.");
    $webform = $originalWebform->createDuplicate();
    $id = $this->randomMachineName();
    $webform->set('id', $id);
    $webform->save();
    $this->markEntityForCleanup($webform);

    // Open the new donation webform for testing.
    $this->donationFormUrl = $id;
    $this->setUpWebformAtUrl($webform, $this->donationFormUrl);

    // Set the webform store
    $handler = $webform->getHandler('commerce_webform_order_handler');
    $config = $handler->getConfiguration();
    $this->webFormHandlerSettings['store'] = $config['settings']['store']['store_entity'];
    // Make sure there's a commerce store with currency
    $currentStore = \Drupal::service('commerce_store.current_store')->getStore();
    if (empty($currentStore)) {
      $store = $this->createStore('Test store', 'admin@example.com', 'online', TRUE, 'GB', 'GBP');
      $this->markEntityForCleanup($store);
      $config['settings']['store']['store_entity'] = (string) $store->id();
    }
    $handler->setConfiguration($config);
    $webform->updateWebformHandler($handler);

    // Make sure there is donation product.
    $this->ensureProductBySku($this->miscDonationSku);
  }

  public function tearDown(): void {
    if ($user = $this->loadEntityByProperties('user', ['mail' =>$this->getMyEmail()])) {
      $this->markEntityForCleanup($user);
    }
    $this->restoreMailSettings();
    parent::tearDown();
  }

  /**
   * @When I submit a miscellaneous form
   * @When I submit a miscellaneous form specifying :amount
   */
  public function iSubmitAMiscellaneousForm($amount = 100) {
    // Store the enrolment values for use in later steps.
    $this->amount = $amount;

    $this->drupalGet("/". $this->donationFormUrl);
    $this->waitToPassHoneypot();

    // Complete the donation webform.
    $this->assertSession()->addressEquals($this->donationFormUrl);
    $this->assertSession()->pageTextContains('What would you like to contribute');
    $formValues = [
      'amount' => $this->amount,
      'sku' => $this->miscDonationSku,
    ];

    $this->submitForm($formValues, 'OK');

    // Form submits successfull
    $this->assertSession()->pageTextNotContains('What would you like to contribute');
   }

  /**
   * @Then a miscellaneous order is started for me
   */
  public function aMiscellaneousOrderIsStartedForMe() {
   $this->orderId = $this->assertOrderByEmail(
     $this->getMyEmail(),
     'ahs_miscellaneous',
     'ahs_miscellaneous',
     $this->amount,
     [$this->miscDonationSku],
     'draft')
     ->id();
  }

  /**
   * @When I submit my credit card details
   */
  public function iSubmitMyCreditCardDetails() {
    $this->submitCreditCardDetails('Pay now');
  }

  /**
   * @When I submit my accepted credit card details that require 3DS
   */
  public function iSubmitMyAcceptedCreditCardDetailsThatRequire3DS() {
    $this->submitCreditCardDetails('Pay now', self::$CARD_BASIC_ACCEPTED_3DS_REQUIRED);
  }

  /**
   * @When I submit my declined credit card details that require 3DS
   */
  public function iSubmitMyDeclinedCreditCardDetailsThatRequire3DS() {
    $this->submitCreditCardDetails('Pay now', self::$CARD_BASIC_DECLINED_3DS_REQUIRED);
  }


  /**
   * @When I complete the miscellaneous payment steps
   */
  public function iCompleteTheMiscellaneousPaymentSteps() {
    $this->iShouldBeOnThePaymentInformationStepOfTheCheckout();
    $this->iSubmitMyCreditCardDetails();
    // Check the order has a user/customer
    $order = $this->getOrder($this->orderId);
    $this->assertFalse($order->getCustomerId() == '0' OR $order->getCustomerId() == NULL, "The customer should not be anonymous");
  }

  /**
   * @When I provide my email
   */
  public function iProvideMyEmail() {
    $this->assertSession()->pageTextContains('Email');
    $formValues = [
      'mail' => $this->getMyEmail(),
    ];
    $this->submitForm($formValues, 'Continue to next step');
  }

}
