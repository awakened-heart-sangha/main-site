<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;

class UserViewTest extends UserTestBase {

  use BrowsingDefinitions;

  protected $mentorDescription;

  protected static $feature = <<<'FEATURE'
Feature: Support staff can view student info
    In order to provide student support to people
    We need to be able to view their basic info

    Scenario: Staff can view email
      Given there is a student
      And I am logged in as a "student_support"
      When I view the student
      And I should see the student's email

    Scenario: Member cannot view email
      Given there is a student
      And I am logged in as a "member"
      When I view the student
      And I should not see the student's email

    Scenario: Can view own email
      Given I am logged in
      When I view myself
      Then I should see my email

    Scenario: Can view mentor description
      Given there is a student
      And the student has a mentor description
      When I view the student
      Then I should see the mentor description


FEATURE;

  /**
   * @When I view myself
   */
  public function iViewMyself() {
    $this->drupalGet("/user/" . $this->getMe()->id());
    $this->assertSession()->pageTextContains($this->getMe()->getDisplayName());
  }

  /**
   * @Then I should see the student's email
   */
  public function iShouldSeeTheStudentsEmail() {
    $this->assertSession()->pageTextContains($this->student->getEmail());
  }

  /**
   * @Then I should not see the student's email
   */
  public function iShouldNotSeeTheStudentsEmail() {
    $this->assertSession()->pageTextNotContains($this->student->getEmail());
  }

  /**
   * @Then I should see my email
   */
  public function iShouldSeeMyEmail() {
    $this->assertSession()->pageTextContains($this->getMe()->getEmail());
  }

  /**
   * @Given the student has a mentor description
   */
  public function theStudentHasAMentorDescription() {
    $this->mentorDescription = $this->randomMachineName();
    $fields = ['field_description' => $this->mentorDescription];
    $this->updateProfile($this->student, 'mentor', $fields);
  }

}

