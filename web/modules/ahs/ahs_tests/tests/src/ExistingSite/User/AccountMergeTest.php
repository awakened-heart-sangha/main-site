<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Behat\Gherkin\Node\TableNode;
use Drupal\comment\CommentManagerInterface;
use Drupal\comment\Entity\Comment;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard;
use Drupal\entity_merge\Entity\MergeRequest;
use Drupal\group\Entity\Group;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\MailDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

/**
 * @group ClonedDatabaseRequired
 */
class AccountMergeTest extends UserTestBase {

  use BrowsingDefinitions;
  use UserTrait;
  use QueueTrait;
  use GroupTrait;
  use GroupDefinitions;
  use MailCollectionTrait;
  use MailDefinitions;

  // @todo
  // Training records
  // Orders
  // Customer profiles
  // Merging subscribers considering alternative emails?

  protected $originalUsers;
  protected $notes;
  protected $originalDefaultCustomerProfiles;
  protected $course;

  protected static $feature = <<<'FEATURE'
Feature: Users can be merged
    In order to prevent duplicate people
    We need to be able to merge users

    Background:
      Given a user called "John"
      And a user called "Jack"

    Scenario: Secondary email added as alternative when merging
      When Jack is merged into John
      Then John has an unchanged email
      And Jack has an empty email
      And Jack has no alternative emails
      And the email of Jack is an alternative email for John

    Scenario: Login details are merged
      Given Jack has a username
      When Jack is merged into John
      Then Jack is blocked
      And Jack now has no username
      And John now has a username

    Scenario: Primary names are kept if primary has names
      Given the firstname of John is "John" and his lastname is "Smith"
      And the Sangha name of John is "Virya"
      And the firstname of Jack is "Jack" and his lastname is "Smit"
      And the Sangha name of Jack is "Dawa"
      When Jack is merged into John
      Then the firstname of John is now "John" and his lastname is "Smith"
      And the Sangha name of John is now "Virya"

    Scenario: Secondary names are used if primary has no name
      Given John has no firstname and lastname
      And John has no Sangha name
      And the firstname of Jack is "Jack" and his lastname is "Smith"
      And the Sangha name of Jack is "Dawa"
      When Jack is merged into John
      Then the firstname of John is now "Jack" and his lastname is "Smith"
      And the Sangha name of John is now "Dawa"

    Scenario: Not deceased if neither are deceased
      Given John is not deceased
      And Jack is not deceased
      When Jack is merged into John
      Then John is not now deceased

    Scenario: Deceased if primary is deceased
      Given John is deceased
      And Jack is not deceased
      When Jack is merged into John
      Then John is now deceased

    Scenario: Deceased if secondary is deceased
      Given John is not deceased
      And Jack is deceased
      When Jack is merged into John
      Then John is now deceased

    Scenario: Deceased if both deceased
      Given John is deceased
      And Jack is deceased
      When Jack is merged into John
      Then John is now deceased

    Scenario Outline: Membership roles
      Given John has a role "<John>"
      And Jack has a role "<Jack>"
      When Jack is merged into John
      Then John now has a role "<Now>"
      And Jack now has no roles

      Examples:
        | John          | Jack          | Now           |
        | member        | member        | member        |
        | member        | former_member | member        |
        | member        |               | member        |
        | former_member | member        | member        |
        | former_member | former_member | former_member |
        | former_member |               | former_member |
        |               | member        | member        |
        |               | former_member | former_member |
        |               |               |               |

    Scenario: Membership with no roles
      When Jack is merged into John
      Then John now has no roles
      And Jack now has no roles

    Scenario: Merging staff roles for primary member
      Given John has a role "member"
      And Jack has a role "archivist"
      When Jack is merged into John
      Then John now has a role "member"
      And John now has a role "archivist"
      And Jack now has no roles

    Scenario: Merging staff roles for secondary member
      Given John has a role "student_support"
      And Jack has a role "member"
      And Jack has a role "archivist"
      When Jack is merged into John
      Then John now has a role "student_support"
      And John now has a role "member"
      And John now has a role "archivist"
      And Jack now has no roles

    Scenario: No membership start if neither have membership start
      Given John has no membership start date
      And Jack has no membership start date
      When Jack is merged into John
      Then John has no membership start date

    Scenario: Membership start date if primary has membership start date
      Given John has a membership start date of "2021-03-21"
      And Jack has no membership start date
      When Jack is merged into John
      Then John now has a membership start date of "2021-03-21"

    Scenario: Membership start date if secondary has membership start date
      Given John has no membership start date
      Given Jack has a membership start date of "2021-03-21"
      When Jack is merged into John
      Then John now has a membership start date of "2021-03-21"

    Scenario: Membership start date if primary start date is earlier than secondary
      Given John has a membership start date of "2021-03-21"
      And Jack has a membership start date of "2022-03-21"
      When Jack is merged into John
      Then John now has a membership start date of "2021-03-21"

    Scenario: Membership start date if primary start date is later than secondary
      Given John has a membership start date of "2022-03-21"
      And Jack has a membership start date of "2021-03-21"
      When Jack is merged into John
      Then John now has a membership start date of "2021-03-21"

    Scenario: Admin notes are merged
      Given John has an admin note
      And Jack has an admin note
      When Jack is merged into John
      Then John now has both admin notes
      And Jack still has an admin note

    Scenario: Stripe if only primary has customer
      Given John has a Stripe customer and payment method
      And Jack has no Stripe customer or payment method
      When Jack is merged into John
      Then the Stripe customer of John has not changed
      And John now has a Stripe payment method

    Scenario: Stripe if only secondary has customer
      Given John has no Stripe customer or payment method
      And Jack has a Stripe customer and payment method
      When Jack is merged into John
      Then the Stripe customer of Jack is now the Stripe customer of John
      And John now has a Stripe payment method
      And Jack now has no Stripe payment method

    Scenario: Stripe if both have customers
      Given John has a Stripe customer and payment method
      And Jack has a Stripe customer and payment method
      When Jack is merged into John
      Then the Stripe customer of John has not changed
      And John now has a Stripe payment method
      And Jack now has a Stripe payment method

#    Scenario: Stripe & paypal mixed
#      Given John has a Paypal customer and payment method
#      And Jack has a Stripe customer and payment method
#      When Jack is merged into John
#      Then the Paypal customer of John is unchanged
#      And John now has a Stripe customer
#      And John has a payment method
#      And John does not have Jack's payment method

    Scenario: No secondary group membership
      Given a course group
      And John is a member of the group
      And Jack is not a member of the group
      When Jack is merged into John
      Then John is still a member of the group
      And Jack is still not a member of the group

    Scenario: No primary group membership
      Given a course group
      And John is not a member of the group
      And Jack is a member of the group
      When Jack is merged into John
      Then John is now a member of the group
      And Jack is now not a member of the group

    Scenario: Both have group memberships
      Given a course group
      And John is a member of the group
      And Jack is a member of the group
      When Jack is merged into John
      Then John is now a member of the group
      And John has only a single group membership
      And Jack is now not a member of the group

    Scenario: Primary is a mentor, secondary is member
      Given a course group
      And John is a mentor in the group
      And Jack is a member of the group
      When Jack is merged into John
      Then John is now a mentor in the group
      And Jack is now not a member of the group

    Scenario: Primary is a member, secondary is a mentor
      Given a course group
      And John is a member of the group
      And Jack is a mentor in the group
      When Jack is merged into John
      Then John is now a mentor in the group
      And Jack is now not a member of the group

    Scenario: Primary is a mentor, secondary is admin
      Given a course group
      And John is a mentor in the group
      And Jack is an admin in the group
      When Jack is merged into John
      Then John is now a mentor in the group
      Then John is now an admin in the group
      And Jack is now not a member of the group

    Scenario: Secondary is a mentor, primary is admin
      Given a course group
      And John is an admin in the group
      And Jack is a mentor in the group
      When Jack is merged into John
      Then John is now a mentor in the group
      Then John is now an admin in the group
      And Jack is now not a member of the group

#    Scenario: Primary has no subscription
#      Given John has no membership subscription
#      And Jack has a membership subscription for 20
#      When Jack is merged into John
#      Then John has a membership subscription for 20
#      When a month passes
#      Then John makes a membership donation for 20

#    Scenario: Secondary has no subscription
#      Given John has a membership subscription for 20
#      And Jack has no membership subscription
#      When Jack is merged into John
#      Then John has a membership subscription for 20
#      When a month passes
#      Then John makes a membership donation for 20

#    Scenario: Secondary has more recent subscription
#      Given John has a membership subscription for 20
#      And Jack has a more recent membership subscription for 10
#      When Jack is merged into John
#      Then John has a membership subscription for 10
#      When a month passes
#      Then John makes a membership donation for 10

#    Scenario: Secondary has less recent subscription
#      Given John has a membership subscription for 20
#      And Jack has a less recent membership subscription for 10
#      When Jack is merged into John
#      Then John has a membership subscription for 20
#      When a month passes
#      Then John makes a membership donation for 20

    Scenario: Customer profile if both have profiles
      Given John has a customer profile
      And Jack has a customer profile
      When Jack is merged into John
      Then the customer profile of John is still the default
      And John now has 2 customer profiles

    Scenario: Customer profile if primary has no profile
      Given John has no customer profile
      And Jack has a customer profile
      When Jack is merged into John
      Then the customer profile of Jack is the default for John


FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->clearQueues();
  }

  public function tearDown(): void {
    $this->tearDownQueues();
    parent::tearDown();
  }

  /**
   * @Given a user called :name
   */
  public function aUserCalled($name) {
    $this->users[$name] = $this->createDecoupledUser();
  }

  /**
   * @When :secondary is merged into :primary
   */
  public function aIsMergedIntoB($secondary, $primary) {
    $this->users[$secondary] = $this->reloadEntity($this->users[$secondary]);
    $this->users[$primary] = $this->reloadEntity($this->users[$primary]);
    $this->originalUsers[$secondary] = clone $this->users[$secondary];
    $this->originalUsers[$primary] = clone $this->users[$primary];

    $request = MergeRequest::create([
      'primary_id' => $this->users[$primary]->id(),
      'secondary_id' => $this->users[$secondary]->id(),
      'target_type' => 'user',
    ]);

    $merger = \Drupal::service('entity_merge.merger');
    $result = $merger->run($request);
    $this->assertTrue($result, "User merge failed: " . print_r($request->log->getValue(), TRUE));
  }

  /**
   * @Then :name has an unchanged email
   */
  public function hasAnUnchangedEmail($name) {
    $this->assertFieldUnchanged($name, 'mail');
  }

  protected function assertFieldUnchanged($name, $fieldId, $profile = NULL) {
    $originalEntity = $this->originalUsers[$name];
    $entity = $this->getUserByName($name);
    if ($profile) {
      $originalEntity = $originalEntity->get("{$profile}_profiles")->entity;
      $entity = $entity->get("{$profile}_profiles")->entity;
    }
    $this->assertSame($originalEntity->get($fieldId)->getValue(), $entity->get($fieldId)->getValue(), "The $fieldId field should be unchanged for $name");
  }

  /**
   * @Then :name has an empty email
   */
  public function hasAnEmptyEmail($name) {
    $this->assertEmpty($this->getUserByName($name)->getEmail());
  }

  /**
   * @Then :name has no alternative emails
   */
  public function hasNoAlternativeEmails($name) {
    $this->assertTrue($this->getUserByName($name)->get('alternative_user_emails')->isEmpty(), "$name should have no alternative emails.");
  }

  /**
   * @Then the email of :secondary is an alternative email for :primary
   */
  public function theEmailIsAnAlternativeEmail($secondary, $primary) {
    $alternatives = $this->getUserByName($primary)->get('alternative_user_emails');
    $this->assertFalse($alternatives->isEmpty(), "There should be an alternative email for the primary user");
    $alternative = $alternatives->value;
    $this->assertNotEmpty($alternative, "The alternative email should not be empty");
    $this->assertSame($this->originalUsers[$secondary]->getEmail(), $alternative, "The alternative email of $primary should be the original email of $secondary");
  }

  /**
   * @Given :name has a username
   */
  public function userHasAUsername($name) {
    $user = $this->getUserByName($name);
    $user->setUsername($this->randomMachineName());
    $user->save();
    // This causes username merge tests to fail for unknown reasons.
    // $password = user_password();
    //$user->setPassword($password);
    //$user->save();
  }

  /**
   * @Then :name is blocked
   */
  public function userIsBlocked($name) {
    $this->assertTrue($this->getUserByName($name)->isBlocked());
  }

  /**
   * @Then :name now has no username
   */
  public function userHasNoUsername($name) {
    $this->assertEmpty($this->getUserByName($name)->getAccountName());
  }

  /**
   * @Then :name now has a username
   */
  public function userHasUsername($name) {
    $this->assertNotEmpty($this->getUserByName($name)->getAccountName());
  }

  protected function getUserByName($name) {
    return $this->reloadEntity($this->users[$name]);
  }

  /**
   * @Given the firstname of :name is :firstname and his lastname is :lastname
   */
  public function givenFirstNameLastName($name, $firstname, $lastname = NULL) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $profile->get('field_name')->given = $firstname;
      $profile->get('field_name')->family = $lastname;
    $profile->save();
  }

  /**
   * @Given the firstname of :name is :firstname
   */
  public function givenFirstName($name, $firstname) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $profile->get('field_name')->given = $firstname;
    $profile->save();
  }


  /**
   * @Given the Sangha name of :name is :sanghaname
   */
  public function givenSanghaName($name, $sanghaname) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $profile->get('field_name_sangha')->value = $sanghaname;
    $profile->save();
  }

  /**
   * @Given :name has no firstname and lastname
   */
  public function givenNoName($name) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $profile->set('field_name', NULL);
    $profile->save();
  }

  /**
   * @Given :name has no Sangha name
   */
  public function givenNoSanghaName($name) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $profile->set('field_name_sangha', NULL);
    $profile->save();
  }

  /**
   * @Then the firstname of :name is now :firstname and his lastname is :lastname
   */
  public function thenFirstNameLastName($name, $firstname, $lastname) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $this->assertFalse($profile->get('field_name')->isEmpty(), "The merged user should have a name");
    $this->assertSame($firstname, $profile->get('field_name')->given, "The firstname should be $firstname");
    $this->assertSame($lastname, $profile->get('field_name')->family, "The lastname should be $lastname");
  }

  /**
   * @Then the Sangha name of :name is now :sanghaname
   */
  public function thenSanghaName($name, $sanghaname) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $this->assertFalse($profile->get('field_name_sangha')->isEmpty(), "The merged user should have a Sangha name");
    $this->assertSame($sanghaname, $profile->get('field_name_sangha')->value, "The Sangha name should be $sanghaname");
  }

  /**
   * @Given :name is deceased
   */
  public function isDeceased($name) {
    $this->setDeceased($name, TRUE);
  }

  /**
   * @Given :name is not deceased
   */
  public function isNotDeceased($name) {
    $this->setDeceased($name, FALSE);
  }

  protected function setDeceased($name, $isDeceased) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $profile->set('field_deceased', $isDeceased);
    $profile->save();
  }

  /**
   * @Then :name is now deceased
   */
  public function isNowDeceased($name) {
    $this->assertDeceased($name, TRUE);
  }

  /**
   * @Then :name is not now deceased
   */
  public function isNotNowDeceased($name) {
    $this->assertDeceased($name, FALSE);
  }

  protected function assertDeceased($name, $isDeceased) {
    $user = $this->getUserByName($name);
    $profile = $user->get("about_profiles")->entity;
    $this->assertSame((bool) $isDeceased, (bool) $profile->get('field_deceased')->value);
  }

  /**
   * @Given :name has a role :rid
   */
  public function hasARole($name, $rid = NULL) {
    $user = $this->getUserByName($name);
    if (!empty($rid)) {
      $user->addRole($rid);
      $user->save();
    }
  }

  /**
   * @Given :name now has a role :rid
   */
  public function  nowHasARole($name, $rid = NULL) {
    $user = $this->getUserByName($name);
    $roles = $user->getRoles(TRUE);
    $group = Group::load(583);
    if (empty($rid)) {
      $this->assertEmpty($roles, "$name should have no roles");
      $this->assertEmpty($group->getMember($user));
    }
    else {
      $this->assertNotEmpty($roles, "$name should have a $rid role but has none");
      $this->assertContains($rid, $roles, "$name should have the $rid role.");

      if ($rid === 'member') {
        $this->assertNotEmpty($group->getMember($user));
        $opposite = 'former_member';
      }
      elseif ($rid === 'former_member') {
        $this->assertEmpty($group->getMember($user));
        $opposite = 'member';
      }
      if (isset($opposite)) {
        $this->assertNotContains($opposite, $roles, "$name should not have the $opposite role.");
      }
    }
  }

  /**
   * @Given :name now has no roles
   */
  public function  nowHasnoRoles($name) {
    $this->nowHasARole($name, NULL);
  }

  /**
   * @Given :name has a membership start date of :date
   */
  public function hasAMembershipStartDate($name, $date) {
    $user = $this->getUserByName($name);
    $profile = $user->get('admin_profiles')->entity;
    $profile->set('field_membership_start', $date);
    $profile->save();
  }

  /**
   * @Given :name has no membership start date
   */
  public function hasNoMembershipStartDate($name) {
    $user = $this->getUserByName($name);
    $profile = $user->get('admin_profiles')->entity;
    $profile->set('field_membership_start', NULL);
    $profile->save();
  }

  /**
   * @Then :name now has a membership start date of :date
   */
  public function noHasAMembershipStartDate($name, $date) {
    $user = $this->getUserByName($name);
    $profile = $user->get('admin_profiles')->entity;
    $this->assertFalse($profile->get('field_membership_start')->isEmpty(), "There should be a membership start date");
    $membership_start = $profile->get('field_membership_start')->date->format('Y-m-d');
    $this->assertEquals($date, $membership_start);
  }

  /**
   * @Given :name has an admin note
   */
  public function hasanAdminNote($name) {
    $user = $this->getUserByName($name);
    $profile = $user->get('admin_profiles')->entity;

    $note = $this->randomMachineName();
    $this->notes[] = $note;
    $comment = [
      'entity_id' => $profile->id(),
      'entity_type' => 'profile',
      'field_name' => 'field_notes',
      'subject' => $note,
      'comment_body' => $note,
      'status' => TRUE,
    ];
    $comment = Comment::create($comment);
    $comment->save();
    $this->assertCount(1, $this->getNotes($name));
  }

  /**
   * @Then :name now has both admin notes
   */
  public function hasBothAdminNotes($name) {
    $comments = array_values($this->getNotes($name));
    // An extra comment is added by the merge.
    $this->assertSame(count($this->notes) + 1, count($comments), "$name should have a certain number of notes. $name had: " . print_r(
        array_map(function($comment) {
          return $comment->get('comment_body')->value;},
        $comments),
      TRUE));
    foreach($this->notes as $i => $note) {
      $this->assertSame($note, $comments[$i]->get('comment_body')->value, "Comment #$i should have the right body");
    }
  }

  /**
   * @Then :name still has an admin note
   */
  public function stillHasAnAdminNote($name) {
    $comments = $this->getNotes($name);
    // An extra comment is added by the merge.
    $this->assertSame(2, count($comments), "$name should have a certain number of notes. $name had: " . print_r(
        array_map(function($comment) {
          return $comment->get('comment_body')->value;},
          $comments),
        TRUE));
  }

  protected function getNotes($name) {
    $user = $this->getUserByName($name);
    $profile = $user->get('admin_profiles')->entity;
    $storage = \Drupal::entityTypeManager()->getStorage('comment');
    $comments = $storage->loadThread($profile, 'field_notes', CommentManagerInterface::COMMENT_MODE_FLAT);
    return $comments;
  }

  /**
   * @Given :name has a Stripe customer and payment method
   */
  public function hasAStripeCustomerAndPaymentMethod($name) {
    $user = $this->getUserByName($name);
    $user->get('commerce_remote_id')->setByProvider('shrimala_trust_stripe|test', $this->randomMachineName());
    $user->save();
    $payment_method = PaymentMethod::create([
      'type' => 'credit_card',
      'payment_gateway' => 'shrimala_trust_stripe',
      'payment_gateway_mode' => 'test',
      'card_type' => 'visa',
      'card_number' => '9999',
      'reusable' => TRUE,
    ]);
    $payment_method->setOwner($user);
    $payment_method->save();
  }

  /**
   * @Given :name has no Stripe customer or payment method
   */
  public function hasNoStripeCustomerOrPaymentMethod($name) {
    $user = $this->getUserByName($name);
    $user->get('commerce_remote_id')->setByProvider('shrimala_trust_stripe|test', NULL);
    $user->save();
    $methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties([
      'uid' => $user->id(),
      'payment_gateway' => 'shrimala_trust_stripe',
    ]);
    $this->assertEmpty($methods);
  }

  /**
   * @Then the Stripe customer of :name has not changed
   */
  public function theStripeCustomerHasNotChanged($name) {
    $user = $this->getUserByName($name);
    $currentId = $user->get('commerce_remote_id')->getByProvider('shrimala_trust_stripe|test');
    $originalId = $this->originalUsers[$name]->get('commerce_remote_id')->getByProvider('shrimala_trust_stripe|test');
    $this->assertSame($originalId, $currentId, "The commerce remote id should not have changed.");
  }

  /**
   * @Then :name now has no Stripe customer
   */
  public function nowHasNoStripeCustomer($name) {
    $user = $this->getUserByName($name);
    $this->assertEmpty($user->get('commerce_remote_id')->getByProvider('shrimala_trust_stripe|test'));
  }

  /**
   * @Then :name now has a Stripe payment method
   */
  public function hasAPaymentMethod($name) {
    $user = $this->getUserByName($name);
    $methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties([
      'uid' => $user->id(),
      'payment_gateway' => 'shrimala_trust_stripe',
    ]);
    $method = reset($methods);
    $this->assertNotFalse($method, "A payment method entity should exist for $name.");
  }

  /**
   * @Then :name now has no Stripe payment method
   */
  public function nowHasNoPaymentMethod($name) {
    $user = $this->getUserByName($name);
    $methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties([
      'uid' => $user->id(),
      'payment_gateway' => 'shrimala_trust_stripe',
    ]);
    $this->assertEmpty($methods, "No payment method entity should exist for $name.");
  }

  /**
   * @Then the Stripe customer of :name_b is now the Stripe customer of :name_a
   */
  public function theStripeCustomerIsNowTheStripeCustomer($name_b, $name_a) {
    $user_a = $this->getUserByName($name_a);
    $this->assertNotEmpty($user_a->get('commerce_remote_id')->getByProvider('shrimala_trust_stripe|test'));
    $this->assertSame($this->originalUsers[$name_b]->get('commerce_remote_id')->getByProvider('shrimala_trust_stripe|test'), $user_a->get('commerce_remote_id')->getByProvider('shrimala_trust_stripe|test'));
  }

  /**
   * @Given :name is a member of the group
   */
  public function isAMemberOfTheGroup($name) {
    $user = $this->getUserByName($name);
    $this->group->addMember($user, []);
  }

  /**
   * @Given :name is not a member of the group
   */
  public function isNotAMemberOfTheGroup($name) {
    $user = $this->getUserByName($name);
    $this->assertEmpty($this->group->getMember($user));
  }

  /**
   * @Then :name is now/still a member of the group
   */
  public function isNowNotAMemberOfTheGroup($name) {
    $user = $this->getUserByName($name);
    $this->assertNotEmpty($this->group->getMember($user));
  }

  /**
   * @Then :name has only a single group membership
   */
  public function hasOnlyASingleGroupMembership($name) {
    $user = $this->getUserByName($name);
    $loader = \Drupal::service('group.membership_loader');
    $this->assertCount(1, $loader->loadByUser($user));
  }

  /**
   * @Then :name is now/still not a member of the group
   */
  public function isNowAMemberOfTheGroup($name) {
    $user = $this->getUserByName($name);
    $this->assertEmpty($this->group->getMember($user));
  }

  /**
   * @Given :name is a mentor in the group
   */
  public function isAMentorInTheGroup($name) {
    $user = $this->getUserByName($name);
    $mentorTermId = \Drupal\ahs_training\Entity\TrainingRecord::MENTOR_TERM_ID;
    $membership = ['field_participant_type' => ['target_id' => $mentorTermId], 'group_roles' => [$this->group->bundle() . '-mentor']];
    $this->group->addMember($user, $membership);
  }

  /**
   * @Given :name is an admin in the group
   */
  public function isAnAdminInTheGroup($name) {
    $user = $this->getUserByName($name);
    $studentTermId = \Drupal\ahs_training\Entity\TrainingRecord::STUDENT_TERM_ID;
    $membership = ['field_participant_type' => ['target_id' => $studentTermId], 'group_roles' => [$this->group->bundle() . '-admin']];
    $this->group->addMember($user, $membership);
  }

  /**
   * @Then :name is now/still a mentor in the group
   */
  public function isNowNotAMentorInTheGroup($name) {
    $user = $this->getUserByName($name);
    $membership = $this->group->getMember($user);
    $this->assertNotEmpty($membership);
    $mentorTermId = \Drupal\ahs_training\Entity\TrainingRecord::MENTOR_TERM_ID;
    $content = $membership->getGroupRelationship();
    $this->assertEquals($mentorTermId, $content->get('field_participant_type')->target_id);
    $roles = array_column($content->get('group_roles')->getValue(), 'target_id');
    $this->assertContains($this->group->bundle() . '-mentor', $roles);
  }

  /**
   * @Then :name is now/still an admin in the group
   */
  public function isNowAnAdminInTheGroup($name) {
    $user = $this->getUserByName($name);
    $membership = $this->group->getMember($user);
    $this->assertNotEmpty($membership);
    $content = $membership->getGroupRelationship();
    $roles = array_column($content->get('group_roles')->getValue(), 'target_id');
    $this->assertContains($this->group->bundle() . '-admin', $roles);
  }

  /**
   * @Given :name has no customer profile
   */
  public function hasNoCustomerProfile($name) {
    $user = $this->getUserByName($name);
    $profiles = $this->entityTypeManager->getStorage('profile')->loadByProperties([
      'uid' => $user->id(),
      'type' => 'customer',
    ]);
    $this->assertEmpty($profiles);
  }

  /**
   * @Given :name has a customer profile
   */
  public function hasACustomerProfile($name) {
    $user = $this->getUserByName($name);
    $profile = Profile::create([
      'uid' => $user->id(),
      'type' => 'customer',
    ]);
    $profile->setPublished();
    $profile->save();
    $this->assertNotEmpty($this->entityTypeManager->getStorage('profile')->loadByUser($user, 'customer'), "$name should have a customer profile.");
    $this->assertNotEmpty($this->entityTypeManager->getStorage('profile')->loadDefaultByUser($user, 'customer'), "$name should have a default customer profile.");
    $this->originalDefaultCustomerProfiles[$name] = $this->entityTypeManager->getStorage('profile')->loadDefaultByUser($user, 'customer');
  }

  /**
   * @Then :name now has :count customer profiles
   */
  public function hasCustomerProfiles($name, $count) {
    $user = $this->getUserByName($name);
    $profiles = $this->entityTypeManager->getStorage('profile')->loadMultipleByUser($user, 'customer');
    $this->assertCount((int) $count, $profiles);
  }

  /**
   * @Then the customer profile of :name is still/now the default
   */
  public function theCustomerProfileIsTheDefault($name) {
    $user = $this->getUserByName($name);
    $this->assertNotEmpty($this->entityTypeManager->getStorage('profile')->loadByUser($user, 'customer'), "$name should have a customer profile.");
    $this->assertNotEmpty($this->entityTypeManager->getStorage('profile')->loadDefaultByUser($user, 'customer'), "$name should have a default customer profile.");
    $this->assertSame($this->originalDefaultCustomerProfiles[$name]->id(), $this->entityTypeManager->getStorage('profile')->loadDefaultByUser($user, 'customer')->id());
  }

  /**
   * @Then the customer profile of :secondary is the default for :primary
   */
  public function theCustomerProfileIsTheDefaultFor($primary, $secondary) {
    $primaryUser = $this->getUserByName($primary);
    $this->assertNotEmpty($this->entityTypeManager->getStorage('profile')->loadByUser($primaryUser, 'customer'), "$primary should have a customer profile.");
    $this->assertNotEmpty($this->entityTypeManager->getStorage('profile')->loadDefaultByUser($primaryUser, 'customer'), "$primary should have a default customer profile.");
    $this->assertSame($this->originalDefaultCustomerProfiles[$secondary]->id(), $this->entityTypeManager->getStorage('profile')->loadDefaultByUser($primaryUser, 'customer')->id());
  }

}

