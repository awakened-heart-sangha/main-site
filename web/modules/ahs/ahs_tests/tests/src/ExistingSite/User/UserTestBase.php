<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\user\UserInterface;

class UserTestBase extends ExistingSiteBehatBase {

  use UserTestTrait;
}

