<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Behat\Gherkin\Node\TableNode;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\TabsTrait;

class UserTabsTest extends UserTestBase {

  use BrowsingDefinitions;
  use TabsTrait;

  protected static $feature = <<<'FEATURE'
Feature: User tabs
    In order to organise information about a user
    Different tabs should be available depending on role

    Scenario: Someone else's tabs for anonymous
      Given there is a student
      And I am not logged in
      When I view the student
      Then I should see no tabs

    Scenario: Someone else's tabs for member
      Given there is a student
      And I am logged in as a "member"
      When I view the student
      Then I should see no tabs

    Scenario: Someone else's tabs for support
      Given there is a student
      And I am logged in as a "student_support"
      When I view the student
      Then I should see tabs:
        | tab           |
        | Overview      |
        | Manage        |
        | Emails        |
        | Contribution  |
        | Participation |
        | Addresses     |
        | Logs          |

    Scenario: User tabs caching
      Given there is a student
      And I am logged in as a "member"
      When I view the student
      Then I should see no tabs
      Given I am logged in as a "student_support"
      When I view the student
      Then I should see tabs:
        | tab           |
        | Overview      |
        | Manage        |
        | Emails        |
        | Contribution  |
        | Participation |
        | Addresses     |
        | Logs          |
      Given I am logged in as a "member"
      When I view the student
      Then I should see no tabs

    Scenario: Own tabs for student
      Given I am logged in as a "member"
      When I view my own account
      Then I should see tabs:
        | tab           |
        | Overview      |
        | Settings      |
        | Emails        |
        | Contribution  |
        | Participation |
        | Addresses     |

    Scenario: Own tabs for support
      Given I am logged in as a "student_support"
      When I view my own account
      Then I should see tabs:
        | tab           |
        | Overview      |
        | Settings      |
        | Manage        |
        | Emails        |
        | Contribution  |
        | Participation |
        | Addresses     |
        | Logs          |

    Scenario: Someone else's contribution tabs
      Given there is a student
      And I am logged in as a "student_support"
      When I view the student's contributions
      Then I should see subtabs:
        | tab             |
        | Online payments |
        | Membership      |

    Scenario: Own contribution tabs
      And I am logged in as a "member"
      When I view my contributions
      Then I should see subtabs:
        | tab             |
        | Online payments |
        | Membership      |


FEATURE;


  /**
   * @Given the student has a customer profile
   */
  public function theStudentHasACustomerProfile() {
    $profile = Profile::create([
      'uid' => $this->student->id(),
      'type' => 'customer'
    ]);
    $profile->setPublished();
    $profile->save();
  }

  /**
   * @When I view the student's contributions
   */
  public function iViewTheStudentsContributions() {
    $this->drupalGet("/user/" . $this->student->id() . "/contribution/online");
  }

  /**
   * @When I view my contributions
   */
  public function iViewMyContributions() {
    $this->drupalGet("/user/" . $this->getMe()->id() . "/contribution/online");
  }

}

