<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

/**
 * Test staff creating new users.
 *
 * @group js
 */
class UserAddTest extends ExistingSiteJsBehatBase {

  protected $inVisualRegressionHide = [".view-user-admin-people"];

  use BrowsingDefinitions;
  use UserTrait;
  use UserDefinitions;
  use FormTrait;

  protected static $feature = <<<'FEATURE'
Feature: Support staff can add users
    In order to track people who come via sources other than the website
    Support staff need to be able to add new users

    Scenario: Staff can add user
      Given I am logged in as a "student_admin"
      When I visit "admin/people/create"
      And I enter an email
      Then I should not see "New to the Sangha"
      When I press "Create new account"
      Then I should see "Created a new user account"
      And a new user should exist with that email

    Scenario Outline: Staff can see add user button
      Given I am logged in as a "<role>"
      When I visit "admin/people"
      Then I should see "Add user"

      Examples:
        | role          |
        | student_admin |
        | accountant    |

FEATURE;

  protected $newUserEmail;

  /**
   * @When I enter an email
   */
  public function iEnterAnEmail() {
    $page = $this->getCurrentPage();
    $this->newUserEmail = $this->randomEmail();
    $page->fillField('Email', $this->newUserEmail);
    $this->waitToPassHoneypot();
  }

  /**
   * @Then a new user should exist with that email
   */
  public function aNewUserShouldExistWithThatEmail() {
    $newUser = $this->loadEntityByProperties('user', ['mail' => $this->newUserEmail]);
    $this->assertNotEmpty($newUser, "There should be a user with the email {$this->newUserEmail}");
  }

}

