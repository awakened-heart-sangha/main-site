<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\user\UserInterface;

trait UserTestTrait {

  use UserDefinitions;
  use UserTrait;

  /**
   * @var \Drupal\user\UserInterface
   */
  protected $student;

  protected $mentorDescription;

  /**
   * @Given there is a student
   */
  public function thereIsAStudent() {
    $edit = [
      'roles' => ['member'],
      'mail' => $this->randomEmail()
    ];
    $this->student = $this->createUser([], $edit);
    $this->updateProfile($this->student, 'admin', []);
    $this->users['student'] = $this->student;
  }

  /**
   * @When I view the student
   */
  public function iViewTheStudent() {
    $this->drupalGet("/user/" . $this->student->id());
    $this->assertSession()->pageTextContains($this->student->getDisplayName());
  }

  /**
   * @Given the student is logged in
   */
  public function theStudentIsLoggedIn() {
    $this->setMe($this->student);
    $this->iLogin();
  }

  /**
   * @Given the student views themselves
   */
  public function theStudentViewsThemselves() {
    $this->drupalGet("/user/" . $this->getMe()->id());
    $this->assertSession()->pageTextContains($this->getMe()->getDisplayName());
  }

  /**
   * @When I view my own account
   */
  public function iViewMyOwnAccount() {
    $this->drupalGet("/user/" . $this->getMe()->id());
    $this->assertSession()->pageTextContains($this->getMe()->getDisplayName());
  }

  /**
   * @Then I should see the mentor description
   */
  public function iShouldSeeTheMentorDescription() {
    $this->assertSession()->pageTextContains($this->mentorDescription);
  }

  /**
   * @Then I should not see the mentor description
   */
  public function iShouldNotSeeTheMentorDescription() {
    $this->assertSession()->pageTextNotContains($this->mentorDescription);
  }

}

