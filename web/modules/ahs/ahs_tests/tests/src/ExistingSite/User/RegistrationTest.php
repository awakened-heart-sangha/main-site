<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\MailDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;

/**
 * @group js
 */
class RegistrationTest extends ExistingSiteJsBehatBase {

  use UserDefinitions;
  use BrowsingDefinitions;
  use MailDefinitions;
  use MailCollectionTrait;
  use FormTrait;

  protected static $feature = <<<'FEATURE'
Feature: Decoupled auth and email registration automatic usernames
    In order to allow decoupled_auth to distinguish coupled and uncoupled users
    We need to allow users to not have usernames

    Scenario: New users can register
      Given I am not a user
      When I go to the registration form
      And I enter my email as the email
      And I indicate I am new to the Sangha
      And I press "Create new account"
      Then I should see "A welcome message with further instructions has been sent to your email address"
      And I should not see "None of the email addresses you entered are known to us"
      And I should not see "An account is already registered"
      And I should have a username
      And I should get a welcome email at my email
      #And I follow the link to "ahs" from the email
      #Then I should see "waay"

    Scenario: New users must indicate they are new
      Given I am not a user
      When I go to the registration form
      And I enter my email as the email
      And I press "Create new account"
      Then I should see "None of the email addresses you entered are known to us"
      And I should not see "A welcome message with further instructions has been sent to your email address"
      When I enter my alternative email as the alternative email
      And I press "Create new account"
      Then I should see "None of the email addresses you entered are known to us"
      And I should not see "A welcome message with further instructions has been sent to your email address"

    Scenario: Decoupled users don't have usernames
      Given I am a user without a password
      Then I should not have a username
      When I change my email
      Then I should still not have a username

    Scenario: Decoupled users can register
      Given I am a user without a password
      When I go to the registration form
      And I enter my email as the email
      And I press "Create new account"
      Then I should see "A welcome message with further instructions has been sent to your email address"
      And I should have a username
      And I should get a welcome email at my email
      #And I follow the link to "ahs" from the email
      #Then I should see "waay"
      When I change my email
      Then I should still have a username

    Scenario: Decoupled users can register using alternative email
      Given I am a user without a password
      And my email is "other@fail.com"
      And my alternative email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "fred@bloggs.com" as the email
      And I press "Create new account"
      Then my email is still "other@fail.com"
      Then I should get a welcome email at "fred@bloggs.com"
      Then I should see "A welcome message with further instructions has been sent to your email address"

    Scenario: Coupled users can register
      Given I am a user with a password
      When I go to the registration form
      And I enter my email as the email
      And I press "Create new account"
      Then I should see "An account is already registered"
      And I should see "A link to help you log in has been sent"
      Then I should get a welcome email at my email
      #And I follow the link to "ahs" from the email
      #Then I should see "waay"

    Scenario: Coupled users can register using alternative email
      Given I am a user with a password
      And my email is "other@fail.com"
      And my alternative email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "fred@bloggs.com" as the email
      And I press "Create new account"
      Then I should see "An account is already registered for fred@bloggs.com"
      And I should see "A link to help you log in has been sent to fred@bloggs.com"
      Then my email is still "other@fail.com"
      And I should get a welcome email at "fred@bloggs.com"

    Scenario: Alternative emails aren't saved for coupled users
      Given I am a user with a password
      And my email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "fred@bloggs.com" as the email
      And I enter "other@fail.com" as the alternative email
      And I press "Create new account"
      Then I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails

    Scenario: Alternative emails aren't saved for decoupled users
      Given I am a user without a password
      And my email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "fred@bloggs.com" as the email
      And I enter "other@fail.com" as the alternative email
      And I press "Create new account"
      Then I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails

    Scenario: Acquiring a decoupled user by alternative email from registration form
      Given I am a user without a password
      And my email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "other@fail.com" as the email
      And I enter "fred@bloggs.com" as the alternative email
      And I press "Create new account"
      Then I should see "An account is already registered for fred@bloggs.com"
      And I should see "A welcome message with further instructions has been sent to your email address"
      And I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails

    Scenario: Acquiring a coupled user by alternative email from registration form
      Given I am a user with a password
      And my email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "other@fail.com" as the email
      And I enter 'fred@bloggs.com' as the alternative email
      And I press "Create new account"
      Then I should see "An account is already registered for fred@bloggs.com"
      And I should see "A link to help you log in has been sent to fred@bloggs.com"
      And I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails

    Scenario: Acquiring a decoupled user by alternative email on alternative email
      Given I am a user without a password
      And my alternative email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "other@fail.com" as the email
      And I enter "fred@bloggs.com" as the alternative email
      And I press "Create new account"
      Then I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails

    Scenario: Acquiring a coupled user by alternative email on alternative email
      Given I am a user with a password
      And my alternative email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "other@fail.com" as the email
      And I enter 'fred@bloggs.com' as the alternative email
      And I press "Create new account"
      Then I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails

    Scenario: Space-separated alternative emails on registration form
      Given I am a user without a password
      And my email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "other@fail.com" as the email
      And I enter 'other2@fail.com fred@bloggs.com' as the alternative email
      And I press "Create new account"
      Then I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails
      And "other2@fail.com" is not one of my alternative emails

    Scenario: Comma-separated alternative emails on registration form
      Given I am a user without a password
      And my email is "fred@bloggs.com"
      When I go to the registration form
      And I enter "other@fail.com" as the email
      And I enter 'fred@bloggs.com, other2@fail.com' as the alternative email
      And I press "Create new account"
      Then I should get a welcome email at "fred@bloggs.com"
      And "other@fail.com" is not one of my alternative emails
      And "other2@fail.com" is not one of my alternative emails

FEATURE;


  protected function setUp(): void {
    parent::setUp();
    $this->startMailCollection();
    $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['mail' => ['fred@bloggs.com', 'other@fail.com', 'other2@fail.com']]);
    foreach ($users as $user) {
      $user->delete();
    }
  }

  public function tearDown(): void {
    $this->restoreMailSettings();
    $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['mail' => ['fred@bloggs.com', 'other@fail.com', 'other2@fail.com']]);
    foreach ($users as $user) {
      $this->markEntityForCleanup($user);
    }
    parent::tearDown();
  }

  /**
   * @When I go to the registration form
   */
  public function iGoToRegister() {
    $this->drupalGet('/user/register');
    $this->waitToPassHoneypot();
  }

  /**
   * @When I indicate I am new to the Sangha
   */
  public function iIndicateIAmNewToTheSangha() {
    $this->selectRadio("ahs_user_management_email_known", "1");
  }

  /**
   * @Then I should (still )not have a username
   */
  public function iShouldNotHaveAUsername() {
    $this->assertEmpty($this->getMe()->getAccountName());
  }

  /**
   * @Then I should (still )have a username
   */
  public function iShouldHaveAUsername() {
    $this->assertNotEmpty($this->getMe()->getAccountName());
  }

  /**
   * @When I set a password
   * @When I change my password
   */
  public function iGetAPassword() {
    $this->getMe()->setPassword($this->randomMachineName())->save();
  }

  /**
   * @When I enter my email as the email
   */
  public function iEnterMyEmailAsTheEmail() {
    $page = $this->getCurrentPage();
    $page->fillField('mail', $this->getMyEmail());
  }

  /**
   * @When I enter :email as the email
   */
  public function iEnterAsTheEmail($email) {
    $page = $this->getCurrentPage();
    $page->fillField('mail', $email);
  }

  /**
   * @When I enter my alternative email as the alternative email
   */
  public function iEnterMyAlternativeEmailAsTheEmail() {
    $page = $this->getCurrentPage();
    $page->fillField('Alternative emails', $this->getMyAlternativeEmail());
  }



  /**
   * @When I enter :string as the alternative email(s)
   */
  public function iEnterAsTheAlternativeEmail($string) {
    $page = $this->getCurrentPage();
    $page->fillField('Alternative emails', $string);
  }

  /**
   * @Then I should get a welcome email at my email
   */
  public function iShouldGetAWelcomeEmailAtMyEmail() {
    $this->assertWelcomeEmail($this->getMyEmail());
  }

  /**
   * @Then I should get a welcome email at my alternative email
   */
  public function iShouldGetAWelcomeEmailAtMyAlternativeEmail() {
    $this->assertWelcomeEmail($this->getMyAlternativeEmail());
  }

  /**
   * @Then I should get a welcome email at :email
   */
  public function iShouldGetAWelcomeEmailAt($email) {
    $this->assertWelcomeEmail($email);
  }

  protected function assertWelcomeEmail($email) {
    $mails = $this->getMail(['to' => $email]);
    // 1 new user registration email, 1 enrolment thankyou.
    //$this->assertCount(2, $mails, 'Exactly 2 emails should be sent to me');
    $this->assertMailCount($mails, 1);
  }

  /**
   * @Then :email is not one of my alternative emails
   */
  public function isNotOneOfMyAlternativeEmails($email) {
    $emails = array_column($this->getMe()->get('alternative_user_emails')->getValue(), 'value');
    $this->assertNotContains($email, $emails, "$email should not be of my alternative emails");
  }

  /**
   * @Then my email is still :email
   */
  public function myEmailIsStill($email) {
    $this->assertSame($email, $this->getMyEmail());
  }


}

