<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\OneTimeLoginTrait;

class UserManageTest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use FormTrait;
  use UserTestTrait;
  use OneTimeLoginTrait;

  protected $myDisplayName;
  protected $newEmail;
  protected $newName;
  protected $mentorDescription;

  protected static $feature = <<<'FEATURE'
Feature: Support staff can manage students
    In order to provide better admin support to people
    We need to be able to update their basic info

    Scenario: Can't change password when managing another
      Given there is a student
      And I am logged in as a "student_admin"
      When I manage the student
      Then I cannot change the password

    Scenario: Can change own password
      Given I am logged in as a "student_admin"
      When I edit my own information
      Then I can change the password

    Scenario: No add more link for about profile
      Given there is a student
      And I am logged in as a "student_admin"
      When I manage the student
      Then I should not see "Add more"

    Scenario: Managing student name as student support
      Given there is a student
      And I am logged in as a "student_support"
      When I manage the student
      And I set the Sangha name and email
      Then the student is updated
      And my display name should be unchanged
      # Try again as we may have created a subscriber which might matter
      When I manage the student
      And I set the Sangha name and email
      Then the student is updated

    Scenario: Managing student name as student admin
      # Student admins have more permisisons, which can trigger more bugs
      Given there is a student
      And I am logged in as a "student_admin"
      When I manage the student
      And I set the Sangha name and email
      Then the student is updated
      And my display name should be unchanged

    Scenario: Student admin assign student support role
      Given there is a student
      And I am logged in as a "student_admin"
      When I manage the student
      Then I should not see the "events_admin" role option
      When I set the "student_support" role option
      Then the student has the "student_support" role

    Scenario: Student support assign member role
      Given there is a student
      And I am logged in as a "student_support"
      When I manage the student
      When I set the "member" role option
      Then the student has the "member" role

    Scenario: Student support assigning member and former member
      Given there is a student
      And I am logged in as a "student_support"
      When I manage the student
      When I set the "former_member" role option
      Then the student does not have the "former_member" role
      And the student still has the "member" role
      When I unset the "member" role option
      Then the student does not have the "member" role
      When I set the "former_member" role option
      Then the student has the "former_member" role
      When I set the "member" role option
      Then the student does not have the "former_member" role
      And the student has the "member" role

    Scenario: Administrator assign any role
      Given there is a student
      And I am logged in as a "administrator"
      When I manage the student
      When I set the "student_admin" role option
      Then the student has the "student_admin" role

    Scenario: Managing mentor profile as student admin
      Given there is a student
      And I am logged in as a "student_admin"
      When I manage the student
      And I fill in the mentor description
      And I press "Save"
      When I view the student
      Then I should see the mentor description

    Scenario: One time login link
      Given there is a student
      And I am logged in as a "administrator"
      When I view the student
      And I should see 'Generate one-time login link'
      When I click 'Generate one-time login link'
      Then a one-time link should be seen on the page
      Then I am not logged in
      When I follow the one-time login link
      Then I can set my password and login

FEATURE;


  /**
   * @When I manage the student
   */
  public function iManageTheStudent() {
    $this->myDisplayName = $this->getMe()->getDisplayName();
    $this->drupalGet("/user/" . $this->student->id() . "/manage");
  }

  /**
   * @When I edit my own information
   */
  public function iEditMyOwnInformation() {
    $this->drupalGet("/user/" . $this->getMe()->id() . "/edit");
  }

  /**
   * @Then I cannot change the password
   */
  public function iCannotChangeThePassword() {
    // Confirm-password input is hidden by default.
    $this->assertSession()->pageTextNotContains('Password');
  }

  /**
   * @Then I can change the password
   */
  public function iCanChangeThePassword() {
    $this->assertSession()->pageTextContains('Password');
    // Fill Password input to trigger JS for password-confirm input.
    $this->getCurrentPage()->fillField('Password', 'test');
    $this->assertSession()->pageTextContains('Confirm password');
  }

  /**
   * @When I set the Sangha name and email
   */
  public function iSetTheSanghaNameAndEmail() {
    $page = $this->getCurrentPage();
    $this->newEmail = $this->randomEmail();
    $this->newName = $this->randomMachineName();
    $page->fillField('Email', $this->newEmail);
    $page->findLink("Names & phone")->click();
    $page->fillField('Sangha name', $this->newName);
    $page->pressButton('Save');
  }

  /**
   * @Then the student is updated
   */
  public function theStudentIsUpdated() {
    $student = $this->reloadEntity($this->student);
    $profile = $student->get('about_profiles')->entity;
    $this->assertEntityType($profile, 'profile');
    $profile = $this->reloadEntity($profile);
    $student = $this->reloadEntity($this->student);
    $this->assertUserProfiles($student);

    $this->assertSame($this->newEmail, $student->getEmail(), "The user email should be updated");
    $this->assertSame($this->newName, $profile->get('field_name_sangha')->value, "The Sangha name should be updated");
    $this->assertEquals(ucfirst($this->newName), (string) $student->getDisplayName(), "The display name should be updated");
  }

  /**
   * @Then my display name should be unchanged
   */
  public function myDisplayNameShouldBeUnchanged() {
    // There was weird bug where the profile widget temporarily saved the new
    // profile to the editing user not the edited user.
    $this->assertEquals((string) $this->myDisplayName, (string) $this->getMe()->getDisplayName(), "The editing user's display name should be unchanged.");
  }

  /**
   * @Then I should not see the :role role option
   */
  public function iShouldNotSeeTheRoleOption($role) {
    $this->switchTab('roles');
    $this->assertSession()->fieldNotExists("roles[$role]");
    $this->assertSession()->fieldNotExists("role_change[$role]");
  }

  /**
   * @When I set the :role role option
   */
  public function iSetTheRoleOption($role) {
    $this->switchTab('roles');
    // Role delegation adds its own role_change field for use by users who don't
    // have the 'administer users' permission.
    $page = $this->getSession()->getPage();
    $field = $page->findField("roles[$role]") ?? $page->findField("role_change[$role]");
    $field->check();
    $page->pressButton('Save');
  }

  protected function switchTab($tabName) {
    $tab = $this->getSession()->getPage()->find('xpath', '//a[@href="#edit-group-' . $tabName . '"]');
    $this->assertNotEmpty($tab, "There should be a $tabName tab");
    $tab->click();
  }

  /**
   * @When I unset the :role role option
   */
  public function iunsetTheRoleOption($role) {
    $page = $this->getSession()->getPage();
    $this->switchTab('roles');
    // Role delegation adds its own role_change field for use by users who don't
    // have the 'administer users' permission.
    $field = $page->findField("roles[$role]") ?? $page->findField("role_change[$role]");
    $field->uncheck();
    $page->pressButton('Save');
  }


  /**
   * @Then the student( still) has the :role role
   */
  public function theStudentHasTheRole($role) {
    $this->assertTrue($this->reloadEntity($this->student)->hasRole($role), "The student should have the $role role.");
  }

  /**
   * @Then the student does not have the :role role
   */
  public function studentDoesNotHaveTheRole($role) {
    $this->assertFalse($this->reloadEntity($this->student)->hasRole($role), "The student should not have the $role role.");
  }

  /**
   * @When I fill in the mentor description
   */
  public function iFillInTheMentorDescription() {
    $this->mentorDescription = $this->randomMachineName();
    $this->switchTab('mentoring');
    // Waiting to switch a tab.
    sleep(3);
    $this->fillWysiwygEditor('mentor_profiles[0][entity][field_description][0][value]', $this->mentorDescription);
  }

  /**
   * @Then a one-time link should be seen on the page
   */
  public function ISeeOneTimeLoginLink() {
    $message = $this->getCurrentPage()->find('css', '.messages__wrapper')->getText();
    $this->assertPageHasOneTimeLoginLink($message);
  }

}

