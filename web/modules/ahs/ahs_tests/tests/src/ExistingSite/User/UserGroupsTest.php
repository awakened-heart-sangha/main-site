<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group NewDatabaseRequired
 * @group js
 */
class UserGroupsTest extends ExistingSiteJsBehatBase  {

  use TrainingTrait;

  protected $course;
  protected $event;
  protected $course_template;

  protected static $feature = <<<'FEATURE'
Feature: Pages showing joined and possible groups for a user
    In order for administrators to support users
    They need to see the groups the user has or could join

    Background:
      Given a course that starts in the future
      And I am logged in as a "student_support"
      Given experiences:
        | experience  |
        | ExperienceB |
        | ExperienceC |
      And the course has no eligibility conditions
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceC |
      And a user named "Fred" has training records for:
        | experience  |
        | ExperienceB |
      And a user named "Jane" has training records for:
        | experience  |
        | ExperienceC |

    Scenario: Joined groups page
      When I visit the user "/participation/joined" page for Fred
      Then the page header contains "Fred"
      And I should not see the course
      When Fred joins the course
      And I visit the user "/participation/joined" page for Fred
      Then I should see the course

    Scenario: Eligible groups page
      When I visit the user "/participation/eligible" page for Fred
      Then the page header contains "Fred"
      And I should see the course
      When Fred joins the course
      And I visit the user "/participation/eligible" page for Fred
      Then I should not see the course

    Scenario: Suggested groups page - not suggested
      When I visit the user "/participation/suggested" page for Fred
      Then the page header contains "Fred"
      And I should not see the course
      When Fred joins the course
      And I visit the user "/participation/suggested" page for Fred
      Then I should not see the course

    Scenario: Suggested groups page - suggested
      When I visit the user "/participation/suggested" page for Jane
      Then the page header contains "Jane"
      And I should see the course
      When Jane joins the course
      And I visit the user "/participation/suggested" page for Jane
      Then I should not see the course

    Scenario: Experiences page
      When I visit the user "/participation/experiences" page for Fred
      Then the page header contains "Fred"
      And I should see "ExperienceB"
      When I visit the user "/participation/experiences" page for Jane
      Then the page header contains "Jane"
      And I should see "ExperienceC"

    Scenario: Non-admin access to own groups
      Given I am logged in as a "member"
      When I visit the user "/participation/eligible" page for myself
      Then I should see the course
      When I join the course
      And I visit the user "/participation/eligible" page for myself
      Then I should not see the course
      When I visit the user "/participation/joined" page for myself
      Then I should see the course
      When I visit the user "/participation/suggested" page for myself
      Then I should not see "Access denied"

    Scenario: Non-admin access to other people's groups
      Given I am logged in as a "member"
      When I visit the user "/participation/joined" page for Fred
      Then I should see "Access denied"
      When I visit the user "/participation/eligible" page for Fred
      Then I should see "Access denied"
      When I visit the user "/participation/suggested" page for Fred
      Then I should see "Access denied"

    Scenario: Non-admin access to own unpublished groups
      Given an unpublished future suggested course
      And I am logged in as a "member"
      When I visit the user "/participation/eligible" page for myself
      Then I should not see the course
      When I visit the user "/participation/suggested" page for myself
      Then I should not see the course
      When I join the course
      And I visit the user "/participation/joined" page for myself
      Then I should not see the course

    Scenario: Courses, events & templates as possible user groups
      Given a future suggested course
      And a future suggested event
      And a suggested course_template
      And I am logged in as a "member"
      When I visit the user "/participation/eligible" page for myself
      Then I should see the course
      And I should see the event
      And I should see the course_template
      When I visit the user "/participation/suggested" page for myself
      Then I should see the course
      And I should see the event
      And I should see the course_template

    Scenario: Caching for access to other people's groups
      When Fred joins the course
      Given I am logged in as a "member"
      When I visit the user "/participation/joined" page for Fred
      Then I should see "Access denied"
      Given I am logged in as a "student_support"
      When I visit the user "/participation/joined" page for Fred
      Then I should see the course
      Given I am logged in as a "member"
      When I visit the user "/participation/joined" page for Fred
      Then I should see "Access denied"


FEATURE;

  /**
   * @When I visit the user :path page for :name
   */
  public function iVisitTheUserPageForName($path, $name) {
    $user = $name === 'myself' ? $this->getMe() : $this->users[$name];
    $this->drupalGet('/user/' . $user->id() . $path);
  }

  /**
   * @Given I should see the :bundle
   */
  public function iShouldSeeTheGroup($bundle) {
    $this->assertSession()->pageTextContains($this->{$bundle}->label());
  }

  /**
   * @Given I should not see the :bundle
   */
  public function iShouldNotSeeTheGroup($bundle) {
    $this->assertSession()->pageTextNotContains($this->{$bundle}->label());
  }

}

