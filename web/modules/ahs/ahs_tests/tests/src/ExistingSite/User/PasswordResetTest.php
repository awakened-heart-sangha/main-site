<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\OneTimeLoginTrait;

class PasswordResetTest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use UserDefinitions;
  use FormTrait;
  use MailCollectionTrait;
  use OneTimeLoginTrait;

  protected static $feature = <<<'FEATURE'
Feature: Password reset
    In order to be able to regain site access if I forget my password
    As a user I need to be able to reset my password

    Scenario: User resets password
      Given I am a user with a password
      When I visit "user/login"
      And I enter my email and a bad password
      And I press "Log in"
      Then I should see "Unrecognised email or password"
      When I select "Forgot your password?"
      And I wait in order to not trigger honeypot
      And I enter my email
      And I press "Submit"
      Then I am on "/user/login"
      And I should see "Further instructions have been sent to your email address"
      And a password reset link should be sent to my email
      When I follow the one-time login link
      Then I can set my password and login
      When I logout
      And I press "Log out"
      And I visit "/user/login"
      When I enter my email and my new password          |
      And I press "Log in"
      Then I should not see "Unrecognised email or password"
      And I should see "Registered"
      And the page header does not contain "Log in"

    Scenario: User resets password using alternative email
      Given I am a user with a password
      And I have an alternative email
      When I visit "user/password"
      And I wait in order to not trigger honeypot
      And I enter my alternative email
      And I press "Submit"
      Then the page header contains "Log in"
      And I should see "Further instructions have been sent to your email address"
      And a password reset link should be sent to my alternative email

    Scenario: User resets password using main email but has alternative
      Given I am a user with a password
      And I have an alternative email
      When I visit "user/password"
      And I wait in order to not trigger honeypot
      And I enter my email
      And I press "Submit"
      Then a password reset link should be sent to my email

    Scenario: Decoupled user resets password
      Given I am a user without a password
      When I visit "user/password"
      And I wait in order to not trigger honeypot
      And I enter my email
      And I press "Submit"
      Then a password reset link should be sent to my email
      When I follow the one-time login link
      Then I can set my password and login
      When I logout
      And I press "Log out"
      And I visit "/user/login"
      When I enter my email and my new password          |
      And I press "Log in"
      Then I should not see "Unrecognised email or password"
      And I should see "Registered"
      And the page header does not contain "Log in"


FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->startMailCollection();
  }

  public function tearDown(): void {
    $this->restoreMailSettings();
    parent::tearDown();
  }

  /**
   * @When I enter my email and a bad password
   */
  public function iEnterMyEmailAndABadPassword() {
    $page = $this->getCurrentPage();
    $page->fillField('name', $this->getMyEmail());
    $page->fillField('pass', 'badpassword');
  }

  /**
   * @When I enter my email
   */
  public function iEnterMyEmail() {
    $page = $this->getCurrentPage();
    $page->fillField('Email', $this->getMyEmail());
  }

  /**
   * @When I enter my alternative email
   */
  public function iEnterMyAlternativeEmail() {
    $page = $this->getCurrentPage();
    $page->fillField('Email', $this->getMyAlternativeEmail());
  }

  /**
   * @Then a password reset link should be sent to my email
   */
  public function iShouldReceiveAnEmailWithAPasswordResetLink() {
    $this->assertPasswordResetEmailSent($this->getMe()->getEmail());
  }

  /**
   * @Then a password reset link should be sent to my alternative email
   */
  public function aPasswordResetLinkShouldBeSentToMyAlternativeEmail() {
    $this->assertPasswordResetEmailSent($this->getMyAlternativeEmail());
  }

  protected function assertPasswordResetEmailSent($email) {
    $mails = $this->getMail(['to' => $email]);
    $this->assertCount(1, $mails, 'Exactly 1 email should be sent to me');

    $properties = [
      'subject' => 'Reset password for the Awakened Heart Sangha website',
      'body' => [
        'A request to reset the password for your account has been made at the Awakened Heart Sangha website',
      ],
      'reply-to' => 'support@ahs.org.uk'
    ];
    $this->assertTrue($this->matchesMail($mails[0], $properties), sprintf("'Mail should have the right subject & body':\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($mails, 2000), print_r($properties, TRUE)));
    $this->assertEmailHasOneTimeLoginLink($mails[0]);
  }

  /**
   * @Then I am on :path
   */
  public function iAmOn($uri) {
    $this->assertSession()->addressEquals($uri);
  }

  /**
   * @When I enter my email and my new password          |
   */
  public function iEnterMyEmailAndMyNewPassword() {
    $page = $this->getCurrentPage();
    $page->fillField('name', $this->getMyEmail());
    $page->fillField('pass', 'mynewpassword');
  }

}

