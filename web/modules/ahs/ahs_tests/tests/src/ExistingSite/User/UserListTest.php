<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Behat\Gherkin\Node\TableNode;

/**
 * Js is not needed here, and there's too many examples to bother visual
 * regression testing.
 *
 * @group NewDatabaseRequired
 */
class UserListTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use UserTrait;
  use UserDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Support staff can find students
    In order to find students
    Support staff need to be able to search by name

    Background:

    Scenario Outline: User can be found when searched for.
      Given there are students:
        | firstname | lastname | nickname | sangha name|
        | James     | Smith    | Jim      | Dorje      |
        | Frederick | Bloggs   | Fred     | Sherab     |
        | John      | Doe      |          |            |

      Given I am logged in as an "student_support"
      When I visit "admin/people"
      Then I should not see "Has web account?"
      And I search for "<searches>"
      Then I should see "Jim Dorje Smith"
      And I should not see "Fred Sherab Bloggs"
      And I should not see "John Doe"

      Examples:
      | searches    |
      | James       |
      | Jim         |
      | jim         |
      | Smith       |
      | Jam         |
      | Smit        |
      | Ji          |
      | Dorje       |
      | Dor         |
      | James Smith |
      | Jam Smith   |
      | Jim Smith   |
      | James Smit  |
      | Jim Dorje   |
      | Dorje Smith |


FEATURE;

  /**
   * @Given there are students:
   */
  public function thereAreStudents(TableNode $table) {
    foreach($table->getHash()  as $row) {
      $edit = [
        'mail' => $this->randomEmail(),
        'roles' => ['member']
      ];
      $user = $this->createUser([], $edit);
      $fields = [
        'field_name' => [
          'given' => $row['firstname'],
          'family' => $row['lastname']
        ],
        'field_name_nickname' => $row['nickname'],
        'field_name_sangha' => $row['sangha name'],
      ];
      $this->updateProfile($user, 'about', $fields);
    }
  }

  /**
   * @When I search for :term
   */
  public function iSearchFor($term) {
    $page = $this->getCurrentPage();
    $page->fillField('AHS name', $term);
    $page->findButton('Search')->press();
  }

}

