<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;

class UserMePathTest extends UserTestBase {

  use BrowsingDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Me paths
    In order for support to guide people to access own info
    We need explicit paths to current user

    Scenario: Me paths
      Given I am logged in
      When I go to '/me'
      Then I should not see 'page not found'
      And I should see my email
      When I go to '/me/email-news'
      Then I should not see 'page not found'
      And I should see 'Email news'
      When I go to '/me/contribution/online'
      Then I should not see 'page not found'
      And I should see 'Online donations & payments'

FEATURE;

  /**
   * @Then I should see my email
   */
  public function iShouldSeeMyEmail() {
    $this->assertSession()->pageTextContains($this->getMe()->getEmail());
  }

}

