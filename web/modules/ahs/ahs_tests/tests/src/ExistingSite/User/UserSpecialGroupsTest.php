<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\group\Entity\Group;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

/**
 * @group ClonedDatabaseRequired
 */
class UserSpecialGroupsTest extends ExistingSiteBehatBase  {

  use GroupTrait;
  use GroupDefinitions;
  use UserTrait;
  use UserDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Special groups for users
    In order to handle members
    They should be put in a group

    Scenario: Automatically added to members group
      Given I am a user
      When I change to being a "member"
      Then I am in the membership group
      When I change to being a "former_member"
      Then I am not in the membership group
      When I change to being a "member"
      Then I am in the membership group
      When I have no roles
      Then I am not in the membership group
      When I change to being a "former member"
      Then I am not in the membership group
      When I have no roles
      Then I am not in the membership group
      When I change to being a "member"
      Then I am in the membership group

FEATURE;

  /**
   * @When I change to being a :role
   */
  public function iBecomeARole($rid) {
    $user = $this->getMe();
    $user->set('roles', NULL);
    $user->addRole($rid);
    $user->save();
  }

  /**
   * @When I have no roles
   */
  public function iHaveNoRoles() {
    $user = $this->getMe();
    $user->set('roles', NULL);
    $user->save();
  }


  /**
   * @Then I am in the membership group
   */
  public function iAmInTheMembershipGroup() {
    $group = Group::load(583);
    $this->assertNotEmpty($group->getMember($this->getMe()));
  }

  /**
   * @Then I am not in the membership group
   */
  public function iAmNotInTheMembershipGroup() {
    $group = Group::load(583);
    $this->assertEmpty($group->getMember($this->getMe()));
  }

}

