<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;

class AdminNoteTest extends UserTestBase {

  use BrowsingDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Support staff can make notes about people
    In order to provide better admin support to people
    We need to be able to make notes about them

    Scenario: Support staff can make private notes
      Given there is a student
      And I am logged in as a "student_support"
      When I view the student
      And I leave a note
      When I view the student
      Then I see my note
      Given the student is logged in
      And the student views themselves
      Then the student cannot see my note
      Given another support person is logged in
      When the other support person views the student
      Then the other support person can see my note

FEATURE;

  protected $note = "A note about the user";

  /**
   * @When I leave a note
   */
  public function iLeaveANote() {
    $page = $this->getCurrentPage();
    $page->fillField('Comment', $this->note);
    $page->findButton('Save')->press();
  }

  /**
   * @Then I see my note
   */
  public function iSeeMyNote() {
    $this->assertSession()->pageTextContains($this->note);
  }

  /**
   * @Then the student cannot see my note
   */
  public function theStudentCannotSeeMyNote() {
    $this->assertSession()->pageTextNotContains($this->note);
  }

  /**
   * @Given another support person is logged in
   */
  public function anotherSupportPersonIsLoggedIn() {
    $edit = [
      'roles' => ['student_support'],
      'mail' => $this->randomEmail(),
    ];
    $user = $this->createUser([],$edit);
    $this->setMe($user);
    $this->iLogin();
  }

  /**
   * @When the other support person views the student
   */
  public function theOtherSupportPersonViewsTheStudent() {
    $this->drupalGet("/user/" . $this->student->id());
    $this->assertSession()->pageTextContains($this->student->calculateDisplayName());
  }

  /**
   * @Then the other support person can see my note
   */
  public function theOtherSupportPersonCanSeeMyNote() {
    $this->assertSession()->pageTextContains($this->note);
  }

}

