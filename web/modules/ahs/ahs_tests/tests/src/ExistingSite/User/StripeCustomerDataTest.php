<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

class StripeCustomerDataTest extends ExistingSiteJsBehatBase {

  use UserDefinitions;
  use UserTrait;
  use QueueTrait;
  use CommerceDefinitions;
  use CommerceTrait;

  protected static $feature = <<<'FEATURE'
Feature: Stripe Customer Data Update
    In order to link Stripe and CRM records
    Customers in Stripe are synced with Drupal profile data

    Scenario: Name and email are propagated to new Stripe customer
      Given I have a Stripe customer
      And the queue is processed
      Then the name is updated on my Stripe customer

    Scenario: Changes to user email are propagated to the Stripe customer
      Given I have a Stripe customer
      And the queue is processed
      When I change my email
      And the queue is processed
      Then the email is updated on my Stripe customer

    Scenario: Changes to user display name are propagated to the Stripe customer
      Given I have a Stripe customer
      And the queue is processed
      When a Sangha name is given to me programatically
      And the queue is processed
      Then the name is updated on my Stripe customer

    # Necessary because hook_user_update original entity doesn't always handle profile well.
    Scenario: Edits to user display name are propagated to the Stripe customer
      Given I have a Stripe customer
      And the queue is processed
      When I add a Sangha name
      And the queue is processed
      Then the name is updated on my Stripe customer

FEATURE;

  protected function setUp(): void {
    parent::setUp();
    // Instantiating the gateway ensures the Stripe connection is setup.
    $gateway = $this->entityTypeManager->getStorage('commerce_payment_gateway')->load('shrimala_trust_stripe');
    $gateway->getPlugin();

    $this->clearQueues();
  }

  /**
   * @Given I have a Stripe customer
   */
  public function iHaveAStripeCustomer() {
    $me = $this->getMe();
    $customer = \Stripe\Customer::create([
      'email' => $me->getEmail(),
      'description' => 'Customer for ' . $me->getEmail(),
    ]);
    $me->get('commerce_remote_id')->setByProvider('shrimala_trust_stripe|test', $customer->id);
    $me->save();
  }

  /**
   * @When the queue is processed
   */
  public function theQueueIsProcessed() {
    $this->processAdvancedQueue('ahs_commerce_stripe_customer_update');
  }


}

