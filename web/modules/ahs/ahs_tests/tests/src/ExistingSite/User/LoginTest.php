<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\MailDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;

/**
 * @group js
 */
class LoginTest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use UserDefinitions;
  use MailDefinitions;
  use MailCollectionTrait;
  use FormTrait;

  protected static $feature = <<<'FEATURE'
Feature: Users can login
    In order to offer personalised information
    We need users to be able to identify themselves persistently

    Scenario: Page title is present on login page
      Given I visit "user/login"
      Then I should see the page title block

    Scenario: Users with password can login
      Given I am a user with a password
      And I visit "user/login"
      Then I should see "Log in"
      When I enter my email and password
      And I press "Log in"
      Then I should not see "Log in"
      And I should not see "Unrecognized e-mail address or password"

    Scenario: Users can login using alternative email
      Given I am a user with a password
      And I have an alternative email
      And I visit "user/login"
      When I enter my alternative email and password
      And I press "Log in"
      Then I should not see "Log in"
      And I should not see "Unrecognized e-mail address or password"

    Scenario: Login if access denied
      Given I am a user with a password
      And I have the "administrator" role
      And I am not logged in
      When I go to "/admin"
      Then the page header contains "Log in"
      And I should see "Access denied"
      When I enter my email and password
      And I press "Log in"
      Then I should not see "Log in"
      And I should see "Administration"

    Scenario: Access denied for authenticated users
      Given I am a user with a password
      And I am logged in
      When I visit "/admin"
      Then I should see "Access denied"
      And I should not see "Log in"

FEATURE;

  /**
   * @When I enter my email and password
   */
  public function iEnterMyEmailAndPassword() {
    $page = $this->getCurrentPage();
    $page->fillField('name', $this->getMe()->getEmail());
    $page->fillField('pass', $this->getMe()->passRaw);
  }

  /**
   * @When I enter my alternative email and password
   */
  public function iEnterMyAlternativeEmailAndPassword() {
    $page = $this->getCurrentPage();
    $page->fillField('name', $this->getMe()->get('alternative_user_emails')->first()->value);
    $page->fillField('pass', $this->getMe()->passRaw);
  }

}

