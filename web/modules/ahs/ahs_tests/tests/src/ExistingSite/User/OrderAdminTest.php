<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\ContentTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\EnrolmentTrait;
use Drupal\Tests\ahs_tests\Traits\EntityTrait;
use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;

/**
 * @group NewDatabaseRequired
 */
class OrderAdminTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use EntityTrait;
  use CommerceTrait;
  use UserDefinitions;
  use ContentTrait;
  use GroupTrait;
  use EnrolmentTrait;

  protected $order;
  protected $customer;
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Support staff can see orders
    In order to help customers and do bookkeeping
    Support staff need to be able to see orders

    Scenario Outline: Staff can see all orders
      Given I am logged in as a "<role>"
      And there is a miscellaneous donation
      When I visit "admin/commerce/orders"
      Then I can view the order

      Examples:
        | role            |
        | administrator   |
        | student_support |
        | accountant      |

    Scenario Outline: Staff can see misc donations
      Given I am logged in as a "<role>"
      And there is a miscellaneous donation
      When I visit "admin/commerce/orders/misc"
      Then I can view the order

      Examples:
        | role            |
        | administrator   |
        | student_support |
        | accountant      |

    Scenario Outline: Staff can see enrolments
      Given the DHB special group
      And I am logged in as a "<role>"
      And there is an enrolment
      When I visit "admin/commerce/orders/lah"
      Then I can view the order

      Examples:
        | role            |
        | administrator           |
        | student_support |
        | accountant      |

FEATURE;


  /**
   * @Given the DHB special group
   */
  public function theDHBSpecialGroup() {
    $this->group = $this->ensureDHBGroups();
  }

  /**
   * @Given there is a miscellaneous donation
   */
  public function thereIsAMiscellaneousDonation() {
    $this->order = $this->createOrder('ahs_miscellaneous', 'misc_donation_miscellaneous');
  }

  /**
   * @Given there is an enrolment
   */
  public function thereIsAnEnrolment() {
    $this->ensureInitialMaterials();
    $this->order = $this->createOrder('ahs_lah_enrolment', 'ahs_membership_monthly');
  }

  /**
   * @Then I can view the order
   */
  public function iCanViewTheOrder() {
    // The order should be on the view.
    $this->assertSession()->pageTextContains($this->customer->getDisplayName(), "Customer display name should be visible on order list");
    $this->assertSession()->pageTextContains($this->customer->getEmail(), "Customer email should be visible on order list");

    // Clicking the order number should take you to the order details page.
    $this->getSession()->getPage()->find('named', ['link_or_button', $this->order->getOrderNumber()])->click();
    $this->assertSession()->pageTextContains("Order items", "Order detail page should have heading 'Order items'");
    $this->assertSession()->pageTextContains("Order activity", "Order detail page should have a heading 'Order activity'");
  }

}

