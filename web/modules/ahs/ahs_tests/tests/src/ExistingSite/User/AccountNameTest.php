<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBase;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Account name has complex logic.
 *
 * Only user with passwords should have account names.
 * Only members should have simple user names, and they should have them if available.
 *
 * @group NewDatabaseRequired
 */
class AccountNameTest extends ExistingSiteBase {

  use UserTrait;

  public function testDecoupledUser() {
    $user = $this->createDecoupledUser();
    $this->updateProfile($user, 'about', ['field_name' => ['given' => 'Tom', 'family' => 'Hanks']]);
    $user = $this->reloadUser($user);
    $this->assertEmpty($user->getAccountName(), "Decoupled users should not have account name");
    $user->save();
    $this->assertEmpty($user->getAccountName(), "Decoupled users should not have account name");
  }

  public function testCoupled() {
    $user = $this->createUser();
    $this->updateProfile($user, 'about', ['field_name' => ['given' => 'Tom', 'family' => 'Hanks']]);
    $user = $this->reloadUser($user);
    $expectedName = "tom_hanks_" . $user->id();
    $this->assertSame($expectedName, $user->getAccountName());
    $user->save();
    $this->assertSame($expectedName, $user->getAccountName());

    // Members should not have id appended to username unless collision with existing user.
    $user->addRole('member');
    $user->save();
    $expectedName = "tom_hanks";
    $this->assertSame($expectedName, $user->getAccountName());
    $user->save();
    $this->assertSame($expectedName, $user->getAccountName());

    // Add a second user with same name.
    $user2 = $this->createUser();
    $this->updateProfile($user2, 'about', ['field_name' => ['given' => 'Tom', 'family' => 'Hanks']]);
    $user2 = $this->reloadUser($user2);
    // As a non-member, this second user has id appended.
    $expectedName = "tom_hanks_" . $user2->id();
    $this->assertSame($expectedName, $user2->getAccountName());
    $user->save();
    $this->assertSame($expectedName, $user2->getAccountName());

    // When second user becomes a member, id is still appended because of name collision.
    $user2->addRole('member');
    $user2->save();
    $expectedName = "tom_hanks_" . $user2->id();
    $this->assertSame($expectedName, $user2->getAccountName());
    $user->save();
    $this->assertSame($expectedName, $user2->getAccountName());

    // When user ceases to be a member, id gets appended to username.
    $user->set('roles', NULL);
    $user->save();
    $expectedName = "tom_hanks_" . $user->id();
    $this->assertSame($expectedName, $user->getAccountName());
    $user->save();
    $this->assertSame($expectedName, $user->getAccountName());

    // When second user is now saved, name gets simplified as it no longer collides.
    $user2->save();
    $expectedName = "tom_hanks";
    $this->assertSame($expectedName, $user2->getAccountName(), "Members should be assigned simplified account name if it becomes available.");
    $user->save();
    $this->assertSame($expectedName, $user2->getAccountName());


  }

}

