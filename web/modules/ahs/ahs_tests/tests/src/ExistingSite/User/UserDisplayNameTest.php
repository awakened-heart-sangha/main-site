<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;

class UserDisplayNameTest extends ExistingSiteBehatBase {

  use UserDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: User names
    In order to help people communicate
    We need to store names and have sensible display names

    Background:
      Given I am a new user with the email "herbert@example.com"

    Scenario: Display name is based on email if no real names or Sangha name
      Then I have the display name "herbert"
      When I change my email to "jack@example.com"
      Then I have the display name "jack"

    Scenario: Display name defaults to real first name if no Sangha name or nickname
      Given I have the firstname "James" and lastname "Bloggs"
      Then I have the display name "James Bloggs"
      When I change my firstname to "Fred"
      Then I have the display name "Fred Bloggs"

    Scenario: Display name includes Sangha name if set
      Given I have the firstname "Fred" and lastname "Bloggs"
      And I have the Sangha name "Dorje"
      Then I have the display name "Fred Dorje Bloggs"

    Scenario: Display name uses Sangha name only if that's all there is
      Given I have the Sangha name "Dorje"
      Then I have the display name "Dorje"

    Scenario: Display name uses Sangha name only if no lastname
      Given I have the firstname "Fred" and lastname ""
      And I have the Sangha name "Dorje"
      Then I have the display name "Fred Dorje"

    Scenario: Display name uses nickname instead of first name if set
      Given I have the firstname "Frederick" and lastname "Bloggs"
      And I have the nickname "Fred"
      Then I have the display name "Fred Bloggs"

    Scenario: Display name uses nickname only if that's all there is
      Given I have the nickname "Fred"
      Then I have the display name "Fred"

    Scenario: Display name uses nickname only if no lastname
      Given I have the firstname "Frederick" and lastname ""
      And I have the nickname "Fred"
      Then I have the display name "Fred"

    Scenario: Sangha name is dropped if identical to preferred name
      Given I have the firstname "Frederick" and lastname "Bloggs"
      And I have the nickname "Dorje"
      And I have the Sangha name "Dorje"
      Then I have the display name "Dorje Bloggs"

    Scenario: Anonymous has display name 'Anonymous'
      Given I am the anonymous user
      Then I have the display name "Anonymous"

    Scenario: Special characters are escaped in display names
      Given I have the firstname "Jim" and lastname "O'Neill"
      Then I have the display name "Jim O&#039;Neill"

FEATURE;

  /**
   * @Given I have the firstname :firstname and lastname :lastname
   */
  public function iHaveTheFirstnameAndLastname($firstname, $lastname) {
    $fields = ['field_name' => ['given' => $firstname, 'family' => $lastname,],];
    $this->updateProfile($this->getMe(), 'about', $fields);
    $this->assertEquals($firstname, $this->getMe()->get("about_profiles")->entity->field_name->given);
    $this->assertEquals($lastname, $this->getMe()->get("about_profiles")->entity->field_name->family);
  }

  /**
   * @Then I have the display name :displayName
   */
  public function iHaveTheDisplayName($displayName) {
    $this->assertEquals($displayName, (string) $this->getMe()->getDisplayName(), "Calculated display name should be correct.");
  }

  /**
   * @When I change my firstname to :firstname
   */
  public function iChangeMyFirstNameTo($firstname) {
    // Leave the lastname untouched.
    $lastname = $this->getMe()->get("about_profiles")->entity->get('field_name')->family;
    $fields = ['field_name' => ['given' => $firstname, 'family'=> $lastname]];
    $this->updateProfile($this->getMe(),'about', $fields);
    $this->assertEquals($firstname, $this->getMe()->get("about_profiles")->entity->field_name->given);
    $this->assertEquals($lastname, $this->getMe()->get("about_profiles")->entity->field_name->family);
  }

  /**
   * @Given I am a new user with the email :email
   */
  public function iAmANewUserWithTheEmail($email) {
    $this->createNamedUser('me', ['mail' => $email], FALSE, FALSE);
  }

  /**
   * @Given I have the nickname :name
   */
  public function iHaveTheNickname($name) {
    $fields = ['field_name_nickname' => $name];
    $this->updateProfile($this->getMe(), 'about', $fields);
    $this->assertEquals($name, $this->getMe()->get("about_profiles")->entity->field_name_nickname->value);
  }


}

