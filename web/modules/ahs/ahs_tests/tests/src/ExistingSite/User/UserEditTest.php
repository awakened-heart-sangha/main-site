<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\User;

use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;

class UserEditTest extends UserTestBase {

  use BrowsingDefinitions;
  use FormTrait;
  use NewsTrait;

  protected $newEmail;
  protected $newName;

  protected static $feature = <<<'FEATURE'
Feature: Users can edit their own details
    In order to reduce support burden
    Users need to be able to edit their own details

    Scenario: Can change own password
      Given I am logged in
      When I edit my own information
      Then I can change my password

    Scenario: Can change own email and name
      Given I am logged in
      When I edit my own information
      Then I should not see "Roles"
      When I set my Sangha name and email
      Then my information is updated
      And my display name should be changed

    # Members have simplenews subscribers
    Scenario: Member changing email updates subscriber
      Given I have the member role
      And I am logged in
      When I edit my own information
      Then I should not see "Roles"
      When I set my Sangha name and email
      Then my information is updated
      And my subscriber has my user's email


FEATURE;


  /**
   * @When I edit my own information
   */
  public function iEditMyOwnInformation() {
    $this->drupalGet("/user/" . $this->getMe()->id() . "/edit");
  }

  /**
   * @Then I can change my password
   */
  public function iCanChangeMyPassword() {
    $this->assertSession()->pageTextContains('Password');
    // Fill Password input to trigger JS for password-confirm input.
    $this->getCurrentPage()->fillField('Password', 'test');
    $this->assertSession()->pageTextContains('Confirm password');
  }

  /**
   * @When I set my Sangha name and email
   */
  public function iSetTheSanghaNameAndEmail() {
    $page = $this->getCurrentPage();
    $this->newEmail = $this->randomEmail();
    $this->newName = lcfirst($this->randomMachineName());
    $page->fillField('Email', $this->newEmail);
    $page->fillField('Sangha name', $this->newName);
    // Password is required to change own email.
    $page->fillField('Current password', $this->getMe()->passRaw);
    $page->pressButton('Save');
  }

  /**
   * @Then my information is updated
   */
  public function myInformationIsUpdated() {
    $me = $this->getMe();
    $this->assertSame($this->newEmail, $me->getEmail(), "My email should be updated");
    $profile = $me->get('about_profiles')->entity;
    $this->assertEntityType($profile, 'profile');
    $profile = $this->reloadEntity($profile);
    $this->assertSame($this->newName, $profile->get('field_name_sangha')->value, "My Sangha name should be updated");
    $this->assertEquals(ucfirst($this->newName), (string) $me->getDisplayName(), "My display name should be updated");
    $this->assertUserProfiles($me);
  }

  /**
   * @Then my subscriber has my user's email
   */
  public function mySubscriberHasMyUsersEmail() {
    $me = $this->getMe();
    $this->assertSame($me->getEmail(), $this->getUserSubscriberEmail($me), "My subscriber email should be updated");
  }

  /**
   * @Then my display name should be changed
   */
  public function myDisplayNameShouldBeChanged() {
    $this->assertEquals(ucfirst($this->newName), (string) $this->getMe()->getDisplayName(), "My display name should be changed.");
  }

}

