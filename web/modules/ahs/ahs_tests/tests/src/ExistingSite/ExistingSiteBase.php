<?php

namespace Drupal\Tests\ahs_tests\ExistingSite;

use weitzman\DrupalTestTraits\ExistingSiteBase as DTTExistingSiteBase;
use Drupal\Tests\ahs_tests\Traits\BaseTrait;
use Psr\Log\LoggerInterface;

/**
 * Base class for AHS site tests.
 */
abstract class ExistingSiteBase extends DTTExistingSiteBase implements LoggerInterface
{
  use BaseTrait;

  protected function setUp(): void {
    parent::setUp();
    $this->setUpBase();
  }

  public function tearDown(): void {
    $this->tearDownBase();
    parent::tearDown();
    $this->releaseMemory();
  }

  protected function debugPage($name = '') {
    $this->debugBasic($name);
  }

}
