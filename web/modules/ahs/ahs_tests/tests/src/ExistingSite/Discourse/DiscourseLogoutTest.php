<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Discourse;

use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\MockHttpClientTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\user\Entity\User;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;

/**
 * @group NewDatabaseRequired
 */
class DiscourseLogoutTest extends ExistingSiteBehatBase {

  use UserDefinitions;
  use UserTrait;
  use MockHttpClientTrait;
  use QueueTrait;

  protected static $feature = <<<'FEATURE'
Feature: Discourse user syncing
    In order to facilitate single sign on to Discourse
    Drupal users are synced with Discourse users

    Scenario: Updated users are synced to discourse
      Given I am logged in as a "member"
      When I am logged out
      Then I am logged out of discourse


FEATURE;

  /**
   * The discourse user id.
   *
   * @var int
   */
  protected $discourseId;

  protected function setUp(): void {
    parent::setUp();
    $this->setUpQueues();
    $this->discourseId = mt_rand();
    $this->mockHttpClient([
      ['200', ['user' => ['id' => $this->discourseId]]],
      ['200', []],
    ]);
  }

  /**
   * @When I am logged out
   */
  public function iAmLoggedOut() {
    // We can't do a real logout because the guzzle mocking doesn't seem to work for browser tests.
    \Drupal::moduleHandler()->invokeAll('user_logout', [$this->getMe()]);
  }


  /**
   * @Then I am logged out of discourse
   */
  public function iAmLoggedOutOfDiscourse() {
    $requests = $this->getNewHttpRequests();
    $this->assertNotEmpty($requests);
    $this->assertCount(2, $requests, "There should be exactly 2 new discourse requests. " . $this->prettyPrintHttpRequests($requests));

    $path = $requests[0]->getUri()->getPath();
    $this->assertSame('/users/by-external/' . $this->getMe()->id() . '.json', $path, "The first request should be to the user by external id path.");

    $path = $requests[1]->getUri()->getPath();
    $this->assertSame('/admin/users/' . $this->discourseId . '/log_out', $path, "The second request should be to the logout path.");
  }

}

