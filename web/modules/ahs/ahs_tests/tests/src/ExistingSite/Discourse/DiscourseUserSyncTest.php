<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Discourse;

use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\MockHttpClientTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\user\Entity\User;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;

/**
 * @group NewDatabaseRequired
 */
class DiscourseUserSyncTest extends ExistingSiteBehatBase {

  use UserDefinitions;
  use UserTrait;
  use MockHttpClientTrait;
  use QueueTrait;

  protected static $feature = <<<'FEATURE'
Feature: Discourse user syncing
    In order to facilitate single sign on to Discourse
    Drupal users are synced with Discourse users

    Scenario: Updated users are synced to discourse
      Given I am a user
      And the queue is processed
      Then my Discourse user is checked and not synced
      When I change my email
      And the queue is processed
      Then my Discourse user is checked and not synced
      When I become a member
      And the queue is processed
      Then my Discourse user is synced

    Scenario: Discourse usernames
      Given users:
        | reference | firstname | lastname | decoupled |
        | Fred1     | Fred      | Bloggs   | TRUE      |
        | Fred2     | Fred      | Bloggs   | TRUE      |
        | James     | James     | Bloggs   | TRUE      |
      Then the discourse username for Fred1 is "fred_bloggs_" with id
      And the discourse username for Fred1 is "fred_bloggs_" with id
      And the discourse username for James is "james_bloggs_" with id
      And Fred1 has no drupal account name
      And Fred2 has no drupal account name
      And James has no drupal account name
      When James gets a password
      Then the discourse username for James is "james_bloggs_" with id
      And the drupal account name for James is "james_bloggs_" with id
      When Fred1 becomes a member
      Then the discourse username for Fred1 is "fred_bloggs_" with id
      And Fred1 has no drupal account name
      When Fred1 gets a password
      Then the discourse username for Fred1 is "fred_bloggs"
      And the drupal account name for Fred1 is "fred_bloggs"
      When Fred2 becomes a member
      Then the discourse username for Fred2 is "fred_bloggs_" with id
      And Fred2 has no drupal account name
      When Fred2 gets a password
      # fred_bloggs is no longer available
      Then the discourse username for Fred2 is "fred_bloggs_" with id
      And the drupal account name for Fred2 is "fred_bloggs_" with id


FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->setUpQueues();
    $this->setUpSuccessfulMockHttpClient();
  }

  /**
   * @When the queue is processed
   */
  public function theQueueIsProcessed() {
    $this->processAdvancedQueue('ahs_discourse_sso_sync');
  }

  /**
   * @Then my Discourse user is checked and not synced
   */
  public function myDiscourseUserChecked() {
    $requests = $this->getNewHttpRequests();
    $this->assertNotEmpty($requests);
    $this->assertCount(1, $requests, "There should be exactly 1 new discourse requests. " . $this->prettyPrintHttpRequests($requests));
    $path = $requests[0]->getUri()->getPath();
    $this->assertSame('/users/by-external/' . $this->getMe()->id() . '.json', $path, "The path should be the user by external id path.");
  }

  /**
   * @Then my Discourse user is synced
   */
  public function myDiscourseUserIsSyncedOrCreated() {
    $requests = $this->getNewHttpRequests();
    $this->assertNotEmpty($requests);
    $this->assertCount(1, $requests, "There should be exactly 1 new discourse requests. " . $this->prettyPrintHttpRequests($requests));
    $this->assertDiscourseUserSsoSynced($requests[0]);
  }

    protected function assertDiscourseUserSsoSynced($request) {
    $path = $request->getUri()->getPath();
    $this->assertSame('/admin/users/sync_sso', $path, "The path should be the sync_sso path.");

    $body = json_decode($request->getBody(), true);
    $this->assertArrayHasKey('sso', $body, "The request body should have an sso key");
    parse_str(base64_decode($body['sso']), $ssoParams);

    $this->assertEquals($this->getMe()->id(), $ssoParams['external_id'], "Discourse user external id should be drupal user id.");

    $this->assertEquals($this->getMe()->getEmail(), $ssoParams['email'], "Discourse user email should be drupal user email.");
    $this->assertEquals($this->getMe()->getDiscourseUsername(), $ssoParams['username'], "Discourse username should be set.");
  }

  /**
   * @Then the discourse username for :user is :name with id
   */
  public function theDiscourseUsernameIsWithId($user, $name) {
    $user = $this->reloadEntity($this->users[$user]);
    $this->assertSame($name . $user->id(), $user->getDiscourseUsername());
  }

  /**
   * @Then the discourse username for :user is :name
   */
  public function theDiscourseUsernameIs($user, $name) {
    $user = $this->reloadEntity($this->users[$user]);
    $this->assertSame($name, $user->getDiscourseUsername());
  }

  /**
   * @Then the drupal account name for :user is :name with id
   */
  public function theDrupalAccountNameIsWithId($user, $name) {
    $user = $this->reloadEntity($this->users[$user]);
    $this->assertSame($name . $user->id(), $user->getAccountName());
  }

  /**
   * @Then the drupal account name for :user is :name
   */
  public function theDrupalAccountNameIs($user, $name) {
    $user = $this->reloadEntity($this->users[$user]);
    $this->assertSame($name, $user->getAccountName());
  }

  /**
   * @Then :user has no drupal account name
   */
  public function hasNoDrupalAccountName($user) {
    $user = $this->reloadEntity($this->users[$user]);
    $this->assertEmpty($user->getAccountName());
  }

  /**
   * @When :name gets a password
   */
  public function getsAPassword($name) {
    $user = $this->reloadEntity($this->users[$name]);
    $this->coupleUser($user);
  }

  /**
   * @When :name becomes a member
   */
  public function becomesAMember($name) {
    $user = $this->reloadEntity($this->users[$name]);
    $user->addRole('member');
    $user->save();
  }

  /**
   * @Given I am a member
   */
  public function iAmAMember() {
    $me = $this->getMe();
    $me->addRole('member');
    $me->save();
  }


}

