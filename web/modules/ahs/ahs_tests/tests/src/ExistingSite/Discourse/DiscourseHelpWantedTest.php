<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Discourse;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\MockHttpClientTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\user\UserInterface;

/**
 * @group NewDatabaseRequired
 * @group todo
 *
 * @todo remove this test along with functionality as it is not used.
 */
class DiscourseHelpWantedTest extends ExistingSiteBehatBase {

  use UserDefinitions;
  use UserTrait;
  use MockHttpClientTrait;
  use QueueTrait;

  protected static $feature = <<<'FEATURE'
Feature: Discourse help wanted syncing
    In order to show help wanted topics from Sanghaspace
    Discourse topics are synced with Drupal nodes

    Scenario: Help wanted topics are synced from discourse
      Given two help wanted topics
      And an already synced discourse topic node
      And an outdated discourse topic node
      When AHS Discourse cron is run
      Then there are two synced discourse topic nodes


FEATURE;

  protected $topics = [
    [
      'id' => '17',
      'title' => "Help 1",
      'excerpt' => "First para. Second para ",
      'created_at' => "2021-09-23T15:37:44.077Z",
      'last_posted_at' =>  "2022-02-15T15:55:36.867Z",
      'posters' => [
        ['user_id' => '33'],
        ['user_id' => '98'],
      ],
    ],
    [
      'id' => '34',
      'title' => "Help 2",
      'excerpt' => 'First para.' . "\n" . 'Second para.',
      'created_at' => "2021-09-25T16:18:40.877Z",
      'last_posted_at' => "2021-09-25T21:22:42.837Z",
      'posters' => [
        ['user_id' => '112'],
      ],
    ],
  ];

  protected function setUp(): void {
    parent::setUp();
    $this->setUpQueues();

    $fred = $this->createNamedUser('fred', [], TRUE);
    $jack = $this->createNamedUser('jack', [], TRUE);

    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    $nodes = $nodeStorage->loadByProperties(['type' => 'discourse_topic']);
    $nodeStorage->delete($nodes);

    $this->mockHttpClient([
      // First request gets the topics
      ['200', [
        'users' => [
          [
            'id' => $this->topics[0]['posters'][0]['user_id'],
            'username' => $fred->getAccountName(),
          ],
        ],
        'topic_list' => [
          'topics' => $this->topics,
        ],
      ]],
      // Second request gets external id for second topics creator.
      // First topic does not need a request for this because username is supplied in users array.
      ['200', [
        'id' => $this->topics[1]['posters'][0]['user_id'],
        'single_sign_on_record' => [
          'external_id' => $jack->id(),
        ]
      ]],
      // Third response should not be needed by makes debugging easier.
      ['500', []],
    ]);
  }

  /**
   * @Given two help wanted topics
   */
  public function twoHelpWantedTopics() {
    $this->assertCount(2, $this->topics);
  }

  /**
   * @Given an already synced discourse topic node
   */
  public function anAlreadySyncedHelpWantedTopic() {
    $node = Node::create([
      'type' => 'discourse_topic',
      'title' => $this->randomMachineName(),
      'uid' => $this->getUser('fred')->id(),
      'created' => strtotime($this->topics[0]['created_at']),
      'changed' => 334455,
      'excerpt' => $this->randomString(),
    ]);
    $node->save();
    $this->markEntityForCleanup($node);
  }

  /**
   * @Given an outdated discourse topic node
   */
  public function anOutdatedHelpWantedNode() {
    // this does not correspond to anything in $this->>topics.
    $node = Node::create([
      'type' => 'discourse_topic',
      'title' => $this->randomMachineName(),
      'uid' => 1,
      'created' => 455333,
      'changed' => 666334,
      'excerpt' => $this->randomString(),
    ]);
    $node->save();
    $this->markEntityForCleanup($node);
  }

  /**
   * @When AHS Discourse cron is run
   */
  public function ahsDiscourseCronIsRun() {
    // Ignore any queued jobs from user syncing etc
    $this->clearQueues();

    ahs_discourse_cron();

    $requests = $this->getNewHttpRequests();
    $this->assertNotEmpty($requests);
    $this->assertCount(2, $requests, "There should be exactly 2 new discourse requests. " . $this->prettyPrintHttpRequests($requests));
  }

  /**
   * @Then there are two synced discourse topic nodes
   */
  public function thereAreTwoSyncedDiscourseTopicNodes() {
    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => 'discourse_topic']);
    $this->assertCount(2, $nodes);
    $this->assertSyncedTopicNode(array_values($nodes)[0], $this->topics[0], $this->getUser('fred'), "Node should be synced with first topic ");
    $this->assertSyncedTopicNode(array_values($nodes)[1], $this->topics[1], $this->getUser('jack'),"Node should be synced with second topic ");
  }

  protected function assertSyncedTopicNode(NodeInterface $node, array $topic, UserInterface $user, $message) {
    $this->markEntityForCleanup($node);
    $this->assertEquals($topic['title'], $node->getTitle(), $message . "title");
    $this->assertEquals("First para.", $node->get('field_discourse_excerpt')->value, $message . "excerpt");
    $this->assertEquals($topic['id'], $node->get('field_discourse_id')->value, $message . "discourse id");
    $this->assertEquals(strtotime($topic['created_at']), $node->getCreatedTime(), $message . "created time");
    $this->assertEquals(strtotime($topic['last_posted_at']), $node->getChangedTime(), $message . "changed time");
    $this->assertEquals($user->id(), $node->getOwnerId(),$message . "uid");
  }

}

