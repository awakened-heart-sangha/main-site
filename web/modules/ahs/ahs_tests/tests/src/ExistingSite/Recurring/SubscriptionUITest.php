<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Recurring;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\EnrolmentTrait;

/**
 * @group js
 */
class SubscriptionUITest extends RecurringTestBase {

  use BrowsingDefinitions;

  protected $inVisualRegressionHide = [
    "h1.page-title em",
    ".views-field-starts",
    "#edit-starts-0-value",
  ];
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Subscription UI
    In order to allow users to modify subscriptions
    There should be a subscriptions UI.

    Scenario: Can change amount of own membership
      Given I have enrolled
      When I go to 'me/contribution/membership'
      Then I should not see "You are not currently making regular monthly donations"
      And I should see "monthly"
      When I click "Update"
      Then I should see "regular membership donation"
      When I enter "23" as the monthly amount
      And I press "Update"
      Then I should see "Regular membership donations"
      And I should see "23"
      And I should see "Update"
      And my subscription amount is "23"

    # We remove the ability for members to cancel own subscriptions
    #Scenario: Can cancel own membership
    #  Given I have enrolled
    #  When I go to 'me/contribution/membership'
    #  And I click "Update"
    #  Then I should see "regular membership donation"
    #  And I press "Cancel donation"
    #  And I press "Confirm"
    #  Then my subscription is canceled
    #  And I should see "Regular membership donations"
    #  And I should see "You are not currently making regular monthly donations"
    #  And I should not see "Update"



FEATURE;

  /**
   * @When I enter :amount as the monthly amount
   */
  public function iEnterAsTheMonthlyAmount($amount) {
    $page = $this->getCurrentPage();
    $page->fillField('Monthly amount', (string) $amount);
  }

  /**
   * @Then my subscription amount is :amount
   */
  public function mysubscriptionAmountIs(float $amount) {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['uid' => $this->getMe()->id()]);
    $this->assertEquals($amount, $subscription->getUnitPrice()->getNumber());
  }

  /**
   * @Then my subscription is canceled
   */
  public function mySubscriptionIsCanceled() {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['uid' => $this->getMe()->id()]);
    $this->assertEquals('canceled', $subscription->getState()->getId());
  }

}

