<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Recurring;

use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\Tests\ahs_tests\Traits\ContentTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\EnrolmentTrait;
use Drupal\Tests\ahs_tests\Traits\EntityTrait;
use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\Tests\ahs_tests\Traits\WebformTrait;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\Traits\Core\CronRunTrait;

class RecurringTestBase extends ExistingSiteJsBehatBase {

  use UserDefinitions;
  use CommerceDefinitions;
  use EntityTrait;
  use CommerceTrait;
  use MailCollectionTrait;
  use WebformTrait;
  use FormTrait;
  use ContentTrait;
  use GroupTrait;
  use CronRunTrait;
  use QueueTrait;
  use EnrolmentTrait;

  protected $amount;

  protected $orderId;

  protected $enrolmentFormUrl;

  protected $webFormSettings = [];

  protected $currency = 'GBP';

  protected $group;

  protected $immediateMailCount;

  protected $subscriptionFirstRecurringOrderId;

  protected function setUp(): void {
    parent::setUp();
    $this->setUpQueues();
    $this->startMailCollection();
    $this->group = $this->ensureDHBGroups();
    $this->setUpWebform();
  }

  public function tearDown(): void {
    if ($user = $this->loadEntityByProperties('user', ['mail' =>$this->getMyEmail()])) {
      $this->markEntityForCleanup($user);
    }

    // Restore original webform settings.
    $this->tearDownWebform();
    $this->restoreMailSettings();
    $this->tearDownQueues();
    parent::tearDown();
  }

  protected function tearDownWebform() {
    $this->tearDownWebformSettings();
  }

  /**
   * @When I give :value as the amount
   */
  public function iGiveAsTheAmount($value) {
    $this->amount = $value;
    $page = $this->getSession()->getPage();
    $amount_field = $page->find('css', "#edit-monthly-donation-other");
    $amount_field->setValue($value);
  }

  /**
   * @Then a monthly membership donation should be setup for me
   * @Then a monthly membership donation for :amount per month should be setup for me
   */
  public function aMonthlyMembershipDonationShouldBeSetupForMe($amount = NULL) {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $amount = is_null($amount) ? $this->amount : $amount;
    $sku = 'ahs_membership_monthly';
    $scheduleId = 'monthly';
    $this->assertSubscriptionCreatedFromInitialOrder($order, $amount, $this->currency, $sku, $scheduleId);
  }

//  /**
//   * @When a month passes and payment fails
//   * @When a month passes and payment fails after :numRetries retries.
//   */
//  public function aMonthPassesAndPaymentFails($numRetries = 1) {
//    $this->startTime = \Drupal::service('datetime.time')->getCurrentTime();
//    \Drupal::service('datetime.time')->setTime("+35 days");
//
//    // Close the recurring order manually, rather than via cron & queue,
//    // because the payment method is not set up on Stripe and will fail.
//
//    $order = $this->loadEntity('commerce_order', $this->orderId);
//    $recurringOrder = $this->getRecurringOrderForSubscriptionOrder($order);
//    $this->markEntityForCleanup($recurringOrder);
//
//    $this->processAdvancedQueue('commerce_recurring');
//
//    // Here we should trigger the cron so the payment is attempted, fails and
//    // the dunning mail is sent, but I'm getting failures.
//    $payment = $this->createPayment($recurringOrder);
//    $payment->setState('failed');
//    $payment->save();
//    $recurringOrder->set('state', 'needs_payment')->save();
//    $recurringOrder = $this->reloadEntity($recurringOrder);
//    $this->assertSame('needs_payment', $recurringOrder->getState()->getId(), "The order should be in a needs_payment state.");
//  }

  /**
   * @When the card expires
   */
  public function theCardExpires() {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $subscription = $this->getSubscriptionFromInitialOrder($order);
    $paymentMethod = $subscription->getPaymentMethod();
    $paymentMethod->setRemoteId('pm_card_chargeDeclinedExpiredCard');
    $paymentMethod->setExpiresTime(0);
    $paymentMethod->save();
    \Drupal::entityTypeManager()->getStorage('commerce_payment_method')->resetCache();
    \Drupal::entityTypeManager()->getStorage('commerce_subscription')->resetCache();
  }

  /**
   * @When a month passes
   * @When another month passes
   */
  public function aMonthPasses() {
    $this->immediateMailCount = count($this->getMail(['to' => $this->getMyEmail()]));
    $this->aMonthPassesAfterEnrolment();
  }

  /**
   * @When I retry the payment as anonymous
   */
  public function iRetryThePaymentAsAnonymous() {
    $recurringOrder = $this->loadEntity('commerce_order', $this->subscriptionFirstRecurringOrderId);

    $this->iAmNotLoggedIn();
    $this->visit('/orders/' . $recurringOrder->id() . '/payment-retry');
    $this->iShouldBeOnThePaymentInformationStepOfTheRetryCheckout();
  }

  protected function getRecurringOrderForSubscriptionOrder(OrderInterface $order) {
    $subscription = $this->getSubscriptionFromInitialOrder($order);
    return $subscription->getOrders()[0];
  }

  protected function getSubscriptionFromInitialOrder(OrderInterface $order) {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['initial_order' => $order->id()]);
    $this->assertInstanceOf(SubscriptionInterface::class, $subscription, "There should be a subscription which has " . $order->id() . " as its initial order");
    return $subscription;
  }

  protected function iShouldBeOnThePaymentInformationStepOfTheRetryCheckout() {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $recurringOrder = $this->getRecurringOrderForSubscriptionOrder($order);

    $this->assertSession()->pageTextContains('Order information');
    $this->assertSame('needs_payment', $recurringOrder->getState()->getId(),
      "The order should be in a needs_payment state.");
  }

}

