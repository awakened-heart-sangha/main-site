<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Recurring;

use Drupal\commerce_price\Price;

/**
 * @group js
 */
class ModifySubscriptionAmountTest extends RecurringTestBase {

  protected $countRecurringPayments = 1;

  protected $inVisualRegressionHide = [".field--name-field-dates"];

  protected $subscriptionInitialOrderId;
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Modify subscription amount
    In order to allow increasing or decreasing membership contributions,
    Subscriptions payment must respect changes to subscription amounts.

    Scenario: Recurring payments process correctly when subscription amounts are changed
      Given I have enrolled giving 30 per month
      Then a first donation should be received from me
      Then a monthly membership donation should be setup for me
      When a month passes
      Then I have made a first recurring donation of 30
      Then the subscription amount is changed to 45
      When another month passes
      Then I have made a second recurring donation of 45
      When another month passes
      Then I have made a third recurring donation of 45

FEATURE;

  /**
   * @Then the subscription amount is changed to :amount
   */
  public function theSubscriptionAmountIsChangedTo($amount) {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['initial_order' => $this->orderId]);
    $price = $subscription->getUnitPrice();
    $subscription->setUnitPrice(new Price($amount, $price->getCurrencyCode()));
    $subscription->save();
  }

  /**
   * @Then I have made a first recurring donation of :amount
   */
  public function iHaveMadeAFirstDonationOf($amount) {
    $recurringOrder = $this->loadEntity('commerce_order', $this->subscriptionInitialOrderId);
    $this->assertRecurringOrderCompleted($recurringOrder, $amount, 0);
  }

  /**
   * @Then I have made a second recurring donation of :amount
   */
  public function iHaveMadeASecondDonationOf($amount) {
    $recurringOrder = $this->loadEntity('commerce_order', $this->subscriptionInitialOrderId);
    $this->assertRecurringOrderCompleted($recurringOrder, $amount, 1);
  }

  /**
   * @Then I have made a third recurring donation of :amount
   */
  public function iHaveMadeAThirdDonationOf($amount) {
    $recurringOrder = $this->loadEntity('commerce_order', $this->subscriptionInitialOrderId);
    $this->assertRecurringOrderCompleted($recurringOrder, $amount, 2);
  }

}

