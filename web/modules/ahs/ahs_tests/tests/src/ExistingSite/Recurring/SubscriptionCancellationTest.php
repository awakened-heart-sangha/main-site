<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Recurring;

use Drupal\Tests\ahs_tests\Traits\EnrolmentTrait;

/**
 * @group js
 */
class SubscriptionCancellationTest extends RecurringTestBase {

  protected $inVisualRegressionHide = [".field--name-field-dates"];
  protected $storeId;
  protected $subscriptionInitialOrderId;

  protected static $feature = <<<'FEATURE'
Feature: Subscription cancellation
    In order not take payments for cancelled memberships
    Placed recurring orders should be cancelled when subscriptions are.

    Scenario: Can retry payment of a failed subscription charge.
      Given I have enrolled
      Then a first donation should be received from me
      Then a monthly membership donation should be setup for me
      When the card expires
      And a month passes
      And the subscription is cancelled
      Then the recurring orders are cancelled too

FEATURE;

  /**
   * @When the subscription is cancelled
   */
  public function theSubscriptionIsCancelled() {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['initial_order' => $order->id()]);
    $subscription->cancel(FALSE);
    $subscription->save();
  }

  /**
   * @Then the recurring orders are cancelled too
   */
  public function theOrderIsCancelledToo() {
    $initalOrder = $this->loadEntity('commerce_order', $this->orderId);
    $subscription = $this->getSubscriptionFromInitialOrder($initalOrder);
    $orders = $subscription->getOrders();
    foreach ($orders as $order) {
      $this->assertContains($order->getState()->getId(), ['canceled', 'completed', 'failed'], "Outstanding subscriptiom orders should have been canceled.");
    }
  }


}

