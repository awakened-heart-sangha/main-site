<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Recurring;

use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\EnrolmentTrait;

/**
 * @group js
 */
class MonthlyMembershipDonationTest extends RecurringTestBase {

  use BrowsingDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Setting up a monthly membership donation
    In order to allow users to restart membership or switch from bank/paypal
    There should be a form for setting up a monthly membership donation.

    Scenario: Can set up monthly membership donation
      Given I am logged in
      When I go to 'monthly-donations'
      And I enter "30" as the monthly donation
      And I press "Setup monthly donation"
      Then I should be on the payment information step of the checkout
      When I submit my credit card details
      Then I should be on the Thankyou step of the checkout
      And my subscription amount is "30"
      And my subscription is active

FEATURE;

  /**
   * @When I enter :amount as the monthly donation
   */
  public function iEnterAsTheMonthlyDonation($amount) {
    $selector = $this->getCurrentPage()->find('named', [
      'field',
      'monthly_donation[select]'
    ]);
    $selector->selectOption($amount);
  }

  /**
   * @Then my subscription amount is :amount
   */
  public function mySubscriptionAmountIs(float $amount) {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['uid' => $this->getMe()->id()]);
    $this->assertEquals($amount, $subscription->getUnitPrice()->getNumber());
  }

  /**
   * @Then my subscription is active
   */
  public function mySubscriptionIsActive() {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['uid' => $this->getMe()->id()]);
    $this->assertEquals('active', $subscription->getState()->getId());
  }

}

