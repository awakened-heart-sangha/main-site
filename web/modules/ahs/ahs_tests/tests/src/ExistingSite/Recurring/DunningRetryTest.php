<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Recurring;

/**
 * @group js
 */
class DunningRetryTest extends RecurringTestBase {

  protected $failOnLogsLevel = 0;

  protected $inVisualRegressionHide = [".field--name-field-dates"];

  protected $subscriptionFirstRecurringOrderId;
  protected $storeId;
  protected $subscriptionInitialOrderId;
  protected $mails;

  protected static $feature = <<<'FEATURE'
Feature: Dunning retry checkout
    In order for continuing enrolled in the program,
    students can pay if their payment charge fails.

    Scenario: Can retry payment of a failed subscription charge
      Given I have enrolled
      Then a first donation should be received from me
      Then a monthly membership donation should be setup for me
      When the card expires
      And the month passes
      Then the recurring payment attempt fails gracefully
      Then I should get a dunning email
      When I retry the payment as anonymous
      And I pay with the same payment method
      Then the donation has been paid
      And I see a "Thankyou for your contribution" message

FEATURE;

  /**
   * @Then the donation has been paid
   */
  public function theDonationHasBeenPaid() {
    $recurringOrder = $this->loadEntity('commerce_order', $this->subscriptionFirstRecurringOrderId);
    $this->assertSame('completed', $recurringOrder->getState()->getId(), "The order should be in a completed state.");
  }

  /**
   * @Then I see a :text message
   */
  public function iSeeAMessage($text) {
    $this->assertSession()->pageTextContains($text);
  }

  /**
   * @When the month passes
   */
  public function aMonthPasses() {
    $mails = $this->getMail(['to' => $this->getMyEmail()]);

    $this->mails = [];
    // Store unique mails for the user.
    foreach ($mails as $mail) {
      $this->mails[$mail['timestamp']] = $mail;
    }

    $this->immediateMailCount = count($mails);
    $this->aMonthPassesAfterEnrolment();
  }

  /**
   * @Then the recurring payment attempt fails gracefully
   */
  public function theRecurringPaymentAttemptFailsGracefully() {
    $recurringOrder = $this->loadEntity('commerce_order', $this->subscriptionFirstRecurringOrderId);
    $this->assertSame('needs_payment', $recurringOrder->getState()->getId(), "The order should still be needing payment.");
    $this->assertNoOrderPayment($recurringOrder);

    $order = $this->loadEntity('commerce_order', $this->orderId);
    $subscription = $this->getSubscriptionFromInitialOrder($order);
    $this->assertCount(2, $subscription->getOrders(), "The subscription should now have 2 recurring orders");
  }

  /**
   * @Then I should get a dunning email
   */
  public function iShouldGetADunningEmail() {
    $mails = $this->getMail(['to' => $this->getMyEmail()]);
    foreach ($mails as $mail) {
      $this->mails[$mail['timestamp']] = $mail;
    }

    $this->assertMailCount($this->mails, $this->immediateMailCount + 1);
    $dunningMail = end($this->mails);
    $properties = [
      'subject' => 'Your latest membership donation',
    ];
    $this->assertTrue($this->matchesMail($dunningMail, $properties), sprintf("'Mail should have the right subject':\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($mails, 2000), print_r($properties, TRUE)));
  }

  /**
   * @When I pay with the same payment method
   */
  public function iPayWithTheSamePaymentMethod() {
    $this->submitCreditCardDetails('Continue to review');
    $this->assertSession()->waitForText('Complete');
    $this->assertSession()->pageTextContains('Complete');
  }

}

