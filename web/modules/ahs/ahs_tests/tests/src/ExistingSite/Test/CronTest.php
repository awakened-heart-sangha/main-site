<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Test;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\Tests\ahs_tests\Traits\CronTrait;

class CronTest extends ExistingSiteBehatBase {

  use QueueTrait;
  use CronTrait;

  protected static $feature = <<<'FEATURE'
Feature: Cron error logs testing
  In order to make debugging test fails easier
  As a developer
  I need cron to not generate log errors that cause test failures

  Scenario: Process queues
    Then queues are processed

  Scenario: Run cron
    Then cron runs

FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->clearQueues();
  }

  public function tearDown(): void {
    // Remove cron lock in case cron failed.
    \Drupal::lock()->release('cron');
    $this->clearQueues();
    parent::tearDown();
  }

  /**
   * @Then queues are processed
   */
  public function queuesAreProcessed() {
    $this->processQueues();
  }

  /**
   * @Then cron runs
   */
  public function cronRuns() {
    $this->runCronJobs();
  }

}

