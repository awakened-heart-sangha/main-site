<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Test;

use Drupal\Tests\ahs_tests\Traits\Definitions\MailDefinitions;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;

class MailTest extends ExistingSiteBehatBase {

  use MailDefinitions;
  use MailCollectionTrait;

  protected static $feature = <<<'FEATURE'
Feature: Mail testing
  In order to prove the mail collection & testing is working properly
  As a developer
  I need to use the step definitions for mail testing

  Scenario: Mail is sent
    When Drupal sends an email:
      | to      | fred@example.com |
      | subject | test             |
      | body    | test body        |
    And Drupal sends a mail:
      | to      | jane@example.com |
      | subject | test             |
      | body    | test body 2      |
    Then only these mails have been sent:
      | to   | subject | body        |
      | fred |         | test body   |
      | jane | test    | body 2      |
    When Drupal sends a mail:
      | to      | jack@example.com          |
      | subject | for jack                  |
      | body    | test body with many words |
    Then an email has been sent:
      | to   | body | body       |
      | jack | test | many words |
    And a mail has been sent to "jane@example.com"
    And a mail has been sent to "jane@example.com":
      | subject |
      | test    |
    And an email has been sent with the subject "test"
    And emails have been sent with the subject "test":
      | to   |
      | fred |
      | jane |
    And a mail has been sent to "fred" with the subject "test"
    And emails have been sent to "fred" with the subject "test":
      | body      |
      | test body |

  Scenario: No mail is sent
    Then no mail has been sent

  Scenario: Count sent mail
    When Drupal sends an email:
      | to      | fred@example.com |
      | subject | test             |
    And Drupal sends a mail:
      | to      | jane@example.com |
      | subject | test             |
    And Drupal sends a mail:
      | to      | jane@example.com |
      | subject | something else   |
    Then 2 emails have been sent with the subject "test"
    And 1 mail has been sent to "jane" with the subject "something else"
    And no mail has been sent to "hans"

  Scenario: I follow link in mail
    When Drupal sends a mail:
      | to      | fred@example.com                        |
      | subject | test link                               |
      | body    | A link to Google: https://www.Google.com |
    And I follow the link to "google" from the mail with the subject "test link"
    Then I should see "Google"

  Scenario: We try to be order insensitive
    When Drupal sends an email:
      | to      | fred@example.com |
      | subject | test             |
      | body    | test body        |
    And Drupal sends a mail:
      | to      | jane@example.com |
      | subject | test             |
      | body    | test body 2      |
    Then mails have been sent:
      | to   | subject | body        |
      | jane | test    | body 2      |
      | fred |         | test body   |
FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->startMailCollection();
  }

  public function tearDown(): void {
    $this->restoreMailSettings();
    parent::tearDown();
  }

  /**
   * @Then I should see :text
   */
  public function iShouldSee($text) {
    $this->assertSession()->titleEquals($text);
  }

  public function testMatchesMail() {
    $email = [
      'to' => 'joe@ahs.org.uk',
      'subject' => 'some good subject line',
      'body' => 'some good body text'
    ];

    $criteria = ['subject' => 'some good subject line'];
    $this->assertTrue($this->matchesMail($email, $criteria));

    $criteria = ['subject' => 'good subject'];
    $this->assertTrue($this->matchesMail($email, $criteria));

    $criteria = ['subject' => 'bad subject'];
    $this->assertFalse($this->matchesMail($email, $criteria));

    $criteria = [];
    $this->assertTrue($this->matchesMail($email, $criteria));

    $criteria = ['subject' => '', 'to' => ''];
    $this->assertTrue($this->matchesMail($email, $criteria));

    $criteria = ['body' => 'good body'];
    $this->assertTrue($this->matchesMail($email, $criteria));

    $criteria = ['subject' => 'good subject', 'body' => 'good body'];
    $this->assertTrue($this->matchesMail($email, $criteria));

    $criteria = ['subject' => 'good subject', 'body' => 'bad body'];
    $this->assertFalse($this->matchesMail($email, $criteria));

  }

  public function testFilterMail() {
    $emails = [
      [
        'to' => 'joeA@ahs.org.uk',
        'subject' => 'some good subject line A',
        'body' => 'some good body text A',
      ],
      [
        'to' => 'joeB@ahs.org.uk',
        'subject' => 'some good subject line B',
        'body' => 'some good body text B',
      ],
    ];

    $criteria = ['subject' => 'good subject'];
    $this->assertCount(2, $this->FilterMail($emails, $criteria));

    $criteria = ['subject' => 'good subject line B'];
    $this->assertCount(1, $this->FilterMail($emails, $criteria));

    $criteria = ['subject' => 'bad subject'];
    $this->assertCount(0, $this->FilterMail($emails, $criteria));

    $criteria = ['subject' => '', 'to' => ''];
    $this->assertCount(2, $this->FilterMail($emails, $criteria));

  }

}

