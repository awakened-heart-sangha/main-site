<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class NewsGroupRegistrationSubscriptionsTest extends ExistingSiteBehatBase {

  use TrainingTrait;
  use NewsTrait;
  use NewsDefinitions;
  use MailCollectionTrait;

  protected $event;
  protected $course;

  protected static $feature = <<<'FEATURE'
Feature: Users who join groups are automatically subscribed to news
    In order to send information to engaged students
    Users who join group are automatically subscribed

    Scenario: Joining events subscribes to news
      Given an event group
      When I join the event
      Then I am subscribed to the "important" newsletter
      And I am subscribed to the "retreats" newsletter
      And I am not subscribed to the "online_courses" newsletter

    Scenario: Joining courses subscribes to news
      Given a course group
      When I join the course
      Then I am subscribed to the "important" newsletter
      And I am subscribed to the "retreats" newsletter
      And I am subscribed to the "online_courses" newsletter

    Scenario: Joining groups respects existing subscriptions
      Given a course group
      And I have subscribed to the "important" newsletter
      And I have unsubscribed from the "retreats" newsletter
      When I join the course
      Then I am subscribed to the "important" newsletter
      And I am not subscribed to the "retreats" newsletter
      And I am subscribed to the "online_courses" newsletter

FEATURE;

  /**
   * @Given I have unsubscribed from the :newsletter_id newsletter
   */
  public function userIsUnSubscribedFromTheNewsletter($newsletter_id, $name = 'me') {
    $user = $this->getOrCreateNamedUser($name);
    $this->subscribeUser($user, $newsletter_id);
    $subscriber = $this->assertUserSubscriber($user);
    $subscriber->unsubscribe($newsletter_id);
    $subscriber->save();
    $this->assertSubscribed($user, $newsletter_id, FALSE);
  }

}
