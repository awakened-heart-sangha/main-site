<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

/**
 * @group NewDatabaseRequired
 */
class NewsMassSendingTest extends ExistingSiteBehatBase {

  use GroupTrait;
  use NewsTrait;
  use NewsDefinitions;
  use MailCollectionTrait;
  use UserTrait;

  protected $courses;
  protected $otherNews;
  protected $issue;
  protected $ctaAbsoluteUrl;

  protected static $feature = <<<'FEATURE'
Feature: Email news mass sending
    In order to send email to lists
    Many entities have to be rendered in one cron request

    # This tests a core bug:
    # Can only intentionally re-render an entity with references 20 times
    # https://www.drupal.org/project/drupal/issues/2940605
    Scenario: Send news to many people
      Given a leader
      And 10 suggested courses with that leader
      And 10 other news items by that leader
      And a news issue for the important newsletter
      And many people are subscribed to the important newsletter
      When the news issue is sent by cron
      Then an email is sent to each subscriber
      And the emails contain the suggested courses
      And the emails contain the other news items

FEATURE;

  /**
   * @Given a leader
   */
  public function aLeader() {
    $this->createNamedUser('leader');
  }

  /**
   * @Given 10 suggested courses with that leader
   */
  public function suggestedCoursesWithThatLeader() {
    $leader = $this->getUser('leader');

    $image = File::create([
      'uri' => 'public://' . $this->randomMachineName(),
      'uid' => $leader->id(),
    ]);
    $image->setPermanent();
    $image->save();
    $this->markEntityForCleanup($image);

    $media = Media::create([
      'bundle' => 'image',
      'name' => $this->randomMachineName(),
      'field_media_image' => [
        [
          'target_id' => $image->id(),
          'alt' => $this->randomMachineName(),
          'title' => $this->randomMachineName(),
        ],
      ],
    ]);
    $media->save();
    $this->markEntityForCleanup($media);

    for ($i = 0; $i < 10; $i++) {
      $this->courses[] = $this->createGroup([
        'type' => 'course',
        'field_leader' => $leader,
        'field_hero_image' => $media,
        'field_suggested' => TRUE,
      ], TRUE);
    }
  }

  /**
   * @Given 10 other news items by that leader
   */
  public function otherNewsItemsByThatLeader() {
    $leader = $this->getUser('leader');
    for ($i = 0; $i < 10; $i++) {
      $node = Node::create([
        'type' => 'news',
        'title' => $this->randomMachineName(),
        'uid' => $leader->id(),
      ]);
      $node->setPublished();
      $node->save();
      $this->markEntityForCleanup($node);
      $this->otherNews[] = $node;
    }
  }

  /**
   * @Given many people are subscribed to the important newsletter
   */
  public function peopleAreSubscribedToTheNewsletter() {
    $config = \Drupal::config('simplenews.settings');
    $count = $config->get('mail.throttle');
    for ($i = 0; $i < $count; $i++) {
      $user = $this->createNamedUser("recipient" . $i);
      $this->subscribeUser($user, 'important');
    }
  }

  /**
   * @When the news issue is sent by cron
   */
  public function theNewsIssueIsSent() {
    \Drupal::service('simplenews.spool_storage')->addIssue($this->issue);
    simplenews_cron();
  }

  /**
   * @Then an email is sent to each subscriber
   */
  public function mailIsSentToEachSubscriber() {
    $config = \Drupal::config('simplenews.settings');
    $count = $config->get('mail.throttle');
    $mails = $this->getMails();
    $this->assertMailCount($mails, $count);
    for ($i = 0; $i < $count; $i++) {
      $user = $this->getUser("recipient" . $i);
      $mails = $this->getUserMails($user);
      $this->assertMailCount($mails, 1);
    }
  }

  protected function assertEachMailEachContent(array $content, $typeMessage) {
      $properties = ['body' => []];
      foreach($content as $item) {
        $properties['body'][] = $item->label();
      }
      $mails = $this->getMails();
      foreach ($mails as $i => $mail) {
        $message = sprintf("Mail $i should contain $typeMessage:\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails([$mail], 2000), $this->prettyMail($properties, 300));
        $this->assertMatchesMail($mails, $properties, $i, $message);
      }
  }

  /**
   * @Then the emails contain the suggested courses
   */
  public function theEmailsContainTheSuggestedCourses() {
    $this->assertEachMailEachContent($this->courses, 'courses');
  }

  /**
   * @Then the emails contain the other news items
   */
  public function theEmailsContainTheOtherNewsItems() {
    $this->assertEachMailEachContent($this->otherNews, 'other news');
  }

}
