<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;

class UserNewslettersTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use NewsTrait;
  use NewsDefinitions;
  use SubscriptionsPageTrait;
  use UserDefinitions;

  protected $student;
  protected $newEmail;

  protected static $feature = <<<'FEATURE'
Feature: User newsletter preferences can be changed
    In order to control which newsletters they get
    There should be a newsletters page for users

    Scenario: User can change own email on newsletters page
      Given I am logged in
      And I view my newsletters page
      And I enter a new email
      And I press "Update"
      Then my email has been changed
      # Try again as now there is a subscriber entity already existing, which may matter.
      When I view my newsletters page
      And I enter a new email
      And I press "Update"
      Then my email has been changed

    Scenario: User can subscribe without changing own email
      Given I am logged in
      Then I am not subscribed to the collaboration newsletter
      When I view my newsletters page
      And I check "Collaboration"
      And I press "Update"
      Then I am subscribed to the collaboration newsletter
      When I view my newsletters page
      And I uncheck "Collaboration"
      And I press "Update"
      Then I am not subscribed to the collaboration newsletter

    Scenario: User can change email and subscribe on newsletters page
      Given I am logged in
      Then I am not subscribed to the collaboration newsletter
      When I view my newsletters page
      And I enter a new email
      And I check "Collaboration"
      And I press "Update"
      Then my email has been changed
      And I am subscribed to the collaboration newsletter
      # Try again as now there is a subscriber entity already existing, which may matter.
      When I view my newsletters page
      And I enter a new email
      And I uncheck "Collaboration"
      And I press "Update"
      Then my email has been changed
      And I am not subscribed to the collaboration newsletter
      When I view my newsletters page
      And I check "Collaboration"
      And I press "Update"
      Then I am subscribed to the collaboration newsletter

    Scenario: Member can unsubscribe from retreats
      Given I have the 'member' role
      Then I am subscribed to the retreats newsletter
      Given I am logged in
      When I view my newsletters page
      And I uncheck "Retreats"
      And I press "Update"
      Then I am not subscribed to the retreats newsletter

    Scenario: Cannot change email to another user's on newsletters page
      Given there is another user
      And I am logged in
      When I view my newsletters page
      And I enter the other user's email
      And I press "Update"
      Then I should see "A user already exists with the email"

    Scenario: Unsubscribe from all on user newsletters page
      Given I am logged in
      And I have subscribed to the retreats newsletter
      And I have subscribed to the collaboration newsletter
      When I view my newsletters page
      And I press "Unsubscribe from all"
      Then I am not subscribed to the retreats newsletter
      And I am not subscribed to the collaboration newsletter

    Scenario: Staff can change user's email and subscribe on newsletters page
      Given I am logged in as a student_support
      When I view a student's newsletters page
      And I enter a new email
      And I check "Collaboration"
      And I press "Update"
      Then the student's email has been changed
      And the student is subscribed to the collaboration newsletter
      # Try again as now there is a subscriber entity already existing, which may matter.
      When I view a student's newsletters page
      And I enter a new email
      And I uncheck "Collaboration"
      And I press "Update"
      Then the student's email has been changed
      And the student is not subscribed to the collaboration newsletter
      When I view a student's newsletters page
      And I check "Collaboration"
      And I press "Update"
      Then the student is subscribed to the collaboration newsletter


FEATURE;

  /**
   * @When I view my newsletters page
   */
  public function iViewNewslettersPage() {
    $this->drupalGet("/user/" . $this->getMe()->id() . "/email-news");
    $this->assertSession()->pageTextContains($this->getMe()->getDisplayName());
  }

  /**
   * @When I view a student's newsletters page
   */
  public function iViewAStudentsNewslettersPage() {
    if (empty($this->student)) {
     $this->student = $this->createDecoupledUser();
    }
    $student = $this->reloadEntity($this->student);
    $this->drupalGet("/user/" . $student->id() . "/email-news");
    $this->assertSession()->pageTextNotContains("Access denied");
    $this->debugPage();
  }

  /**
   * @Then the student's email has been changed
   */
  public function theStudentsEmailHasBeenChanged() {
    $student = $this->reloadEntity($this->student);
    $this->assertSame($this->newEmail, $student->getEmail(), "The student's user email should be updated");
    $this->assertSame($this->newEmail, $this->getUserSubscriberEmail($student), "The student's subscriber email should be updated");
  }

  /**
   * @Then the student is subscribed to the :newsletter_id newsletter
   */
  public function theStudentIsSubscribedToTheNewsletter($newsletter_id) {
    $this->assertSubscribed($this->student, $newsletter_id, TRUE);
  }

  /**
   * @Then the student is not subscribed to the :newsletter_id newsletter
   */
  public function theStudentIsNotSubscribedToTheNewsletter($newsletter_id) {
    $this->assertSubscribed($this->student, $newsletter_id, FALSE);
  }


}

