<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;

/**
 * @group NewDatabaseRequired
 */
class NewsTemplatesTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use NewsTrait;
  use NewsDefinitions;
  use UserDefinitions;
  use MailCollectionTrait;

  protected $issue;
  protected $ctaAbsoluteUrl;
  protected $mails;
  protected $topic;
  protected $mailtoEmail;
  protected $otherNews;
  protected $subscriber;

  protected static $feature = <<<'FEATURE'
Feature: Email news templates render news content
    In order to theme new emails
    We have a separate ahs_news theme with complex templates

    Background:
      Given I am logged in as a mailer
      And a draft news issue

    Scenario: Templated news email fields
      When the news issue is sent
      Then the html email has the news fields
      And the plain email has the news fields

    Scenario: Email links in news emails
      Given the body has a mailto link
      When the news issue is sent
      Then the email has the mailto link

    Scenario: Help wanted not shown to non-members
      Given there is a synced help wanted topic
      When the news issue is sent to a non-member
      Then the html email does not contain "Help wanted"

    Scenario: Help wanted shown to members
      Given there is a synced help wanted topic
      When the news issue is sent to a member
      Then the html email contains the help wanted topic

    Scenario: No help wanted
      Given there are no synced help wanted topics
      When the news issue is sent to a non-member
      Then the html email does not contain "Help wanted"

    Scenario: Other news
      Given there is an item of other news
      When the news issue is sent
      Then the html email contains the other news

    Scenario: No other news
      Given there are no items of other news
      When the news issue is sent
      Then the html email does not contain "Other news"

    Scenario: Other news excludes current
      Given there is an item of other news

FEATURE;

  /**
   * @Then I should see the news fields
   */
  public function iShouldSeeTheNewsFields() {
    $this->assertSession()->pageTextContains($this->issue->getTitle());
    $this->assertSession()->pageTextContains($this->issue->get('field_subtitle')->value);
    $this->assertSession()->pageTextContains($this->issue->get('body')->value);
    $this->assertSession()->pageTextContains($this->issue->get('field_link')->title);
    $this->assertSession()->linkByHrefExists($this->ctaAbsoluteUrl, 0, $this->issue->get('field_link')->value);
    $this->assertSession()->pageTextContains($this->issue->get('field_authors')->entity->getDisplayName());
  }

  /**
   * @Then the issue is not in the other news
   */
  public function theSentIssueShouldNotBeInOtherNews() {
    $this->assertSession()->elementTextNotContains('css', ".view-news", $this->issue->getTitle());
  }

  /**
   * @When the news issue is sent
   * @When the news issue is sent to a non-member
   */
  public function theNewsIssueIsSent() {
    $this->issue->setPublished();
    $this->issue->save();
    $this->sendIssueToUser($this->issue);
    $this->mails = $this->getMail();
    $this->assertMailCount($this->mails, 1);
  }

  /**
   * @When the news issue is sent to a member
   */
  public function theNewsIssueIsSentToAMember() {
    $user = $this->createNamedUser('member');
    $user->addRole('member');
    $user->save();
    $this->issue->setPublished();
    $this->issue->save();
    $this->sendIssueToUser($this->issue, $user);
    $this->mails = $this->getMail();
    $this->assertMailCount($this->mails, 1);
  }

  /**
   * @Then the html email has the news fields
   */
  public function theHtmlEmailHasTheNewsFields() {
    $properties = [
      'subject' => $this->issue->getTitle(),
      'body' => [
          $this->issue->getTitle(),
          $this->issue->get('field_subtitle')->value,
          $this->issue->get('body')->value,
          $this->issue->get('field_link')->value,
          $this->ctaAbsoluteUrl,
          $this->issue->get('field_authors')->entity->getDisplayName()
        ],
    ];
    $this->assertInlineStylesPresent($this->mails[0]['body']);
    $this->assertMatchesMail($this->mails, $properties);
  }

  /**
   * @Then the plain email has the news fields
   */
  public function thePlainEmailHasTheNewsFields() {
    $properties = [
      'plain' => [
        $this->issue->getTitle(),
        $this->issue->get('field_subtitle')->value,
        $this->issue->get('body')->value,
        $this->issue->get('field_link')->value,
        $this->ctaAbsoluteUrl,
        $this->issue->get('field_authors')->entity->getDisplayName()
      ],
    ];
    $this->assertMatchesMail($this->mails, $properties);
  }

  /**
   * @Given there are no synced help wanted topics
   */
  public function thereAreNoSyncedHelpWantedTopics() {
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $nodes = $storage->loadByProperties(['type' => 'discourse_topic']);
    $storage->delete($nodes);
    $this->topic = NULL;
  }


  /**
   * @Then the html email contains the help wanted topic
   */
  public function theHtmlEmailHasTheHelpWantedTopic() {
    $properties = [
      'subject' => $this->issue->getTitle(),
      'body' => [
        'Help Wanted',
        $this->topic->getTitle(),
        $this->topic->get('field_discourse_excerpt')->value
      ],
    ];
    $this->assertMatchesMail($this->mails, $properties);
  }

  /**
   * @Then the html email does not contain :value
   */
  public function theHtmlEmailDoesNotContain($value) {
    $properties = [
      'body' => [
        $value,
      ],
    ];
    $this->assertNotMatchesMail($this->mails, $properties);
  }

  /**
   * @Given there are no items of other news
   */
  public function thereAreNoItemsOfOtherNews() {
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $nodes = $storage->loadByProperties(['type' => 'news']);
    try {
      unset($nodes[$this->issue->id()]);
      $storage->delete($nodes);
    }
    catch (\Exception $e) {}
    $this->topic = NULL;
  }

  /**
   * @Then the html email contains the other news
   */
  public function theHtmlEmailContainsTheOtherNews() {
    $properties = [
      'body' => [
        'Other news',
      ],
    ];
    $path = $GLOBALS['base_url'] . '/node/';
    foreach($this->otherNews as $item) {
      $properties['body'][] = $item->getTitle();
      $properties['body'][] = $item->get('field_subtitle')->value;
      $properties['body'][] = $path . $item->id();
    }
    $this->assertMatchesMail($this->mails, $properties);
  }

  /**
   * @Given the body has a mailto link
   */
  public function theBodyHasAMailtoLink() {
    $body = $this->issue->get('body')->value;
    $this->mailtoEmail = 'events@ahs.org.uk';
    $mailtoLink = '<a href="mailto:' . $this->mailtoEmail . '">the events team at ' . $this->mailtoEmail . '</a>';
    $this->issue->set('body', $body . ' ' . $mailtoLink . $this->randomMachineName());
    $this->issue->save();
  }

  /**
   * @Then the email has the mailto link
   */
  public function theHtmlEmailHasTheMailToLink() {
    $properties = [
      'body' => [
        'href="mailto:' . $this->mailtoEmail . '"',
        $this->mailtoEmail . '</a>'
      ],
    ];
    $message = sprintf("The html format should have intact mailto links:\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($this->mails, 2000), $this->prettyMail($properties, 300));
    $this->assertMatchesMail($this->mails, $properties, 0,$message);
    $properties = [
      'plain' => [
        $this->mailtoEmail,
      ],
    ];
    $message = sprintf("The plain format should have intact mailto emails:\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($this->mails, 2000), $this->prettyMail($properties, 300));
    $this->assertMatchesMail($this->mails, $properties, 0,$message);
  }

}

