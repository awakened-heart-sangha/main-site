<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\simplenews\Entity\Subscriber;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;

/**
 * See Drupal\simplenews\Form\SubscriptionsPageForm
 */
class AnonymousNewslettersUpdatingTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use NewsTrait;
  use NewsDefinitions;
  use SubscriptionsPageTrait;
  use UserDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Newsletter preferences can be changed by anonymous users
    In order to easily unsubscribe and manage preferences
    Subscribers should not have to log in when following a link from email footer

    Scenario: Anonymous user cannot change own email on preferences page
      Given I am not logged in
      When I view my preferences page by hashed link
      And I should not see an email input
      And I should see "to change your email address, please login"

    Scenario: Logged in user can change own email on preferences page
      Given I am logged in
      When I view my preferences page by hashed link
      Then I should see an email input
      And I should not see "to change your email address, please login"
      When I enter a new email
      And I press "Update"
      Then my email has been changed

    Scenario: Anonymous user can subscribe on preferences page
      Given I am not logged in
      Then I am not subscribed to the collaboration newsletter
      When I view my preferences page by hashed link
      And I check "Collaboration"
      And I press "Update"
      Then I am subscribed to the collaboration newsletter
      When I view my preferences page by hashed link
      And I uncheck "Collaboration"
      And I press "Update"
      Then I am not subscribed to the collaboration newsletter

    Scenario: Logged in user can subscribe on prefernces page
      Given I am logged in
      Then I am not subscribed to the collaboration newsletter
      When I view my preferences page by hashed link
      And I check "Collaboration"
      And I press "Update"
      Then I am subscribed to the collaboration newsletter
      When I view my preferences page by hashed link
      And I uncheck "Collaboration"
      And I press "Update"
      Then I am not subscribed to the collaboration newsletter

    # Now irrelevant as anon cannot change email on prefs page
    #Scenario: Anonymous user cannot change email to another user's on preferences page
    #  Given there is another user
    #  And I am not logged in
    #  When I view my preferences page by hashed link
    #  And I enter the other user's email
    #  And I press "Update"
    #  Then I should see "A user already exists with the email"

    Scenario: Logged in user cannot change email to another user's on preferences page
      Given there is another user
      And I am logged in
      When I view my preferences page by hashed link
      And I enter the other user's email
      And I press "Update"
      Then I should see "A user already exists with the email"

    Scenario: Unsubscribe from all on preferences page
      Given I am logged in
      And I have subscribed to the retreats newsletter
      And I have subscribed to the collaboration newsletter
      When I view my preferences page by hashed link
      And I press "Unsubscribe from all"
      Then I am not subscribed to the retreats newsletter
      And I am not subscribed to the collaboration newsletter

FEATURE;

  /**
   * @When I view my preferences page by hashed link
   */
  public function iViewNewslettersPage() {
    // loadByUid can create subscriber.
    $subscriber = Subscriber::loadByUid($this->getMe()->id(), TRUE);
    $subscriber->save();
    $subscriber = $this->reloadEntity($subscriber);
    $this->assertUserSubscriber($this->getMe());
    $timestamp = \Drupal::service('datetime.time')->getCurrentTime();
    $hash = simplenews_generate_hash($subscriber->getMail(), 'manage', $timestamp);
    $snid = $subscriber->id();
    $this->drupalGet("/newsletter/subscriptions/{$snid}/{$timestamp}/{$hash}");
    $this->assertSession()->pageTextContains($this->getMe()->getEmail());
  }


  /**
   * @Then I should see an email input
   */
  public function iShouldSeeAnEmailInput() {
    // There is a subscriber form with a mail field in the footer we want to ignore.
    $page = $this->getSession()->getPage();
    $form = $page->find('css','form.simplenews-subscriber-account-form') ?? $page->find('css','form.simplenews-subscriber-page-form');
    $this->assertNotNull($form, "There should be a subscriber page or account form");
    $this->assertSession()->fieldExists("mail[0][value]", $form);
  }

  /**
   * @Then I should not see an email input
   */
  public function iShouldNotSeeAnEmailInput() {
    // There is a subscriber form  with a mail field in the footer we want to ignore.
    $page = $this->getSession()->getPage();
    $form = $page->find('css','form.simplenews-subscriber-account-form') ?? $page->find('css','form.simplenews-subscriber-page-form');
    $this->assertNotNull($form, "There should be a subscriber page or account form");
    $this->assertSession()->fieldNotExists("mail[0][value]", $form);
  }

}

