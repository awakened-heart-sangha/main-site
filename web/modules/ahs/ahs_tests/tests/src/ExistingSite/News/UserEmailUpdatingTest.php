<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBase;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * User and subscribers are synced in complex ways that can yield surprising bugs.
 */
class UserEmailUpdatingTest extends ExistingSiteBase {

  use NewsTrait;

  public function testUserOnly() {
    $originalEmail = $this->randomEmail();
    $newEmail = $this->randomEmail();
    $user = User::create(['mail' => $originalEmail]);
    $user->save();
    $this->markEntityForCleanup($user);
    $this->assertUserEmail($user, $originalEmail);
    // User email updates should stick.
    $this->updateUserEmail($user, $newEmail);
  }

  public function testUserFirst() {
    $originalEmail = $this->randomEmail();
    $user = User::create(['mail' => $originalEmail]);
    $user->save();
    $this->markEntityForCleanup($user);

    // loadByUser creates subscribers.
    $subscriber = Subscriber::loadByUid($user->id(), TRUE);
    $subscriber->save();
    $this->assertUserSubscriber($user);
    $this->assertUserSubscriberEmail($user, $originalEmail);

    // When subscriber email is updated, both user and subscriber email are updated.
    $newSubscriberEmail = $this->randomEmail();
    $this->updateSubscriberEmail($subscriber, $newSubscriberEmail);
    $this->assertUserSubscriberEmail($user, $newSubscriberEmail);

    // When user email is updated, both user and subscriber email are updated.
    $newUserEmail = $this->randomEmail();
    $this->updateUserEmail($user, $newUserEmail);
    $this->assertUserSubscriberEmail($user, $newUserEmail);
  }


  public function testSubscriberFirst() {
    $originalEmail = $this->randomEmail();
    // loadByMail creates subscribers.
    $subscriber = Subscriber::loadByMail($originalEmail, TRUE);
    $subscriber->save();
    $this->markEntityForCleanup($subscriber);
    $subscriber = $this->reloadEntity($subscriber);

    // A user should be created automatically for a subscriber by ahs_news_simplenews_subscriber_presave().
    $user = $this->assertSubscriberWithUser($subscriber);
    $this->assertUserEmail($user, $originalEmail);

    // When subscriber email is updated, both user and subscriber email are updated.
    $newSubscriberEmail = $this->randomEmail();
    $this->updateSubscriberEmail($subscriber, $newSubscriberEmail);
    $this->assertUserSubscriberEmail($user, $newSubscriberEmail);

    // When user email is updated, both user and subscriber email are updated.
    $newUserEmail = $this->randomEmail();
    $this->updateUserEmail($user, $newUserEmail);
    $this->assertUserSubscriberEmail($user, $newUserEmail);
  }


  protected function updateUserEmail(UserInterface $user, $email) {
    $user = $this->reloadEntity($user);
    $user->setEmail($email);
    $user->save();
  }

  protected function updateSubscriberEmail(SubscriberInterface $subscriber, $email) {
    $subscriber = $this->reloadEntity($subscriber);
    $subscriber->setMail($email);
    $subscriber->save();
  }

}

