<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;
use Drupal\user\UserInterface;

/**
 * A trait to help with testing simplenews newsletter subscriptions pages.
 */
trait SubscriptionsPageTrait {

  protected $newEmail;
  protected $otherUser;

  /**
   * @When I enter a new email
   */
  public function iEnterANewEmail() {
    $page = $this->getCurrentPage();
    $this->newEmail = $this->randomEmail();
    $page->fillField('Email', $this->newEmail);
  }

  /**
   * @Then my email has been changed
   */
  public function myEmailIsChanged() {
    $me = $this->getMe();
    $this->assertSame($this->newEmail, $me->getEmail(), "My user email should be updated");
    $this->assertSame($this->newEmail, $this->getUserSubscriberEmail($me), "My subscriber email should be updated");
  }

  /**
   * @Given there is another user
   */
  public function iAmANewUserWithTheEmail() {
    $this->otherUser = $this->createDecoupledUser();
  }

  /**
   * @When I enter the other user's email
   */
  public function theOtherUsersEmail() {
    $page = $this->getCurrentPage();
    $page->fillField('Email', $this->otherUser->getEmail());
  }

  /**
   * @Then email validation should fail
   */
  public function emailValidationShouldFail() {
    $page = $this->getCurrentPage();
    $page->fillField('Email', $this->otherUser->getEmail());
  }

}
