<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

/**
 * @group ClonedDatabaseRequired
 */
class SubscriptionBlockTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use NewsTrait;
  use NewsDefinitions;
  use SubscriptionsPageTrait;
  use UserDefinitions;
  use UserTrait;

  protected static $feature = <<<'FEATURE'
Feature: Signup form in footer of page
    In order to allow easy signup to newsletters
    There is a simple form in page footer

    Scenario: Subscription form for anonymous non-subscribers
      Given I am not logged in
      When I visit "<front>"
      Then I should see the signup form
      When I enter my email into the signup form
      And I press "Subscribe"
      Then I should see "Your email news preferences"
      And I am subscribed to the "important" newsletter
      And I am subscribed to the "retreats" newsletter
      And I am not subscribed to the "online_courses" newsletter

    Scenario: Subscription form for anonymous non-subscribers adds to existing subscriptions
      Given I am a user
      And I have subscribed to the "important" newsletter
      And I have subscribed to the "online_courses" newsletter
      And I have not subscribed to the "retreats" newsletter
      And I am not logged in
      When I visit "<front>"
      Then I should see the signup form
      When I enter my email into the signup form
      And I press "Subscribe"
      Then I should see "Your email news preferences"
      And I am subscribed to the "important" newsletter
      And I am subscribed to the "retreats" newsletter
      And I am subscribed to the "online_courses" newsletter

    Scenario: Subscription form for logged-in non-subscribers
      Given I am logged in
      When I visit "<front>"
      Then I should see the signup form
      And the email input on the signup form is disabled
      When I press "Subscribe"
      Then I should see "Your email news preferences"
      And I am subscribed to the "important" newsletter
      And I am subscribed to the "retreats" newsletter
      And I am not subscribed to the "online_courses" newsletter

    Scenario: Subscription form for logged-in subscribers
      Given I am logged in
      And I have subscribed to the "important" newsletter
      And I have subscribed to the "online_courses" newsletter
      And I have not subscribed to the "retreats" newsletter
      When I visit "<front>"
      Then I should see the signup form
      And the email input on the signup form is disabled
      When I press "Subscribe"
      Then I should see "Your email news preferences"
      And I am subscribed to the "important" newsletter
      And I am subscribed to the "retreats" newsletter
      And I am subscribed to the "online_courses" newsletter

    Scenario: Subscription form for logged-in subscribers with nothing to add
      Given I am logged in
      And I have subscribed to the "important" newsletter
      And I have subscribed to the "retreats" newsletter
      When I visit "<front>"
      Then I should not see the signup form


FEATURE;

  /**
   * @Then I should see the signup form
   */
  public function iShouldSeeTheSignupForm() {
    $this->assertSession()->fieldExists("mail[0][value]");
  }

  /**
   * @Then I should not see the signup form
   */
  public function iShouldNotSeeTheSignupForm() {
    $this->assertSession()->fieldNotExists("mail[0][value]");
  }

  /**
   * @Then I enter my email into the signup form
   */
  public function iEnterMyEmailIntoTheSignupForm() {
    $page = $this->getSession()->getPage();
    $page->fillField("mail[0][value]", $this->getMyEmail());
  }

  /**
   * @Then the email input on the signup form is disabled
   */
  public function theEmailInputOnTheSignupFormIsDisabled() {
    $this->assertSession()->elementAttributeExists('named', ['field', 'mail[0][value]'], 'disabled');
  }

}

