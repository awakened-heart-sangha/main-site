<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;

/**
 * @group NewDatabaseRequired
 */
class NewsHistoryTest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use NewsTrait;
  use UserDefinitions;
  use MailCollectionTrait;
  use NewsDefinitions;

  protected $news;

  protected static $feature = <<<'FEATURE'
Feature: Email news is marked as read, even without login
    In order to highlight unread news
    We track news read by subscribers, even without login

    Background:
      Given 3 items of news
      And I have subscribed to the important newsletter
      And Fred has subscribed to the important newsletter
      Then the first news is not marked as read for me

    Scenario: Sent news is marked as read
      When the first news is sent to the important newsletter
      Then the first news is marked as read for me
      And the first news is marked as read for Fred

    Scenario: Read other news is not sent
      Given I have read the second news
      And Fred has not read the second news
      When the first news is sent to the important newsletter
      Then the html email sent to me does not contain the second news as other news
      And the html email sent to me contains the third news as other news
      And the html email sent to Fred contains the second news as other news
      And the html email sent to Fred contains the third news as other news

    Scenario: Other news links have subscriber uuid in query
      When the first news is sent to the important newsletter
      Then the link to the other news sent to me contains the subscriber uuid
      And the link to the other news sent to Fred contains the subscriber uuid

    Scenario: Visiting news marks as read if logged in
      Given I am logged in
      When I visit the first news
      Then the first news is marked as read for me
      And the second news is not marked as read for me
      When I visit the second news
      Then the second news is marked as read for me

    Scenario: Visiting news does not mark as read if not logged in
      Given I am not logged in
      When I visit the first news
      Then the first news is not marked as read for me
      And the second news is not marked as read for me
      When I visit the second news
      Then the second news is not marked as read for me

    Scenario: Visiting news with subscriber uuid in query marks as read if not logged in
      Given I am not logged in
      When I visit the first news with subscriber uuid in query
      Then the first news is marked as read for me
      And the second news is not marked as read for me
      When I visit the second news
      Then the second news is marked as read for me

    Scenario: Visiting news with subscriber uuid in query marks as read if logged in
      Given I am logged in
      When I visit the first news with subscriber uuid in query
      Then the first news is marked as read for me
      And the second news is not marked as read for me
      When I visit the second news
      Then the second news is marked as read for me

    Scenario: Visiting news and logging in is unproblematic
      Given I am not logged in
      When I visit the first news with subscriber uuid in query
      Then the first news is marked as read for me
      When I log in
      Then the first news is marked as read for me
      And the second news is not marked as read for me
      When I visit the second news
      Then the second news is marked as read for me

FEATURE;


  /**
   * @Given :count items of news
   */
  public function itemsOfNews($count) {
    for ($i = 0; $i < 3; $i++) {
      $node = Node::create([
        'type' => 'news',
        'title' => $this->randomMachineName(),
      ]);
      $node->setPublished();
      $node->save();
      $this->markEntityForCleanup($node);
      $this->news[] = $node;
    }
  }

  /**
   * @Then the :position news is marked as read for :name
   */
  public function theNewsIsMarkedAsReadFor($position, $name) {
    $this->assertNewsMarkedRead($position, $name, TRUE);
  }

  /**
   * @Then the :position news is not marked as read for :name
   */
  public function theNewsIsNotMarkedAsReadFor($position, $name) {
    $this->assertNewsMarkedRead($position, $name, FALSE);
  }

  protected function assertNewsMarkedRead($position, $name, $isTrue = TRUE) {
    $user = $this->getUser($name);
    $news = $this->getNewsByPosition($position);
    $result = \Drupal::database()->query('SELECT nid, timestamp FROM {history} WHERE uid = :uid AND nid IN ( :nids[] )', [
      ':uid' => $user->id(),
      ':nids[]' => [$news->id()],
    ]);
    $arr = $result->fetchAssoc();
    $this->assertSame($isTrue, !empty($arr));
  }

  protected function getNewsByPosition($position) {
    $index = $position === 'first' ? 0 : (($position === 'second') ? 1 : 2);
    $news = $this->news[$index];
    return $news;
  }

  /**
   * @Given :name has/have read the :position news
   */
  public function hasReadTheNews($position, $name) {
    $news = $this->getNewsByPosition($position);
    history_write($news->id(), $this->getUser($name));
    $this->assertNewsMarkedRead($position, $name);
  }

  /**
   * @Given :name has not read the :position news
   */
  public function hasNotReadTheNews($position, $name) {
    $this->assertNewsMarkedRead($position, $name, FALSE);
  }

  /**
   * @When the first news is sent to the important newsletter
   */
  public function theFirstNewsIsSentToTheImportantNewsletter() {
    $issue = $this->news[0];
    $issue->set('simplenews_issue', [
        'target_id' => 'default',
        'handler_settings' => ['newsletters' => ['important' => 'important']],
      ]
    );
    $issue->save();
    $this->sendIssue($issue);
  }

  /**
   * @Then the html email sent to :name contains the :position news as other news
   */
  public function theHtmlEmailSentContainsTheOtherNews($name, $position) {
    $user = $this->getUser($name);
    $mails = $this->getUserMails($user);
    $this->assertNotEmpty($mails, "There should be mail for $name");
    $properties = [
      'body' => [
        $this->getNewsByPosition($position)->getTitle(),
      ],
    ];
    $this->assertMatchInMails($mails, $properties);
  }

  /**
   * @Then the html email sent to :name does not contain the :position news as other news
   */
  public function theHtmlEmailSentDoesNotContainTheOtherNews($name, $position) {
    $user = $this->getUser($name);
    $mails = $this->getUserMails($user);
    $this->assertNotEmpty($mails, "There should be mail for $name");
    $properties = [
      'body' => [
        $this->getNewsByPosition($position)->getTitle(),
      ],
    ];
    $this->assertNoMatchInMails($mails, $properties);
  }

  /**
   * @Then the link to the other news sent to :name contains the subscriber uuid
   */
  public function theLinktoTheOtherNewsSentContainstheSubscriberUuid($name) {
    $user = $this->getUser($name);
    $subscriber = Subscriber::loadByUid($user->id());
    $properties = [
      'body' => [
        $this->getNodeSuuidUrl($this->news[1], $subscriber),
        $this->getNodeSuuidUrl($this->news[2], $subscriber),
      ],
    ];
    $message = "$name subscriber uuid " . $subscriber->get('uuid')->value . " should be appended to news links in mail to $name";
    $this->assertUserMailWithProperties($user, $properties, TRUE, $message);

    // Make sure subscriber uuid is not cached.
    foreach($this->users as $other_user_name => $other_user) {
      if ($other_user_name !== $name) {
        $other_subscriber = Subscriber::loadByUid($other_user->id());
        $properties = [
          'body' => [
            $this->getNodeSuuidUrl($this->news[1], $other_subscriber),
            $this->getNodeSuuidUrl($this->news[2], $other_subscriber),
          ],
        ];
        $message = "$other_user_name subscriber uuid " . $other_subscriber->get('uuid')->value . " should not be appended to news links in mail to $name who has subscriber uuid " . $subscriber->get('uuid')->value;
        $this->assertUserMailWithProperties($user, $properties, FALSE, $message);
      }
    }

  }

  /**
   * @When I visit the :position news
   */
  public function iVisitTheNews($position) {
    $news = $this->getNewsByPosition($position);
    $this->drupalGet($news->toUrl()->toString());
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    sleep(2);
  }

  /**
   * @When I visit the :position news with subscriber uuid in query
   */
  public function iVisitTheNewsWithSubscriberUuid($position) {
    $user = $this->getUser('me');
    $subscriber = Subscriber::loadByUid($user->id());
    $news = $this->getNewsByPosition($position);
    $url = $this->getNodeSuuidUrl($news, $subscriber);
    $this->drupalGet($url);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    sleep(2);
  }

  protected function getNodeSuuidUrl(NodeInterface $node, SubscriberInterface $subscriber) {
    return "/node/" . $node->id() . "/suuid/" . $subscriber->get('uuid')->value;
  }

}

