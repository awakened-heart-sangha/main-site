<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group NewDatabaseRequired
 */
class NewsFooterSuggestedGroupsTest extends ExistingSiteBehatBase {

  use TrainingTrait;
  use NewsTrait;
  use NewsDefinitions;
  use MailCollectionTrait;

  protected $issue;
  protected $event;
  protected $course;
  protected $ctaAbsoluteUrl;

  protected static $feature = <<<'FEATURE'
Feature: Email news footer has suggested groups
    In order to remind students of suggested groups
    Email footers have a personalised list of suggested groups

    Background:
      Given a course that starts in the future
      And experiences:
        | experience  |
        | ExperienceB |
        | ExperienceC |
      And the course has no eligibility conditions
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceC |
      And a user named "Jack" has no training records
      And a user named "Jane" has training records for:
        | experience  |
        | ExperienceC |
      And a user named "Fred" has training records for:
        | experience  |
        | ExperienceB |
      And Jack has subscribed to the important newsletter
      And Jane has subscribed to the important newsletter
      And Fred has subscribed to the important newsletter
      And a news issue for the important newsletter

    # This is tricky because it requires the body to be personalised per subscriber, not cached.
    Scenario: Personalised suggested groups
      When the news issue is sent
      Then the course is not in the email sent to Jack
      And the course is in the email sent to Jane
      And the course is not in the email sent to Fred

    Scenario: Joined groups appear if future not past
      Given an event that starts in the past
      And Jane is already a participant in the event
      And Jack is already a participant in the course
      When the news issue is sent
      Then the course is in the email sent to Jack
      And the event is not in the email sent to Jack

FEATURE;

  /**
   * @When the news issue is sent
   */
  public function theNewsIssueIsSent() {
    $this->sendIssue($this->issue);
  }

  /**
   * @Then the :bundle is in the email sent to :name
   */
  public function theCourseIsInTheEmailSentTo($bundle, $name) {
    $user = $this->getUser($name);
    $mails = $this->getUserMails($user);
    $this->assertMailCount($mails, 1);
    $properties = [
      'body' => [
        'Coming soon',
        $this->{$bundle}->label(),
        $GLOBALS['base_url'] . $this->{$bundle}->toUrl()->toString(),
      ],
    ];
    $this->assertMatchesMail($mails, $properties);
  }

  /**
   * @Then the :bundle is not in the email sent to :name
   */
  public function theCourseIsNotInTheEmailSentTo($bundle, $name) {
    $user = $this->getUser($name);
    $mails = $this->getUserMails($user);
    $this->assertMailCount($mails, 1);
    $properties = [
      'body' => [
        'Coming soon',
        $this->{$bundle}->label(),
      ],
    ];
    $this->assertNotMatchesMail($mails, $properties);
  }

  /**
   * @Given an event that starts in the past
   */
  public function anEventThatStartsInThePast() {
    $date = new DrupalDateTime("-3 months");
    $fields = [
      'field_dates' => ['value' => $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),],
      'type' => 'event',
    ];
    $this->event = $this->createGroup($fields, FALSE);

  }

}
