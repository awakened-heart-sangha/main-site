<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\Component\Utility\Html;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;
use Drupal\Tests\ahs_tests\Traits\TabsTrait;

/**
 * @group NewDatabaseRequired
 */
class NewsSendingUITest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use NewsTrait;
  use NewsDefinitions;
  use UserDefinitions;
  use MailCollectionTrait;
  use TabsTrait;

  protected $issue;
  protected $student;
  protected $ctaAbsoluteUrl;

  protected static $feature = <<<'FEATURE'
Feature: Editors can send newsletters
    In order to control which recipients get a newsletter
    Editors should be able to choose from newsletter lists and initiate sending

    Background:
      # Test uses low-volume lists to help performance on dev environment with production db
      Given a student has subscribed to the collaboration newsletter
      And the student has subscribed to the lama_shenpen newsletter
      And a draft news issue

    Scenario: Mailer email news UI for drafts not set to be sent
      Given I am logged in as a mailer
      When I am viewing the draft news issue
      Then I should see "Publish"
      And I should see "Scheduling options"
      And I should see "Send to anyone"
      And I should see an "Email" tab
      When I view the email sending form
      Then I should see "to 0 subscribers"
      And I should see "No recipients selected yet"
      And I should not see "will be be sent"
      And I should see a button labelled "Send on publish"

    Scenario: Non-mailer email news UI for drafts not set to be sent
      Given I am logged in as a news_writer
      When I am viewing the draft news issue
      Then I should see "Publish"
      And I should see "Scheduling options"
      And I should see "Send to anyone"
      When I view the node page
      And I should see an "Email" tab
      When I view the email sending form
      Then I should see "to 0 subscribers"
      And I should see "No recipients selected yet"
      And I should not see "will be be sent"
      And I should not see a button labelled "Send on publish"

    Scenario: Mailer email news UI for drafts with recipients set to send on publish
      Given I am logged in as a mailer
      And the news will be sent to the collaboration list when published
      When I am viewing the draft news issue
      Then I should see "Publish"
      And I should see "Scheduling options"
      And I should see "Send to anyone"
      When I view the email sending form
      Then I should see "to 1 subscriber"
      And I should see "Send to anyone subscribed to collaboration"
      And I should see "Mails will be sent when this news is published"
      And I should see a button labelled "Stop sending"

    Scenario: Non-mailer email news UI for drafts with recipients set to send on publish
      Given I am logged in as a news_writer
      And the news will be sent to the collaboration list when published
      When I am viewing the draft news issue
      Then I should see "Publish"
      And I should see "Scheduling options"
      And I should not see "Send to anyone"
      When I view the email sending form
      Then I should see "to 1 subscriber"
      And I should see "Send to anyone subscribed to collaboration"
      And I should see "Mails will be sent when this news is published"
      And I should see a button labelled "Stop sending"

    Scenario: Sending news to collaboration list
      Given I am logged in as a mailer
      When I am viewing the draft news issue
      And I check "Send to anyone"
      And I check "Collaboration" for "anyone"
      And I save and send the newsletter issue
      Then the student receives a single email

    # Test this in order to prove validity of sending to multiple in next scenario.
    Scenario: Sending news to Lama Shenpen list
      Given I am logged in as a mailer
      When I am viewing the draft news issue
      And I check "Send to anyone"
      And I check "Lama Shenpen" for "anyone"
      And I save and send the newsletter issue
      Then the student receives a single email

    Scenario: Sending news to multiple lists
      Given I am logged in as a mailer
      When I am viewing the draft news issue
      And I check "Send to anyone"
      And I check "Collaboration" for "anyone"
      And I check "Lama Shenpen" for "anyone"
      And I save and send the newsletter issue
      Then the student receives a single email

    Scenario: Sending news to lists a user is not subscribed to
      Given I am logged in as a mailer
      When I am viewing the draft news issue
      And I check "Send to anyone"
      And I check "Help wanted" for "anyone"
      And I save and send the newsletter issue
      Then the student receives no email

    Scenario: Sending news to members only sends no emails to non-members
      Given I am logged in as a mailer
      When I am viewing the draft news issue
      And I check "Send to members"
      And I check "Collaboration" for "members"
      And I save and send the newsletter issue
      Then the student receives no email

    Scenario: Sending news to members only sends emails to members
      Given I am logged in as a mailer
      And the student is a member
      When I am viewing the draft news issue
      And I check "Send to members"
      And I check "Collaboration" for "members"
      And I save and send the newsletter issue
      Then the student receives a single email
      When I view the email sending form
      Then I should see "to 1 subscriber"
      And I should see "Sent to members subscribed to collaboration"


FEATURE;

  /**
   * @When I am viewing the draft news issue
   */
  public function iAmViewingADraftNewsIssue() {
    $this->drupalGet("/node/" . $this->issue->id() . "/edit");
    $this->assertSession()->pageTextContains($this->issue->getTitle());
  }

  /**
   * @Given the news will be sent to the :newsletter_id list when published
   */
  public function theNewsWillBeSentToWhenPublished($newsletter_id) {
    $this->issue->set('simplenews_issue', [
        'target_id' => 'default',
        'handler_settings' => ['newsletters' => [$newsletter_id => $newsletter_id]],
        'status' => SIMPLENEWS_STATUS_SEND_PUBLISH,
      ]
    );
    $this->issue->save();
  }

  /**
   * @When I save and send the newsletter issue
   */
  public function iSendTheNewsletterIssue() {
    // Helps if developing against a clone of the production db.
    \Drupal::service('simplenews.spool_storage')->deleteMails([]);
    $this->icheck("Published");
    $this->iPress("Save");
    $this->drupalGet("/node/" . $this->issue->id() . "/email");
    $this->iPress("Send now");
    simplenews_cron();
    // Running twice helps if developing against a clone of the production db.
    simplenews_cron();
  }

  /**
   * @When I view the node page
   */
  public function iViewtheNoePage() {
    $this->drupalGet("/node/" . $this->issue->id());
  }

  /**
   * @When I view the email sending form
   */
  public function iViewtheEmailForm() {
    $this->drupalGet("/node/" . $this->issue->id() . "/email");
  }

  /**
   * @Given a/the student has subscribed to the :newsletter_id newsletter
   */
  public function iAmSubscribedToTheNewsletter($newsletter_id) {
    $student = $this->getStudent();
    $this->subscribeUser($student, $newsletter_id);
  }

  /**
   * @Given the student is a member
   */
  public function theStudentIsAMember() {
    $student = $this->getStudent();
    $student->addRole('member');
    $student->save();
  }

  protected function getStudent() {
    if (!isset($this->student)) {
      $this->student = $this->createDecoupledUser();
    }
    return $this->reloadEntity($this->student);
  }

  /**
   * @Then the student receives a single email
   */
  public function theStudentReceivesASingleEmail() {
    $mails = $this->getMail(['to' => $this->getStudent()->getEmail()]);
    $this->assertMailCount($mails, 1);
    $properties = [
      'subject' => $this->issue->getTitle(),
      'body' => [$this->issue->get('body')->value],
    ];
    $this->assertTrue($this->matchesMail($mails[0], $properties), sprintf("'Mail should have the right subject & body':\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($mails, 2000), print_r($properties, TRUE)));
  }

  /**
   * @Then the student receives no email
   */
  public function theStudentReceivesNoEmail() {
    $mails = $this->getMail(['to' => $this->getStudent()->getEmail()]);
    $this->assertMailCount($mails, 0);
  }

  /**
   * @When I check :checkbox for :type
   */
  public function icheckForType($checkbox, $type) {
    $page = $this->getCurrentPage();
    $this->assertContains($type, ['members', 'anyone']);
    $selector_id = $type === 'members' ? 'member' : 'anyone';
    $checkbox_id = Html::getId($checkbox);
    $selector = ("edit-simplenews-issue-handler-settings-$selector_id-newsletters-$checkbox_id");
    $this->assertSession()->waitForField($selector);
    $checkboxElement = $page->findField($selector);
    $this->assertNotNull($checkboxElement, "A '$checkbox' form field should be on the page.");
    $checkboxElement->check();
  }

}
