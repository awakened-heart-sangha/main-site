<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\News;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;

/**
 * @group NewDatabaseRequired
 */
class NewsAccessTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use NewsTrait;
  use NewsDefinitions;
  use UserDefinitions;

  protected $issue;
  protected $ctaAbsoluteUrl;

  protected static $feature = <<<'FEATURE'
Feature: Administering existing news
    In order to adminster news
    Writers should have access to unpulished

    Background:
      Given a draft news issue

    Scenario: Admin listing
      Given I am logged in as a news_writer
      When I go to "/admin/news"
      Then I should see the news issue title
      When the news issue is published
      And I go to "/admin/news"
      Then I should see the news issue title

    Scenario: Unpublished access for writers
      Given I am logged in as a news_writer
      When I visit the news issue
      Then I should see the news issue title

    Scenario: Unpublished access for student
      Given I am logged in as a member
      When I visit the news issue
      Then I should see "Access denied"
      When the news issue is published
      And I visit the news issue
      Then I should see the news issue title

    Scenario: Public listings
      Given I am logged in as a member
      When I visit "/news"
      Then I should not see the news issue title
      When the news issue is published
      And I visit "/news"
      Then I should see the news issue title


FEATURE;

  /**
   * @Then I should see the news issue title
   */
  public function iShouldSeeTheNewsIssue() {
    $this->assertSession()->pageTextContains($this->issue->getTitle());
  }

  /**
   * @Then I should not see the news issue title
   */
  public function iShouldNotSeeTheNewsIssue() {
    $this->assertSession()->pageTextNotContains($this->issue->getTitle());
  }

  /**
   * @When I visit the news issue
   */
  public function iVisitTheNews() {
    $this->drupalGet($this->issue->toUrl()->toString());
  }

  /**
   * @When the news issue is published
   */
  public function thrNewsIssueIsPublished() {
    $this->issue->setPublished();
    $this->issue->save();
  }

}
