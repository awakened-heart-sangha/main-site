<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\VisualRegression;

/**
 * @group ClonedDatabaseRequired
 * @group js
 */
class InformationTest extends VisualRegressionBase {

  protected static $feature = <<<'FEATURE'
Feature: Public information pages

    Scenario Outline: Main public pages screenshots
      Then I take a screenshot of "<page>"

      Examples:
        | page                    |
        | <front>                 |
        | /about                  |
        | /meditation-online      |
        | /lama-shenpen           |
        | /donations-and-payments |

    # Currently crashing on Gitlab CI
    #Scenario: Training page screenshot
      #Then I take a screenshot of the training page

    # Used for debugging visual regression
    #Scenario: Debug page screenshot
    #  Then I take a screenshot of the debug page

FEATURE;

  /**
   * @Then I take a screenshot of the training page
   */
  public function iTakeAScreenshotOfTheTrainingPage()
  {
    $this->windowHeight = 1000;

    // The training page is very long so sometimes crashes the test browser
    try {
      $this->iTakeAScreenshotOf("/training");
    }
    catch(\Exception $exception) {
      $this->markAsRisky();
    }
  }

  /**
   * @Then I take a screenshot of the debug page
   */
  public function iTakeAScreenshotOfTheDebugPage()
  {
    $this->iTakeAScreenshotOf("/debug");
  }

}

