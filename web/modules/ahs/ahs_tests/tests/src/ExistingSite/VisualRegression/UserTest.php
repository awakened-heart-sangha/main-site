<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\VisualRegression;

/**
 * @group ClonedDatabaseRequired
 * @group js
 */
class UserTest extends VisualRegressionBase {

  protected static $feature = <<<'FEATURE'
Feature: User pages

    Scenario Outline: User pages
      Then I take a screenshot of "<page>"

      Examples:
        | page |
        | /user/register |
        | /user/login    |
        | /user/password |

FEATURE;


}

