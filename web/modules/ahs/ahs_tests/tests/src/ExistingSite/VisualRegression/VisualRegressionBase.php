<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\VisualRegression;

use Behat\Gherkin\Node\TableNode;
use Behat\Behat\EventDispatcher\Event\AfterStepTested;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\RetryTrait;

class VisualRegressionBase extends ExistingSiteJsBehatBase {

  use RetryTrait;

  /*
   * Retry any failing test 3 times, in case it's due to random browser crash.
   */
  protected $retry = 3;

  protected function setUp(): void {
    parent::setUp();
    $this->appendWidthToScreenshotName = TRUE;
  }

  /**
   * @Then I take a screenshot
   */
  public function iTakeAScreenshot()
  {
    $this->preparePageForVisualRegression();
    $this->screenshotPage($this->visualRegressionDirectory);
  }

  /**
   * @Then I take a screenshot of :page
   */
  public function iTakeAScreenshotOf($page)
  {
    $this->drupalGet($page);
    $filename = $this->sanitiseFilePathComponent($page);
    $this->preparePageForVisualRegression();
    $this->screenshotPage($this->visualRegressionDirectory, $filename);
  }

  /**
   * @Then I take screenshots of( these pages):
   */
  public function iTakeScreenshots(TableNode $pages)
  {
    $pages = $pages->getHash();
    foreach ($pages as $page) {
     $this->iTakeAScreenshotOf($page['page']);
    }
  }

  /**
   * Disable automatic screenshots when doing explicit visual regression steps.
   *
   * @param AfterStepTested $event
   */
  public function autoScreenshot(AfterStepTested $event) {}

}

