<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\VisualRegression;

use Drupal\Tests\ahs_tests\Traits\Definitions\NewsDefinitions;
use Drupal\Tests\ahs_tests\Traits\NewsTrait;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group NewDatabaseRequired
 * @group js
 */
class EmailNewsTest extends VisualRegressionBase {

  use NewsDefinitions;
  use NewsTrait;
  use TrainingTrait;

  protected $ctaAbsoluteUrl;
  protected $issue;
  protected $course;
  protected $otherNews;
  protected $topic;

  protected static $feature = <<<'FEATURE'
Feature: Newsletter theming

    Scenario: Newsletter preview page
      Given a draft news issue
      And a course that starts in the future
      And the course has no eligibility conditions
      And the course is suggested
      And there is an item of other news
      And there is a synced help wanted topic
      And I am logged in as a mailer

FEATURE;


}

