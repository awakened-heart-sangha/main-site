<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\VisualRegression;

use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBase;
use Drupal\Tests\ahs_tests\Traits\RetryTrait;

/**
 * @group ClonedDatabaseRequired
 * @group js
 */
class StyleguidesTest extends ExistingSiteJsBase {

  use UserDefinitions;
  use RetryTrait;

  /*
   * Retry any failing test 3 times, in case it's due to random browser crash.
   */
  protected $retry = 3;

  protected $inVisualRegressionDisplayNone = ['header', 'footer', '.page-header'];

  protected function setUp(): void {
    parent::setUp();
    $this->appendWidthToScreenshotName = TRUE;
  }

  /**
   * Create visual regression screenshots for  top-level styleguide paragraphs
   * in isolation from each other, rather than simply screenshotting the whole page.
   */
  public function testStyleguides() {
    $user = $this->createUser();
    $this->setMe($user);
    $this->iLogin();

    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    $styleguides = $nodeStorage->loadByProperties(['type' => 'styleguide']);

    foreach ($styleguides as $styleguide) {
      $ids = array_column($styleguide->field_elements->getValue(), 'target_id');
      foreach($ids as $id) {

        $this->drupalGet('/styleguide/section/' . $id);
        $filename = $this->sanitiseFilePathComponent($styleguide->label() . "-" . $id);
        $this->preparePageForVisualRegression();
        $this->screenshotPage($this->visualRegressionDirectory, $filename);
      }
    }
  }

}

