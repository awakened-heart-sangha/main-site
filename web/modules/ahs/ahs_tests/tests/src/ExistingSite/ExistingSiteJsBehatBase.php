<?php

namespace Drupal\Tests\ahs_tests\ExistingSite;

use Behat\Mink\Exception\ExpectationException;
use PHPUnitBehat\PHPUnit\Framework\AssertionFailedWrappedError;
use PHPUnitBehat\TestTraits\BehatTestTrait;

/**
 * Base class for AHS javascript site tests that provide Behat features.
 */
abstract class ExistingSiteJsBehatBase extends ExistingSiteJsBase {

  protected static $feature = <<<'FEATURE'
Feature: BehatProvidingTrait
    In order to test a feature
    We need to able provide it to phpunit

    Scenario: ExistingSiteJsBehatBase
      Then js behat test has a step

FEATURE;

  use BehatTestTrait {
    assertBehatScenarioPassed as assertBehatScenarioPassedTrait;
  }

  public static function assertBehatScenarioPassed($scenarioResults, $scenario = NULL, $stepResults = [], $snippetGenerator = NULL, $environment = NULL, $message = '', $callHandler = '')
  {
    try {
      self::assertBehatScenarioPassedTrait($scenarioResults, $scenario, $stepResults, $snippetGenerator, $environment, $message, $callHandler);
    }
    catch (ExpectationException $e) {
      throw new AssertionFailedWrappedError($e);
    }
  }

  /**
   * @Then js behat test has a step
   */
  public function jsBehatHasDefautltStep() {
    $this->assertTrue(TRUE);
  }

}
