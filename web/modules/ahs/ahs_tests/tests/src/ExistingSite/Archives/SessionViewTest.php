<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Archives;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

class SessionViewTest extends ExistingSiteBehatBase  {

  use GroupDefinitions;
  use UserDefinitions;
  use GroupTrait;
  use UserTrait;

  protected $node;
  protected $media;
  protected $event;
  protected $paragraph;
  protected $course;
  protected $venue;

  protected static $feature = <<<'FEATURE'
Feature: Sessions can be viewed
    In order for participants to understand sessions
    They should show appropriate information

    Background:
      Given an event group
      And a course group
      And a session

    Scenario: Session view shows only primary group label
      Given the session is part of the event
      And the session is published
      And the session is referenced from a course paragraph that is published
      And I am logged in
      And I am a participant in the course
      And I am a participant in the event
      When I view the session
      Then I should see the event label
      And I should not see the course label

    Scenario: Session time is shown in venue timezone
      Given a venue in "America/New_York" timezone
      And the event has that venue
      And the session is part of the event
      And the stored session time is "2024-01-01T13:00:00"
      And I am logged in
      And I am a participant in the event
      Then the session timezone should be "America/New_York"
      When I view the session
      Then I should see the time "Mon 1 Jan 2024, 8am"

FEATURE;

  /**
   * @Given a session
   */
  public function aSessionWithMedia() {
    $this->node = $this->createEntity('node', ['type' => 'session', 'title' => $this->randomMachineName()]);

    $this->media = $this->createEntity('media', ['bundle' => 'audio']);
    $this->node->set('field_media', $this->media);
    $this->node->save();
  }


  /**
   * @Given the session is part of the event
   */
  public function theSessionIsPartOfTheEvent() {
    $this->event->addRelationship($this->node, 'group_node:session');
    // Assert the session is correctly defaulted to belonging primarily to this event.
    $content = $this->event->getRelationships('group_node:session');
    $content = reset($content);
    $this->assertEquals($this->node->id(), $content->getEntity()->id());
    $this->assertTrue((bool) $content->get('field_primary')->value);
  }

  /**
   * @Given the session is :published
   */
  public function theSessionIsPublished($published) {
    $status = $published === 'published';
    if ($status) {
      $this->node->setPublished()->save();
    }
    else {
      $this->node->setUnpublished()->save();
    }
  }

  /**
   * @Given the session is referenced from a course paragraph that is :published
   */
  public function theSessionIsReferencedFromACourseParagraph($published) {
    $this->paragraph = Paragraph::create([
      'type' => 'sessions',
      'field_sessions' => [$this->node],
    ]);
    $this->paragraph->setParentEntity($this->course, 'field_content');
    if($published === 'published') {
      $this->paragraph->setPublished();
    }
    else {
      $this->paragraph->setUnpublished();
    }
    $this->paragraph->save();
    $this->course->set('field_content', [$this->paragraph])->save();
  }

  /**
   * @Given a venue in :timezone timezone
   */
  public function aVenueInTimezone($timezone) {
    $this->venue = $this->createEntity('node', [
      'type' => 'venue',
      'title' => $this->randomMachineName(),
      'field_timezone' => $timezone,
    ]);
  }

  /**
   * @Given the event has that venue
   */
  public function theEventHasThatVenue() {
    $this->event->set('field_venue', $this->venue);
    $this->event->save();
    \Drupal::entityTypeManager()->getStorage('node')->resetCache([$this->event->id()]);
  }

  /**
   * @Given the stored session time is :datetime
   */
  public function theStoredSessionTimeIs($datetime) {
    $this->node->set('field_datetime', ['value' => $datetime]);
    $this->node->save();
  }

  /**
   * @When I view the session
   */
  public function iViewtheSession() {
    $this->drupalGet($this->node->toUrl()->toString());
  }

  /**
   * @Then I should see the event label
   */
  public function iShouldSeeTheEventLabel() {
    $this->assertSession()->pageTextContains($this->event->label());
  }

  /**
   * @Then I should not see the course label
   */
  public function iShouldNotSeeThecourseLabel() {
    $this->assertSession()->pageTextNotContains($this->course->label());
  }

  /**
   * @Then I should see the time :time
   */
  public function iShouldSeeTheTime($time) {
    $this->assertSession()->pageTextContains($time);
  }

  /**
   * @Then the session timezone should be :timezone
   */
  public function theSessionTimezoneShouldBe($timezone) {
    $session = $this->node;
    $this->assertSame($timezone, $session->getTimezone());
  }

}
