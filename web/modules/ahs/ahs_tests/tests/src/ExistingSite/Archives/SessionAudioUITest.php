<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Archives;

use Drupal\file\Entity\File;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\RetryTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\Tests\ahs_tests\Traits\FormTrait;

/**
 * @group js
 */
class SessionAudioUITest extends ExistingSiteJsBehatBase {

  use FormTrait;
  use UserDefinitions;
  use UserTrait;
  use SessionAudioTrait;
  use RetryTrait;
  use GroupTrait;
  use GroupDefinitions;

  /*
   * Retry any failing test, as the complex UI is prone to random
   * failures.
   */
  protected $retry = 3;

  /*
   * Hide node author and last saved date from visual regression screenshots.
   */
  protected $inVisualRegressionHide = ["div.entity-meta__header"];

  // The file extension to use.
  protected $ext = "mp3";

  protected $theSession;
  protected $createdFiles;
  protected $originalFilename;
  protected $pending;
  protected $sessionTitle;
  protected $filenames;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Session audio files can be selected using entity browser
    In order to see keep audio files organised
    They are managed within the session

    Background:
      Given an event group

    Scenario: Pending audio file added as session media for archivist
      Given I am logged in as an "archivist"
      Given a pending file on S3
      # Setting the time on the session form proved very hard, so title only
      When I am creating a new session
      And I add an audio media
      And I press "Select file"
      And I select the pending file in the entity browser
      And I save the session
      Then the session has an audio media and file named with the session title

    Scenario: Pending audio file added as session media for group admin
      Given I am logged in
      And I am an admin of the event
      Given a pending file on S3
      When I am creating a new session
      And I add an audio media
      And I press "Select file"
      And I select the pending file in the entity browser
      And I save the session
      Then the session has an audio media and file named with the session title

    Scenario: Adding two audio files
      Given I am logged in as an "archivist"
      Given 2 pending files on S3
      When I am creating a new session
      And I add an audio media
      And I press "Select file"
      And I select a pending file in the entity browser
      And I press "Create recording"
      And I add an audio media
      And I press "Select file"
      And I select a pending file in the entity browser
      And I press "Create recording"
      And I save the session
      Then the session has two audio media and files named with the session title

    Scenario: Editing media before saving should be possible
      Given I am logged in as an "archivist"
      Given a pending file on S3
      When I am creating a new session
      And I add an audio media
      And I press "Select file"
      And I select the pending file in the entity browser
      And I press "Create recording"
      And I press "Edit"
      Then I should see the selected file

    Scenario: Changing your mind while creating a session is fine
      Given I am logged in as an "archivist"
      Given a pending file on S3
      When I am creating a new session
      And I add an audio media
      And I press "Select file"
      And I select the pending file in the entity browser
      And I remove the file
      # "Select file" button is not reappearing after file removed, since gin installed,
      # so cancelling and restarting media is necessary
      And I press "Cancel"
      And I add an audio media
      And I press "Select file"
      And I select the pending file in the entity browser
      And I press "Create recording"
      And I remove the audio media
      And I add an audio media
      And I press "Select file"
      And I select the pending file in the entity browser
      And I save the session
      Then the session has an audio media and file named with the session title

    # Test fails weirdly with iframe not found
    #Scenario: Moving media from another session
    #  Given a session with an audio media and file
    #  When I am creating a new session
    #  And I press "Add existing recording"
    #  And I select the existing audio media in the entity browser
    #  And I save the session
    #  Then the session has an audio media and file named with the session title

    Scenario: Adding a managed file that has no media
      Given I am logged in as an "archivist"
      Given an audio file without a media
      When I am creating a new session
      And I add an audio media
      And I press "Select file"
      And I select the detached file in the entity browser
      And I save the session
      Then the session has an audio media and file named with the session title

    Scenario: Uploading audio file added as session media
      Given I am logged in as an "archivist"
      When I am creating a new session
      And I add an audio media
      And I press "Select file"
      And I upload new media in the entity browser
      And I save the session
      Then the session has an audio media and file named with the session title

FEATURE;

  public function setUp():void {
    parent::setUp();
    $this->setUpAudio();
  }

  public function tearDown():void {
    $this->tearDownAudio();
    parent::tearDown();
  }

  /**
   * @Given :count pending file(s) on S3
   */
  public function aPendingFileOnS3($count) {
    if ($count === 'a') {
      $count = 1;
    }
    for ($i=0; $i<$count;$i++) {
      $this->pending[] = $this->createFileOnS3("pending");
    }
  }

  /**
   * @When I am creating a new session
   */
  public function iAmCreatingANewSession() {
    $this->drupalGet("/group/{$this->event->id()}/content/create/group_node%3Asession");
    $this->assertSession()->pageTextContains('Add Group node (Session)');
    $page = $this->getSession()->getPage();
    $title_field = $page->find('css', "#edit-title-0-value");
    // Node titles are capitalised.
    $this->sessionTitle = ucfirst($this->randomMachineName());
    $title_field->setValue($this->sessionTitle);
  }

  /**
   * @When I add an audio media
   */
  public function iAddAnAudioMedia() {
    $page = $this->getSession()->getPage();
    $mediaType = $page->findField('field_media[actions][bundle]');
    $this->assertNotNull($mediaType, "There should be a new media bundle selector");
    $mediaType->selectOption('audio');
    $page->find('named', ['link_or_button', 'Add new recording'])->click();
  }

  /**
   * @When I press "Select file"
   */
  public function iPressSelectFile() {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->getSession()->getPage()->find('named', ['link_or_button', 'Select file'])->click();
  }

  /**
   * @When I press "Cancel"
   */
  public function iPressCancel() {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->getSession()->getPage()->find('named', ['link_or_button', 'Cancel'])->click();
  }

  /**
   * @When I select the/a pending file in the entity browser
   */
  public function iSelectTheFileInTheEntityBrowser() {
    $filename = $this->getPendingFilename();
    $path = "s3://audio/pending/$filename";
    $this->selectFileInAudioFileBrowser($path, 'files');
    $this->assertSession()->pageTextContains($filename);
  }

  /**
   * @When I press "Create recording"
   */
  public function iPressCreateRecording() {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->getSession()->getPage()->find('named', ['link_or_button', 'Create recording'])->click();
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
  }

  /**
   * @When I press "Add existing recording"
   */
  public function iPressAddExistingRecording() {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->getSession()->getPage()->find('named', ['link_or_button', 'Add existing recording'])->click();
  }

  /**
   * @When I save the session
   */
  public function iSaveTheSession() {
    $this->getSession()->getPage()->pressButton('Save');
    $this->assertSession()->pageTextNotContains('Create session');
    $this->assertSession()->pageTextContains("Session {$this->sessionTitle} has been created");
    $this->theSession = $this->loadEntityByProperties('node', ['type' => 'session', 'title' => $this->sessionTitle]);
  }

  /**
   * @Then the session has an audio media and file named with the session title
   */
  public function theSessionHasAnAudioMediaAndFileNamedWithTheSessionTitle() {
    $this->assertFalse($this->theSession->get('field_media')->isEmpty(), "The session should have media");
    $audios = $this->referencedEntitiesByBundle($this->theSession->get('field_media'))['audio'];
    $this->assertSame(1, count($audios), "There should only be 1 audio media referenced in field_media");
    $media = $this->theSession->get('field_media')->entity;
    $this->assertAudioMediaAndFileNamed($media, $this->theSession->getTitle());
  }

  /**
   * @Then the session has two audio media and files named with the session title
   */
  public function theSessionHasTwoAudioMediaAndFilesNamedWithTheSessionTitle() {
    $this->assertFalse($this->theSession->get('field_media')->isEmpty(), "The session should have media");
    $audios = $this->referencedEntitiesByBundle($this->theSession->get('field_media'))['audio'];
    $this->assertCount(2, $audios, "There should be two audios referenced form field_media");
    $this->assertAudioMediaAndFilesNamedAsAPair($audios, $this->theSession->getTitle());
  }

  protected function getPendingFilename() {
    $filename = reset($this->pending);
    unset($this->pending[0]);
    $this->filenames[] = $filename;
    return $filename;
  }

  /**
   * @When I press "Edit"
   */
  public function iPressEdit() {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $ief = $this->getSession()->getPage()->find('css', 'div.field--name-field-media');
    $ief->find('named', ['link_or_button', 'Edit'])->click();
  }

  /**
   * @Then I should see the selected file
   */
  public function iShouldSeeTheSelectedFile() {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->assertSession()->pageTextNotContains('No files yet');
    $this->assertSession()->pageTextContains($this->filenames[0]);
  }

  protected function findEntityBrowser($name) {
    sleep(5);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $iframe = $this->assertSession()->waitForElementVisible('named', ['id_or_name', "entity_browser_iframe_$name"]);
    $this->assertNotEmpty($iframe, "The entity browser iframe should be present");

    // Repeatedly try to enter the iframe. Sometimes switching to the iframe
    // weirdly fails and we silently remain on the outer frame.
    for ($i=0; $i < 10; $i++) {
      $this->getSession()->switchToIFrame("entity_browser_iframe_$name");

      $browser = $this->waitForEntityBrowserForm($name);
      if ($browser) {
        break;
      }
      $this->getSession()->switchToIFrame();
      sleep(3);
      $this->getSession()->switchToIFrame("entity_browser_iframe_$name");
    }
    $this->assertNotNull($browser, "The entity browser form should be present");
    return $browser;

  }

  protected function waitForEntityBrowserForm($name) {
    $identifier = \Drupal\Component\Utility\Html::cleanCssIdentifier($name);
    $form = $this->assertSession()->waitForElementVisible('css', "form#entity-browser-$identifier-form", 3000);
    return $form;
  }

  /**
   * @When I select the existing audio media in the entity browser
   */
  public function iSelectAnExistingAudioMediaInTheEntityBrowser() {
    $name = $this->audioMedia[0]->getName();
    $id = $this->audioMedia[0]->id();

    $this->getSession()->switchToIFrame();
    $browser = $this->findEntityBrowser("session_media");
    $checkbox = $browser->findField("entity_browser_select[media:$id]");
    $this->assertNull($checkbox, "The media checkbox should not be displayed initially");
    $this->assertFalse(strpos($browser->getText(), $name), "The media name should not be displayed initially");

    $filter = $browser->findField("name");
    $this->assertNotNull($filter, "The media name views filter should be exposed");
    $filter->setValue(substr($name, 0, 6));
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}

    $checkbox = $browser->findField("    entity_browser_select[media:$id]");
    $this->assertNotNull($checkbox, "There should be a checkbox for the audio media");
    $checkbox->click();
    $this->getSession()->getPage()->find('named', ['link_or_button', 'Move media'])->click();

    // Assert that the media is selected on the session form.
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->getSession()->switchToIFrame();
    sleep(3);
    $this->assertEmpty($this->getSession()->getPage()->find('named', ['id_or_name', 'entity_browser_iframe_session_media']), "The entity browser modal should no longer be present.");
    $this->assertSession()->pageTextContains($name);
  }

  /**
   * @When I remove the file
   */
  public function iRemoveTheFile() {
    $audioField = $this->getSession()->getPage()->find('css', '.field--name-field-audio');
    $audioField->find('named', ['link_or_button', 'Remove'])->click();
    $this->pending[] = $this->filenames[0];
    $this->filenames = [];
  }

  /**
   * @When I remove the audio media
   */
  public function iRemoveTheAudioMedia() {
    $mediaField = $this->getSession()->getPage()->find('css', '.field--name-field-media');
    $mediaField->find('named', ['link_or_button', 'Remove'])->click();
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    // Confirmation is required.
    $mediaField->find('named', ['link_or_button', 'Remove'])->click();
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
  }

  /**
   * @Given an audio file without a media
   */
  public function anAudioFileWithoutAMedia() {
    $this->createManagedAudioFileOnS3("miscellaneous");
  }

  /**
 * @When I select the detached file in the entity browser
 */
  public function iSelectTheDetachedFileInTheEntityBrowser() {
    $fileId = $this->files[0]->id();
    $this->selectFileInAudioFileBrowser("file:$fileId", 'entity_browser_select', 'Detached');
    $this->assertSession()->pageTextContains($this->files[0]->getFilename());
  }

  protected function selectFileInAudioFileBrowser($option, $element, $tab = NULL) {
    // Wait for the s3 bucket to be queried.
    sleep(10);
    $browser = $this->findEntityBrowser("audio_pending");

    // Select the right widget if necessary.
    if ($tab) {
      $nav = $browser->find('css', 'nav');
      $this->assertNotNull($nav, "The navigation tabs should be present in the entity browser");
      $tabElement = $nav->find('named', ['link_or_button', $tab]);
      $this->assertNotNull($nav, "The '$tab' tab should be present in the entity browser");
      $isActiveTab = !is_null($nav->find('named', [
        'link_or_button',
        "$tab.active"
      ]));
      if (!$isActiveTab) {
        $tabElement->click();
        sleep(3);
        try {
          $this->assertSession()->assertWaitOnAjaxRequest();
        }
        catch (\RuntimeException $e) {}
        $browser = $this->waitForEntityBrowserForm("audio_pending");
        $nav = $browser->find('css', 'nav');
        $isActiveTab = is_null($nav->find('named', [
          'link_or_button',
          "$tab.active"
        ]));
        $this->assertTrue($isActiveTab, "The '$tab' tab should now be active");
      }
    }

    $options = $this->getRadioOptions($element, $browser);
    $this->assertContains($option, array_keys($options), "The file should be a radio button option");

    $this->selectRadio($element, $option, $browser);
    $browser->find('named', ['link_or_button', 'Add selected file'])->click();

    // Assert that the file is selected on the session form.
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->getSession()->switchToIFrame();
    sleep(3);
    $this->assertEmpty($this->getSession()->getPage()->find('named', ['id_or_name', 'entity_browser_iframe_audio_pending']), "The entity browser modal should no longer be present.");
  }


  /**
   * @Given a session
   */
  public function aSession() {
    $session = $this->getTheSession();
  }

  /**
   * @When I enter the same title as the existing session
   */
  public function iEnterTheSameTitleAsTheExistingSession() {
    $page = $this->getSession()->getPage();
    $title_field = $page->find('css', "#edit-title-0-value");
    $session = $this->getTheSession();
    $this->assertNotNull($session);
    $title_field->setValue($session->getTitle());
  }

  /**
   * @When I press Save
   */
  public function iPressSave() {
    $this->getSession()->getPage()->pressButton('Save');
  }

  /**
   * @Then I should see a unique session validation error
   */
  public function iShouldSeeAValidationError() {
    $title = $this->getTheSession()->getTitle();
    $message = "A session with the title '$title' and date 'unspecified' already exists.";
    $this->assertSession()->pageTextContains($message);
  }

  /**
   * @When I upload new media in the entity browser
   */
  public function iSelectAnUploadInTheEntityBrowser() {
    $file = $this->createMediaFile();
    $filePath = $file->getFileUri();
    $mediaPath = \Drupal::service('file_system')->realpath($filePath);

    if ($mediaPath === false) {
      throw new \RuntimeException(sprintf('Media file "%s" not found.', $filePath));
    }

    $browser = $this->findEntityBrowser("audio_pending");
    $browser->find('named', ['link_or_button', 'Upload'])->click();

    if (!$fileInput = $browser->find('named', ['field', 'files[upload]'])) {
      return;
    }

    // Attach new media file.
    $fileInput->attachFile($mediaPath);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    // Upload file.
    $this->getSession()->getPage()->find('css', 'input.is-entity-browser-submit')->click();
    sleep(3);
    // Create recording.
    $this->getSession()->getPage()->find('css', 'input.ief-entity-submit')->click();
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->getSession()->switchToIFrame();
  }

  /**
   * @Given I am an admin of the :bundle
   */
  public function iAmAnAdminOfTheGroup($bundle) {
    $membership_fields = ['field_participant_type' => '69']; // Student
    $this->{$bundle}->addMember($this->getMe(), $membership_fields);
    $membership = $this->{$bundle}->getMember($this->getMe())->getGroupRelationship();
    $membership->group_roles[] = "$bundle-admin";
    $membership->save();
    $this->assertGroupMember($this->getMe(), $this->{$bundle}, \Drupal\ahs_training\Entity\TrainingRecord::STUDENT_TERM_ID, ['admin']);
  }

  // Creates a new file in the public folder for test-passing use.
  protected function createMediaFile() {
    $filename = "file_" . $this->randomMachineName() . ".{$this->ext}";
    $uri = trim('public://' . $filename, "/");
    file_put_contents($uri, $this->randomString());
    $file = File::create([
      'uri' => $uri,
      'filename' => $filename,
    ]);
    $file->save();
    $this->markEntityForCleanup($file);

    return $file;
  }

}

