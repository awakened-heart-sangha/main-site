<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Archives;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\MockHttpClientTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use Drupal\user\Entity\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

/**
 * @group ClonedDatabaseRequired
 * @group todo
 */
class YoutubeAudioExtractTest extends ExistingSiteBehatBase {

  use SessionAudioTrait;
  use QueueTrait;
  use MockHttpClientTrait;

  // The file extension to use.
  protected $ext = "m4a";

  protected static $feature = <<<'FEATURE'
Feature: Youtube audio extraction
    In order to provide audio as well as video
    Audio is derived from youtube video

    Scenario: Adding an s3 file to a media
      Given a pipedream file on S3
      And a session with a date
      And the session has a youtube media with url
      And the session has a derived audio media
      And the audio media has no file
      Then the audio media name is the session date and title
      When the file becomes managed
      And the file is added to the audio media
      Then the filename and path is derived from the session date and title in the derived directory
      And the audio media name is the session date and title

    Scenario: Reference an s3 file using jsonapi
      Given a pipedream file on S3
      And a session with a date
      And the session has a youtube media with url
      And the session has a derived audio media
      When the file uri is posted by jsonapi
      Then the session has an audio media and file named with the session title and date in the derived directory

    Scenario: Derive audio from youtube
      Given a session with a date
      And the session has no audio media
      And the session has a youtube media with url
      Then an audio extraction is requested
      And the session has an audio media named with the session title and date
      When the audio extraction is completed
      Then the session has an audio media and file named with the session title and date in the derived directory
      And the audio media is marked as derived from the youtube media

    Scenario: Don't derive audio if an independent audio exists
      Given a session with a date
      And the session has an independent audio media
      And the session has a youtube media with url
      Then an audio extraction is not requested

    Scenario: Don't derive audio if a derived audio already exists
      Given a session with a date
      And the session has a youtube media with url
      Then an audio extraction is requested
      When the audio extraction is completed
      Then the session has an audio media and file named with the session title and date in the derived directory
      When the youtube media is renamed
      Then an audio extraction is not requested

    # Weird Guzzle mock errors occur because media tries to access thumbnail on youtube when url is changed.
    #Scenario: Rederive audio if a youtube url is changed
    #  Given a session with a date
    #  And the session has a youtube media with url
    #  Then an audio extraction is requested
    #  When the audio extraction is completed
    #  Then the session has an audio media and file named with the session title and date in the derived directory
    #  When the youtube media url is changed
    #  Then an audio extraction is requested

FEATURE;

  public function setUp():void {
    parent::setUp();
    $this->setUpQueues();
    $this->setUpSuccessfulMockHttpClient();
    $this->setUpAudio();
  }

  public function tearDown():void {
    $this->tearDownAudio();
    $this->tearDownQueues();
    parent::tearDown();
  }

  /**
   * @Given a pipedream file on S3
   */
  public function aPipedreamFileOnS3() {
    $this->createFileOnS3('pipedream');
  }

  /**
   * @Given the session has a youtube media with url
   */
  public function aSessionHasAYoutubeMediaWithUrl() {
    $fields = [
      'bundle' => 'youtube',
      'field_media_video_embed_field' => 'https://youtube.com/someurl',
    ];
    $media = $this->createMedia($fields);
    $this->youtubeMedia[] = $media;
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id'=> $media->id()]);
    $session->save();
    $this->theSession = $session;
  }

  /**
   * @Given the session has a derived audio media
   */
  public function aSessionHasAMedia() {
    $media = $this->createMedia([
      'bundle' => 'audio',
      'field_derived' => $this->youtubeMedia[0],
    ]);
    $this->audioMedia[] = $media;
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id'=> $media->id()]);
    $session->save();
    $this->theSession = $session;
  }

  /**
   * @Given the audio media has no file
   */
  public function theAudioMediaHasNoFile() {
    $session = $this->getTheSession();
    $media = $this->referencedEntitiesByBundle($session->get('field_media'))['audio'][0];
    $this->assertNull($media->getSource()->getSourceFieldValue($media));
  }

  /**
   * @When the file becomes managed
   */
  public function theFileBecomesManaged() {
    $fileStorage = $this->entityTypeManager->getStorage('file');
    $file = $fileStorage->create(['uri' => $this->createdFiles[0]]);
    // The file is not set permanent at this point. This is set automatically by core when
    // the file is referenced from another entity.
    $file->save();
    $this->files[] = $file;
    $this->markEntityForCleanup($file);
  }

  /**
   * @When the file is added to the audio media
   */
  public function theFileIsAddedToTheAudioMedia() {
    $session = $this->getTheSession();
    /** @var \Drupal\Media\MediaInterface $media */
    $media = $this->referencedEntitiesByBundle($session->get('field_media'))['audio'][0];
    $field_name = $media->getSource()->getSourceFieldDefinition($media->bundle->entity)->getName();
    $media->set($field_name, $this->getTheFile());
    $media->save();
  }

  /**
   * @Given the session has no :type media
   */
  public function theSessionHasNoMedia($type) {
    $session = $this->getTheSession();
    $media = $this->referencedEntitiesByBundle($session->get('field_media'));
    $this->assertNotContains('audio', $media, "There should be no audio media on the session");
  }

  /**
   * @Then an audio extraction is requested
   */
  public function anAudioExtractionIsRequested() {
    $leader = $this->createNamedUser($this->randomMachineName());
    $this->getTheSession()->set('field_leader', $leader)->save();

    // Reset http request count to zero.
    $this->getNewHttpRequests();
    $this->processAdvancedQueue('youtube_audio_extract');
    $requests = $this->getNewHttpRequests();
    $this->assertCount(1, $requests, "There should be exactly 1 new http request. " . $this->prettyPrintHttpRequests($requests));
    $json = json_decode($requests[0]->getBody(), true);
    $this->assertNotEmpty($json);

    $audios = $this->referencedEntitiesByBundle($this->getTheSession()->get('field_media'))['audio'];
    $this->assertCount(1, $audios, "There should be one audio media on the session");
    $audio = reset($audios);
    $this->assertSame($audio->uuid(), $json['audio_uuid'], "Audio uuid supplied in request should be audio meia uuid");
    $this->assertEquals($leader->getDisplayName(), $json['leader'], "Leader display name should be included in request");
  }

  /**
   * @Then an audio extraction is not requested
   */
  public function anAudioExtractionIsNotRequested() {
    // Reset http request count to zero.
    $this->getNewHttpRequests();
    $this->processAdvancedQueue('youtube_audio_extract');
    $requests = $this->getNewHttpRequests();
    $this->assertCount(0, $requests, "There should be no new http requests. " . $this->prettyPrintHttpRequests($requests));
  }

  /**
   * @When the audio extraction is completed
   */
  public function theAudioExtractionIsCompleted() {
    // Simulate file being created on s3 by pipedream
    $filename = $this->createFileOnS3('pipedream');
    $uri = "{$this->audioRoot}/pipedream/$filename";
    $this->postFileReferenceByJsonApi($uri);
  }

  /**
   * @When the file uri is posted by jsonapi
   */
  public function theFileIsPostedByJsonApi() {
    $this->postFileReferenceByJsonApi($this->createdFiles[0]);
  }


  /**
   * Simuates jsonapi requests at end of pipedream audio extraction workflow
   *
   * @param $uri
   *   The uri of file created on s3 by pipedream.
   */
  protected function postFileReferenceByJsonApi($uri) {
    $audios = $this->referencedEntitiesByBundle($this->getTheSession()->get('field_media'))['audio'];
    $this->assertCount(1, $audios, "There should be one audio media on the session");
    $audio = reset($audios);

    $users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['mail' => 'pipedream@noemail.ahs.org.uk']);
    $user = reset($users); // Get the first user matching the username.
    $this->assertNotNull($user, 'There should be a pipedream user');
    $apiKey = $user->get('api_key')->value;

    $this->restoreRealHttpClient();
    $client = \Drupal::httpClient();

    $headers = [
      'api-key' => $apiKey,
      'Content-Type' => 'application/vnd.api+json',
      'Accept' => 'application/vnd.api+json',
    ];

    $fileCreationUrl = Url::fromUri('base:/jsonapi/file/file')->setAbsolute()->toString();
    $mediaUpdateUrl = Url::fromUri('base:/jsonapi/media/audio/' . $audio->uuid())->setAbsolute()->toString();;

    // Create file entity
    try {
      $response = $client->post($fileCreationUrl, [
        'headers' => $headers,
        'json' => [
          'data' => [
            'type' => 'file--file',
            'attributes' => [
              'uri' => $uri,
              'filemime' => 'audio/mp4',
              'filesize' => 12345, // Dummy size.
            ],
            'relationships' => [
              'uid' => [
                'data' => [
                  'type' => 'user--user',
                  'id' => $user->uuid(),
                ],
              ],
            ],
          ],
        ],
      ]);
    } catch (RequestException $e) {
      $this->prettyRequestFail($e);
    }

    $body = json_decode($response->getBody());
    $fileUuid = $body->data->id;
    $this->assertNotNull($fileUuid);

    // Update media entity.
    try {
      $client->patch($mediaUpdateUrl, [
        'headers' => $headers,
        'json' => [
          'data' => [
            'type' => 'media--audio',
            'id' => $audio->uuid(),
            'status' => 1,
            'relationships' => [
              'field_audio' => [
                'data' => [
                  'type' => 'file--file',
                  'id' => $fileUuid,
                ],
              ],
            ],
          ],
        ],
      ]);
    } catch (RequestException $e) {
      $this->prettyRequestFail($e);
    }
  }

  protected function prettyRequestFail(RequestException $e) {
    $message = "Client error: request failed with. ";
    if ($response = $e->getResponse()) {
      $message .= $response->getReasonPhrase();
      $responseBody = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      if (isset($responseBody['errors'])) {
        $simple_errors = array_map(function($error) { unset($error['meta']); return $error; }, $responseBody['errors']);
        $errors = print_r($simple_errors, TRUE);
        $message = "Errors: {$errors}";
      }
    }
    $this->fail($message);
  }


  /**
   * @Then the session has an audio media named with the session title and date
   */
  public function theSessionHasAnAudioMedia() {
    $session = $this->getTheSession();
    $audios = $this->referencedEntitiesByBundle($session->get("field_media"))['audio'];
    $this->assertCount(1, $audios, "There should only be one audio media on the session");
    $audio = reset($audios);
    $audio = $this->reloadEntity($audio);
    $name = $this->date['readable'] . " " . $session->getTitle();
    $this->assertSame($name, $audio->getName(), "The audio media should be named properly.");
  }

  /**
   * @Then the session has an audio media and file named with the session title and date in the derived directory
   */
  public function theSessionHasAnAudioMediaAndFile() {
    $session = $this->getTheSession();
    $audios = $this->referencedEntitiesByBundle($session->get("field_media"))['audio'];
    $this->assertCount(1, $audios, "There should only be one audio media on the session");
    $audio = reset($audios);
    $name = $this->date['readable'] . " " . $session->getTitle();
    $this->assertAudioMediaAndFileNamed($audio, $name, "derived/" . $this->date['year']);
  }

  /**
   * @Then the audio media is marked as derived from the youtube media
   */
  public function theAudioMediaIsMarkedAsDerivedFromTheYoutubeMedia() {
    $session = $this->getTheSession();
    $audios = $this->referencedEntitiesByBundle($session->get("field_media"))['audio'];
    $this->assertCount(1, $audios, "There should only be one audio media on the session");
    $audio = reset($audios);
    $this->assertTrue($audio->isPublished(), "The audio media should be published");
    $youtubes = $this->referencedEntitiesByBundle($session->get('field_media'))['youtube'];
    $this->assertCount(1, $youtubes, "There should only be one youtube media on the session");
    $youtube = reset($youtubes);
    $this->assertSame($youtube->id(), $audio->get('field_derived')->target_id, "The audio should be marked as derived from the audio.");
  }

  /**
   * @Given the session has an independent audio media
   */
  public function aSessionHasAnIndependentAudioMedia() {
    $media = $this->createMedia(['bundle' => 'audio']);
    $this->assertTrue($media->get('field_derived')->isEmpty());
    $this->audioMedia[] = $media;
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id'=> $media->id()]);
    $session->save();
    $this->theSession = $session;
  }

  /**
   * @When the youtube media is renamed
   */
  public function theYoutubeMediaIsRenamed() {
    $this->youtubeMedia[0]->setName($this->randomMachineName())->save();
  }

  /**
   * @When the youtube media url is changed
   */
  public function theYoutubeMediaUrlIsChanged() {
    $this->youtubeMedia[0]->set('field_media_video_embed_field', "https://youtube.com/" . $this->randomMachineName())->save();
  }

  /**
   * @Then the filename and path is derived from the session date and title in the derived directory
   */
  public function theFilenameIsDerivedFromTheSessionDateAndTitleInTheDerivedDirectory() {
    $basename = $this->date['readable'] . " " . $this->getTheSession()->getTitle();
    $this->assertFilePath($basename, "derived/" . $this->date['year']);
  }

}

