<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Archives;

use Behat\Gherkin\Node\TableNode;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;

class SessionAudioTest extends ExistingSiteBehatBase {

  // The file extension to use.
  protected $ext = "mp3";

  use SessionAudioTrait;

  protected $originalSession;

  protected static $feature = <<<'FEATURE'
Feature: Audio files are named after sessions
    In order to see keep audio files organised
    Audio file names & paths are based on session dates and titles

    Scenario: Media is named after filename or session title
      Given a pending file on S3
      When an audio media is created with the file as its source
      Then the audio media name is the filename
      And the file is in the "miscellaneous" directory
      When the audio media is added to a session
      Then the audio media name is the session title
      And the filename is derived from the session title
      And the file is still in the "miscellaneous" directory

    Scenario: Punctuation in session title is not converted to html characters in filename
      Given a pending file on S3
      And an audio media with the file as its source
      And a session with an ampersand in its title
      When the audio media is added to the session
      Then the audio media name is literally the session title
      And the filename is derived literally from the session title

    Scenario: Invalid punctuation in session title is removed in media and file names
      Given a pending file on S3
      And an audio media with the file as its source
      And a session with invalid punctuation in its title
      When the audio media is added to the session
      Then the audio media name is the session title without invalid punctuation
      And the filename is derived from the session title without invalid punctuation

    Scenario: Session date is used in media and file names
      Given a pending file on S3
      And an audio media with the file as its source
      And a session with a date
      When the audio media is added to the session
      Then the audio media name is the session date and title
      And the filename and path is derived from the session date and title

    Scenario: Media and file names are updated when session title changes
      Given an audio media with a file as its source
      When the audio media is added to a session
      And the session title is changed
      Then the audio media name is the new session title
      And the filename is derived from the new session title

    Scenario: Multiple audio media are named sequentially
      Given two audio media with files
      When both audio media are added to a session
      Then the audio media and files are named as a pair

    Scenario: Youtube media are not considered when naming audio media sequentially
      Given two audio media with files
      And a session with a youtube media
      When an audio media is added to the session
      Then the audio media and file are named as a singleton
      When another youtube media is added to the session
      Then the audio media and file are still named as a singleton
      When the other audio media is added to the session
      Then the audio media and files are named as a pair
      When another youtube media is added to the session
      Then the audio media and files are still named as a pair

    Scenario: Media and audio files can only belong to one session
      Given a session with an audio media and file
      When the audio media is added to another session
      Then the audio media is removed from the original session
      And the audio media name is the new session title
      And the filename is derived from the new session title

    Scenario: Session media are sorted on the session
      Given a session with an audio media and file
      When a youtube media is added to the session
      Then the youtube media is referenced before the audio media

    Scenario: Deleting a session
      Given a session with an audio media and file
      When the session is deleted
      Then the audio media name is derived from the original filename and the session
      And the filename is derived from the original filename and the session

    Scenario: Removing audio media from session
      Given a session with an audio media and file
      When the audio media is removed from the session
      Then the audio media name is derived from the original filename and the session
      And the filename is derived from the original filename and the session

    Scenario: Deleting session audio media
      Given a session with an audio media and file
      When the audio media is removed from the session and deleted
      Then the filename is derived from the original filename and the session

    Scenario: Deleting session audio media
      Given a session with an audio media and file
      When the audio media is deleted and removed from the session
      Then the filename is derived from the original filename and the session

    Scenario: Removing file from audio media
      Given a session with an audio media and file
      When the file is removed from the audio media
      Then the filename is derived from the original filename and the session

    #Scenario Outline: Duplicate session titles are not allowed
    #  Given a session without a date
    #  Then another session with "<title>" title and "<date>" date may be allowed: "<allowed>"
    #
    # Examples:
    #  | title     | date | allowed |
    #  | same      | no   | no      |
    #  | same      | yes  | yes     |
    #  | different | no   | yes     |
    #  | different | yes  | yes     |

    #Scenario Outline: Duplicate session titles and dates are not allowed
    #  Given a session with a date
    #  Then another session with "<title>" title and "<date>" date may be allowed: "<allowed>"
    #
    #  Examples:
    #  | title     | date      | allowed |
    #  | same      | same      | no      |
    #  | same      | different | yes     |
    #  | same      | none      | yes     |
    #  | different | same      | yes     |
    #  | different | different | yes     |
    #  | different | none      | yes     |

FEATURE;

  public function setUp():void {
    parent::setUp();
    $this->setUpAudio();
  }

  public function tearDown():void {
    $this->tearDownAudio();
    parent::tearDown();
  }

  /**
   * @Given a pending file on S3
   */
  public function aPendingFileOnS3() {
    $this->createPendingManagedAudioFile();
  }


  /**
   * @Given an audio media with the/a file as its source
   * @When an audio media is created with the/a file as its source
   */
  public function anAudioMediaWithTheFileAsItsSource() {
    $file = $this->getTheFile() ?? $this->createPendingManagedAudioFile();
    $this->audioMedia[] = $this->createAudioMediaWithFile($file);
  }

  /**
   * @Then the audio media name is the filename
   */
  public function theAudioMediaNameIsTheFilename() {
    $filename = $this->getTheFile()->getFilename();
    $filename_without_extension = substr($filename, 0, strrpos($filename, "."));
    $this->assertSame($this->audioMedia[0]->getName(), $filename_without_extension);
  }

  /**
   * @Then the file is (still )in the :directory directory
   */
  public function theFileIsInTheDirectory($directory) {
    $file = $this->getTheFile();
    $this->assertManagedFileAtUri("{$this->audioRoot}/$directory/" . $file->getFilename(), $file);
  }

  /**
   * @When an/the audio media is added to a/the session
   */
  public function theAudioMediaIsAddedToASession() {
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id' => $this->audioMedia[0]->id()]);
    $session->save();
  }

  /**
   * @Then the filename is derived from the session title without invalid punctuation
   */
  public function theFilenameIsDerivedFromTheSessionTitleWithoutInvalidPunctuation() {
    $title = "Start - 'middle' end";
    $this->assertFilePath($title, 'miscellaneous');
  }

  /**
   * @Given a session with an ampersand in its title
   */
  public function aSessionWithAnAmpersandInItsTitle() {
    $session = $this->getTheSession();
    $session->set('title', "{$this->randomMachineName()} & {$this->randomMachineName()}");
    $session->save();
  }

  /**
   * @Given a session with invalid punctuation in its title
   */
  public function aSessionWithInvalidPunctuationInItsTitle() {
    $session = $this->getTheSession();
    $session->set('title', '*start: "middle" / <end>? ' . '\\');
    $session->save();
  }

  /**
   * @Then the audio media name is (literally )the (new )session title
   */
  public function theAudioMediaNameIsTheSessionTitle() {
    $title = $this->getTheSession()->getTitle();
    $name = $this->reloadEntity($this->audioMedia[0])->getName();
    $this->assertSame($title, $name);
  }

  /**
   * @Then the audio media name is the session title without invalid punctuation
   */
  public function theAudioMediaNameIsTheSessionTitleWithoutInvalidPunctuation() {
    $name = $this->reloadEntity($this->audioMedia[0])->getName();
    $this->assertSame("Start - 'middle' end", $name);
  }

  /**
   * @When the session title is changed
   */
  public function theSessionTitleIsChanged() {
    $this->getTheSession()->set('title', $this->randomMachineName())->save();
  }

  /**
   * @Given two audio media with files
   */
  public function twoAudioMediaWithFiles() {
    $this->audioMedia[] = $this->createAudioMediaWithFile();
    $this->audioMedia[] = $this->createAudioMediaWithFile();
  }

  /**
   * @When both audio media are added to a/the session
   */
  public function bothAudioMediaAreAddedToASession() {
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id' => $this->audioMedia[0]->id()]);
    $session->get('field_media')->appendItem(['target_id' => $this->audioMedia[1]->id()]);
    $session->save();
  }

  /**
   * @Given a session with a youtube media
   * @When a(nother) youtube media is added to the session
   */
  public function aSessionWithAYoutubeMedia() {
    $media = $this->createMedia(['bundle' => 'youtube', 'field_media_video_embed_field' => 'https://www.youtube.com/watch?v=shoVsQhou-8']);
    $media->save();
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id'=> $media->id()]);
    $session->save();
  }

  /**
   * @When the other audio media is added to the session
   */
  public function theOtherAudioMediaIsAddedToTheSession() {
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id' => $this->audioMedia[1]->id()]);
    $session->save();
  }

  /**
   * @Then the audio media and file are (still )named as a singleton
   */
  public function theAudioMediaAndFileAreNamedAsASingleton() {
    $this->theAudioMediaNameIsTheSessionTitle();
    $this->theFilenameIsDerivedFromTheSessionTitle();
  }

  /**
   * @Then the audio media and files are (still )named as a pair
   */
  public function theAudioMediaAndFilesAreNamedAsAPair() {
    $this->assertAudioMediaAndFilesNamedAsAPair($this->audioMedia, $this->getTheSession()->getTitle(), 'miscellaneous', $this->files);
  }

  /**
   * @When the audio media is added to another session
   */
  public function theAudioMediaIsAddedToAnotherSession() {
    $this->originalSession = $this->theSession;
    $this->theSession = NULL;
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id'=> $this->audioMedia[0]->id()]);
    $session->save();
    $this->theSession = $session;
  }

  /**
   * @Then the audio media is removed from the original session
   */
  public function theAudioMediaIsRemovedFromTheOriginalSession() {
    $session = $this->reloadEntity($this->originalSession);
    $this->assertTrue($session->get('field_media')->isEmpty());
  }

  /**
   * @Then the youtube media is referenced before the audio media
   */
  public function theYoutubeMediaIsReferencedBeforeTheAudioMedia() {
    $session = $this->reloadEntity($this->theSession);
    $fieldItems = $session->get('field_media');
    $this->assertTrue($fieldItems->get(0)->entity->bundle() === 'youtube');
    $this->assertTrue($fieldItems->get(1)->entity->bundle() === 'audio');
  }

  /**
   * @When the audio media is removed from the session
   */
  public function theAudioMediaIsRemovedFromTheSession() {
    $session = $this->getTheSession();
    $session->get('field_media')->setValue([]);
    $session->save();
  }

  /**
   * @When the audio media is removed from the session and deleted
   */
  public function theAudioMediaIsRemovedFromTheSessionAndDeleted() {
    $session = $this->getTheSession();
    $session->get('field_media')->setValue([]);
    $session->save();
    $media = $session->get('field_media')->entity;
    $media->delete();
  }

  /**
   * @When the audio media is deleted and removed from the session
   */
  public function theAudioMediaIsDeletedAndRemovedFromTheSession() {
    $session = $this->getTheSession();
    $media = $session->get('field_media')->entity;
    $media->delete();
    $session->get('field_media')->setValue([]);
    $session->save();
  }

  /**
   * @When the file is removed from the audio media
   */
  public function theFileIsRemovedFromTheAudioMedia() {
    $media = $this->reloadEntity($this->audioMedia[0]);
    $media->get('field_audio')->setValue([]);
    $media->save();
  }

  /**
   * @When the session is deleted
   */
  public function theSessionIsDeleted() {
    $session = $this->getTheSession();
    $session->delete();
  }

  /**
   * @Then the audio media name is derived from the original filename and the session
   */
  public function theAudioMediaNameIsDerivedFromTheOriginalFilenameAndTheSession() {
    $name = $this->reloadEntity($this->audioMedia[0])->getName();
    $this->assertSame($this->buildDetachedName(), $name);
  }

  /**
   * @Then the filename is derived from the original filename and the session
   */
  public function theFilenameIsDerivedFromTheOriginalFilenameAndTheSession() {
    $name = $this->buildDetachedName();
    $this->assertFilePath($name, 'miscellaneous');
  }

  /**
   * Construct what the name should be for a file detached from a session.
   *
   * @return string
   *   The name.
   */
  protected function buildDetachedName() {
    // Don't use getTheSession() because reloading a deleted session won't work.
    $title = $this->theSession->getTitle();
    $original = explode(".", $this->originalFilename)[0];
    return "Unknown - was $title (originally $original)";
  }

  /**
   * @Given a session without a date
   */
  public function aSessionWithoutADate() {
    $session = $this->getTheSession();
  }

  /**
   * @Then another session with :title title and :date date may be allowed: :allowed
   */
  public function anotherSessionValidity($title, $date, $allowed) {
    $previousSession = $this->getTheSession();
    $fields = [
      'type' => 'session',
      'title' => $this->randomMachineName(),
    ];

    if ($title === 'same') {
      $fields['title'] = $previousSession->getTitle();
    }

    if ($date === 'same') {
      $fields['field_datetime'] = $previousSession->get('field_datetime')->date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    }
    elseif($date === 'yes' || $date === 'different') {
      $fields['field_datetime'] = '1025-06-20T17:00:00';
    }

    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $session = $storage->create($fields);
    $isValid = $allowed === 'yes' ? TRUE : FALSE;
    $this->assertSessionValidation($session, $isValid);

    // The AhsArchivesSessionLabelUnique validator uses the id of the session
    // so potentially behaves differently with new/existing sessions.
    $session->save();
    $this->assertSessionValidation($session, $isValid);

    $this->markEntityForCleanup($session);
  }

  /**
   * Assert that a session is valid or invalid.
   *
   * @param \Drupal\node\NodeInterface $session
   *   The session to be validated.
   * @param bool $isValid
   *   Whether the session should be valid
   */
  protected function assertSessionValidation(NodeInterface $session, $isValid) {
    $violations = $session->validate();
    $messages = [];
    foreach ($violations as $violation) {
      $messages[$violation->getPropertyPath()] = (string) $violation->getMessage();
    }
    $message = empty($messages) ? "No validation violations founds" : count($violations) . "Unexpected violations: " . print_r($messages, TRUE);
    $this->assertSame($isValid, empty($messages), $message);
  }

  /**
   * @Then the filename and path is derived from the session title
   */
  public function theFilenameIsDerivedFromTheSessionTitle() {
    $this->assertFilePath($this->getTheSession()->getTitle(), 'miscellaneous');
  }

  /**
   * @Then the filename and path is derived from the session date and title
   */
  public function theFilenameIsDerivedFromTheSessionDateAndTitleInTheDerivedDirectory() {
    $basename = $this->date['readable'] . " " . $this->getTheSession()->getTitle();
    $this->assertFilePath($basename, $this->date['year']);
  }

}

