<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Archives;

use Drupal\Component\Serialization\Json;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

class MediaTranscriptTest extends ExistingSiteBehatBase {

  use SessionAudioTrait;
  use QueueTrait;

  // The audio file extension to use.
  protected $ext = "mp3";

  /**
   * History of requests/responses.
   *
   * @var array
   */
  protected $mockClientHistory = [];

  /**
   * Mock client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $mockClient;

  protected $theSession;
  protected $youtubeMedia;

  protected static $feature = <<<'FEATURE'
Feature: Session media have transcripts
    In order to provide transcripts of recordings
    Transcript nodes are automatically created for every session media

    Scenario: Audio media have transcripts
      Given a discussion session
      And the session has an audio media
      Then every media has a transcript with the session title

    Scenario: Youtube media have transcripts
      Given a discussion session
      And the session has a youtube media
      Then every media has a transcript with the session title

    Scenario: Multiple media have transcripts
      Given a discussion session
      And the session has an audio media
      And the session has a youtube media
      And the session has an audio media
      Then every media has a transcript with the session title

#    Scenario: Transcript requested
#      Given the remote API is up
#      And a discussion session
#      And the session has a youtube media
#      When the transcript is requested
#      Then a single request has been dispatched
#      And the queue runs
#      Then a single request has been dispatched

#    Scenario: Transcript queued
#      Given the remote API is up
#      And a discussion session
#      And the session has a youtube media
#      When the transcript is queued
#      And the queue runs
#      Then a single request has been dispatched

#    Scenario: Teaching have transcripts requested automatically
#      Given the remote API is up
#      And a teaching session
#      And the session has a youtube media
#      And the queue runs
#      Then a single request has been dispatched

FEATURE;

  protected function setUp():void {
    parent::setUp();
    $this->setUpQueues();
    $this->setUpAudio();
  }

  public function tearDown():void {
    $this->tearDownAudio();
    $this->tearDownQueues();
    parent::tearDown();
  }

  /**
   * @Given a discussion session
   */
  public function aDiscussionSession() {
    $session = $this->getTheSession();
    $session->set('field_session_type', 72)->save();
  }

  /**
   * @Given a teaching session
   */
  public function aTeachingSession() {
    $session = $this->getTheSession();
    $session->set('field_session_type', 52)->save();
  }

  /**
   * @Then every media has a transcript with the session title
   */
  public function everyMediaHasATranscript() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $media_ids = array_column($this->getTheSession()->get('field_media')->getValue(), 'target_id');
    $transcripts = $nodeStorage->loadByProperties(['type' => 'transcript', 'field_recording' => $media_ids]);
    $this->assertCount(count($media_ids), $transcripts, "Every media should have a transcript");
    foreach($transcripts as $transcript) {
      $this->assertSame($this->getTheSession()->getTitle(), $transcript->getTitle());
    }
  }

  /**
   * @When the transcript is requested
   */
  public function theTranscriptIsRequested() {
    $transcript = $this->getTranscript();
    $transcript->get('field_state')->first()->applyTransitionById('request')->save();
  }

  /**
   * @When the transcript is queued
   */
  public function theTranscriptIsQueued() {
    $transcript = $this->getTranscript();
    $transcript->get('field_state')->applyTransitionById('request')->save();
  }

  /**
   * @When the queue runs
   */
  public function theQueueRuns() {
    $this->processAdvancedQueue('transcription_request');
  }

  /**
   * @Given the remote API is up
   */
  public function theRemoteApiIsUp() {
    $success = ['200', []];
    $responseStubs = array_fill(0,10, $success);
    $this->setMockClient($responseStubs);
  }

  /**
   * @Given the remote API is down
   */
  public function theRemoteApiIsDown() {
    $result = ['429', []];
    $responseStubs = array_fill(0,10, $result);
    $this->setMockClient($responseStubs);
  }

  /**
   * @Then a single request has been dispatched
   */
  public function aSingleRequestHasBeenDispatched() {
    $this->assertCount(1, $this->mockClientHistory);
  }

  /**
   * Get the transcript for the first media in the session.
   */
  protected function getTranscript() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $media = $this->getTheSession()->get('field_media')->first()->entity;
    $transcripts = $nodeStorage->loadByProperties(['type' => 'transcript', 'field_recording' => $media->id()]);
    $transcript = reset($transcripts);
    return $transcript;
  }

  /**
   * Mocks the http-client.
   */
  protected function setMockClient($responseStubs) {
    // Convert the simple response data into proper objects.
    $responses = [];
    foreach ($responseStubs as $responseStub) {
      $responses[] = new Response((string)$responseStub[0], [], Json::encode($responseStub[1]));
    }

    // Create a mock and stack responses.
    $mock = new MockHandler($responses);
    $handler_stack = HandlerStack::create($mock);
    $history = Middleware::history($this->mockClientHistory);
    $handler_stack->push($history);
    $this->mockClient = new Client(['handler' => $handler_stack]);
    $this->container->set('http_client', $this->mockClient);
  }

}

