<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Archives;

use \Drupal\Core\Site\Settings;
use Drupal\file\FileInterface;

trait SessionAudioTrait {

  protected $audioRoot = "s3://audio";

  // Stored date is in UT timezone, readable date is in BST for Europe/London.
  // @see _ahs_archives_update_session_audio_name().
  protected $date = [
    'stored' => '2022-06-20T17:00:00',
    'year' => '2022',
    'readable' => '2022-06-20 1800'
  ];
  protected $audioMedia = [];
  protected $theSession;
  protected $files = [];
  protected $createdFiles;
  protected $originalFilename;
  protected $pending;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->assertNotEmpty(Settings::get('s3fs.secret_key'), "S3fs secret key should be set, typically via environment variable 'AHS_ARCHIVES_STORAGE_ACCOUNT_KEY'");
    $s3fsConfig = \Drupal::service('config.factory')->get('s3fs.settings')->get();
    \Drupal::service('s3fs')->refreshCache($s3fsConfig);
    \Drupal::service('file_system')->deleteRecursive($this->audioRoot);
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown(): void {
    // Cleanup entities before wiping file system, to prevent errors while
    // deleting managed file entities.
    $this->tearDownBase();

    // Wipe file system clean.
    $s3fsConfig = \Drupal::service('config.factory')->get('s3fs.settings')->get();
    \Drupal::service('s3fs')->refreshCache($s3fsConfig);
    \Drupal::service('file_system')->deleteRecursive($this->audioRoot);
  }

  /**
   * @inheritDoc
   */
  public function setUpAudio() {
    $this->assertNotEmpty(Settings::get('s3fs.secret_key'), "S3fs secret key should be set, typically via environment variable 'AHS_ARCHIVES_STORAGE_ACCOUNT_KEY'");
    $s3fsConfig = \Drupal::service('config.factory')->get('s3fs.settings')->get();
    \Drupal::service('s3fs')->refreshCache($s3fsConfig);
    \Drupal::service('file_system')->deleteRecursive($this->audioRoot);
  }

  /**
   * @inheritDoc
   */
  public function tearDownAudio() {
    // Cleanup entities before wiping file system, to prevent errors while
    // deleting managed file entities.
    $this->tearDownBase();

    // Wipe file system clean.
    $s3fsConfig = \Drupal::service('config.factory')->get('s3fs.settings')->get();
    \Drupal::service('s3fs')->refreshCache($s3fsConfig);
    \Drupal::service('file_system')->deleteRecursive($this->audioRoot);
  }

  /**
   * @Given a session with a date
   */
  public function aSessionWithADate() {
    $session = $this->getTheSession();
    $session->set('field_datetime', $this->date['stored']);
    $session->save();
  }

  /**
   * @Given the session has a/an :type media
   */
  public function aSessionHasAMedia($type) {
    $media = $this->createMedia(['bundle' => $type]);
    $var = "{$type}Media";
    $this->{$var}[] = $media;
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id'=> $media->id()]);
    $session->save();
    $this->theSession = $session;
  }

  /**
   * @Given a session with an audio media and file
   */
  public function aSessionWithAnAudioMediaAndFile() {
    $media = $this->createAudioMediaWithFile();
    $this->audioMedia[] = $media;
    $session = $this->getTheSession();
    $session->get('field_media')->appendItem(['target_id'=> $media->id()]);
    $session->save();
    $this->theSession = $session;
  }

  protected function assertManagedFileAtUri($uri, FileInterface $file) {
    $file = $this->reloadEntity($file);
    $files = \Drupal::service('file_system')->scanDirectory($this->audioRoot, '/.*/', ['recurse' => TRUE]);
    $files_message = "Files present in filesystem were: " . print_r(array_keys($files), TRUE);
    $this->assertSame($uri, $file->getFileUri(), "The managed file uri should be the expected file uri. $files_message");
    $filename = \Drupal::service('file_system')->basename($uri);
    $this->assertSame($filename, $file->getFilename(), "The managed file filename field should be the same as the basename of the managed file uri field. $files_message");
    $this->assertTrue(file_exists($uri), "A file should exist at the uri $uri. $files_message");
  }


  protected function assertAudioMediaAndFileNamed($media, $name, $directory = 'miscellaneous', $file = NULL) {
    $media = $this->reloadEntity($media);
    $this->assertSame('audio', $media->bundle(), "The media should be an audio media");
    $this->assertSame($name, $media->getName(), "The media should be named properly.");
    $this->assertFalse($media->get('field_audio')->isEmpty(), "The media should have a file");
    $referencedFile = $media->get('field_audio')->entity;
    $this->markEntityForCleanup($referencedFile);
    $uri = "{$this->audioRoot}/$directory/$name.{$this->ext}";
    $this->assertManagedFileAtUri($uri, $referencedFile);
    if ($file) {
      $file = $this->reloadEntity($file);
      $this->assertEquals($file->id(), $referencedFile->id(), "The expected file entity should be referenced on the audio media");
    }
  }

  protected function assertAudioMediaAndFilesNamedAsAPair(array $media, $name, $directory = 'miscellaneous', array $files = [NULL, NULL]) {
    $this->assertCount(2, $media, "There should be two media to form a pair");
    $this->assertAudioMediaAndFileNamed($media[0], "$name #1", $directory, $files[0]);
    $this->assertAudioMediaAndFileNamed($media[1], "$name #2", $directory, $files[1]);
  }

  protected function createAudioMediaWithFile($file = NULL) {
    if (empty($file)) {
      $file = $this->createPendingManagedAudioFile();
    }
    $media = $this->createMedia(['bundle' => 'audio']);
    $media = $this->addFileToAudioMedia($media, $file);
    return $media;
  }

  protected function addFileToAudioMedia($media, $file){
    $media->get('field_audio')->appendItem(['target_id' => $file->id()]);
    $media->save();
    return $media;
  }

  protected function getTheSession() {
    if (!isset($this->theSession)) {
      $nodeStorage = $this->entityTypeManager->getStorage('node');
      $this->theSession = $nodeStorage->create([
        'type' => 'session',
        'title' => "session" . $this->randomMachineName(),
      ]);
      $this->theSession->save();
    }
    $session = $this->reloadEntity($this->theSession);
    return $session;
  }

  protected function getTheFile() {
    if (isset($this->files[0])) {
      return $this->reloadEntity($this->files[0]);
    }
  }

  protected function createMedia($fields) {
    $mediaStorage = $this->entityTypeManager->getStorage('media');
    $media = $mediaStorage->create($fields);
    $media->save();
    $this->markEntityForCleanup($media);
    return $media;
  }

  protected function referencedEntitiesByBundle($fieldItemList) {
    $entitiesByBundle = [];
    foreach ($fieldItemList as $item) {
      $entitiesByBundle[$item->entity->bundle()][] = $item->entity;
    }
    return $entitiesByBundle;
  }

  protected function createFileOnS3($directory) {
    $root = "{$this->audioRoot}/$directory";
    $filename = "file" . $this->randomMachineName() . ".{$this->ext}";
    $uri = trim("$root/$filename", "/");
    file_put_contents($uri, $this->randomString());
    $this->createdFiles[] = $uri;
    return $filename;
  }

  protected function createManagedAudioFileOnS3($directory) {
    $this->originalFilename = $this->createFileOnS3($directory);
    $fileStorage = $this->entityTypeManager->getStorage('file');
    $file = $fileStorage->create(['uri' => "{$this->audioRoot}/{$directory}/{$this->originalFilename}"]);
    $file->setPermanent();
    $file->save();
    $this->markEntityForCleanup($file);
    $this->files[] = $file;
    return $file;
  }

  protected function createPendingManagedAudioFile() {
    $file = $this->createManagedAudioFileOnS3("pending");
    $this->pending[] = $file->getFilename();
    return $file;
  }

  /**
   * @Then the filename is derived (literally )from the (new )session title
   */
  public function theFilenameIsDerivedFromTheSession() {
    $this->assertFilePath($this->getTheSession()->getTitle(), 'miscellaneous');
  }

  protected function assertFilePath($basename, $directory) {
    $uri = "{$this->audioRoot}/$directory/$basename.{$this->ext}";
    $this->assertManagedFileAtUri($uri, $this->getTheFile());
  }

  /**
   * @Then the audio media name is the session date and title
   */
  public function theAudioMediaNameIsTheSessionDateAndTitle() {
    $title = $this->getTheSession()->getTitle();
    $name = $this->reloadEntity($this->audioMedia[0])->getName();
    $this->assertSame("{$this->date['readable']} $title", $name);
  }

}

