<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Archives;

use Drupal\Tests\ahs_tests\ExistingSite\Groups\EventsAdminListTest;

/**
 * @group NewDatabaseRequired
 */
class PastEventsListTest extends EventsAdminListTest {

  protected static $feature = <<<'FEATURE'
Feature: Events & archives staff can see past events
    In order to see past events
    Events & archives staff need an admin view

    Background:
      Given events:
        | title      | start date   | end date   | published |
        | Event1     | -3 weeks     | -2 weeks   | yes       |
        | Event2     | -10 weeks    | -8 weeks   | no        |
        | Event3     | -1 week      | +1 week    | yes       |
        | Event4     | +4 weeks     | +5 weeks   | yes       |

    Scenario Outline: Staff can see published & unpublished past events
      Given I am logged in as a "<role>"
      When I visit "admin/archives"
      Then I should see the following events:
        | title  |
        | Event3 |
        | Event1 |

      Examples:
        | role         |
        | events_admin |
        | archivist    |

FEATURE;

}

