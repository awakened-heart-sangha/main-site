<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;

/**
 * We need a new database because the select2 autocomplete searching
 * is tricky to test so we need to limit the number of existing experiences.
 *
 * @group NewDatabaseRequired
 * @group js
 */
class TrainingConditionsWidgetTest extends ExistingSiteJsBehatBase {

  use TrainingTrait;
  use FormTrait;

  protected $condition;
  protected $experienceTitle;

  /*
   * Hide node author and last saved date from visual regression screenshots.
   */
  protected $inVisualRegressionHide = ["div.entity-meta__header"];

  protected static $feature = <<<'FEATURE'
Feature: Trainings conditions widget
    In order for staff to specify conditions for experiences
    They need to reference training conditions that reference experiences

    Background:
      Given experiences A, B and C

    Scenario: Training condition on experiences
      Given I am logged in as a "training_admin"
      When I am creating an experience
      And I select "ExperienceB" as the experience for the condition
      And I save the experience
      Then the condition is stored as I intended
      When I edit the experience
      And I save the experience
      Then the condition is still stored as I intended
      When I edit the experience
      And I change the experience for the condition to "ExperienceC"
      And I save the experience
      Then the condition is still stored as I intended

    Scenario: Eligibility conditions on groups
      Given I am logged in as a "training_admin"
      When I am creating a group
      And I press "Add new eligibility condition"
      And I select "ExperienceB" as the experience for the eligibility condition
      And I save the group
      Then the eligibility condition is stored as I intended
      When I am editing the group
      And I save the group
      Then the eligibility condition is still stored as I intended
      When I am editing the group
      And I change the experience for the eligibility condition to "ExperienceC"
      And I save the group
      Then the eligibility condition is stored as I intended

    Scenario: Suggestion conditions on groups
      Given I am logged in as a "training_admin"
      When I am creating a group
      And I check "Suggested"
      And I press "Add new suggestion condition"
      And I select "ExperienceB" as the experience for the suggestion condition
      And I save the group
      Then the suggestion condition is stored as I intended
      When I am editing the group
      And I save the group
      Then the suggestion condition is still stored as I intended
      When I am editing the group
      And I change the experience for the suggestion condition to "ExperienceC"
      And I save the group
      Then the suggestion condition is stored as I intended

FEATURE;

  /**
   * @Given experiences A, B and C
   */
  function experiencesABAndC() {
    $arr = ['A', 'B', 'C'];
    foreach ($arr as $item) {
      $this->experiences["Experience$item"] = $this->createEntity('node', [
        'type' => 'training_experience',
        'title' => "Experience$item",
      ]);
    }
  }

  /**
   * @When I am creating an experience
   */
  function iAmCreatingAnExperience() {
    $this->drupalGet('/node/add/training_experience');
    $this->assertSession()->addressEquals('/node/add/training_experience');
    $this->assertSession()->pageTextContains('Create Training experience');
    $page = $this->getCurrentPage();
    $this->experienceTitle = 'The random title XYZ';
    $page->fillField('Title', $this->experienceTitle);
  }

  /**
   * @And I select :value as the experience for the condition
   * @When I select :value as the experience for the condition
   */
  function iSelectAsTheExperienceForTheCondition($value) {
    $page = $this->getCurrentPage();
    $page->findButton('Add new condition')->press();
    sleep(2);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->selectOptionFromSelect2EntityReferenceWidget('field_automatic_conditions[form][0][experiences]', $value, FALSE);
    $this->condition[] = ['target_id' => $this->experiences[$value]->id()];
  }

  /**
   * @When I change the experience for the condition to :value
   */
  public function iChangeTheExperienceForTheConditionTo($value) {
    $page = $this->getCurrentPage();
    $page->findById('edit-field-automatic-conditions-entities-0-actions-ief-entity-edit')->press();
    sleep(2);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->emptySelect2EntityReferenceWidget('field_automatic_conditions[form][inline_entity_form][entities][0][form][experiences]');
    $this->selectOptionFromSelect2EntityReferenceWidget('field_automatic_conditions[form][inline_entity_form][entities][0][form][experiences]', $value, FALSE);
    $page->findButton('Update condition')->press();
    sleep(2);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->condition = [];
    $this->condition[] = ['target_id' => $this->experiences[$value]->id()];
  }

  /**
   * @And I save the experience
   * @When I save the experience
   */
  function iSaveTheExperience() {
    $page = $this->getCurrentPage();
    $page->findButton('Save')->press();
  }

  /**
   * @Then the condition is stored as I intended
   * @Then the condition is still stored as I intended
   */
  function theConditionIsStoredAsiIntended() {
    $experience = $this->loadEntityByProperties('node', [
      'title' => $this->experienceTitle
    ], TRUE);
    $this->assertNotFalse(
      $experience,
      sprintf('An experience titled "%s" should be stored', $this->experienceTitle)
    );
    $cid = $experience->get('field_automatic_conditions')->getString();
    $tCondition = $this->loadEntityByProperties('training_condition', [
      'id' => $cid
    ]);
    // Without reloading the entity
    // it retains the previous value??
    sleep(2);
    $tCondition = $this->reloadEntity($tCondition);
    $this->assertNotFalse(
      $tCondition,
      sprintf('A training condition should be stored')
    );
    $this->assertEquals(
      $tCondition->get('experiences')->getValue(),
      $this->condition,
      'The condition should include the correct Training(s)');
  }

  /**
   * @When I edit the experience
   */
  function iEditTheExperience() {
    $node = $this->loadEntityByProperties('node', [
      'type' => 'training_experience',
      'title' => $this->experienceTitle
    ], TRUE);
    $this->drupalGet('/node/'.$node->id().'/edit');
    $this->assertSession()->addressEquals('/node/'.$node->id().'/edit');
    $this->assertSession()->pageTextContains('The random title XYZ');
  }

  /**
   * @When I am creating a group
   */
  public function iAmCreatingAGroup() {
    $this->drupalGet('/group/add/course');
    $this->assertSession()->addressEquals('group/add/course');
    $this->assertSession()->pageTextContains('Add Course');
    $page = $this->getCurrentPage();
    $this->courseTitle = 'The random course title';
    $page->fillField('Title', $this->courseTitle);
    $this->switchToFieldGroupHorizontalTab('visibility');
  }

  protected function switchToFieldGroupHorizontalTab($name) {
    $page = $this->getCurrentPage();
    $href = "#edit-group-$name";
    $tab = $page->find('xpath', '//a[@href="' . $href .'"]');
    $this->assertNotNull($tab, "There should be a tab field group with the href '$href'.");
    $tab->click();
  }

  /**
   * @When I select :value as the experience for the eligibility condition
   */
  public function iSelectAsTheExperienceForTheEligibilityCondition($value) {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->switchToFieldGroupHorizontalTab('visibility');
    $this->selectOptionFromSelect2EntityReferenceWidget('field_eligibility_conditions[form][0][experiences]', $value, FALSE);
    $this->condition = [];
    $this->condition[] = ['target_id' => $this->experiences[$value]->id()];
    $this->getCurrentPage()->findButton('Create eligibility condition')->press();
  }

  /**
   * @When I select :value as the experience for the suggestion condition
   */
  public function iSelectAsTheExperienceForTheSuggestionCondition($value) {
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->switchToFieldGroupHorizontalTab('visibility');
    $this->selectOptionFromSelect2EntityReferenceWidget('field_suggestion_conditions[form][0][experiences]', $value, FALSE);
    $this->condition = [];
    $this->condition[] = ['target_id' => $this->experiences[$value]->id()];
    $this->getCurrentPage()->findButton('Create suggestion condition')->press();
  }

  /**
   * @When I save the group
   */
  public function iSaveTheGroup() {
    $page = $this->getCurrentPage();
    $page->findById('edit-submit')->press();
  }

  /**
   * @When I am editing the group
   */
  public function iAmEditingTheGroup() {
    $course = $this->loadEntityByProperties('group', [
      'type' => 'course',
      'label' => $this->courseTitle
    ], TRUE);
    $this->drupalGet('/group/'.$course->id().'/manage');
    $this->assertSession()->addressEquals('/group/'.$course->id().'/manage');
    $this->assertSession()->pageTextContains('Edit ' . $this->courseTitle);
  }

  /**
   * @Then the eligibility condition is stored as I intended
   * @Then the eligibility condition is still stored as I intended
   */
  public function theEligibilityConditionIsStoredAsIIntended() {
    // Accepts field name of entity reference field
    $this->theConditionIsStoredOnTheGroupAsIntended('field_eligibility_conditions');
  }

  /**
   * @Then the suggestion condition is stored as I intended
   * @Then the suggestion condition is still stored as I intended
   */
  public function theSuggestionConditionIsStoredAsIIntended() {
    // Accepts field name of entity reference field
    $this->theConditionIsStoredOnTheGroupAsIntended('field_suggestion_conditions');
  }

  /**
   * Helper function
   */
  public function theConditionIsStoredOnTheGroupAsIntended($field_name) {
    $course = $this->loadEntityByProperties('group', [
      'type' => 'course',
      'label' => $this->courseTitle
    ], TRUE);
    $this->assertNotFalse(
      $course,
      sprintf('A course titled "%s" should be stored', $this->courseTitle)
    );
    $ecid = $course->get($field_name)->getString();
    $eCondition = $this->loadEntityByProperties('training_condition', [
      'id' => $ecid
    ]);
    // Without reloading the entity
    // it retains the previous value??
    sleep(2);
    $eCondition = $this->reloadEntity($eCondition);
    $this->assertNotFalse(
      $eCondition,
      sprintf('An eligibility condition should be stored')
    );
    $this->assertEquals(
      $eCondition->get('experiences')->getValue(),
      $this->condition,
      'The condition should include the correct Experience(s)');
  }

  /**
   * @And I change the experience for the eligibility condition to :value
   * @When I change the experience for the eligibility condition to :value
   */
  public function iChangeTheExperienceForTheEligibilityConditionTo($value) {
    $this->switchToFieldGroupHorizontalTab('visibility');
    $page = $this->getCurrentPage();
    $page->findById('edit-field-eligibility-conditions-entities-0-actions-ief-entity-edit')->press();
    sleep(2);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->emptySelect2EntityReferenceWidget('field_eligibility_conditions[form][inline_entity_form][entities][0][form][experiences]');
    sleep(1);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->selectOptionFromSelect2EntityReferenceWidget('field_eligibility_conditions[form][inline_entity_form][entities][0][form][experiences]', $value, FALSE);
    $page->findButton('Update eligibility condition')->press();
    $this->condition = [];
    $this->condition[] = ['target_id' => $this->experiences[$value]->id()];
  }

  /**
   * @When I change the experience for the suggestion condition to :value
   */
  public function iChangeTheExperienceForTheSuggestionConditionTo($value) {
    $this->switchToFieldGroupHorizontalTab('visibility');
    $page = $this->getCurrentPage();
    $page->findById('edit-field-suggestion-conditions-entities-0-actions-ief-entity-edit')->press();
    sleep(2);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->emptySelect2EntityReferenceWidget('field_suggestion_conditions[form][inline_entity_form][entities][0][form][experiences]');
    sleep(1);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    $this->selectOptionFromSelect2EntityReferenceWidget('field_suggestion_conditions[form][inline_entity_form][entities][0][form][experiences]', $value, FALSE);
    $page->findButton('Update suggestion condition')->press();
    $this->condition = [];
    $this->condition[] = ['target_id' => $this->experiences[$value]->id()];
  }


}
