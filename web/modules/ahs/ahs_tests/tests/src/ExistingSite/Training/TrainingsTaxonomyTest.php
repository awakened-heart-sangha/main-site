<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class TrainingsTaxonomyTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $groups = [];
  protected $trainings = [];
  protected $course;
  protected $course_template;

  protected static $feature = <<<'FEATURE'
Feature: Groups can be assigned to training areas
    In order for training conditions to be useful
    They must be evaluable against users

    Background:
      Given a training area "AreaA"
      And a training area "AreaB"
      And a training area "AreaC"
      And "AreaB" is part of "AreaA"
      And a course group called "CourseA"
      And a course group called "CourseB"
      And a course group called "CourseC"
      And a course_template group called "TemplateC"
      And "CourseA" is associated with "AreaA"
      And "CourseB" is associated with "AreaB"
      And "CourseC" is associated with "AreaC"
      And "TemplateC" is associated with "AreaC"
      And I am logged in

    Scenario: Child area does not show parent groups
      When I visit "/training/areas/areab"
      Then I should not see "CourseA"
      And I should see "CourseB"
      And I should not see "CourseC"
      When "AreaC" becomes part of "AreaB"
      And I visit "/training/areas/areab" again
      Then I should not see "CourseA"
      And I should see "CourseB"
      And I should see "CourseC"

    Scenario: Parent area shows child area groups
      When I visit "/training/areas/areaa"
      Then I should see "CourseA"
      And I should see "CourseB"
      And I should not see "CourseC"
      When "AreaC" becomes part of "AreaB"
      And I visit "/training/areas/areaa" again
      Then I should see "CourseA"
      And I should see "CourseB"
      And I should see "CourseC"

    Scenario: Description visible
      When I visit "/training/areas/areaa"
      Then I should see the description of "AreaA"

    Scenario: Template visible on training area
      When I visit "/training/areas/areac"
      Then I should see "CourseC"
      Then I should see "TemplateC"

FEATURE;

  /**
   * @Given a training area :name
   */
  public function aTrainingArea($name) {
    $this->trainings[$name] = $this->createEntity('taxonomy_term', [
      'vid' => 'trainings',
      'name' => $name,
      'description' => $this->randomString(),
    ]);
  }

  /**
   * @Given :child is part of :parent
   * @When :child becomes part of :parent
   */
  public function isPartOf($child, $parent) {
    $this->trainings[$child]->set('parent', $this->trainings[$parent])->save();
  }

  /**
   * @Given :course is associated with :training
   * @When :course becomes associated with :training
   */
  public function groupBecomesAssociatedWithTraining($course, $training) {
    if (!isset($this->groups[$course])) {
      $this->groups[$course] = $this->createGroup(['type' => 'course', 'label' => $course], TRUE);
    }
    $this->groups[$course]->set('field_trainings', $this->trainings[$training])->save();
  }

  /**
   * @Then I should see the description of :training
   */
  public function iShouldSeeTheDescription($training) {
    $this->iShouldSee($this->trainings[$training]->description->value);
  }

}
