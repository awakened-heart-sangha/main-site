<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Behat\Gherkin\Node\TableNode;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class TrainingConditionsTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $uidsMeetingConditions;

  protected $date;

  protected $conditions;

  protected $namesMeetingConditions;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Trainings conditions logic
    In order for training conditions to be useful
    They must be evaluable against users

    Background:
      Given experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
        | ExperienceD |
      Given a user named "Fred" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceA |
        | ExperienceB |
      Given a user named "Jane" has training records for:
        | experience  | effective_time |
        | ExperienceA |                |
        | ExperienceA | 2023-01-1      |
        | ExperienceC |                |
      Given a user named "Harry" has training records for:
        | experience  | effective_time |
        | ExperienceA | 2021-01-01     |
        | ExperienceC | 2023-01-1      |
      And a user named "Jack" has no training records


    Scenario: Single condition single experience, two pass
      Given a set of training conditions:
        | conditions   |
        | ExperienceA |
      Then "Fred" passes the conditions
      And "Jane" passes the conditions
      And "Harry" passes the conditions
      And "Jack" fails the conditions

    Scenario: Single condition single experience, one pass
      Given a set of training conditions:
        | conditions   |
        | ExperienceB |
      Then "Fred" passes the conditions
      And "Jane" fails the conditions
      And "Harry" fails the conditions
      And "Jack" fails the conditions

    Scenario: Single condition, single experience, all fail
      Given a set of training conditions:
        | conditions   |
        | ExperienceD |
      Then "Fred" fails the conditions
      And "Jane" fails the conditions
      And "Harry" fails the conditions
      And "Jack" fails the conditions

    Scenario: Single condition, two experiences
      Given a set of training conditions:
        | conditions               |
        | ExperienceA, ExperienceC |
      Then "Fred" fails the conditions
      And "Jane" passes the conditions
      And "Harry" passes the conditions
      And "Jack" fails the conditions

    Scenario: Two conditions
      Given a set of training conditions:
      | conditions  |
      | ExperienceB |
      | ExperienceC |
      Then "Fred" passes the conditions
      And "Jane" passes the conditions
      And "Harry" passes the conditions
      And "Jack" fails the conditions

    Scenario: Two conditions, double qualify
      Given a set of training conditions:
        | conditions  |
        | ExperienceA |
        | ExperienceC |
      Then "Fred" passes the conditions
      And "Jane" passes the conditions
      And "Harry" passes the conditions
      And "Jack" fails the conditions

    Scenario: Multiple conditions
      Given a set of training conditions:
      | conditions  |
      | ExperienceA |
      | ExperienceB |
      | ExperienceC |
      | ExperienceD |
      Then "Fred" passes the conditions
      And "Jane" passes the conditions
      And "Harry" passes the conditions
      And "Jack" fails the conditions

    Scenario: Single condition evaluated before effective
      Given a set of training conditions:
        | conditions  |
        | ExperienceC |
      And the conditions are evaluated on "2022-01-01"
      Then "Fred" fails the conditions
      And "Jane" passes the conditions
      # Harry's experienceC is effective on 2023-01-01
      And "Harry" fails the conditions
      And "Jack" fails the conditions

    Scenario: Single condition evaluated after effective
      Given a set of training conditions:
        | conditions  |
        | ExperienceC |
      And the conditions are evaluated on "2024-01-01"
      Then "Fred" fails the conditions
      And "Jane" passes the conditions
      # Harry's experienceC is effective on 2023-01-01
      And "Harry" passes the conditions
      And "Jack" fails the conditions

    Scenario: Pair condition evaluated before one part effective
      Given a set of training conditions:
        | conditions               |
        | ExperienceA, ExperienceC |
      And the conditions are evaluated on "2022-01-01"
      Then "Fred" fails the conditions
      And "Jane" passes the conditions
      # Harry's experienceC is effective on 2023-01-01
      And "Harry" fails the conditions
      And "Jack" fails the conditions

    Scenario: Pair condition evaluated after both parts effective
      Given a set of training conditions:
        | conditions               |
        | ExperienceA, ExperienceC |
      And the conditions are evaluated on "2024-01-01"
      Then "Fred" fails the conditions
      And "Jane" passes the conditions
      # Harry's experienceC is effective on 2023-01-01
      And "Harry" passes the conditions
      And "Jack" fails the conditions

    Scenario: Double condition evaluated after both parts effective
      Given a set of training conditions:
        | conditions               |
        | ExperienceA, ExperienceA |
      Given the conditions are evaluated on "2024-01-01"
      Then "Fred" passes the conditions
      # Jane has a second experienceA effective on 2023-01-01
      And "Jane" passes the conditions
      And "Harry" fails the conditions
      And "Jack" fails the conditions

    Scenario: Double condition evaluated before both parts effective
      Given a set of training conditions:
        | conditions               |
        | ExperienceA, ExperienceA |
      And the conditions are evaluated on "2022-01-01"
      Then "Fred" passes the conditions
      # Jane has a second experienceA effective on 2023-01-01
      Then "Jane" fails the conditions
      And "Harry" fails the conditions
      And "Jack" fails the conditions


FEATURE;

  /**
   * @Given a set of training conditions:
   */
  public function aSetOfTrainingConditions(TableNode $table) {
    $this->conditions = $this->getConditionsFromTable($table);
  }

  /**
   * @Then :name passes the conditions
   */
  public function passesTheConditions($name) {
    $this->assertConditionsMet(TRUE, $name);
  }

  /**
   * @Then :name fails the conditions
   */
  public function failsTheConditions($name) {
    $this->assertConditionsMet(FALSE, $name);
  }

  /**
   * @Given the conditions are evaluated on :date
   */
  public function theConditionsAreEvaluatedAt($date) {
    $this->date = new DrupalDateTime($date);
  }

  /**
   * Helper function
   */
  protected function assertConditionsMet($isMet, $name) {
    $uid = $this->users[$name]->id();
    if (is_null($this->uidsMeetingConditions)) {
      $this->uidsMeetingConditions = \Drupal::service('ahs_training.group_users_manager')->getUsersMeetingConditions($this->conditions, $this->date);
      if (!is_array($this->uidsMeetingConditions)) {
        $this->namesMeetingConditions = "All";
      }
      else {
        $this->namesMeetingConditions = [];
        foreach ($this->users as $user_name => $user) {
          if (in_array($user->id(), $this->uidsMeetingConditions)) {
            $this->namesMeetingConditions[] = $user_name;
          }
        }
      }
    }

    $userAmongThoseMeetingConditions = ($this->uidsMeetingConditions === TRUE) || in_array($uid, $this->uidsMeetingConditions);
    $actual = \Drupal::service('ahs_training.user_experience_evaluator')->isAnyConditionMetByUser($this->conditions, $uid, $this->date);
    if ($isMet) {
      $this->assertTrue($actual, "$name should meet the conditions");
      $this->assertTrue($userAmongThoseMeetingConditions, "$name should be among the users meeting the conditions. They were: " . print_r($this->namesMeetingConditions, TRUE));
    }
    else {
      $this->assertFalse($actual, "$name should not meet the conditions");
      $this->assertFalse($userAmongThoseMeetingConditions, "$name should not be among the users meeting the conditions. They were: " . print_r($this->namesMeetingConditions, TRUE));
    }
  }


}
