<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class ExperienceAdminUITest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected static $feature = <<<'FEATURE'
Feature: Experiences admin UI
    In order for staff to administer experiences
    They need a basic user interface

    Scenario: Admins can view experiences list
      Given an experience
      And I am logged in as a "training_admin"
      When I visit the experience list page
      Then I should see the experience listed

    Scenario: Non-admins can not view experiences list
      Given I do not have the permission "administer trainings"
      When I visit the experience list page
      Then I should be denied access

    Scenario: Admins can add experiences
      Given I am logged in as a "training_admin"
      When I visit the experience add page
      And I submit a new experience with only a title "New Experience"
      When I visit the experience list page
      Then I should see the experience titled "New Experience"

FEATURE;


  /**
   * @Given an experience
   */
  function anExperience() {
    $values = ['type' => 'training_experience', 'title' => 'My Experience'];
    $this->experience = $this->createEntity('node', $values);
  }

  /**
   * @Given I do not have the permission :permission
   */
  function iDoNotHaveThePermission($permission) {
    $this->setMe($this->createUser());
    $this->iLogin();
  }

  /**
   * @When I visit the experience list page
   */
  function iVisitTheExperienceListPage() {
    $this->drupalGet('/admin/experiences');
  }

  /**
   * @When I visit the experience add page
   */
  function iVisitTheExperienceAddPage() {
    $this->drupalGet('/node/add/training_experience');
    $this->assertSession()->addressEquals('/node/add/training_experience');
    $this->assertSession()->pageTextContains('Create Training experience');
  }

  /**
   * @Then I should see the experience listed
   */
  function iShouldSeeTheExperienceListed() {
    $this->assertSession()->addressEquals('/admin/experiences');
    $this->assertSession()->pageTextContains($this->experience->getTitle());
  }

  /**
   * @Then I should see the experience titled :title
   */
  function iShouldSeeTheExperienceTitled($title) {
    $this->assertSession()->pageTextContains($title);
  }

  /**
   * @Then I should be denied access
   */
  function iShouldBeDeniedAccess() {
    $this->assertSession()->addressEquals('/admin/experiences');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * @When I submit a new experience with only a title :title
   */
  function iSubmitANewExperienceWithOnlyATitle($title) {
    $page = $this->getCurrentPage();
    $page->fillField('Title', $title);
    $page->find('css', '#edit-submit')->click();
  }
}
