<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Behat\Gherkin\Node\TableNode;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class UserExperienceEvaluationTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $trainings;

  protected static $feature = <<<'FEATURE'
Feature: Training completion evaluation
    In order for a user's experience history to be understood
    Training records must reveal if a user has an experience
    Background:
      Given experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |

    Scenario: Single training records
      Given a user has training records for:
       | experience  |
       | ExperienceA |
       | ExperienceB |
      Then the user is evaluated as having completed "ExperienceA"
      And the user is evaluated as having completed "ExperienceB"
      And the user is not evaluated as having completed "ExperienceC"

    Scenario: No training records
      Given a user has no training records
      Then the user is not evaluated as having completed "ExperienceA"
      And the user is not evaluated as having completed "ExperienceB"
      And the user is not evaluated as having completed "ExperienceC"

    Scenario: Duplicate training records
      Given a user has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceA |
        | ExperienceB |
      Then the user is evaluated as having completed "ExperienceA"
      Then the user is evaluated as having completed "ExperienceA" twice
      And the user is evaluated as having completed "ExperienceB"
      And the user is not evaluated as having completed "ExperienceC"

    Scenario: Multiple training records with dates
      Given a user has training records for:
        | experience  | effective_time |
        | ExperienceA | 2021-01-01          |
        | ExperienceA | 2023-01-1           |
      Given today is "2020-01-01"
      Then the user is not evaluated as having completed "ExperienceA"
      Then the user is not evaluated as having completed "ExperienceA" twice
      Given today is "2022-01-01"
      Then the user is evaluated as having completed "ExperienceA"
      Then the user is not evaluated as having completed "ExperienceA" twice
      Given today is "2024-01-01"
      Then the user is evaluated as having completed "ExperienceA"
      Then the user is evaluated as having completed "ExperienceA" twice

FEATURE;

  /**
   * @Then the user is evaluated as having completed :experience
   */
  public function theUserIsEvaluatedAsHavingCompleted($experience) {
    $this->assertTrue(
      $this->hasExperience($experience),
      "The user should have the '$experience' experience"
    );
  }

  /**
   * @Then the user is not evaluated as having completed :experience
   */
  public function theUserIsNotEvaluatedAsHavingCompleted($experience) {
    $this->assertFalse(
      $this->hasExperience($experience),
      "The user should not have the '$experience' experience"
    );
  }

  /**
   * @Then the user is evaluated as having completed :experience twice
   */
  public function theUserIsEvaluatedAsHavingCompletedTwice($experience) {
    $this->assertTrue(
      $this->hasExperience($experience, 2),
      "The user should have the '$experience' experience twice"
    );
  }

  /**
   * @Then the user is not evaluated as having completed :experience twice
   */
  public function theUserIsNotEvaluatedAsHavingCompletedTwice($experience) {
    $this->assertFalse(
      $this->hasExperience($experience, 2),
      "The user should not have the '$experience' experience twice"
    );
  }

  protected function hasExperience($experience, $experienceCount = 1) {
    $time = \Drupal::service('datetime.time')->getCurrentTime();
    $date = DrupalDateTime::createFromTimestamp($time);
    $trService = \Drupal::service('ahs_training.user_experience_evaluator');
    $nid = $this->experiences[$experience]->id();
    return $trService->hasExperience($nid, $this->user->id(), $date, $experienceCount);
  }

  protected function hasExperiences($experiences) {
    $time = \Drupal::service('datetime.time')->getCurrentTime();
    $date = DrupalDateTime::createFromTimestamp($time);
    $trService = \Drupal::service('ahs_training.user_experience_evaluator');
    $experienceArray = array_map('trim', explode(',', $experiences));
    $nids = [];
    foreach ($experienceArray as $experience) {
      $nids[] = $this->experiences[$experience]->id();
    }
    return $trService->hasAllExperiences($nids, $this->user->id(), $date);
  }


}
