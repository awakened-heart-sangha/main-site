<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * @group ClonedDatabaseRequired
 */
class GroupEligibilityTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $groups;

  protected static $feature = <<<'FEATURE'
Feature: Groups participation bestows group eligibility

    Scenario: Joining a group bestows eligibility on later groups
      Given I am not participating in any group
      And group A confers a training experience
      And eligibility for group B requires the training experience
      And group B starts after group A finishes
      Then I am not eligible for group B
      When I join group A
      Then I am eligible for group B

    Scenario: Joining a group does not bestow eligibility on earlier groups
      Given I am not participating in any group
      And group A confers a training experience
      And eligibility for group B requires the training experience
      And group B starts before group A finishes
      Then I am not eligible for group B
      When I join group A
      Then I am not eligible for group B

    Scenario: Adding an experience to a group bestows eligibility on existing participants
      Given I am participating in group A
      And eligibility for group B requires a training experience
      And group B starts after group A finishes
      Then I am not eligible for group B
      When group A is changed to confer the training experience
      Then I am eligible for group B
      When group A is changed to not confer the training experience
      Then I am not eligible for group B

    Scenario: Leaving a group removes eligibility
      Given group A confers a training experience
      And Fred, Jane and Jack are participating in group A
      And eligibility for group B requires the training experience
      Then "Fred" is eligible for group B
      And "Jane" is eligible for group B
      And "Jack" is eligible for group B
      When "Jane" is removed from group A
      Then "Fred" is still eligible for group B
      And "Jane" is not eligible for group B
      And "Jack" is still eligible for group B


FEATURE;

  /**
   * @Given I am not participating in any group
   */
  public function iAmNotParticipatingInAnyGroup() {
    $edit = [];
    $edit['name'] = $this->randomMachineName();
    $edit['pass'] = \Drupal::service('password_generator')->generate();
    $edit['mail'] = $this->randomEmail();
    $edit['roles'] = ['authenticated', 'member'];
    $this->user = $this->createEntity('user', $edit);
  }

  /**
   * @Given group A confers a training experience
   */
  public function groupAConfersATrainingExperience() {
    $edit = [
      'type' => 'training_experience',
      'title' => 'An experience',
      'uid' => 1,
    ];
    $this->experience = $this->createEntity('node', $edit);
    $props = [
      'type' => 'course',
      'label' => "A: " . $this->randomMachineName(),
      'field_experiences_given' => ['target_id' => $this->experience->id()]
    ];
    $this->groups['A'] = $this->createGroup($props);
  }

  /**
   * @Given eligibility for group B requires the training experience
   */
  public function eligibilityForGroupBRequiresTheTrainingExperience() {
    $eligibilityCondition = $this->createEntity('training_condition', [
      'experiences' => ['target_id' => $this->experience->id()]
    ]);
    $props = [
      'type' => 'course',
      'label' => "B: " . $this->randomMachineName(),
      'field_eligibility_conditions' => ['target_id' => $eligibilityCondition->id()]
    ];
    $this->groups['B'] = $this->createGroup($props);
  }


  /**
   * @Given eligibility for group B requires a training experience
   */
  public function eligibilityForGroupBRequiresATrainingExperience() {
    $edit = [
      'type' => 'training_experience',
      'title' => 'An experience',
      'uid' => 1,
    ];
    $this->experience = $this->createEntity('node', $edit);
    $eligibilityCondition = $this->createEntity('training_condition', [
      'experiences' => ['target_id' => $this->experience->id()]
    ]);
    $props = [
      'type' => 'course',
      'field_eligibility_conditions' => ['target_id' => $eligibilityCondition->id()]
    ];
    $this->groups['B'] = $this->createGroup($props);
  }

  /**
   * @Given group B starts :before_or_after group A finishes
   */
  public function groupBStartsAfterGroupAFinishes($before_or_after) {
    $aStart = new DrupalDateTime('-35 days');
    $aFinish = new DrupalDateTime();
    $bFinish = new DrupalDateTime('+36 days');
    if ($before_or_after === 'before') {
      $bStart = new DrupalDateTime('-35 days');
    }
    elseif ($before_or_after === 'after') {
      $bStart = new DrupalDateTime('+35 days');
    }
    else {
      throw new \Exception();
    }
    $this->groups['A']->set('field_dates', [
      'value' => $aStart->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $aFinish->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    ])->save();
    $this->groups['B']->set('field_dates', [
      'value' => $bStart->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $bFinish->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    ])->save();
  }

  /**
   * @Then I am not eligible for group B
   */
  public function iAmNotEligibleForGroupB() {
    $this->assertFalse(
      $this->groups['B']->areEligibilityConditionsMetByUser($this->user),
      'The user should NOT be eligible for Group B'
    );
    $eligibleUsers = \Drupal::service('ahs_training.group_users_manager')->getEligibleUsersForGroup($this->groups['B']);
    $this->assertNotContains($this->user->id(), $eligibleUsers, "The user should not be among the users eligible for group B");
  }

  /**
   * @When I join group A
   */
  public function iJoinGroupA() {
    $membership = ['field_participant_type' => $this->STUDENT_TERM_ID];
    $this->groups['A']->addMember($this->user, $membership);
  }

  /**
   * @Then I am eligible for group B
   */
  public function iAmEligibleForGroupB() {
    $this->assertTrue(
      $this->groups['B']->areEligibilityConditionsMetByUser($this->user),
      'The user should be eligible for Group B'
    );
    $eligibleUsers = \Drupal::service('ahs_training.group_users_manager')->getEligibleUsersForGroup($this->groups['B']);
    $this->assertContains($this->user->id(), $eligibleUsers, "The user should be among the users eligible for group B");
  }

  /**
   * @Given I am participating in group A
   */
  public function iAmParticipatingInGroupA() {
    $aStart = new DrupalDateTime();
    $aFinish = new DrupalDateTime('+5 hours');
    $edit = [];
    $edit['name'] = $this->randomMachineName();
    $edit['pass'] = \Drupal::service('password_generator')->generate();
    $edit['mail'] = $this->randomEmail();
    $edit['roles'] = ['authenticated', 'member'];
    $this->user = $this->createEntity('user', $edit);

    $props = [
      'type' => 'course',
      'field_dates' => [
        'value' => $aStart->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        'end_value' => $aFinish->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      ]
    ];
    $this->groups['A'] = $this->createGroup($props);
    $membership = ['field_participant_type' => $this->STUDENT_TERM_ID];
    $this->groups['A']->addMember($this->user, $membership);
  }

  /**
   * @When group A is changed to confer the training experience
   */
  public function groupAIsChangedToConferTheTrainingExperience() {
    $this->groups['A']->set('field_experiences_given', $this->experience);
    $this->groups['A']->save();
  }

  /**
   * @When group A is changed to not confer the training experience
   */
  public function groupAIsChangedToNotConferTheTrainingExperience() {
    $this->groups['A']->set('field_experiences_given', NULL);
    $this->groups['A']->save();
  }

  /**
   * @Given Fred, Jane and Jack are participating in group A
   */
  public function fredJaneAndJackAreParticipatingInGroupA() {
    $edit = [];
    $edit['pass'] = \Drupal::service('password_generator')->generate();
    $edit['roles'] = ['authenticated', 'member'];
    $edit['name'] = $this->randomMachineName();
    $edit['mail'] = $this->randomEmail();
    $this->users['Fred'] = $this->createEntity('user', $edit);
    $edit['name'] = $this->randomMachineName();
    $edit['mail'] = $this->randomEmail();
    $this->users['Jane'] = $this->createEntity('user', $edit);
    $edit['name'] = $this->randomMachineName();
    $edit['mail'] = $this->randomEmail();
    $this->users['Jack'] = $this->createEntity('user', $edit);

    $membership = ['field_participant_type' => $this->STUDENT_TERM_ID];
    $this->groups['A']->addMember($this->users['Fred'], $membership);
    $this->groups['A']->addMember($this->users['Jane'], $membership);
    $this->groups['A']->addMember($this->users['Jack'], $membership);
  }

  /**
   * @Then :name is eligible for group B
   * @Then :name is still eligible for group B
   */
  public function isEligibleForGroupB($name) {
    $this->assertTrue(
      $this->groups['B']->areEligibilityConditionsMetByUser($this->users[$name]),
      'The user should be eligible for Group B'
    );
    $eligibleUsers = \Drupal::service('ahs_training.group_users_manager')->getEligibleUsersForGroup($this->groups['B']);
    $this->assertContains($this->users[$name]->id(), $eligibleUsers, "The user should be among the users eligible for group B");
  }

  /**
   * @When :name is removed from group A
   */
  public function isRemovedFromGroupA($name) {
    $this->groups['A']->removeMember($this->users[$name]);
  }

  /**
   * @Then :name is not eligible for group B
   */
  public function isNotEligibleForGroupB($name) {
    $this->assertFalse(
      $this->groups['B']->areEligibilityConditionsMetByUser($this->users[$name]),
      'The user should NOT be eligible for Group B'
    );
    $eligibleUsers = \Drupal::service('ahs_training.group_users_manager')->getEligibleUsersForGroup($this->groups['B']);
    $this->assertNotContains($this->users[$name]->id(), $eligibleUsers, "The user should not be among the users eligible for group B");
  }


}
