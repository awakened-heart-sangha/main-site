<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class GroupConditionsTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $userResults = [];

  protected $course;
  protected $course_template;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Trainings conditions logic
    In order for training conditions to be useful
    They must be evaluable against users

    Background:
      Given a course that starts on "2022-01-01"
      And the course is promotable
      And today is "2021-01-01"
      Given experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
      Given a user named "Fred" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceB |
      Given a user named "Jane" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceC |
      Given a user named "Harry" has training records for:
        | experience  | effective_time |
        | ExperienceA | 2020-01-01          |
        | ExperienceC | 2023-01-01          |
      Given a user named "Lisa" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
      And a user named "Jack" has no training records
      And "Lisa" is already a participant in the course

    Scenario: Eligibility based on condition
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceA |
      And the course is not suggested
      Then "Fred" is eligible
      And "Jane" is eligible
      And "Lisa" is eligible
      And "Jack" is not eligible
      Then "Fred" is not suggested
      And "Jane" is not suggested
      And "Lisa" is not suggested
      And "Jack" is not suggested
      And the course is available to "Fred"
      And the course is available to "Jane"
      And the course is not available to "Lisa"
      And the course is not available to "Jack"

    Scenario: Eligibility without conditions
      Given the course has no eligibility conditions
      And the course is not suggested
      Then "Fred" is eligible
      And "Jane" is eligible
      And "Lisa" is eligible
      And "Jack" is eligible
      Then "Fred" is not suggested
      And "Jane" is not suggested
      And "Lisa" is not suggested
      And "Jack" is not suggested
      And the course is available to "Fred"
      And the course is available to "Jane"
      And the course is not available to "Lisa"
      And the course is available to "Jack"

    Scenario: Suggestion without condition
      Given the course has no eligibility conditions
      And the course is suggested
      Then "Fred" is eligible
      And "Jane" is eligible
      And "Lisa" is eligible
      And "Jack" is eligible
      Then "Fred" is suggested unconditionally
      And "Jane" is suggested unconditionally
      And "Lisa" is not suggested
      And "Jack" is suggested unconditionally
      And the course is available to "Fred"
      And the course is available to "Jane"
      And the course is not available to "Lisa"
      And the course is available to "Jack"

    Scenario: Suggestion conditions ignored if not suggested
      Given the course has no eligibility conditions
      And the course is not suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceA |
      Then "Fred" is not suggested
      And "Jane" is not suggested
      And "Lisa" is not suggested
      And "Jack" is not suggested

    Scenario: Suggestion based on condition
      Given the course has no eligibility conditions
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceA |
      Then "Fred" is eligible
      And "Jane" is eligible
      And "Lisa" is eligible
      And "Jack" is eligible
      Then "Fred" is suggested
      And "Jane" is suggested
      And "Lisa" is not suggested
      And "Jack" is not suggested
      And the course is available to "Fred"
      And the course is available to "Jane"
      And the course is not available to "Lisa"
      And the course is available to "Jack"

    Scenario: Suggestion does not impose eligibility
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceA |
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceC |
      Then "Fred" is eligible
      And "Jane" is eligible
      And "Lisa" is eligible
      And "Jack" is not eligible
      Then "Fred" is not suggested
      And "Jane" is suggested
      And "Lisa" is not suggested
      And "Jack" is not suggested
      And the course is available to "Fred"
      And the course is available to "Jane"
      And the course is not available to "Lisa"
      And the course is not available to "Jack"

    Scenario: Suggestion does not confer eligibility
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceB |
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceC |
      Then "Fred" is eligible
      And "Jane" is not eligible
      And "Lisa" is eligible
      And "Jack" is not eligible
      Then "Fred" is not suggested
      And "Jane" is not suggested
      And "Lisa" is not suggested
      And "Jack" is not suggested
      And the course is available to "Fred"
      And the course is not available to "Jane"
      And the course is not available to "Lisa"
      And the course is not available to "Jack"

    Scenario: Group start date is considered when evaluating suggestion
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceA |
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceC |
      Then "Fred" is eligible
      And "Jane" is eligible
      # Harry's experienceA is effective at time 2021-01-01
      And "Harry" is eligible
      And "Jack" is not eligible
      Then "Fred" is not suggested
      Then "Jane" is suggested
      # Harry's experienceC is effective at time 2023-01-01
      And "Harry" is not suggested
      And "Jack" is not suggested
      And the course is available to "Fred"
      And the course is available to "Jane"
      And the course is not available to "Lisa"
      And the course is not available to "Jack"

    Scenario: Group start date is considered when evaluating eligibility
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceC |
      # Harry's experienceC is effective at time 2023-01-01
      Then "Harry" is not eligible
      And the course is not available to "Harry"

    Scenario: Existing experiences are not suggested
      Given the course has no eligibility conditions
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceA |
      And the course gives experience "ExperienceC"
      Then "Fred" is eligible
      And "Jane" is eligible
      And "Lisa" is eligible
      And "Jack" is eligible
      Then "Fred" is suggested
      And "Jane" is not suggested
      And "Lisa" is not suggested
      # Harry's experienceC is not effective until 2023-01-01
      # but he is still not suggested the group as it gives this experience
      And "Harry" is not suggested
      And "Jack" is not suggested
      And the course is available to "Fred"
      And the course is available to "Jane"
      And the course is not available to "Lisa"
      And the course is available to "Jack"

    Scenario: Courses, events & templates can be suggested for users
      Given a future suggested course
      And a future suggested event
      And a suggested course_template
      Then "Jack" is suggested unconditionally for the course
      And "Jack" is suggested unconditionally for the event
      And "Jack" is suggested unconditionally for the course_template

    Scenario: Course templates use a different criteria for available and suggested
      Given a suggested course_template
      And "Jack" has previously engaged with the template
      Then the course_template is available to "Lisa"
      And the course_template is available to "Jack"
      And "Lisa" is suggested for the course_template
      And "Jack" is not suggested for the course_template

    Scenario: Effective date is still relevant for course templates
      Given a suggested course_template
      And the course_template has eligibility conditions:
        | conditions  |
        | ExperienceA |
      And the course_template has suggestion conditions:
        | conditions  |
        | ExperienceC |
      And "Fred" is eligible for the course_template
      And "Jane" is eligible for the course_template
      And "Harry" is eligible for the course_template
      And "Fred" is not suggested for the course_template
      And "Jane" is suggested for the course_template
      And "Harry" is not suggested for the course_template

    Scenario: Eligibility without registration
      Given the course has no eligibility conditions
      And the course is suggested
      Then "Fred" is eligible
      And "Fred" is suggested
      When registration is closed for the course
      Then "Fred" is eligible
      And "Fred" is not suggested

FEATURE;

  /**
   * @Then the :bundle is available to :name
   */
  public function theGroupIsAvailableTo($bundle, $name) {
    $this->assertGroupAvailable($name, TRUE, $bundle);
  }

  /**
   * @Then the :bundle is not available to :name
   */
  public function theGroupIsNotAvailableTo($bundle, $name) {
    $this->assertGroupAvailable($name, FALSE, $bundle);
  }

  /**
   * @Then :name is eligible
   * @Then :name is eligible for the :bundle
   */
  public function isEligible($name, $bundle = 'course') {
    $this->assertGroupEligible($name, TRUE, $bundle);
  }

  /**
   * @Then :name is not eligible
   * @Then :name is not eligible for the :bundle
   */
  public function isNotEligible($name, $bundle = 'course') {
    $this->assertGroupEligible($name, FALSE, $bundle);
  }

  /**
   * @Then :name is suggested
   * @Then :name is suggested for the :bundle
   */
  public function isSuggested($name, $bundle = 'course') {
   $this->assertGroupSuggested($name, TRUE, $bundle);
  }

  /**
   * @Then :name is suggested unconditionally
   */
  public function isSuggestedUnconditionally($name) {
    $this->assertFalse($this->group->hasSuggestionConditions());
    $this->assertGroupSuggested($name, TRUE);
  }

  /**
   * @Then :name is suggested unconditionally for the :bundle
   */
  public function isSuggestedUnconditionallyForTheBundle($name, $bundle) {
    $this->assertFalse($this->{$bundle}->hasSuggestionConditions());
    $this->assertGroupSuggested($name, TRUE, $bundle);
  }

  /**
   * @Then :name is not suggested
   * @Then :name is not suggested for the :bundle
   */
  public function isNotSuggested($name, $bundle = 'course') {
    $this->assertGroupSuggested($name, FALSE, $bundle);
  }

  /**
   * @Then the :bundle gives experience :experience
   */
  public function theGroupGivesExperience($bundle, $experience) {
    $this->group->set('field_experiences_given', $this->experiences[$experience])->save();
  }

  /**
   * @Given the :bundle is promotable
   */
  public function theGroupIsPromotable($bundle) {
    $this->group->set('status', TRUE)->save();
    $this->group->set('field_registration_open', TRUE)->save();
    $this->spreadRegistrationSetting($this->group, 'status', TRUE);
  }

  /**
   * @When registration is closed for the :bundle
   */
  public function registrationIsClosedForTheGroup($bundle) {
    $this->group->set('field_registration_open', FALSE)->save();
    $this->spreadRegistrationSetting($this->group, 'status', FALSE);
  }

  /**
   * @Given :name has previously engaged with the template
   */
  public function hasPreviouslyEngagedWithTheTemplate($name) {
    $this->course = $this->createGroup(['type' => 'course', 'field_template' => $this->course_template]);
    $this->course->addMember($this->users[$name], []);
  }


  protected function assertGroupAvailable($name, $result, $bundle) {
    $user = $this->users[$name];
    $this->assertSame($result, $this->{$bundle}->isRegistrationAvailableToUser($user), "The group should be available for the user.");

    $groups = \Drupal::service('ahs_groups.user_groups_manager')->getAvailableGroupsForUser($user);
    $this->assertGroupAmongstGroups($this->{$bundle}, $groups, $result, $name, 'available');
  }

  protected function assertGroupEligible($name, $result, $bundle ='course') {
    $user = $this->users[$name];
    $this->assertSame($result, $this->{$bundle}->areEligibilityConditionsMetByUser($user), "The user should meet the group eligibility conditions");

    $eligibleUsers = \Drupal::service('ahs_training.group_users_manager')->getEligibleUsersForGroup($this->{$bundle}, FALSE);
    $this->assertUserAmongstUsers($name, $eligibleUsers, $result, 'eligible');
  }

  protected function assertGroupSuggested($name, $result, $bundle = 'course') {
    $user = $this->users[$name];
    $this->assertSame($result, $this->{$bundle}->isSuggestedToUser($user), "The group should be suggested to the user.");

    $groups = \Drupal::service('ahs_groups.user_groups_manager')->getSuggestedGroupsForUser($user);
    $this->assertGroupAmongstGroups($this->{$bundle}, $groups, $result, $name, 'suggested');

    if ($this->{$bundle}->hasSuggestionConditions()) {
      $suggestedUsers = \Drupal::service('ahs_training.group_users_manager')->getSuggestedUsersForGroup($this->{$bundle});
      $this->assertUserAmongstUsers($name, $suggestedUsers, $result, 'suggested');
    }
  }

  protected function assertGroupAmongstGroups($group, $groups, $result, $name, $condition) {
    if ($result) {
      $this->assertContains((int) $group->id(), array_keys($groups), "The group should be among those $condition for $name. The $condition groups were:" . print_r(array_keys($groups), TRUE));
    }
    else {
      $this->assertNotContains((int) $group->id(), array_keys($groups), "The group should not be among those $condition for $name. The $condition groups were:" . print_r(array_keys($groups), TRUE));
    }
  }

  protected function assertUserAmongstUsers($name, $users, $result, $condition) {
    // If all users are eligible, nothing to test here.
    if ($users === TRUE) {
      return;
    }

    // Get the names of the provided users for pretty message.
    if (!is_array($users)) {
      $namesMeetingConditions = $users ? 'All' : 'None';
    }
    else {
      assert(is_array($users));
      $namesMeetingConditions = [];
      foreach ($this->users as $user_name => $userObj) {
        if (in_array($userObj->id(), $users)) {
          $namesMeetingConditions[] = $user_name;
        }
      }
      $namesMeetingConditions = print_r($namesMeetingConditions, TRUE);
    }

    // Do the actual assertion.
    $user = $this->users[$name];
    $userMeetsConditions = $users !== FALSE && in_array($user->id(), $users);
    $this->assertSame($result, $userMeetsConditions, "$name should be $condition. But $condition were: " . print_r($namesMeetingConditions, TRUE));
  }

}
