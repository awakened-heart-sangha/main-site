<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group ClonedDatabaseRequired
 */
class TrainingRecordingTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $participantType;
  protected $course;
  protected $event;
  protected $special;

  protected static $feature = <<<'FEATURE'
Feature: Experience recording
    In order for the progress of experience to be recorded
    Training records must be created

    Scenario: Course experience recording
      Given a user
      And a course group
      And an experience
      And a participant type "Student"
      Given the group confers the experience
      When the user is added as a member of the group with the participant type
      Then a training record is created referencing the user, group and participant type

    Scenario: Event experience recording
      Given a user
      And an event group
      And an experience
      And a participant type "Mentor"
      Given the group confers the experience
      When the user is added as a member of the group with the participant type
      Then a training record is created referencing the user, group and participant type

    Scenario: Special group experience recording
      Given a user
      And a special group
      And an experience
      And a participant type "Test"
      Given the group confers the experience
      And the participant type is a possible type for the group
      When the user is added as a member of the group with the participant type
      Then a training record is created referencing the user, group and participant type

    Scenario: Manual training recording
      Given a user
      And an experience
      When the user is recorded as having completed the experience with a note
      Then a training record is created with the note referencing the user

FEATURE;

  /**
   * @Given a user
   */
  public function aUser() {
    $edit = [];
    $edit['name'] = $this->randomMachineName();
    $edit['pass'] = \Drupal::service('password_generator')->generate();
    $edit['mail'] = $this->randomEmail();
    $edit['roles'] = ['authenticated', 'member'];
    $this->user = $this->createEntity('user', $edit);
  }

  /**
   * @Given an experience
   */
  public function anExperience() {
    $edit = [
      'type' => 'training_experience',
      'title' => 'An experience',
      'uid' => 1,
    ];
    $this->experience = $this->createEntity('node', $edit);
  }

  /**
   * @Given a participant type :type
   */
  public function aParticipantType($type) {
    if (strtolower($type) === 'student') {
      $this->participantType = $this->STUDENT_TERM_ID;
    }
    elseif (strtolower($type) === 'mentor') {
      $this->participantType = $this->MENTOR_TERM_ID;
    }
    else {
      $edit = [
        'vid' => 'participant_type',
        'name' => $type,
      ];
      $this->participantType = $this->createEntity('taxonomy_term', $edit)->id();
    }
  }

  /**
   * @Given the group confers the experience
   */
  public function theGroupConfersTheExperience() {
    $this->group->set('field_experiences_given', $this->experience->id());
    $this->group->save();
  }

  /**
   * @Given the participant type is a possible type for the group
   */
  public function theParticipantTypeIsAPossibleTypeForTheGroup() {
    $this->group->set('field_participant_types', $this->participantType);
    $this->group->save();
  }

  /**
   * @When the user is added as a member of the group with the participant type
   */
  public function theUserIsAddedAsAMemberOfTheGroup() {
    $membership = ['field_participant_type' => $this->participantType];
    $this->group->addMember($this->user, $membership);
  }

  /**
   * @Then a training record is created referencing the user, group and participant type
   */
  public function aTrainingRecordIsCreatedReferencingTheUserAndGroup() {
    $this->assertTrainingRecordWasCreated($this->user->id(), $this->experience->id(), $this->group->id(), $this->participantType);
  }

  /**
   * @When the user is recorded as having completed the experience with a note
   */
  public function theUserIsRecordedAsHavingCompletedTheExperienceWithANote() {
    $values = [
      'name' => 'A training record',
      'uid' => $this->user->id(),
      'experience' => $this->experience->id(),
      'note' => [
        'text_format' => 'basic_html',
        'value' => 'A note for the training record'
      ]
    ];
    $this->createEntity('training_record', $values);
  }

  /**
   * @Then a training record is created with the note referencing the user
   */
  public function aTrainingRecordIsCreatedWithTheNoteReferencingTheUser() {
    $values = [
      'uid' => $this->user->id(),
      'experience' => $this->experience->id()
    ];
    $result = $this->loadEntityByProperties('training_record', $values, TRUE);
    $this->assertNotFalse(
      $result,
      sprintf('A training record should have been created')
    );
    $this->assertEquals(
      'A note for the training record',
      $result->get('note')->getValue()[0]['value'],
      sprintf('The training record should contain the note')
    );
  }

}
