<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class TrainingCascadingRecordsTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected static $feature = <<<'FEATURE'
Feature: Cascading experience records
    In order for composite experiences to be recorded
    Records must be automatically created when component experiences are present

    Background:
      Given experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
      And a user named "Fred" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceC |
      And a user named "Jane" has training records for:
        | experience  |
        | ExperienceB |
        | ExperienceC |
      And a user named "Jack" has no training records

    Scenario: No auto completion
      Given a new experience "ExperienceD" has no auto-completion conditions
      Then "Fred" is not determined to have completed Experience "D"
      When the cascading experiences queue runs
      Then "Fred" is not determined to have completed Experience "D"
      And "Jane" is not determined to have completed Experience "D"
      And "Jack" is not determined to have completed Experience "D"

    Scenario: Single condition auto completion
      Given a new experience "ExperienceD" has auto-completion conditions
        | experience  |
        | ExperienceB |
      Then "Jane" is not determined to have completed Experience "D"
      When the cascading experiences queue runs
      Then "Jane" is determined to have completed Experience "D"
      And "Fred" is not determined to have completed Experience "D"
      And "Jack" is not determined to have completed Experience "D"

    Scenario: Complex auto-completion conditions
      Given an experience "ExperienceD" has auto-completion conditions
        | experience               |
        | ExperienceB              |
        | ExperienceA, ExperienceC |
      When the cascading experiences queue runs
      Then "Fred" is determined to have completed Experience "D"
      And "Jane" is determined to have completed Experience "D"
      And "Jack" is not determined to have completed Experience "D"

    Scenario: Simple chain autocompletion
      Given an experience "ExperienceD" has auto-completion conditions
        | experience  |
        | ExperienceC |
      Given an experience "ExperienceE" has auto-completion conditions
        | experience  |
        | ExperienceD |
      Given an experience "ExperienceF" has auto-completion conditions
        | experience               |
        | ExperienceD, ExperienceB |
      When the cascading experiences queue runs
      Then "Jane" is determined to have completed Experience "D"
      And "Jane" is determined to have completed Experience "E"
      And "Jane" is determined to have completed Experience "F"

    Scenario: Complex chain autocompletion
      Given a new experience "ExperienceD" has auto-completion conditions
        | experience  |
        | ExperienceC |
      Given an experience "ExperienceE" has auto-completion conditions
        | experience  |
        | ExperienceD |
      Given an experience "ExperienceF" has auto-completion conditions
        | experience               |
        | ExperienceD, ExperienceB |
      Given an experience "ExperienceG" has auto-completion conditions
        | experience               |
        | ExperienceE, ExperienceF |
      When the cascading experiences queue runs
      Then "Jane" is determined to have completed Experience "D"
      And "Jane" is determined to have completed Experience "E"
      And "Jane" is determined to have completed Experience "F"
      And "Jane" is determined to have completed Experience "G"

    Scenario: Becoming completed
      Given a new experience "ExperienceD" has auto-completion conditions
        | experience  |
        | ExperienceC |
      Then "Jack" is not determined to have completed Experience "D"
      When "Jack" is recorded as completing Experience "A"
      Then "Jack" is not determined to have completed Experience "D"
      When "Jack" is recorded as completing Experience "C"
      Then "Jack" is determined to have completed Experience "D"

    Scenario: Becoming completed by Complex chain
      Given a new experience "ExperienceD" has auto-completion conditions
        | experience  |
        | ExperienceA |
      Given an experience "ExperienceE" has auto-completion conditions
        | experience  |
        | ExperienceD |
      Given an experience "ExperienceF" has auto-completion conditions
        | experience               |
        | ExperienceD, ExperienceB |
      Given an experience "ExperienceG" has auto-completion conditions
        | experience               |
        | ExperienceE, ExperienceF |
      When "Jane" is recorded as completing Experience "A"
      Then "Jane" is determined to have completed Experience "D"
      And "Jane" is determined to have completed Experience "E"
      And "Jane" is determined to have completed Experience "F"
      And "Jane" is determined to have completed Experience "G"

FEATURE;

  /**
   * @Given a new experience :title has no auto-completion conditions
   */
  public function aNewExperienceHasNoAutoCompletionConditions($title) {
    $edit = [
      'title' => $title,
      'type' => 'training_experience',
      'uid' => 1,
    ];
    $this->experiences[$title] = $this->createEntity('node', $edit);
  }

  /**
   * @Given a new experience :title has auto-completion conditions
   * @Given an experience :title has auto-completion conditions
   */
  public function aNewExperienceHasAutoCompleteConditions($title, TableNode $table) {
    $cEntities = [];
    foreach($table->getHash() as $row) {
      $conditions = [];
      $name = $row['experience'];
      if (strpos($name, ',') !== false) {
        $parts = explode(', ', $name);
        foreach ($parts as $part) {
          $conditions[] = ['target_id' => $this->experiences[$part]->id()];
        }
      } else {
        $conditions[] = ['target_id' => $this->experiences[$name]->id()];
      }
      $trainingConditions = $this->createEntity('training_condition', [
        'experiences' => $conditions
      ]);
      $cEntities[] = ['target_id' => $trainingConditions->id()];
    }
    $this->experiences[$title] = $this->createEntity('node', [
      'type' => 'training_experience',
      'title' => $title,
      'field_automatic_conditions' => $cEntities,
      'uid' => 1,
    ]);
  }

  /**
   * @Then :name is determined to have completed Experience :arg
   */
  public function isDeterminedToHaveCompletedExperience($name, $arg) {
    $participant_id = $this->users[$name]->id();
    $training_id = $this->experiences['Experience'.$arg]->id();
    $this->assertTrainingRecordWasCreated($participant_id, $training_id);
  }

  /**
   * @Then :name is not determined to have completed Experience :arg
   */
  public function isNotDeterminedToHaveCompletedExperience($name, $arg) {
    $participant_id = $this->users[$name]->id();
    $training_id = $this->experiences['Experience'.$arg]->id();
    $this->assertTrainingRecordWasNotCreated($participant_id, $training_id);
  }

  /**
   * @When :name is recorded as completing Experience :arg
   */
  public function isRecordedAsCompletingExperience($name, $arg) {
    $edit = [];
    $edit['name'] = 'Experience'.$arg;
    $edit['experience'] = $this->experiences['Experience'.$arg]->id();
    $edit['uid'] = $this->users[$name]->id();
    $this->createEntity('training_record', $edit);
    $this->assertTrainingRecordWasCreated($this->users[$name]->id(), $this->experiences['Experience'.$arg]->id());
  }

  /**
   * @When the cascading experiences queue runs
   */
  public function theCascadingExperienceQueueRuns() {
    $this->processAdvancedQueue('ahs_training_update_cascading_records_for_user');
  }

}
