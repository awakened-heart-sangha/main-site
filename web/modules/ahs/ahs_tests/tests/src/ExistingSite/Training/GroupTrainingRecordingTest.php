<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Training;

use Drupal\ahs_training\Entity\TrainingRecord;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group ClonedDatabaseRequired
 */
class GroupTrainingRecordingTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected static $feature = <<<'FEATURE'
Feature: Group training completion
    In order for progress through the training via groups to be recorded
    Training records must be created for participants on group completion

    Scenario: Multiple participants have group experience recorded
      Given a group that confers an experience
      And several group students
      And several group mentors
      And several group observers
      Then the students are recorded as having the experience
      And the mentors are recorded as having the experience
      And the observers are not recorded as having the experience

    Scenario: Single participant has group experience recorded
      Given a group that confers an experience
      And a group student
      And a group mentor
      And a group observer
      Then the student is recorded as having the experience
      And the mentor is recorded as having the experience
      And the observer is not recorded as having the experience

    Scenario: Participants without mentor or observer has experience recorded
      Given a group that confers an experience
      And a group student
      Then the student is recorded as having the experience
      When the student is removed from the group
      Then the student is not recorded as having the experience

    Scenario: Multiple experiences are recorded
      Given a group that confers two experiences
      And a group student
      Then the student is recorded as having the experiences
      When the student is removed from the group
      Then the student is not recorded as having the experiences

FEATURE;

  /**
   * @Given a group that confers an experience
   */
  public function aGroupThatConfersAnExperience() {
    $this->group = $this->createGroup(['type' => 'course']);
    $edit = [
      'type' => 'training_experience',
      'title' => 'An experience',
    ];
    $this->experiences[] = $this->createEntity('node', $edit);
    $this->group->set('field_experiences_given', $this->experiences[0]);
    $this->group->save();
  }

  /**
   * @Given a group that confers two experiences
   */
  public function aGroupThatConfersTwoExperiences() {
    $this->group = $this->createGroup(['type' => 'course']);
    $edit = [
      'type' => 'training_experience',
      'title' => 'An experience',
    ];
    $this->experiences[] = $this->createEntity('node', $edit);
    $this->experiences[] = $this->createEntity('node', $edit);
    $this->group->set('field_experiences_given', $this->experiences);
    $this->group->save();
  }

  /**
   * @Given several group students
   */
  public function severalGroupStudents() {
    $this->students = $this->createSomeGroupMembers($this->group, TrainingRecord::STUDENT_TERM_ID, 4);
  }

  /**
   * @Given several group mentors
   */
  public function severalGroupMentors() {
    $this->mentors = $this->createSomeGroupMembers($this->group, TrainingRecord::MENTOR_TERM_ID, 4);
  }

  /**
   * @Given several group observers
   */
  public function severalGroupObservers() {
    $this->observers = $this->createSomeGroupMembers($this->group, TrainingRecord::OBSERVER_TERM_ID, 4);
  }

  /**
   * @Given a group student
   */
  public function aGroupStudent() {
    $this->students = $this->createSomeGroupMembers($this->group, TrainingRecord::STUDENT_TERM_ID, 1);
  }

  /**
   * @Given a group mentor
   */
  public function aGroupMentor() {
    $this->mentors = $this->createSomeGroupMembers($this->group, TrainingRecord::MENTOR_TERM_ID, 1);
  }

  /**
   * @Given a group observer
   */
  public function aGroupObserver() {
    $this->observers = $this->createSomeGroupMembers($this->group, TrainingRecord::OBSERVER_TERM_ID, 1);
  }

  /**
   * @Then the students are recorded as having the experience(s)
   * @Then the student is recorded as having the experience(s)
   */
  public function theStudentsAreRecordedAsHavingCompletedTheTraining() {
    $this->assertGroupRecordsCreated($this->students, TrainingRecord::STUDENT_TERM_ID);
  }

  /**
   * @Then the mentors are recorded as having the experience(s)
   * @Then the mentor is recorded as having the experience(s)
   */
  public function theMentorsAreRecordedAsHavingCompletedTheTraining() {
    $this->assertGroupRecordsCreated($this->mentors, TrainingRecord::MENTOR_TERM_ID);
  }

  protected function assertGroupRecordsCreated(array $users, $type) {
    foreach ($users as $user) {
      foreach ($this->experiences as $experience) {
        $this->assertTrainingRecordWasCreated($user->id(), $experience->id(), $this->group->id(), $type);
      }
    }
  }

  /**
   * @Then the observers are not recorded as having the experience(s)
   * @Then the observer is not recorded as having the experience(s)
   */
  public function theObserversAreNotRecordedAsHavingCompletedTheTraining() {
    $this->assertGroupRecordsNotExist($this->observers);
  }

  /**
   * @Then the students are not recorded as having the experience(s)
   * @Then the student is not recorded as having the experience(s)
   */
  public function theStudentsAreNotRecordedAsHavingCompletedTheTraining() {
    $this->assertGroupRecordsNotExist($this->students);
  }

  protected function assertGroupRecordsNotExist(array $users) {
    foreach ($users as $user) {
      foreach ($this->experiences as $experience) {
        $this->assertTrainingRecordWasNotCreated($user->id(), $experience->id(), $this->group->id());
      }
    }
  }

  /**
   * @When the student(s) is/are removed from the group
   */
  public function theStudentisRemovedFromTheGroup() {
    foreach ($this->students as $student) {
      $this->group->removeMember($student);
    }
  }

}
