<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Misc;

trait EmailObfuscationTrait
{

  // The created group.
  protected $group;

  /**
   * @Given an event with the description :desc
   */
  public function anEventWithTheDescription($desc) {
    $edit = [
      'type' => 'event',
      'field_description' => [
        'value' => $desc,
        'format' => 'basic_html'
      ]
    ];
    $this->group = $this->createGroup($edit);
  }

  /**
   * @When I visit the event
   */
  public function iVisitTheEvent() {
    $this->drupalGet('/group/' . $this->group->id());
    $this->assertSession()->pageTextContains('For questions contact');
  }
}
