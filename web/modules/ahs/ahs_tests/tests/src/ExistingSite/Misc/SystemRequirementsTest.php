<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Misc;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\system\SystemManager;

class SystemRequirementsTest extends ExistingSiteBehatBase {

  protected $requirements;

  protected static $feature = <<<'FEATURE'
Feature: System requirements are satisfied
    In order to ensure the system is working properly
    There should be no system errors reported

    Scenario: There are no system errors
      Then there are no system errors reported

FEATURE;

  protected $allowedErrors = [
    'config_inspector_report' => [],
    'update_contrib'=> [],
    'rebuild access'=> [],
    'sendgrid_integration'=> [],
    'trusted_host_patterns'=> [],
    'configuration_files'=> [],
    'update_core'=> [],
    'coverage_core'=> [],
      '*' => [
      'Unresolved dependency',
    ],
  ];

  protected function setUp(): void {
    parent::setUp();
    // Logging failures temporarily disabled see https://gitlab.com/awakened-heart-sangha/main-site/-/issues/691
    $this->failOnLogsLevel = 0;
    $this->requirements = \Drupal::service('system.manager')->listRequirements();
  }

  /**
   * @Then there are no system errors reported
   */
  public function thereAreNoSystemErrorsReported() {
    $filter = function($requirement, $key) {
      if (isset($requirement['severity']) && $requirement['severity'] === SystemManager::REQUIREMENT_ERROR) {
        // This error is allowed for this module.
        if (isset($this->allowedErrors[$key]) && (empty($this->allowedErrors[$key]) || in_array((string) $requirement['title'], $this->allowedErrors[$key]))) {
          return FALSE;
        }
        // Errors of this type are allowed for any module
        elseif (in_array((string) $requirement['title'], $this->allowedErrors['*'])) {
          return FALSE;
        }
        // It's an unallowed error.
        return TRUE;
      }
      // It's not an error.
      return FALSE;
    };

    $errors = array_filter($this->requirements, $filter, ARRAY_FILTER_USE_BOTH);
    array_walk($errors, function(&$value, $key) {
      $value = [
        'title' => (string) $value['title'],
        'description' => (string) $value['value'],
      ];
    });
    $this->assertEmpty($errors, "There should be no requirement errors except for those whitelisted: " . print_r($errors, TRUE));
  }


}

