<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Misc;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;

class EmailObfuscationNoJsTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use GroupTrait;
  use EmailObfuscationTrait;

  protected $inVisualRegressionHide = ["div.field--name-field-dates"];

  protected static $feature = <<<'FEATURE'
Feature: Emails are obfuscated for non-js users
    In order to prevent published emails from being harvested for spam
    Emails should be obfuscated and invisible without js

    Scenario: Emails are obfuscated in events descriptions without js
      Given an event with the description "For questions contact test@ahs.org.uk"
      When I visit the event
      Then I should not see "test@ahs.org.uk"

FEATURE;




}

