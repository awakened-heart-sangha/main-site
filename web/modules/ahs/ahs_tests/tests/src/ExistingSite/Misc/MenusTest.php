<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Misc;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

/**
 * @group ClonedDatabaseRequired
 */
class MenusTest extends ExistingSiteBehatBase {

  use UserTrait;
  use UserDefinitions;
  use BrowsingDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Main site menus
    In order to help users navigate
    The main site menus should show appropriate content

    Scenario: Different menu for anonymous and authenticated in frontend
      Given I am not logged in
      And I visit "<front>"
      Then I should see the menu for anonymous users
      And I should not see the frontend menu for authenticated users
      When I log in
      And I visit "training"
      Then I should not see the menu for anonymous users
      And I should see the frontend menu for authenticated users
      And I should not see an "Admin" item in the frontend menu

    Scenario: Main menu in backend
      Given I am logged in as a "student_support"
      And I visit "/training"
      Then I should not see the menu for anonymous users
      And I should see the frontend menu for authenticated users
      And I should see an "Admin" item in the frontend menu
      When I visit "/admin"
      Then I should see the backend menu for authenticated users
      And I should see an "Admin" item in the backend menu

    Scenario: Admin page shows menu
      Given I am logged in as a "student_support"
      When I visit "/admin"
      Then I should see a link to "/admin/people" in the page
      And I should not see a link to "/admin/commerce/orders" in the page

    Scenario: Admin page shows menu
      Given I am logged in as an "accountant"
      When I visit "/admin"
      Then I should see a link to "/admin/people" in the page
      And I should see a link to "/admin/commerce/orders" in the page

    Scenario: User menu name is not overcached
      Given I am logged in as an "accountant"
      When I visit "/admin"
      Then I should see my name in the user menu
      When I log in as a different "accountant"
      And I visit "/admin"
      Then I should see my name in the user menu


FEATURE;


  /**
   * @Then I should see the menu for anonymous users
   */
  public function iShouldSeeTheMenuForAnonymousUsers() {
    $this->assertSession()->elementExists('css', '#block-ahs-bootstrap-main-menu');
  }


  /**
   * @Then I should not see the menu for anonymous users
   */
  public function iShouldNotSeeTheMenuForAnonymousUsers() {
    $this->assertSession()->elementNotExists('css', '#block-ahs-bootstrap-main-menu');
  }

  /**
   * @Then I should see the frontend menu for authenticated users
   */
  public function iShouldSeeTheFrontendMenuForAuthenticatedUsers() {
    $this->assertSession()->elementExists('css', '#block-mainnavigationauthenticated-3');
  }

  /**
   * @Then I should not see the frontend menu for authenticated users
   */
  public function iShouldNotSeeTheFrontendMenuForAuthenticatedUsers() {
    $this->assertSession()->elementNotExists('css', '#block-mainnavigationauthenticated-3');
  }

  /**
   * @Then I should see the backend menu for authenticated users
   */
  public function iShouldSeeTheBackendMenuForAuthenticatedUsers() {
    $this->assertSession()->elementExists('css', '#block-mainnavigationauthenticated');
  }

  /**
   * @Then I should see a/an :item item in the frontend menu
   */
  public function iShouldSeeAnItemInTheFrontendMenu($item) {
    $this->assertSession()->elementTextContains('css', '.region-navigation-collapsible', $item);
  }

  /**
   * @Then I should not see a/an :item item in the frontend menu
   */
  public function iShouldNotSeeAnItemInTheFrontendMenu($item) {
    $this->assertSession()->elementTextNotContains('css', '.region-navigation-collapsible', $item);
  }

  /**
   * @Then I should see a/an :item item in the backend menu
   */
  public function iShouldNotSeeAnItemInTheBackendMenu($item) {
    $this->assertSession()->elementTextContains('css', '#block-mainnavigationauthenticated', $item);
  }

  /**
   * @Then I should see a link to :href in the page
   */
  public function iShouldSeeALinkTo($href) {
    $link = $this->getLinkInPageContent($href);
    $this->assertNotEmpty($link, "A link to $href should have been found in the page content region.");
  }

  protected function getLinkInPageContent($href) {
    $content = $this->getSession()
      ->getPage()
      ->find('css', '.region-content');
    $link = $content->find('xpath', "//a[contains(@href, '$href')]");
    return $link;
  }

  /**
   * @Then I should not see a link to :href in the page
   */
  public function iShouldNotSeeALinkTo($href) {
    $link = $this->getLinkInPageContent($href);
    $this->assertEmpty($link, "A link to $href should not have been found in the page content region.");
  }

  /**
   * @Then I should see my name in the user menu
   */
  public function iShouldSeeMyNameInTheUserMenu() {
    $this->assertSession()->elementTextContains('css', '#block-ahsadminaccountmenu', $this->getMe()->getDisplayName());
  }

}

