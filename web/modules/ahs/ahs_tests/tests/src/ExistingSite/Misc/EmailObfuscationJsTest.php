<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Misc;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;

/**
 * @group js
 */
class EmailObfuscationJsTest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use GroupTrait;
  use EmailObfuscationTrait;

  protected $inVisualRegressionHide = ["div.field--name-field-dates"];

  protected static $feature = <<<'FEATURE'
Feature: Emails are visible to real users with js
    In order to prevent published emails from being harvested for spam
    Emails should be visible if you have js

    Scenario: Emails are visible in events descriptions with js
      Given an event with the description "For questions contact test@ahs.org.uk"
      When I visit the event
      Then I should see "test@ahs.org.uk"

FEATURE;

}

