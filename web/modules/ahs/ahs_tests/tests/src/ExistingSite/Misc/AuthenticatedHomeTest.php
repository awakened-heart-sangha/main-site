<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Behat\Mink\Exception\ElementNotFoundException;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group NewDatabaseRequired
 * @group js
 */
class AuthenticatedHomeTest extends ExistingSiteJsBehatBase  {

  use TrainingTrait;

  protected $course_template;
  protected $course;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Home page
    In order to give personalise experience
    Anonymous & authenticated need different home pages

    Background:
      Given a future course
      And experiences:
        | experience  |
        | ExperienceC |
      And the course has no eligibility conditions
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceC |

    Scenario: Anonymous home page
      Given I am not logged in
      When I visit "<front>"
      Then I should not see "Registered"
      And I should not see "Suggested"

    Scenario: Registered & suggested courses on home page
      Given I am logged in
      When I visit "<front>"
      Then I should see "Registered"
      And I should see "Suggested"
      And I should not see the course as "Registered"
      And I should not see the course as "Suggested"
      When I am given training records for:
        | experience  |
        | ExperienceC |
      And I visit "<front>"
      Then I should see "Suggested"
      And I should see the course as "Suggested"
      And I should not see the course as "Registered"
      When I join the course
      And I visit "<front>"
      Then I should see the course as "Registered"
      And I should not see the course as "Suggested"

    Scenario: Unpublished courses on home page
      Given I am logged in
      And an unpublished future suggested course
      When I visit "<front>"
      And I should not see the course as "Registered"
      Then I should see "Suggested"
      When I join the course
      And I visit "<front>"
      Then I should not see the course as "Registered"
      And I should not see the course as "Suggested"

    Scenario: Courses, events & templates on home page
      # Waits between group creation are necesary to ensure consistent views sort order for visual regression.
      Given I wait 1 second
      And a future course
      And the course is suggested
      And I wait 1 second
      And a future event
      And the event is suggested
      And I wait 1 second
      And a course_template group
      And the course_template is suggested
      And I am logged in as a "member"
      When I visit "<front>"
      Then I should not see the course as "Registered"
      And I should not see the event as "Registered"
      And I should not see the course_template as "Registered"
      And I should see "Suggested"
      Then I should see the course as "Suggested"
      And I should see the event as "Suggested"
      And I should see the course_template as "Suggested"
      And the course_template should be listed last

FEATURE;

  /**
   * @Given I should see the :bundle as :block
   */
  public function iShouldSeeTheGroupAs($bundle, $block) {
    $this->assertSession()->elementExists('css', ".block-views:contains('$block')");
    $this->assertSession()->elementTextContains('css', ".block-views:contains('$block')", $this->group->label());
  }

  /**
   * @Given I should not see the :bundle as :block
   */
  public function iShouldNotSeeTheGroupAs($bundle, $block) {
    try {
      $this->assertSession()->elementTextNotContains('css', ".block-views:contains('$block')", $this->group->label());
    }
    catch (ElementNotFoundException $e) {}
  }

  /**
   * @Given I wait :n second(s)
   */
  public function iSleepSecond($n) {
    sleep($n);
  }

  /**
   * @Then the course_template should be listed last
   */
  public function theCourseTemplateShouldBeListedLast() {
    $suggested = $this->getCurrentPage()->find('css', ".view-id-user_possible_groups.view-display-id-block_home");
    $rows = $suggested->findAll('css', '.views-row');
    $last = end($rows);
    $label = $this->course_template->label();
    $lastText = $last->getText();
    $this->assertTrue(strpos($lastText, $label) !== FALSE, "The last row should contain '$label' but is '$lastText'.");
  }

}

