<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Enrolment;

use Drupal\Tests\ahs_tests\Traits\RetryTrait;

/**
 * @group ClonedDatabaseRequired
 * @group js
 */
class EnrolmentFromPageTestSkip extends EnrolmentTestBase {

  use RetryTrait;

  /*
   * Retry any failing test 5 times, as the long page sometimes makes the browser crash.
   */
  protected $retry = 5;

  protected $enrolmentFormUrl = "training";

  protected static $feature = <<<'FEATURE'
Feature: Enrolment from training page
    In order for students to enrol
    The webform needs to be on the training page

    Scenario: Enrolment from training page
      When I submit an enrolment form
      Then an enrolment is started for me

FEATURE;

  // Don't mess with the webform and products as we're on (a clone of) the production db.
  protected function setUpWebform() {}

  protected function submitForm(array $edit, $submit, $form_html_id = NULL) {
    sleep(10);
    parent::submitForm($edit, $submit, $form_html_id);
  }
}

