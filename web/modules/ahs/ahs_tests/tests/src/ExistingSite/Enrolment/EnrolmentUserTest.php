<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Enrolment;

use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;

/**
 * @group js
 */
class EnrolmentUserTest extends EnrolmentTestBase {

  use QueueTrait;

  // How many emails are sent to me immediately when I enrol.
  protected $immediateMailCount;

  protected $inVisualRegressionHide = [".field--name-field-dates"];
  protected $storeId;
  protected $subscriptionInitialOrderId;
  protected $subscriptionFirstRecurringOrderId;

  protected static $feature = <<<'FEATURE'
Feature: Enrolment checkout with payment
    In order for students to join LAH
    They need to be able to enrol and setup a donation

    Scenario: New users can enrol
      When a month passes
      When I submit an enrolment form
      Then an enrolment is started for me
      And a new user should have been created for me
      When I complete the enrolment payment steps
      Then the order should be in the paid state
      And the order should have a billing profile
      And my user should be the customer on the enrolment order
      And a first donation should be received from me
      And a monthly membership donation should be setup for me
      And I should have an about profile
      When a month passes
      Then another donation should be received from me
      And no recurring payment receipt was sent

    Scenario: Existing decoupled users can enrol
      Given I am already a user but without a password
      When I submit an enrolment form
      Then an enrolment is started for me
      When I complete the enrolment payment steps
      Then the order should be in the paid state
      And the order should have a billing profile
      And my user should be the customer on the enrolment order
      And a first donation should be received from me
      And a monthly membership donation should be setup for me
      And I should have an about profile
      When a month passes
      Then another donation should be received from me
      And no recurring payment receipt was sent

    Scenario: Existing decoupled users can enrol using alternative email
      Given I am already a user but without a password
      And I have an alternative email
      When I submit an enrolment form with my alternative email
      Then an enrolment is started with my alternative email
      When I complete the enrolment payment steps
      Then the order should be in the paid state
      And the order should have a billing profile
      And my user should be the customer on the enrolment order
      And a first donation should be received from me
      And a monthly membership donation should be setup for me
      And I should have an about profile
      When a month passes
      Then another donation should be received from me
      And no recurring payment receipt was sent

    Scenario: Existing coupled users who are already logged in can enrol
      Given I am already a user with a password
      And I am logged in
      When I submit an enrolment form
      Then an enrolment is started for me
      And my user should be the customer on the enrolment order
      When I complete the enrolment payment steps
      Then the order should be in the paid state
      And the order should have a billing profile
      And a first donation should be received from me
      And a monthly membership donation should be setup for me
      And I should have an about profile
      When a month passes
      Then another donation should be received from me
      And no recurring payment receipt was sent

    Scenario: Existing coupled users who are not logged in can enrol
      Given I am already a user with a password
      When I submit an enrolment form
      Then an enrolment is started for me
      And I should be required to login
      When I login at checkout
      Then my user should be the customer on the enrolment order
      When I complete the enrolment payment steps
      Then the order should be in the paid state
      And the order should have a billing profile
      And my user should be the customer on the enrolment order
      And a first donation should be received from me
      And a monthly membership donation should be setup for me
      And I should have an about profile
      When a month passes
      Then another donation should be received from me
      And no recurring payment receipt was sent

    Scenario: Expired cards work OK on enrolment recurring orders
      When I submit an enrolment form
      Then an enrolment is started for me
      When I complete the enrolment payment steps
      Then a first donation should be received from me
      And my card expires in two weeks
      And a month passes
      # The payment method expiry is not updated on Stripe, so card should still work.
      Then another donation should be received from me

    Scenario Outline: Enrolment recurring payments have correct amounts
      When I submit an enrolment form specifying "<amount>" per month
      Then an enrolment is started for me
      When I complete the enrolment payment steps
      Then a first donation of "<amount>" should be received from me
      And a monthly membership donation for "<amount>" per month should be setup for me
      When a month passes
      Then another donation of "<amount>" should be received from me
      And no recurring payment receipt was sent

      Examples:
      | amount |
      | 30     |
      | 50     |

FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->setUpQueues();
  }

  public function tearDown(): void {
    $this->tearDownQueues();
    parent::tearDown();
  }

  /**
   * @Then a monthly membership donation should be setup for me
   * @Then a monthly membership donation for :amount per month should be setup for me
   */
  public function aMonthlyMembershipDonationShouldBeSetupForMe(float $amount = NULL) {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $amount = is_null($amount) ? (float) $this->amount : $amount;
    $sku = 'ahs_membership_monthly';
    $scheduleId = 'monthly';
    $this->assertSubscriptionCreatedFromInitialOrder($order, $amount, $this->currency, $sku, $scheduleId);
  }

    /**
   * @Then my user should be the customer on the enrolment order
   */
  public function myUserShouldBeTheCustomerOnTheEnrolmentOrder() {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertGreaterThan(0, $order->getCustomerId(), "The order should have a customer.");
    $this->assertSame($this->getMe()->id(), $order->getCustomerId(), "The order should have the specificied user as the customer.");
  }

  /**
   * @Then there should not yet be a customer on the enrolment order
   */
  public function thereShouldNotYetBeACustomerOnTheEnrolmentOrder() {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertEmpty($order->getCustomerId(), "The order should not yet have a customer.");
  }

  /**
   * @When a month passes
   */
  public function aMonthPasses() {
    $this->immediateMailCount = count($this->getMail(['to' => $this->getMyEmail()]));
    $this->aMonthPassesAfterEnrolment();
  }

  /**
   * @Then another donation should be received from me
   * @Then another donation of :amount should be received from me
   */
  public function anotherDonationShouldBeReceivedFromMe($amount = NULL) {
    $enrolmentOrder = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertRecurringOrderCompleted($enrolmentOrder, $amount, 0);
  }

  /**
   * @Then the order should have a billing profile
   */
  public function theOrderShouldHaveABillingProfile($amount = NULL) {
    $enrolmentOrder = $this->loadEntity('commerce_order', $this->orderId);
    $profile = $enrolmentOrder->getBillingProfile();
    $this->assertBillingProfile($profile, $this->getMe());

    // Order profile should be added to customer address book.
    $addressBookProfile = $this->loadEntityByProperties('profile', ['type' => 'customer', 'uid' => $enrolmentOrder->getCustomer()->id()]);
    $this->assertNotEmpty($addressBookProfile, "The enrolment customer profile should have been placed in the address book");
  }

  /**
   * @Then no recurring payment receipt was sent
   */
  public function noRecurringPaymentReceiptWasSent()
  {
    $actualMail = $this->getMail(['to' => $this->getMyEmail()]);
    $this->assertMailCount($actualMail, $this->immediateMailCount);
  }

  /**
   * @When my card expires in two weeks
   */
  public function myCardExpiresInTwoWeeks() {
    $enrolmentOrder = $this->loadEntity('commerce_order', $this->orderId);
    $paymentMethod = $enrolmentOrder->get('payment_method')->entity;

    // Set the payment method to have just expired.
    // Do this directly via the database to avoid any hooks that might update
    // the payment method remotely.
    $now = \Drupal::service('datetime.time')->getCurrentTime();
    $twoWeeksAgo = $now + (14 * 24 * 60 * 60);
    //$paymentMethod->setExpiresTime($now - 10)

    \Drupal::database()->update('commerce_payment_method')
      ->fields(array('expires' => $twoWeeksAgo))
      ->condition('method_id', $paymentMethod->id())
      ->execute();
    $storage = $this->entityManager->getStorage('commerce_payment_method');
    $storage->resetCache([$paymentMethod->id()]);
    // In case the payment method is cached on the order.
    $storage = $this->entityManager->getStorage('commerce_order');
    $storage->resetCache([$enrolmentOrder->id()]);
    $this->reloadEntity($enrolmentOrder);
  }

  /**
   * @When I submit an enrolment form with my alternative email
   */
  public function iSubmitAnEnrolmentFormWithMyAlternativeEmail($amount = 30) {
    $this->submitEnrolmentForm($this->getMyAlternativeEmail(TRUE), $amount);
  }

  /**
   * @Then an enrolment is started with my alternative email
   */
  public function anEnrolmentIsStartedWithMyAlternativeEmail() {
    $this->assertEnrolmentOrderByEmail($this->getMyAlternativeEmail());
  }


}

