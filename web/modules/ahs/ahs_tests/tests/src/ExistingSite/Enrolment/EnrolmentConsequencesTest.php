<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Enrolment;


use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\OneTimeLoginTrait;
use Drupal\Tests\ahs_tests\Traits\QueueTrait;

/**
 * @group ClonedDatabaseRequired
 * @group js
 */
class EnrolmentConsequencesTest extends EnrolmentTestBase {

  use QueueTrait;
  use OneTimeLoginTrait;
  use BrowsingDefinitions;

  protected $storeId;

  protected $inVisualRegressionHide = [
    ".field--name-field-dates",
    "body.path-frontpage .region-content"
  ];

  protected static $feature = <<<'FEATURE'
Feature: Enrolment consequences
    In order for students to start LAH once they enrol
    The enrolment needs to have consequences

    Scenario: Enrolment has consequences
      When I submit an enrolment form
      Then an enrolment is started for me
      And a new user should have been created for me
      When I complete the enrolment payment steps
      Then the order should be in the paid state
      And I should be a Sangha member
      And I should be a member of the DHB group
      And I should have an about profile
      And I should have a start date
      And I should have a "Getting Started" course
      And the page header contains "Getting Started"
      And support should get a notification email
      And I should get a welcome email
      When I follow the one-time login link in the email
      Then I can set my password and login
      And materials should be ordered for me
      When the Stripe customer data queue is processed
      Then my customer details are set on Stripe

#    Scenario: Re-enrolment has consequences
#      Given I have enrolled
#      And I am logged in
#      When I enrol again with a different address
#      Then the order should be in the paid state
#      And I should still be a Sangha member
#      And I should have only a single membership of the DHB group
#      And my about profile should contain the new contact details
#     And my start date should not have changed

    #Scenario: Re-enrolment subscription
    #  Given I have enrolled giving 20 per month
    #  And I submit an enrolment form specifying 30 per month
    #  And I complete the enrolment payment steps
    #  Then I have only a single active membership subscription for 30

FEATURE;

  protected $startDate;
  protected $registerEmail;

  protected function setUp(): void {
    parent::setUp();
    $this->setUpQueues();
  }

  /**
   * @Then I should (still )be a Sangha member
   */
  public function iShouldBeASanghaMember() {
    $this->assertTrue($this->getMe()->hasRole('member'), 'I should have the member role.');
  }

  /**
   * @Then I should be a member of the DHB group
   */
  public function iShouldBeAMemberOfTheDHBGroup() {
    $this->assertTrue($this->group->getMember($this->getMe()) !== FALSE, 'I should be a member of the group.');
  }

  /**
   * @Then I should get a welcome email
   */
  public function iShouldGetAWelcomeEmail() {
    $mails = $this->getMail(['to' => $this->getMyEmail()]);
    $this->assertMailCount($mails, 1);
    $amount = $this->loadEntity('commerce_order', $this->orderId)->getTotalPrice()->getNumber();
    $amount = \Drupal::service('commerce_price.number_formatter')->format($amount, ['minimum_fraction_digits' => 2]);
    $properties = [
      'subject' => 'Welcome',
      'body' => [
        $this->name['given_name'],
        "£$amount. ",
      ],
      'reply-to' => 'support@ahs.org.uk'
    ];

    $this->assertTrue($this->matchesMail($mails[0], $properties), sprintf("'Mail should have the right subject & body':\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($mails, 2000), print_r($properties, TRUE)));
    $this->assertEmailHasOneTimeLoginLink($mails[0]);

//    $properties = [
//      'subject' => [
//        'An administrator created an account for you at Awakened Heart Sangha'
//      ],
//      'body' => [
//        'A site administrator at Awakened Heart Sangha has created an account for you.',
//        'This link can only be used once to log in and will lead you to a page where'
//      ],
//    ];
//    $this->assertTrue($this->matchesMail($mails[0], $properties), sprintf("'Mail should have the right subject & body':\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($mails, 2000), print_r($properties, TRUE)));
  }

  /**
   * @Then support should get a notification email
   */
  public function supportShouldGetANotificationEmail() {
    $mails = $this->getMail(['to' => 'support@ahs.org.uk']);
    $this->assertMailCount($mails, 1);
    $properties = [
      'subject' => 'New enrolment: ' . $this->name['given_name'] . ' ' . $this->name['family_name'],
      'body' => [
        $this->getMyEmail(),
        $this->phone,
        ],
    ];
    $this->assertTrue($this->matchesMail($mails[0], $properties), sprintf("'Mail should have the right subject & body':\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($mails, 1000), print_r($properties, TRUE)));
 }

  /**
   * @When I enrol again with a different address
   */
  public function iEnrolAgainWithADifferentAddress() {
    // Set new address and phone number
    $this->address['address_line1'] = '12 West Brook Ave';
    $this->address['locality'] = 'Harrogate';
    $this->address['postal_code'] = 'HG2 0PT';
    $this->phone = '020 7856 7324';

    $this->amount = '50';

    $this->iSubmitAnEnrolmentForm($this->amount);
    $this->aReEnrolmentIsStartedForMe();
    $this->iCompleteTheEnrolmentPaymentSteps();

  }

  function aReEnrolmentIsStartedForMe() {
    // Simulate user having a membership start date 1 year ago
    //$profile = $this->loadEntityByProperties('profile', ['uid' => $this->getMe()->id(), 'type' => 'admin']);
    //$simDate = new DrupalDateTime();
    //$simDate->modify('-1 year');
    //$this->startDate = $simDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    //$profile->set('field_membership_start', $this->startDate);
    //$profile->save();
    // Get most recent order
    $storage = $this->entityTypeManager->getStorage('commerce_order');
    $orders = $storage->loadByProperties(['mail' => $this->myEmail, 'type' => 'ahs_lah_enrolment']);
    $order = end($orders);
    $this->assertNotEquals($order->id(), $this->orderId, 'The re-enrollment order should be different to the original enrolment order');
    $this->assertOrder(
      $order,
      'ahs_lah_enrolment',
      'ahs_lah_enrolment',
      $this->amount,
      [$this->membershipSku],
      'draft'
    );
    $this->orderId = $order->id();
  }

  /**
   * @Then I should have only a single membership of the DHB group
   */
  public function iShouldHaveOnlyASingleMembershipOfTheDhbGroup() {
    $members = $this->group->getMembers();
    $count = 0;
    foreach ($members as $member) {
      if ($member->getUser()->id() == $this->getMe()->id()) {
        $count++;
      }
    }
    $this->assertEquals(
      '1',
      $count,
      'You should have 1 membership of the DHB group'
    );
  }

  /**
   * @Then my about profile should contain the new contact details
   */
  public function myAboutProfileShouldContainTheNewContactDetails() {
    $profile = $this->getMe()->get('about_profiles')->entity;
    $this->assertEntityType($profile, 'profile');
    $profile = $this->reloadEntity($profile);
    $this->assertSame($this->phone, $profile->field_telephone_home->value, 'Phone number on about profile should be the new one supplied during re-enrolment.');
  }

  /**
   * @Then my start date should not have changed
   */
  public function myStartDateShouldNotHaveChanged() {
    $profile = $this->getMe()->get('admin_profiles')->entity;
    $this->assertEntityType($profile, 'profile');
    $profile = $this->reloadEntity($profile);
    $membership_start = $profile->get('field_membership_start')->date->format('Y-m-d');
    $this->assertEquals($this->startDate, $membership_start, 'Membership start date should not have changed');
  }

  /**
   * @Then I should have a start date
   */
  public function iShouldHaveAStartDate() {
    $profile = $this->getMe()->get('admin_profiles')->entity;
    $this->assertEntityType($profile, 'profile');
    $profile = $this->reloadEntity($profile);
    $membership_start = $profile->get('field_membership_start')->date;
    $this->assertNotEmpty($membership_start, 'User should have a membership start date');
    // Check start date is recent.
    $now = DrupalDateTime::createFromTimestamp(\Drupal::service('datetime.time')->getCurrentTime());
    $interval = $membership_start->diff($now);
    $seconds = $interval->format('%s');
    $this->assertTrue($seconds < 600, 'Start date should be recent');
    if (!isset($this->startDate)) {
      $profile->set('field_membership_start', '2005-10-01')->save();
      $this->startDate = '2005-10-01';
    }
  }

  /**
   * @When the Stripe customer data queue is processed
   */
  public function theQueueIsProcessed() {
    $this->processAdvancedQueue('ahs_commerce_stripe_customer_update');
  }

  /**
   * @Then materials should be ordered for me
   */
  public function materialsShouldBeOrderedForMe() {
    $skus = ['book_hom', 'book_ocs', 'book_dhb_course_companion'];
    $this->assertOrderByEmail($this->myEmail, 'ahs_free', 'default', 0, $skus, 'fulfillment');
  }

  /**
   * @Then I should have a :title course
   */
  public function iShouldHaveACourse($title) {
    $loader = \Drupal::service('group.membership_loader');
    $memberships = $loader->loadByUser($this->getMe());
    $hasTitleMembership = FALSE;
    foreach($memberships as $membership) {
      $group = $membership->getGroup();
      if ($group->bundle() === 'course' && $group->get('field_template')->target_id == 592) {
        $hasTitleMembership = TRUE;
      }
    }
    $this->assertTrue($hasTitleMembership, "I should have a 'Getting Started' course.");
  }

}

