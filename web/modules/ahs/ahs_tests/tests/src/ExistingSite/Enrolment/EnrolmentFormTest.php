<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Enrolment;

/**
 * @group js
 */
class EnrolmentFormTest extends EnrolmentTestBase {

  protected $inVisualRegressionHide = [".field--name-field-dates"];
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Enrolment steps
    In order for students to ensure enrolments don't waste our time
    The enrolment form needs to challenge people offering low amounts

    Scenario: Default enrolment amount
      When I visit an enrolment form
      And I select "£40" as the monthly amount
      Then the submit button should be enabled
      And I should not see the low amount notice
      When I complete the form
      Then I should be on the About step of the checkout
      And an enrolment is started for me
      When I complete the enrolment payment steps
      Then a first donation of "40" should be received from me

    Scenario: Other enrolment amount more than 20
      When I visit an enrolment form
      And I select "Other amount"
      And I give "25" as the amount
      Then the submit button should be enabled
      And I should not see the low amount notice
      When I complete the form
      Then I should be on the About step of the checkout
      And an enrolment is started for me
      When I complete the enrolment payment steps
      Then a first donation of "25" should be received from me

    Scenario: Other enrolment amount less than 20
      When I visit an enrolment form
      And I select "Other amount"
      And I give "10" as the amount
      Then the submit button should not be enabled
      And I should see the low amount notice
      When I complete the form
      Then I should not be on the About step of the checkout

FEATURE;

  /**
   * @Given a Getting started page
   */
  public function aGettingStartedPage() {
    $page = $this->loadEntityByProperties('node',
      [
        'type' => 'information',
        'title' => 'Getting started'
        ]);
    if (!$page) {
      $this->createNode([
        'type' => 'information',
        'title' => 'Getting started',
      ]);
    }
  }

  /**
   * @Then I should be on the Getting started page
   */
  public function iShouldBeOnTheGettingStartedPage() {
    $this->assertSession()->elementExists('css', 'article[about="/getting-started"]');
  }

  /**
   * @Then the submit button should be enabled
   */
  public function theSubmitButtonShouldBeEnabled() {
    $page = $this->getCurrentPage();
    $submit_btn = $page->find('css', "#edit-actions");
    $state = $submit_btn->getAttribute('disabled');
    $this->assertEquals(
      NULL,
      $state,
      "The submit button should be enabled"
    );
  }

  /**
   * @Then the submit button should not be enabled
   */
  public function theSubmitButtonShouldNotBeEnabled() {
    $page = $this->getCurrentPage();
    $submit_btn = $page->find('css', "#edit-actions");
    $state = $submit_btn->getAttribute('disabled');
    $this->assertEquals(
      'disabled',
      $state,
      "The submit button should not be enabled"
    );
  }

  /**
   * @When I select :value as the monthly amount
   * @When I select :value
   */
  public function iSelectAsTheMonthlyAmount($value) {
    $this->amount = ($value != 'Other amount') ? str_replace('£', '', $value) : NULL;
    $select_value = ($value != 'Other amount') ? str_replace('£', '', $value) : '_other_';
    $page = $this->getSession()->getPage();
    $options = $page->find('css', '#edit-monthly-donation-select');

    $select = $page->find('xpath', "//option[text() = '".$value."']");
    $this->assertNotEmpty(
      $select,
      sprintf('Select option "%s" should be available', $value)
    );
    $options->selectOption($select_value);
    if($value === 'Other amount') {
      $this->assertSession()->waitForElementVisible('css', "div.js-webform-select-other-input", 3000);
      sleep(3);
    }
  }

  /**
   * @When I visit an enrolment form
   */
  public function iVisitAnEnrolmentForm() {
    $this->drupalGet("/" . $this->enrolmentFormUrl);
    $this->waitToPassHoneypot();
  }

  /**
   * @When I complete the form
   */
  public function iCompleteTheForm() {
    $page = $this->getCurrentPage();
    $this->myEmail = $this->randomEmail();
    $page->fillField('Email', $this->myEmail);
    $page->findButton('Join today')->click();
  }

  /**
   * @Then I should not see the low amount notice
   */
  public function iShouldNotSeeTheLowAmountNotice() {
    $this->assertSession()->pageTextNotContains('If you\'re considering giving less than');
  }

  /**
   * @Then I should see the low amount notice
   */
  public function iShouldSeeTheLowAmountNotice() {
    $this->assertSession()->pageTextContains('If you\'re considering giving less than');
  }

}
