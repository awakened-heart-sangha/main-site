<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Enrolment;

use Drupal\Tests\ahs_tests\Traits\ContentTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\EnrolmentTrait;
use Drupal\Tests\ahs_tests\Traits\EntityTrait;
use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_recurring\Entity\BillingSchedule;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\WebformTrait;
use Drupal\Tests\ahs_tests\Traits\FormTrait;

class EnrolmentTestBase extends ExistingSiteJsBehatBase {

  use UserDefinitions;
  use CommerceDefinitions;
  use EntityTrait;
  use CommerceTrait;
  use MailCollectionTrait;
  use WebformTrait;
  use FormTrait;
  use ContentTrait;
  use GroupTrait;
  use EnrolmentTrait;

  protected $amount;

  protected $orderId;

  protected $webFormSettings = [];

  protected $currency = 'GBP';

  protected $group;

  protected function setUp(): void {
    parent::setUp();
    $this->startMailCollection();
    $this->group = $this->ensureDHBGroups();
    $this->setUpWebform();
  }

  public function tearDown(): void {
    if ($user = $this->loadEntityByProperties('user', ['mail' =>$this->getMyEmail()])) {
      $this->markEntityForCleanup($user);
    }

    // Restore original webform settings.
    $this->tearDownWebform();
    $this->restoreMailSettings();
    parent::tearDown();
  }

  protected function tearDownWebform() {
    $this->tearDownWebformSettings();
  }

  /**
   * @When I give :value as the amount
   */
  public function iGiveAsTheAmount($value) {
    $this->amount = $value;
    $page = $this->getSession()->getPage();
    $amount_field = $page->find('css', "#edit-monthly-donation-other");
    $amount_field->setValue($value);
  }

  /**
   * @When I submit my credit card details
   */
  public function iSubmitMyCreditCardDetails() {
    $this->submitCreditCardDetails('Donate now');
  }

  /**
   * @When I submit my accepted credit card details that require 3DS
   */
  public function iSubmitMyAcceptedCreditCardDetailsThatRequire3DS() {
    $this->submitCreditCardDetails('Donate now', self::$CARD_BASIC_ACCEPTED_3DS_REQUIRED);
  }

  /**
   * @When I submit my declined credit card details that require 3DS
   */
  public function iSubmitMyDeclinedCreditCardDetailsThatRequire3DS() {
    $this->submitCreditCardDetails('Donate now', self::$CARD_BASIC_DECLINED_3DS_REQUIRED);
  }

  /**
   * @Then I should not be on the About step of the checkout
   */
  public function iShouldNotBeOnTheAboutStepOfTheCheckout() {
    $this->assertSession()->pageTextNotContains('About you');
  }

  /**
   * @Then a new user should have been created for me
   */
  public function aNewUserShouldHaveBeenCreatedForMe() {
    $user = $this->loadEntityByProperties('user', ['mail' => $this->getMyEmail()]);
    $this->assertNotEmpty($user, "A user with the specified email should exist.");
    $this->setMe($user);
  }

  /**
   * @Then I should have an about profile
   */
  public function iShouldHaveAnAboutProfile() {
    $profile = $this->loadEntityByProperties('profile', ['uid' => $this->getMe()->id(), 'type' => 'about']);
    $this->assertEntityType($profile, 'profile');
    $profile = $this->reloadEntity($profile);
    $this->assertSame($this->name['given_name'], $profile->field_name->given, 'Given name on about profile should be that supplied during enrolment.');
    $this->assertSame($this->name['family_name'], $profile->field_name->family, 'Family name on about profile should be that supplied during enrolment.');
    $this->assertSame($this->phone, $profile->field_telephone_home->value, 'Phone number on about profile should be that supplied during enrolment.');
    $profileFromUser = $this->getMe()->get("about_profiles")->entity;
    $this->assertNotEmpty($profileFromUser, "About profile should be available from about_profiles field on user.");
    $this->assertEquals($profileFromUser->id(), $profile->id(), "Profile in about_profiles should be same as that loaded independently");
  }

}

