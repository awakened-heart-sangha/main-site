<?php

namespace Drupal\Tests\ahs_tests\ExistingSite;

use Drupal\Tests\ahs_tests\Traits\ScreenshotTrait;
use weitzman\DrupalTestTraits\ExistingSiteSelenium2DriverTestBase as DTTExistingSiteSelenium2DriverTestBase;
use Drupal\Tests\ahs_tests\Traits\BaseTrait;
use Drupal\Tests\ahs_tests\Traits\VisualRegressionTrait;
use Psr\Log\LoggerInterface;

/**
 * Base class for AHS site tests that need webdriver.
 */
abstract class ExistingSiteJsBase extends DTTExistingSiteSelenium2DriverTestBase implements LoggerInterface
{
  use BaseTrait;
  use ScreenshotTrait;
  use VisualRegressionTrait;

  protected function setUp(): void {
    parent::setUp();
    $this->setUpBase();
    $this->setUpVisualRegression();
  }

  public function tearDown(): void {
    $this->tearDownVisualRegression();
    $this->tearDownBase();
    parent::tearDown();
    $this->releaseMemory();
  }

  protected function debugPage($name = '') {
    // Don't try to take screenshot if no session.
    if (!$this->hasSession()) {
      return;
    }
    $directory = $this->getDebugDirectory();
    $this->debugBasic($name, $directory);
    $this->screenshotPage($directory, $name);
  }

}
