<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;

class CourseTemplateStartingTest extends ExistingSiteBehatBase  {

  use GroupCloningTrait;

  protected $course_template;
  protected $cloned_course;
  protected $paragraph;

  protected static $feature = <<<'FEATURE'
Feature: Starting from a course template
    In order for participants to study template courses
    They need to start their own course based on the template

    Scenario: Anonymous users cannot start course templates
      Given a course_template group called "test_template"
      And the course_template is open
      And the course_template uses normal registration
      When I go to the course_template start page
      Then I should not see "test_template"

    Scenario: Unregisterable course templates cannot be started
      Given a course_template group called "test_template"
      And the course_template is not open
      And I am logged in as a member
      When I go to the course_template start page
      Then I should not see "test_template"

    Scenario: Starting a course from a course template
      Given a course_template group called "test_template"
      And the course_template has an expected duration of 4 weeks
      And the course_template is open
      And the course_template uses normal registration
      And a published public paragraph on the group
      And I am logged in as a member
      When I go to the course_template start page
      Then I should see "Start test_template"
      And I should not see "Type" in the content region
      And I should not see "Students" in the content region
      And I should not see "Content" in the content region
      And I should not see "Duration" in the content region
      When I press "Start"
      Then a cloned course is created from the course_template
      And the course_template is referenced from the cloned course
      And the cloned course has the same title as the course_template
      And the cloned course is published
      And the cloned course is private
      And the cloned course is not open for registration
      And the cloned course starts today
      And the cloned course ends in 3 weeks
      And I should see "Thankyou for registering"
      And the course_template paragraph has been cloned to the course
      And the cloned course paragraph is published
      And I am the owner of the cloned course
      And I am a member of the cloned course
      And I am not a member of the course_template

FEATURE;

}

