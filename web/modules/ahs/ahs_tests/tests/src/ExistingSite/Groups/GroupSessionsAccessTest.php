<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\user\Entity\User;

class GroupSessionsAccessTest extends ExistingSiteBehatBase  {

  use GroupDefinitions;
  use GroupTrait;
  use UserTrait;
  use BrowsingDefinitions;
  use UserDefinitions;

  protected $node;
  protected $media;
  protected $event;
  protected $paragraph;
  protected $course;

  protected static $feature = <<<'FEATURE'
Feature: Group members can access group sessions
    In order for participants to access event recordings and course materials
    Appropriate access needs to be granted to group members

    Background:
      Given an event group
      And a course group
      And a session

    Scenario: Unique session validation error
      Given the session is part of the event
      And I am logged in as a "administrator"
      When I am creating a new group session
      And I submit a new group session
      Then I should see a unique group session validation error

    Scenario: Access to unpublished sessions
      Given the session is part of the event
      And the session is unpublished
      Then an anonymous user cannot view the session
      And an authenticated user cannot view the session
      And a participant in the event cannot view the session
      And an admin of the event can view the session
      And an archivist can view the session
      And a participant in the event cannot update the session
      And an admin of the event can update the session
      And an archivist can update the session
      And an archivist can delete the session

    Scenario: Access to published sessions
      Given the session is part of the event
      And the session is published
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then a participant in the event can view the session
      Then an admin of the event can view the session
      Then an archivist can view the session
      Then a curator can view the session
      And a participant in the event cannot update the session
      And an admin of the event can update the session
      And an archivist can update the session
      And an events_admin can update the session
      And an archivist can delete the session
      And an events_admin can delete the session

    Scenario: Access to published sessions via published paras
      Given the session is part of the event
      And the session is published
      And the session is referenced from a course paragraph that is published
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then a participant in the event can view the session
      Then an admin of the event can view the session
      And a participant in the event cannot update the session
      And an admin of the event can update the session
      Then a participant in the course can view the session
      Then an admin of the course can view the session
      Then an archivist can view the session
      Then a curator can view the session
      And a participant in the course cannot update the session
      And an admin of the course cannot update the session
      And an archivist can update the session

    Scenario: Access to published sessions via unpublished paras
      Given the session is part of the event
      And the session is published
      And the session is referenced from a course paragraph that is unpublished
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then a participant in the course cannot view the session
      # Not yet implemented: Then an admin of the course cannot view the session
      Then an archivist can view the session
      And a participant in the course cannot update the session
      And an admin of the course cannot update the session
      And an archivist can update the session

    Scenario: Access to unpublished sessions via unpublished paras
      Given the session is part of the event
      And the session is unpublished
      And the session is referenced from a course paragraph that is unpublished
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then a participant in the course cannot view the session
      # We don't care about this: Then an admin of the course cannot view the session
      Then an archivist can view the session
      And a participant in the course cannot update the session
      And an admin of the course cannot update the session
      And an archivist can update the session

    Scenario: Access to unpublished sessions via published paras
      Given the session is part of the event
      And the session is unpublished
      And the session is referenced from a course paragraph that is published
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then a participant in the course cannot view the session
      # We don't care about this: Then an admin of the course can view the session
      Then an archivist can view the session
      And a participant in the course cannot update the session
      And an admin of the course cannot update the session
      And an archivist can update the session

    Scenario: Removal of access on paragraph unpublishing
      Given the session is part of the event
      And the session is published
      And the session is referenced from a course paragraph that is published
      Then a participant in the course can view the session
      When the course paragraph is unpublished
      Then a participant in the course cannot view the session
      When the course paragraph is published
      Then a participant in the course can view the session

    Scenario: Access is possible if there is no primary group
      # This scenario should never happen, but you never know...
      Given the session is published
      And the session is referenced from a course paragraph that is published
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then a participant in the course can view the session
      Then an admin of the course can view the session
      And an archivist can view the session
      And a participant in the course cannot update the session
      And an admin of the course cannot update the session
      And an archivist can update the session

    Scenario: Access to sessions from restricted events
      Given the session is part of the event
      And the session is published
      And the event is restricted
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then an archivist cannot view the session
      Then a curator cannot view the session
      Then an archivist cannot update the session
      Then a participant in the event can view the session
      Then an admin of the event can view the session
      And a participant in the event cannot update the session
      And an admin of the event can update the session

    Scenario: Access to referenced sessions from restricted events
      Given the session is part of the event
      And the session is published
      And the event is restricted
      And the session is referenced from a course paragraph that is published
      Then an anonymous user cannot view the session
      Then an authenticated user cannot view the session
      Then a participant in the event can view the session
      Then an admin of the event can view the session
      And a participant in the event cannot update the session
      And an admin of the event can update the session
      Then a participant in the course can view the session
      Then an admin of the course can view the session
      Then an archivist cannot view the session
      Then a curator cannot view the session
      And a participant in the course cannot update the session
      And an admin of the course cannot update the session
      And an archivist cannot update the session

    Scenario: Access to sessions from edited paras
      Given the session is part of the event
      And the session is published
      Then a participant in the course cannot view the session
      When the session is added to an existing course paragraph that is published
      Then a participant in the course can view the session
      When the session is removed from the course paragraph
      Then a participant in the course cannot view the session


FEATURE;

  /**
   * @Given a session
   */
  public function aSessionIsPartOfTheEvent() {
    $this->node = $this->createEntity('node', ['type' => 'session', 'title' => $this->randomMachineName()]);

    $this->media = $this->createEntity('media', ['bundle' => 'audio']);
    $this->node->set('field_media', $this->media);
    $this->node->save();
  }


  /**
   * @Given the session is part of the event
   */
  public function theSessionIsPartOfTheEvent() {
    $this->event->addRelationship($this->node, 'group_node:session');
    // Assert the session is correctly defaulted to belonging primarily to this event.
    $content = $this->event->getRelationships('group_node:session');
    $content = reset($content);
    $this->assertEquals($this->node->id(), $content->getEntity()->id());
    $this->assertTrue((bool) $content->get('field_primary')->value);
  }

  /**
   * @Given the event is restricted
   */
  public function theEventIsRestricted() {
    $this->event->set('field_restricted', TRUE)->save();
  }

  /**
   * @Given the session is :published
   */
  public function theSessionIsPublished($published) {
    $status = $published === 'published';
    if ($status) {
      $this->node->setPublished()->save();
    }
    else {
      $this->node->setUnpublished()->save();
    }
  }

  /**
   * @When I am creating a new group session
   */
  public function iAmCreatingANewGroupSession() {
    $this->drupalGet("/group/{$this->event->id()}/content/create/group_node:session");

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Add Group node (Session)');

    $page = $this->getSession()->getPage();
    $title_field = $page->find('css', "#edit-title-0-value");

    $title_field->setValue($this->node->label());
  }

  /**
   * @When I submit a new group session
   */
  public function iPressSave() {
    $this->getSession()->getPage()->find('css', '#edit-submit')->click();
  }


  /**
   * @Then I should see a unique group session validation error
   */
  public function iShouldSeeAValidationError() {
    $title = $this->node->label();
    $message = "A session with the title '{$title}' and date 'unspecified' already exists.";
    $this->assertSession()->pageTextContains($message);
  }

  /**
   * @Given the session is referenced from a course paragraph that is :published
   */
  public function theSessionIsReferencedFromACourseParagraph($published) {
    $this->paragraph = Paragraph::create([
      'type' => 'sessions',
      'field_sessions' => [$this->node],
    ]);
    $this->paragraph->setParentEntity($this->course, 'field_content');
    if($published === 'published') {
      $this->paragraph->setPublished();
    }
    else {
      $this->paragraph->setUnpublished();
    }
    $this->paragraph->save();
    $this->course->set('field_content', [$this->paragraph])->save();
  }

  /**
   * @When the course paragraph is :published
   */
  public function theCourseParagraphIsPublished($published) {
    if($published === 'published') {
      $this->paragraph->setPublished();
    }
    else {
      $this->paragraph->setUnpublished();
    }
    $this->paragraph->setNewRevision(TRUE);
    $this->paragraph->save();
    $this->course->set('field_content', [$this->paragraph])->save();
  }

  /**
   * @Then an anonymous user cannot :operation the session
   */
  public function anAnonymousUserCannotAccessTheSession($operation) {
    $user = User::getAnonymousUser();
    $this->assertSessionAccess($operation, $user, FALSE);
  }

  /**
   * @Then an authenticated user cannot :operation the session
   */
  public function anAuthenticatedUserCannotAccessTheSession($operation) {
    $user = $this->createUser();
    $this->assertSessionAccess($operation, $user, FALSE);
  }

  /**
   * @Then a/an :role in/of the :bundle :can :operation the session
   */
  public function aRoleInTheBundleCanAccessTheSession($role, $bundle, $can, $operation) {
    $expected = $can === 'can';
    $user = $this->createUser();
    $this->{$bundle}->addMember($user);
    if ($role !== 'participant') {
      $membership = $this->{$bundle}->getMember($user)->getGroupRelationship();
      $membership->group_roles[] = "$bundle-$role";
      $membership->save();
    }
    $this->assertSessionAccess($operation, $user, $expected);
  }

  /**
   * @Then a/an :role :can :operation the session
   */
  public function aRoleCanAccessTheSession($role, $can, $operation) {
    $expected = $can === 'can';
    $user = $this->createUser();
    $user->addRole($role);
    $user->save();
    $this->assertSessionAccess($operation, $user, $expected);
  }

  protected function assertSessionAccess($operation, $user, $expected) {
    $not = $expected ? '' : 'not';
    $this->assertSame($expected, $this->node->access($operation, $user), "'$operation' access to the node should $not be permitted.");
    $this->media = $this->reloadEntity($this->media);
    if ($operation === 'view') {
      $this->assertSame($expected, $this->media->access($operation, $user), "'$operation' access to the media should $not be permitted.");
    }
  }

  /**
   * @When the session is added to an existing course paragraph that is published
   */
  public function theSessionIsAddedtoACourseParagraph() {
    $this->paragraph = Paragraph::create([
      'type' => 'sessions',
      'field_sessions' => [],
    ]);
    $this->paragraph->setPublished();
    $this->paragraph->save();
    $this->course->set('field_content', [$this->paragraph])->save();

    $this->paragraph->set('field_sessions', [$this->node]);
    $this->paragraph->save();
    $this->course->save();
  }

  /**
   * @When the session is removed from the course paragraph
   */
  public function theSessionIsRemovedFromTheCourseParagraph() {
    $this->paragraph->set('field_sessions', []);
    $this->paragraph->save();
    $this->course->save();
  }

}

