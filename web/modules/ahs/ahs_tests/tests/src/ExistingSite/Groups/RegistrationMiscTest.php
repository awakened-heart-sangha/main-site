<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Core\Url;
use Drupal\registration\Entity\Registration;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\commerce_price\Price;
use Drupal\registration\HostEntityInterface;

class RegistrationMiscTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use GroupDefinitions;
  use UserDefinitions;
  use GroupTrait;
  use UserTrait;
  use RegistrationTrait;

  protected $course;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Group registration miscellaneous functionality
    In order to allow participant to join courses & events
    They can register themselves

    Scenario: Editing registration for a closed group
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And the event registration deadline has passed
      And I am logged in
      And I have registered for the event variation "Online"
      When I go to edit my registration for the event variation "Online"
      Then I should not see "Access denied"
      And I should see "Save Registration"

    Scenario: Editing registration for a finished group
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And the event has ended
      And I am logged in
      And I have registered for the event variation "Online"
      When I go to edit my registration for the event variation "Online"
      Then I should not see "Save Registration"
      And I should see "Access denied"

    Scenario: Selecting registration host as first step of editing
      Given an event group
      And the event registration system is "registration"
      And there is an "Online variation A" variation for the event with type "online"
      And there is an "Online variation B" variation for the event with type "online"
      And I am logged in
      And I have registered for the event variation "Online variation A"
      When I go to edit my registration for the event variation "Online variation A"
      Then I should not see "Save Registration"
      And I should not see "Access denied"
      And I should see "Online variation A"
      And I should see "Online variation B"
      When I click "Online variation A"
      And I press "Save Registration"
      Then I am still registered for the event variation "Online variation A"

    Scenario: Changing registration host
      Given an event group
      And the event registration system is "registration"
      And there is an "Online variation A" variation for the event with type "online"
      And there is an "Online variation B" variation for the event with type "online"
      And I am logged in
      And I have registered for the event variation "Online variation A"
      When I go to edit my registration for the event variation "Online variation A"
      Then I should not see "Save Registration"
      And I should not see "Access denied"
      And I should see "Online variation A"
      And I should see "Online variation B"
      When I click "Online variation B"
      Then I should not see "Access denied"
      And I press "Save and confirm"
      Then I am registered for the event variation "Online variation B"

FEATURE;
  /**
   * @Given I have registered for the :bundle variation :variation
   */
  public function iHaveRegisteredForTheGroup($bundle, $variation) {
    $host = $this->getRegistrationHostByName($this->{$bundle}, $variation);
    $this->assertInstanceOf(HostEntityInterface::class, $host);
    $registration = Registration::create([
      'type' => 'online',
      'entity_id' => $host->id(),
      'entity_type_id' => $host->getEntityTypeId(),
      'user_uid' => $this->getMe()->id(),
    ]);
    $registration->save();
  }

  /**
   * @When I go to edit my registration for the :bundle variation :variation
   */
  public function iGoToEditMyRegistration($bundle, $variation) {
    $host = $this->getRegistrationHostByName($this->{$bundle}, $variation);
    $this->assertInstanceOf(HostEntityInterface::class, $host);
    $storage = \Drupal::entityTypeManager()->getStorage('registration');
    $registrations = $storage->loadByProperties([
      'entity_id' => $host->id(),
      'entity_type_id' => $host->getEntityTypeId(),
      'user_uid' => $this->getMe()->id(),
    ]);
    $this->assertNotEmpty($registrations, "There should be a registration for " . $host->label());
    $registration = reset($registrations);
    $this->drupalGet($registration->toUrl('edit-form'));
  }

  /**
   * @Then I am (still )registered for the :bundle variation :variation
   */
  public function iAmRegisteredForTheGroup($bundle, $variation) {
    $host = $this->getRegistrationHostByName($this->{$bundle}, $variation);
    $storage = \Drupal::entityTypeManager()->getStorage('registration');
    $registrations = $storage->loadByProperties([
      'entity_id' => $host->id(),
      'entity_type_id' => $host->getEntityTypeId(),
      'user_uid' => $this->getMe()->id(),
    ]);
    $this->assertCount(1, $registrations, "There should be a single regsistration for '{$variation}'.");
  }

}
