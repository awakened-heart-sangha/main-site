<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Core\Url;
use Drupal\registration\Entity\Registration;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

class GroupRegistrationTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use GroupDefinitions;
  use UserDefinitions;
  use GroupTrait;
  use UserTrait;
  use RegistrationTrait;

  protected $course;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Group registration
    In order to allow participant to join courses & events
    They can register themselves

    Scenario: Registration not open
      Given a course group
      And the course is not open
      And I am logged in
      When I go to register for the course
      # Access denied
      Then I should not see the course title

    Scenario: Event registration
      Given an event group
      And the event is open
      And I am logged in
      And my name is "Fred Bloggs"
      When I go to register for the event
      Then I should see the event title
      When I press "Register"
      Then I become a participant in the event with the type "student"
      And my name is still "Fred Bloggs"

    Scenario: Course registration
      Given a course group
      And the course is open
      And the course uses normal registration
      And I am logged in
      And my name is "Fred Bloggs"
      When I go to register for the course
      Then I should see the course title
      When I press "Register"
      Then I become a participant in the course with the type "student"
      And my name is still "Fred Bloggs"

    Scenario: Name is required for registration
      Given an event group
      And the event is open
      And I am logged in
      And my name is unknown
      When I go to register for the event
      Then I should see the event title
      When I enter "Fred Bloggs" as my name
      And I press "Register"
      Then I become a participant in the event with the type "student"
      And my name is now "Fred Bloggs"

FEATURE;

  /**
   * @When I go to register for the :bundle
   */
  public function iGoToRegisterForTheGroup($bundle) {
    $url = Url::fromRoute('entity.group.join', [
      'group' => $this->{$bundle}->id(),
    ])->toString();
    $this->drupalGet($url);
  }

  /**
   * @Then I should see the :bundle title
   */
  public function iShouldSeeTheGroupTitle($bundle) {
    $this->assertSession()->elementTextContains('css', 'h1', $this->{$bundle}->label());
  }

  /**
   * @Then I should not see the :bundle title
   */
  public function iShouldNotSeeTheGroupTitle($bundle) {
    $this->assertSession()->pageTextNotContains($this->{$bundle}->label());
  }

  /**
   * @When I enter ":name" as my name
   */
  public function iEnterAsMyName($name) {
    $nameParts = explode(" ", $name, 2);
    $page = $this->getCurrentPage();
    $page->fillField('name[given]', $nameParts[0]);
    $page->fillField('name[family]', $nameParts[1]);
  }

}
