<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\user\UserInterface;

trait RegistrationTrait {

  protected function getRegistrationHostByName(AhsGroup $group, $name) {
    $hosts = $group->getRegistrationHostEntities();
    /** @var \Drupal\registration\HostEntityInterface $host */
    foreach($hosts as $host) {
      if ($host->label() == $name) {
        return $host;
      }
    }
  }

  /**
   * @Then I become a participant in the :bundle with the type :type
   */
  public function iBecomeAParticipantInTheGroup($bundle, $type) {
    $membership = $this->{$bundle}->getMember($this->getMe());
    $this->assertNotEmpty($membership, "I should be a group member");
    $actualType = $membership->getGroupRelationship()->get('field_participant_type')->entity;
    $this->assertNotEmpty($actualType, "There should be a particpant type on the group membership");
    $actualTypeName = $actualType->getName();
    $this->assertSame(strtolower($type), strtolower($actualTypeName), "The participant type should be $type");
  }

  /**
   * @Given my name is ":name"
   */
  public function myNameIs($name) {
    $this->setUserFullName($this->getMe(), $name);
    $this->assertUserFullName($this->getMe(), $name);
  }

  /**
   * @Given my name is unknown
   */
  public function iDoNotHaveAName() {
    $fields = [
      'field_name' => NULL,
      'field_name_sangha' => NULL,
    ];
    $this->updateProfile($this->getMe(), 'about', $fields);
    $this->assertEmpty($this->getMe()->getName());
  }

  /**
   * @Then my name is now/still ":name"
   */
  public function myNameIsStill($name) {
    $this->assertUserFullName($this->getMe(), $name);
  }

  protected function assertUserFullName(UserInterface $user, $name) {
    $nameParts = explode(" ", $name, 2);
    $nameArray = [
      'given' => $nameParts[0],
    ];
    if (count($nameParts) > 1) {
      $nameArray['family'] = $nameParts[1];
    }

    $profile = $user->get('about_profiles')->entity;
    $this->assertFalse($profile->get('field_name')->isEmpty(), "Name field should not be empty.");
    $value = $profile->get('field_name')->getValue()[0];
    foreach($nameArray as $key => $expected) {
      $this->assertArrayHasKey($key, $value, "Name field should have name part '$key'. " . print_r($value, TRUE));
      $this->assertSame($expected, $value[$key]);
    }
  }


  /**
   * @Given the :bundle registration system is :system
   */
  public function theGroupRegistrationSystemIs($bundle, $system) {
    $this->{$bundle}->set('field_registration_system', $system)->save();
  }

  /**
   * @Given there is a/an :name variation for the :bundle with type :type
   */
  public function thereIsAVariationForTheGroupWithType($name, $bundle, $type) {
    $host = $this->getRegistrationHostByName($this->{$bundle}, $name);
    if (!$host) {
      $variation = ProductVariation::create([
        'type' => 'ahs_registration',
        'field_registration_type' => $type,
        'title' => $name,
        'price' => new Price(0, 'GBP'),
        'field_payment_type' => 'donation',
      ]);
      $variation->save();

      $product = Product::create([
        'type' => 'ahs_registration',
        'title' => $name,
      ]);
      $product->addVariation($variation);
      $product->save();

      $this->{$bundle}->get('field_product')->appendItem(['target_id' => $product->id()]);
      $this->{$bundle}->save();
    }
  }

}
