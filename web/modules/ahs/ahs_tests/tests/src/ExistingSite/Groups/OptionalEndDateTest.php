<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use \Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\EntityTrait;

/**
 * @group js
 */
class OptionalEndDateTest extends ExistingSiteJsBehatBase  {

  use BrowsingDefinitions;
  use UserDefinitions;
  use EntityTrait;

  protected static $feature = <<<'FEATURE'
Feature: Date range fields can have optional end dates

    Scenario: Courses can be created without end dates
      Given I can create courses
      When I go to "group/add/course"
      Then I should see "Add Course"
      When I enter "My course" as a title
      And I enter a start date
      And I press "Create Course"
      Then a new course is created with a NULL end date

FEATURE;

  /**
   * @Given I can create courses
   */
  public function iCanCreateCourses() {
    $this->setMe($this->createUser(['create course group']));
    $this->iLogin();
  }

  /**
   * @When I enter :value as a title
   */
  public function iEnterAsATitle($value) {
    $page = $this->getCurrentPage();
    $page->fillField('Title', $value);
  }

  /**
   * @When I enter a start date
   */
  public function iEnterAStartDate() {
    $page = $this->getCurrentPage();
    $page->find('css', '#edit-field-dates-0-value-date')->setValue('01012020');
    $page->find('css', '#edit-field-dates-0-value-time')->setValue('111111');
  }

  /**
   * @Then a new course is created with a null end date
   */
  public function aNewCourseIsCreatedWithANullEndDate() {
    $edit = [
      'label' => 'My Course',
      'field_dates' => [
        'value' => '2020-01-01T11:11:00',
        'end_value' => NULL
      ]
    ];
    $this->assertNotEmpty(
      $this->loadEntityByProperties('group', $edit),
      "A new course should have been created"
    );
  }

}

