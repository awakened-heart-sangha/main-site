<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * This doesn't need JS, and it's a poor fit for visual regression because it
 * depends a lot on the date.
 *
 * @group NewDatabaseRequired
 */
class EventsAdminListTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use UserDefinitions;
  use GroupTrait;

  protected static $feature = <<<'FEATURE'
Feature: Events staff can see coming events
    In order to see coming events
    Events staff need an admin view

    Background:
      Given events:
        | title      | start date   | end date   | published |
        | Event1     | -3 weeks     | -2 weeks   | yes       |
        | Event2     | +8 weeks     | +10 weeks  | yes       |
        | Event3     | -1 week      | +1 week    | yes       |
        | Event4     | +4 weeks     | +5 weeks   | no        |

    Scenario: Events admins can see published & unpublished events that have not yet finished
      Given I am logged in as an "events_admin"
      When I visit "admin/events"
      Then I should see the following events:
        | title  |
        | Event2 |
        | Event4 |
        | Event3 |
        | Event1 |

FEATURE;

  /**
   * @Given events:
   */
  public function events(TableNode $table) {
    foreach($table->getHash() as $row) {
      $start_date = new DrupalDateTime($row['start date']);
      $end_date = new DrupalDateTime($row['end date']);
      $edit = [
        'type' => 'event',
        'status' => ($row['published'] == 'yes') ? TRUE : FALSE,
        'field_dates' => [
          'value' => $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
          'end_value' => $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
        ],
      ];
      $edit['label'] = $row['title'];
      $this->createGroup($edit);
    }
  }

  /**
   * @Then I should see the following events:
   */
  public function iShouldSeeTheFollowingEvents(TableNode $table) {
    $tableHash = $table->getHash();
    $page = $this->getCurrentPage();
    $results = $page->findAll('css', "td.views-field-label:contains('Event')");
    $this->assertCount(
      count($tableHash),
      $results,
      "All events should be listed"
    );
    $i = 0;
    foreach ($results as $result) {
      $this->assertEquals(
        $tableHash[$i]['title'],
        $result->getText(),
        "Events should be listed in the correct order"
      );
      $i++;
    }

  }

}

