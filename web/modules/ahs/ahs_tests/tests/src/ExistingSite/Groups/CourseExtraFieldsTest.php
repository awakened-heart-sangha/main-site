<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * @group ClonedDatabaseRequired
 */
class CourseExtraFieldsTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $groups;
  protected $course;

  protected static $feature = <<<'FEATURE'
Feature: Training & joining info is shown when viewing a group

    Background:
      Given a course group
      And the course uses normal registration
      And experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
      And I am logged in as a member

    Scenario: Group participants cannot register
      Given the course is open
      And the course has not ended
      And I am a participant in the course
      And the course is suggested
      When I view the course group
      Then I should not see a registration button
      And I should see "You are registered"
      And I should not see "now closed"
      And I should not see "eligible"
      And I should not see "suggested"

    Scenario: Only open groups can be joined, but closed groups display eligibility
      Given the course is not open
      And the course has not ended
      And the course is suggested
      When I view the course group
      Then I should not see a registration button
      And I should not see "registration"
      And I should see "eligible"
      And I should not see "suggested"

    Scenario: Nonsuggested groups don't mention suggestion
      Given the course is open
      When I view the course group
      Then I should see a registration button
      And I should see "Everyone is eligible"
      And I should not see "suggested"

    Scenario: Groups can have registration deadline
      Given the course is open
      And the course registration deadline has not passed
      When I view the course group
      Then I should see "Registration is open until"

    Scenario: Groups cannot be joined after deadline, but still display eligibility
      Given the course is open
      And the course registration deadline has passed
      And the course has not ended
      And the course is suggested
      When I view the course group
      Then I should not see a registration button
      And I should see "now closed"
      And I should see "eligible"
      And I should not see "suggested"

    Scenario: Joining and eligibility are irrelevant after end
      Given the course is open
      And the course has ended
      And the course is suggested
      When I view the course group
      Then I should not see a registration button
      And I should not see "now closed"
      And I should not see "eligible"
      And I should not see "suggested"

    Scenario: Courses without end dates are joinable
      Given the course is open
      And the course has no end date
      When I view the course group
      Then I should see a registration button
      And I should see "Everyone is eligible"

    Scenario: Group membership status is not overcached between users
      Given the course is open
      When I view the course group
      Then I should see a registration button
      And I should not see "You are registered"
      When I log in as a different member
      And I am a participant in the course
      And I view the course group
      Then I should not see a registration button
      And I should see "You are registered"
      And I should not see "Everyone is eligible"

    Scenario: Group membership status is not overcached within user
      Given the course is open
      When I view the course group
      Then I should see a registration button
      And I should not see "You are registered"
      When I join the course
      And I view the course group
      Then I should not see a registration button
      And I should see "You are registered"
      And I should not see "Everyone is eligible"

    Scenario: Anonymous should login for more info
      Given I am not logged in
      When I view the course group
      Then I should see "Login to register for this"
      And I should not see a registration button
      And I should see "Everyone is eligible"
      And I should not see "may not be suggested"
      And I should not see "is suggested"
      Given the course is suggested
      When I view the course group
      Then I should not see "may not be suggested"
      And I should not see "login to check"

    Scenario: Eligibility conditions
      Given the course is open
      And the course is suggested
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceA |
      Given the course has suggestion conditions:
        | conditions  |
        | ExperienceB |
      When I view the course group
      Then I should not see a registration button
      And I should see "You are not eligible"
      And I should not see "suggested"

    Scenario: Suggestion conditions
      Given the course is open
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceA |
      When I view the course group
      Then I should see a registration button
      And I should see "Everyone is eligible"
      And I should not see "is suggested for everyone"
      And I should see "is not suggested for you"

    Scenario: Access denied for unpublished groups
      Given an unpublished future suggested course
      When I view the course group
      # Access denied
      Then I should not see the course group

FEATURE;

  /**
   * @Then I should see a registration button
   */
  public function iShouldSeeARegistrationButton() {
    $this->assertTrue($this->hasRegistrationButton());
  }

  /**
   * @Then I should not see a registration button
   */
  public function iShouldNotSeeARegistrationButton() {
    $this->assertFalse($this->hasRegistrationButton());
  }

  protected function hasRegistrationButton() {
    $page = $this->getSession()->getPage();
    $link = $page->find('named', ['link_or_button', "Register"]);
    return !empty($link);
  }

}
