<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use weitzman\DrupalTestTraits\Entity\NodeCreationTrait;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\FormTrait;

/**
 * @group js
 */
class EventsAdminEditTest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use UserDefinitions;
  use GroupDefinitions;
  use GroupTrait;
  use FormTrait;

  protected $venue;

  protected $inVisualRegressionHide = [
    "div.field--name-field-dates",
    "input#edit-field-leaders-0-target-id",
    "input#edit-field-venue-0-target-id"
  ];

  protected static $feature = <<<'FEATURE'
Feature: Events staff can see & edit events
    In order to administer events
    Events & archives staff need to be able to edit them

    Scenario Outline: Staff can create new event
      Given a venue called "Test venue"
      #And a user with the name "Fred Bloggs"
      And I am logged in as a "<role>"
      When I go to the add event form
      And I enter "Event1" as the event title
      And I select "Test venue" as the event venue
      #And I add "Fred Bloggs" as an event leader
      And I press "Create Event"
      Then a new event is created
      And the event has the title "Event1"
      And the event has the venue "Test venue"
      #And the event has the leader "Fred Bloggs"

      Examples:
        | role         |
        | events_admin |
        | archivist    |

     Scenario Outline: Staff can edit existing events
      Given an event with the title "Old title"
      And I am logged in as a "<role>"
      When I manage the group
      And I enter "New title" as the event title
      And I press "Save"
      Then the event has the title "New title"

      Examples:
        | role         |
        | events_admin |
        | archivist    |

    Scenario: Staff can edit events they are a member of
      Given an event with the title "Old title"
      And I am logged in as a "events_admin"
      And I am a member of the group
      When I manage the group
      And I enter "New title" as the event title
      And I press "Save"
      Then the event has the title "New title"

FEATURE;

  // The created group
  protected $group;

  /**
   * @Given a venue called :name
   */
  public function aVenueCalled($name) {
    $values = ['type' => 'venue', 'title' => $name];
    $this->venue = $this->createEntity('node', $values);
  }

  /**
   * @When I go to the add event form
   */
  public function iGoToTheAddEventForm() {
    $this->drupalGet("/group/add/event");
    $this->assertSession()->addressEquals('group/add/event');
    $this->assertSession()->pageTextContains('Add Event');
  }

  /**
   * @When I enter :title as the event title
   */
  public function iEnterAsTheEventTitle($title) {
    $page = $this->getCurrentPage();
    $page->fillField('Title', $title);
  }

  /**
   * @When I select :name as the event venue
   */
  public function iSelectAsTheEventVenue($name) {
    $this->selectOptionFromSelect2EntityReferenceWidget('field_venue', $name, FALSE);
  }

  /**
   * @When I add :name as an event leader
   */
  public function iAddAsAnEventLeader($name) {
    // Sadly not working, hard to identify bug.
    $this->selectOptionFromSelect2EntityReferenceWidget('field_leaders', $name);
  }

  /**
   * @When I press :button
   */
  public function iPress($button) {
    $page = $this->getCurrentPage();
    $page->findButton($button)->press();
  }

  /**
   * @Then a new event is created
   */
  public function aNewEventIsCreated() {
    $properties = [
      'type' => 'event',
      'uid' => $this->getMe()->id()
    ];
    $this->group = $this->loadEntityByProperties('group', $properties);
    $this->assertNotEmpty(
      $this->group,
      sprintf('A new group should have been created')
    );
  }

  /**
   * @Then the event has the title :title
   */
  public function theEventHasTheTitle($title) {
    $this->group = $this->reloadEntity($this->group);
    $this->assertEquals(
      $this->group->label(),
      $title,
      sprintf('The created group should have the title %s', $title)
    );
  }

  /**
   * @Then the event has the venue :name
   */
  public function theEventHasTheVenue($name) {
    $node = $this->group->get('field_venue')->entity;
    $this->assertEquals(
      $name,
      $node->getTitle(),
      sprintf('The group should reference a venue with the title %s', $name)
    );
  }

  /**
   * @Then the event has the leader :name
   */
  public function theEventHasTheLeader($name) {
    $user = $this->group->get('field_leaders')->entity;
    $this->assertEquals(
      $name,
      $user->getDisplayName(),
      sprintf('The group should reference a leader/user with the display name %s', $name)
    );
  }

  /**
   * @Given an event with the title :title
   */
  public function anEventWithTheTitle($title) {
    $this->group = $this->createGroup(['type' => 'event', 'label' => $title]);
  }

  /**
   * @Given a user with the name :name
   */
  public function aUserWithTheName($name) {
    $user = $this->createDecoupledUser();
    $this->setUserFullName($user, $name);
  }

}

