<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class GroupMentorsAccessTest extends ExistingSiteBehatBase  {

  use TrainingTrait;

  protected $course;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Mentor access to users
    In order for mentors to counsel and contact users
    They need to see information about individual users

    Background:
      Given a course that starts on "2022-01-01"
      And the course ends on "2022-02-01"
      And an event that starts on "2022-01-01"
      And a user named "Fred"

    Scenario: Ordinary mentor can only see mentees
      Given I am logged in
      And today is "2021-01-01"
      And Fred joins the course
      Then I should not have view_sensitive access to Fred
      When I become a mentor of the course
      Then I should have view_sensitive access to Fred

    Scenario: Mentor access depends on students being group members
      Given I am logged in
      And today is "2021-01-01"
      When I become a mentor of the course
      Then I should not have view_sensitive access to Fred
      When Fred joins the course
      Then I should have view_sensitive access to Fred

    Scenario: Staff can always see students regardless of being mentors
      Given I am logged in as a "student_support"
      And today is "2021-01-01"
      And Fred joins the course
      Then I should have view_sensitive access to Fred
      When I become a mentor of the course
      Then I should have view_sensitive access to Fred

    Scenario: Mentor access ends one month after group ends
      Given I am logged in
      And today is "2021-01-01"
      And Fred joins the course
      When I become a mentor of the course
      Then I should have view_sensitive access to Fred
      Given today is "2022-01-29"
      Then I should have view_sensitive access to Fred
      Given today is "2022-02-28"
      Then I should have view_sensitive access to Fred
      Given today is "2022-03-05"
      Then I should not have view_sensitive access to Fred

    Scenario: Mentor access in multiple groups
      Given I am logged in
      And today is "2021-01-01"
      And Fred joins the course
      And Fred joins the event
      Then I should not have view_sensitive access to Fred
      When I become a mentor of the course
      Then I should have view_sensitive access to Fred
      When I become a mentor of the event
      Then I should have view_sensitive access to Fred
      Given the event ends on "2022-03-01"
      And today is "2022-03-05"
      Then I should have view_sensitive access to Fred
      And today is "2022-04-05"
      Then I should not have view_sensitive access to Fred

FEATURE;

  /**
   * @Given the :bundle ends on :date
   */
  public function theGroupEndsOnDate($bundle, $date) {
    $dateObject = new DrupalDateTime($date);
    $start = $this->{$bundle}->field_dates->value;
    $this->{$bundle}->set('field_dates', [
      'value' => $start,
      'end_value' => $dateObject->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    ]);
    $this->{$bundle}->save();
  }

  /**
   * @Then I should have :operation access to :name
   */
  public function iShouldHaveAccessTo($operation, $name) {
    // The handler has a dumb static cache.
    $handler = \Drupal::entityTypeManager()->getHandler('user', 'access');
    $handler->resetCache();
    // Our UserAccessControlHandler depends on loading the group from group memberships that are static cached.
    \Drupal::entityTypeManager()->getStorage('group_content')->resetCache();


    $user = $this->getUser($name);
    $result = $user->access($operation, $this->getMe(), TRUE);
    $this->assertTrue($result->isAllowed(),  ahs_miscellaneous_debug_access("$operation access failed. ", $this->getMe(), $user, $result));
  }

  /**
   * @Then I should not have :operation access to :name
   */
  public function iShouldNotHaveAccessTo($operation, $name) {
    // The handler has a dumb static cache.
    $handler = \Drupal::entityTypeManager()->getHandler('user', 'access');
    $handler->resetCache();
    // Our UserAccessControlHandler depends on loading the group from group memberships that are static cached.
    \Drupal::entityTypeManager()->getStorage('group_content')->resetCache();

    $user = $this->getUser($name);
    $result = $user->access($operation, $this->getMe(), TRUE);
    $this->assertFalse($result->isAllowed(),  ahs_miscellaneous_debug_access("$operation access wrongly succeeded. ", $this->getMe(), $user, $result));
  }

}

