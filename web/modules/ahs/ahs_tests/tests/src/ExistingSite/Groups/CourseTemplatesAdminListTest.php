<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;

/**
 * This doesn't need JS, and it's a poor fit for visual regression because it
 * depends a lot on the date.
 *
 * @group NewDatabaseRequired
 */
class CourseTemplatesAdminListTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use UserDefinitions;
  use GroupTrait;

  protected static $feature = <<<'FEATURE'
Feature: Training admin can see all course templates
    In order to see course templates
    Training admins need an admin view

    Background:
      Given course templates:
        | title               | published |
        | CourseTemplate1     | yes       |
        | CourseTemplate2     | yes       |
        | CourseTemplate3     | yes       |
        | CourseTemplate4     | no        |

    Scenario: Training admins can see published & unpublished course templates
      Given I am logged in as a "training_admin"
      When I visit "admin/training/courses/templates"
      Then I should see the following course templates:
        | title           |
        | CourseTemplate1 |
        | CourseTemplate2 |
        | CourseTemplate3 |
        | CourseTemplate4 |

FEATURE;

  /**
   * @Given course templates:
   */
  public function course_templates(TableNode $table) {
    foreach($table->getHash() as $row) {
      $edit = [
        'type' => 'course_template',
        'status' => ($row['published'] == 'yes') ? TRUE : FALSE,
      ];
      $edit['label'] = $row['title'];
      $this->createGroup($edit);
    }
  }

  /**
   * @Then I should see the following course templates:
   */
  public function iShouldSeeTheFollowingCourseTemplate(TableNode $table) {
    $tableHash = $table->getHash();
    $page = $this->getCurrentPage();
    $results = $page->findAll('css', "td.views-field-label:contains('CourseTemplate')");
    $this->assertCount(
      count($tableHash),
      $results,
      "All Course templates should be listed"
    );
  }

}
