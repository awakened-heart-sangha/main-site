<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\TabsTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

class EventTabsTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use GroupDefinitions;
  use UserDefinitions;
  use GroupTrait;
  use UserTrait;
  use TabsTrait;

  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Event tabs
    In order to organise information about an event
    Different tabs should be available depending on event role

    Background:
      Given an event group

    Scenario: Non-participant event tabs
      Given I am not logged in
      When I view the group
      Then I should see no tabs

    Scenario: Non-participant event tabs logged in
      Given I am logged in
      When I view the group
      Then I should see no tabs

    Scenario: Tabs for outsider events admin
      Given I am logged in as an "events_admin"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Participants  |
        | Manage        |
        | Clone         |

    Scenario: Event tabs for insider events admin
      Given I am logged in as an "events_admin"
      And I am a member of the group
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Participants  |
        | Manage        |
        | Clone         |

    Scenario: Tabs for outsider archivist
      Given I am logged in as an "archivist"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Manage        |

    Scenario: Event tabs for insider archivist
      Given I am logged in as an "archivist"
      And I am a member of the group
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Manage        |

    Scenario: Tabs for outsider student_admin
      Given I am logged in as a "student_admin"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Participants  |

    Scenario: Tabs for outsider student_support
      Given I am logged in as an "student_support"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Participants  |

    Scenario: Event tabs for group member
      Given I am logged in
      And I am a member of the group
      When I view the group
      Then I should see no tabs

    Scenario: Event tabs for group admin
      Given I am logged in
      And I am a member of the group with the role "admin"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Participants  |
        | Manage        |

FEATURE;


}

