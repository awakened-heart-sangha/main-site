<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\Core\Url;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

class GroupMaterialsTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use GroupDefinitions;
  use UserDefinitions;
  use GroupTrait;
  use UserTrait;
  use CommerceTrait;

  protected $material;
  protected $course_template;
  protected $firstName;
  protected $course;
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Group materials
    In order to make sure participants have the materials they need
    They are encouraged to order when registering

    Background:
      Given a material
      And I am logged in

    Scenario: Course template materials
      Given a registerable course_template group that requires the material
      And I start the course_template
      Then the page header contains "Request"
      And I should see the material title
      When I press "Request materials"
      Then I should see "Your request"
      And I should not see "Your order"
      And I should not see "Your contribution"
      When I enter my address
      And I press "Confirm"
      Then the page header contains the course_template title
      And I should see "Thankyou for registering"
      And I should see "The materials you requested"
      And an order for the material should be placed

    Scenario: Course materials
      Given a registerable course group that requires the material
      And I register for the course
      Then the page header contains "Request"
      And I should see the material title
      When I press "Request materials"
      Then I should see "Your request"
      And I should not see "Your order"
      And I should not see "Your contribution"
      When I enter my address
      And I press "Confirm"
      Then the page header contains the course title
      And I should see "Thankyou for registering"
      And I should see "The materials you requested"
      And an order for the material should be placed

    Scenario: Course materials skipped
      Given a registerable course group that requires the material
      And I register for the course
      When I press "Skip"
      Then I should not see "Your request"
      And the page header contains the course title
      And I should see "Thankyou for registering"
      And I should not see "The materials you requested"

    Scenario: Course already ordered material
      Given a registerable course group that requires the material
      And I have previously requested the material
      And I register for the course
      Then I should not see "Request materials"
      And the page header contains the course title


FEATURE;

  /**
   * @Given a material
   */
  public function aMaterial() {
    $product = $this->createEntity('commerce_product', [
      'type' => 'ahs_course_materials',
      'title' => $this->randomMachineName(),
    ]);
    $this->material = $this->createEntity('commerce_product_variation',
      [
        'type' => 'default',
        'sku' => $this->randomMachineName(),
        'product_id' => $product->id(),
      ]
    );
  }


  /**
   * @Given a registerable :bundle group that requires the material
   */
  public function aGroupThatRequiresTheMaterial($bundle) {
    $this->{$bundle} = $this->createGroup([
      'type' => $bundle,
      'field_materials' => $this->material,
      'field_registration_open' => TRUE,
      'field_registration_normal' => TRUE,
    ], TRUE);
  }

  /**
   * @Given I have previously requested the material
   */
  public function iHavePreviouslyRequestedTheMaterial() {
    $orderItem = OrderItem::create([
      'type' => 'default',
      'quantity' => 1,
      'unit_price' => new Price(0, 'GBP'),
      'purchased_entity' => $this->material->id(),
      'overridden_unit_price' => TRUE,
    ]);
    $orderItem->save();
    $order = Order::create([
      'type' => 'ahs_free',
      'uid' => $this->getMe()->id(),
      'state' => 'fulfillment',
      'store_id' => $this->getStoreId(),
      'order_items' => [$orderItem],
    ]);
    $order->save();
  }


  /**
   * @When I register for the :bundle
   */
  public function iRegisterForTheGroup($bundle) {
    $url = Url::fromRoute('entity.group.join', [
      'group' => $this->{$bundle}->id(),
    ])->toString();
    $this->drupalGet($url);
    $this->iPress('Register');
  }

  /**
   * @When I start the course_template
   */
  public function iStartTheTemplate() {
    $url = Url::fromRoute('entity.group.start', [
      'group' => $this->course_template->id(),
    ])->toString();
    $this->drupalGet($url);
    $this->iPress('Start');
  }

  /**
   * @When I enter my address
   */
  public function iEnterMyAddress() {
    $this->firstName = $this->randomMachineName();
    $this->fillBillingInformation(
      $this->firstName,
      $this->randomMachineName(), [
      'address_line1' => $this->randomMachineName(),
      'locality' => $this->randomMachineName(),
      'postal_code' => "GU34 4PJ",
      ]
      );
  }

  /**
   * @Then an order for the material should be placed
   */
  public function anOrderForTheMaterialShouldBePlaced() {
    $order = $this->loadEntityByProperties('commerce_order', ['uid' => $this->getMe()->id(),]);
    $order = $this->reloadEntity($order);
    $this->assertOrder($order, 'ahs_free', 'default', 0, [$this->material->getSku()], 'fulfillment');
    $profile = $order->getBillingProfile();
    $this->assertNotEmpty($profile, "There should be billing profile on the order");
    $address = $profile->get('address')->first();
    $this->assertNotEmpty($address, "There should be an address on the profile");
    $firstName = $address->getGivenName();
    $this->assertSame($this->firstName, $firstName, "The entered first name should be saved on the billing profile");
  }


  /**
   * @Then I should see the material title
   */
  public function iShouldSeeTheMaterialTitle() {
    $this->assertSession()->pageTextContains($this->material->label());
  }


  /**
   * @Then the page header contains the :bundle title
   */
  public function iShouldSeeTheGroupTitle($bundle) {
    $this->assertSession()->elementTextContains('css', 'h1', $this->{$bundle}->label());
  }


}

