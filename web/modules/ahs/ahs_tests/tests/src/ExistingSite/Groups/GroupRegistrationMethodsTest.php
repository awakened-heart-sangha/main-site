<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\registration\HostEntityInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBase;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;

/**
 * Test registration methods on AhsGroup.
 */
class GroupRegistrationMethodsTest extends ExistingSiteBase {

  use GroupTrait;

  public function testCourse() {
    $this->assertTrue(TRUE);
    /* @var \Drupal\ahs_groups\Entity\AhsGroup */
    $group = $this->createGroup(
      [
        'type' => 'course',
        'field_registration_normal' => TRUE,
      ],
      TRUE
    );
    $this->assertSame('group', $group->getRegistrationSystem());
    $this->assertFalse($group->isStarted());
    $this->assertFalse($group->isFinished());
    $this->assertTrue($group->isRegistrationOpen());
    $this->assertTrue($group->isRegistrationAvailable());

    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $startDate = $group->get('field_dates')->start_date;
    $this->assertNotNull($startDate);
    $group->set('field_registration_deadline', $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    $this->assertTrue($group->hasRegistrationDeadlinePending());

    $group->set('field_registration_open', FALSE)->save();
    $this->assertFalse($group->isRegistrationOpen());
    $this->assertFalse($group->isRegistrationAvailable());
  }

  public function testLegacyEvent() {
    $this->assertTrue(TRUE);
    /* @var \Drupal\ahs_groups\Entity\AhsGroup */
    $group = $this->createGroup(
      [
        'type' => 'event',
        'field_registration_normal' => TRUE,
      ],
      TRUE
    );
    $this->assertSame('group', $group->getRegistrationSystem());
    $this->assertFalse($group->isStarted());
    $this->assertFalse($group->isFinished());
    $this->assertTrue($group->isRegistrationOpen());
    $this->assertTrue($group->isRegistrationAvailable());

    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $startDate = $group->get('field_dates')->start_date;
    $this->assertNotNull($startDate);
    $group->set('field_registration_deadline', $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    $this->assertTrue($group->hasRegistrationDeadlinePending());

    $group->set('field_registration_open', FALSE)->save();
    $this->assertFalse($group->isRegistrationOpen());
    $this->assertFalse($group->isRegistrationAvailable());
  }

  public function testNewEvent() {
    $this->assertTrue(TRUE);
    /* @var \Drupal\ahs_groups\Entity\AhsGroup */
    $group = $this->createGroup(
      [
        'type' => 'event',
      ],
      TRUE
    );
    $this->assertSame('group', $group->getRegistrationSystem());
    $this->assertFalse($group->isStarted());
    $this->assertFalse($group->isFinished());
    $this->assertTrue($group->isRegistrationOpen());
    $this->assertTrue($group->isRegistrationAvailable());

    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $startDate = $group->get('field_dates')->start_date;
    $this->assertNotNull($startDate);
    $group->set('field_registration_deadline', $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    $this->assertTrue($group->hasRegistrationDeadlinePending());

    $group->set('field_registration_open', FALSE)->save();
    $this->assertFalse($group->isRegistrationOpen());
    $this->assertFalse($group->isRegistrationAvailable());
  }

  public function testEventWithRegistrationSystem() {
    $this->assertTrue(TRUE);
    /* @var \Drupal\ahs_groups\Entity\AhsGroup */
    $group = $this->createGroup(
      [
        'type' => 'event',
        'field_registration_system' => 'registration',
      ],
      TRUE
    );
    $this->assertSame('registration', $group->getRegistrationSystem());
    $this->assertFalse($group->isStarted());
    $this->assertFalse($group->isFinished());
    $this->assertTrue($group->isRegistrationOpen());
    $this->assertTrue($group->isRegistrationAvailable());
    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $this->assertFalse($group->get('field_registration_open')->isEmpty());
    $this->assertTrue((bool) $group->get('field_registration_open')->value);
    $this->assertTrue($group->get('field_registration_deadline')->isEmpty());
    $this->assertNull($group->get('field_registration_deadline')->value);

    $host = $group->getRegistrationHostEntities()[0];
    $this->assertInstanceOf(HostEntityInterface::class, $host);
    $settings = $host->getSettings();

    // Set a registration date as a setting.
    $startDate = $group->get('field_dates')->start_date;
    $this->assertNotNull($startDate);
    $settings->set('close', $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    $this->assertTrue($group->hasRegistrationDeadlinePending());

    // When the group is saved, this should be stored on the group too.
    $group->save();
    $this->assertFalse($group->get('field_registration_deadline')->isEmpty());
    $this->assertNotNull($group->get('field_registration_deadline')->value);

    // Disable registrations via the variation.
    $settings->set('status', FALSE)->save();
    $this->assertFalse($group->isRegistrationOpen());
    $this->assertFalse($group->isRegistrationAvailable());
    // Deadlines are ignored for disabled variations.
    $this->assertFalse($group->hasRegistrationDeadlinePending());

    // When the group is saved, the disabling should be stored on the group too.
    $group->save();
    $this->assertFalse($group->get('field_registration_open')->isEmpty());
    $this->assertFalse((bool) $group->get('field_registration_open')->value);
    // Deadlines are ignored for disabled variations.
    $this->assertTrue($group->get('field_registration_deadline')->isEmpty());

  }

  public function testEventWithMultipleVariations() {
    $variation1 = ProductVariation::create([
      'type' => 'ahs_registration',
      'field_registration_type' => 'online',
      'title' => 'Online',
      'price' => new Price(0, 'GBP'),
    ]);
    $product1 = Product::create([
      'type' => 'ahs_registration',
      'title' => 'Online',
    ]);
    $product1->addVariation($variation1);
    $product1->save();

    $variation2 = ProductVariation::create([
      'type' => 'ahs_registration',
      'field_registration_type' => 'online',
      'title' => 'Online2',
      'price' => new Price(0, 'GBP'),
    ]);
    $product2 = Product::create([
      'type' => 'ahs_registration',
      'title' => 'Online2',
    ]);
    $product2->addVariation($variation2);
    $product2->save();

    $variation3 = ProductVariation::create([
      'type' => 'ahs_registration',
      'field_registration_type' => 'in_person',
      'title' => 'In person',
      'price' => new Price(0, 'GBP'),
    ]);
    $product3 = Product::create([
      'type' => 'ahs_registration',
      'title' => 'In person',
    ]);
    $product3->addVariation($variation3);
    $product3->save();

    /* @var \Drupal\ahs_groups\Entity\AhsGroup */
    $group = $this->createGroup(
      [
        'type' => 'event',
        'field_registration_system' => 'registration',
        'field_product' => [
          $product1,
          $product2,
          $product3
        ]
      ],
      TRUE
    );
    $comparison_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;

    // All 3 variations saved.
    $this->assertCount(3, $group->getRegistrationHostEntities());

    // Default values for multiple variations are the same as for 1.
    $this->assertSame('registration', $group->getRegistrationSystem());
    $this->assertFalse($group->isStarted());
    $this->assertFalse($group->isFinished());
    $this->assertTrue($group->isRegistrationOpen());
    $this->assertTrue($group->isRegistrationAvailable());
    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $this->assertFalse($group->get('field_registration_open')->isEmpty());
    $this->assertTrue((bool) $group->get('field_registration_open')->value);
    $this->assertTrue($group->get('field_registration_deadline')->isEmpty());
    $this->assertNull($group->get('field_registration_deadline')->value);

    // Set a deadline on all 3.
    $startDate = $group->get('field_dates')->start_date;
    $this->assertNotNull($startDate);
    foreach($group->getRegistrationHostEntities() as $host) {
      $this->assertInstanceOf(HostEntityInterface::class, $host);
      $settings = $host->getSettings();
      $settings->set('close', $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    }
    $this->assertTrue($group->hasRegistrationDeadlinePending());
    $this->assertFalse($group->hasRegistrationDeadlinePassed());

    // Deadline should be saved on group when group is saved.
    $group->save();
    $this->assertFalse($group->get('field_registration_deadline')->isEmpty());
    $this->assertNotNull($group->get('field_registration_deadline')->value);

    // It's the latest deadline that matters, not the first.
    $now = new DrupalDateTime();
    $interval = new \DateInterval('P1D');
    $pastDate = $now->sub($interval);
    $host = $group->getRegistrationHostEntities()[0];
    $settings = $host->getSettings();
    $settings->set('close', $pastDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    $this->assertTrue($group->hasRegistrationDeadlinePending());
    $group->save();
    $this->assertEquals($startDate->format($comparison_format), $group->get('field_registration_deadline')->date->format($comparison_format));

    // Set past deadline on all 3
    foreach($group->getRegistrationHostEntities() as $host) {
      $this->assertInstanceOf(HostEntityInterface::class, $host);
      $settings = $host->getSettings();
      $settings->set('close', $pastDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    }
    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $this->assertTrue($group->hasRegistrationDeadlinePassed());
    $group->save();
    $this->assertEquals($pastDate->format($comparison_format), $group->get('field_registration_deadline')->date->format($comparison_format));

    // Set a future date on one variation while leaving others past.
    $host = $group->getRegistrationHostEntities()[0];
    $settings = $host->getSettings();
    $settings->set('close', $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT))->save();
    $this->assertTrue($group->hasRegistrationDeadlinePending());
    $this->assertFalse($group->hasRegistrationDeadlinePassed());
    $group->save();
    $this->assertEquals($startDate->format($format = 'Y-m-d H:i:s'), $group->get('field_registration_deadline')->date->format($format = 'Y-m-d H:i:s'));

    // Disabled variations are ignored.
    $host = $group->getRegistrationHostEntities()[0];
    $settings = $host->getSettings();
    $settings->set('status', FALSE)->save();
    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $this->assertTrue($group->hasRegistrationDeadlinePassed());
    $group->save();
    $this->assertEquals($pastDate->format($format = 'Y-m-d H:i:s'), $group->get('field_registration_deadline')->date->format($format = 'Y-m-d H:i:s'));

    // Missing deadline for any variation means no deadline for the group.
    $host = $group->getRegistrationHostEntities()[0];
    $settings = $host->getSettings();
    $settings->set('status', TRUE)->save();
    $settings->set('close', NULL)->save();
    $this->assertFalse($group->hasRegistrationDeadlinePending());
    $this->assertFalse($group->hasRegistrationDeadlinePassed());
    $group->save();
    $this->assertTrue($group->get('field_registration_deadline')->isEmpty());
  }

}
