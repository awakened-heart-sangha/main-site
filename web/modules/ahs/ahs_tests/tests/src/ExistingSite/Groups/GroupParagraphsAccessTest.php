<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\user\Entity\User;

class GroupParagraphsAccessTest extends ExistingSiteBehatBase  {

  use GroupDefinitions;
  use GroupTrait;
  use UserTrait;

  protected $paragraph;
  protected $course;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Access to group paragraphs depends on group membership
    In order for participants to access event information and course materials
    Appropriate access needs to be granted to group members

    Scenario: Access to published public course paragraphs
      Given a course group
      And a published public paragraph on the group
      Then an anonymous user can view the paragraph
      And an authenticated user can view the paragraph
      And a participant in the course can view the paragraph
      And an admin of the course can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the events_admin role can view the paragraph
      And a user with the archivist role can view the paragraph
      And a user with the curator role can view the paragraph

    Scenario: Access to published private course paragraphs
      Given a course group
      And a published private paragraph on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the course can view the paragraph
      And an admin of the course can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the archivist role can view the paragraph
      And a user with the curator role can view the paragraph
      And a user with the events_admin role cannot view the paragraph

    Scenario: Access to unpublished public course paragraphs
      Given a course group
      And an unpublished private paragraph on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the course cannot view the paragraph
      And an admin of the course can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the events_admin role cannot view the paragraph

    Scenario: Access to unpublished private course paragraphs
      Given a course group
      And an unpublished private paragraph on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the course cannot view the paragraph
      And an admin of the course can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the events_admin role cannot view the paragraph

    Scenario: Access to published course paragraphs that don't specify public/private
      Given a course group
      And a published paragraph without a public field on the group
      Then an anonymous user can view the paragraph
      And an authenticated user can view the paragraph
      And a participant in the course can view the paragraph
      And an admin of the course can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the events_admin role can view the paragraph

    Scenario: Access to unpublished course paragraphs that don't specify public/private
      Given a course group
      And an unpublished paragraph without a public field on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the course cannot view the paragraph
      And an admin of the course can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the events_admin role cannot view the paragraph

    # EVENTS .........................

    Scenario: Access to published public event paragraphs
      Given an event group
      And a published public paragraph on the group
      Then an anonymous user can view the paragraph
      And an authenticated user can view the paragraph
      And a participant in the event can view the paragraph
      And an admin of the event can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the events_admin role can view the paragraph

    Scenario: Access to published private event paragraphs
      Given an event group
      And a published private paragraph on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the event can view the paragraph
      And an admin of the event can view the paragraph
      And a user with the training_admin role cannot view the paragraph
      And a user with the events_admin role can view the paragraph

    Scenario: Access to unpublished public event paragraphs
      Given an event group
      And an unpublished private paragraph on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the event cannot view the paragraph
      And an admin of the event can view the paragraph
      And a user with the training_admin role cannot view the paragraph
      And a user with the events_admin role can view the paragraph

    Scenario: Access to unpublished private event paragraphs
      Given an event group
      And an unpublished private paragraph on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the event cannot view the paragraph
      And an admin of the event can view the paragraph
      And a user with the training_admin role cannot view the paragraph
      And a user with the events_admin role can view the paragraph

    Scenario: Access to published event paragraphs that don't specify public/private
      Given an event group
      And a published paragraph without a public field on the group
      Then an anonymous user can view the paragraph
      And an authenticated user can view the paragraph
      And a participant in the event can view the paragraph
      And an admin of the event can view the paragraph
      And a user with the training_admin role can view the paragraph
      And a user with the events_admin role can view the paragraph

    Scenario: Access to unpublished event paragraphs that don't specify public/private
      Given an event group
      And an unpublished paragraph without a public field on the group
      Then an anonymous user cannot view the paragraph
      And an authenticated user cannot view the paragraph
      And a participant in the event cannot view the paragraph
      And an admin of the event can view the paragraph
      And a user with the training_admin role cannot view the paragraph
      And a user with the events_admin role can view the paragraph

FEATURE;

  /**
   * @Given a/an :published :public paragraph on the group
   */
  public function aPublishedPublicParagraphOnTheGroup($published, $public) {
    $isPublished = $published === 'published';
    $isPublic = $public === 'public';
    $this->createGroupParagraph('layout_section', $isPublished, $isPublic);
  }

  /**
   * @Given a/an :published paragraph without a public field on the group
   */
  public function aPublishedParagraphWithoutAPublicFieldOnTheGroup($published) {
    $isPublished = $published === 'published';
    $this->createGroupParagraph('text', $isPublished);
  }


  /**
   * @Then an anonymous user :can :operation the paragraph
   */
  public function anAnonymousUserCanAccessTheParagraph($can, $operation) {
    $result = $can === 'can';
    $user = User::getAnonymousUser();
    $this->assertSame($result, $this->paragraph->access($operation, $user));
  }

  /**
   * @Then an authenticated user :can :operation the paragraph
   */
  public function anAuthenticatedUserCanAccessTheParagraph($can, $operation) {
    $result = $can === 'can';
    $user = $this->createUser();
    $this->assertSame($result, $this->paragraph->access($operation, $user));
  }

  /**
   * @Then a/an :role in/of the :bundle :can :operation the paragraph
   */
  public function aRoleInTheBundleCanAccessTheParagraph($role, $bundle, $can, $operation) {
    $result = $can === 'can';
    $user = $this->createUser();
    $this->group->addMember($user);
    if ($role !== 'participant') {
      $membership = $this->group->getMember($user)->getGroupRelationship();
      $membership->group_roles[] = "$bundle-$role";
      $membership->save();
    }
    $this->assertSame($result, $this->paragraph->access($operation, $user));
  }

  /**
   * @Then a/an user with the :role role :can :operation the paragraph
   */
  public function anUserWithTheGlobalRoleCanAccessTheParagraph($can, $operation, $role) {
    $result = $can === 'can';
    $user = $this->createUser();
    $user->addRole($role);
    $user->save();
    $this->assertSame($result, $this->paragraph->access($operation, $user));
  }

}

