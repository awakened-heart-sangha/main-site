<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\group\Entity\Group;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;


/**
 * @group NewDatabaseRequired
 * @group js
 *
 * Needs to use javascript because of select2 user references on the leaders field.
 */
class GroupCloningJsTest extends ExistingSiteJsBehatBase  {

  use GroupCloningTrait;


  protected $course_template;
  protected $cloned_course;
  protected $paragraph;
  protected $course;

  protected static $feature = <<<'FEATURE'
Feature: Starting from a course template
    In order for participants to study template courses
    They need to start their own course based on the template

    Scenario: Cloning a course as a course with changed values
      Given a course group
      And a published public paragraph on the group
      And the course has a leader
      And I am logged in as a training_admin
      When I go to the course clone page
      And I enter "test course title" as the course title
      And I empty the leaders field for the course
      And I check "Private"
      And I press "Clone"
      Then I should not be on the course clone form
      And a cloned course is created from the course
      And the cloned course has the title "Test course title"
      And the cloned course has no leaders
      And the course paragraph has been cloned to the course
      And the cloned course paragraph is unpublished
      And I am the owner of the cloned course
      And I am not a member of the cloned course

FEATURE;

  protected function setUp(): void {
    parent::setUp();
    \Drupal::service('datetime.time')->setTime("2021-01-01 00:00:00");
  }

  /**
   * @When I empty the leaders field for the :bundle
   */
  public function iEmptyTheLeadersField($bundle) {
    $this->emptySelect2EntityReferenceWidget("fields-$bundle-field-leaders");
    $this->debugPage();
  }

}

