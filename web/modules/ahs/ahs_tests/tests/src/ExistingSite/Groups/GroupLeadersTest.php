<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;

class GroupLeadersTest extends ExistingSiteBehatBase  {

  use GroupTrait;
  use GroupDefinitions;

  protected $course;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Group leaders
    In order for staff to quickly create groups
    Leaders are added as mentor participants

    Scenario: Course leaders
      Given a course group
      And the course has a leader
      Then the leader is a mentor in the course

    Scenario: Event leaders
      Given an event group
      And the event has a leader
      Then the leader is a mentor in the event

FEATURE;

  /**
   * @Then the leader is a mentor in the :bundle
   */
  public function theLeaderIsAMentorInTheClonedGroup($bundle) {
    $this->assertLeadersAreMentors($this->{$bundle});
  }

}

