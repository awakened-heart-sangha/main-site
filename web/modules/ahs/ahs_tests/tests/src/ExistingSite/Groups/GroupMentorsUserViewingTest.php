<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group NewDatabaseRequired
 * @group js
 */
class GroupMentorsUserViewingTest extends ExistingSiteJsBehatBase  {

  use TrainingTrait;

  protected $inVisualRegressionHide = [".views-field-created"];
  protected $course;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Mentor access to users
    In order for mentors to counsel and contact users
    They need to see information about individual users

    Background:
      Given a future course
      And a future event
      And a user named "Fred"

    Scenario: Ordinary mentor can only see mentees
      Given I am logged in
      And Fred joins the course
      When I visit the user "/" page for Fred
      Then I should not see the email for Fred
      When I visit the user "/participation/joined" page for Fred
      Then I should see "Access denied"
      When I visit the user "/participation/eligible" page for Fred
      Then I should see "Access denied"
      When I visit the user "/participation/suggested" page for Fred
      Then I should see "Access denied"
      When I become a mentor of the course
      And I visit the user "/" page for Fred
      Then I should see the email for Fred
      When I visit the user "/participation/joined" page for Fred
      Then I should not see "Access denied"
      When I visit the user "/participation/eligible" page for Fred
      Then I should not see "Access denied"
      When I visit the user "/participation/suggested" page for Fred
      Then I should not see "Access denied"

    Scenario: Mentor access depends on students being group members
      Given I am logged in
      When I become a mentor of the course
      And I visit the user "/" page for Fred
      Then I should not see the email for Fred
      When Fred joins the course
      And I visit the user "/" page for Fred
      Then I should see the email for Fred

    Scenario: Staff can always see students regardless of being mentors
      Given I am logged in as a "student_support"
      And Fred joins the course
      When I visit the user "/" page for Fred
      Then I should see the email for Fred
      When I visit the user "/participation/joined" page for Fred
      Then I should not see "Access denied"
      When I become a mentor of the course
      And I visit the user "/" page for Fred
      Then I should see the email for Fred
      When I visit the user "/participation/joined" page for Fred
      Then I should not see "Access denied"

    Scenario: Ordinary mentor can see group participants
      Given I am logged in
      And Fred joins the course
      When I visit the course group "/participants/joined" page
      Then I should see "Access denied"
      When I become a mentor of the course
      And I visit the course group "/participants/joined" page
      Then I should not see "Access denied"
      And I should see the email for Fred
      When I visit the course group "/participants/eligible" page
      Then I should see "Access denied"

    Scenario: Staff can see group & eligible participants
      Given I am logged in as a "student_support"
      And Fred joins the course
      When I visit the course group "/participants/joined" page
      Then I should not see "Access denied"
      And I should see the email for Fred
      When I visit the course group "/participants/eligible" page
      Then I should not see "Access denied"

FEATURE;

  /**
   * @When I visit the user :path page for :name
   */
  public function iVisitTheUserPageForName($path, $name) {
    $user = $this->getUser($name);
    $this->drupalGet('/user/' . $user->id() . $path);
  }

  /**
   * @Given the :bundle ends on :date
   */
  public function theGroupEndsOnDate($bundle, $date) {
    $dateObject = new DrupalDateTime($date);
    $start = $this->{$bundle}->field_dates->value;
    $this->{$bundle}->set('field_dates', [
      'value' => $start,
      'end_value' => $dateObject->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    ]);
    $this->{$bundle}->save();
  }

  /**
   * @Then I should see the email for :name
   */
  public function iShouldSeeTheEmailForName($name) {
    $this->assertSession()->pageTextContains($this->getUser($name)->getEmail());
  }

  /**
   * @Then I should not see the email for :name
   */
  public function iShouldNotSeeTheEmailForName($name) {
    $this->assertSession()->pageTextNotContains($this->getUser($name)->getEmail());
  }

}

