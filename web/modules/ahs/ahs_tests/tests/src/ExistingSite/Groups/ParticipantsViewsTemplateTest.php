<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group js
 */
class ParticipantsViewsTemplateTest extends ExistingSiteJsBehatBase  {

  protected $inVisualRegressionHide = [
    "div.view-footer h3",
    ".views-field-created",
  ];

  protected $course_template;
  protected $course;

  use TrainingTrait;

  protected static $feature = <<<'FEATURE'
Feature: Pages showing particpants and possible participants
    In order for administrators to set up groups for users
    They need to see who the actual and possible participants are for groups

    Background:
      Given a course_template group
      And today is "2021-01-01"
      And I am logged in as a "training_admin"
      Given experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
      Given a user named "Fred" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceB |
      Given a user named "Jane" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceC |
      Given a user named "Harry" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceC |
      Given a user named "Lisa" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
      And a user named "Jack" has no training records
      And "Harry" is a member
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceA |
      And the course_template is suggested
      And the course_template has suggestion conditions:
        | conditions  |
        | ExperienceC |

    Scenario: Eligible participants page
      When I visit the course_template group "/participants/eligible" page
      Then the page header contains the group title
      And I should see "Fred"
      And I should see "Jane"
      And I should see "Lisa"
      And I should see "Harry"
      And I should not see "Jack"

    Scenario: Suggested participants page
      When I visit the course_template group "/participants/suggested" page
      Then the page header contains the group title
      And I should not see "Fred"
      And I should see "Jane"
      And I should see "Lisa"
      And I should see "Harry"
      And I should not see "Jack"

FEATURE;

  /**
   * @Given :name is a member
   */
  public function isAMember($name) {
    $this->users[$name]->addRole('member');
    $this->users[$name]->save();
  }

  /**
   * @Then the page header contains the group title
   */
  public function thePageHEaderContainsTheGroupTitle() {
    $this->assertSession()->elementTextContains('css', 'h1.page-header', $this->group->label());
  }

}

