<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

/**
 * @group NewDatabaseRequired
 * @group js
 */
class ParticipantsViewsCourseTest extends ExistingSiteJsBehatBase  {

  use TrainingTrait;

  protected $inVisualRegressionHide = [".views-field-created"];
  protected $course;

  protected static $feature = <<<'FEATURE'
Feature: Pages showing particpants and possible participants
    In order for administrators to set up groups for users
    They need to see who the actual and possible participants are for groups

    Background:
      Given a course that starts on "2022-01-01"
      And today is "2021-01-01"
      And I am logged in as a "training_admin"
      Given experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
      Given a user named "Fred" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceB |
      Given a user named "Jane" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceC |
      Given a user named "Harry" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceC |
      Given a user named "Lisa" has training records for:
        | experience  |
        | ExperienceA |
        | ExperienceB |
        | ExperienceC |
      And a user named "Jack" has no training records
      And "Lisa" is already a participant in the course
      And "Harry" is a member
      Given the course has eligibility conditions:
        | conditions  |
        | ExperienceA |
      And the course is suggested
      And the course has suggestion conditions:
        | conditions  |
        | ExperienceC |

    Scenario: Current participants page
      When I visit the group "/participants/joined" page
      Then the page header contains the group title
      And I should not see "Fred"
      And I should not see "Jane"
      And I should see "Lisa"
      And I should not see "Harry"
      And I should not see "Jack"
      When Harry joins the course
      And I visit the group "/participants/joined" page
      Then I should see "Harry"

    Scenario: Eligible participants page
      When I visit the group "/participants/eligible" page
      And the page header contains the group title
      Then I should see "Fred"
      And I should see "Jane"
      And I should see "Lisa"
      And I should see "Harry"
      And I should not see "Jack"
      When Harry joins the course
      And I visit the group "/participants/eligible" page
      # Participants are still show on the eligible list
      Then I should see "Harry"

    Scenario: Suggested participants page
      When I visit the group "/participants/suggested" page
      And the page header contains the group title
      And I should not see "Fred"
      And I should see "Jane"
      And I should see "Lisa"
      And I should see "Harry"
      And I should not see "Jack"
      When Harry joins the course
      And I visit the group "/participants/suggested" page
      # Participants are still shown on the suggested page
      Then I should see "Harry"

    Scenario: Prospective participants page
      # The prospective display only shows suggested members not already participating
      When I visit the group "/participants/prospective" page
      Then the page header contains the group title
      And I should not see "Fred"
      # Although Jane is suggested, she's not a member
      And I should not see "Jane"
      And I should not see "Lisa"
      # Harry is a member
      And I should see "Harry"
      And I should not see "Jack"
      When Harry joins the course
      And I visit the group "/participants/prospective" page
      # Participants are NOT shown on the prospective list
      Then I should not see "Harry"

FEATURE;

  /**
   * @When I visit the group :path page
   */
  public function iVisitTheGroupPage($path) {
    $this->drupalGet('/group/' . $this->group->id() . $path);
  }

  /**
   * @Given :name is a member
   */
  public function isAMember($name) {
    $this->users[$name]->addRole('member');
    $this->users[$name]->save();
  }

  /**
   * @Then the page header contains the group title
   */
  public function thePageHEaderContainsTheGroupTitle() {
    $this->assertSession()->elementTextContains('css', 'h1.page-header', $this->group->label());
  }

}

