<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;

/**
 * @group NewDatabaseRequired
 * @group js
 */
class AddGroupMemberTest extends ExistingSiteJsBehatBase  {

  use UserDefinitions;
  use GroupTrait;
  use BrowsingDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Add group member by any name field
    In order for decoupled users to be easily added to groups
    It needs to be possible to reference users by any name field

    Background:

    Scenario Outline: User can be referenced by name
      Given users:
        | firstname | lastname | nickname | sangha name | email           | username | decoupled |
        | Fred      | Bloggs   |          |             | yy1@example.com | xxx1     | FALSE     |
        | James     | Bloggs   | Jim      |             | yy2@example.com | xxx2     | FALSE     |
        | Fred      | Bloggs   |          | Dorje       | yy3@example.com | xxx3     |           |
        | Robert    | Smith    |          |             | bob@example.com | xxx4     |           |
        | Frank     | Butcher  |          |             | fb@example.com  |          | TRUE      |

      And I am logged in as an "administrator"
      When I go to add a user to a group
      And I enter "<text>" in the user select
      Then I should see "<options>" as the options
      When I select "<selected>"
      And I press "Save"
      Then "<selected_email>" is added to the group

      Examples:
      | text       | selected          | options                                    | selected_email   | comment                         |
      | James Blo  | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Firstname match                 |
      | Jam        | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Partial match                   |
      | ames       | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Partial match                   |
      | ame        | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Partial match                   |
      | james      | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Case insensitivity              |
      | JAMES      | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Case insensitivity              |
      | Fred       | Fred Bloggs       | Fred Bloggs, Fred Dorje Bloggs             | yy1@example.com  | Firstname multiple matches      |
      | Fred       | Fred Dorje Bloggs | Fred Bloggs, Fred Dorje Bloggs             | yy3@example.com  | Select different match          |
      | Bloggs     | Fred Bloggs       | Fred Bloggs, Fred Dorje Bloggs, Jim Bloggs | yy1@example.com  | Lastname match                  |
      | Jim        | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Nickname                        |
      | Jim Bloggs | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Nickname combined with lastname |
      | Bloggs Jim | Jim Bloggs        | Jim Bloggs                                 | yy2@example.com  | Order insensitive               |
      | Dorje      | Fred Dorje Bloggs | Fred Dorje Bloggs                          | yy3@example.com  | Nickname                        |
      | bob        | Robert Smith      | Robert Smith                               | bob@example.com  | Email match                     |
      | bob Smith  | Robert Smith      | Robert Smith                               | bob@example.com  | Email combined with name        |
      | Smith bob  | Robert Smith      | Robert Smith                               | bob@example.com  | Order insensitive               |
      | xxx1       | Fred Bloggs       | Fred Bloggs                                | yy1@example.com  | Username                        |
      | Frank      | Frank Butcher     | Frank Butcher                              | fb@example.com   | No Username                     |

FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $edit = [
      'type' => 'special',
    ];
    $this->group = $this->createGroup($edit);
  }

  /**
   * @When I go to add a user to a group
   */
  public function iGoToAddAUserToAGroup() {
    $this->drupalGet('/group/' . $this->group->id() . '/content/add/group_membership');
    $this->assertSession()->pageTextContains('The user you want to make a member');
  }

  /**
   * @When I enter :text in the user select
   */
  public function iEnterInTheUserSelect($text) {
    $field_name = 'entity_id';
    $assert_session = $this->assertSession();
    $autocomplete_field = $assert_session->waitForElement('css', '[name="' . $field_name . '[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue($text);
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $assert_session->waitOnAutocomplete();
    // Make sure loading messages are finished before screenshooting.
    sleep(1);
  }

  /**
   * @Then I should see :options as the options
   */
  public function iShouldSeeAsTheOptions($options) {
    $page = $this->getSession()->getPage();
    $options = explode(',', $options);
    foreach ($options as $option) {
      $result = $page->find('xpath', "//a[text() = '".trim($option)."']");
      $this->assertNotEmpty(
        $result,
        sprintf('Autocomplete option "%s" should be visible', $option)
      );
    }
  }

  /**
   * @When I press :button
   */
  public function iPress($button) {
    $page = $this->getCurrentPage();
    $page->findButton($button)->press();
  }

  /**
   * @Then :selected is added to the group
   */
  public function isAddedToTheGroup($selected_email) {
    $user = $this->loadEntityByProperties('user', ['mail' => $selected_email]);
    $this->assertNotEmpty(
      $this->group->getMember($user),
      sprintf('The user with email "%s" should be in the group with id "%s"', $selected_email, $this->group->id())
    );
  }

}

