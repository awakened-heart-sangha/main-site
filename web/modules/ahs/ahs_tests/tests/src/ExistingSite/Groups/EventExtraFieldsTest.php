<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * @group ClonedDatabaseRequired
 */
class EventExtraFieldsTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $groups;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Training & joining info is shown when viewing a group

    Background:
      Given an event group
      And the event uses normal registration
      And experiences:
        | experience  |
        | ExperienceA |
        | ExperienceB |
      And I am logged in as a member

    Scenario: Group participants cannot register
      Given the event is open
      And the event has not ended
      And I am a participant in the event
      And the event is suggested
      When I view the event group
      Then I should not see a registration button
      And I should see "You are registered"
      And I should not see "now closed"
      And I should not see "eligible"
      And I should not see "suggested"

    Scenario: Only open groups can be joined, but closed still display eligibility
      Given the event is not open
      And the event has not ended
      And the event is suggested
      When I view the event group
      Then I should not see a registration button
      And I should not see "registration"
      And I should see "eligible"
      And I should not see "suggested"

    Scenario: Nonsuggested groups don't mention suggestion
      Given the event is open
      When I view the event group
      Then I should see a registration button
      And I should see "Everyone is eligible"
      And I should not see "suggested"

    Scenario: Groups can have registration deadline
      Given the event is open
      And the event registration deadline has not passed
      When I view the event group
      Then I should see "Registration is open until"

    Scenario: Groups cannot be joined after deadline, but still show eligibility
      Given the event is open
      And the event registration deadline has passed
      And the event has not ended
      And the event is suggested
      When I view the event group
      Then I should not see a registration button
      And I should see "now closed"
      And I should see "eligible"
      And I should not see "suggested"

    Scenario: Joining and eligibility are irrelevant after end
      Given the event is open
      And the event has ended
      And the event is suggested
      When I view the event group
      Then I should not see a registration button
      And I should not see "now closed"
      And I should not see "eligible"
      And I should not see "suggested"

    Scenario: Events without end dates are joinable
      Given the event is open
      And the event has no end date
      When I view the event group
      Then I should see a registration button
      And I should see "Everyone is eligible"

    Scenario: Event membership status is not overcached between users
      Given the event is open
      When I view the event group
      Then I should see a registration button
      And I should not see "You are registered"
      When I log in as a different member
      And I am a participant in the event
      And I view the event group
      Then I should not see a registration button
      And I should see "You are registered"
      And I should not see "Everyone is eligible"

    Scenario: Event membership status is not overcached within user
      Given the event is open
      When I view the event group
      Then I should see a registration button
      And I should not see "You are registered"
      When I join the event
      And I view the event group
      Then I should not see a registration button
      And I should see "You are registered"
      And I should not see "Everyone is eligible"

    Scenario: Anonymous should login for more info
      Given I am not logged in
      When I view the event group
      Then I should see "Login to register for this"
      And I should not see a registration button
      And I should see "Everyone is eligible"
      And I should not see "may not be suggested"
      And I should not see "is suggested"
      Given the event is suggested
      When I view the event group
      Then I should not see "may not be suggested"
      And I should not see "login to check"

    Scenario: Eligibility conditions
      Given the event is open
      And the event is suggested
      Given the event has eligibility conditions:
        | conditions  |
        | ExperienceA |
      Given the event has suggestion conditions:
        | conditions  |
        | ExperienceB |
      When I view the event group
      Then I should not see a registration button
      And I should see "You are not eligible"
      And I should not see "suggested"

    Scenario: Suggestion conditions
      Given the event is open
      And the event is suggested
      And the event has suggestion conditions:
        | conditions  |
        | ExperienceA |
      When I view the event group
      Then I should see a registration button
      And I should see "Everyone is eligible"
      And I should not see "is suggested for everyone"
      And I should see "is not suggested for you"

    Scenario: Access denied for unpublished groups
      Given an unpublished future suggested event
      When I view the event group
      # Access denied
      Then I should not see the event group

FEATURE;

  /**
   * @Then I should see a registration button
   */
  public function iShouldSeeARegistrationButton() {
    $this->assertTrue($this->hasRegistrationButton());
  }

  /**
   * @Then I should not see a registration button
   */
  public function iShouldNotSeeARegistrationButton() {
    $this->assertFalse($this->hasRegistrationButton());
  }

  protected function hasRegistrationButton() {
    $page = $this->getSession()->getPage();
    $link = $page->find('named', ['link_or_button', "Register"]);
    return !empty($link);
  }

}
