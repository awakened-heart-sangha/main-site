<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Behat\Gherkin\Node\TableNode;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * This doesn't need JS, and it's a poor fit for visual regression because it
 * depends a lot on the date.
 *
 * @group NewDatabaseRequired
 */
class CoursesAdminListTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use UserDefinitions;
  use GroupTrait;

  protected static $feature = <<<'FEATURE'
Feature: Training staff can see coming courses
    In order to see coming courses

    Background:
      Given courses:
        | title      | start date   | end date   | published |
        | Course1     | -3 weeks     | -2 weeks   | yes       |
        | Course2     | +8 weeks     | +10 weeks  | yes       |
        | Course3     | -1 week      | +1 week    | yes       |
        | Course4     | +4 weeks     | +5 weeks   | no        |

    Scenario: Training admins can see published & unpublished courses that have not yet finished
      Given I am logged in as an "training_admin"
      When I visit "admin/training/courses"
      Then I should see the following courses:
        | title  |
        | Course2 |
        | Course4 |
        | Course3 |
        | Course1 |

FEATURE;

  /**
   * @Given courses:
   */
  public function events(TableNode $table) {
    foreach($table->getHash() as $row) {
      $start_date = new DrupalDateTime($row['start date']);
      $end_date = new DrupalDateTime($row['end date']);
      $edit = [
        'type' => 'course',
        'status' => ($row['published'] == 'yes') ? TRUE : FALSE,
        'field_dates' => [
          'value' => $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
          'end_value' => $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
        ],
      ];
      $edit['label'] = $row['title'];
      $this->createGroup($edit);
    }
  }

  /**
   * @Then I should see the following courses:
   */
  public function iShouldSeeTheFollowingEvents(TableNode $table) {
    $tableHash = $table->getHash();
    $page = $this->getCurrentPage();
    $results = $page->findAll('css', "td.views-field-label:contains('Course')");
    $this->assertCount(
      count($tableHash),
      $results,
      "All courses should be listed"
    );
    $i = 0;
    foreach ($results as $result) {
      $this->assertEquals(
        $tableHash[$i]['title'],
        $result->getText(),
        "Courses should be listed in the correct order"
      );
      $i++;
    }

  }

}

