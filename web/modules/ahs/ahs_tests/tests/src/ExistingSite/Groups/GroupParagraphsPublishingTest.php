<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\user\Entity\User;

class GroupParagraphsPublishingTest extends ExistingSiteBehatBase  {

  use GroupDefinitions;
  use GroupTrait;

  protected $sectionParagraph;
  protected $event;
  protected $paragraph;
  protected $childParagraph;

  protected static $feature = <<<'FEATURE'
Feature: Published status of group paragraphs depends on section
    In order to have only the right group content
    Published status of group paragraphs depends on the section

    Scenario: Publishing course paragraphs
      Given an event group
      And a section paragraph on the group
      And a child paragraph on the group
      Then the section paragraph is published
      And the child paragraph is published
      When the section paragraph becomes published
      Then the child paragraph is published
      When the section paragraph becomes unpublished
      Then the child paragraph is unpublished
      When the section paragraph becomes disabled
      Then the section paragraph is unpublished
      And the child paragraph is unpublished
      When the section paragraph becomes enabled
      Then the section paragraph is unpublished
      And the child paragraph is unpublished
      When the section paragraph becomes published
      Then the child paragraph is published
      When the child paragraph becomes disabled
      Then the child paragraph is unpublished
      When the child paragraph becomes enabled
      Then the child paragraph is published

FEATURE;

  /**
   * @Given a/an :type paragraph on the group
   */
  public function aParagraphOnTheGroup($type) {
    $bundle = $type === 'section' ? 'layout_section': 'text';
    $property = $type . 'Paragraph';
    $this->{$property} = $this->createGroupParagraph($bundle);

    $settings = [
      'parent_uuid' => '',
      'region' => '',
      'layout' => '',
      'config' => '',
    ];
    if ($type === 'child') {
      $settings['parent_uuid'] = $this->sectionParagraph->uuid();
      $settings['region'] = 'content';
    }
    elseif ($type === 'section') {
      $settings['layout'] = 'layout_onecol';
    }
    $this->{$property}->setBehaviorSettings('layout_paragraphs', $settings);

    $this->{$property}->save();
    $this->group->save();
  }

  /**
   * @When the :type paragraph becomes published
   */
  public function theParagraphIsPublished($type) {
    $property = $type . 'Paragraph';
    $this->{$property}->setPublished()->save();
    $this->group->save();
  }

  /**
   * @When the :type paragraph becomes unpublished
   */
  public function theParagraphIsUnpublished($type) {
    $property = $type . 'Paragraph';
    $this->{$property}->setUnpublished()->save();
    $this->group->save();
  }

  /**
   * @When the :type paragraph becomes enabled
   */
  public function theParagraphIsEnabled($type) {
    $property = $type . 'Paragraph';
    $settings = [
      'region' => $type === 'child' ? 'content' : ''
    ];
    if ($type === 'child') {
      $settings['parent_uuid'] = $this->sectionParagraph->uuid();
    }
    $this->{$property}->setBehaviorSettings('layout_paragraphs', $settings);
    $this->{$property}->save();
    $this->group->save();
  }

  /**
   * @When the :type paragraph becomes disabled
   */
  public function theParagraphIsDisabled($type) {
    $property = $type . 'Paragraph';
    $settings = [
      'region' => '_disabled',
      'parent_uuid' => NULL,
    ];
    $this->{$property}->setBehaviorSettings('layout_paragraphs', $settings);
    $this->{$property}->save();
    $this->group->save();
  }

  /**
   * @Then the :type paragraph is :published
   */
  public function thenTheParagraphIsPublished($type, $published) {
    $property = $type . 'Paragraph';
    $para = $this->reloadEntity($this->{$property});
    $actualPublished = $para->isPublished();
    $expectedPublished = $published === 'published';
    $this->assertSame($expectedPublished, $actualPublished, "The $type paragraph was expected to be $published");
  }


}

