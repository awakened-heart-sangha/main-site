<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\ExistingSite\Groups\GroupExtraFieldsTest;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Tests the extra fields on course templates. Actual cloning of a template into
 * a course when a user registers is covered in GroupCloningTest.
 */
class CourseTemplateExtraFieldsTest extends CourseExtraFieldsTest {

  protected $groups;
  protected $course;
  protected $course_template;

  protected static $feature = <<<'FEATURE'
Feature: Training & joining info is shown when viewing a course template

    Background:
      Given a course_template group
      And the course_template uses normal registration
      And I am logged in as a member

    Scenario: Not all templates can be started
      And the course_template is not open
      When I view the course_template group
      Then I should not see a registration button

    Scenario: New students can start
      Given the course_template is open
      When I view the course_template group
      Then I should see a registration button
      When I press "Register"
      Then I should be on the start form

    Scenario: Previous students can restart
      Given the course_template is open
      And I have previously started the course
      When I view the course_template group
      Then I should see "You have already engaged"
      And I should see the previous course
      And I should see a registration button
      When I press "Register"
      Then I should be on the start form

FEATURE;

  /**
   * @Then I should be on the start form
   */
  public function iShouldBeOnTheStartForm() {
    $this->assertSession()->elementTextContains('css', 'h1', "Start " . $this->group->label());
  }

  /**
   * @Given I have previously started the course
   */
  public function ihavePreviouslyStartedTheCourse() {
    $this->course = $this->createGroup(['type' => 'course', 'field_template' => $this->course_template]);
    $this->course->addMember($this->getMe(), []);
  }

  /**
   * @Then I should see the previous course
   */
  public function iShouldSeeThePreviousCourse() {
    $this->assertSession()->pageTextContains($this->course->label());
  }

}
