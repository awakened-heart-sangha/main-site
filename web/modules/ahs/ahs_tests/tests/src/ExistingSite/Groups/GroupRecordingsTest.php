<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\TrainingTrait;

class GroupRecordingsTest extends ExistingSiteBehatBase {

  use TrainingTrait;

  protected $groups;
  protected $recordingsNote;
  protected $recordingsNoneNote;
  protected $event;

  protected static $feature = <<<'FEATURE'
Feature: Training & joining info is shown when viewing a group

    Background:
      Given an event group
      And the event is set to hide the recordings section when there are no recordings
      And the event has a no recordings note
      And the event has a recordings note

    Scenario: Anonymous cannot see group recordings
      Given I am not logged in
      When I view the event group
      Then I should not see "Recordings"
      Given the event has recordings
      When I view the event group
      Then I should not see "Recordings"

    Scenario: Authenticated cannot see group recordings
      Given I am logged in
      When I view the event group
      Then I should not see "Recordings"
      Given the event has recordings
      When I view the event group
      Then I should not see "Recordings"

    Scenario: Participants can see group recordings if there are recordings
      Given I am logged in
      And I am a participant in the event
      When I view the event group
      Then I should not see "Recordings"
      Given the event has recordings
      When I view the event group
      Then I should see "Recordings"

    Scenario: Participants can see recordings note
      Given I am logged in
      And I am a participant in the event
      When I view the event group
      Given the event has recordings
      When I view the event group
      Then I should not see the no recordings note
      And I should see the recordings note

    Scenario: Participants can see recordings section even when no recordings if set
      Given the event is set to show the recordings section when there are no recordings
      And I am logged in
      And I am a participant in the event
      When I view the event group
      Then I should see "Recordings"
      When I click "Recordings"
      Then I should see the no recordings note
      And I should see the recordings note

    Scenario: Recordings section hidden if starts more than a week in future
      Given the event is set to show the recordings section when there are no recordings
      And the event starts more than a week in the future
      And I am logged in
      And I am a participant in the event
      When I view the event group
      Then I should not see "Recordings"

FEATURE;

  /**
   * @Given the :bundle is set to hide the recordings section when there are no recordings
   */
  public function theGroupIsSetToHideRecordings($bundle) {
    $this->{$bundle}->set('field_recordings_none_hide', TRUE);
    $this->{$bundle}->save();
  }

  /**
   * @Then the :bundle starts more than a week in the future
   */
  public function theGroupStartsMoreThanAWeekInTheFuture($bundle) {
    $time = \Drupal::service('datetime.time')->getCurrentTime();
    $dateObject = DrupalDateTime::createFromTimestamp($time);
    $interval = new \DateInterval('P8D');
    $dateString = $dateObject->add($interval)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $this->{$bundle}->set('field_dates', [
      'value' => $dateString,
      'end_value' => $dateString,
    ]);
    $this->{$bundle}->save();
  }

  /**
   * @Given the :bundle is set to show the recordings section when there are no recordings
   */
  public function theGroupIsSetToShowRecordings($bundle) {
    $this->{$bundle}->set('field_recordings_none_hide', FALSE);
    $this->{$bundle}->save();
  }

  /**
   * @Given the :bundle has a recordings note
   */
  public function theGroupHasARecordingsNote($bundle) {
    $this->recordingsNote = $this->randomMachineName();
    $this->{$bundle}->set('field_recordings_note', $this->recordingsNote);
    $this->{$bundle}->save();
  }


  /**
   * @Given the :bundle has a no recordings note
   */
  public function theGroupHasANoRecordingsNote($bundle) {
    $this->recordingsNoneNote = $this->randomMachineName();
    $this->{$bundle}->set('field_recordings_none_note', $this->recordingsNoneNote);
    $this->{$bundle}->save();
  }

  /**
   * @Then I should see the no recordings note
   */
  public function iShouldSeeTheNoRecordingsNote() {
    $this->assertSession()->pageTextContains($this->recordingsNoneNote);
  }

  /**
   * @Then I should see the recordings note
   */
  public function iShouldSeeTheRecordingsNote() {
    $this->assertSession()->pageTextContains($this->recordingsNote);
  }

  /**
   * @Then I should not see the recordings note
   */
  public function iShouldNotSeeTheRecordingsNote() {
    $this->assertSession()->pageTextNotContains($this->recordingsNote);
  }

  /**
   * @Then I should not see the no recordings note
   */
  public function iShouldNotSeeTheNoRecordingsNote() {
    $this->assertSession()->pageTextNotContains($this->recordingsNoneNote);
  }

  /**
   * @Given the :bundle has recordings
   */
  public function theGroupHasRecordings($bundle) {
    $node = $this->createEntity('node', ['type' => 'session', 'title' => $this->randomMachineName()]);
    $media = $this->createEntity('media', ['bundle' => 'audio']);
    $node->set('field_media', $media);
    $node->save();
    $this->{$bundle}->addRelationship($node, 'group_node:session');
    // Assert the session is correctly defaulted to belonging primarily to this event.
    $content = $this->{$bundle}->getRelationships('group_node:session');
    $content = reset($content);
    $this->assertEquals($node->id(), $content->getEntity()->id());
    $this->assertTrue((bool) $content->get('field_primary')->value);
  }

}
