<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\group\Entity\Group;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;
use Drupal\commerce_product\Entity\Product;

trait GroupCloningTrait
{

  use GroupDefinitions;
  use GroupTrait;
  use UserTrait;
  use UserDefinitions;
  use FormTrait;
  use BrowsingDefinitions;

  /**
   * @When I go to the course_template start page
   */
  public function iGoToTheTemplateStartPage()
  {
    $uri = '/group/' . $this->course_template->id() . '/start';
    $this->drupalGet($uri);
  }

  /**
   * @When I go to the :bundle clone page
   */
  public function iGoToTheGroupClonePage($bundle)
  {
    $uri = 'entity_clone/group/' . $this->{$bundle}->id();
    $this->drupalGet($uri);
    $this->assertSession()->elementTextContains('css', 'h1', "Clone " . $this->{$bundle}->label());
  }

  /**
   * @Then a cloned :cloned_bundle is created from the :bundle
   */
  public function aClonedGroupIsCreated($cloned_bundle, $bundle)
  {
    $query = \Drupal::entityTypeManager()->getStorage('group')->getQuery();
    $groupIds = $query
      ->accessCheck(FALSE)
      ->sort('id', 'DESC')
      ->range(0, 1)
      ->execute();
    $groupId = reset($groupIds);
    $clone = Group::load($groupId);
    $this->assertNotNull($clone, "There should be a clone");
    $this->{"cloned_$cloned_bundle"} = $clone;
    $this->assertNotEquals($clone->id(), $this->{$bundle}->id(), "The clone should not be the same entity as the source entity. Probably cloning failed.");
    $this->assertSame($cloned_bundle, $clone->bundle(), "The cloned entity should be a $cloned_bundle");
  }

  /**
   * @Then the course_template is referenced from the cloned course
   */
  public function theTemplateIsReferencedFromTheClonedCourse()
  {
    $this->assertSame($this->course_template->id(), $this->cloned_course->get('field_template')->target_id);
  }

  /**
   * @Given the course_template has an expected duration of :weeks weeks
   */
  public function theCourseTemplateHasAnExpectedDurationOfWeeks($weeks)
  {
    $this->course_template->set('field_expected_weeks', $weeks)->save();
  }

  /**
   * @Then I should not be on the :bundle clone form
   */
  public function iShouldNotBeOnTheCloneForm($bundle)
  {
    $this->assertSession()->elementTextNotContains('css', 'h1', "Clone " . $this->{$bundle}->label());
  }

  /**
   * @Then I should be on the :bundle clone form
   */
  public function iShouldBeOnTheCloneForm($bundle)
  {
    $this->assertSession()->elementTextContains('css', 'h1', "Clone " . $this->{$bundle}->label());
  }


  /**
   * @Then the cloned :cloned_bundle has the same title as the :source_bundle
   */
  public function theCloneHasTheSameTitleAsTemplate($cloned_bundle, $source_bundle)
  {
    $cloneTitle = (string)$this->{"cloned_$cloned_bundle"}->get('label')->value;
    $sourceTitle = (string)$this->{$source_bundle}->get('label')->value;
    $this->assertEquals($sourceTitle, $cloneTitle, "Clone should have same title as template");
  }

  /**
   * @Then the cloned :bundle is published
   */
  public function theCloneIsPublished($bundle)
  {
    $this->assertTrue((bool)$this->{"cloned_$bundle"}->get('status')->value, "The clone should be published");
  }

  /**
   * @Then the cloned :bundle is not published
   */
  public function theCloneIsNotPublished($bundle)
  {
    $this->assertFalse((bool)$this->{"cloned_$bundle"}->get('status')->value, "The clone should not be published");
  }

  /**
   * @Then the cloned :bundle is private
   */
  public function theCloneIsPrivate($bundle)
  {
    $this->assertTrue((bool)$this->{"cloned_$bundle"}->get('field_private')->value, "The clone should be private");
  }

  /**
   * @Then the cloned :bundle is not private
   */
  public function theCloneIsNotPrivate($bundle)
  {
    $this->assertFalse((bool)$this->{"cloned_$bundle"}->get('field_private')->value, "The clone should not be private");
  }

  /**
   * @Then the cloned :bundle is open for registration
   */
  public function theCloneIsOpenForRegistration($bundle)
  {
    $this->assertTrue((bool)$this->{"cloned_$bundle"}->get('field_registration_open')->value, "The clone should be open for registration");
  }

  /**
   * @Then the cloned :bundle is not open for registration
   */
  public function theCloneIsNotOpenForRegistration($bundle)
  {
    $this->assertFalse((bool)$this->{"cloned_$bundle"}->get('field_registration_open')->value, "The clone should not be open for registration");
  }

  /**
   * @Then the cloned :bundle has the title :title
   */
  public function theCloneHasTheTitle($bundle, $title)
  {
    $this->assertSame($title, (string)$this->{"cloned_$bundle"}->get('label')->value, "The clone should have '$title' in its label field.");
  }

  /**
   * @Then the cloned :bundle starts today
   */
  public function theCloneStartsToday($bundle)
  {
    $time = \Drupal::service('datetime.time')->getCurrentTime();
    $this->assertFalse($this->{"cloned_$bundle"}->get('field_dates')->isEmpty(), "$bundle dates should not be empty.");
    $this->assertNotNull($this->{"cloned_$bundle"}->get('field_dates')->start_date, "$bundle start date should not be null.");
    $startTime = $this->{"cloned_$bundle"}->get('field_dates')->start_date->getTimestamp();
    $difference = $time - $startTime;
    // There's some kind of timezone issue.
    $this->assertLessThan(3601, $difference, "The course should start approximately now.");
    $this->assertGreaterThan(-3601, $difference, "The course should start approximately now.");
  }

  /**
   * @Then the cloned :bundle ends in :weeks weeks
   */
  public function theCloneEndsInWeeks($bundle, $weeks)
  {
    $time = \Drupal::service('datetime.time')->getCurrentTime();
    $this->assertFalse($this->{"cloned_$bundle"}->get('field_dates')->isEmpty(), "$bundle dates should not be empty.");
    $this->assertNotNull($this->{"cloned_$bundle"}->get('field_dates')->end_date, "$bundle end date should not be null.");
    $endTime = $this->{"cloned_$bundle"}->get('field_dates')->end_date->getTimestamp();
    $expectedEndTime = $time + (86400 * 7 * $weeks);
    $difference = $expectedEndTime - $endTime;
    $this->assertLessThan(7201, $difference, "The course should end in approximately $weeks weeks from now.");
    $this->assertGreaterThan(-7201, $difference, "The course should end in approximately $weeks weeks from now.");
  }

  /**
   * @Then the cloned :bundle starts on :date
   */
  public function theCloneStartsOnDate($bundle, $date)
  {
    $dateObject = new DrupalDateTime($date);
    $this->assertFalse($this->{"cloned_$bundle"}->get('field_dates')->isEmpty(), "$bundle dates should not be empty.");
    $this->assertNotNull($this->{"cloned_$bundle"}->get('field_dates')->start_date, "$bundle start date should not be null.");
    $this->assertEquals($dateObject, $this->{"cloned_$bundle"}->get('field_dates')->start_date);
  }

  /**
   * @Then the cloned :bundle ends on :date
   */
  public function theCloneEndsOnDate($bundle, $date)
  {
    $dateObject = new DrupalDateTime($date);
    $this->assertFalse($this->{"cloned_$bundle"}->get('field_dates')->isEmpty(), "$bundle dates should not be empty.");
    $this->assertNotNull($this->{"cloned_$bundle"}->get('field_dates')->end_date, "$bundle end date should not be null.");
    $this->assertEquals($dateObject, $this->{"cloned_$bundle"}->get('field_dates')->end_date);
  }

  /**
   * @Then the :bundle paragraph has been cloned to the :cloned_bundle
   */
  public function theGroupParagraphHasBeenCloned($bundle, $cloned_bundle)
  {
    $clonedParagraph = $this->{"cloned_$cloned_bundle"}->field_content->entity;
    $sourceParagraph = $this->reloadEntity($this->{$bundle})->field_content->entity;
    $this->assertEquals($this->paragraph->id(), $sourceParagraph->id(), "The source paragraph should still be referenced on the source group");
    $this->assertTrue($clonedParagraph instanceof ParagraphInterface, "There should be a paragraph referenced from field_content on the cloned group");
    $this->assertNotEquals($this->paragraph->id(), $clonedParagraph->id(), "The cloned group should reference a cloned paragraph not the original paragraph.");
    $this->assertTrue($sourceParagraph->isPublished(), "The source paragraph should still be published");
  }

  /**
   * @Then the cloned :bundle paragraph is published
   */
  public function theGroupParagraphIsPublished($bundle)
  {
    $paragraph = $this->{"cloned_$bundle"}->field_content->entity;
    $this->assertTrue($paragraph->isPublished(), "The cloned paragraph should be published");
  }

  /**
   * @Then the cloned :bundle paragraph is unpublished
   */
  public function theGroupParagraphIsUnpublished($bundle)
  {
    $paragraph = $this->{"cloned_$bundle"}->field_content->entity;
    $this->assertFalse($paragraph->isPublished(), "The cloned paragraph should be published");
  }

  /**
   * @Then the cloned :bundle has no content
   */
  public function theCloneHasNoContent($bundle)
  {
    $this->assertTrue($this->{"cloned_$bundle"}->get('field_content')->isEmpty(), "$bundle content should be empty.");
  }


  /**
   * @Then I am the owner of the cloned :bundle
   */
  public function iAmTheOwnerOfTheClonedGroup($bundle)
  {
    $this->assertEquals($this->{"cloned_$bundle"}->getOwnerId(), $this->getMe()->id());
  }

  /**
   * @Then I am a member of the cloned :bundle
   */
  public function iAmAMemberOfTheClonedGroup($bundle)
  {
    $this->assertNotEmpty($this->{"cloned_$bundle"}->getMember($this->getMe()), "My user account should be a group member.");
  }

  /**
   * @Then I am not a member of the cloned :bundle
   */
  public function iAmNotAMemberOfTheClonedGroup($bundle)
  {
    $this->assertFalse($this->{"cloned_$bundle"}->getMember($this->getMe()), "My user account should not be a group member.");
  }

  /**
   * @Then I am not a member of the :bundle
   */
  public function iAmNotAMemberOfTheGroup($bundle)
  {
    $this->assertFalse($this->{$bundle}->getMember($this->getMe()));
  }

  /**
   * @When I enter :datetime as the typical time
   */
  public function iEnterAsTheTypicaltime($datetime)
  {
    $this->setDateTimeFieldFromString($datetime, "fields[course][field_typical_time][0][value]");
  }

  /**
   * @When I enter :datetime as the :bundle start date/time
   */
  public function iEnterAsTheGroupStartTime($datetime, $bundle)
  {
    $this->setDateTimeFieldFromString($datetime, "fields[$bundle][field_dates][0][value]");
  }

  /**
   * @When I enter :datetime as the :bundle end date/time
   */
  public function iEnterAsTheGroupEndTime($datetime, $bundle)
  {
    $this->setDateTimeFieldFromString($datetime, "fields[$bundle][field_dates][0][end_value]");
  }


  /**
   * @When I enter :weeks as the duration in weeks
   */
  public function iEnterAsTheDurationInWeeks($weeks)
  {
    $field = $this->assertSession()->fieldExists('weeks');
    $field->setValue($weeks);
  }

  /**
   * @When I enter :title as the :bundle title
   */
  public function iEnterAsTheGroupTitle($title, $bundle)
  {
    $field = $this->assertSession()->fieldExists("fields[$bundle][label][0][value]");
    $field->setValue($title);
  }

  /**
   * @Then the cloned :bundle has no leaders
   */
  public function theClonedGroupHasNoLeaders($bundle)
  {
    $this->assertTrue($this->{"cloned_$bundle"}->get('field_leaders')->isEmpty());
  }

  /**
   * @Then the cloned :bundle has :sessions sessions
   */
  public function theClonedGroupHasSessions($bundle, $sessions) {
    $sessionContent = $this->{"cloned_$bundle"}->getRelationships('group_node:session');

    $sessionContent = array_filter($sessionContent, static function ($content_item) {
      return ($content_item->hasField('field_primary') && $content_item->field_primary->value);
    });

    $this->assertSame((int) $sessions, count($sessionContent), "The group should have $sessions sessions.");
  }

  /**
   * @Then the :bundle leader is also the leader of the cloned :cloned_bundle
   */
  public function theGroupLeaderIsAlsoTheClonedGroupLeader($bundle, $cloned_bundle)
  {
    $this->assertFalse($this->{"cloned_$bundle"}->get('field_leaders')->isEmpty(), "The cloned group should have a leader.");
    $this->assertSame($this->{$bundle}->get('field_leaders')->entity->id(), $this->{"cloned_$cloned_bundle"}->get('field_leaders')->entity->id(), "The same user should be leader of the cloned and original groups");
  }

  /**
   * @Then the leader is a mentor in the cloned :bundle
   */
  public function theLeaderIsAMentorInTheClonedGroup($bundle)
  {
    $this->assertLeadersAreMentors($this->{"cloned_$bundle"});
  }

  /**
   * @Given the :bundle has 2 enabled registration variations with deadlines
   */
  public function theGroupHas2EnabledRegistrationVariationsWithDeadlines($bundle) {
    $online = ProductVariation::create([
      'type' => 'ahs_registration',
      'field_registration_type' => 'online',
      'title' => 'Online',
    ]);
    $online->save();

    $online_product = Product::create([
      'type' => 'ahs_registration',
      'title' => 'Online',
    ]);
    $online_product->addVariation($online);
    $online_product->save();

    $settings = $online->getRegistrationSettings();
    $settings->set('close', '2020-12-31T23:59:59');
    $settings->set('confirmation', 'online message');
    $settings->save();

    $hermitage = ProductVariation::create([
      'type' => 'ahs_registration',
      'field_registration_type' => 'in_person_hermitage',
      'title' => 'Hermitage',
    ]);
    $hermitage->save();

    $hermitage_product = Product::create([
      'type' => 'ahs_registration',
      'title' => 'Hermitage',
    ]);
    $hermitage_product->addVariation($hermitage);
    $hermitage_product->save();

    $settings = $hermitage->getRegistrationSettings();
    $settings->set('close', '2020-12-31T23:59:59');
    $settings->set('confirmation', 'hermitage message');
    $settings->save();

    $this->{$bundle}->set('field_product', [$online_product, $hermitage_product])->save();
  }

  /**
   * @Then the cloned :bundle does not have a registration deadline
   */
  public function theClonedGroupHasNoRegistrationDeadline($bundle)
  {
    $this->assertTrue($this->{"cloned_$bundle"}->get('field_registration_deadline')->isEmpty());
  }

  /**
   * @Then the cloned :bundle has 2 disabled cloned registration variations without deadlines
   */
  public function theClonedGroupHas2DisabledClonedRegistrationVariations($bundle) {
    $originalProductIds = [];
    $originalVariationIds = [];
    $originalSettingIds = [];

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    foreach ($this->{$bundle}->get('field_product')->referencedEntities() as $product) {
      $originalProductIds[] = $product->id();
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product */
      foreach ($product->getVariations() as $variation) {
        $originalVariationIds[] = $variation->id();
        $originalSettingIds[] = $variation->getRegistrationSettings()->id();
      }
    }

    $this->assertCount(2, $this->{"cloned_$bundle"}->get('field_product'));

    $online_product = $this->{"cloned_$bundle"}->get('field_product')[0]->entity;
    $this->assertNotContains($online_product->id(), $originalProductIds, "The registration products should be cloned not referenced.");
    $online_product_variations = $online_product->getVariations();
    $online = reset($online_product_variations);

    $this->assertNotContains($online->id(), $originalVariationIds, "The registration variation should be cloned not referenced.");
    $this->assertContains($online->get('field_registration_type')->registration_type, ['online', 'in_person_hermitage'], "The registration variation type should be set.");
    $this->assertSame('online', $online->get('field_registration_type')->registration_type, "The first registration variation should be online.");
    $settings = $online->getRegistrationSettings();
    $this->assertSame('online message', $settings->get('confirmation')->value, "The registration variation confirmation message should have been cloned");
    $this->assertNotContains($settings->id(), $originalSettingIds);
    $this->assertFalse((bool)$settings->get('status')->value, "The registration variation should be disabled");
    $this->assertTrue($settings->get('close')->isEmpty(), "The registration variation should have no close date");

    $hermitage_product = $this->{"cloned_$bundle"}->get('field_product')[1]->entity;
    $this->assertNotContains($hermitage_product->id(), $originalProductIds, "The registration products should be cloned not referenced.");
    $hermitage_product_variations = $hermitage_product->getVariations();
    $hermitage = reset($hermitage_product_variations);

    $this->assertNotContains($hermitage->id(), $originalVariationIds, "The registration variation should be cloned not referenced.");
    $this->assertContains($hermitage->get('field_registration_type')->registration_type, ['online', 'in_person_hermitage'], "The registration variation type should be set.");
    $this->assertSame('in_person_hermitage', $hermitage->get('field_registration_type')->registration_type, "The second registration variation should be online.");
    $settings = $hermitage->getRegistrationSettings();
    $this->assertSame('hermitage message', $settings->get('confirmation')->value, "The registration variation confirmation message should have been cloned");
    $this->assertNotContains($settings->id(), $originalSettingIds);
    $this->assertFalse((bool)$settings->get('status')->value, "The registration variation should be disabled");
    $this->assertTrue($settings->get('close')->isEmpty(), "The registration variation should have no close date");
  }

}

