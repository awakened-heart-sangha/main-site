<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\commerce_price\Price;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\registration\Entity\RegistrationInterface;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBehatBase;
use Drupal\Tests\ahs_tests\Traits\CommerceTrait;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\EntityTrait;
use Drupal\Tests\ahs_tests\Traits\FormTrait;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\MailCollectionTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

class RegistrationStepsTest extends ExistingSiteJsBehatBase {

  use BrowsingDefinitions;
  use GroupDefinitions;
  use UserDefinitions;
  use GroupTrait;
  use UserTrait;
  use CommerceDefinitions;
  use EntityTrait;
  use CommerceTrait;
  use FormTrait;
  use RegistrationTrait;
  use MailCollectionTrait;

  protected $event;
  protected $storeId;

  protected static $feature = <<<'FEATURE'
Feature: Registration using commerce registration
    In order to encourage participants to donate at registration
    They need to checkout to register for events

    Scenario: Event registration with donation
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And the suggested amount for the event "Online" variation is 80
      And I am logged in
      And my name is "Fred Bloggs"
      When I view the event group
      When I press "Register for Online"
      Then I should not see "Pay now"
      And I should see "Donation to the Sangha"
      When I select "40" from the "donation_amount[radios]" radios
      And I press "Register"
      Then I should be on the payment information step of the checkout
      And I should not see a billing profile form
      When I submit my credit card details
      Then I see a "Thankyou for registering" message
      And I have donated "40"
      And I become a participant in the event with the type "student"
      And my name is still "Fred Bloggs"
      And a donation thankyou email is sent to me
      And a registration confirmation email is sent to me

    Scenario: Event registration with payment
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And the deposit for the event "Online" variation is 80
      And the event "Online" variation has the payment type "payment"
      And I am logged in
      And my name is "Fred Bloggs"
      When I view the event group
      When I press "Register for Online"
      Then I should see "Pay now"
      And I should not see "Donation to the Sangha"
      When I select "80" from the "payment_amount[radios]" radios
      And I press "Register"
      Then I should be on the payment information step of the checkout
      And I should not see a billing profile form
      When I submit my credit card details
      Then I see a "Thankyou for registering" message
      And I have donated "80"
      And I become a participant in the event with the type "student"
      And my name is still "Fred Bloggs"
      And a payment thankyou email is sent to me
      And a registration confirmation email is sent to me

    Scenario: Event registration with no donation
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And the suggested amount for the event "Online" variation is 80
      And I am logged in
      And my name is "Fred Bloggs"
      When I view the event group
      When I press "Register for Online"
      And I select "_other_" from the "donation_amount[radios]" radios
      And I enter "0" as the amount
      And I press "Register"
      Then I see a "Thankyou for registering" message
      And I become a participant in the event with the type "student"
      And my name is still "Fred Bloggs"
      And no thankyou email is sent to me
      And a registration confirmation email is sent to me

    Scenario: Event registration requires name
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And the suggested amount for the event "Online" variation is 100
      And I am logged in
      And my name is unknown
      When I view the event group
      When I press "Register for Online"
      And I press "Register"
      When I submit my credit card details
      Then I should see "First name"
      When I enter "Harry Jones" as my name
      And I press "Finish"
      And I become a participant in the event with the type "student"
      Then my name is now "Harry Jones"

    Scenario: Variation suggested amount is zero
      Given an event group
      And the event registration system is "registration"
      And there is an "New variation" variation for the event with type "online"
      And the suggested amount for the event "New variation" variation is 0
      And my name is "Fred Bloggs"
      And I am logged in
      When I view the event group
      When I press "Register for New variation"
      And I enter "10" as the amount
      And I press "Register"
      And I submit my credit card details
      Then I see a "Thankyou for registering" message
      And I have donated "10"
      And I become a participant in the event with the type "student"

    Scenario: Variation payment type is none
      Given an event group
      And the event registration system is "registration"
      And there is a "New" variation for the event with type "online"
      And the event "New" variation has the payment type "none"
      And my name is "Fred Bloggs"
      And I am logged in
      When I view the event group
      When I press "Register for New"
      And I press "Register"
      Then I see a "Thankyou for registering" message
      And I become a participant in the event with the type "student"

    Scenario: Event registration for in person
      Given an event group
      And the event registration system is "registration"
      And there is an "In person" variation for the event with type "in_person_hermitage"
      And the suggested amount for the event "In person" variation is 150
      And I am logged in
      And my name is "Fred Bloggs"
      When I view the event group
      When I press "Register for In person"
      And I select "150" from the "donation_amount[radios]" radios
      And I press "Register"
      And I submit my credit card details
      Then I should see "Health information"
      And I should see "Emergency contact"
      And I should not see "First name"
      When I enter "I have a heart condition" as "Health information"
      And I enter "Nancy" as "Emergency contact name"
      And I enter "123456" as "Emergency contact telephone"
      And I press "Finish"
      Then I see a "Thankyou for registering" message
      And I have donated "150"
      And I become a participant in the event with the type "student"
      And my name is still "Fred Bloggs"
      And my registration health information is "I have a heart condition"
      And my emergency contact name is "Nancy"
      And my emergency contact telephone is "123456"

    Scenario: In person participation profile is already completed
      Given an event group
      And the event registration system is "registration"
      And there is an "In person" variation for the event with type "in_person_hermitage"
      And the suggested amount for the event "In person" variation is 150
      And my existing emergency contact name is "Nancy"
      And my existing emergency contact telephone is "123456"
      And I am logged in
      And my name is "Fred Bloggs"
      When I view the event group
      When I press "Register for In person"
      And I press "Register"
      And I submit my credit card details
      Then I should see "Health information"
      And I should see "Emergency contact"
      When I enter "I have a heart condition" as "Health information"
      And I press "Finish"
      Then I see a "Thankyou for registering" message
      And I become a participant in the event with the type "student"
      And my name is still "Fred Bloggs"
      And my registration health information is "I have a heart condition"
      And my emergency contact name is "Nancy"
      And my emergency contact telephone is "123456"

    Scenario: Registration can be cancelled
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And the suggested amount for the event "Online" variation is 150
      And I am logged in
      And my name is "Harry Jones"
      When I view the event group
      When I press "Register for Online"
      And I press "Register"
      And I submit my credit card details
      When I view the event group
      And I press "Update registration"
      And I select "canceled" from the "state[0]" radios
      And I press "Save Registration"
      Then my registration is canceled
      When I view the event group
      When I press "Register for Online"
      And I press "Register"
      And I submit my credit card details
      Then I see a "Thankyou for registering" message
      And I become a participant in the event with the type "student"

      Scenario: Registration can be changed
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And there is an "In person" variation for the event with type "in_person_hermitage"
      And I am logged in
      And my name is "Harry Jones"
      When I view the event group
      When I press "Register for Online"
      And I enter "0" as the amount
      And I press "Register"
      When I view the event group
      And I press "Update registration"
      When I click "In person"
      And I press "Save and confirm"
      Then I see a "Thankyou for registering" message

    Scenario: Registration can be changed via participants tab
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And there is an "In person" variation for the event with type "in_person_hermitage"
      And I am logged in as an events_admin
      And my name is "Harry Jones"
      When I view the event group
      When I press "Register for Online"
      And I enter "0" as the amount
      And I press "Register"
      When I view the event participant list
      When I click "Edit registration"
      When I click "In person"
      When I select "registration_registrant_type_me" from the "who_is_registering" select
      And I press "Save and confirm"
      Then I see a "Thankyou for registering" message

    Scenario: Registration can be accessed via admin
      Given an event group
      And the event registration system is "registration"
      And there is an "Online" variation for the event with type "online"
      And there is an "In person" variation for the event with type "in_person_hermitage"
      And I am logged in
      And my name is "Harry Jones"
      When I view the event group
      When I press "Register for Online"
      And I enter "0" as the amount
      And I press "Register"
      Then I see a "Thankyou for registering" message
      And I am logged in as an events_admin
      When I view the event participant list
      When I click "Edit registration"
      When I click "In person"
      When I select "registration_registrant_type_user" from the "who_is_registering" select
      And I press "Save and confirm"
      Then I see a "Thankyou for registering" message


FEATURE;

  protected function setUp(): void {
    parent::setUp();
    $this->startMailCollection();
  }

  /**
   * @When I submit my credit card details
   */
  public function iSubmitMyCreditCardDetails() {
    $this->submitCreditCardDetails('Pay now');
  }

  /**
   * @Then my registration is canceled
   */
  public function iCanCancelRegistration() {
    $registrations = $this->event->getRegistrations($this->getMe());
    $registration = reset($registrations);
    $this->assertInstanceOf(RegistrationInterface::class, $registration);
    $this->assertSame('canceled', $registration->getState()->id());
  }

  /**
   * @When I view the :bundle participant list
   */
  public function iViewTheParticipantList($bundle) {
    $this->drupalGet('/group/' . $this->{$bundle}->id() . '/participants/joined');
    $this->assertSession()->pageTextContains($this->{$bundle}->label());
  }

  /**
   * @Given the suggested amount for the :bundle :name variation is :amount
   * @Given the deposit for the :bundle :name variation is :amount
   */
  public function theSuggestedAmountForTheGroupVariationIs($bundle, $name, $amount) {
    $host = $this->getRegistrationHostByName($this->{$bundle}, $name);
    $variation = $host->getEntity();
    $variation->setPrice(new Price($amount, 'GBP'))->save();
    $this->{$bundle} = $this->reloadEntity($this->{$bundle});
  }

  /**
   * @Given the :bundle :name variation has the payment type :type
   */
  public function theGroupVariationHasThePaymentType($bundle, $name, $type) {
    $host = $this->getRegistrationHostByName($this->{$bundle}, $name);
    $variation = $host->getEntity();
    $variation->set('field_payment_type', $type)->save();
    $this->{$bundle} = $this->reloadEntity($this->{$bundle});
  }

  /**
   * @When I enter :value as the amount
   */
  public function iEnterAsTheAmount($value) {
    $page = $this->getCurrentPage();
    $page->fillField('donation_amount[other]', $value);
  }

  /**
   * @When I enter :text as :field
   */
  public function iFillField($text, $field) {
    $page = $this->getCurrentPage();
    $page->fillField($field, $text);
  }

  /**
   * @Then I have donated :amount
   */
  public function iHaveDonated($amount) {
    $this->assertOrderByEmail(
      $this->getMe()->getEmail(),
      'ahs_registration',
      'ahs_registration',
      $amount,
      [],
      'paid');
  }

  /**
   * @Then I see a :text message
   */
  public function iSeeAMessage($text) {
    $this->assertSession()->pageTextContains($text);
  }

  /**
   * @Then my registration health information is :text
   */
  public function myregistrationHealthInformation($text) {
    $registration_storage = \Drupal::entityTypeManager()->getStorage('registration');
    $registrations = $registration_storage->loadByProperties(['user_uid' => $this->getMe()->id()]);
    $registration = reset($registrations);
    $this->assertInstanceOf(RegistrationInterface::class, $registration);
    $this->assertSame($text, $registration->get('field_health_information')->value);
  }

  /**
   * @Then my emergency contact name is :name
   */
  public function myEmergencyContactNameIs($name) {
    $profile = $this->getMe()->get('in_person_participation_profiles')->entity;
    $this->assertInstanceOf(ProfileInterface::class, $profile);
    $this->assertSame($name, $profile->get('field_emergency_contact_name')->value);
  }

  /**
   * @Then my emergency contact telephone is :telephone
   */
  public function myEmergencyContactTelephoneIs($telephone) {
    $profile = $this->getMe()->get('in_person_participation_profiles')->entity;
    $this->assertInstanceOf(ProfileInterface::class, $profile);
    $this->assertSame($telephone, $profile->get('field_emergency_contact_phone')->value);
  }

  /**
   * @Given my existing emergency contact name is :name
   */
  public function myExistingEmergencyContactNameIs($name) {
    $profile = $this->getMe()->get('in_person_participation_profiles')->entity;
    $profile = $profile ?? Profile::create(['type' => 'in_person_participation', 'uid' => $this->getMe()->id()]);
    $profile->set('field_emergency_contact_name', $name)->save();
  }

  /**
   * @Given my existing emergency contact telephone is :telephone
   */
  public function myExistingEmergencyContactTelephoneIs($telephone) {
    $profile = $this->getMe()->get('in_person_participation_profiles')->entity;
    $profile = $profile ?? Profile::create(['type' => 'in_person_participation', 'uid' => $this->getMe()->id()]);
    $profile->set('field_emergency_contact_phone', $telephone)->save();
  }

  /**
   * @When I enter ":name" as my name
   */
  public function iEnterAsMyName($name) {
    $nameParts = explode(" ", $name, 2);
    $page = $this->getCurrentPage();
    $page->fillField('ahs_registration_user_name[field_name][0][given]', $nameParts[0]);
    $page->fillField('ahs_registration_user_name[field_name][0][family]', $nameParts[1]);
  }

  /**
   * @Then a :type thankyou email is sent to me
   */
  public function aDonationThankyouEmailIsSentToMe($type) {
    $mails = $this->getMail(['to' => $this->getMyEmail(), 'subject' => "Thankyou for your $type"]);
    $this->assertMailCount($mails, 1);
  }

  /**
   * @Then a registration confirmation email is sent to me
   */
  public function aRegistrationConfirmationEmailIsSentToMe() {
    $mails = $this->getMail(['to' => $this->getMyEmail(), 'subject' => "You're registered"]);
    $this->assertMailCount($mails, 1);
  }

  /**
   * @Then no thankyou email is sent to me
   */
  public function noThankyouEmailIsSentToMe() {
    $mails = $this->getMail(['to' => $this->getMyEmail(), 'subject' => 'Thankyou']);
    $this->assertEmpty($mails, 'No thank you email should be sent to the customer');
  }

}
