<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;

class GroupCloningTest extends ExistingSiteBehatBase  {

  use GroupCloningTrait;

  protected $course_template;
  protected $cloned_course;
  protected $paragraph;
  protected $event;
  protected $course;
  protected $cloned_event;
  protected $cloned_course_template;

  protected static $feature = <<<'FEATURE'
Feature: Cloning groups
    In order for staff to quickly create groups
    They should be able to clone existing groups

    Scenario: Validation when cloning course templates as courses
      Given a course_template group
      And I am logged in as a training_admin
      When I go to the course_template clone page
      And I press "Clone"
      Then I should be on the course_template clone form
      Then I should see "A day and time should be specified unless the course is private"

    Scenario: Validation when cloning courses
      Given a course group
      And I am logged in as a training_admin
      When I go to the course clone page
      And I press "Clone"
      Then I should be on the course clone form
      And I should see "A day and time should be specified unless the course is private"

    Scenario: Validation when cloning events
      Given an event group
      And I am logged in as a training_admin
      When I go to the event clone page
      And I press "Clone"
      Then I should be on the event clone form

    Scenario: Cloning a course template as a course with defaults
      Given a course_template group called "test_template"
      And the course_template has an expected duration of 3 weeks
      And a published public paragraph on the group
      And I am logged in as a training_admin
      When I go to the course_template clone page
      Then I should see "Clone test_template course template"
      And I should see "Type" in the content region
      And I should see "Students" in the content region
      And I should see "Content" in the content region
      And I should see "Duration" in the content region
      And I enter "2020-05-01 19:00" as the typical time
      When I press "Clone"
      Then I should not be on the course_template clone form
      And a cloned course is created from the course_template
      And the course_template is referenced from the cloned course
      And the cloned course has the same title as the course_template
      And the cloned course is not published
      And the cloned course is not private
      And the cloned course is not open for registration
      And the cloned course starts on "2020-05-01 19:00"
      And the cloned course ends on "2020-05-15 20:00"
      And the cloned course has 3 sessions
      And the course_template paragraph has been cloned to the course
      And the cloned course paragraph is unpublished
      And I am the owner of the cloned course
      And I am not a member of the cloned course

    Scenario: Cloning a course template as a course with changed values
      Given a course_template group
      And the course_template has an expected duration of 3 weeks
      And a published public paragraph on the group
      And I am logged in as a training_admin
      When I go to the course_template clone page
      And I enter "2020-05-01 19:00" as the typical time
      And I enter "4" as the duration in weeks
      And I check "Published"
      And I check "Private"
      And I enter "test course title" as the course title
      And I select "clone" from the "content" radios
      When I press "Clone"
      Then I should not be on the course_template clone form
      And a cloned course is created from the course_template
      And the course_template is referenced from the cloned course
      And the cloned course has the title "Test course title"
      And the cloned course is published
      And the cloned course is private
      And the cloned course is not open for registration
      And the cloned course starts on "2020-05-01 19:00"
      And the cloned course ends on "2020-05-22 20:00"
      And the cloned course has 4 sessions
      And the course_template paragraph has been cloned to the course
      And the cloned course paragraph is published
      And I am the owner of the cloned course
      And I am not a member of the cloned course

    Scenario: Cloning a course template as a course template with defaults
      Given a course_template group called "test_template"
      And a published public paragraph on the group
      And I am logged in as a training_admin
      When I go to the course_template clone page
      And I select "course_template" from the "type" radios
      And I select "remove" from the "content" radios
      And I press "Clone"
      Then I should not be on the course_template clone form
      And a cloned course_template is created from the course_template
      And the cloned course_template has no content
      And I am the owner of the cloned course_template
      And I am not a member of the cloned course_template

    Scenario: Cloning a course template as a course template with changed values
      Given a course_template group called "test_template"
      And a published public paragraph on the group
      And I am logged in as a training_admin
      When I go to the course_template clone page
      And I select "course_template" from the "type" radios
      And I select "clone" from the "content" radios
      And I enter "test course template title" as the course_template title
      And I press "Clone"
      Then I should not be on the course_template clone form
      And a cloned course_template is created from the course_template
      And the cloned course_template has the title "Test course template title"
      And the course_template paragraph has been cloned to the course_template
      And the cloned course_template paragraph is published
      And I am the owner of the cloned course_template
      And I am not a member of the cloned course_template

    Scenario: Cloning an event as an event
      Given an event group
      And a published public paragraph on the group
      And I am logged in as an events_admin
      When I go to the event clone page
      And I enter "2020-05-01 19:00" as the event start date
      And I enter "2020-05-03 10:00" as the event end date
      And I enter "test event title" as the event title
      When I press "Clone"
      Then I should not be on the event clone form
      And a cloned event is created from the event
      And the cloned event has the title "Test event title"
      And the cloned event is not published
      And the cloned event is not open for registration
      And the cloned event starts on "2020-05-01 19:00"
      And the cloned event ends on "2020-05-03 10:00"
      And the event paragraph has been cloned to the event
      And the cloned event paragraph is unpublished
      And I am the owner of the cloned event
      And I am not a member of the cloned event

    Scenario: Cloning event registration variations
      Given an event group
      And the event has 2 enabled registration variations with deadlines
      And I am logged in as an events_admin
      When I go to the event clone page
      And I enter "2020-05-01 19:00" as the event start date
      And I enter "2020-05-03 10:00" as the event end date
      And I enter "test event title" as the event title
      When I press "Clone"
      Then I should not be on the event clone form
      And a cloned event is created from the event
      And the cloned event has the title "Test event title"
      And the cloned event is not open for registration
      And the cloned event does not have a registration deadline
      And the cloned event has 2 disabled cloned registration variations without deadlines

    # Leaders are cloned for events but not courses
    # Leaders are automatically members
    Scenario: Cloning an event with leader
      Given an event group
      And the event has a leader
      And I am logged in as an events_admin
      When I go to the event clone page
      And I enter "2020-05-01 19:00" as the event start date
      And I enter "2020-05-03 10:00" as the event end date
      And I press "Clone"
      Then I should not be on the event clone form
      And a cloned event is created from the event
      And the event leader is also the leader of the cloned event
      And the leader is a mentor in the cloned event

FEATURE;

}

