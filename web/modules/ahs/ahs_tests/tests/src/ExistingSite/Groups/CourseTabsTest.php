<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Groups;

use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\Traits\GroupTrait;
use Drupal\Tests\ahs_tests\Traits\TabsTrait;
use Drupal\Tests\ahs_tests\Traits\UserTrait;

class CourseTabsTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use GroupDefinitions;
  use UserDefinitions;
  use GroupTrait;
  use UserTrait;
  use TabsTrait;

  protected $course;

  protected static $feature = <<<'FEATURE'
Feature: Course tabs
    In order to organise information about an event
    Different tabs should be available depending on event role

    Background:
      Given a course group

    Scenario: Non-participant course tabs
      Given I am not logged in
      When I view the group
      Then I should see no tabs

    Scenario: Non-participant course tabs logged in
      Given I am logged in
      When I view the group
      Then I should see no tabs

    Scenario: Tabs for outsider training admin
      Given I am logged in as an "training_admin"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Participants  |
        | Manage        |
        | Clone         |
      When I view the participants
      Then I should see subtabs:
        | tab           |
        | Joined        |
        | Eligible      |
        | Suggested     |
        | Prospective   |

    Scenario: Course tabs for insider training admin
      Given I am logged in as an "training_admin"
      And I am a member of the group
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Participants  |
        | Manage        |
        | Clone         |
      When I view the participants
      Then I should see subtabs:
        | tab           |
        | Joined        |
        | Eligible      |
        | Suggested     |
        | Prospective   |

    Scenario: Course for outsider archivist
      Given I am logged in as an "archivist"
      When I view the group
      #Then I should see no tabs
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |

    Scenario: Tabs for outsider student_admin
      Given I am logged in as an "student_admin"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Participants  |
      When I view the participants
      Then I should see subtabs:
        | tab           |
        | Joined        |
        | Eligible      |
        | Suggested     |
        | Prospective   |

    Scenario: Tabs for outsider student_support
      Given I am logged in as an "student_support"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Participants  |
      When I view the participants
      Then I should see subtabs:
        | tab           |
        | Joined        |
        | Eligible      |
        | Suggested     |
        | Prospective   |

    Scenario: Course tabs for group member
      Given I am logged in
      And I am a member of the group
      When I view the group
      #Then I should see no tabs
      Then I should see no tabs

    Scenario: Course tabs for group admin
      Given I am logged in
      And I am a member of the group with the role "admin"
      When I view the group
      Then I should see tabs:
        | tab           |
        | View          |
        | Sessions        |
        | Participants  |
        | Manage        |
      When I view the participants
      Then I should see no subtabs

FEATURE;

  /**
   * @When I view the participants
   */
  public function iViewTheParticipants() {
    $this->drupalGet('/group/' . $this->group->id() . '/participants/joined');
    $this->assertSession()->pageTextContains($this->group->label());
  }

}

