<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Information;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use weitzman\DrupalTestTraits\Entity\NodeCreationTrait;

/**
 * SKIP because redirectin gis happening at platform route level, so not testable
 *
 * @group ClonedDatabaseRequired
 */
class RedirectsTestSkip extends ExistingSiteBehatBase {

  use BrowsingDefinitions;

  protected static $feature = <<<'FEATURE'
Feature: Redirects
    In order to maintain continuity for inbound links and SERPS
    We redirect some URLs from the old site

    Scenario Outline: Redirects
      When I go to "<old>"
      Then I am visiting "<new>"

      Examples:
        | old                              | new                |
        | /meditation                      | /meditation-online |
        | /hermitage-of-the-awakened-heart | /about             |
        | /local-groups                    | /locations         |

FEATURE;

}

