<?php

namespace Drupal\Tests\ahs_tests\ExistingSite\Information;

use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteBehatBase;
use weitzman\DrupalTestTraits\Entity\NodeCreationTrait;

class InformationTest extends ExistingSiteBehatBase {

  use BrowsingDefinitions;
  use UserDefinitions;
  use NodeCreationTrait;

  protected static $feature = <<<'FEATURE'
Feature: Information pages
    In order to offer information to the public
    We need static content pages

    Scenario: Information is accessible to anonymous at alias
      Given I am not logged in
      And an information page with the alias "abracadabra"
      When I go to "abracadabra"
      Then I am visiting "abracadabra"

    Scenario: Page title block is hidden on information pages
      Given I am not logged in
      And an information page with the title "abracadabra title"
      When I go to "abracadabra-title"
      Then I am visiting "abracadabra-title"
      And I should not see the page title block

FEATURE;

  /**
   * @Given an information page with the alias :alias
   */
  public function anInformationPageWithTheAlias($alias) {
    $fields = [
      'title' => $this->randomString(),
      'type' => 'information',
      'path' => [
        'pathauto' => FALSE,
        'alias' => "/$alias",
      ],
    ];
    $this->createNode($fields);
  }

  /**
   * @Given an information page with the title :title
   */
  public function anInformationPageWithTheTitle($title) {
    $fields = [
      'title' => $title,
      'type' => 'information',
    ];
    $this->createNode($fields);
  }

}

