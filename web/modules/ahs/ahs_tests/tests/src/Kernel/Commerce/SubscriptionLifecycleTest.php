<?php

namespace Drupal\Tests\ahs_tests\Kernel\Commerce;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_recurring\Entity\Subscription;
use Drupal\Tests\commerce_recurring\Kernel\RecurringKernelTestBase;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_recurring\Entity\BillingSchedule;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_product\Entity\ProductVariationType;

/**
 * Tests the subscription lifecycle.
 *
 * @group commerce_recurring
 */
class SubscriptionLifecycleTest extends RecurringKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $user = $this->createUser();
    $this->user = $this->reloadEntity($user);

    /** @var \Drupal\commerce_recurring\Entity\BillingScheduleInterface $billing_schedule */
    $billing_schedule = BillingSchedule::create([
      'id' => 'test_id',
      'label' => 'Hourly schedule',
      'displayLabel' => 'Hourly schedule',
      'billingType' => BillingSchedule::BILLING_TYPE_POSTPAID,
      'plugin' => 'fixed',
      'configuration' => [
        'trial_interval' => [
          'number' => '10',
          'unit' => 'day',
        ],
        'interval' => [
          'number' => '1',
          'unit' => 'month',
        ],
      ],
    ]);
    $billing_schedule->save();
    $this->billingSchedule = $this->reloadEntity($billing_schedule);

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::create([
      'id' => 'example',
      'label' => 'Example',
      'plugin' => 'example_onsite',
    ]);
    $payment_gateway->save();
    $this->paymentGateway = $this->reloadEntity($payment_gateway);

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = PaymentMethod::create([
      'type' => 'credit_card',
      'payment_gateway' => $this->paymentGateway,
      'uid' => $this->user->id(),
    ]);
    $payment_method->save();
    $this->paymentMethod = $this->reloadEntity($payment_method);

    /** @var \Drupal\commerce_product\Entity\ProductVariationTypeInterface $product_variation_type */
    $product_variation_type = ProductVariationType::load('default');
    $product_variation_type->setGenerateTitle(FALSE);
    $product_variation_type->save();
    // Install the variation trait.
    $trait_manager = \Drupal::service('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('purchasable_entity_subscription');
    $trait_manager->installTrait($trait, 'commerce_product_variation', 'default');

    $variation = ProductVariation::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '10.00',
        'currency_code' => 'USD',
      ],
      'billing_schedule' => $this->billingSchedule,
      'subscription_type' => [
        'target_plugin_id' => 'product_variation',
      ],
    ]);
    $variation->save();
    $this->variation = $this->reloadEntity($variation);




    // An order item type that doesn't need a purchasable entity.
    OrderItemType::create([
      'id' => 'test',
      'label' => 'Test',
      'orderType' => 'default',
    ])->save();

    $order_type = OrderType::load('default');
    $order_type->setWorkflowId('order_default_validation');
    $order_type->save();
  }

  /**
   * Tests the subscription lifecycle, without a free trial.
   *
   * Placing an initial order should create an active subscription.
   * Canceling the initial order should cancel the subscription.
   */
  public function testLifecycle() {
    $configuration = $this->billingSchedule->getPluginConfiguration();
    unset($configuration['trial_interval']);
    $this->billingSchedule->setPluginConfiguration($configuration);
    $this->billingSchedule->save();

    $first_order_item = OrderItem::create([
      'type' => 'test',
      'title' => 'I promise not to start a subscription',
      'unit_price' => [
        'number' => '10.00',
        'currency_code' => 'USD',
      ],
      'quantity' => 1,
    ]);
    $first_order_item->save();
    $second_order_item = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $this->variation,
      'unit_price' => [
        'number' => '2.00',
        'currency_code' => 'USD',
      ],
      'quantity' => '3',
    ]);
    $second_order_item->save();
    $initial_order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'uid' => $this->user,
      'order_items' => [$first_order_item, $second_order_item],
      'state' => 'draft',
    ]);
    $initial_order->save();

    // Confirm that placing the initial order with no payment method doesn't
    // create the subscription.
    $initial_order->getState()->applyTransitionById('place');
    $initial_order->save();
    $subscriptions = Subscription::loadMultiple();
    $this->assertCount(0, $subscriptions);

    // Confirm that placing an order with a payment method creates an
    // active subscription.
    $initial_order->set('state', 'draft');
    $initial_order->set('payment_method', $this->paymentMethod);
    $initial_order->save();
    $initial_order->getState()->applyTransitionById('place');
    $initial_order->save();
    $subscriptions = Subscription::loadMultiple();
    $this->assertCount(1, $subscriptions);
    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription */
    $subscription = reset($subscriptions);

    $this->assertEquals('active', $subscription->getState()->getId());
    $this->assertEquals($this->store->id(), $subscription->getStoreId());
    $this->assertEquals($this->billingSchedule->id(), $subscription->getBillingSchedule()->id());
    $this->assertEquals($this->user->id(), $subscription->getCustomerId());
    $this->assertEquals($this->paymentMethod->id(), $subscription->getPaymentMethod()->id());
    $this->assertEquals($this->variation->id(), $subscription->getPurchasedEntityId());
    $this->assertEquals($this->variation->getOrderItemTitle(), $subscription->getTitle());
    $this->assertEquals('3', $subscription->getQuantity());
    $this->assertEquals($this->variation->getPrice(), $subscription->getUnitPrice());
    $this->assertEquals($initial_order->id(), $subscription->getInitialOrderId());
    $orders = $subscription->getOrders();
    $this->assertCount(1, $orders);
    $order = reset($orders);
    $this->assertFalse($order->getTotalPrice()->isZero());
    $this->assertEquals('recurring', $order->bundle());
    // Confirm that the recurring order has an order item for the subscription.
    $order_items = $order->getItems();
    $this->assertCount(1, $order_items);
    $order_item = reset($order_items);
    $this->assertEquals($subscription->id(), $order_item->get('subscription')->target_id);
    $this->assertEquals('draft', $order->getState()->getId());

    // Test initial order cancellation.
    $initial_order->getState()->applyTransitionById('cancel');
    $initial_order->save();
    $subscription = $this->reloadEntity($subscription);
    $this->assertEquals('canceled', $subscription->getState()->getId());
  }

  /**
   * Tests the subscription lifecycle, with a free trial.
   *
   * Placing an initial order should create a trial subscription.
   * Canceling the initial order should cancel the trial.
   */
  public function testLifecycleWithTrial() {
    $first_order_item = OrderItem::create([
      'type' => 'test',
      'title' => 'I promise not to start a subscription',
      'unit_price' => [
        'number' => '10.00',
        'currency_code' => 'USD',
      ],
      'quantity' => 1,
    ]);
    $first_order_item->save();
    $second_order_item = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $this->variation,
      'unit_price' => [
        'number' => '2.00',
        'currency_code' => 'USD',
      ],
      'quantity' => '3',
    ]);
    $second_order_item->save();
    $initial_order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'uid' => $this->user,
      'order_items' => [$first_order_item, $second_order_item],
      'state' => 'draft',
    ]);
    $initial_order->save();

    // Confirm that placing the initial order creates a trial subscription,
    // even without a payment method.
    $initial_order->getState()->applyTransitionById('place');
    $initial_order->save();
    $subscriptions = Subscription::loadMultiple();
    $this->assertCount(1, $subscriptions);
    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription */
    $subscription = reset($subscriptions);

    $this->assertEquals('trial', $subscription->getState()->getId());
    $this->assertEquals($this->store->id(), $subscription->getStoreId());
    $this->assertEquals($this->billingSchedule->id(), $subscription->getBillingSchedule()->id());
    $this->assertEquals($this->user->id(), $subscription->getCustomerId());
    $this->assertNull($subscription->getPaymentMethod());
    $this->assertEquals($this->variation->id(), $subscription->getPurchasedEntityId());
    $this->assertEquals($this->variation->getOrderItemTitle(), $subscription->getTitle());
    $this->assertEquals('3', $subscription->getQuantity());
    $this->assertEquals($this->variation->getPrice(), $subscription->getUnitPrice());
    $this->assertEquals($initial_order->id(), $subscription->getInitialOrderId());
    $this->assertNotEmpty($subscription->getTrialStartTime());
    $this->assertNotEmpty($subscription->getTrialEndTime());
    $this->assertEquals(864000, $subscription->getTrialEndTime() - $subscription->getTrialStartTime());
    $orders = $subscription->getOrders();
    $this->assertCount(1, $orders);
    $order = reset($orders);
    $this->assertEquals('recurring', $order->bundle());
    $this->assertTrue($order->getTotalPrice()->isZero());
    // Confirm that the recurring order has an order item for the subscription.
    $order_items = $order->getItems();
    $this->assertCount(1, $order_items);
    $order_item = reset($order_items);
    $this->assertEquals($subscription->id(), $order_item->get('subscription')->target_id);

    // Test initial order cancellation.
    $initial_order->getState()->applyTransitionById('cancel');
    $initial_order->save();
    $subscription = $this->reloadEntity($subscription);
    $this->assertEquals('canceled', $subscription->getState()->getId());
  }

}
