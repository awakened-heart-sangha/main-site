<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Render\FormattableMarkup;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Drupal\Core\Routing\RouteMatch;

/**
 * Creates a decoupled user with an email but no name or password.
 */
trait EntityTrait
{

  /**
   * Reloads the given entity from the storage and returns it.
   *
   * Copied from core's EntityKernelTestBase.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be reloaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The reloaded entity.
   */
  protected function reloadEntity(EntityInterface $entity) {
    return $this->loadEntity($entity->getEntityTypeId(), $entity->id());
  }

  protected function loadEntity($entityType, $id) {
    $storage = $this->entityManager->getStorage($entityType);
    $storage->resetCache([$id]);
    return $storage->load($id);
  }

  /**
   * Creates a new entity. Borrowed from CommerceTestBase.
   *
   * @param string $entity_type
   *   The entity type to be created.
   * @param array $values
   *   An array of settings.
   *   Example: 'id' => 'foo'.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A new entity.
   */
  protected function createEntity($entity_type, array $values) {
    /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
    $storage = \Drupal::service('entity_type.manager')->getStorage($entity_type);
    $entity = $storage->create($values);
    $status = $entity->save();
    $this->assertEquals(SAVED_NEW, $status, new FormattableMarkup('Created %label entity %type.', [
      '%label' => $entity->getEntityType()->getLabel(),
      '%type' => $entity->id(),
    ]));
    // The newly saved entity isn't identical to a loaded one, and would fail
    // comparisons.
    $entity = $storage->load($entity->id());
    $this->markEntityForCleanup($entity);

    return $entity;
  }

  /**
   * Debugger method to save additional HTML output. Borrowed from CommerceTestBase.
   *
   * The base class will only save browser output when accessing page using
   * ::drupalGet and providing a printer class to PHPUnit. This method
   * is intended for developers to help debug browser test failures and capture
   * more verbose output.
   */
  protected function saveHtmlOutput() {
    $out = $this->getSession()->getPage()->getContent();

    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();

    if ($this->htmlOutputEnabled) {
      $html_output = '<hr />Ending URL: ' . $this->getSession()->getCurrentUrl();
      $html_output .= '<hr />' . $out;
      $html_output .= $this->getHtmlOutputHeaders();
      $this->htmlOutput($html_output);
    }
  }

  /**
   * Asserts that the passed field values are correct. Borrowed from CommerceTestBase.
   *
   * Ignores differences in ordering.
   *
   * @param array $field_values
   *   The field values.
   * @param array $expected_values
   *   The expected values.
   * @param string $message
   *   (optional) A message to display with the assertion. Do not translate
   *   messages:
   *   use \Drupal\Component\Render\FormattableMarkup::placeholderFormat()
   *   to embed variables in the message text, not t().
   *   If left blank, a default message will be displayed.
   */
  protected function assertFieldValues(array $field_values, array $expected_values, $message = '') {
    $valid = TRUE;
    if (count($field_values) == count($expected_values)) {
      foreach ($expected_values as $value) {
        if (!in_array($value, $field_values)) {
          $valid = FALSE;
          break;
        }
      }
    }
    else {
      $valid = FALSE;
    }

    $this->assertNotEmpty($valid, $message);
  }

  protected function getCurrentRouteEntity($entityType = NULL) {
    $url = $this->getSession()->getCurrentUrl();
    $simple = \Drupal::service('router.no_access_checks')->match($url);
    return;

/*        $router = $this->container->get('router.no_access_checks');
   $routeMatch = $router->match($url);
   var_dump($routeMatch);
   $request = Request::create($url);

   $routeMatch = $this->container->get('router.no_access_checks')->matchRequest($request);

//$routeMatch = new RouteMatch($this->container->get('router.no_access_checks')->matchRequest($request));
var_dump($routeMatch);
//$route = $route_match[RouteObjectInterface::ROUTE_OBJECT];
$routeMatch = RouteMatch::createFromRequest($request);
var_dump($routeMatch);

$route = $routeMatch->getRouteObject();
var_dump($route);
return; */

    // Entity will be found in the route parameters.
    if ($parameters = $route->getOption('parameters')) {
      // Determine if the current route represents an entity.
      foreach ($parameters as $name => $options) {
        if (isset($options['type']) && strpos($options['type'], 'entity:') === 0) {
          $entity = $route_match->getParameter($name);
          if (empty($entityType) || $entity->getEntityTypeId() === $entityType) {
            return $entity;
          }
        }
      }
    }
  }

  protected function loadEntityByProperties($entityType, array $properties, $strict = TRUE) {
    $storage = $this->entityTypeManager->getStorage($entityType);
    $entities = $storage->loadByProperties($properties);
    if ($strict) {
      if (count($entities) > 1) {
        throw new \Exception('Multiple ' . $entityType . ' entities match these properties.');
      }
    }
    $entity = end($entities);
    return $entity;
  }

  protected function assertEntityType($entity, $type) {
    $this->assertInstanceOf('Drupal\Core\Entity\EntityInterface', $entity);
    $this->assertSame($type, $entity->getEntityTypeId(), 'Entity should be of specifed type');
  }

  protected function deleteEntitiesByProperties($entityType, array $properties) {
    $storage = $this->entityTypeManager->getStorage($entityType);
    $entities = $storage->loadByProperties($properties);
    if (is_array($entities)) {
      foreach ($entities as $entity) {
        $entity->delete();
      }
    }
  }

}
