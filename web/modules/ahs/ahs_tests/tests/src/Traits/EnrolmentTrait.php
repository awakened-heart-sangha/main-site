<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_recurring\Entity\BillingSchedule;
use Drupal\group\Entity\Group;
use Drupal\Tests\ahs_tests\Traits\Definitions\CommerceDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;

trait EnrolmentTrait {

  use CronTrait;

  protected $enrolmentFormUrl;

  protected $membershipSku = 'ahs_membership_monthly';

  protected $name = [
    'given_name' => 'Fred',
    'family_name' => 'bloggs',
  ];

  protected $address = [
    'address_line1' => '70 Happy Row',
    'locality' => 'London',
    'postal_code' => 'SE17 3FG',
  ];

  protected $phone = '020 7856 7324';

//  /**
//   * @Given I have enrolled
//   */
//  public function iHaveEnrolled() {
//    $this->iAmLoggedIn();
//    $this->iSubmitAnEnrolmentForm();
//    $this->anEnrolmentIsStartedForMe();
//    $this->iCompleteTheEnrolmentPaymentSteps();
//    $this->theOrderShouldBeInTheState('paid');
//  }

  /**
   * @Given I have enrolled
   * @Given I have enrolled giving :amount per month
   */
  public function ihaveEnrolled($amount = NULL) {
    $this->myEmail = $this->getMe()->getEmail();
    $this->iAmLoggedIn();
    $this->iSubmitAnEnrolmentForm($amount);
    $this->anEnrolmentIsStartedForMe();
    $this->iShouldBeOnTheAboutStepOfTheCheckout();
    $this->debugBasic('about');
    $this->iSubmitMyNameAndAddress();
    $this->iShouldBeOnThePaymentInformationStepOfTheCheckout();
    $this->debugBasic('payment');
    $this->iSubmitMyCreditCardDetails();
    $this->debugBasic('details');
    $order = Order::load($this->orderId);
    $order = $this->reloadEntity($order);
    $this->assertSame('paid', $order->getState()->getId(), "The order should be completed.");
  }


  /**
   * @When I submit an enrolment form
   * @When I submit an enrolment form specifying :amount per month
   */
  public function iSubmitAnEnrolmentForm($amount = NULL) {
    $this->submitEnrolmentForm($this->getMyEmail(), $amount);
  }

  /**
   * Get and submit an enrolment webform
   *
   * @param $amount
   * @param $email
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function submitEnrolmentForm($email, $amount = NULL) {
    // Store the amount for use in later steps.
    $this->amount = $amount ?? 30;

    $this->drupalGet("/" . $this->enrolmentFormUrl);
    $this->waitToPassHoneypot();

    // Complete the enrolment webform.
    $this->assertSession()->addressEquals($this->enrolmentFormUrl);
    $formValues = ['monthly_donation[select]' => $this->amount];
    if ($this->isLoggedOut()) {
      $formValues['email'] = $email;
    }

    $this->submitForm($formValues, 'Join today');

    // Form submits successfull
    $this->assertSession()->pageTextNotContains('Join today');
  }

  /**
   * @Then an enrolment is started for me
   */
  public function anEnrolmentIsStartedForMe() {
    $this->assertEnrolmentOrderByEmail($this->getMyEmail());
  }

  /**
   * Assert that an enrolment order is started.
   *
   * @param $email
   */
  protected function assertEnrolmentOrderByEmail($email) {
    $this->orderId = $this->assertOrderByEmail(
      $email,
      'ahs_lah_enrolment',
      'ahs_lah_enrolment',
      $this->amount,
      [$this->membershipSku],
      'draft')
      ->id();
  }

  /**
   * @When I complete the enrolment payment steps
   */
  public function iCompleteTheEnrolmentPaymentSteps() {
    $this->iShouldBeOnTheAboutStepOfTheCheckout();
    $this->iSubmitMyNameAndAddress();
    $this->iShouldBeOnThePaymentInformationStepOfTheCheckout();
    $this->iSubmitMyCreditCardDetails();
  }

  protected function setUpWebform() {
    $originalWebform = $this->entityTypeManager->getStorage('webform')->load('ahs_lah_enrolment_form');
    $webform = $originalWebform->createDuplicate();
    $id = $this->randomMachineName();
    $webform->set('id', $id);
    $webform->save();
    $this->markEntityForCleanup($webform);

    $handler = $webform->getHandler('enrolment_commerce_order');
    $config = $handler->getConfiguration();

    // Setup a store if we're on an empty database without one.
    $store = \Drupal::service('commerce_store.chain_store_resolver')->resolve();
    if (empty($store)) {
      $store = $this->setUpTestStore();
      $config['settings']['store']['store_entity'] = (string) $store->id();
    }

    // Make sure the webform points to a membership product.
    $this->ensureMembershipProduct();
    $variation = $this->loadEntityByProperties('commerce_product_variation', ['sku' => $this->membershipSku]);
    $this->assertNotEmpty($variation);
    $config['settings']['order_item']['product_variation_entity'] = (string) $variation->id();

    $handler->setConfiguration($config);
    $webform->updateWebformHandler($handler);

    // Temporarily open the enrolment webform at random url.
    $this->enrolmentFormUrl = $id;
    $this->setUpWebformAtUrl($webform, $this->enrolmentFormUrl);
  }

  protected function ensureMembershipProduct() {
    $variation = $this->loadEntityByProperties('commerce_product_variation', ['sku' => $this->membershipSku]);
    if (!empty($variation)) {
      // Check the variation has a product, in case the data has got corrupt.
      $productId = $variation->getProductId();
      if (empty($productId)) {
        throw new \Exception('Variation is missing a product Id');
      }
    }
    else {
      $variationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');
      $variation = $variationStorage->create([
        'type' => 'ahs_recurring_donation',
        'sku' => $this->membershipSku,
        'title' => 'Test membership',
        'billing_schedule' => BillingSchedule::load('monthly'),
        'subscription_type' => [
          'target_plugin_id' => 'product_variation',
        ],
        'price' => [
          'number' => '10.00',
          'currency_code' => 'GBP',
        ],
      ]);
      $variation->save();
      $this->markEntityForCleanup($variation);

      $productStorage = $this->entityTypeManager->getStorage('commerce_product');
      $product = $productStorage->create([
        'type' => 'ahs_recurring_donation',
        'variations' => [$variation],
        'title' => 'Membership',
      ]);
      $product->save();
      $this->markEntityForCleanup($product);
    }
  }


  /**
   * @Then I should be on the About step of the checkout
   */
  public function iShouldBeOnTheAboutStepOfTheCheckout() {
    $this->assertSession()->pageTextContains('About you');
  }

//  /**
//   * @When I submit my name and address
//   */
//  public function iSubmitMyNameAndAddress() {
//    $this->assertSession()->pageTextContains('About you');
//    $page = $this->getCurrentPage();
//    if ($selector = $page->find('named', ['field', 'ahs_billing_information[profile][select_address]'])) {
//      $selector->selectOption('_new');
//      $this->assertSession()->assertWaitOnAjaxRequest();
//      $this->assertSession()->waitForField('First name');
//    }
//    $page->fillField('First name', $this->name['given_name']);
//    $page->fillField('Last name', $this->name['family_name']);
//    $page->fillField('Street address', $this->address['address_line1']);
//    $page->fillField('Post town', $this->address['locality']);
//    $page->fillField('Postal code', $this->address['postal_code']);
//    $page->fillField('Telephone', $this->phone);
//    $page->findButton('Continue to next step')->press();
//
//    $this->assertSession()->pageTextNotContains('About you');
//    if (!empty($this->orderId)) {
//      $order = $this->loadEntity('commerce_order', $this->orderId);
//      $this->assertNotEmpty($order->getBillingProfile(), "The order should have a billing profile.");
//    }
//  }

  /**
   * @When I submit my name and address
   */
  public function iSubmitMyNameAndAddress() {
    $this->assertSession()->pageTextContains('About you');
    $this->fillBillingInformation($this->name['given_name'], $this->name['family_name'], $this->address, $this->phone);
    $page = $this->getCurrentPage();
    $page->findButton('Continue to next step')->press();

    $this->assertSession()->pageTextNotContains('About you');
    if (!empty($this->orderId)) {
      $order = $this->loadEntity('commerce_order', $this->orderId);
      $this->assertNotEmpty($order->getBillingProfile(), "The order should have a billing profile.");
    }
  }

  protected function ensureInitialMaterials() {
    $initialMaterialsSkus = ['book_hom', 'book_ocs', 'book_dhb_course_companion'];
    foreach($initialMaterialsSkus as $sku) {
      $this->ensureProductBySku($sku);
    }
  }

  protected function ensureDHBGroups() {
    $group = Group::load(592);
    if (!$group) {
      $this->createGroup(['type' => 'course_template', 'label' => 'Getting Started', 'id' => 592]);
    }
    $group = Group::load(1);
    if (!$group) {
      $group = $this->createGroup(['type' => 'special', 'label' => 'Discovering the Heart of Buddhism']);
    }
    return $group;
  }

  protected function aMonthPassesAfterEnrolment() {
    // Advance time by 1 month.
    \Drupal::service('datetime.time')->setTime("+32 days");

    // Reset static caches. Without this we get a weird bug where the recurring order
    // payment method owner does not have the commerce remote id set.
    \Drupal::service('entity.memory_cache')->deleteAll();

    // We don't need to run cron twice, because ultimate cron is configured so
    // that advancedqueue cron runs after recurring cron.
    $this->runCronJobs();
  }

  /**
   * @Then a first donation should be received from me
   * @Then a first donation of :amount should be received from me
   */
  public function aFirstDonationShouldBeReceivedFromMe($amount = NULL) {
    $amount = is_null($amount) ? $this->amount : $amount;
    $enrolmentOrder = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertOrderPayment($enrolmentOrder, $amount);
  }

}
