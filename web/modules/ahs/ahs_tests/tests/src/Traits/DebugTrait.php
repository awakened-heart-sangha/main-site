<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\Core\File\FileSystemInterface;
use PHPUnitBehat\TestTraits\BehatTestTrait;
use Behat\Behat\EventDispatcher\Event\AfterStepTested;
use Behat\Behat\EventDispatcher\Event\StepTested;
use Behat\Behat\EventDispatcher\ServiceContainer\EventDispatcherExtension;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Behat\Testwork\Tester\Result\TestResult;

/**
 * A trait to help with debugging tests.
 */
trait DebugTrait
{

  /**
   * The directory to output debugging info for test failures.
   *
   * @var string
   */
  protected $failuresDirectory;

  /**
   * The log level which will trigger a test failure.
   *
   * @var
   */
  protected $failOnLogsLevel = RfcLogLevel::ERROR;

  protected function setUpDebugging() {
    // Setup Behat to catch failures.
    if (method_exists($this, 'getBehatContainer')) {
      $dispatcher = $this->getBehatContainer()->get(EventDispatcherExtension::DISPATCHER_ID);
      $dispatcher->addListener(StepTested::AFTER, [$this, 'debugFailures']);
    }

    // Make this a logger, so we can fail on logs.
    $this->container->get('logger.factory')->addLogger($this);

    // Mark the start of the test in the log,
    // which helps to track down which logs come from which test.
    $className = (new \ReflectionClass($this))->getShortName();
    \Drupal::logger('ahs_tests')->notice($className . ": " . $this->getTestLabel() . "; " . static::class );
  }

    /**
     * Respond to test failures by generating debugging output.
     *
     * @param AfterStepTested $event
     */
     public function debugFailures(AfterStepTested $event)
    {
      // If the test retries, only output for the final attempt.
      if (method_exists($this, 'isFinalAttempt') && !$this->isFinalAttempt()) {
        return;
      }
      if ($event->getTestResult()->getResultCode() === TestResult::FAILED) {
          try {
            $stepText = $event->getStep()->getText();
            $this->debugPage($stepText);
          }
          catch (\Exception $e) {}
      }
    }

  /**
   * Pauses the scenario until the user presses a key. Useful when debugging a scenario.
   *
   * @Then (I )break
   */
  public function break() {
    fwrite(STDOUT, "\033[s \033[93m[Breakpoint] Press \033[1;93m[RETURN]\033[0;93m to continue, or 'q' to quit...\033[0m");
    do {
      $line = trim(fgets(STDIN, 1024));
      //Note: this assumes ASCII encoding.  Should probably be revamped to
      //handle other character sets.
      $charCode = ord($line);
      switch ($charCode) {
        case 0: //CR
        case 121: //y
        case 89: //Y
          break 2;
        // case 78: //N
        // case 110: //n
        case 113: //q
        case 81: //Q
          throw new \Exception("Exiting test intentionally.");
        default:
          fwrite(STDOUT, sprintf("\nInvalid entry '%s'.  Please enter 'y', 'q', or the enter key.\n", $line));
          break;
      }
    } while (true);
    fwrite(STDOUT, "\033[u");
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    if ($level <= $this->failOnLogsLevel) {
      $this->fail("Error (level " . $level . ") logged: " . strtr($message, $context));
    }
  }

  protected function debugBasic($name = '', $directory = NULL) {
    if (!$this->hasSession()) {
      return;
    }
    $directory = $directory ?? $this->getDebugDirectory();
    // Write out page HTML to file.
    $name = empty($name) ? microtime() : $name;
    $name = $this->sanitiseFilePathComponent($name);
    $filename = $directory . $name . '.html';
    // Append filename if already exists.
    $path = \Drupal::service('file_system')->getDestinationFilename($filename, 0);
    $body = $this->getSession()->getPage()->find('css', 'body');
    $content = $body ? $body->getOuterHtml() : $this->getCurrentPageContent();
    file_put_contents($path, $content);
  }


  protected function getDebugDirectory() {
    if (!empty(getenv('PHPUNIT_DEBUG_OUTPUT_PUBLIC_PATH'))) {
      $base['stem'] = "public://" . getenv('PHPUNIT_DEBUG_OUTPUT_PUBLIC_PATH');
    }
    else {
      $base['stem'] = "public://test-output";
    }
    $class = $this->sanitiseFilePathComponent((new \ReflectionClass($this))->getShortName());
    $scenario = $this->sanitiseFilePathComponent($this->getTestLabel());
    $base['test'] = $class . "-" . $scenario;
    $directory = implode('/', $base) . "/";
    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    return $directory;
  }

}
