<?php

namespace Drupal\Tests\ahs_tests\Traits;


use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Element\NodeElement;

trait TabsTrait
{

  /**
   * @Then I should see no tabs
   */
  public function iShouldSeeNoTabs() {
    $tabsList = $this->getTabsElement('primary');
    $this->assertNoTabs($tabsList, "No primary tabs should be present.");
  }

  /**
   * @Then I should see tabs:
   */
  public function iShouldSeeTabs(TableNode $table) {
    $tabsList = $this->getTabsElement('primary');
    $this->assertNotNull($tabsList, "No tabs found, but tabs expected");
    $this->assertTabsFromTable($tabsList, $table);
  }

  /**
   * @Then I should see a/an :tab tab
   */
  public function iShouldSeeATab($tab) {
    $tabsElement = $this->getTabsElement('primary');
    $this->assertNotNull($tabsElement, "No tabs found, but tab expected");
    $this->assertTabInElement($tab, $tabsElement);
  }

  /**
   * @Then I should see subtabs:
   */
  public function iShouldSeeSubtabs(TableNode $table) {
    $tabsList = $this->getTabsElement('secondary');
    $this->assertNotNull($tabsList, "No subtabs found, but subtabs expected");
    $this->assertTabsFromTable($tabsList, $table);
  }

  /**
   * @Then I should see no subtabs
   */
  public function iShouldSeeNoSubtabs() {
    $tabsList = $this->getTabsElement('secondary');
    $this->assertNoTabs($tabsList, "No secondary tabs should be present.");
  }

  protected function getTabsElement($type) {
    $nav = $this->getSession()->getPage()->find('css', 'nav.tabs');
    if (is_null($nav)) {
      // Try in admin theme instead.
      $nav = $this->getSession()->getPage()->find('css', 'nav.tabs-wrapper');
    }
    if (is_null($nav)) {
      return NULL;
    }
    return $nav->find('css', "ul.tabs--$type");
  }

  protected function getTabsFromElement($tabsElement) {
    $tabsElements = $tabsElement->findAll('css', 'a');
    $tabs = [];
    foreach ($tabsElements as $tabElement) {
      $str = $tabElement->getText();
      $substring = "(active tab)";
      if (substr($str,-strlen($substring))===$substring) $str = substr($str, 0, strlen($str)-strlen($substring));
      $tabs[] = preg_replace('/(active tab)$/', '', $str);
    }
    return $tabs;
  }

  protected function assertTabsFromTable($tabsElement, $table) {
    $tabs = $this->getTabsFromElement($tabsElement);
    $expectedTabs = [];
    foreach($table->getHash() as $row) {
      $expectedTabs[] = $row['tab'];
    }
    $this->assertSame($expectedTabs, $tabs);
  }

  protected function assertTabInElement($tabName, $tabsElement) {
    $tabs = $this->getTabsFromElement($tabsElement);
    $this->assertContains($tabName, $tabs);
  }

  protected function assertNoTabs($tabsList, $message) {
    $message = $tabsList instanceof NodeElement ? $message . "\nTabs were: " . $tabsList->getText() : $message;
    $this->assertNull($tabsList, $message);
  }

}
