<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Behat\Mink\Element\Element;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Tests\ahs_tests\ExistingSite\ExistingSiteJsBase;
use Behat\Mink\Exception\ElementNotFoundException;

/**
 * A trait to help interacting with forms.
 */
trait FormTrait
{

  protected function getRadios($name, Element $container = NULL) {
    if (is_null($container)) {
      $container = $this
        ->getSession()
        ->getPage();
    }
    $radios = $container->findAll('named', ['radio', $name]);
    $this->assertNotEmpty($radios, "Radio elements named '$name' should be found");
    return $radios;
  }

  /**
   * Helper function to get radio options.
   *
   * Adapted from BrowserTestBase, but extended to handle radios.
   *
   * @param string $name: Name or ID of radios.
   * @param \Behat\Mink\Element\Element $container: (optional) Container element to check against. Defaults to current page.
   *
   * @return array
   *   Associative array of option keys and values.
   */
  protected function getRadioOptions($name, Element $container = NULL) {
    $elements = $this->getRadios($name, $container);
    foreach($elements as $element) {
      $this->assertSame('radio', $element->getAttribute('type'), "Element should be a radio");
      $id = $element->getAttribute('id');
      $labelElement = $this->getSession()->getPage()->find('css', 'label[for="' . $id . '"]');
      $label = $labelElement->getText();
      $value = $element->getAttribute('value');
      $options[$value] = $label;
    }
    return $options;
  }

  protected function selectRadio($name, $value, $container = NULL) {
    $radios = $this->getRadios($name, $container);
    foreach($radios as $radio) {
      if ($radio->getAttribute('value') == $value) {
        try {
          $radio->click();
        }
        catch(UnsupportedDriverActionException $e) {
          // Goutte doesn't support clicking radios.
          $this->getCurrentPage()->fillField($radio->getAttribute('id'), $value);
        }
        return;
      }
    }
    throw new \Exception(sprintf("Radio button with value '%s' should be found.", $value));
  }

  /**
   * Some forms are protected by a honeypot that rejects submission if they
   * happen too quickly after the page is loaded.
   */
  protected function waitToPassHoneypot() {
    sleep(6);
  }

  protected function selectOptionFromAutocomplete($field_name, $option) {
    $page = $this->getCurrentPage();
    $assert_session = $this->assertSession();
    $autocomplete_field = $assert_session->waitForElement('css', '[name="' . $field_name . '[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue($option);
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $assert_session->waitOnAutocomplete();
    $result = $page->find('xpath', "//a[text() = '{$option}']");
    $this->assertNotEmpty(
      $result,
      sprintf('Autocomplete option "%s" should be visible', $option)
    );
    $result->click();
  }

  protected function selectOptionFromSelect2EntityReferenceWidget($field_name, $text, $search = TRUE) {
    $formItemSelector = ".form-item--" . Html::cleanCssIdentifier($field_name);
    $formItem = $this->findFormElement($formItemSelector, "Form item");

    // Open the dropdown
    $selection = $formItem = $this->findFormElement("$formItemSelector .select2-selection", "Select2 selection", $formItem);
    $selection->click();
    sleep(1);
    try {
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
    catch (\RuntimeException $e) {}
    // It's possible the dropdown was already open and we just closed it, in which case reopen it.
    if (is_null($this->getSession()->getPage()->find('css', '.select2-results__options'))) {
      $selection->click();
      sleep(1);
    }

    // Sometimes searching just doesn't work.
    if ($search) {
      // Whether the search input is in the form item div or the select2 container
      // appended to the page depends on whether this is single value or multivalue.
      $search = $this->findFormElement('.select2-search__field', "Search box", $formItem);
      $search = $search ?? $this->findFormElement('.select2-search__field', "Search box");
      // This test is broken for multivalue ER fields that do not load all values initially.
      $search->setValue($text);
      sleep(3);
    }
    $dropdown = $this->findFormElement('.select2-results__options', 'Select2 results dropdown');
    $xpath = '//li[contains(@class, "select2-results__option") and text()="' . $text . '"]';
    $this->assertSession()->waitForElement('xpath', $xpath);
    sleep(3);
    $option = $this->findFormElement($xpath, "$text option", $dropdown, 'xpath');
    $option->click();
  }

  protected function findFormElement($selector, $description = NULL, $container = NULL, $selectorType = 'css') {
    if (!$container) {
      $container = $this->getSession()->getPage();
    }
    $element = $container->find($selectorType, $selector);
    $message = ($description ?? "Element") . " should be found";
    if (is_null($element)) {
      $this->debugPage($message);
    }
    $this->assertNotNull($element, "$message using '$selector' $selectorType");
    return $element;
  }

  protected function emptySelect2EntityReferenceWidget($field_name) {
    $formItemSelector = ".form-item--" . Html::cleanCssIdentifier($field_name);
    $formItem = $this->findFormElement($formItemSelector, "Form item");
    $removes = $formItem->findAll('css', '.select2-selection__choice__remove');
    foreach ($removes as $remove) {
      $remove->click();
    }
  }

  protected function openDetailsElement($selector) {
    $page = $this->getSession()->getPage();
    $details = $page->find('css', "$selector summary");
    $this->assertNotNull($details, "There should be a details field group with the selector '$selector'.");
    $details->click();
  }

  protected function setDateTimeFieldFromString($dateTimeString, $fieldName) {
    // Append seconds.
    if (substr_count($dateTimeString, ':') == 2) {
      $dateTimeString .= ':00';
    }
    $date = new DrupalDateTime($dateTimeString, timezone_open(date_default_timezone_get()));
    $this->setDateTimeField($date, $fieldName);
    // Update the timezone to the system default.
    //$date->setTimezone(timezone_open(date_default_timezone_get()));
  }

  protected function setDateTimeField(DateTimePlus $date, $fieldName) {
    $date_format = DateFormat::load('html_date')->getPattern();
    $time_format = DateFormat::load('html_time')->getPattern();
    $field = $this->assertSession()->fieldExists($fieldName . "[date]");
    $field->setValue($date->format($date_format));
    $field = $this->assertSession()->fieldExists($fieldName . "[time]");
    $field->setValue($date->format($time_format));
  }

  /**
   * Get the wysiwyg instance variable to use in Javascript.
   *
   * @param string
   *   The instanceId used by the WYSIWYG module to identify the instance.
   *
   * @throws Exception
   *   Throws an exception if the editor does not exist.
   *
   * @return string
   *   A Javascript expression representing the WYSIWYG instance.
   */
  protected function getWysiwygInstance($instanceId) {
    $instance = "Drupal.CKEditor5Instances.get('$instanceId')";
    if (!$this->getSession()->evaluateScript("return !!$instance")) {
      throw new \Exception(sprintf('The editor "%s" was not found on the page %s', $instanceId, $this->getSession()->getCurrentUrl()));
    }
    return $instance;
  }

  protected function fillWysiwygEditor($locator, $text) {
    if (!$this instanceof ExistingSiteJsBase) {
      $this->getSession()->getPage()->fillField($locator, $text);
      return;
    }

    $field = $this->getSession()->getPage()->findField($locator);
    if (null === $field) {
      throw new ElementNotFoundException($this->getDriver(), 'form field', 'id|name|label|value|placeholder', $locator);
    }
    $id = $field->getAttribute('data-ckeditor5-id');
    $instance = $this->getWysiwygInstance($id);
    $this->getSession()->executeScript("$instance.setData(\"$text\");");
  }

}
