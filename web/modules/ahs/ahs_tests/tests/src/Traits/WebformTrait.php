<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Render\FormattableMarkup;
use PHPUnit\Framework\Error\Warning;
/**
 * Helper methods for managing webforms in tests.
 */
trait WebformTrait
{

  protected $webformSettings = [];
  protected $webformHandlerSettings = [];

  protected function tearDownWebformSettings() {
    foreach ($this->webFormSettings as $id =>$settings) {
      $webform = $this->entityTypeManager->getStorage('webform')->load($id);
      foreach ($settings as $name => $value) {
        $webform->setSetting($name, $value);
      }
      $webform->save();
    }
  }

  protected function setUpWebformAtUrl(&$webform, $url) {
    $webform->setSetting('page', TRUE);
    $url = $url[0] === '/' ? $url : '/' . $url;
    $webform->setSetting('page_submit_path', $url);
    $webform->save();
  }
}
