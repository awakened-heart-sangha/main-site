<?php

namespace Drupal\Tests\ahs_tests\Traits\Definitions;

// Depends on UserTrait.
trait CommerceDefinitions
{

  /**
   * @Then I should be required to login
   */
  public function iShouldBeRequiredToLogin() {
    $this->assertSession()->pageTextContains('Login');
    // Test that cannot proceed without logging in with correct password
    $this->submitForm([
      'ahs_login[returning_customer][name]' => $this->getMe()->getEmail(),
      'ahs_login[returning_customer][password]' => 'invalid password',
    ], 'Log in');
    sleep(1);
    $this->assertSession()->pageTextContains('Login');
    $this->assertSession()->pageTextNotContains('Payment information');
  }

  /**
   * @Then I should not be required to login
   */
  public function iShouldNotBeRequiredToLogin() {
    $this->assertSession()->pageTextNotContains('Login');
    $this->assertSession()->pageTextContains('Payment information');
  }

  /**
   * @When I login at checkout
   */
  public function iLoginAtCheckout() {
    $this->submitForm([
      'ahs_login[returning_customer][name]' => $this->getMe()->getAccountName(),
      'ahs_login[returning_customer][password]' => $this->getMe()->passRaw,
    ], 'Log in');
    $this->assertSession()->pageTextNotContains('Unrecognized username or password');
  }

  /**
   * @Then the order should be in the :state state
   */
  public function theOrderShouldBeInTheState($state) {
    $order = $this->loadEntity('commerce_order', $this->orderId);
    $this->assertSame($state, $order->getState()->getId(), "The order should be in the specified state.");
  }

  /**
   * @Then I should be on the payment information step of the checkout
   */
  public function iShouldBeOnThePaymentInformationStepOfTheCheckout() {
    $this->assertSession()->pageTextContains('Payment information');
    if (!empty($this->orderId)) {
      $order = $this->loadEntity('commerce_order', $this->orderId);
      $this->assertSame('draft', $order->getState()->getId(), "The order should be in a draft state.");
    }
  }


  /**
   * @Then I should not see a billing profile form
   */
  public function iShouldNotSeeABillingProfileForm() {
    $this->assertSession()->pageTextNotContains('First name');
    $this->assertSession()->pageTextNotContains('Last name');
    $this->assertSession()->pageTextNotContains('Post town');
    $this->assertSession()->pageTextNotContains('Postal code');
  }

  /**
   * @Then a new user should have been created for me
   */
  public function aNewUserShouldHaveBeenCreatedForMe() {
    $user = $this->loadEntityByProperties('user', ['mail' => $this->getMyEmail()]);
    $this->assertNotEmpty($user, "A user with the specified email should exist.");
    $this->setMe($user);
  }

  /**
   * @When I submit my credit card details
   */
  public function iSubmitMyCreditCardDetails() {
    $this->submitCreditCardDetails('Donate now');
  }

  /**
   * @When I submit my accepted credit card details that require 3DS
   */
  public function iSubmitMyAcceptedCreditCardDetailsThatRequire3DS() {
    $this->submitCreditCardDetails('Donate now', self::$CARD_BASIC_ACCEPTED_3DS_REQUIRED);
  }

  /**
   * @When I submit my declined credit card details that require 3DS
   */
  public function iSubmitMyDeclinedCreditCardDetailsThatRequire3DS() {
    $this->submitCreditCardDetails('Donate now', self::$CARD_BASIC_DECLINED_3DS_REQUIRED);
  }

  /**
   * @Then the name is updated on my Stripe customer
   * @Then the email is updated on my Stripe customer
   * @Then my customer details are set on Stripe
   */
  public function myStripeCustomerIsUpdated() {
    $this->assertStripeCustomer();
  }

  /**
   * @Then I should be on the Thankyou step of the checkout
   */
  public function iShouldBeOnTheThankyouStep() {
    // The pane message should be customised.
    $this->assertSession()->elementContains('css', ".checkout-complete", 'Thankyou for your contribution to the Awakened Heart Sangha');
  }

}
