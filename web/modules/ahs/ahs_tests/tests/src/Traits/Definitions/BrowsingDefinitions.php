<?php

namespace Drupal\Tests\ahs_tests\Traits\Definitions;

trait BrowsingDefinitions
{

  /**
   * @When I go to :uri
   * @When I go to :uri again
   */
  public function iGoTo($uri) {
    // <front> is a valid uri here.
    $start = substr($uri, 0, 1);
    if (!in_array($start, ['/', '<'])) {
      $uri = "/" . $uri;
    }
    $this->drupalGet($uri);
  }

  /**
   * @When I visit :uri
   * @When I visit :uri again
   */
  public function iVisit($uri) {
    // <front> is a valid uri here.
    $start = substr($uri, 0, 1);
    if (!in_array($start, ['/', '<'])) {
      $uri = "/" . $uri;
    }
    $this->drupalGet($uri);
    $uri = $uri === '<front>' ? '/' : $uri;
    $this->assertSession()->addressEquals($uri);
    //$this->assertSession()->statusCodeEquals(200);
  }

  /**
   * @Then I am visiting :uri
   */
  public function iAmVisiting($uri) {
    $this->assertSession()->addressEquals($uri);
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * @Then I am at :uri
   */
  public function iAmAt($uri) {
    $this->assertSession()->addressEquals($uri);
  }

  /**
   * @Then I should see :text
   */
  public function iShouldSee($text) {
    $this->assertSession()->pageTextContains($text);
  }

  /**
   * @Then I should not see :text
   */
  public function iShouldNotSee($text) {
    $this->assertSession()->pageTextNotContains($text);
  }

  /**
   * @Then I should see :text in the :region region
   */
  public function iShouldSeeInTheRegion($text, $region) {
    $this->assertSession()->elementTextContains('css', ".region-$region", $text);
  }

  /**
   * @Then I should not see :text in the :region region
   */
  public function iShouldNotSeeInTheRegion($text, $region) {
    $this->assertSession()->elementTextNotContains('css', ".region-$region", $text);
  }

  /**
   * @Then I should see the page title block
   */
  public function iShouldSeeThePageTitleBlock() {
    $this->assertSession()->elementExists('css', 'h1.page-header');
  }

  /**
   * @Then I should not see the page title block
   */
  public function iShouldNotSeeTheBlock() {
    $this->assertSession()->elementNotExists('css', 'h1.page-header');
  }

  /**
   * @When I select :selected
   */
  public function iSelect($selected) {
    $page = $this->getSession()->getPage();
    $target = $page->find('xpath', "//a[text() = '{$selected}']");
    $target->click();
  }

  /**
   * @When I press :button
   */
  public function iPress($button) {
    $page = $this->getCurrentPage();
    $buttonElement = $page->find('named', ['button', $button]);
    if ($buttonElement) {
      $buttonElement->press();
      return;
    }
    $buttonElement = $page->find('named', ['link_or_button', $button]);
    $this->assertNotNull($buttonElement, "A '$button' button should be on the page.");
    $buttonElement->press();
  }

  /**
   * @When I click :button
   */
  public function iClick($link) {
    $page = $this->getCurrentPage();
    $element = $page->find('named', ['link_or_button', $link]);
    $this->assertNotNull($element, "A '$link' link should be on the page.");
    $element->press();
  }

  /**
   * @Then I should see a button labelled :button
   */
  public function iShouldSeeAButtonLabelled($button) {
    $page = $this->getCurrentPage();
    $buttonElement = $page->find('named', ['button', $button]);
    $this->assertNotNull($buttonElement, "A '$button' button should be on the page.");
  }

  /**
   * @Then I should not see a button labelled :button
   */
  public function iShouldNotSeeAButtonLabelled($button) {
    $page = $this->getCurrentPage();
    $buttonElement = $page->find('named', ['button', $button]);
    $this->assertNull($buttonElement, "A '$button' button should be on the page.");
  }

  /**
   * @When I check :checkbox
   */
  public function icheck($checkbox) {
    $page = $this->getCurrentPage();
    $checkboxElement = $page->findField($checkbox);
    $this->assertNotNull($checkboxElement, "A '$checkbox' form field should be on the page.");
    $checkboxElement->check();
  }

  /**
   * @When I uncheck :checkbox
   */
  public function iUncheck($checkbox) {
    $page = $this->getCurrentPage();
    $checkboxElement = $page->findField($checkbox);
    $this->assertNotNull($checkboxElement, "A '$checkbox' form field should be on the page.");
    $checkboxElement->uncheck();
  }

  /**
   * @When I select :option from the :radios radios
   */
  public function iSelectOptionFromTheRadios($option, $radios) {
    $this->selectRadio($radios, $option);
  }

  /**
   * @When I select :option from the :select select
   */
  public function iSelectOptionFromTheSelect($option, $select) {
    $page = $this->getCurrentPage();
    $page->selectFieldOption($select, $option);
  }

  /**
   * @Then the page header contains :header
   */
  public function thePageHeaderContains($header) {
    $this->assertSession()->elementTextContains('css', 'h1', $header);
  }

  /**
   * @Then the page header does not contain :header
   */
  public function thePageHeaderDoesNotContain($header) {
    if ($this->getSession()->getPage()->find('css', 'h1')) {
      $this->assertSession()->elementTextNotContains('css', 'h1', $header);
    }
  }

  /**
   * @When I wait in order to not trigger honeypot
   */
  public function iWaitInOrderToNotTriggerHoneypot() {
    $this->waitToPassHoneypot();
  }

}
