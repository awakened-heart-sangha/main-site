<?php

namespace Drupal\Tests\ahs_tests\Traits\Definitions;

use Behat\Gherkin\Node\TableNode;

/**
 * Trait for enabling and disabling mail collection during tests.
 *
 * Usage:
 *   From ::setUp call $this->startMailCollection()
 *   In ::tearDown call $this->restoreMailSettings()
 *
 * @property \Symfony\Component\DependencyInjection\ContainerInterface $container
 */
trait MailDefinitions
{

  /**
   * Check all mail sent during the scenario.
   *
   * @Then only this/these (e)mail(s) has/have been sent:
   * @Then only this/these (e)mail(s) has/have been sent to :to:
   * @Then only this/these (e)mail(s) has/have been sent with the subject :subject:
   * @Then only this/these (e)mail(s) has/have been sent to :to with the subject :subject:
   */
  public function onlyThisMailHasBeenSent(TableNode $expectedMailTable, $to = '', $subject = '')
  {
    $expectedMail = $expectedMailTable->getHash();
    $actualMail = $this->getMail(['to' => $to, 'subject' => $subject], false);
    $this->compareMail($actualMail, $expectedMail);
  }

  /**
   * Check all mail sent during the scenario.
   *
   * @Then (a )(an )(this )(these )(e)mail(s) has/have been sent:
   * @Then (a )(an )(this )(these )(e)mail(s) has/have been sent to :to:
   * @Then (a )(an )(this )(these )(e)mail(s) has/have been sent with the subject :subject:
   * @Then (a )(an )(this )(these )(e)mail(s) has/have been sent to :to with the subject :subject:
   */
  public function mailHasBeenSent(TableNode $expectedMailTable, $to = '', $subject = '')
  {
    $expectedMail = $expectedMailTable->getHash();
    $actualMail = $this->getMail(['to' => $to, 'subject' => $subject], false);
    $this->compareMail($actualMail, $expectedMail, TRUE);
  }

  /**
   * Check all mail sent during the scenario.
   *
   * @Then :count (e)mail(s) has/have been sent
   * @Then :count (e)mail(s) has/have been sent to :to
   * @Then :count (e)mail(s) has/have been sent with the subject :subject
   * @Then :count (e)mail(s) has/have been sent to :to with the subject :subject
   */
  public function numberMailHasBeenSent($count, $to = '', $subject = '')
  {
    $actualMail = $this->getMail(['to' => $to, 'subject' => $subject], false);
    $count = $count === 'no' ? 0 : $count;
    $count = $count === 'a' ? null : $count;
    $count = $count === 'an' ? null : $count;
    $this->assertMailCount($actualMail, $count);
  }

  /**
   * @When I follow the link to :urlFragment from the (e)mail
   * @When I follow the link to :urlFragment from the (e)mail to :to
   * @When I follow the link to :urlFragment from the (e)mail with the subject :subject
   * @When I follow the link to :urlFragment from the (e)mail to :to with the subject :subject
   */
  public function followLinkInMail($urlFragment, $to = '', $subject = '')
  {
    // Get the mail
    $matches = ['to' => $to, 'subject' => $subject];
    $mail = $this->getMail($matches, false, -1);
    $this->assertNotEmpty($mail, 'Mail should be present.');

    // Find web URLs in the mail
    $body = $mail['body'];
    $urlPattern = '`.*?((http|https)://[\w#$&+,\/:;=?@.-]+)[^\w#$&+,\/:;=?@.-]*?`i';
    $matched = preg_match_all($urlPattern, $body, $urls);
    $this->assertNotEmpty($matched, 'A url should be present in the mail body.');
    // Visit the first url that contains the desired fragment.
    foreach ($urls[1] as $url) {
      $match = (strpos(strtolower($url), strtolower($urlFragment)) !== false);
      if ($match) {
        $this->drupalGet($url);
        return;
      }
    }
    $this->assertTrue(FALSE, "A url matching the provided fragment should have been found");
  }

  /**
   * This is mainly useful for testing this context.
   *
   * @When Drupal sends a/an (e)mail:
   */
  public function drupalSendsMail(TableNode $fields)
  {
    $mail = [
      'body' => $this->randomMachineName(),
      'subject' => $this->randomMachineName(),
      'to' => $this->randomMachineName() . '@anonexample.com',
      'langcode' => '',
    ];
    foreach ($fields->getRowsHash() as $field => $value) {
      $mail[$field] = $value;
    }
    // Send the mail, via the system module's hook_mail.
    $params['context']['message'] = $mail['body'];
    $params['context']['subject'] = $mail['subject'];
    $mailManager = \Drupal::service('plugin.manager.mail');
    $result = $mailManager->mail('system', '', $mail['to'], $mail['langcode'], $params, NULL, TRUE);
  }

}
