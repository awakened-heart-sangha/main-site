<?php

namespace Drupal\Tests\ahs_tests\Traits\Definitions;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Definitions for working with group entities.
 *
 * Depends on GroupTrait & UserTrait.
 *
 * @package Drupal\Tests\ahs_tests\Traits\Definitions
 */
trait GroupDefinitions {

  /**
   * @Given a/an :bundle group
   */
  public function aBundleGroup($bundle) {
    $this->group  = $this->createGroup(['type' => $bundle]);
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given a (future )suggested :bundle( group)
   */
  public function aSuggestedGroup($bundle) {
    $future = $bundle === 'course_template' ? FALSE : TRUE;
    $this->group  = $this->createGroup(['type' => $bundle, 'field_suggested' => TRUE], $future);
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given a future :bundle
   */
  public function aFutureGroup($bundle) {
    $this->group  = $this->createGroup(['type' => $bundle], TRUE);
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given an unpublished future suggested :bundle
   */
  public function anUnpublishedFutureSuggestedGroup($bundle) {
    $this->group  = $this->createGroup(['type' => $bundle, 'field_suggested' => TRUE, 'status' => FALSE], TRUE);
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given a/an :bundle that starts on :date
   */
  public function aGroupThatStartsOn($bundle, $date) {
    $this->group = $this->createGroup(['type' => $bundle]);
    $dateObject = new DrupalDateTime($date);
    $interval = new \DateInterval('P1D');
    $this->group->set('field_dates', [
      'value' => $dateObject->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $dateObject->add($interval)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    ]);
    $this->group->save();
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given a/an :bundle group called :title
   * @Given a/an :bundle that starts in the future
   */
  public function aGroupCalledTitle($bundle, $title = NULL) {
    $this->group  = $this->createGroup(['type' => $bundle, 'label' => $title], TRUE);
    $this->{$bundle} = $this->group;
  }

  /**
   * @When I view the group
   * @When I view the :bundle group
   */
  public function iViewTheGroup($bundle = NULL) {
    $group = $bundle ? $this->{$bundle} : $this->group;
    $this->drupalGet("/group/" . $this->group->id());
  }

  /**
   * @When I manage the group
   * @When I edit the group
   */
  public function iManageTheGroup() {
    $this->drupalGet("/group/" . $this->group->id() . "/manage");
  }

  /**
   * @Given I am a member of the :bundle
   * @Given I am a participant in the :bundle
   * @Given I am a member of the :bundle with the role :role
   */
  public function iAmAMemberOfTheGroup($bundle, $role = NULL) {
    $membership_fields = ['field_participant_type' => '69']; // Student

    if (!empty($role)) {
      $group_role = $this->{$bundle}->bundle() . "-" . $role;
      $membership_fields['group_roles'] = [$group_role];
    }

    $this->{$bundle}->addMember($this->getMe(), $membership_fields);

    if (!empty($role)) {
      $membership = $this->{$bundle}->getMember($this->getMe());
      $roles = $membership->getRoles();
      $this->assertArrayHasKey($group_role, $roles, "The user should have the '" . $role . "' in the group");
    }
  }

  /**
   * @Given I am a mentor of the :bundle
   * @When I become a mentor of the :bundle
   */
  public function iAmAMentorOfTheGroup($bundle) {
    $membership_fields = ['field_participant_type' => '70']; // Mentor
    $this->{$bundle}->addMember($this->getMe(), $membership_fields);
    $membership = $this->{$bundle}->getMember($this->getMe())->getGroupRelationship();
    $membership->group_roles[] = "$bundle-mentor";
    $membership->save();
    $this->assertGroupMember($this->getMe(), $this->{$bundle}, \Drupal\ahs_training\Entity\TrainingRecord::MENTOR_TERM_ID, ['mentor']);
  }


  /**
   * @Given a/an :published :public paragraph on the group
   */
  public function aPublishedPublicParagraphOnTheGroup($published, $public) {
    $isPublished = $published === 'published';
    $isPublic = $public === 'public';
    $this->createGroupParagraph('layout_section', $isPublished, $isPublic);
  }

  /**
   * @Given the :bundle uses normal registration
   */
  public function theGroupUsesNormalRegistration($bundle) {
    $this->{$bundle}->set('field_registration_normal', TRUE)->save();
  }

  /**
   * @Given the :bundle is open
   */
  public function theGroupisOpen($bundle) {
    $this->{$bundle}->set('field_registration_open', TRUE)->save();
    $this->spreadRegistrationSetting($this->group, 'status', TRUE);
  }

  /**
   * @Given the :bundle is not open
   */
  public function theGroupisNotOpen($bundle) {
    $this->{$bundle}->set('field_registration_open', FALSE)->save();
    $this->spreadRegistrationSetting($this->group, 'status', FALSE);
  }

  /**
   * @Given the :bundle has a leader
   */
  public function theGroupHasALeader($bundle) {
    $user = $this->createDecoupledUser();
    $this->{$bundle}->set('field_leaders', $user)->save();
  }

  /**
   * @When I visit the :bundle group :path page
   */
  public function iVisitTheGroupPage($path, $bundle = NULL) {
    $group = $bundle ? $this->{$bundle} : $this->group;
    $this->drupalGet('/group/' . $group->id() . $path);
  }

  /**
   * @Given the :bundle has not ended
   */
  public function theGroupHasNotEnded($bundle) {
    $now = new DrupalDateTime();
    $interval = new \DateInterval('P1D');
    $startDate = $now->add($interval);
    $endDate = $startDate->add($interval);
    $this->{$bundle}->set('field_dates', [
      'value' => $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $endDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    ]);
    $this->{$bundle}->save();
  }

  /**
   * @Given the :bundle has ended
   */
  public function theGroupHasEnded($bundle) {
    $startDate = new DrupalDateTime('-30 days');
    $interval = new \DateInterval('P1D');
    $endDate = $startDate->add($interval);
    $this->{$bundle}->set('field_dates', [
      'value' => $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $endDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    ]);
    $this->{$bundle}->save();
  }

  /**
   * @Given the :bundle has no end date
   */
  public function theGroupHasNoEndDate($bundle) {
    $now = new DrupalDateTime();
    $this->{$bundle}->set('field_dates', [
      'value' => $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => NULL,
    ]);
    $this->{$bundle}->save();
    $perm = $this->{$bundle}->hasPermission('join group', $this->getMe());
  }

  /**
   * @Then I should not see the :bundle group
   */
  public function iShouldNotSeeTheGroup($bundle) {
    $this->assertSession()->pageTextNotContains($this->{$bundle}->label());
  }

  /**
   * @Given the :bundle registration deadline has not passed
   */
  public function theRegistrationDeadlineHasNotPassed($bundle) {
    $deadline = new DrupalDateTime('+1 day');
    $value = $deadline->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $this->setRegistrationDeadline($this->{$bundle}, $value);
  }

  /**
   * @Given the :bundle registration deadline has passed
   */
  public function theRegistrationDeadlineHasPassed($bundle) {
    $deadline = new DrupalDateTime('-1 day');
    $value = $deadline->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $this->setRegistrationDeadline($this->{$bundle}, $value);
  }

}
