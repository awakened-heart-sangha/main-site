<?php

namespace Drupal\Tests\ahs_tests\Traits\Definitions;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\simplenews\Entity\Subscriber;

trait NewsDefinitions {

  /**
   * @Given :name has subscribed to the :newsletter_id newsletter
   * @Given I have subscribed to the :newsletter_id newsletter
   */
  public function userIsSubscribedToTheNewsletter($newsletter_id, $name = 'me') {
    $user = $this->getOrCreateNamedUser($name);
    $this->subscribeUser($user, $newsletter_id);
  }

  /**
   * @Then I am subscribed to the :newsletter_id newsletter
   * @Then :name is subscribed to the :newsletter_id newsletter
   */
  public function iAmSubscribedToTheNewsletter($newsletter_id, $name = 'me') {
    $user = $this->getOrCreateNamedUser($name);
    $this->assertSubscribed($user, $newsletter_id, TRUE);
  }

  /**
   * @Given I have not subscribed to the :newsletter_id newsletter
   * @Given :name has not subscribed to the :newsletter_id newsletter
   * @Then I am not subscribed to the :newsletter_id newsletter
   * @Then :name is not subscribed to the :newsletter_id newsletter
   */
  public function iAmNotSubscribedToTheNewsletter($newsletter_id, $name = 'me') {
    $user = $this->getOrCreateNamedUser($name);
    $this->assertSubscribed($user, $newsletter_id, FALSE);
  }

  /**
   * @Given there is an item of other news
   */
  public function thereIsAnItemOfOtherNews() {
    $node = Node::create([
      'type' => 'news',
      'title' => $this->randomMachineName(),
      'field_subtitle' => $this->randomMachineName(),
    ]);
    $node->setPublished();
    $node->save();
    $this->markEntityForCleanup($node);
    $this->otherNews = [$node];
  }

  /**
   * @Given a draft news issue
   */
  public function ADraftNewsIssue() {
    $issue = $this->createDraftNewsIssue();
    $this->issue = $issue;
  }

  /**
   * @Given a news issue for the :newsletter_id newsletter
   */
  public function ANewsIssueForTheNewsletter($newsletter_id) {
    $node = $this->createDraftNewsIssue();
    $node->set('simplenews_issue', [
        'target_id' => 'default',
        'handler_settings' => ['newsletters' => [$newsletter_id => $newsletter_id]],
      ]
    );
    $node->setPublished();
    $node->save();
    $this->issue = $node;
  }

  /**
   * @Given there is a synced help wanted topic
   */
  public function thereIsASyncedHelpWantedTopic() {
    $node = Node::create([
      'type' => 'discourse_topic',
      'title' => $this->randomMachineName(),
      'field_discourse_excerpt' => $this->randomMachineName(),
    ]);
    $node->save();
    $this->markEntityForCleanup($node);
    $this->topic = $node;
  }

}
