<?php

namespace Drupal\Tests\ahs_tests\Traits\Definitions;

use Behat\Gherkin\Node\TableNode;
use Drupal\user\Entity\User;

trait UserDefinitions {

  /**
   * @Given I have the email :email
   * @Given my email is :email
   * @When I change my email to :email
   * @When I change my email
   */
  public function iChangeMyEmailTo($email = NULL) {
    if (empty($email)) {
     $email = $this->randomEmail();
    }
    $me = $this->getMe();
    $me->setEmail($email)->save();
  }

  /**
   * @Given I am not logged in
   */
  public function iAmNotLoggedIn() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      $this->drupalLogout();
    }

    user_logout();
    $this->drupalResetSession();
    if ($this->hasUser('me')) {
      $this->assertFalse($this->isUserLoggedIn($this->getMe()), 'User should not be logged in.');
    }
  }

  /**
   * @Given I am logged in
   */
  public function iAmLoggedIn() {
    $this->coupleMe();
    if (!$this->isUserLoggedIn($this->getMe())) {
      $this->useOneTimeLoginLinks = FALSE;
      $this->drupalLogin($this->getMe());
    }
    $this->assertTrue($this->isUserLoggedIn($this->getMe()), 'User ' . $this->getMe()->getEmail() . ' is not logged in.');
  }

  /**
   * @When I log in
   */
  public function iLogin() {
      $this->iAmNotLoggedIn();
      $this->iAmLoggedIn();
  }

  /**
   * @Given I am( already) a user with a password
   */
  public function iAmAlreadyAUserWithAPassword() {
    $this->coupleMe();
  }

  /**
   * @Given I am( already) a user
   * @Given I am( already) a user( but) without a password
   */
  public function iAmAlreadyAUserButWithoutAPassword() {
    $this->setMe($this->createDecoupledUser());
  }

  /**
   * @Given I am not a user
   */
  public function iAmNotAUser() {
    $email = $this->getMyEmail();
    $user = $this->loadEntityByProperties('user', ['mail' => $email]);
    $this->assertEmpty($user);
    $this->removeNamedUser('me');
  }

  /**
   * @Given users:
   */
  public function users(TableNode $table) {
    foreach ($table as $row) {
      if (isset($row['decoupled']) && $row['decoupled'] === TRUE || !isset($row['username']) || ($row['username'] == NULL)) {
        $user = $this->createDecoupledUser([], $row['email'] ?? NULL);
      }
      else {
        $edit = [];
        if (isset($row['username']) && !empty($row['username'])) {
          $edit['name'] = $row['username'];
        }
        if (isset($row['email']) && !empty($row['email'])) {
          $edit['mail'] = $row['email'];
        }
        $user = $this->createUser([], $edit);
      }

      $fields = [];
      if (isset($row['firstname']) && !empty($row['firstname'])) {
        $fields['field_name'] = [
          'given' => $row['firstname'],
          'family' => $row['lastname']
        ];
      }
      if (isset($row['nickname']) && !empty($row['nickname'])) {
        $fields['field_name_nickname'] = $row['nickname'];
      }
      if (isset($row['sangha name']) && !empty($row['sangha name'])) {
        $fields['field_name_sangha'] = $row['sangha name'];
      }
      if (!empty($fields)) {
        $this->updateProfile($user, 'about', $fields);
      }

      if (isset($row['reference'])) {
        $this->users[$row['reference']]  = $user;
      }
    }
  }

  /**
   * @Given I am logged in as a/an :role
   */
  public function iAmLoggedInAsA($role) {
    $edit = [
      'mail' => $this->randomEmail(),
    ];
    if ($role) {
      $roleObject = $this->loadEntityByProperties('user_role', ['id' => $role]);
      if (empty($roleObject)) {
        $roleObject = $this->loadEntityByProperties('user_role', ['label' => $role]);
      }
      if (empty($roleObject)) {
        throw new \Exception("Role with id or label '$role' not found.");
      }
      $edit['roles'] = [$roleObject->id()];
    }
    $user = $this->createUser([], $edit);
    $this->setMe($user);
    $this->iLogin();
    $this->assertTrue($this->isUserLoggedIn($this->getMe()), 'User ' . $this->getMe()->getEmail() . ' is not logged in.');
  }


  /**
   * @Given I have the :role role
   * @When I become a/an :role
   */
  public function ihavetheRole($role)
  {
    $roleObject = $this->loadEntityByProperties('user_role', ['id' => $role]);
    if (empty($roleObject)) {
      $roleObject = $this->loadEntityByProperties('user_role', ['label' => $role]);
    }
    if (empty($roleObject)) {
      throw new \Exception("Role with id or label '$role' not found.");
    }
    $me = $this->getMe();
    $me->addRole($roleObject->id());
    $me->save();
  }

  /**
   * @When I log in as a different user
   * @When I log in as a different :role
   */
  public function iLoginAsADifferentUser($role = NULL) {
    // Make sure we log in as a new user not an existing one.
    $this->forgetMe();
    $this->iAmLoggedInAsA($role);
  }

  /**
   * @Given I am the anonymous user
   */
  public function iAmTheAnonymousUser() {
    $user = User::load(0);
    $this->assertNotEmpty($user);
    $this->setNamedUser($user, 'me');
  }

  /**
   * @Given I have an alternative email
   * @Given I have the alternative email :email
   * @Given my alternative email is :email
   */
  public function iHaveAnAlternativeEmail($email = TRUE) {
    $this->getMyAlternativeEmail($email);
  }

  /**
   * @Given I have the Sangha name :name
   * @When a Sangha name is given to me programatically
   */
  public function ihaveTheSanghaName($name = NULL) {
    $name = $name ?? $this->randomMachineName();
    $fields = ['field_name_sangha' => $name];
    $this->updateProfile($this->getMe(), 'about', $fields);
    $this->assertEquals($name, $this->getMe()->get("about_profiles")->entity->field_name_sangha->value);
  }

  /**
   * @When I add a Sangha name
   */
  public function iAddSanghaName($name = NULL) {
    $this->iAmLoggedIn();
    $uri = "/user/" . $this->getMe()->id() . "/edit";
    $this->drupalGet($uri);
    $page = $this->getCurrentPage();
    $name = $name ?? $this->randomMachineName();
    $page->fillField('Sangha name', $name);
    $page->pressButton('Save');
    // No idea why this reload is needed.
    $profile_id = $this->getMe()->get('about_profiles')->target_id;
    $this->entityTypeManager->getStorage('profile')->resetCache([$profile_id]);
    $profile = $this->entityTypeManager->getStorage('profile')->load($profile_id);
    $this->assertEquals($name, $this->getMe()->get("about_profiles")->entity->field_name_sangha->value);
  }

  /**
   * @When I logout
   */
  public function iLogout() {
    $this->drupalGet('/user/logout');
  }

  /**
   * @Given a user named :name
   * @Given a user named :name with email :email
   */
  public function aUserNamed($name, $email = NULL) {
    $fields = [];
    if ($email) {
      $fields['mail'] = $email;
    }
    $this->createNamedUser($name, $fields, FALSE);
  }

  /**
   * @Given a user with the :role role
   */
  public function aUserWithTheRole($role) {
    $user = $this->createNamedUser($role);
    $user->addRole($role);
    $user->save();
  }

}
