<?php

namespace Drupal\Tests\ahs_tests\Traits;


use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\UserInterface;

trait GroupTrait
{

  protected $group;

  /**
   * Creates a single group
   */
  protected function createGroup($fields, $future = FALSE) {
    if (!isset($fields['type']) || empty($fields['type'])) {
      throw new \Exception("createGroup method requires a value for the group type");
    }
    if (!isset($fields['label']) || empty($fields['label'])) {
      $fields['label'] = $fields['type'] . $this->randomMachineName();
    }
    if (!isset($fields['status'])) {
      $fields['status'] = TRUE;
    }

    $groupStorage = $this->entityTypeManager->getStorage('group');
    $group = $groupStorage->create($fields);

    if ($future && $group->hasField('field_dates') && !isset($fields['field_dates'])) {
      $now = new DrupalDateTime();
      $interval = new \DateInterval('P1D');
      $startDate = $now->add($interval);
      $endDate = $startDate->add($interval);
      $group->set('field_dates', [
        'value' => $startDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        'end_value' => $endDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      ]);
    }

    $group->enforceIsNew();
    $group->save();

    if ($group->getRegistrationSystem() === 'registration') {
      $this->spreadRegistrationSetting($group, 'status', $group->isRegistrationOpen());
      if (!$group->get('field_registration_deadline')->isEmpty()) {
        $this->spreadRegistrationSetting($group, 'close', $group->get('field_registration_deadline')->value);
      }
    }
    $group = $this->reloadEntity($group);

    $this->markEntityForCleanup($group);
    return $group;
  }

  /**
   * Creates some group members
   */
  protected function createSomeGroupMembers($group, $participant_type_tid, $number) {
    $users = [];
    $edit = [];
    $edit['roles'] = ['authenticated', 'member'];
    $edit['pass'] = \Drupal::service('password_generator')->generate();
    for ($x = 0; $x < $number; $x++) {
      $edit['name'] = $this->randomMachineName();
      $edit['mail'] = $this->randomEmail();
      $user = $this->createEntity('user', $edit);
      $membership = ['field_participant_type' => ['target_id' => $participant_type_tid]];
      $group->addMember($user, $membership);
      $users[] = $user;
    }
    return $users;
  }

  protected function setGroupDates($startDate, $endDate) {

  }


  protected function createGroupParagraph($type, $isPublished = NULL, $isPublic = NULL) {
    $this->paragraph = Paragraph::create([
      'type' => $type,
    ]);
    if (isset($isPublic)) {
      $this->paragraph->set('field_public', $isPublic);
    }
    $this->paragraph->setParentEntity($this->group, 'field_content');
    if (!is_null($isPublished)) {
      if ($isPublished) {
        $this->paragraph->setPublished();
      }
      else {
        $this->paragraph->setUnpublished();
      }
    }
    $this->paragraph->save();
    $this->group->set('field_content', [$this->paragraph])->save();
    return $this->paragraph;
  }

  protected function assertGroupMember(UserInterface $user, GroupInterface $group, $participantTypeId = NULL, $roles = []) {
    $membership = $group->getMember($user);
    $this->assertNotFalse($membership, "The user should be a group member.");
    if ($participantTypeId) {
      $this->assertTrue($membership->getGroupRelationship()->hasField('field_participant_type'), 'Group membership should have a participant type field');
      $this->assertEquals($participantTypeId, $membership->getGroupRelationship()->field_participant_type->target_id);
    }
    $actualRoles = array_column($membership->getGroupRelationship()->group_roles->getValue(), 'target_id');
    $type = $group->bundle();
    foreach ($roles as $role) {
      $this->assertContains("$type-$role", $actualRoles);
    }

  }

  protected function assertLeadersAreMentors(GroupInterface $group) {
    $leaders = $group->get('field_leaders')->referencedEntities();
    foreach($leaders as $leader) {
      $this->assertGroupMember($leader, $group, \Drupal\ahs_training\Entity\TrainingRecord::MENTOR_TERM_ID, ['mentor']);
    }
  }

  protected function setRegistrationDeadline(GroupInterface $group, $value) {
    if ($group->hasField('field_product')) {
      $this->spreadRegistrationSetting($group, 'close', $value);
    }
    $group->set('field_registration_deadline', ['value' => $value]);
    $group->save();
  }

  protected function spreadRegistrationSetting(AhsGroup $group, $setting, $value, $property = 'value') {
    $group = $this->reloadEntity($group);
    foreach($group->getRegistrationHostEntities() as $host) {
      $settings = $host->getSettings();
      $settings->set($setting, [$property => $value]);
      $settings->save();
    }
  }


}
