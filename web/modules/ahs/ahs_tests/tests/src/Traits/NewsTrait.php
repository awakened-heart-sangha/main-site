<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\Mail\MailEntity;
use Drupal\simplenews\SubscriberInterface;
use Drupal\user\UserInterface;

/**
 * A trait to help with testing simplenews email newsletters.
 */
trait NewsTrait {

  protected function getUserSubscriberEmail(UserInterface $user) {
    $subscriber = Subscriber::loadByUid($user->id());
    $this->assertInstanceOf(SubscriberInterface::class, $subscriber, "A subscriber should exist for user " . $user->getDebugLabel());
    $subscriber = $this->reloadEntity($subscriber);
    $this->assertInstanceOf(SubscriberInterface::class, $subscriber, "A subscriber should exist for user " . $user->getDebugLabel());
    $email = $subscriber->getMail();
    $this->assertNotEmpty($email, "The subscriber should have an email");
    return $email;
  }

  protected function assertSubscribed(UserInterface $user, $newsletterId, $isSubscribed) {
    $user = $this->reloadEntity($user);
    $subscriber = Subscriber::loadByUid($user->id());
    if ($isSubscribed) {
      $this->assertNotEmpty($subscriber, "There should be a subscriber for " . $user->getDebugLabel());
    }

    if ($subscriber) {
      // Purge subscriber from entity storage cache to help subscription manager.
      $this->reloadEntity($subscriber);
      $this->assertSubscriberEmail($subscriber, $user->getEmail());
    }

    $subscriptionManager = \Drupal::service('simplenews.subscription_manager');
    $subscriptionManager->reset();
    $result = $subscriptionManager->isSubscribed($user->getEmail(), $newsletterId);
    $this->assertSame($isSubscribed, $result);
  }

  protected function assertUserEmail(UserInterface $user, $email, $message = '') {
    $user = $this->reloadEntity($user);
    $this->assertSame($email, $user->getEmail(), "$message The user email should be $email for " . $user->getDebugLabel() . '.');
  }

  protected function assertSubscriberEmail(SubscriberInterface $subscriber, $email, $message = '') {
    $subscriber = $this->reloadEntity($subscriber);
    $this->assertSame($email, $subscriber->getMail(), "$message The subscriber email should be $email.");
  }

  protected function assertUserSubscriberEmail(UserInterface $user, $email, $message = '') {
    $subscriber = $this->assertUserSubscriber($user);
    $this->assertUserEmail($user, $email, $message);
    $this->assertSubscriberEmail($subscriber, $email, $message);
  }

  protected function assertUserSubscriber(UserInterface $user) {
    $subscriber = Subscriber::loadByUid($user->id());
    $this->assertNotEmpty($subscriber, "There should be a subscriber for " . $user->getDebugLabel());
    $this->assertSubscriberWithUser($subscriber);
    return $subscriber;
  }

  protected function assertSubscriberWithUser(SubscriberInterface $subscriber) {
    $this->markEntityForCleanup($subscriber);
    $subscriber = $this->reloadEntity($subscriber);
    $this->assertNotEmpty($subscriber->getUserId(), "A user id should be stored on the subscriber.");
    $user = $subscriber->getUser();
    $this->markEntityForCleanup($user);
    $this->assertNotEmpty($user, "A user should be loadable from the subscriber");
    $user = $this->reloadEntity($user);
    return $user;
  }

  protected function sendIssueToUser(NodeInterface $issue, UserInterface $user = NULL) {
    $subscriber = NULL;
    if ($user) {
      $subscriber = Subscriber::loadByUid($user->id);
    }
    if (!$subscriber) {
      $subscriber = Subscriber::create(['mail' => $this->randomEmail()]);
      if ($user) {
        $subscriber->set('uid', $user->id());
      }
      $subscriber->save();
    }
    $mail = new MailEntity($issue, $subscriber, \Drupal::service('simplenews.mail_cache'));
    $result = \Drupal::service('simplenews.mailer')->sendMail($mail);
    $this->assertNotEmpty($result, "The issue should be sent successfully.");
    $this->subscriber = $subscriber;
  }

  protected function sendIssue(NodeInterface $issue) {
    \Drupal::service('simplenews.spool_storage')->addIssue($issue);
    simplenews_cron();
  }

  protected function subscribeUser(UserInterface $user, $newsletter_id) {
    $subscriber = Subscriber::loadByUid($user->id(), TRUE);
    $subscriber->subscribe($newsletter_id);
    $subscriber->save();
    $this->assertSubscribed($user, $newsletter_id, TRUE);
  }

  protected function createDraftNewsIssue() {
    $linkedNodeTitle = $this->randomMachineName();
    $linkedNode = Node::create(['type' => 'information', 'title' => $linkedNodeTitle]);
    $linkedNode->save();
    $this->markEntityForCleanup($linkedNode);
    $site_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $this->ctaAbsoluteUrl = strtolower($site_url . $linkedNodeTitle);

    $node = Node::create(['type' => 'news']);
    $node->setTitle($this->randomMachineName());
    $node->set('body', $this->randomMachineName());
    $node->set('field_subtitle', $this->randomMachineName());
    $node->field_link = ['title' => $this->randomMachineName(), 'uri' => 'internal:' . $linkedNode->toUrl()->toString()];
    $node->set('field_authors', $this->getMe()->id());

    $node->save();
    $this->markEntityForCleanup($node);
    $this->assertFalse($node->isPublished(), "Newsletter issues should be unpublished by default initially.");
    return $node;
  }

  /*
   * Assert the inline styling is being added by Swiftmailer.
   */
  protected function assertInlineStylesPresent($html) {
    // We test for an arbitrary property we added to the style sheet for this purpose.
    $this->assertGreaterThan(0, stripos($html, "--testname: 'testvalue'"), "Email theme styles should be inlined by SymfonyMailer");
  }

}
