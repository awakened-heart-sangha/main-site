<?php

namespace Drupal\Tests\ahs_tests\Traits;

trait CronTrait {

  protected function runCronJobs() {
    \Drupal::service('cron')->run();
    \Drupal::database()->truncate('ultimate_cron_log')->execute();
  }

}
