<?php

namespace Drupal\Tests\ahs_tests\Traits;

use PHPUnit\Util\Test;

trait RetryTrait {
  /**
   * The number of the current attempt.
   *
   * @var int
   */
  protected $retryAttempt = 0;

  public function runBare(): void {
    $e = NULL;
    do {
      try {
        $this->retryAttempt++;
        parent::runBare();
        return;
      } catch (\Throwable $e) {
        // last one thrown below
      } catch (\Exception $e) {
        // last one thrown below
      }
    } while (!$this->isFinalAttempt());
    if ($e) {
      throw $e;
    }
  }

  /**
   * Whether the current test attempt is the last one that should be attempted.
   *
   * @return bool
   */
  protected function isFinalAttempt() {
    return $this->retryAttempt < $this->getNumberOfRetries();
  }

  /**
   * Get the total number of attempts to be made.
   *
   * @return int
   */
  private function getNumberOfRetries() {
    $numberOfRetries = 1;
    $annotations = Test::parseTestMethodAnnotations(
      static::class,
      $this->getName()
    );
    if (isset($annotations['method']['retry'][0])) {
      $numberOfRetries = $annotations['method']['retry'][0];
    }
    if (isset($annotations['class']['retry'][0])) {
      $numberOfRetries = $annotations['class']['retry'][0];
    }
    if (isset($this->retry) && (!empty($this->retry))) {
      $numberOfRetries = $this->retry;
    }

    if (FALSE == is_numeric($numberOfRetries)) {
      throw new \LogicException(sprintf('The $numberOfRetries must be a number but got "%s"', var_export($numberOfRetries, TRUE)));
    }
    $numberOfRetries = (int) $numberOfRetries;
    if ($numberOfRetries <= 0) {
      throw new \LogicException(sprintf('The $numberOfRetries must be a positive number greater than 0 but got "%s".', $numberOfRetries));
    }
    return $numberOfRetries;
  }
}
