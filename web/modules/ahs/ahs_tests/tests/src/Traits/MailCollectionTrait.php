<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Behat\Gherkin\Node\TableNode;
use Drupal\Core\Mail\MailFormatHelper;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\user\UserInterface;
use weitzman\DrupalTestTraits\Mail\MailCollectionTrait as DTTMailCollectionTrait;

/**
 * Trait for enabling and disabling mail collection during tests.
 *
 * Usage:
 *   From ::setUp call $this->startMailCollection()
 *   In ::tearDown call $this->restoreMailSettings()
 *
 * @property \Symfony\Component\DependencyInjection\ContainerInterface $container
 */
trait MailCollectionTrait
{

  use DTTMailCollectionTrait {
    startMailCollection as parentStartMailCollection;
  }

  /**
   * Extend weitzman\DrupalTestTraits\Mail::startMailCollection
   */
  protected function startMailCollection() {
    // Call extended method
    // weitzman\DrupalTestTraits\Mail::startMailCollection
    $this->parentStartMailCollection();

    // Empty any previously collected mail.
    $this->container->get('state')->set('system.test_mail_collector', []);
  }

  /**
   * Get collected mail, matching certain specifications.
   *
   * @param array $matches
   *   Associative array of mail fields and the values to filter by.
   * @param bool $new
   *   Whether to ignore previously seen mail.
   * @param null|int $index
   *   A particular mail to return, e.g. 0 for first or -1 for last.
   *
   * @return \stdClass[]
   *   An array of mail, each formatted as a Drupal 8
   * \Drupal\Core\Mail\MailInterface::mail $message array.
   */
  protected function getMail($matches = [], $new = false, $index = null)
  {
    $mail = $this->container->get('state')->get('system.test_mail_collector', []);
    //$mail = $this->getMailManager()->getMail($store);
    //$this->mailCount[$store] = count($mail);

    // Ignore previously seen mail.
    //if ($new) {
    //  $mail = array_slice($mail, $previousMailCount);
    //}

    $mail = $this->filterMail($mail, $matches);

    // Return an individual mail if specified by an index.
    if (is_null($index) || count($mail) === 0) {
      return $mail;
    } else {
      return array_slice($mail, $index, 1)[0];
    }
  }

  /**
   * Get collected mail, matching certain specifications.
   *
   * @param array $matches
   *   Associative array of mail fields and the values to filter by.
   * @param bool $new
   *   Whether to ignore previously seen mail.
   * @param null|int $index
   *   A particular mail to return, e.g. 0 for first or -1 for last.
   *
   * @return \stdClass[]
   *   An array of mail, each formatted as a Drupal 8
   * \Drupal\Core\Mail\MailInterface::mail $message array.
   */
  protected function getUserMails(UserInterface $user, $matches = [], $new = false, $index = null) {
    $this->assertNotEmpty($user->getEmail());
    $matches['to'] = $user->getEmail();
    return $this->getMail($matches, $new, $index);
  }

  /**
   * Filter mail by specifications.
   *
   * Keep only mail where each field mentioned in $matches contains the value
   * specified for that field.
   *
   * @param array $matches
   *   An array of mail, each formatted as a Drupal 8
   * \Drupal\Core\Mail\MailInterface::mail $message array.
   * @param array $matches
   *   Associative array of mail fields and the values to filter by.
   *
   * @return \stdClass[]
   *   An array of mail, each formatted as a Drupal 8
   * \Drupal\Core\Mail\MailInterface::mail $message array.
   */
  protected function filterMail(array $mail, array $matches = [], $new = false, $index = null)
  {
    return array_values(array_filter($mail, function ($singleMail) use ($matches) {
      return ($this->matchesMail($singleMail, $matches));
    }));
  }

  /**
   * Determine if a mail meets criteria.
   *
   * @param array $mail
   *   The mail, as an array of mail fields.
   * @param array $matches
   *   The criteria: an associative array of mail fields and desired values.
   * @param bool $negate
   *   How to match. If FALSE, then all criteria must pass. If THEN then no criteria must pass.
   *
   * @return bool
   *   Whether the mail matches the criteria.
   */
  protected function matchesMail($mail = [], $matches = [], $negate = FALSE)
  {
    // For each criteria, check the specified mail field contains the values.
    foreach ($matches as $field => $values) {
      if (!is_array($values)) {
        $values = [$values];
      }
      // Discard criteria that are just zero-length strings.
      $values = array_filter($values, static fn($value) => !empty($value));
      foreach ($values as $value) {
        // Case insensitive.
        if (stripos($mail[$field], (string) $value) === FALSE) {
          return $negate;
        }
      }
    }
    return !$negate;
  }

  /**
   * Assert there is the expected number of mails, or that there are some mails
   * if the exact number expected is not specified.
   *
   * @param array $actualMail
   *   An array of actual mail.
   * @param int $expectedCount
   *   Optional. The number of mails expected.
   */
  protected function assertMailCount($actualMail, $expectedCount = NULL, $min = FALSE) {
    $actualCount = count($actualMail);
    if (is_null($expectedCount)) {
      $this->assertNotEmpty($actualMail, sprintf("Expected mail not found. Emails were:\n\n%s",  print_r($this->prettyMails($this->getMail()), TRUE)));
    }
    else {
      if ($min) {
        $this->assertLessThanOrEqual($actualCount, $expectedCount, sprintf("Expected min %s matching emails, but %s found. Emails were:\n\n%s", $expectedCount, $actualCount, print_r($this->prettyMails($this->getMail()), true)));
      }
      else {
        $message = sprintf("Expected %s matching emails, but %s found.", $expectedCount, $actualCount);
        if ($actualCount < 5 && $actualCount > 0) {
          $message = sprintf("Expected %s matching emails, but %s found. Emails were:\n\n%s", $expectedCount, $actualCount, print_r($this->prettyMails($actualMail), true));
        }
        $this->assertEquals($expectedCount, $actualCount, $message);
      }
    }
  }

  // Prepare a simple list of mail.
  protected function prettyMails($mails = NULL, $trim = 50) {
    // For debugging convenience allow a simple call.
    if (!$mails) {
      $mails = $this->getMail();
    }
    $prettyMails = [];
    foreach ($mails as $mail) {
      $prettyMails[] = $this->prettifyMail($mail, $trim);
    }
    return print_r($prettyMails, TRUE);
  }

  protected function prettyMail($mail, $trim = 50) {
    return print_r($this->prettifyMail($mail, $trim), TRUE);
  }

  protected function prettifyMail(array $mail, $trim = 50) {
    $prettyMail = [];
    if (isset($mail['to'])) {
      $prettyMail['to'] = $mail['to'];
    }
    if (isset($mail['from'])) {
      $prettyMail['from'] = $mail['from'];
    }
    if (isset($mail['subject'])) {
      $prettyMail['subject'] = $this->prettifyText($mail['subject'], $trim);
    }
    if (isset($mail['body'])) {
      $prettyMail['body'] = $this->prettifyText($mail['body'], $trim);
    }
    if (isset($mail['plain'])) {
      $prettyMail['plain'] = $this->prettifyText($mail['plain'], $trim);
    }
    return $prettyMail;
  }

  protected function prettifyText($values, $trim) {
    if (!is_array($values)) {
      $values = [$values];
    }
    $text = implode(" ... ", $values);
    // Ignore HTML head.
    if (strstr($text, '<body')) {
      $text = strstr($text, '<body');
    }
    $plain = MailFormatHelper::htmlToText($text);
    $trimmed = mb_strimwidth($plain, 0, $trim, "...");
    return $trimmed;
  }

  /**
   * Compare actual mail with expected mail.
   *
   * @param array $actualMail
   *   An array of actual mail.
   * @param array $expectedMail
   *   An array of expected mail.
   */
  protected function compareMail($actualMail, $expectedMail, $all = FALSE)
  {
    $this->assertMailCount($actualMail, count($expectedMail), $all);

    // For each row of expected mail, check the corresponding actual mail.
    // Make the comparison insensitive to the order mails were sent.
    $actualMail = $this->sortMail($actualMail);
    $expectedMail = $this->sortMail($expectedMail);
    foreach ($expectedMail as $expectedIndex => $expectedMailItem) {
      $match = $this->matchInMails($actualMail, $expectedMailItem);
      $this->assertNotNull($match, sprintf("Mail matching criteria should be present in set.\n\nExpected (#%s):\n%s\n\nMail:\n%s", $expectedIndex, $this->prettyMail($expectedMailItem), $this->prettyMails($actualMail)));
      unset($actualMail[$match]);
    }
  }

  /**
   * Determine if a mail with specified criteria is present in  set of mail.
   *
   * @param array $mails
   *   The set of mail, each an array of mail fields.
   * @param array $matches
   *   The criteria: an associative array of mail fields and desired values.
   *
   * @return int|NULL
   *   The index of the mail that matches, or NULL if none.
   */
  protected function matchInMails($mails, $matches) {
    foreach ($mails as $index => $mail) {
      $match = $this->matchesMail($mail, $matches);
      if ($match) {
        return $index;
      }
    }
    return NULL;
  }

  /**
   * Sort mail by to, subject and body.
   *
   * @param array $mail
   *   An array of mail to sort.
   *
   * @return array
   *   The same mail, but sorted.
   */
  protected function sortMail($mail)
  {
    // Can't sort an empty array.
    if (count($mail) === 0) {
      return [];
    }

    // To, subject and body keys must be present.
    // Empty strings are ignored when matching so adding them is harmless.
    foreach ($mail as $key => $row) {
      if (!array_key_exists('to', $row)) {
        $mail[$key]['to'] = '';
      }
      if (!array_key_exists('subject', $row)) {
        $mail[$key]['subject'] = '';
      }
      if (!array_key_exists('body', $row)) {
        $mail[$key]['body'] = '';
      }
    }

    // Obtain a list of columns.
    foreach ($mail as $key => $row) {
      if (array_key_exists('to', $row)) {
        $to[$key] = $row['to'];
      }
      if (array_key_exists('subject', $row)) {
        $subject[$key] = $row['subject'];
      }
      if (array_key_exists('body', $row)) {
        $body[$key] = $row['body'];
      }
    }

    // Add $mail as the last parameter, to sort by the common key.
    array_multisort($to, SORT_ASC, $subject, SORT_ASC, $body, SORT_ASC, $mail);
    return $mail;
  }

  /**
   * Assert that a mail matches  set of properties.
   *
   * @param $mails
   *   A series of received mails.
   * @param $properties
   *   The properties the target mail should match.
   * @param $index
   *   The position of the target mail in the series.
   */
  protected function assertMatchesMail($mails, $properties, $index = 0, $message = NULL) {
    $message = $message ?? sprintf("'Mail should have the right subject & body':\n\nActual:\n%s\n\nExpected:\n%s", $this->prettyMails($mails, 2000), $this->prettyMail($properties, 300));
    $this->assertTrue($this->matchesMail($mails[$index], $properties), $message);
  }

  /**
   * Assert that a mail matches  set of properties.
   *
   * @param $mails
   *   A series of received mails.
   * @param $properties
   *   The properties the target mail should match.
   * @param $index
   *   The position of the target mail in the series.
   */
  protected function assertNotMatchesMail($mails, $properties, $index = 0) {
    $message = sprintf("'Mail subject & body contain unexpected values':\n\nActual:\n%s\n\nUnexpected:\n%s", $this->prettyMails($mails, 2000), $this->prettyMail($properties, 300));
    $this->assertTrue($this->matchesMail($mails[$index], $properties, TRUE), $message);
  }

  /**
   * Assert mail with specified criteria is present in  set of mail.
   *
   * @param array $mails
   *   The set of mail, each an array of mail fields.
   * @param array $matches
   *   The criteria: an associative array of mail fields and desired values.
   */
  protected function assertMatchInMails(array $mails, $matches, $message = '') {
    $message .= sprintf("Expected mail matching criteria in %s emails. Criteria:\n%s", count($mails), $this->prettyMail($matches, 300));
    if (count($mails) < 5) {
      $message .= sprintf("Emails were:\n\n%s", $this->prettyMails($mails, 2000));
    }
    $this->assertNotNull($this->matchInMails($mails, $matches), $message);
  }

  /**
   * Assert mail with specified criteria is not present in  set of mail.
   *
   * @param array $mails
   *   The set of mail, each an array of mail fields.
   * @param array $matches
   *   The criteria: an associative array of mail fields and desired values.
   */
  protected function assertNoMatchInMails(array $mails, $matches, $message = '') {
    $message .= sprintf("Expected no mail matching criteria in %s emails. Criteria:\n%s", count($mails), $this->prettyMail($matches, 300));
    if (count($mails) < 5) {
      $message .= sprintf("Emails were:\n\n%s", $this->prettyMails($mails, 2000));
    }
    $this->assertNull($this->matchInMails($mails, $matches), $message);
  }

  protected function assertUserMailWithProperties(UserInterface $user, $properties, $isTrue = TRUE, $message = '') {
    $mails = $this->getUserMails($user);
    if (empty($message)) {
      $message = $user->getEmail() . ': ';
    }
    $this->assertNotEmpty($mails, "There should be mail for " . $user->getEmail());
    if ($isTrue) {
      $this->assertMatchInMails($mails, $properties, $message);
    }
    else {
      $this->assertNoMatchInMails($mails, $properties, $message);
    }
  }

}
