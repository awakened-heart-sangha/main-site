<?php

namespace Drupal\Tests\ahs_tests\Traits;

/**
 * A trait for working with one time login links.
 */
trait OneTimeLoginTrait {

  protected $oneTimeLoginUrl;

  protected function assertEmailHasOneTimeLoginLink($email) {
    $email['body'] = strip_tags($email['body'], '<a>');

    preg_match_all('/\/user\/reset[^"]+/i', $email['body'], $matches);
    preg_match('/\/user\/reset\/[^\s"]+/',  $email['body'], $matches);
    $this->assertNotEmpty($matches[0], "The email should contain a one-time login link. " . $email['body']);
    $url = $matches[0];
    $this->assertNotEmpty($url);
    $maxLength = strlen('/user/reset/2303/1624902090/On3aTKllH4BABsTkRgnUA2yKd3ZWksqeXSyjULBo8vc') + 10;
    $this->assertTrue(strlen($url) < $maxLength, "The onetime login url looks too long: $url");
    $this->oneTimeLoginUrl = $url;
  }

  protected function assertPageHasOneTimeLoginLink($message) {
    preg_match('/\/user\/reset\/[^\s"]+/', $message, $matches);
    $this->assertNotEmpty($matches[0], "The page should contain a one-time login link. " . $message);
    $url = $matches[0];
    $this->assertNotEmpty($url);
    $maxLength = strlen('/user/reset/2303/1624902090/On3aTKllH4BABsTkRgnUA2yKd3ZWksqeXSyjULBo8vc') + 10;
    $this->assertTrue(strlen($url) < $maxLength, "The onetime login url looks too long: $url");
    $this->oneTimeLoginUrl = $url;
  }

  /**
   * @When I follow the one-time login link( in the email)
   */
  public function iFollowTheOneTimeLoginLink() {
    $this->drupalGet($this->oneTimeLoginUrl);
  }

  /**
   * @Then I can set my password and login
   */
  public function iCanSetMyPasswordAndLogin() {
    $this->waitToPassHoneyPot();
    $page = $this->getCurrentPage();
    $page->fillField('Password', 'mynewpassword');
    $page->fillField('Confirm password', 'mynewpassword');
    $page->pressButton("Log in");
    $this->assertSession()->pageTextContains("Registered");
    $this->assertSession()->linkNotExists("Log in");
  }

}
