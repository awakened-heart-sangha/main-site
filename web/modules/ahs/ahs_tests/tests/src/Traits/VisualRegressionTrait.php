<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Behat\Testwork\Tester\Result\TestResult;
use Behat\Behat\EventDispatcher\Event\AfterStepTested;
use Behat\Behat\EventDispatcher\Event\StepTested;
use Behat\Behat\EventDispatcher\ServiceContainer\EventDispatcherExtension;
use Drupal\Core\File\FileSystemInterface;

/**
 * Helper methods for managing visual regression screenshots.
 */
trait VisualRegressionTrait {

  /**
   * The directory to save screenshots in.
   *
   * @var string
   */
  protected $visualRegressionDirectory;

  /**
   * Whether or not to automatically take screenshots of all steps.
   *
   * @var bool
   */
  protected $shouldAutoScreenshot = FALSE;

  /**
   * Page elements to be rendered invisible using css.
   *
   * @var array
   */
  protected $inVisualRegressionHide = [];

  /**
   * Page elements to be not rendered using css.
   *
   * @var array
   */
  protected $inVisualRegressionDisplayNone = [];

  protected function setUpVisualRegression() {
    if (!empty(getenv('VISUAL_REGRESSION_WINDOW_WIDTH'))) {
      $this->appendWidthToScreenshotName = TRUE;
      $this->windowWidth = (int) getenv('VISUAL_REGRESSION_WINDOW_WIDTH');
    }

    if (!empty(getenv('VISUAL_REGRESSION_OUTPUT_SHORT_PATH'))) {
      $base['stem'] = "public://" . getenv('VISUAL_REGRESSION_OUTPUT_SHORT_PATH');
    }
    else {
      $base['stem'] = "public://visual-regression";
    }
    if (!empty(getenv('VISUAL_REGRESSION_CONTEXT'))) {
      $this->shouldAutoScreenshot = TRUE;
      $base['environment'] = getenv('VISUAL_REGRESSION_CONTEXT');
    }
    $base['type'] = 'captured';
    if (!empty(getenv('VISUAL_REGRESSION_DATABASE_TYPE'))) {
      $base['database_type'] = getenv('VISUAL_REGRESSION_DATABASE_TYPE');
    }
    $base['class'] = $className = (new \ReflectionClass($this))->getShortName();
    $base['test'] = $this->sanitiseFilePathComponent($this->getTestLabel(), 100);
    $this->visualRegressionDirectory = implode('/', $base) . "/";
    \Drupal::service('file_system')->prepareDirectory(
      $this->visualRegressionDirectory, 
      FileSystemInterface::CREATE_DIRECTORY
    );


    if ($this->shouldAutoScreenshot && method_exists($this, 'getBehatContainer')) {
      // Setup Behat to automatically screenshot every When and Then step.
      $dispatcher = $this->getBehatContainer()->get(EventDispatcherExtension::DISPATCHER_ID);
      $dispatcher->addListener(StepTested::AFTER, array($this, 'autoScreenshot'));
    }
  }

  protected function tearDownVisualRegression() {
    if ($this->shouldAutoScreenshot && method_exists($this, 'getBehatContainer')) {
      // Remove the auto screenshot event listener.
      $dispatcher = $this->getBehatContainer()->get(EventDispatcherExtension::DISPATCHER_ID);
      $dispatcher->removeListener(StepTested::AFTER, array($this, 'autoScreenshot'));
    }
  }

  /**
   * Take screenshots automatically after every successful When and Then step.
   *
   * @param AfterStepTested $event
   */
  public function autoScreenshot(AfterStepTested $event)
  {
    if ($event->getTestResult()->getResultCode() === TestResult::PASSED) {
      $stepType = $event->getStep()->getKeywordType();
      if ($stepType == 'When' || $stepType == 'Then') {
        $stepText = $event->getStep()->getText();
        $filename = $this->sanitiseFilePathComponent($stepText);
        try {
          $this->preparePageForVisualRegression();
          $this->screenshotPage($this->visualRegressionDirectory, $filename);
        }
        catch (\Exception $e) {
        }
      }
    }
  }

  protected function preparePageForVisualRegression() {
    if (!$this->hasSessionForScreenshot()) {
      throw new \Exception("Cannot take screenshot, no session available.");
    }

    // Hide specified elements.
    $elementsRules = "";
    foreach ($this->inVisualRegressionHide as $selector) {
      // Some Mink commands test whether a form element is visible before
      // manipulating it, so many more common ways of hiding an element
      // cannot be used.
      $elementsRules .= "rules.push('$selector {opacity: 0 !important; }');\n";
    }
    foreach ($this->inVisualRegressionDisplayNone as $selector) {
      $elementsRules .= "rules.push('$selector { display: none !important; }');\n";
    }

    //Hide the blinking cursor as it creates visual regression false positives
    $elementsRules .= "rules.push('input { caret-color: transparent; }');\n";
    //Hide css transitions and animations as they create visual regression false positives
    $elementsRules .= "      rules.push('* { transition-property: none !important; animation: none !important; transform: none !important; }');\n";

    $script = "
      var rules = [];
      {$elementsRules}
      var style = document.createElement('style');
      style.type = 'text/css';
      style.innerHTML = rules.join('\\n')
      document.getElementsByTagName('head')[0].appendChild(style);
     ";

    $this->getSession()->executeScript($script);
  }

}
