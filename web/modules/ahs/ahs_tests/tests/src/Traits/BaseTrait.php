<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Logger\RfcLoggerTrait;

/**
 * A trait shared by all ahs_tests base classes.
 */
trait BaseTrait
{
  use UserTrait;
  use RfcLoggerTrait;
  use UtilityTrait;
  use DebugTrait;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A list of generated identifiers.
   *
   * @var array
   */
  protected $generatedIds = array();

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  protected $additionalOwnedEntityTypes = [
    'commerce_order.uid',
    'commerce_subscription.uid',
    'training_record.uid'
  ];

  // An array of all entity types that implement EntityOwnerInterface.
  // This is statically cached as it does not vary between tests.
  static $ownedEntityTypes = [];

  protected $cleanUpLast = [
    'file',
    'media',
    'node',
    'user',
    'profile',
    'training_record',
    ];

  protected function setUpBase() {
    $this->setUpTime();
    $this->entityManager = $this->container->get('entity_type.manager');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->state = $this->container->get('state');
    $this->setUpRandomisation();
    $this->setUpDebugging();
    if (method_exists($this, 'startMailCollection')) {
      $this->startMailCollection();
    }
  }

  protected function tearDownBase() {
    // Ignore log errors during teardown.
    $this->failOnLogsLevel = 0;
    if (method_exists($this, 'restoreMailSettings')) {
      $this->restoreMailSettings();
    }
    $this->cleanUpEntities();
  }

  // Ensure that random values can be the same on different iterations of
  // the same test, so that visual regression tests don't have false positives.
  protected function setupRandomisation() {
    if (!empty(getenv('PHPUNIT_RANDOM_SEED_CONTROL'))) {
      $classHash = $this->numHash((new \ReflectionClass($this))->getName(), 8);
      $scenarioHash = $this->numHash($this->getTestLabel(), 8);
      $hash = $classHash + $scenarioHash;
      mt_srand($hash);
    }
  }

  protected function cleanUpEntities() {
    // Order entities to cleanup by entity type and id.
    // This both prevents duplicates and identifies users.
    $orderedCleanupEntities = [];
    foreach ($this->cleanupEntities as $entity) {
      // Reload entities to fix weird commerce order issues.
      $orderedCleanupEntities[$entity->getEntityTypeId()][$entity->id()] = $this->reloadEntity($entity);
    }
    // Unset the main list of entities to cleanup so that the parent
    // tearDown method will not try to delete them, we're handling that here.
    $this->cleanupEntities = [];

    // Mark for cleanup any entities owned by a user who is marked for cleanup.
    if (isset($orderedCleanupEntities['user'])) {
      $ownedEntities = $this->getOwnedEntitiesToCleanup(array_keys($orderedCleanupEntities['user']));
      $orderedCleanupEntities = array_replace_recursive($ownedEntities, $orderedCleanupEntities);
    }

    // Cleanup some entity types last.
    foreach($this->cleanUpLast as $lastType) {
      if (isset($orderedCleanupEntities[$lastType])) {
        $entities = $orderedCleanupEntities[$lastType];
        unset($orderedCleanupEntities[$lastType]);
        $orderedCleanupEntities[$lastType] = $entities;
      }
    }

    // Flatten ordered cleanup entities
    $cleanupEntities = [];
    array_walk_recursive($orderedCleanupEntities,function($entity) use (&$cleanupEntities){ $cleanupEntities[] = $entity; });
    foreach ($cleanupEntities as $entity) {
      // Already deleted entities (i.e. deleted as prt of test) will be null.
      if (!empty($entity)) {
        $this->cleanUpEntity($entity);
      }
    }
  }

  protected function cleanUpEntity($entity) {
    try {
      $entity->delete();
    }
    catch (\Throwable $e) {
      $message = "Error while cleaning up " . $entity->getEntityTypeId() . " " . $entity->id() . ": " . $e;
      \Drupal::logger('ahs_tests')
        ->notice($message);
    }
  }

  protected function getOwnedEntitiesToCleanup($uids) {
    $ownedEntityTypes = $this->getOwnedEntityTypes();
    $owned = [];
    foreach ($ownedEntityTypes as $ownedEntityType) {
      foreach($uids as $key => $uid) {
        $type = explode(".", $ownedEntityType)[0];
        $field = explode(".", $ownedEntityType)[1];
        try {
          $entities = \Drupal::entityTypeManager()
            ->getStorage($type)
            ->loadByProperties([$field => $uid]);
          foreach ($entities as $entity) {
            $owned[$type][$entity->id()] = $entity;
          }
        } catch (\Exception $e) {
          $message = "Exception while cleaning up type " . $type . " and field " . $field . " " . $e;
          \Drupal::logger('ahs_tests')
            ->notice($message);
        }
      }
    }
    return $owned;
  }

  protected function getOwnedEntityTypes() {
    if (empty(self::$ownedEntityTypes)) {
      $owned = $this->getEntityTypesImplementingOwnerInterface();
      $owned = array_unique(array_merge($owned, $this->additionalOwnedEntityTypes));
      self::$ownedEntityTypes = $owned;
    }
    return self::$ownedEntityTypes;
  }

  protected function getEntityTypesImplementingOwnerInterface() {
      $owned = [];
      $entityTypes = \Drupal::entityTypeManager()->getDefinitions();
      foreach ($entityTypes as $entityType) {
        $entity = $entityType->getClass();
        $interfaces = class_implements($entity);
        $isContent = in_array('Drupal\Core\Entity\ContentEntityInterface', $interfaces);
        if (!$isContent) {
          continue;
        }
        $field = $entityType->getKey('owner');
        $field = $field ? $field : $entityType->getKey('uid');
        if (!$field && $entityType instanceof FieldableEntityInterface) {
          $fields = \Drupal::service('entity_field.manager')->getBaseFieldDefinitions($entityType);
          $field = isset($fields['uid']);
        }
        if ($field) {
          $owned[] = $entityType->id() . '.' . $field;
        }
      }
      return $owned;
  }

  /**
   * Return a number only hash
   * https://stackoverflow.com/a/23679870/175071
   * @param $str
   * @param null $len
   * @return number
   */
  protected function numHash($str, $len=null)
  {
    $binhash = md5($str, true);
    $numhash = unpack('N2', $binhash);
    $hash = $numhash[1] . $numhash[2];
    if($len && is_int($len)) {
      $hash = substr($hash, 0, $len);
    }
    return $hash;
  }

  protected function setUpTime() {
    $realTime = $this->container->get('datetime.time');
    $testTime = new TestTime($realTime);
    $this->container->set('datetime.time', $testTime);
  }

  protected function releaseMemory() {
    // See https://stackoverflow.com/questions/36032168/symfony-and-phpunit-memory-leak
    $refl = new \ReflectionObject($this);
    $defaults = $refl->getDefaultProperties();
    foreach ($refl->getProperties() as $prop) {
      if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
        $prop->setAccessible(true);
        if (isset($defaults[$prop->getName()])) {
          $prop->setValue($this, $defaults[$prop->getName()] ?? NULL);
        }
      }
    }
  }

}
