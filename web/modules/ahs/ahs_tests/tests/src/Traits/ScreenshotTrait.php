<?php

namespace Drupal\Tests\ahs_tests\Traits;

/**
 * A trait for taking full page screenshots.
 */
trait ScreenshotTrait
{

  /**
   * The default window height. Greater than 4000 leads to crashes in Lando,
   * greater than 3000 to random crashes in gitlab.
   *
   * @var int
   */
  protected $windowHeight = 2000;

  /**
   * The default window width.
   *
   * @var int
   */
  protected $windowWidth = 1200;

  /**
   * Whether the window is new.
   *
   * @var bool
   */
  protected $newWindow = TRUE;

  /**
   * Whether to add the window width to the end of the screnshot filename.
   *
   * @var bool
   */
  protected $appendWidthToScreenshotName = FALSE;

  protected function screenshotPage($directory, $filename = '') {
    if (!$this->hasSessionForScreenshot()) {
      throw new \Exception("Cannot take screenshot, no session available.");
    }

    if (empty($filename)) {
      $filename = $this->getSession()->getCurrentUrl();
      $filename = empty($filename) ? $this->randomMachineName() : $filename;
    }
    $filename = $this->sanitiseFilePathComponent($filename);
    if ($this->appendWidthToScreenshotName) {
      $width = $this->sanitiseFilePathComponent($this->windowWidth);
      $width = str_pad($width, 4, '0', STR_PAD_LEFT);
      $filename .= "--" . $width;
    }

    $pageHeight = $this->getSession()->evaluateScript("
      return document.documentElement.scrollHeight;
     ");
    $shots = ceil($pageHeight / $this->windowHeight);

    $alphabet = range('A', 'Z');

    // Take multiple screenshots if the document height exceeds the window height.
    for ($i = 0; $i < $shots; $i++) {
      $shotFilename = $filename;
      if ($shots > 1) {
        $this->getSession()->evaluateScript(
          "window.scrollBy(0," . $i * $this->windowHeight . ");"
        );
        $shotFilename = $filename . "(" . $alphabet[$i] . ")";
      }
      $this->saveScreenshot($directory, $shotFilename);
    }
  }

  protected function saveScreenshot($directory, $filename) {
    if (empty($directory)) {
      throw new \Exception("Must specify a directory to save screenshot into.");
    }
    if (empty($filename)) {
      throw new \Exception("Must specify a filename for saved screenshot.");
    }
    $path = $directory . $filename . '.png';
    // Append filename if already exists.
    $path = \Drupal::service('file_system')->getDestinationFilename($path, 0);
    try {
      file_put_contents($path, $this->getSession()->getScreenshot());
    }
    catch (\Exception $e) {}

  }

  protected function hasSessionForScreenshot() {
    if (empty($this->mink)) {
      return FALSE;
    }
    try {
      $session = $this->getSession();
      return TRUE;
    }
    catch(\Exception $e) {
      return FALSE;
    }
  }

  protected function drupalGet($path, array $options = [], array $headers = []) {
    if (!$this->hasSessionForScreenshot()) {
      throw new \Exception("Cannot get session.");
    }

    // The first time we use a window, resize it.
    if ($this->newWindow) {
      $this->getSession()->resizeWindow($this->windowWidth, $this->windowHeight, 'current');
      $this->newWindow = FALSE;
    }
    return parent::drupalGet($path, $options, $headers);
  }
}
