<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_price\Price;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\commerce_store\StoreCreationTrait;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use mysql_xdevapi\Exception;

/**
 * Creates a decoupled user with an email but no name or password.
 */
trait CommerceTrait {

  use StoreCreationTrait;

  // The default store used as the store in this trait.
  protected $defaultStoreId;

  // A basic card number that is always accepted.
  public static $CARD_BASIC_ACCEPTED = '4242 4242 4242 4242';

  // A 3DS required card number that is always accepted.
  public static $CARD_BASIC_ACCEPTED_3DS_REQUIRED = '4000 0027 6000 3184';

  // A 3DS required card number that is always denied.
  public static $CARD_BASIC_DECLINED_3DS_REQUIRED = '4000 0000 0000 3063';

  // A basic card number that is always declined.
  public static $CARD_BASIC_DECLINED = '4000000000000002';

  // Track product weight when creating multiple products.
  protected $productWeight = 0;

/*   protected function createCartWithOrderItem($order_item, $email = NULL) {
    // Create a cart with the order item.
    $store_resolver = $this->container->get('commerce_store.chain_store_resolver');
    $store = $store_resolver->resolve();
    $order_type_resolver = $this->container->get('commerce_order.chain_order_type_resolver');
    $order_type_id = $order_type_resolver->resolve($order_item);
    // This throws a warning in browser test context, which we suppress with @.
    @$order = $this->container->get('commerce_cart.cart_provider')->createCart($order_type_id, $store);
    if (!empty($email)) {
      $order->setEmail($email);
    }
    $this->cartManager = $this->container->get('commerce_cart.cart_manager');
    $this->cartManager->addOrderItem($order, $order_item);
    $order->save();
    $this->assertNotEmpty($order->cart->value, "A cart exists and has a total amount.");
    return $order;
  }

 */

  protected function getStoreId() {
    if (empty($this->storeId)) {
      if (\Drupal::service('commerce_store.chain_store_resolver')->resolve()) {
        $this->storeId = \Drupal::service('commerce_store.chain_store_resolver')->resolve()->id();
      }
      else {
        $this->storeId = $this->setUpTestStore()->id();
      }
    }
    return $this->storeId;
  }

  protected function setUpTestStore() {
    $store = \Drupal::service('commerce_store.chain_store_resolver')->resolve();
    if (empty($store)) {
      $store = $this->createStore('Test store', 'admin@example.com', 'online', TRUE, 'GB', 'GBP');
      $this->markEntityForCleanup($store);
    }
    $store = \Drupal::service('commerce_store.chain_store_resolver')->resolve();
    $this->assertNotEmpty($store, "A store should be available.");
    return $store;
  }

  protected function submitCreditCardDetails($buttonText, $card = '') {
    $page = $this->getCurrentPage();
    if ($selector = $page->find('named', ['field', 'payment_information[payment_method]'])) {
      $this->selectRadio('payment_information[payment_method]', 'new--credit_card--shrimala_trust_stripe');
      try {
        $this->assertSession()->assertWaitOnAjaxRequest();
      }
      catch (\RuntimeException $e) {}
      $this->assertSession()->waitForElement('css', '#card-number-element');
      sleep(3);
    }
    $this->enterValidStripePaymentDetails($card);
    $page->findButton($buttonText)->press();
    sleep(10);
    // $this->assertSession()->pageTextNotContains('Payment information');
  }

  protected function enterValidStripePaymentDetails($card = '', $expiry = '', $cvv = '') {
    if (empty($card)) {
      $card = self::$CARD_BASIC_ACCEPTED;
    }
    if (empty($expiry)) {
      $year = date("y") + 2;
      $expiry = "04 / $year";
    }
    if (empty($cvv)) {
      $cvv = '356';
    }
    $this->enterStripePaymentDetails($card, $expiry, $cvv);
  }

  protected function enterStripePaymentDetails($cardNumber, $expiry, $cvc) {
    $this->fillStripeField('card-number-element', $cardNumber);
    $this->fillStripeField('expiration-element', $expiry);
    $this->fillStripeField('security-code-element', $cvc);
  }

  protected function fillStripeField($id, $value) {
    $div = $this->getCurrentPage()->findById($id);
    $iframe = $div->find('css', 'iframe');
    $this->assertNotEmpty($iframe, "A stripe iframe should have been inserted into the page by javascript.");
    $iframeName = $iframe->getAttribute('name');
    $this->getSession()->getDriver()->switchToIFrame($iframeName);
    // Various inputs elements are present in each iframe, but only
    // the primary has the class 'InputElement'.
    // This is more elegant than searching for 'cardnumber', 'MM / YY' and 'CVC'.
    $input = $this->getSession()->getPage()->find('css', 'input.InputElement');
    $input->setValue($value);
    // Retry because sometimes it gets randomly garbled.
    while ($input->getValue() != $value) {
      $input->setValue($value);
    }
    $this->getSession()->getDriver()->switchToIFrame(null);

  }

  protected function fillBillingInformation($firstName = NULL, $lastName = NULL, array $address = NULL, $phone = NULL) {
    $page = $this->getCurrentPage();
    if ($selector = $page->find('named', [
      'field',
      'ahs_billing_information[profile][select_address]'
    ])) {
      $selector->selectOption('_new');
      try {
        $this->assertSession()->assertWaitOnAjaxRequest();
      }
      catch (\RuntimeException $e) {}
      $this->assertSession()->waitForField('First name');
    }

    if ($firstName) {
      $page->fillField('First name', $firstName);
    }

    if ($lastName) {
      $page->fillField('Last name', $lastName);
    }

    if ($address) {
      $page->fillField('Street address', $address['address_line1']);
      $page->fillField('Town/City', $address['locality']);
      $page->fillField('Postal code', $address['postal_code']);
    }

    if ($phone) {
      $page->fillField('Telephone', $phone);
    }
  }

  protected function assertOrderByEmail($email, $orderType, $orderItemType, float $amount, array $skus = NULL, $state = NULL) {
    $order = $this->loadEntityByProperties('commerce_order', ['mail' => $email, 'type' => $orderType], FALSE);
    $this->assertNotEmpty($order, "An order of type '$orderType' with the specified email should exist.");
    $this->assertOrder($order, $orderType, $orderItemType, $amount, $skus, $state);
    return $order;
  }

  protected function assertOrder($order, $orderType, $orderItemType, float $amount, array $skus, $state) {
    $this->assertNotEmpty($order, "The order should exist.");

    $this->assertSame($orderType, $order->bundle(), sprintf('The order should be of the type "%s"', $orderType));
    $this->assertOrderHasStore($order);

    $this->assertTrue($order->hasItems(), 'The order should have an order item');
    if ($skus) {
      $skusCount = count($skus);
      $items = $order->getItems();
      $this->assertCount($skusCount, $items, "The order should have $skusCount order items.");
      for ($x = 0; $x < $skusCount; $x++) {
        $item = $items[$x];
        $sku = $skus[$x];
        $this->assertSame($orderItemType, $item->bundle(), 'The order item should be of the type ahs_lah_enrolment');
        $this->assertEquals($amount, $item->getTotalPrice()->getNumber(), 'The order item price should be the test amount.');

        $purchasedEntity = $item->getPurchasedEntity();
        $this->assertNotEmpty($purchasedEntity, "Order item should have a purchased entity.");
        $this->assertSame($sku, $purchasedEntity->getSku(), sprintf("The purchased entity should have specific sku of '%s'", $sku));
      }
    }
    if ($state) {
      $this->assertSame($state, $order->getState()->getId(), "The order should be in a '$state' state.");
    }
  }

  protected function assertNoOrderPayment($order) {
    $orderId = $order->id();
    $payment = $this->loadEntityByProperties('commerce_payment', ['order_id' => $orderId]);
    $this->assertEmpty($payment, "A payment entity should not exist for the order");
  }

  protected function assertOrderPayment($order, float $amount = NULL) {
    $orderId = $order->id();
    $payment = $this->loadEntityByProperties('commerce_payment', ['order_id' => $orderId]);
    $this->assertNotEmpty($payment, "A payment entity should have been created for the order");

    // Payment remote state is not set by stripe gateway so not asserted here.
    $this->assertNotEmpty($payment->getRemoteId());

    // Unfortunately isCompleted is set automatically and
    // does not guarantee the payment is successful.
    $this->assertTrue($payment->isCompleted(), 'Payment should be completed.');
    $this->assertSame('completed', $payment->getState()->getId(), "The payment should be in a completed state.");

    $amount = is_null($amount) ? (float) $order->getTotalPrice()->getNumber() : $amount;
    $this->assertEquals($amount, $payment->getAmount()->getNumber(), 'Payment amount should be correct.');
    $this->assertEquals($order->getTotalPrice()->getCurrencyCode(), $payment->getAmount()->getCurrencyCode(), 'Payment currency should be correct.');
    $this->assertEquals($amount, $payment->getBalance()->getNumber(), 'Payment balance should be correct.');

    // Payment should have a payment method which has a billing profile,
    // both owned by the order customer.
    $paymentMethod = $payment->getPaymentMethod();
    $this->assertNotEmpty($paymentMethod, 'Payment should have a payment method');
    $this->assertOrderHasCustomer($order);
    $this->assertOrderHasStore($order);
    $customer = $order->getCustomer();
    $this->assertPaymentMethodOwner($paymentMethod, $customer);
    if (!empty($order->getBillingProfile())) {
      $this->assertPaymentMethodBillingProfile($paymentMethod, $customer, $order);
    }

  }

  protected function assertSubscriptionCreatedFromInitialOrder($initialOrder, ?float $amount, $currency, $sku, $scheduleId) {
    $this->subscriptionInitialOrderId = $initialOrder->id();
    $this->assertOrderHasCustomer($initialOrder);
    $customer = $initialOrder->getCustomer();
    $this->assertOrderHasPaymentMethod($initialOrder);
    $this->assertOrderHasStore($initialOrder);

    // Assert subscription is created.
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['initial_order' => $initialOrder->id()]);
    $this->assertNotEmpty($subscription, 'A subscription should have been created for the order');
    $this->assertNotEmpty($subscription->getStore(), 'A subscription should have a store');
    $this->assertEquals($subscription->getStoreId(), $this->getStoreId(), 'Subscription store should be the standard store.');
    $this->assertEquals($initialOrder->getStoreId(), $subscription->getStoreId(), 'Subscription store should be same as initial order store.');
    $this->assertEquals($customer->id(), $subscription->getCustomerId(), 'Subscription should be owned by customer.');
    $this->assertLessThan(time(), $subscription->getStartTime(), 'Subscription start time should be in the past');
    $this->assertEmpty($subscription->getEndTime(), 'Subscription should endure forever');
    $this->assertSame('active', $subscription->getState()->getId(), 'Subscription state should be active.');
    $this->assertEquals($amount, $subscription->getUnitPrice()->getNumber());
    $this->assertEquals($currency, $subscription->getUnitPrice()->getCurrencyCode());
    $this->assertEquals(1, $subscription->getQuantity(), 'Quantity expected to be 1, reason unknown.');
    $this->assertTrue($subscription->hasPurchasedEntity(), 'Subscription should have a purchase entity');
    $this->assertSame($sku, $subscription->getPurchasedEntity()->getSku(), 'Subscription should be for specified sku');
    $method = $subscription->getPaymentMethod();
    $this->assertNotEmpty($method, 'Subscription should have a payment method');
    $this->assertPaymentMethodOwner($method, $customer);
    if (!empty($initialOrder->getBillingProfile())) {
      $this->assertPaymentMethodBillingProfile($method, $customer);
    }
    $this->assertEquals($scheduleId, $subscription->getBillingSchedule()->id(), 'Subscription should use specified schedule.');

    // Assert first draft recurring order is created from subscription.
    $this->assertCount(1, $subscription->getOrderIds(), 'The subscription should have generated 1 order.');
    $order = $subscription->getOrders()[0];
    $this->assertOrderGeneratedBySubscription($order, $subscription);
    $this->assertSame('draft', $order->getState()->getId(), 'Order generated by subscription should be in draft state.');
    $this->subscriptionFirstRecurringOrderId = $order->id();
  }

  protected function assertFirstRecurringOrderIsDue($initialOrder, float $amount) {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['initial_order' => $initialOrder->id()]);
    $this->assertInstanceOf(SubscriptionInterface::class, $subscription, "There should be a subscription which has " . $initialOrder->id() . " as its initial order");
    $firstOrder = $subscription->getOrders()[0];

    // First subscription order is now pending.
    $this->assertOrderGeneratedBySubscription($firstOrder, $subscription);
    $this->assertSame('needs_payment', $firstOrder->getState()->getId(), 'Order generated by subscription should be in needs_payment state.');
  }

  protected function assertRecurringOrderCompleted($initialOrder, ?float $amount, $index) {
    $subscription = $this->loadEntityByProperties('commerce_subscription', ['initial_order' => $initialOrder->id()]);
    $this->assertInstanceOf(SubscriptionInterface::class, $subscription, "There should be a subscription which has " . $initialOrder->id() . " as its initial order");
    $firstOrder = $subscription->getOrders()[$index];

    // First subscription order is now completed.
    $this->assertOrderGeneratedBySubscription($firstOrder, $subscription);
    $this->assertSame('completed', $firstOrder->getState()->getId(), 'Order generated by subscription should be in completed state.');
    $this->assertOrderPayment($firstOrder, $amount);

    // A second subscription order is now present.
    $orderCount = $index + 2;
    $this->assertCount($orderCount, $subscription->getOrderIds(), "The subscription should have generated $orderCount orders.");
    $secondOrder = $subscription->getOrders()[$index + 1];
    $this->assertOrderGeneratedBySubscription($secondOrder, $subscription);
    $this->assertSame('draft', $secondOrder->getState()->getId(), 'Order generated by subscription should be in draft state.');
  }

  protected function assertOrderGeneratedBySubscription($order, $subscription) {
    $this->assertEquals($subscription->getCustomerId(), $order->getCustomerId(), 'Order generated by my subscription should be owned by subscription customer.');
    $this->assertSame('recurring', $order->bundle(), 'Order generated by subscription should be of type recurring.');
    $this->assertOrderHasStore($order);
    $this->assertEquals($subscription->getStoreId(), $this->getStoreId(), 'Subscription store should be the standard store.');
    $this->assertEquals($order->getStoreId(), $subscription->getStoreId(), 'Order store should be same as subscription store.');
    $this->assertTrue($order->hasItems(), 'Order generated by subscription should have an order item');
    // Sometimes order items can be stored in a way that hasItems returns true but there isn't really one.
    $this->assertNotEmpty($order->getItems()[0]->id());
    $this->assertSame($subscription->getPurchasedEntity()->getSku(), $order->getItems()[0]->getPurchasedEntity()->getSku(), 'Order generated by subscription should be for Sangha monthly membership');
    $this->assertEquals($subscription->getUnitPrice()->getNumber(), $order->getTotalPrice()->getNumber(), 'Order generated by subscription should have correct amount.');
    $this->assertEquals($subscription->getUnitPrice()->getCurrencyCode(), $order->getTotalPrice()->getCurrencyCode(), 'Order generated by subscription should have correct currency.');
  }

  protected function getOrder($orderId) {
    $order = $this->loadEntity('commerce_order', $orderId);
    $this->assertNotEmpty($order, 'Order should still exist');
    return $order;
  }

  protected function assertOrderHasStore($order) {
    $store =  $order->getStore();
    $this->assertNotEmpty($store, 'Order should have a store');
    $this->assertEquals($store->id(), $this->getStoreId(),'Order store should be the standard store');
  }


  protected function assertOrderHasCustomer($order) {
    $customer =  $order->getCustomer();
    $this->assertNotEmpty($customer, 'Order should have a customer');
    $this->assertFalse($customer->isNew(), 'Order customer should be saved');
    $this->assertFalse($customer->isAnonymous(), 'Order customer should not be anonymous.');
    $this->assertNotEmpty($customer->id(), 'Customer should not be anoymous and should be saved.');
  }

  protected function assertOrderHasPaymentMethod($order) {
    $method = $order->get('payment_method')->entity;
    $this->assertNotEmpty($method, 'Order should have a payment method');
    $this->assertPaymentMethodOwner($method, $order->getCustomer());
    if (!empty($order->getBillingProfile())) {
      $this->assertPaymentMethodBillingProfile($method, $order->getCustomer());
    }
  }

  protected function assertPaymentMethodOwner($paymentMethod, $customer) {
    $methodOwner = $paymentMethod->getOwner();
    $this->assertNotEmpty($methodOwner, 'Payment method should have an owner');
    $this->assertFalse($methodOwner->isAnonymous(), 'Payment method owner should be a specific user, not anonymous.');
    $this->assertEquals( $customer->id(), $methodOwner->id(),'Payment method should belong to specified customer.');
  }

  protected function assertPaymentMethodBillingProfile($paymentMethod, $customer, $order = NULL) {
    $profile = $paymentMethod->getBillingProfile();
    $this->assertNotEmpty($profile, 'Payment method should have a billing profile');


  }

  protected function assertBillingProfile($profile, $customer) {
    $this->assertNotEmpty($profile, "Profile should exist");
    // Don't call $profile->getOwner() as that leads to some weird problem on cleanup.
    $this->assertEquals($profile->getOwnerId(), 0, 'Billing profile should be anonymous');
  }

  protected function createOrder($orderType, $sku, $customer = NULL, $amount = '12.00') {
    if (is_null($customer)) {
      $customer = $this->createDecoupledUser();
    }
    $this->customer = $customer;
    $variation = $this->ensureProductBySku($sku, $amount);
    $order_item = OrderItem::create([
      'type' => $orderType,
      'quantity' => 1,
      'unit_price' => new Price($amount, 'GBP'),
      'purchased_entity' => $variation,
      'overridden_unit_price' => TRUE,
    ]);
    $order_item->save();
    $this->markEntityForCleanup($order_item);

    $paymentMethod = $this->createStripePaymentMethodForUser($customer);

    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = Order::create([
      'type' => $orderType,
      'state' => 'draft',
      'uid' => $customer->id(),
      'order_items' => [$order_item],
      'mail' => $customer->getEmail(),
      'store_id' => $this->getStoreId(),
      'payment_method' => $paymentMethod,
    ]);
    $order->save();
    $this->markEntityForCleanup($order);
    $payment = $this->createPayment($order, $paymentMethod);

    $payment->setState('completed');
    $payment->save();

    // Trigger completion transitions, e.g. to initiate subscriptions.
    $order->set('state', 'paid')->save();
    $order->set('state', 'completed')->save();
    $order = $this->reloadEntity($order);
    return $order;
  }

  protected function createPayment($order) {
    $payment = Payment::create([
      'payment_gateway' => 'shrimala_trust_stripe',
      'order_id' => $order,
      'amount' => $order->getBalance(),
      'state' => 'new',
      'payment_method' => $order->get('payment_method')->entity,
      'remote_id' => $this->randomMachineName(),
    ]);
    $payment->save();
    $this->markEntityForCleanup($payment);
    return $payment;
  }

  protected function ensureProductBySku($sku, $amount = '0.00') {
    $variation = $this->loadEntityByProperties('commerce_product_variation', ['sku' => $sku]);
    if (!empty($variation)) {
      // Check the variation has a product, in case the data has got corrupt.
      $productId = $variation->getProductId();
      if (empty($productId)) {
        throw new \Exception('Variation is missing a product Id');
      }
    }
    else {
      $variationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');
      $variation = $variationStorage->create([
        'type' => 'default',
        'title' => $sku,
        'sku' => $sku,
        'status' => 1,
        'price' => [
          'number' => $amount,
          'currency_code' => 'GBP',
        ],
      ]);
      $variation->save();
      $this->markEntityForCleanup($variation);

      $productStorage = $this->entityTypeManager->getStorage('commerce_product');
      $this->productWeight = $this->productWeight + 1;
      $product = $productStorage->create([
        'type' => 'ahs_miscellaneous',
        'variations' => [$variation],
        'title' => $sku,
        'field_weight' => $this->productWeight,
      ]);
      $product->save();
      $this->markEntityForCleanup($product);
    }
    return $variation;
  }

  protected function createStripePaymentMethodForUser($user) {
    $payment_gateway = PaymentGateway::load('shrimala_trust_stripe');
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = PaymentMethod::create([
      'type' => 'credit_card',
      'payment_gateway' => $payment_gateway,
      'uid' => $user->id(),
      'payment_gateway_mode' => 'test',
      'card_type' => 'visa',
      'card_number' => '1111',
      'reusable' => TRUE,
      // 1 year in the future
      'expires' => time() + 31536000,
    ]);
    $payment_method->save();
    $this->markEntityForCleanup($payment_method);
    return $payment_method;
  }

  protected function assertStripeCustomer(array $expectedProperties = []) {
    $me = $this->getMe();
    $remote_ids = $me->get('commerce_remote_id');
    $remote_id = $remote_ids->getByProvider('shrimala_trust_stripe|live') ?? $remote_ids->getByProvider('shrimala_trust_stripe|test');

    $this->assertFalse($me->get('commerce_remote_id')->isEmpty(), "I should have a remote id stored on my user");
    $defaultExpectedProperties = [
      'email' => $me->getEmail(),
      'name' => $me->label(),
      'description' => sprintf('%s (%s)', $me->label(), $me->id()),
    ];

    $expectedProperties = $expectedProperties + $defaultExpectedProperties;

    $stripeCustomer = \Stripe\Customer::retrieve($remote_id);
    foreach($expectedProperties as $propertyName => $propertyValue) {
      $this->assertEquals($propertyValue, $stripeCustomer->{$propertyName}, "The customer should have '$propertyValue' as $propertyName. Actual customer: " . print_r($stripeCustomer, TRUE));
    }
  }

}
