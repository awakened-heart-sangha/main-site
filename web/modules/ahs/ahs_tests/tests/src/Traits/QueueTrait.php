<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\Core\Queue\RequeueException;

/**
 * A trait for working with queues.
 */
trait QueueTrait {

  /**
   * Setup for a test that will run queues, e.g. by running cron.
   */
  protected function setUpQueues() {
    // Recurring orders from live customers fail in tests when cron is called
    // because the test runs in Stripe's test mode not live mode.
    $this->deleteEntitiesByProperties('commerce_order', ['type' => 'recurring']);
    $this->clearQueues();
  }

  protected function tearDownQueues() {
    // Remove cron lock in case cron failed.
    \Drupal::lock()->release('cron');
    $this->clearQueues();
  }

  protected function clearQueue($queueId) {
    // Try advanced queue first.
    if (\Drupal::service('module_handler')->moduleExists('advancedqueue') && $queue = Queue::load($queueId)) {
      $backend = $queue->getBackend();
      $backend->deleteQueue();
    }
    // If it's not advanced queue, try core queue.
    else {
      $queue = \Drupal::queue($queueId);
      $queue->deleteQueue();
    }
  }

  /**
   * Process jobs in a core queue.
   *
   * This is normally done in a cron run, but that doesn't share a container
   * with tests so makes it hard to mock container services.
   *
   * @param $queue_name
   */
  protected function processCoreQueue($queue_name) {
    \Drupal::logger('ahs_tests')->notice("Processing core queue $queue_name");
    // Queues run in their own process.
    \Drupal::service('entity.memory_cache')->deleteAll();

    \Drupal::service('queue')->get($queue_name)->createQueue();
    $queue_worker = \Drupal::service('plugin.manager.queue_worker')->createInstance($queue_name);
    $queue = \Drupal::service('queue')->get($queue_name);
    while ($item = $queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      } catch (RequeueException $e) {
        // The worker requested the task be immediately requeued.
        $queue->releaseItem($item);
      }
    }
    \Drupal::service('entity.memory_cache')->deleteAll();
  }

  protected function processAdvancedQueue($queue_name) {
    \Drupal::logger('ahs_tests')->notice("Processing advanced queue $queue_name");
    /** @var \Drupal\advancedqueue\Entity\Queue queue */
    if (\Drupal::service('module_handler')->moduleExists('advancedqueue') && $queue = Queue::load($queue_name)) {
      // Queues run in their own process.
      \Drupal::service('entity.memory_cache')->deleteAll();
      $queue_processor = \Drupal::service('advancedqueue.processor');
      $queue_processor->processQueue($queue);
      \Drupal::service('entity.memory_cache')->deleteAll();
    }
    else {
      throw new \Exception('Cannot load advanced queue ' . $queue_name);
    }
  }

  protected function processQueues() {
    \Drupal::logger('ahs_tests')->notice("Processing all queues.");
    $queueManager = \Drupal::service('plugin.manager.queue_worker');
    foreach ($queueManager->getDefinitions() as $queueId => $info) {
      $this->processCoreQueue($queueId);
    }
    if (\Drupal::service('module_handler')->moduleExists('advancedqueue')) {
      $queueStorage = \Drupal::service('entity_type.manager')->getStorage('advancedqueue_queue');
      $queues = $queueStorage->loadMultiple();
      foreach ($queues as $queueId => $queue) {
        $this->clearQueue($queueId);
      }
    }
    \Drupal::logger('ahs_tests')->notice("Finished processing all queues.");
  }

  protected function clearQueues() {
    $queueManager = \Drupal::service('plugin.manager.queue_worker');
    foreach ($queueManager->getDefinitions() as $queueId => $info) {
      $this->clearQueue($queueId);
    }
    if (\Drupal::service('module_handler')->moduleExists('advancedqueue')) {
      $queueStorage = \Drupal::service('entity_type.manager')->getStorage('advancedqueue_queue');
      $queues = $queueStorage->loadMultiple();
      foreach ($queues as $queueId => $queue) {
        $this->clearQueue($queueId);
      }
    }
  }

}
