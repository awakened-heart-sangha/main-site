<?php

namespace Drupal\Tests\ahs_tests\Traits;

/**
 * A trait that holds very generic methods used in different base classes.
 */
trait UtilityTrait
{

  protected function hasSession() {
    if (empty($this->mink)) {
      return FALSE;
    }
    try {
      $session = $this->getSession();
      return TRUE;
    }
    catch(\Exception $e) {
      return FALSE;
    }
  }

  protected function sanitiseFilePathComponent($path, $maxLength = 30) {
    // Put any language specific filters here,
    // like, for example, turning the Swedish letter "å" into "a"

    // Remove any character that is not alphanumeric, white-space, or a hyphen
    $path = preg_replace('/[^a-z0-9\s\-]/i', '', $path);
    // Replace all spaces with hyphens
    $path = preg_replace('/\s/', '-', $path);
    // Replace multiple hyphens with a single hyphen
    $path = preg_replace('/\-\-+/', '-', $path);
    // Remove leading and trailing hyphens, and then lowercase the URL
    $path = trim($path, '-');

    if (strlen($path) > $maxLength) {
      $path = substr($path, 0, $maxLength);

      /**
       * If there is a hyphen reasonably close to the end of the slug,
       * cut the string right before the hyphen.
       */
      $cutPoint = -1 * max(4, min(12, $maxLength / 5));
      if (strpos(substr($path, $cutPoint), '-') !== false) {
        $path = substr($path, 0, strrpos($path, '-'));
      }
    }

    return $path;

  }

  // Gets a pretty name for the current test, which may or may not
  // be provided by a Behat feature.
  protected function getTestLabel() {
    $label = NULL;
    if (method_exists($this, 'getProvidedScenario')) {
      try {
        $label = $this->getProvidedScenario()->getTitle();
      } catch (\Exception $e) {}
    }
    // We couldn't get a Behat scenario, fall back to the phpunit test name.
    if (empty($label)) {
      $label = $this->getName();
    }
    return $label;
  }

}
