<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Behat\Gherkin\Node\TableNode;
use Drupal\ahs_training\Entity\TrainingRecord;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\ahs_tests\Traits\Definitions\BrowsingDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\GroupDefinitions;
use Drupal\Tests\ahs_tests\Traits\Definitions\UserDefinitions;
use Drupal\user\UserInterface;

trait TrainingTrait {

  use GroupTrait;
  use UserTrait;
  use UserDefinitions;
  use BrowsingDefinitions;
  use QueueTrait;
  use GroupDefinitions;

  protected $experiences;
  protected $experience;
  protected $courseTitle;
  protected $group;
  protected $students;
  protected $mentors;
  protected $observers;
  protected $users;

  protected $STUDENT_TERM_ID = 69;
  protected $MENTOR_TERM_ID = 70;
  protected $OBSERVER_TERM_ID = 71;

  protected function setUp(): void {
    parent::setUp();
    $this->clearQueues();
  }

  /**
   * @Given experiences:
   */
  public function experiences(TableNode $table) {
    foreach($table->getHash() as $row) {
      $title = $row['experience'];
      $edit = [
        'title' => $title,
        'type' => 'training_experience',
        'uid' => 1,
      ];
      $this->experiences[$title] = $this->createEntity('node', $edit);
    }
  }

  /**
   * @Given a user named :name has training records for:
   * @Given a user has training records for:
   */
  public function aUserNamedHasTrainingRecordsFor(TableNode $table, $name = NULL) {
    $user = $this->createNamedUser($name, [], FALSE);
    $this->createTrainingRecordsForUser($user, $table);
  }

  /**
   * @Given I have training records for:
   * @When I am given training records for:
   */
  public function iHaveTrainingRecordsFor(TableNode $table) {
    $user = $this->getMe();
    $this->users[(string) $user->getDisplayName()] = $user;
    $this->createTrainingRecordsForUser($user, $table);
  }


  protected function createTrainingRecordsForUser(UserInterface $user, TableNode $table) {
    foreach($table->getHash() as $row) {
      $experience = $this->experiences[$row['experience']];
      $recordFields = [
        'experience' => $experience->id(),
        'uid' => $user->id(),
      ];
      if (isset($row['effective_time']) && !empty($row['effective_time'])) {
        $date = new DrupalDateTime($row['effective_time']);
        $recordFields['effective_time']  = $date->getTimestamp();
      }
      // We can just create the record using this method because it will trigger
      // the postSave method on the TrainingRecord Entity which calls the
      // cascade.
      $this->createEntity('training_record', $recordFields);
      $result = $this->loadEntityByProperties('training_record', $recordFields, FALSE);
      $this->assertNotFalse(
        $result,
        sprintf('A training record should have been created')
      );
    }

  }

  /**
   * @Given a user named :name has no training records
   * @Given a user has no training records
   */
  public function aUserNamedHasNoTrainingRecords($name = NULL) {
    $user = $this->createDecoupledUser();
    $name = $name ?? 'default';
    $this->updateProfile($user, 'about', ['field_name_sangha' => $name]);
    if ($name === 'default') {
      $this->user = $user;
    }
    $this->users[$name] = $user;
    $result = $this->loadEntityByProperties('training_record', [
      'uid' => $this->users[$name]->id()
    ], FALSE);
    $this->assertFalse(
      $result,
      sprintf('%s should have NO training records', $name)
    );
  }

  /**
   * Helper function
   */
  function assertTrainingRecordWasCreated($participant_uid, $experience_id, $group_id = FALSE, $type = NULL) {
    $type = $type ?? TrainingRecord::STUDENT_TERM_ID;
    $values = [
      'uid' => $participant_uid,
      'experience' => $experience_id,
      'participant_type' => $type,
    ];
    if ($group_id !== FALSE) {
      $values['group'] = $group_id;
    }
    $result = $this->loadEntityByProperties('training_record', $values, TRUE);
    $this->assertNotFalse(
      $result,
      sprintf('A training record should have been created for uid: %s, experience_id: %s', $participant_uid, $experience_id)
    );
  }

  /**
   * Helper function
   */
  function assertTrainingRecordWasNotCreated($participant_uid, $training_id, $group_id = FALSE) {
    $values = [
      'uid' => $participant_uid,
      'experience' => $training_id
    ];
    if ($group_id !== FALSE) {
      $values['group'] = $group_id;
    }
    $result = $this->loadEntityByProperties('training_record', $values, TRUE);
    $this->assertFalse(
      $result,
      sprintf('A training record should not have been created for uid: %s, training_id: %s', $participant_uid, $training_id)
    );
  }

  protected function getConditionsFromTable(TableNode $table) {
    $conditions = [];
    foreach($table->getHash() as $row) {
      $experienceValues = [];
      $experienceNames = explode(', ', $row['conditions']);
      foreach ($experienceNames as $experienceName) {
        $experienceValues[] = ['target_id' => $this->experiences[$experienceName]->id()];
      }
      $condition = $this->createEntity('training_condition', [
        'experiences' => $experienceValues
      ]);
      $conditions[$condition->id()] = $condition;
    }
    return $conditions;
  }

  /**
   * @Given the :bundle has :type conditions:
   * @Given a/an :bundle has :type conditions:
   */
  public function aGroupHasConditions($bundle, $type, TableNode $table) {
    $conditions = $this->getConditionsFromTable($table);
    $conditionsValues = [];
    foreach($conditions as $condition) {
      $conditionsValues[] = ['target_id' => $condition->id()];
    }
    $this->group->set("field_{$type}_conditions", $conditionsValues);
    $this->group->save();
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given today is :date
   */
  public function todayIs($date) {
    \Drupal::service('datetime.time')->setTime($date);
  }

  /**
   * @Given :name is already a participant in the :bundle
   * @When :name join(s) the :bundle
   * @When the :name joins the :bundle
   */
  public function isAParticipantInTheGroup($name, $bundle) {
    $user = $name === 'I' ? $this->getMe() : $this->users[$name];
    $membership = ['field_participant_type' => $this->STUDENT_TERM_ID];
    $this->{$bundle}->addMember($user, $membership);
    $this->group = $this->{$bundle};
    $this->entityTypeManager->getStorage('group_content_type')->resetCache();
    $groupMemberships = \Drupal::service('group.membership_loader')->loadByUser($user);
    $this->assertNotNull($groupMemberships);
    $this->assertGroupMember($user, $this->{$bundle}, \Drupal\ahs_training\Entity\TrainingRecord::STUDENT_TERM_ID);
  }

  /**
   * @Given the :bundle has no :type conditions
   * @Given a/an :bundle has no :type conditions
   */
  public function aGroupHasNoConditions($bundle, $type) {
    $this->group->set("field_{$type}_conditions", []);
    $this->group->save();
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given the :bundle is suggested
   */
  public function groupIsSuggested($bundle) {
    $this->group->set('field_suggested', TRUE)->save();
    $this->{$bundle} = $this->group;
  }

  /**
   * @Given the :bundle is not suggested
   */
  public function groupIsNotSuggested($bundle) {
    $this->group->set('field_suggested', FALSE)->save();
    $this->{$bundle} = $this->group;
  }

}
