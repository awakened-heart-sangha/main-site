<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Tests\user\Traits\UserCreationTrait as CoreUserCreationTrait;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\profile\Entity\Profile;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;

/**
 * Creates a decoupled user with an email but no name or password.
 */
trait UserTrait {
  use CoreUserCreationTrait;
  use EntityTrait;

  protected $me;

  protected $myEmail;

  protected $myAlternativeEmail;

  protected $user;

  protected $users;

  protected function getMe() {
    return $this->getOrCreateNamedUser('me');
  }

    protected function reloadUser(UserInterface $user) {
      // If we've lost the raw password, login steps may fail.
      // It's better to catch this explicitly and as early as possible.
      $passRaw = NULL;
      $isLoggedIn = NULL;
      if (isset($user->passRaw)) {
        //$this->assertNotEmpty($this->me->passRaw, "User should have a raw password.");
        $passRaw = $user->passRaw;
      }
      if ($user->isCoupled()) {
        $isLoggedIn = $this->isUserLoggedIn($user);
      }

      $user = $this->reloadEntity($user);

      // Verify entity came through reload without problem.
      if (!is_null($passRaw)) {
        $user->passRaw = $passRaw;
        //$this->assertNotEmpty($me->passRaw, "User should have a raw password.");
      }
      if ($isLoggedIn) {
        $this->assertTrue($this->isUserLoggedIn($user), 'User should still be logged in.');
      }

      // Profiles can be tricky to reload.
      $user->reloadProfileFields();

      return $user;
    }

    protected function coupleMe() {
        $me = $this->getMe();
        $me = $this->coupleUser($me);
        $this->setMe($me);
      }

    protected function setMe(UserInterface $user = NULL) {
        $this->setNamedUser($user, 'me');
    }

    protected function randomEmail() {
      return $this->randomMachineName() . '@example.com';
    }

    /**
     * Creates a decoupled user and tracks it for automatic cleanup.
     *
     * @param array $permissions
     * @param null  $email
     * @param bool  $admin
     *
     * @return \Drupal\user\Entity\User|false
     */
    protected function createDecoupledUser(array $permissions = [], $email = null, $admin = false) {
      $fields = ['mail' => !empty($email) ? $email : $this->randomEmail()];
      $user = $this->createUser($permissions, $fields, $admin);
      return $user;
    }

    /**
     * Creates a user and tracks it for automatic cleanup.
     *
     * @param array $permissions
     * @param null  $email
     * @param bool  $admin
     *
     * @return \Drupal\user\Entity\User|false
     */
    protected function createUser(array $permissions = [], $edit = NULL, $admin = false, $coupled = FALSE) {
        // Default to coupled user if fields are not specified.
        if ($coupled || !is_array($edit) || count($edit) === 0) {
          if (!is_array($edit)) {
            $name = $edit;
            $edit = [];
          }
          $edit['name'] = !empty($name) ? $name : $this->randomMachineName();
          $edit['pass'] = \Drupal::service('password_generator')->generate(10);
        }
        if (!isset($edit['mail'])) {
          $edit['mail'] = $this->randomEmail();
        }

        // Create a role with the given permission set, if any.
        $rid = FALSE;
        if ($permissions) {
          $rid = $this
            ->createRole($permissions);
          if (!$rid) {
              return FALSE;
          }
          $this->markEntityForCleanup($this->loadEntity('user_role', $rid));
        }

        // Create a user assigned to that role.
        if (!isset($edit['status'])) {
            $edit['status'] = 1;
        }
        if ($rid) {
            $edit['roles'] = [$rid,];
        }
        if ($admin) {
            $edit['roles'][] = $this->createAdminRole();
        }

        $account = $this->entityTypeManager->getStorage('user')->create($edit);
        $account->save();
        $violations = $account->validate();
        if ($violations->count() === 0) {
          $account->save();
        }
        else {
          throw new \Exception("Invalid user: " . print_r($violations, TRUE));
        }

        $this->assertTrue(
            !empty($account->id()),
            new FormattableMarkup('User created with email %email', ['%email' => $edit['mail'],]));
        if (empty($account->id())) {
            return FALSE;
        }
        $this->markEntityForCleanup($account);

        // Add the raw password so that we can log in as this user.
        if (isset($edit['pass'])) {
            $account->pass_raw = $edit['pass'];
            // Support BrowserTestBase as well.
            $account->passRaw = $account->pass_raw;
        }
        return $account;
    }

    /**
     * Add a username and password to a decoupled user.
     *
     * @param UserInterface $user
     * @param string  $username
     * @param string  $password
     *
     * @return \Drupal\user\Entity\User
     */
    protected function coupleUser($user, $username = NULL, $password = NULL) {
        if ($user->isDecoupled()) {
            $username = empty($username) ? $this->randomMachineName() : $username;
            $password = empty($password) ? \Drupal::service('password_generator')->generate() : $password;
            $user->setUsername($username);
            $user->setPassword($password);
            $user->save();
            $user->passRaw = $password;
        }
        $this->assertNotEmpty($user->getAccountName(), "User should have a username.");
        $this->assertNotEmpty($user->getPassword(), "User should have a password.");
        $this->assertNotEmpty($user->passRaw, "User should have a raw password.");
        $this->assertFalse($user->isDecoupled(), "User should not be decoupled.");
        return $user;
    }

    /**
     * Adds a profile for a user.
     *
     * @param array $fields
     * @param \Drupal\user\UserInterface $user
     * @param string $type
     *
     * @return \Drupal\profile\Entity\Profile|false
     */
    protected function updateProfile($user = NULL, $type = NULL, $fields = []) {
        if (empty($user)) {
            $user = User::load($fields['uid']);
        }
        $this->assertInstanceOf('Drupal\user\UserInterface', $user);
        // Add a user reference if not already set.
        $fields = $fields + ['uid' => $user->id()];

        // Add a profile type if not already set.
        if (!empty($type)) {
            $fields = $fields + ['type' => $type];
        }

        $user = $this->reloadEntity($user);
        $profile = $user->get("{$type}_profiles")->entity;
        if ($profile) {
            foreach ($fields as $fieldName => $value) {
                $profile->set($fieldName, $value);
            }
        }
        else {
            $profile = Profile::create($fields);
            $this->assertInstanceOf('Drupal\profile\Entity\ProfileInterface', $profile);
        }

        $profile->save();
        //$this->markEntityForCleanup($profile);

        // Verify the profile has been attached to user.
        $this->assertInstanceOf('Drupal\profile\Entity\ProfileInterface', $profile);
        $this->assertEquals($profile->getOwnerId(), $user->id(), "Profile should be referenced to user");

        return $profile;

    }

    /**
     * Returns whether a given user account is logged in.
     *
     * The drupalUserIsLoggedIn method in UiHelperTrait uses session ids and is too complex.
     *
     * @param \Drupal\User\UserInterface $user
     *   The user account object to check.
     *
     * @return bool
     *   Return TRUE if the user is logged in, FALSE otherwise.
     */
    protected function isUserLoggedIn(UserInterface $user) {
        if (!empty($this->loggedInUser) && $loggedInId = $this->loggedInUser->id()) {
            return $loggedInId == $user->id();
        }
        return FALSE;
    }

    protected function isLoggedOut() {
        return empty($this->loggedInUser);
    }

    protected function getMyEmail($email = NULL) {
      if ($this->hasUser('me')) {
        return $this->getUser('me')->getEmail();
      }
      if (empty($this->myEmail)) {
        // Use random email if none supplied or already set.
        $email = !empty($email) ? $email : $this->randomEmail();
        $this->myEmail = !empty($this->myEmail) ? $this->myEmail : $email;
      }
      return $this->myEmail;
    }

  protected function forgetMe() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      $this->drupalLogout();
    }

    user_logout();
    $this->drupalResetSession();
    $this->me = NULL;
    $this->myEmail = NULL;
    $this->myAlternativeEmail = NULL;
  }

  protected function getMyAlternativeEmail($email = NULL) {
    if ($this->hasUser('me')) {
      $me = $this->getUser('me');
      $this->myAlternativeEmail = $me->get('alternative_user_emails')->isEmpty() ? NULL : $me->get('alternative_user_emails')->first()->value;
    }
    if (empty($this->myAlternativeEmail) || $email) {
      $this->setMyAlternativeEmail($email);
    }
    return $this->myAlternativeEmail;
  }

  protected function setMyAlternativeEmail($email = NULL) {
    // Use random email if none supplied and not already set.
    if (empty($email) || $email === TRUE) {
      $email = $this->myAlternativeEmail;
    }
    if (empty($email)) {
      $email = $this->randomEmail();
    }
    $this->myAlternativeEmail = $email;
    if ($this->hasUser('me')) {
      $me = $this->getUser('me');
      $me->get('alternative_user_emails')->appendItem($email);
      $me->save();
    }
  }

  protected function assertUserProfiles(UserInterface $user) {
    $profile_storage = \Drupal::entityTypeManager()->getStorage('profile');
    $aboutProfiles = $profile_storage->loadMultipleByUser($user, 'about');
    $this->assertSame(1, count($aboutProfiles), "The user should have exactly 1 'about' profile.");
    $aboutProfiles = $profile_storage->loadMultipleByUser($user, 'admin');
    $this->assertSame(1, count($aboutProfiles), "The user should have exactly 1 'admin' profile.");
  }

  /**
   * @param null $name
   *   The name to identify this user. @see getUser().
   * @param array $fields
   * @param false $coupled
   *
   * @return \Drupal\user\UserInterface
   */
  protected function createNamedUser($name = NULL, $fields = [], $coupled = FALSE, $sanghaName = TRUE) {
    $name = $this->ensureName($name);
    $fields['mail'] = $fields['mail'] ?? $name . '@' . $this->randomMachineName() . '.example.com';
    $user = $this->createUser([], $fields, FALSE, $coupled);
    if ($sanghaName) {
      $this->updateProfile($user, 'about', ['field_name_sangha' => $name]);
      $user = $this->reloadUser($user);
    }
    $this->setNamedUser($user, $name);
    return $user;
  }

  protected function setNamedUser(UserInterface $user, $name = NULL) {
    $name = $this->ensureName($name);
    if ($name === 'default') {
      $this->user = $user;
    }
    elseif($name === 'me') {
      $this->me === $user;
    }
    $this->users[$name] = $user;
  }

  protected function getOrCreateNamedUser($name = NULL) {
    $name = $this->ensureName($name);

    // In case of 'me' use preset email if possible.
    if ($name === 'me' && !$this->hasUser('me')) {
      $me = $this->loadEntityByProperties('user', ['mail' => $this->getMyEmail()]);
      if ($me) {
        $this->setNamedUser($me, 'me');
      }
      else {
        $this->createNamedUser('me', ['mail' => $this->getMyEmail()]);
      }
    }

    if (!$this->hasUser($name)) {
      $this->createNamedUser($name);
    }
    return $this->getUser($name);
  }

  /**
   * Check if a user is already created with a certain name.
   *
   * @param string|null $name
   *
   * @return bool
   */
  protected function hasUser($name = NULL) {
    $name = $this->ensureName($name);
    return isset($this->users) && is_array($this->users) && isset($this->users[$name]);
  }

  /**
   * Get an already created user.
   *
   * @param string|null $name
   *
   * @return \Drupal\ahs_user_management\Entity\AhsUser
   */
  protected function getUser($name = NULL) {
    $name = $this->ensureName($name);
    $this->assertNotNull($this->users, "There should be an array of stored users.");
    $this->assertTrue($this->hasUser($name), "There should be a stored user named '$name'.");
    $user = $this->users[$name];
    $user = $this->reloadUser($user);
    return $user;
  }

  protected function ensureName($name = NULL) {
    $name = $name ?? 'default';
    $name = strtoupper($name) === 'I' ? 'me' : $name;
    return $name;
  }

  protected function removeNamedUser($name) {
    $name = $this->ensureName();
    if (isset($this->users[$name])) {
      unset($this->users[$name]);
    }
  }

  protected function setUserFullName(UserInterface $user, $name) {
    $nameParts = explode(" ", $name, 2);
    $nameArray = [
      'given' => $nameParts[0],
    ];
    if (count($nameParts) > 1) {
      $nameArray['family'] = $nameParts[1];
    }
    $fields = [
      'field_name' => $nameArray,
    ];
    $this->updateProfile($user, 'about', $fields);
  }

}
