<?php

namespace Drupal\Tests\ahs_tests\Traits;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

/**
 * A trait for working with https requests.
 */
trait MockHttpClientTrait {

  /**
   * History of requests/responses.
   *
   * @var array
   */
  protected $mockClientHistory = [];

  /**
   * Mock client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $mockClient;

  /**
   * Real http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $realClient;

  /**
   * Track the requests that have been expected over the course of a scenario
   *
   * @var int
   */
  protected $requestIndex = 0;


  /**
   * Mocks the http-client.
   */
  protected function setUpSuccessfulMockHttpClient($count = 10) {
    $success = ['200', []];
    $this->mockHttpClient(array_fill(0,$count, $success));
  }

  /**
   * Mocks the http-client.
   */
  protected function mockHttpClient($responseStubs) {
    if (!isset($this->mockClient)) {
      if (!isset($this->realClient)) {
        $this->realClient = \Drupal::httpClient();
      }
      // Convert the simple response data into proper objects.
      $responses = [];
      foreach ($responseStubs as $responseStub) {
        $responses[] = new Response((string) $responseStub[0], [], Json::encode($responseStub[1]));
      }

      // Create a mock and queue responses.
      $mock = new MockHandler($responses);

      $handler_stack = HandlerStack::create($mock);
      $history = Middleware::history($this->mockClientHistory);
      $handler_stack->push($history);
      $this->mockClient = new Client(['handler' => $handler_stack]);
    }
    $this->container->set('http_client', $this->mockClient);
  }

  protected function restoreRealHttpClient() {
    if (isset($this->realClient)) {
      $this->container->set('http_client', $this->realClient);
    }
  }

  protected function getNewHttpRequests() {
    $requests = [];
    for ($i = $this->requestIndex; $i < count($this->mockClientHistory); $i++) {
      $requests[] = $this->mockClientHistory[$i]['request'];
    }
    $this->requestIndex = count($this->mockClientHistory);
    return $requests;
  }

  protected function prettyPrintHttpRequests(array $requests) {
    $output = [];
    foreach($requests as $request) {
      $prettyRequest = [
        'path' => $request->getUri()->getPath(),
      ];
      $body = (string) $request->getBody();
      if ($body) {
        $prettyRequest['body'] = $body;
      }
      $output = $prettyRequest;
    }
    return print_r($output, TRUE);
  }

}
