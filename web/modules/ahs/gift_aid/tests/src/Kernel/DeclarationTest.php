<?php

namespace Drupal\Tests\gift_aid\Kernel\Entity;

use Drupal\gift_aid\Entity\Charity\CharityEntity;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntity;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntityType;
use Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerDefault;
use Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsDefault;
use Drupal\gift_aid\Plugin\Validation\Constraint\StartDateConstraint;
use Drupal\gift_aid_declarer_test\Plugin\GiftAidDeclarer\GiftAidDeclarerTest;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the declaration entity.
 *
 * @coversDefaultClass \Drupal\gift_aid\Entity\Declaration\DeclarationEntity
 */
class DeclarationTest extends EntityKernelTestBase {

  use NodeCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity',
    'node',
    'options',
    'datetime',
    'user',
    'gift_aid',
    'filter',
    'text',
    'gift_aid_declarer_test',
  ];

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * A sample node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * A sample declaration entity (with enduring enabled).
   *
   * @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   */
  protected $declarationEnduring;

  /**
   * A sample declaration entity (no enduring).
   *
   * @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   */
  protected $declarationNoEnduring;

  /**
   * A sample declaration entity (custom declaration type plugin).
   *
   * @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   */
  protected $declarationCustomPlugin;

  /**
   * Current date.
   *
   * @var string
   */
  protected $currentDate;

  /**
   * Start date.
   *
   * @var string
   */
  protected $startDate;

  /**
   * End date.
   *
   * @var string
   */
  protected $endDate;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Date format.
   *
   * @var string
   */
  protected $format = 'html_date';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Install schemas and configs.
    $this->installEntitySchema('gift_aid_charity');
    $this->installEntitySchema('gift_aid_declaration');
    $this->installConfig(['gift_aid']);

    // Install required default configuration for filter module.
    $this->installConfig(['system', 'filter']);

    // Create sample user.
    $user = $this->createUser([
      'mail' => 'declarationtest@example.com',
    ], ['administer declaration entities']);
    $this->user = $this->reloadEntity($user);
    $this->container->get('current_user')->setAccount($user);

    // Create sample node.
    $node = $this->createNode();
    $this->node = $this->reloadEntity($node);

    $this->dateFormatter = \Drupal::service('date.formatter');

    // Current date.
    $this->currentDate = $this->dateFormatter->format(time(), $this->format);

    // Start date.
    $this->startDate = $this->dateFormatter->format(strtotime('today +3 day'), $this->format);

    // End date.
    $this->endDate = $this->dateFormatter->format(strtotime('today +1 month'), $this->format);
  }

  /**
   * Tests CRUD operations.
   *
   * @covers ::getType
   * @covers ::getCharityId
   * @covers ::getDeclaredDate
   * @covers ::getStartDate
   * @covers ::getEndDate
   * @covers ::getRevokedDate
   * @covers ::getDeclarationStatus
   * @covers ::isEnduring
   * @covers ::getDeclarer
   * @covers ::getDonations
   * @covers ::setCharity
   * @covers ::setDeclaredDate
   * @covers ::setStartDate
   * @covers ::setEndDate
   * @covers ::setRevokedDate
   * @covers ::setDeclarationStatus
   * @covers ::setEnduring
   * @covers ::setDeclarer
   * @covers ::setDonations
   */
  public function testCrud() {
    $this->createTests();
    $this->loadTests();
    $this->updateTests();
    $this->deleteTests();
  }

  /**
   * Create charity.
   *
   * @param int $num
   *   Unique suffix.
   *
   * @return \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   *   A new Charity entity.
   */
  public function createCharity($num) {
    $charity = CharityEntity::create([
      'name' => 'Charity name ' . $num,
      'registered_name' => 'Charity registered name ' . $num,
      'registration_number' => 'Charity registration number ' . $num,
      'registered_address' => 'Charity registered address ' . $num,
    ]);
    $charity->save();
    return CharityEntity::load($charity->id());
  }

  /**
   * Create declaration_type.
   *
   * @param int $num
   *   Unique suffix.
   * @param string $declarer_plugin
   *   The declarer plugin name.
   * @param string $donations_plugin
   *   The donations plugin name.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityTypeInterface
   *   A new DeclarationEntityType entity.
   */
  public function createDeclarationType($num, $declarer_plugin = 'gift_aid_declarer_default', $donations_plugin = 'gift_aid_donations_default') {
    $declaration_type = DeclarationEntityType::create([
      'id' => 'declaration_type_' . $num,
      'label' => 'Declaration type ' . $num,
      'declarer_plugin' => $declarer_plugin,
      'donations_plugin' => $donations_plugin,
    ]);
    $declaration_type->save();
    return DeclarationEntityType::load($declaration_type->id());
  }

  /**
   * Test the creation of declaration.
   */
  public function createTests() {
    // Declaration enduring enabled.
    $charity = $this->createCharity(1);
    $declaration_type = $this->createDeclarationType(1);
    $declaration = DeclarationEntity::create([
      'type' => $declaration_type->id(),
      'charity' => $charity->id(),
      'start_date' => NULL,
      'end_date' => $this->endDate,
      'declaration_status' => DeclarationEntity::DECLARATION_ACTIVE,
      'enduring' => TRUE,
      'declarer' => $this->user->id(),
    ]);
    $violations = $declaration->validate();
    $this->assertCount(1, $violations);
    $this->assertInstanceOf(StartDateConstraint::class, $violations[0]->getConstraint());
    $declaration->setStartDate($this->startDate);
    $violations = $declaration->validate();
    $this->assertCount(0, $violations);
    $declaration->save();
    $declaration = DeclarationEntity::load($declaration->id());

    $this->assertTrue($declaration instanceof DeclarationEntity, 'The declaration with enduring is created.');
    $this->assertEquals($declaration_type->id(), $declaration->getType(), 'Testing declaration type.');
    $this->assertEquals($charity->id(), $declaration->getCharityId(), 'Testing declaration charity.');
    $this->assertEquals($this->currentDate, $declaration->getDeclaredDate(), 'Testing declared date.');
    $this->assertEquals($this->startDate, $declaration->getStartDate(), 'Testing start date.');
    $this->assertEquals($this->endDate, $declaration->getEndDate(), 'Testing end date.');
    $this->assertEquals(NULL, $declaration->getRevokedDate(), 'Testing revoked date.');
    $this->assertEquals(DeclarationEntity::DECLARATION_ACTIVE, $declaration->getDeclarationStatus(), 'Testing declaration status.');
    $this->assertEquals(TRUE, $declaration->isEnduring(), 'Testing enduring.');
    $this->assertEquals($this->user, $declaration->getDeclarer(), 'Testing declarer.');
    $this->assertEquals($this->user->getDisplayName(), $declaration->getDeclarerLegalName(), 'Testing declarer legal name.');
    $this->assertEquals('', $declaration->getDeclarerTaxAddress(), 'Testing declarer Tax Address.');
    $this->assertEquals($this->user->getEmail(), $declaration->getDeclarerMail(), 'Testing declarer mail.');
    $this->assertEquals($this->currentDate, \Drupal::service('date.formatter')->format($declaration->getCreatedTime(), 'html_date'), 'Testing created time.');
    $this->assertEquals($this->user, $declaration->getOwner(), 'Testing owner.');
    $this->declarationEnduring = $declaration;

    // Declaration with no enduring.
    $charity = $this->createCharity(2);
    $declaration_type = $this->createDeclarationType(2);
    $declaration = DeclarationEntity::create([
      'type' => $declaration_type->id(),
      'charity' => $charity->id(),
      'declaration_status' => DeclarationEntity::DECLARATION_ACTIVE,
      'enduring' => FALSE,
      'declarer' => $this->user->id(),
      'donations' => $this->node->id(),
    ]);
    $violations = $declaration->validate();
    $this->assertCount(0, $violations);
    $declaration->save();
    $declaration = DeclarationEntity::load($declaration->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The declaration with no enduring is created.');
    $this->assertEquals($this->node, $declaration->getDonations(), 'Testing donations.');
    $this->declarationNoEnduring = $declaration;

    // Enduring declaration, a custom plugin uses node instead of user.
    $charity = $this->createCharity(3);
    $declaration_type = $this->createDeclarationType(3, 'gift_aid_declarer_test');
    $declaration = DeclarationEntity::create([
      'type' => $declaration_type->id(),
      'charity' => $charity->id(),
      'start_date' => $this->startDate,
      'end_date' => $this->endDate,
      'declaration_status' => DeclarationEntity::DECLARATION_ACTIVE,
      'enduring' => TRUE,
      'declarer' => $this->node->id(),
    ]);
    $violations = $declaration->validate();
    $this->assertCount(0, $violations);
    $declaration->save();
    $declaration = DeclarationEntity::load($declaration->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The declaration with custom plugin is created.');
    $this->assertEquals($declaration_type->id(), $declaration->getType(), 'Testing declaration type.');
    $this->assertEquals($this->node, $declaration->getDeclarer(), 'Testing declarer.');
    $this->declarationCustomPlugin = $declaration;
  }

  /**
   * Tests the loading of declaration.
   */
  public function loadTests() {
    // Declaration with enduring enabled.
    $declaration = DeclarationEntity::load($this->declarationEnduring->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The declaration with enduring is loaded.');
    $this->assertTrue($declaration->getDeclarerPlugin() instanceof GiftAidDeclarerDefault, 'Loaded declaration with plugin declarer default.');
    $this->assertTrue($declaration->getDonationsPlugin() instanceof GiftAidDonationsDefault, 'Loaded declaration with plugin donations default.');
    $this->assertEquals($this->currentDate, $declaration->getDeclaredDate(), 'Loaded declared date.');
    $this->assertEquals($this->startDate, $declaration->getStartDate(), 'Loaded start date.');
    $this->assertEquals($this->endDate, $declaration->getEndDate(), 'Loaded end date.');
    $this->assertEquals(NULL, $declaration->getRevokedDate(), 'Loaded revoked date.');
    $this->assertEquals(DeclarationEntity::DECLARATION_ACTIVE, $declaration->getDeclarationStatus(), 'Loaded declaration status.');
    $this->assertEquals(TRUE, $declaration->isEnduring(), 'Loaded enduring.');
    $this->assertEquals($this->user, $declaration->getDeclarer(), 'Loaded declarer.');
    $this->assertEquals($this->user->getDisplayName(), $declaration->getDeclarerLegalName(), 'Loaded declarer legal name.');
    $this->assertEquals('', $declaration->getDeclarerTaxAddress(), 'Loaded declarer Tax Address.');
    $this->assertEquals($this->user->getEmail(), $declaration->getDeclarerMail(), 'Loaded declarer mail.');
    $this->assertEquals($this->currentDate, \Drupal::service('date.formatter')->format($declaration->getCreatedTime(), 'html_date'), 'Loaded created time.');
    $this->assertEquals($this->user, $declaration->getOwner(), 'Loaded owner.');

    $this->declarationEnduring = $declaration;

    // Declaration with no enduring.
    $declaration = DeclarationEntity::load($this->declarationNoEnduring->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The declaration with no enduring is loaded.');
    $this->assertTrue($declaration->getDeclarerPlugin() instanceof GiftAidDeclarerDefault, 'Loaded declaration with plugin declarer default.');
    $this->assertTrue($declaration->getDonationsPlugin() instanceof GiftAidDonationsDefault, 'Loaded declaration with plugin donations default.');
    $this->assertEquals(FALSE, $declaration->isEnduring(), 'Loaded enduring.');
    $this->assertEquals($this->node, $declaration->getDonations(), 'Loaded donations.');
    $this->declarationNoEnduring = $declaration;

    // Enduring declaration, a custom plugin uses node instead of user.
    $declaration = DeclarationEntity::load($this->declarationCustomPlugin->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The declaration entity with custom plugin is created.');
    $this->assertTrue($declaration->getDeclarerPlugin() instanceof GiftAidDeclarerTest, 'Loaded declaration with plugin declarer test.');
    $this->assertEquals($this->node, $declaration->getDeclarer(), 'Loaded declarer.');
    $this->declarationCustomPlugin = $declaration;
  }

  /**
   * Tests the update of declaration.
   */
  public function updateTests() {
    // New declared date.
    $new_declared_date = $this->dateFormatter->format(strtotime('today +1 day'), $this->format);
    // New revoked date.
    $new_revoked_date = $this->dateFormatter->format(strtotime('today +1 week'), $this->format);

    $user = $this->createUser([], ['administer declaration entities']);
    $user = $this->reloadEntity($user);

    // Declaration with enduring enabled.
    $charity = $this->createCharity(11);
    $declaration = $this->declarationEnduring->createDuplicate();
    $declaration->save();
    $declaration = DeclarationEntity::load($declaration->id());

    $this->declarationEnduring->setCharity($charity);
    $this->declarationEnduring->setDeclaredDate($new_declared_date);
    $this->declarationEnduring->setStartDate(NULL);
    $this->declarationEnduring->setEndDate(NULL);
    $this->declarationEnduring->setRevokedDate($new_revoked_date);
    $this->declarationEnduring->setDeclarationStatus(DeclarationEntity::DECLARATION_REVOKED);
    $this->declarationEnduring->setDeclarer($user);
    $this->declarationEnduring->setOwner($user);

    $violations = $this->declarationEnduring->validate();
    $this->assertCount(1, $violations);
    $this->assertInstanceOf(StartDateConstraint::class, $violations[0]->getConstraint());
    $this->declarationEnduring->setEnduring(FALSE);
    $violations = $this->declarationEnduring->validate();
    $this->assertCount(0, $violations);
    $this->declarationEnduring->save();
    $this->declarationEnduring = DeclarationEntity::load($this->declarationEnduring->id());

    $this->assertNotEquals($this->declarationEnduring->getDeclaredDate(), $declaration->getDeclaredDate(), 'Declared date updated.');
    $this->assertNotEquals($this->declarationEnduring->getStartDate(), $declaration->getStartDate(), 'Start date updated.');
    $this->assertNotEquals($this->declarationEnduring->getEndDate(), $declaration->getEndDate(), 'End date updated.');
    $this->assertNotEquals($this->declarationEnduring->getRevokedDate(), $declaration->getRevokedDate(), 'Revoked date updated.');
    $this->assertNotEquals($this->declarationEnduring->getDeclarationStatus(), $declaration->getDeclarationStatus(), 'Declaration status updated.');
    $this->assertNotEquals($this->declarationEnduring->isEnduring(), $declaration->isEnduring(), 'Enduring updated.');
    $this->assertNotEquals($this->declarationEnduring->getDeclarer(), $declaration->getDeclarer(), 'Declarer updated.');
    $this->assertNotEquals($this->declarationEnduring->getOwner(), $declaration->getOwner(), 'Owner updated.');

    // Declaration with no enduring.
    $declaration = $this->declarationNoEnduring->createDuplicate();
    $declaration->save();
    $declaration = DeclarationEntity::load($declaration->id());
    $this->declarationNoEnduring->setEnduring(TRUE);
    $this->declarationNoEnduring->setStartDate($this->startDate);
    $this->declarationNoEnduring->setEndDate($this->endDate);
    $this->declarationNoEnduring->save();
    $this->declarationNoEnduring = DeclarationEntity::load($this->declarationNoEnduring->id());

    $this->assertNotEquals($this->declarationNoEnduring->isEnduring(), $declaration->isEnduring(), 'Enduring updated.');
    $this->assertNotEquals($this->declarationNoEnduring->getStartDate(), $declaration->getStartDate(), 'Start date updated.');
    $this->assertNotEquals($this->declarationNoEnduring->getEndDate(), $declaration->getEndDate(), 'End date updated.');

    // Enduring declaration, a custom plugin uses node instead of user.
    $declaration = $this->declarationCustomPlugin->createDuplicate();
    $declaration->save();
    $declaration = DeclarationEntity::load($declaration->id());
    $node = $this->createNode([
      'uid' => $user->id(),
    ]);
    $node = $this->reloadEntity($node);
    $this->declarationCustomPlugin->setDeclarer($node);

    $this->declarationCustomPlugin->getDeclarer()->setTitle($declaration->getDeclarer()->getTitle() . ' (updated)');
    $this->declarationCustomPlugin->getDeclarer()->getOwner()->setEmail($declaration->getDeclarerMail() . ' (updated)');
    $this->declarationCustomPlugin->save();
    $this->declarationCustomPlugin = DeclarationEntity::load($this->declarationCustomPlugin->id());

    $this->assertNotEquals($this->declarationCustomPlugin->getDeclarer(), $declaration->getDeclarer(), 'Declarer updated.');
    $this->assertNotEquals($this->declarationCustomPlugin->getDeclarerLegalName(), $declaration->getDeclarerLegalName(), 'Declarer legal name updated.');
    $this->assertNotEquals($this->declarationCustomPlugin->getDeclarerTaxAddress(), $declaration->getDeclarerTaxAddress(), 'Declarer Tax Address updated.');
    $this->assertNotEquals($this->declarationCustomPlugin->getDeclarerMail(), $declaration->getDeclarerMail(), 'Declarer mail updated.');
  }

  /**
   * Tests the deleting of declaration.
   */
  public function deleteTests() {
    // Declaration with enduring enabled.
    $declaration = DeclarationEntity::load($this->declarationEnduring->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The loaded entity is a declaration.');
    $declaration->delete();
    $declaration = DeclarationEntity::load($this->declarationEnduring->id());
    $this->assertTrue(is_null($declaration), 'The declaration deleted.');

    // Declaration with no enduring.
    $declaration = DeclarationEntity::load($this->declarationNoEnduring->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The loaded entity is a declaration.');
    $declaration->delete();
    $declaration = DeclarationEntity::load($this->declarationNoEnduring->id());
    $this->assertTrue(is_null($declaration), 'The declaration deleted.');

    // Enduring declaration, a custom plugin uses node instead of user.
    $declaration = DeclarationEntity::load($this->declarationCustomPlugin->id());
    $this->assertTrue($declaration instanceof DeclarationEntity, 'The loaded entity is a declaration.');
    $declaration->delete();
    $declaration = DeclarationEntity::load($this->declarationCustomPlugin->id());
    $this->assertTrue(is_null($declaration), 'The declaration deleted.');
  }

}
