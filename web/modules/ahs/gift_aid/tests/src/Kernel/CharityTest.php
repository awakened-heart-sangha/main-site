<?php

namespace Drupal\Tests\gift_aid\Kernel\Entity;

use Drupal\gift_aid\Entity\Charity\CharityEntity;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the charity entity.
 *
 * @coversDefaultClass \Drupal\gift_aid\Entity\Charity\CharityEntity
 */
class CharityTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity',
    'options',
    'gift_aid',
  ];

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * A sample charity.
   *
   * @var \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   */
  protected $charity;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Install schemas and configs.
    $this->installEntitySchema('gift_aid_charity');
    $this->installConfig(['gift_aid']);

    // Create sample user.
    $user = $this->createUser([], ['administer gift aid charity entities']);
    $this->user = $this->reloadEntity($user);
    $this->container->get('current_user')->setAccount($user);
  }

  /**
   * Tests CRUD operations.
   *
   * @covers ::getName
   * @covers ::getRegisteredName
   * @covers ::getRegistrationNumber
   * @covers ::getRegisteredAddress
   * @covers ::setName
   * @covers ::setRegisteredName
   * @covers ::setRegistrationNumber
   * @covers ::setRegisteredAddress
   */
  public function testCrud() {
    $this->createTests();
    $this->loadTests();
    $this->updateTests();
    $this->deleteTests();
  }

  /**
   * Tests the creation of charity.
   */
  public function createTests() {
    $charity = CharityEntity::create([
      'name' => 'Name 1',
      'registered_name' => 'Registered name 1',
      'registration_number' => 'Registration number 1',
      'registered_address' => 'Address 1',
      'user_id' => $this->user->id(),
    ]);
    $charity->save();
    $this->assertTrue($charity instanceof CharityEntity, 'The newly created entity is a charity.');
    $this->assertEquals('Name 1', $charity->getName(), 'Testing charity name.');
    $this->assertEquals('Registered name 1', $charity->getRegisteredName(), 'Testing charity registered name.');
    $this->assertEquals('Registration number 1', $charity->getRegistrationNumber(), 'Testing registration number.');
    $this->assertEquals('Address 1', $charity->getRegisteredAddress(), 'Testing registration address.');
    $this->charity = $charity;
  }

  /**
   * Tests the loading of charity.
   */
  public function loadTests() {
    $charity = CharityEntity::load($this->charity->id());
    $this->assertTrue($charity instanceof CharityEntity, 'The loaded entity is a charity.');
    $this->assertEquals('Name 1', $charity->getName(), 'Loaded charity name.');
    $this->assertEquals('Registered name 1', $charity->getRegisteredName(), 'Loaded charity registered name.');
    $this->assertEquals('Registration number 1', $charity->getRegistrationNumber(), 'Loaded registration number.');
    $this->assertEquals('Address 1', $charity->getRegisteredAddress(), 'Loaded registration address.');
  }

  /**
   * Tests the creation of charity.
   */
  public function updateTests() {
    $charity = $this->charity->createDuplicate();
    $charity->save();
    $charity = CharityEntity::load($charity->id());

    $this->charity->setName('My name');
    $this->charity->setRegisteredName('My registered name');
    $this->charity->setRegistrationNumber('My registration number');
    $this->charity->setRegisteredAddress('My address');
    $this->charity->save();
    $this->charity = CharityEntity::load($this->charity->id());

    $this->assertNotEquals($this->charity->getName(), $charity->getName(), 'Charity name updated.');
    $this->assertNotEquals($this->charity->getRegisteredName(), $charity->getRegisteredName(), 'Registered name updated.');
    $this->assertNotEquals($this->charity->getRegistrationNumber(), $charity->getRegistrationNumber(), 'Registration number updated.');
    $this->assertNotEquals($this->charity->getRegisteredAddress(), $charity->getRegisteredAddress(), 'Registered address updated.');
  }

  /**
   * Tests the deleting of charity.
   */
  public function deleteTests() {
    $charity = CharityEntity::load($this->charity->id());
    $this->assertTrue($charity instanceof CharityEntity, 'The loaded entity is a charity.');
    $charity->delete();
    $charity = CharityEntity::load($this->charity->id());
    $this->assertTrue(is_null($charity), 'The charity deleted.');
  }

}
