<?php

namespace Drupal\gift_aid_declarer_test\Plugin\GiftAidDeclarer;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerDefault;

/**
 * Provides a Test Gift Aid Declarer.
 *
 * The plugin use the custom Contact Profile.
 *
 * @GiftAidDeclarer(
 *   id = "gift_aid_declarer_test",
 *   label = @Translation("Gift Aid Declarer Test"),
 * )
 */
class GiftAidDeclarerTest extends GiftAidDeclarerDefault implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];

    // Configure the Declarer field.
    $fields['declarer'] = clone $base_field_definitions['declarer'];
    $fields['declarer']->setSettings([
      'target_type' => 'node',
      'handler' => 'default:node',
      'handler_settings' => [
        'include_anonymous' => FALSE,
        'target_bundles' => NULL,
        'filter' => [
          'type' => '_none',
        ],
        'sort' => [
          'field' => '_none',
        ],
        'auto_create' => FALSE,
      ],
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getLegalName() {
    /** @var \Drupal\node\Entity\Node $declarer */
    $declarer = $this->getDeclarer();
    return $declarer->getTitle();
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxAddress() {
    /** @var \Drupal\node\Entity\Node $declarer */
    $declarer = $this->getDeclarer();
    return 'The tax address of declarer ' . $declarer->getTitle();
  }

  /**
   * {@inheritdoc}
   */
  public function getMail() {
    /** @var \Drupal\node\Entity\Node $declarer */
    $declarer = $this->getDeclarer();
    return $declarer->getOwner()->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function viewDeclarer() {
    $declaration = $this->getDeclaration();
    $declarer = $this->getDeclarer();

    // No return when user doesn't have an access to view declarer.
    if (FALSE === $declarer->access('view')) {
      return NULL;
    }

    // Build render wrapper.
    $output = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'declarer-view',
          'declarer-view--declaration--' . $declaration->bundle(),
          'declarer-view--plugin--' . $this->getPluginId(),
        ],
      ],
      '#cache' => [
        'tags' => $declarer->getCacheTags(),
        'contexts' => $declarer->getCacheContexts(),
        'max-age' => $declarer->getCacheMaxAge(),
      ],
    ];

    $viewBuilder = $this->entityTypeManager->getViewBuilder($declarer->getEntityTypeId());

    $output['fields'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        ['#markup' => $this->t('Declarer Name: @name', ['@name' => $this->getLegalName()])],
      ],
    ];

    $output['entity'] = $viewBuilder->view($declarer, 'compact');

    return $output;
  }

}
