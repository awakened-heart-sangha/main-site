<?php

namespace Drupal\gift_aid\Charity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of Charity entities.
 *
 * @ingroup gift_aid
 */
class CharityEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Charity ID');
    $header['name'] = $this->t('Name');
    $header['registered_name'] = $this->t('Registered Name');
    $header['registration_number'] = $this->t('Registration Number');
    $header['registered_address'] = $this->t('Registered Address');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\gift_aid\Entity\Charity\CharityEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = $entity->getName();
    $row['registered_name'] = $entity->getRegisteredName();
    $row['registration_number'] = $entity->getRegistrationNumber();
    $row['registered_address'] = $entity->getRegisteredAddress();
    return $row + parent::buildRow($entity);
  }

}
