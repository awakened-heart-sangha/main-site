<?php

namespace Drupal\gift_aid\Charity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for charity.
 */
class CharityEntityTranslationHandler extends ContentTranslationHandler {

}
