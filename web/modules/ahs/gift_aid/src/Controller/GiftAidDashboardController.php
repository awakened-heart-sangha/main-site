<?php

namespace Drupal\gift_aid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\system\SystemManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GiftAidDashboardController.
 *
 * Controller for the '/admin/gift-aid' path.
 * Provides Gift Aid section in drupal configurations.
 */
class GiftAidDashboardController extends ControllerBase {

  /**
   * System Manager Service.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * Constructs a new SystemController.
   *
   * @param \Drupal\system\SystemManager $systemManager
   *   System manager service.
   */
  public function __construct(SystemManager $systemManager) {
    $this->systemManager = $systemManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('system.manager')
    );
  }

  /**
   * Page callback.
   *
   * @return array
   *   Render array.
   */
  public function pageAction() {
    return $this->systemManager->getBlockContents();
  }

}
