<?php

namespace Drupal\gift_aid\Entity\Charity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Charity entity.
 *
 * @ingroup gift_aid
 *
 * @ContentEntityType(
 *   id = "gift_aid_charity",
 *   label = @Translation("Charity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gift_aid\Charity\CharityEntityListBuilder",
 *     "views_data" = "Drupal\gift_aid\Entity\Charity\CharityEntityViewsData",
 *     "translation" = "Drupal\gift_aid\Charity\CharityEntityTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\gift_aid\Form\Charity\CharityEntityForm",
 *       "add" = "Drupal\gift_aid\Form\Charity\CharityEntityForm",
 *       "edit" = "Drupal\gift_aid\Form\Charity\CharityEntityForm",
 *       "delete" = "Drupal\gift_aid\Form\Charity\CharityEntityDeleteForm",
 *     },
 *     "access" = "Drupal\gift_aid\Charity\CharityEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gift_aid\Charity\CharityEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gift_aid_charity",
 *   data_table = "gift_aid_charity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer gift aid charity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/gift-aid/charity/add",
 *     "edit-form" = "/admin/gift-aid/charity/{gift_aid_charity}/edit",
 *     "delete-form" = "/admin/gift-aid/charity/{gift_aid_charity}/delete",
 *     "collection" = "/admin/gift-aid/charity",
 *   },
 *   field_ui_base_route = "gift_aid_charity.settings"
 * )
 */
class CharityEntity extends ContentEntityBase implements CharityEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegisteredName() {
    return $this->get('registered_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegisteredName($registered_name) {
    $this->set('registered_name', $registered_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegistrationNumber() {
    return $this->get('registration_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegistrationNumber($registration_number) {
    $this->set('registration_number', $registration_number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegisteredAddress() {
    return $this->get('registered_address')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegisteredAddress($registered_address) {
    $this->set('registered_address', $registered_address);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Display Name field.
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The Name of the Charity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Registered Name field.
    $fields['registered_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Registered Name'))
      ->setDescription(t('The Registered Name of the Charity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Registration Number field.
    $fields['registration_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Registration Number'))
      ->setDescription(t('The Registration Number of the Charity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Registered Address field.
    $fields['registered_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Registered Address'))
      ->setDescription(t('The Registered Address of the Charity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Charity Author field.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Charity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
