<?php

namespace Drupal\gift_aid\Entity\Charity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Charity entities.
 */
class CharityEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }

}
