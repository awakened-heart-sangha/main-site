<?php

namespace Drupal\gift_aid\Entity\Charity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Charity entities.
 *
 * @ingroup gift_aid
 */
interface CharityEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Charity Name.
   *
   * @return string
   *   Name of the Charity.
   */
  public function getName();

  /**
   * Sets the Charity Name.
   *
   * @param string $name
   *   The Charity Name.
   *
   * @return \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   *   The called Charity entity.
   */
  public function setName($name);

  /**
   * Gets the Charity Registered Name.
   *
   * @return string
   *   Registered Name of the Charity.
   */
  public function getRegisteredName();

  /**
   * Sets the Charity Registered Name.
   *
   * @param string $registered_name
   *   The Charity Registered Name.
   *
   * @return \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   *   The called Charity entity.
   */
  public function setRegisteredName($registered_name);

  /**
   * Gets the Charity Registration Number.
   *
   * @return string
   *   Registration Number of the Charity.
   */
  public function getRegistrationNumber();

  /**
   * Sets the Charity Registration Number.
   *
   * @param string $registration_number
   *   The Charity Registration Number.
   *
   * @return \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   *   The called Charity entity.
   */
  public function setRegistrationNumber($registration_number);

  /**
   * Gets the Charity Registered Address.
   *
   * @return string
   *   Registered Address of the Charity.
   */
  public function getRegisteredAddress();

  /**
   * Sets the Charity Registered Address.
   *
   * @param string $registered_address
   *   The Charity Registered Address.
   *
   * @return \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   *   The called Charity entity.
   */
  public function setRegisteredAddress($registered_address);

  /**
   * Gets the Charity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Charity.
   */
  public function getCreatedTime();

  /**
   * Sets the Charity creation timestamp.
   *
   * @param int $timestamp
   *   The Charity creation timestamp.
   *
   * @return \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   *   The called Charity entity.
   */
  public function setCreatedTime($timestamp);

}
