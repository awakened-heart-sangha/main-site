<?php

namespace Drupal\gift_aid\Entity\Declaration;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Declaration entities.
 */
class DeclarationEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Override the Declarer view field for the Declaration entity.
    $data['gift_aid_declaration_field_data']['declarer'] = [
      'title' => $this->t('Declarer'),
      'help' => $this->t('View handler provided by selected Gift Aid Declarer plugin in the Declaration Entity type.'),
      'field' => [
        'title' => $this->t('Declarer'),
        'help' => $this->t('Declarer field provided by selected Gift Aid Declarer plugin.'),
        'id' => 'gift_aid_declarer_views_field',
      ],
    ];

    // Override the Donations view field for the Declaration entity.
    $data['gift_aid_declaration_field_data']['donations'] = [
      'title' => $this->t('Donations'),
      'help' => $this->t('View handler provided by selected Gift Aid Donations plugin in the Declaration Entity type.'),
      'field' => [
        'title' => $this->t('Donations'),
        'help' => $this->t('Donations field provided by selected Gift Aid Donations plugin.'),
        'id' => 'gift_aid_donations_views_field',
      ],
    ];

    // @todo set select element for the Declaration status field.
    $data['gift_aid_declaration_field_data']['declaration_status']['filter']['id'] = 'gift_aid_declaration_status_filter';

    return $data;
  }

}
