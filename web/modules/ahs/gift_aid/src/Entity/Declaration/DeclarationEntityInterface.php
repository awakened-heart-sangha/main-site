<?php

namespace Drupal\gift_aid\Entity\Declaration;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\gift_aid\Entity\Charity\CharityEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Declaration entities.
 *
 * @ingroup gift_aid
 */
interface DeclarationEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Declaration status constants.
  const DECLARATION_ACTIVE = 'active';
  const DECLARATION_END = 'end';
  const DECLARATION_REVOKED = 'revoked';

  /**
   * Gets the Charity of the Declaration.
   *
   * @return \Drupal\gift_aid\Entity\Charity\CharityEntityInterface
   *   The Charity entity.
   */
  public function getCharity();

  /**
   * Sets the Charity entity.
   *
   * @param \Drupal\gift_aid\Entity\Charity\CharityEntityInterface $charity
   *   Charity entity.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setCharity(CharityEntityInterface $charity);

  /**
   * Gets the Charity ID of the Declaration.
   *
   * @return int
   *   The Charity entity ID.
   */
  public function getCharityId();

  /**
   * Sets the Charity entity ID of the declaration.
   *
   * @param int $charity_id
   *   Charity entity ID.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setCharityId($charity_id);

  /**
   * Gets the Declared Date.
   *
   * @return string
   *   Declared Date in format YYYY-MM-DD.
   */
  public function getDeclaredDate();

  /**
   * Sets the Declared Date.
   *
   * @param string $date
   *   The Declared Date in format YYYY-MM-DD.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setDeclaredDate($date);

  /**
   * Gets the Start Date.
   *
   * @return string
   *   Start Date in format YYYY-MM-DD.
   */
  public function getStartDate();

  /**
   * Sets the Start Date.
   *
   * @param string $date
   *   The Start Date in format YYYY-MM-DD.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setStartDate($date);

  /**
   * Gets the End Date.
   *
   * @return string
   *   End Date in format YYYY-MM-DD.
   */
  public function getEndDate();

  /**
   * Sets the End Date.
   *
   * @param string $date
   *   The End Date in format YYYY-MM-DD.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setEndDate($date);

  /**
   * Gets the Revoke Date.
   *
   * @return string
   *   Revoke Date in format YYYY-MM-DD.
   */
  public function getRevokedDate();

  /**
   * Sets the Revoke Date.
   *
   * @param string $date
   *   The Revoke Date in format YYYY-MM-DD.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setRevokedDate($date);

  /**
   * Gets the Status.
   *
   * @param bool $formatted
   *   Set TRUE for display formatted Declaration status.
   *
   * @return string
   *   The Status name.
   */
  public function getDeclarationStatus($formatted = FALSE);

  /**
   * Sets the Status.
   *
   * @param string $status
   *   The Status name.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setDeclarationStatus($status);

  /**
   * Returns Enduring status.
   *
   * @return bool
   *   TRUE if the declaration is enduring.
   */
  public function isEnduring();

  /**
   * Set Enduring status.
   *
   * @param bool $status
   *   Enduring status.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setEnduring($status);

  /**
   * Gets the Declaration Type.
   *
   * @return string
   *   The Declaration type.
   */
  public function getType();

  /**
   * Gets the Declaration creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Declaration.
   */
  public function getCreatedTime();

  /**
   * Sets the Declaration creation timestamp.
   *
   * @param int $timestamp
   *   The Declaration creation timestamp.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   The called Declaration entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets Declarer.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Declarer entity.
   */
  public function getDeclarer();

  /**
   * Set Declarer.
   *
   * @param \Drupal\Core\Entity\EntityInterface $declarer
   *   Declarer entity.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface|null
   *   The Declaration or NULL on fail.
   */
  public function setDeclarer(EntityInterface $declarer);

  /**
   * Returns Declaration statuses.
   *
   * @param bool $with_labels
   *   Set TRUE for return statues in associative array with labels.
   *
   * @return array
   *   Declaration statuses.
   */
  public static function getDeclarationStatuses($with_labels = FALSE);

  /**
   * Gets Declarer Legal Name.
   *
   * @return string|null
   *   Declarer Legal Name or NULL on fail.
   */
  public function getDeclarerLegalName();

  /**
   * Gets Declarer Email.
   *
   * @return string|null
   *   Declarer Email or NULL on fail.
   */
  public function getDeclarerMail();

  /**
   * Gets Declarer Tax Address.
   *
   * @return string|null
   *   Declarer Tax Address or NULL on fail.
   */
  public function getDeclarerTaxAddress();

  /**
   * Returns Declarer plugin related to the bundle.
   *
   * @param string $bundle
   *   Bundle ID.
   *
   * @return \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerInterface|null
   *   Declarer plugin or NULL on error.
   */
  public static function instantiateDeclarerPlugin($bundle);

  /**
   * Returns declarer plugin object.
   *
   * @return \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerInterface|null
   *   Declarer plugin object or NULL.
   */
  public function getDeclarerPlugin();

  /**
   * Returns declarer information.
   *
   * @return array
   *   Render array of the Declarer.
   */
  public function viewDeclarer();

  /**
   * Gets Donations.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Donations entity.
   */
  public function getDonations();

  /**
   * Set Donations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $donations
   *   Donations entity.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface|null
   *   The Declaration or NULL on fail.
   */
  public function setDonations(EntityInterface $donations);

  /**
   * Gets Donations Legal Name.
   *
   * @return string|null
   *   Donations Legal Name or NULL on fail.
   */
  public function getDonationsLegalName();

  /**
   * Gets Donations Tax Address.
   *
   * @return string|null
   *   Donations Tax Address or NULL on fail.
   */
  public function getDonationsTaxAddress();

  /**
   * Returns Donations plugin related to the bundle.
   *
   * @param string $bundle
   *   Bundle ID.
   *
   * @return \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsInterface|null
   *   Donations plugin or NULL on error.
   */
  public static function instantiateDonationsPlugin($bundle);

  /**
   * Returns donations plugin object.
   *
   * @return \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsInterface|null
   *   Donations plugin object or NULL.
   */
  public function getDonationsPlugin();

  /**
   * Returns donations information.
   *
   * @return array
   *   Render array of the Donations.
   */
  public function viewDonations();

}
