<?php

namespace Drupal\gift_aid\Entity\Declaration;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Declaration type entity.
 *
 * @ConfigEntityType(
 *   id = "gift_aid_declaration_type",
 *   label = @Translation("Declaration type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gift_aid\Declaration\DeclarationEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\gift_aid\Form\Declaration\DeclarationEntityTypeForm",
 *       "edit" = "Drupal\gift_aid\Form\Declaration\DeclarationEntityTypeForm",
 *       "delete" = "Drupal\gift_aid\Form\Declaration\DeclarationEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\gift_aid\Declaration\DeclarationEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "gift_aid_declaration_type",
 *   admin_permission = "gift aid administer configurations",
 *   bundle_of = "gift_aid_declaration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/gift-aid/declaration-type/{gift_aid_declaration_type}",
 *     "add-form" = "/admin/gift-aid/declaration-type/add",
 *     "edit-form" = "/admin/gift-aid/declaration-type/{gift_aid_declaration_type}/edit",
 *     "delete-form" = "/admin/gift-aid/declaration-type/{gift_aid_declaration_type}/delete",
 *     "collection" = "/admin/gift-aid/declaration-type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "declarer_plugin",
 *     "donations_plugin",
 *   }
 * )
 */
class DeclarationEntityType extends ConfigEntityBundleBase implements DeclarationEntityTypeInterface {

  /**
   * The Declaration type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Declaration type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Gift Aid Declarer plugin.
   *
   * @var string
   */
  protected $declarer_plugin;

  /**
   * The Gift Aid Declarer plugin.
   *
   * @var string
   */
  protected $donations_plugin;

  /**
   * {@inheritdoc}
   */
  public function getDeclarerPlugin() {
    return $this->declarer_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeclarerPlugin($plugin_id) {
    $this->declarer_plugin = $plugin_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDonationsPlugin() {
    return $this->donations_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setDonationsPlugin($plugin_id) {
    $this->donations_plugin = $plugin_id;
    return $this;
  }

}
