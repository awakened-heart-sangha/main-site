<?php

namespace Drupal\gift_aid\Entity\Declaration;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\gift_aid\Entity\Charity\CharityEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Declaration entity.
 *
 * @ingroup gift_aid
 *
 * @ContentEntityType(
 *   id = "gift_aid_declaration",
 *   label = @Translation("Declaration"),
 *   bundle_label = @Translation("Declaration type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gift_aid\Declaration\DeclarationEntityListBuilder",
 *     "views_data" = "Drupal\gift_aid\Entity\Declaration\DeclarationEntityViewsData",
 *     "translation" = "Drupal\gift_aid\Declaration\DeclarationEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\gift_aid\Form\Declaration\DeclarationEntityForm",
 *       "add" = "Drupal\gift_aid\Form\Declaration\DeclarationEntityForm",
 *       "edit" = "Drupal\gift_aid\Form\Declaration\DeclarationEntityForm",
 *       "delete" = "Drupal\gift_aid\Form\Declaration\DeclarationEntityDeleteForm",
 *     },
 *     "access" = "Drupal\gift_aid\Declaration\DeclarationEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gift_aid\Declaration\DeclarationEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gift_aid_declaration",
 *   data_table = "gift_aid_declaration_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer declaration entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "declaration_status" = "declaration_status",
 *   },
 *   links = {
 *     "canonical" = "/admin/gift-aid/declaration/{gift_aid_declaration}",
 *     "add-page" = "/admin/gift-aid/declaration/add",
 *     "add-form" = "/admin/gift-aid/declaration/add/{gift_aid_declaration_type}",
 *     "edit-form" = "/admin/gift-aid/declaration/{gift_aid_declaration}/edit",
 *     "delete-form" = "/admin/gift-aid/declaration/{gift_aid_declaration}/delete",
 *     "collection" = "/admin/gift-aid/declaration",
 *   },
 *   bundle_entity_type = "gift_aid_declaration_type",
 *   field_ui_base_route = "entity.gift_aid_declaration_type.edit_form"
 * )
 */
class DeclarationEntity extends ContentEntityBase implements DeclarationEntityInterface {

  use EntityChangedTrait;

  /**
   * Declarer plugin.
   *
   * @var \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerInterface
   */
  protected $declarerPlugin;

  /**
   * Donations plugin.
   *
   * @var \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsInterface
   */
  protected $donationsPlugin;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return Html::escape("{$this->getDeclaredDate()} ({$this->id()})");
  }

  /**
   * {@inheritdoc}
   */
  public function getCharity() {
    return $this->get('charity')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setCharity(CharityEntityInterface $charity) {
    $this->set('charity', $charity->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCharityId() {
    return $this->get('charity')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setCharityId($charity_id) {
    $this->set('charity', $charity_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclaredDate() {
    return $this->get('declared_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeclaredDate($date) {
    $this->set('declared_date', $date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStartDate() {
    return $this->get('start_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartDate($date) {
    $this->set('start_date', $date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndDate() {
    return $this->get('end_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndDate($date) {
    $this->set('end_date', $date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRevokedDate() {
    return $this->get('revoked_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRevokedDate($date) {
    $this->set('revoked_date', $date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclarationStatus($formatted = FALSE) {
    $status = $this->get('declaration_status')->value;
    if (TRUE === $formatted) {
      $status = DeclarationEntity::getDeclarationStatuses(TRUE)[$status];
    }
    return $status;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function setDeclarationStatus($status) {
    if (FALSE === in_array($status, DeclarationEntity::getDeclarationStatuses())) {
      throw new \Exception('Undefined Declaration Status: ' . Html::escape($status));
    }
    $this->set('declaration_status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnduring() {
    return (bool) $this->get('enduring')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnduring($status) {
    $this->set('enduring', (bool) $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclarer() {
    return $this->get('declarer')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeclarer(EntityInterface $declarer) {
    return $this->getDeclarerPlugin()->setDeclarer($declarer);
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclarerPlugin() {
    if (!$this->declarerPlugin) {
      $bundle = $this->bundle();
      $plugin = self::instantiateDeclarerPlugin($bundle);
      $plugin->setDeclaration($this);
      $this->declarerPlugin = $plugin;
    }
    return $this->declarerPlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclarerLegalName() {
    return $this->getDeclarerPlugin()->getLegalName();
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclarerMail() {
    return $this->getDeclarerPlugin()->getMail();
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclarerTaxAddress() {
    return $this->getDeclarerPlugin()->getTaxAddress();
  }

  /**
   * {@inheritdoc}
   */
  public function viewDeclarer() {
    return $this->getDeclarerPlugin()->viewDeclarer();
  }

  /**
   * {@inheritdoc}
   */
  public function getDonations() {
    return $this->get('donations')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setDonations(EntityInterface $donations) {
    return $this->getDonationsPlugin()->setDonations($donations);
  }

  /**
   * {@inheritdoc}
   */
  public function getDonationsPlugin() {
    if (!$this->donationsPlugin) {
      $bundle = $this->bundle();
      $plugin = self::instantiateDonationsPlugin($bundle);
      $plugin->setDeclaration($this);
      $this->donationsPlugin = $plugin;
    }
    return $this->donationsPlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getDonationsLegalName() {
    return $this->getDonationsPlugin()->getLegalName();
  }

  /**
   * {@inheritdoc}
   */
  public function getDonationsTaxAddress() {
    return $this->getDonationsPlugin()->getTaxAddress();
  }

  /**
   * {@inheritdoc}
   */
  public function viewDonations() {
    return $this->getDonationsPlugin()->viewDonations();
  }

  /**
   * {@inheritdoc}
   */
  public static function getDeclarationStatuses($with_labels = FALSE) {
    if (TRUE === $with_labels) {
      return [
        DeclarationEntity::DECLARATION_ACTIVE  => t('Active'),
        DeclarationEntity::DECLARATION_END => t('End'),
        DeclarationEntity::DECLARATION_REVOKED => t('Revoked'),
      ];
    }
    return [
      DeclarationEntity::DECLARATION_ACTIVE,
      DeclarationEntity::DECLARATION_END,
      DeclarationEntity::DECLARATION_REVOKED,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // The Charity entity.
    $fields['charity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Charity'))
      ->setDescription(t('The Charity ID of the Declaration.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'gift_aid_charity')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => -9,
        'settings' => ['link' => FALSE],
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -9,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // The Declared Date field.
    $fields['declared_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Declared Date'))
      ->setDescription(t('The Declared date of the Declaration.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue([
        'default_date_type' => 'now',
        'default_date' => 'now',
      ])
      ->setSetting('datetime_type', 'date')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'datetime_default',
        'weight' => -8,
        'settings' => [
          'format_type' => 'html_date',
          'timezone_override' => '',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // The Start Date field.
    $fields['start_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Start Date'))
      ->setDescription(t('The Start Date of the Declaration.'))
      ->setRevisionable(TRUE)
      ->setSetting('datetime_type', 'date')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'datetime_default',
        'weight' => -7,
        'settings' => [
          'format_type' => 'html_date',
          'timezone_override' => '',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // The End Date field.
    $fields['end_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('End Date'))
      ->setDescription(t('The End Date of the Declaration.'))
      ->setRevisionable(TRUE)
      ->setSetting('datetime_type', 'date')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'datetime_default',
        'weight' => -7,
        'settings' => [
          'format_type' => 'html_date',
          'timezone_override' => '',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // The Revoked Date field.
    $fields['revoked_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Revoked Date'))
      ->setDescription(t('The Revoked Date of the Declaration.'))
      ->setRevisionable(TRUE)
      ->setSetting('datetime_type', 'date')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'datetime_default',
        'weight' => -6,
        'settings' => [
          'format_type' => 'html_date',
          'timezone_override' => '',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // The Declaration Status field.
    $fields['declaration_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Declaration Status'))
      ->setDescription(t('The Status of the Declaration.'))
      ->setRevisionable(TRUE)
      ->setSetting('allowed_values', DeclarationEntity::getDeclarationStatuses(TRUE))
      ->setDefaultValue(DeclarationEntity::DECLARATION_ACTIVE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // The Enduring field.
    $fields['enduring'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is this an enduring declaration that covers all donations within a period of time?'))
      ->setDefaultValue(TRUE)
      ->setSettings([
        'on_label' => t('Yes'),
        'off_label' => t('No'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -4,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'weight' => -4,
        'label' => 'inline',
        'settings' => [
          'format' => 'default',
          'format_custom_false' => '',
          'format_custom_true' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Borrowed this logic from the Comment module.
    // Warning! May change in the future: https://www.drupal.org/node/2346347
    $fields['declarer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Declarer'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Borrowed this logic from the Comment module.
    // Warning! May change in the future: https://www.drupal.org/node/2346347
    $fields['donations'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Donations'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Declaration Author field.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Declaration entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    // Created time field.
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    // Changed time field.
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);
    if ($declarer_plugin = self::instantiateDeclarerPlugin($bundle)) {
      $fields += $declarer_plugin::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);
    }
    if ($donations_plugin = self::instantiateDonationsPlugin($bundle)) {
      $fields += $donations_plugin::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function instantiateDeclarerPlugin($bundle) {
    $declarer_plugin = NULL;
    /** @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityTypeInterface $declaration_type */
    $declaration_type = DeclarationEntityType::load($bundle);
    /** @var \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager $declarer_manager */
    $declarer_manager = \Drupal::service('plugin.manager.gift_aid_declarer');
    $declarer_plugin_id = $declaration_type->getDeclarerPlugin();
    try {
      /** @var \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerInterface $declarer_plugin */
      $declarer_plugin = $declarer_manager->createInstance($declarer_plugin_id);
    }
    catch (PluginException $e) {
      $error_args = [
        '@bundle' => $bundle,
        '@message' => $e->getMessage(),
        '@code' => $e->getCode(),
      ];
      \Drupal::logger('gift_aid')->error('Undefined load a Declarer Gift Aid Plugin on bundle @bundle. Message: @message (@code)', $error_args);
    }
    return $declarer_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function instantiateDonationsPlugin($bundle) {
    $donations_plugin = NULL;
    /** @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityTypeInterface $declaration_type */
    $declaration_type = DeclarationEntityType::load($bundle);
    /** @var \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsManager $donations_manager */
    $donations_manager = \Drupal::service('plugin.manager.gift_aid_donations');
    $donations_plugin_id = $declaration_type->getDonationsPlugin();
    try {
      /** @var \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsInterface $donations_plugin */
      $donations_plugin = $donations_manager->createInstance($donations_plugin_id);
    }
    catch (PluginException $e) {
      $error_args = [
        '@bundle' => $bundle,
        '@message' => $e->getMessage(),
        '@code' => $e->getCode(),
      ];
      \Drupal::logger('gift_aid')->error('Undefined load a Donations Gift Aid Plugin on bundle @bundle. Message: @message (@code)', $error_args);
    }
    return $donations_plugin;
  }

}
