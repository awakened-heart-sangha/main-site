<?php

namespace Drupal\gift_aid\Entity\Declaration;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Declaration type entities.
 */
interface DeclarationEntityTypeInterface extends ConfigEntityInterface {

  /**
   * Get the Gift Aid Declarer plugin id.
   *
   * @return string
   *   Declarer plugin id.
   */
  public function getDeclarerPlugin();

  /**
   * Set the Declarer plugin.
   *
   * @param string $plugin_id
   *   Declarer plugin id.
   *
   * @return DeclarationEntityTypeInterface
   *   Current Declaration entity type.
   */
  public function setDeclarerPlugin($plugin_id);

  /**
   * Get the Gift Aid Donations plugin id.
   *
   * @return string
   *   Donations plugin id.
   */
  public function getDonationsPlugin();

  /**
   * Set the Donations plugin.
   *
   * @param string $plugin_id
   *   Donations plugin id.
   *
   * @return DeclarationEntityTypeInterface
   *   Current Declaration entity type.
   */
  public function setDonationsPlugin($plugin_id);

}
