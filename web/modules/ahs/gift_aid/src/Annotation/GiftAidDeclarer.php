<?php

namespace Drupal\gift_aid\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Gift aid declarer item annotation object.
 *
 * @see \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager
 * @see plugin_api
 *
 * @Annotation
 */
class GiftAidDeclarer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
