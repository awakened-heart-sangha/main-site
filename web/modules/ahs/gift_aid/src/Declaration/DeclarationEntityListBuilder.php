<?php

namespace Drupal\gift_aid\Declaration;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager;
use Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Declaration entities.
 *
 * @ingroup gift_aid
 */
class DeclarationEntityListBuilder extends EntityListBuilder {

  /**
   * Declarer Manager.
   *
   * @var \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager
   */
  protected $declarerManager;

  /**
   * Donations Manager.
   *
   * @var \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsManager
   */
  protected $donationsManager;

  /**
   * DeclarationEntityListBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The Entity Type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The Entity Storage class.
   * @param \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager $declarerManager
   *   The Declarer Manager.
   * @param \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsManager $donationsManager
   *   The Donations Manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, GiftAidDeclarerManager $declarerManager, GiftAidDonationsManager $donationsManager) {
    parent::__construct($entity_type, $storage);
    $this->declarerManager = $declarerManager;
    $this->donationsManager = $donationsManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.gift_aid_declarer'),
      $container->get('plugin.manager.gift_aid_donations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Declaration ID');
    $header['declaration_type'] = $this->t('Declaration Type');
    $header['charity'] = $this->t('Charity');
    $header['declared_date'] = $this->t('Declared Date');
    $header['start_date'] = $this->t('Start Date');
    $header['end_date'] = $this->t('End Date');
    $header['revoked_date'] = $this->t('Revoked Date');
    $header['declaration_status'] = $this->t('Declaration Status');
    $header['enduring'] = $this->t('Enduring');
    $header['declarer'] = $this->t('Declarer');
    $header['donations'] = $this->t('Donations');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntity $entity */
    $row['id'] = Link::createFromRoute(
      $entity->id(),
      'entity.gift_aid_declaration.edit_form',
      ['gift_aid_declaration' => $entity->id()]
    );
    $row['declaration_type'] = $entity->getType();
    $row['charity'] = $entity->getCharity() ? $entity->getCharity()->label() : $this->t('[NO CHARITY]');
    $row['declared_date'] = $entity->getDeclaredDate();
    $row['start_date'] = $entity->getStartDate() ?? '-';
    $row['end_date'] = $entity->getEndDate() ?? '-';
    $row['revoked_date'] = $entity->getRevokedDate() ?? '-';
    $row['declaration_status'] = $entity->getDeclarationStatus(TRUE);
    $row['enduring'] = $entity->isEnduring() ? $this->t('Yes') : $this->t('No');
    $row['declarer']['data'] = $entity->viewDeclarer();
    $row['donations']['data'] = $entity->viewDonations();

    return $row + parent::buildRow($entity);
  }

}
