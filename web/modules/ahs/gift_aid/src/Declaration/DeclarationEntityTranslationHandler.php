<?php

namespace Drupal\gift_aid\Declaration;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for gift_aid_declaration_entity.
 */
class DeclarationEntityTranslationHandler extends ContentTranslationHandler {

}
