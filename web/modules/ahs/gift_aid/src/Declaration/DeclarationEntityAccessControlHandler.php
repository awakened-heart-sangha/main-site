<?php

namespace Drupal\gift_aid\Declaration;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Declaration entity.
 *
 * @see \Drupal\gift_aid\Entity\Declaration\DeclarationEntity.
 */
class DeclarationEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view gift aid declaration entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit gift aid declaration entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete gift aid declaration entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add gift aid declaration entities');
  }

}
