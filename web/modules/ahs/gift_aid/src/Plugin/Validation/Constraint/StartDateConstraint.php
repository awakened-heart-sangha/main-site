<?php

namespace Drupal\gift_aid\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Supports validating start_date field of the declaration entity.
 *
 * @Constraint(
 *   id = "StartDate",
 *   label = @Translation("Start date", context = "Validation"),
 *   type = "entity:gift_aid_declaration"
 * )
 */
class StartDateConstraint extends CompositeConstraintBase {

  /**
   * Message shown when the start_date field is empty in enduring declaration.
   *
   * @var string
   */
  public $messageRequiredIfEnduring = 'The start_date cannot be empty if declaration is enduring.';

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return ['start_date'];
  }

}
