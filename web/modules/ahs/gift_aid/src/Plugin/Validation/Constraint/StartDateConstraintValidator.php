<?php

namespace Drupal\gift_aid\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the StartDate constraint.
 */
class StartDateConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Validator 2.5 and upwards compatible execution context.
   *
   * @var \Symfony\Component\Validator\Context\ExecutionContextInterface
   */
  protected $context;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager')->getStorage('gift_aid_declaration'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if ($entity->hasField('enduring') && $entity->hasField('start_date')) {
      $enduring = (bool) $entity->enduring->value;
      $start_date = $entity->start_date->value;

      // Do not allow empty start_date field if declaration enduring.
      if ($enduring && empty($start_date)) {
        $this->context->buildViolation($constraint->messageRequiredIfEnduring)
          ->atPath('start_date')
          ->addViolation();
      }
    }
  }

}
