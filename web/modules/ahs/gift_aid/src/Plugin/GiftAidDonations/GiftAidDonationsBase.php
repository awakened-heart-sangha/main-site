<?php

namespace Drupal\gift_aid\Plugin\GiftAidDonations;

use Drupal\Component\Plugin\PluginBase;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface;

/**
 * Base class for Gift aid donations plugins.
 */
abstract class GiftAidDonationsBase extends PluginBase implements GiftAidDonationsInterface {

  /**
   * Declaration entity.
   *
   * @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface|null
   */
  protected $declaration = NULL;

  /**
   * {@inheritdoc}
   */
  public function setDeclaration(DeclarationEntityInterface $declaration) {
    $this->declaration = $declaration;
    return $this->declaration;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclaration() {
    return $this->declaration;
  }

  /**
   * {@inheritdoc}
   */
  public function getLegalName() {
    return NULL;
  }

}
