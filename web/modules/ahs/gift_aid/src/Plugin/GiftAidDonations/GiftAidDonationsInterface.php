<?php

namespace Drupal\gift_aid\Plugin\GiftAidDonations;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface;

/**
 * Defines an interface for Gift aid donations plugins.
 */
interface GiftAidDonationsInterface extends PluginInspectionInterface {

  /**
   * Defines donations fields.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition. Useful when a single class is used for,
   *   multiple possibly dynamic entity types.
   * @param string $bundle
   *   The bundle.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $base_field_definitions
   *   The list of base field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of added/modified bundle field definitions, keyed by field name.
   *
   * @see https://www.drupal.org/docs/8/api/entity-api/defining-and-using-content-entity-field-definitions
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions);

  /**
   * Returns the Donations entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The Donations entity.
   */
  public function getDonations();

  /**
   * Set Declaration entity into plugin.
   *
   * @param \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface $declaration
   *   Declaration entity.
   *
   * @return \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsInterface
   *   Current object.
   */
  public function setDeclaration(DeclarationEntityInterface $declaration);

  /**
   * Return declaration entity.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface|null
   *   Declaration entity or null.
   */
  public function getDeclaration();

  /**
   * Set Donations entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $donations
   *   Donations entity.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   Declaration entity.
   */
  public function setDonations(EntityInterface $donations);

  /**
   * Returns donations information (for Views module).
   *
   * @return array
   *   Render array of the Donations.
   */
  public function viewDonations();

  /**
   * Returns the Legal Name.
   *
   * @return string|null
   *   The Donations Legal Name.
   */
  public function getLegalName();

}
