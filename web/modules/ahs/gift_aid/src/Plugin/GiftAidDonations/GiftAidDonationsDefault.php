<?php

namespace Drupal\gift_aid\Plugin\GiftAidDonations;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Default Gift Aid Donations.
 *
 * The plugin use the custom Contact Profile.
 *
 * @GiftAidDonations(
 *   id = "gift_aid_donations_default",
 *   label = @Translation("Gift Aid Donations Default"),
 * )
 */
class GiftAidDonationsDefault extends GiftAidDonationsBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The Entity type manager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GiftAidDonationsDefault constructor.
   *
   * @param array $configuration
   *   Configurations array.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];

    // Configure the Donations field.
    $fields['donations'] = clone $base_field_definitions['donations'];
    $fields['donations']->setSettings([
      'target_type' => 'node',
      'handler' => 'default:node',
      'handler_settings' => [
        'include_anonymous' => FALSE,
        'target_bundles' => NULL,
        'filter' => [
          'type' => '_none',
        ],
        'sort' => [
          'field' => '_none',
        ],
        'auto_create' => FALSE,
      ],
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getDonations() {
    return $this->getDeclaration()->getDonations();
  }

  /**
   * {@inheritdoc}
   */
  public function setDonations(EntityInterface $donations) {
    $this->getDeclaration()->set('donations', $donations->id());
    return $this->getDeclaration();
  }

  /**
   * {@inheritdoc}
   */
  public function getLegalName() {
    /** @var \Drupal\user\UserInterface $donations */
    $donations = $this->getDonations();
    return $donations->getTitle();
  }

  /**
   * {@inheritdoc}
   */
  public function viewDonations() {
    $declaration = $this->getDeclaration();
    $donations = $this->getDonations();

    // No return when user doesn't have an access to view donations.
    if (!$donations || FALSE === $donations->access('view')) {
      return NULL;
    }

    // Build render wrapper.
    $output = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'donations-view',
          'donations-view--declaration--' . $declaration->bundle(),
          'donations-view--plugin--' . $this->getPluginId(),
        ],
      ],
      '#cache' => [
        'tags' => $donations->getCacheTags(),
        'contexts' => $donations->getCacheContexts(),
        'max-age' => $donations->getCacheMaxAge(),
      ],
    ];

    $output['fields'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        ['#markup' => $this->t('Donations Name: @name', ['@name' => $this->getLegalName()])],
      ],
    ];

    return $output;
  }

}
