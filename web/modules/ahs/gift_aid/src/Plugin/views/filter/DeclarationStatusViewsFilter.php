<?php

namespace Drupal\gift_aid\Plugin\views\filter;

use Drupal\gift_aid\Entity\Declaration\DeclarationEntity;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;

/**
 * A handler to provide a filter for Declaration status field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("gift_aid_declaration_status_filter")
 */
class DeclarationStatusViewsFilter extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = $this->t('Declaration Status');
    $this->definition['options callback'] = [$this, 'getOptions'];
  }

  /**
   * Returns available options for the Declaration Status field.
   *
   * @return array
   *   Associative array with values.
   */
  public function getOptions() {
    return DeclarationEntity::getDeclarationStatuses(TRUE);
  }

}
