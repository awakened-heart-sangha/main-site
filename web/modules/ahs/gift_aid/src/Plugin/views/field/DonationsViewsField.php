<?php

namespace Drupal\gift_aid\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide display the Donations field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("gift_aid_donations_views_field")
 */
class DonationsViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface $declaration */
    $declaration = $values->_entity;
    return $declaration->viewDonations();
  }

}
