<?php

namespace Drupal\gift_aid\Plugin\GiftAidDeclarer;

use Drupal\Component\Plugin\PluginBase;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface;

/**
 * Base class for Gift aid declarer plugins.
 */
abstract class GiftAidDeclarerBase extends PluginBase implements GiftAidDeclarerInterface {

  /**
   * Declaration entity.
   *
   * @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface|null
   */
  protected $declaration = NULL;

  /**
   * {@inheritdoc}
   */
  public function setDeclaration(DeclarationEntityInterface $declaration) {
    $this->declaration = $declaration;
    return $this->declaration;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclaration() {
    return $this->declaration;
  }

  /**
   * {@inheritdoc}
   */
  public function getLegalName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxAddress() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMail() {
    return NULL;
  }

}
