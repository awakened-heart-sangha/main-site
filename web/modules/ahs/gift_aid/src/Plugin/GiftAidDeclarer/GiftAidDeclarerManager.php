<?php

namespace Drupal\gift_aid\Plugin\GiftAidDeclarer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Gift aid declarer plugin manager.
 */
class GiftAidDeclarerManager extends DefaultPluginManager {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new GiftAidDeclarerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct('Plugin/GiftAidDeclarer', $namespaces, $module_handler, 'Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerInterface', 'Drupal\gift_aid\Annotation\GiftAidDeclarer');

    $this->alterInfo('gift_aid_gift_aid_declarer_info');
    $this->setCacheBackend($cache_backend, 'gift_aid_gift_aid_declarer_plugins');

    $this->entityTypeManager = $entityTypeManager;
  }

}
