<?php

namespace Drupal\gift_aid\Plugin\GiftAidDeclarer;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface;

/**
 * Defines an interface for Gift aid declarer plugins.
 */
interface GiftAidDeclarerInterface extends PluginInspectionInterface {

  /**
   * Defines declarer fields.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition. Useful when a single class is used for,
   *   multiple possibly dynamic entity types.
   * @param string $bundle
   *   The bundle.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $base_field_definitions
   *   The list of base field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of added/modified bundle field definitions, keyed by field name.
   *
   * @see https://www.drupal.org/docs/8/api/entity-api/defining-and-using-content-entity-field-definitions
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions);

  /**
   * Returns the Declarer entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The Declarer entity.
   */
  public function getDeclarer();

  /**
   * Set Declaration entity into plugin.
   *
   * @param \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface $declaration
   *   Declaration entity.
   *
   * @return \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerInterface
   *   Current object.
   */
  public function setDeclaration(DeclarationEntityInterface $declaration);

  /**
   * Return declaration entity.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface|null
   *   Declaration entity or null.
   */
  public function getDeclaration();

  /**
   * Set Declarer entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $declarer
   *   Declarer entity.
   *
   * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntityInterface
   *   Declaration entity.
   */
  public function setDeclarer(EntityInterface $declarer);

  /**
   * Returns declarer information (for Views module).
   *
   * @return array
   *   Render array of the Declarer.
   */
  public function viewDeclarer();

  /**
   * Returns the Legal Name.
   *
   * @return string|null
   *   The Declarer Legal Name.
   */
  public function getLegalName();

  /**
   * Returns the Tax address.
   *
   * @return string|null
   *   The Declarer Tax Address.
   */
  public function getTaxAddress();

  /**
   * Returns the Email.
   *
   * @return string|null
   *   The Declarer Email.
   */
  public function getMail();

}
