<?php

namespace Drupal\gift_aid\Plugin\GiftAidDeclarer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Default Gift Aid Declarer.
 *
 * The plugin use the custom Contact Profile.
 *
 * @GiftAidDeclarer(
 *   id = "gift_aid_declarer_default",
 *   label = @Translation("Gift Aid Declarer Default"),
 * )
 */
class GiftAidDeclarerDefault extends GiftAidDeclarerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The Entity type manager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GiftAidDeclarerDefault constructor.
   *
   * @param array $configuration
   *   Configurations array.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];

    // Configure the Declarer field.
    $fields['declarer'] = clone $base_field_definitions['declarer'];
    $fields['declarer']->setSettings([
      'target_type' => 'user',
      'handler' => 'default:user',
      'handler_settings' => [
        'include_anonymous' => FALSE,
        'target_bundles' => NULL,
        'filter' => [
          'type' => '_none',
        ],
        'sort' => [
          'field' => '_none',
        ],
        'auto_create' => FALSE,
      ],
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeclarer() {
    return $this->getDeclaration()->getDeclarer();
  }

  /**
   * {@inheritdoc}
   */
  public function setDeclarer(EntityInterface $declarer) {
    $this->getDeclaration()->set('declarer', $declarer->id());
    return $this->getDeclaration();
  }

  /**
   * {@inheritdoc}
   */
  public function getLegalName() {
    /** @var \Drupal\user\UserInterface $declarer */
    $declarer = $this->getDeclarer();
    return $declarer->getDisplayName();
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxAddress() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getMail() {
    /** @var \Drupal\user\UserInterface $declarer */
    $declarer = $this->getDeclarer();
    return $declarer->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function viewDeclarer() {
    $declaration = $this->getDeclaration();
    $declarer = $this->getDeclarer();

    // No return when user doesn't have an access to view declarer.
    if (FALSE === $declarer->access('view')) {
      return NULL;
    }

    // Build render wrapper.
    $output = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'declarer-view',
          'declarer-view--declaration--' . $declaration->bundle(),
          'declarer-view--plugin--' . $this->getPluginId(),
        ],
      ],
      '#cache' => [
        'tags' => $declarer->getCacheTags(),
        'contexts' => $declarer->getCacheContexts(),
        'max-age' => $declarer->getCacheMaxAge(),
      ],
    ];

    $viewBuilder = $this->entityTypeManager->getViewBuilder($declarer->getEntityTypeId());

    $output['fields'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        ['#markup' => $this->t('Declarer Name: @name', ['@name' => $this->getLegalName()])],
        ['#markup' => $this->t('Declarer Mail: @mail', ['@mail' => $this->getMail()])],
      ],
    ];

    $output['entity'] = $viewBuilder->view($declarer, 'compact');

    return $output;
  }

}
