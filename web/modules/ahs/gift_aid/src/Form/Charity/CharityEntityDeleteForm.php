<?php

namespace Drupal\gift_aid\Form\Charity;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Charity entities.
 *
 * @ingroup gift_aid
 */
class CharityEntityDeleteForm extends ContentEntityDeleteForm {

}
