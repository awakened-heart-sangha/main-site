<?php

namespace Drupal\gift_aid\Form\Declaration;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Declaration entities.
 *
 * @ingroup gift_aid
 */
class DeclarationEntityDeleteForm extends ContentEntityDeleteForm {

}
