<?php

namespace Drupal\gift_aid\Form\Declaration;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager;
use Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DeclarationEntityTypeForm. Provides base form for declarations.
 */
class DeclarationEntityTypeForm extends EntityForm {

  /**
   * Declarer manager.
   *
   * @var \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager
   */
  protected $giftAidDeclarerManager;

  /**
   * Donations manager.
   *
   * @var \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsManager
   */
  protected $giftAidDonationsManager;

  /**
   * DeclarationEntityTypeForm constructor.
   *
   * @param \Drupal\gift_aid\Plugin\GiftAidDeclarer\GiftAidDeclarerManager $giftAidDeclarerManager
   *   Gift Aid Declarer manager.
   * @param \Drupal\gift_aid\Plugin\GiftAidDonations\GiftAidDonationsManager $giftAidDonationsManager
   *   Gift Aid Donations manager.
   */
  public function __construct(GiftAidDeclarerManager $giftAidDeclarerManager, GiftAidDonationsManager $giftAidDonationsManager) {
    $this->giftAidDeclarerManager = $giftAidDeclarerManager;
    $this->giftAidDonationsManager = $giftAidDonationsManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.gift_aid_declarer'),
      $container->get('plugin.manager.gift_aid_donations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\gift_aid\Entity\Declaration\DeclarationEntityTypeInterface $gift_aid_declaration_type */
    $gift_aid_declaration_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $gift_aid_declaration_type->label(),
      '#description' => $this->t("Label for the Declaration type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $gift_aid_declaration_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\gift_aid\Entity\DeclarationEntityType::load',
      ],
      '#disabled' => !$gift_aid_declaration_type->isNew(),
    ];

    // Provide the select element for choose the Gift Aid Declarer plugin.
    $gift_aid_declarer_plugins = $this->giftAidDeclarerManager->getDefinitions();
    // Build options.
    $gift_aid_declarer_plugins_options = array_map(function ($v) {
      return $v['label'];
    }, $gift_aid_declarer_plugins);
    // Display an element.
    $form['declarer_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Gift Aid Declarer plugin'),
      '#options' => $gift_aid_declarer_plugins_options,
      '#required' => TRUE,
      '#description' => $this->t('Select the Gift Aid Declarer plugin for provide Declarer definition on the Declaration.'),
      '#default_value' => $gift_aid_declaration_type->getDeclarerPlugin(),
    ];

    // Provide the select element for choose the Gift Aid Donations plugin.
    $gift_aid_donations_plugins = $this->giftAidDonationsManager->getDefinitions();
    // Build options.
    $gift_aid_donations_plugins_options = array_map(function ($v) {
      return $v['label'];
    }, $gift_aid_donations_plugins);
    // Display an element.
    $form['donations_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Gift Aid Donations plugin'),
      '#options' => $gift_aid_donations_plugins_options,
      '#required' => TRUE,
      '#description' => $this->t('Select the Gift Aid Donations plugin for provide Donations definition on the Declaration.'),
      '#default_value' => $gift_aid_declaration_type->getDonationsPlugin(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $gift_aid_declaration_type = $this->entity;
    $status = $gift_aid_declaration_type->save();

    switch ($status) {
      case SAVED_NEW:

        $this->messenger()->addMessage($this->t('Created the %label Declaration type.', [
          '%label' => $gift_aid_declaration_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Declaration type.', [
          '%label' => $gift_aid_declaration_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($gift_aid_declaration_type->toUrl('collection'));

    return $status;
  }

}
