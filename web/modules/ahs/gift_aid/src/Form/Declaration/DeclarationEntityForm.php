<?php

namespace Drupal\gift_aid\Form\Declaration;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Declaration edit forms.
 *
 * @ingroup gift_aid
 */
class DeclarationEntityForm extends ContentEntityForm {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Show/hide Start and End dates depending on the Enduring state.
    $enduring_checked = [
      ':input[name="enduring[value]"]' => ['checked' => TRUE],
    ];
    $form['start_date']['#states'] = [
      'visible' => $enduring_checked,
    ];
    $form['start_date']['widget'][0]['value']['#states'] = [
      'required' => $enduring_checked,
    ];
    $form['end_date']['#states'] = [
      'visible' => $enduring_checked,
    ];

    // Show/hide/required the Donations field depending on the Enduring state.
    $enduring_unchecked = [
      ':input[name="enduring[value]"]' => ['checked' => FALSE],
    ];
    $form['donations']['#states'] = [
      'visible' => $enduring_unchecked,
    ];
    $form['donations']['widget'][0]['target_id']['#states'] = [
      'required' => $enduring_unchecked,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger->addMessage($this->t('Created the %label Declaration.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger->addMessage($this->t('Saved the %label Declaration.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.gift_aid_declaration.canonical', ['gift_aid_declaration' => $entity->id()]);

    return $status;
  }

}
