<?php

/**
 * @file
 * Contains gift_aid.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\gift_aid\Entity\Charity\CharityEntityInterface;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntity;
use Drupal\gift_aid\Entity\Declaration\DeclarationEntityTypeInterface;

/**
 * Implements hook_help().
 */
function gift_aid_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the gift_aid module.
    case 'help.page.gift_aid':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provided Gift Aid functionality.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_type_build().
 */
function gift_aid_entity_type_build(array &$entity_types) {
  $entity_types['gift_aid_declaration']->addConstraint('StartDate');
}

/**
 * Create declaration.
 *
 * @param \Drupal\gift_aid\Entity\Charity\CharityEntityInterface $charity
 *   The charity entity.
 * @param \Drupal\gift_aid\Entity\Declaration\DeclarationEntityTypeInterface $declaration_type
 *   The declaration entity type.
 * @param \Drupal\Core\Entity\EntityInterface $declarer
 *   The declaration entity type.
 *
 * @return \Drupal\gift_aid\Entity\Declaration\DeclarationEntity
 *   The new declaration entity.
 */
function gift_aid_declaration_create(CharityEntityInterface $charity, DeclarationEntityTypeInterface $declaration_type, EntityInterface $declarer) {
  $start_date = \Drupal::service('date.formatter')->format(strtotime('today'), 'html_date');
  $end_date = \Drupal::service('date.formatter')->format(strtotime('today +1 month'), 'html_date');
  $declaration = DeclarationEntity::create([
    'type' => $declaration_type->id(),
    'charity' => $charity->id(),
    'start_date' => $start_date,
    'end_date' => $end_date,
    'declaration_status' => DeclarationEntity::DECLARATION_ACTIVE,
    'enduring' => TRUE,
    'declarer' => $declarer->id(),
  ]);
  $declaration->save();
  return $declaration;
}
