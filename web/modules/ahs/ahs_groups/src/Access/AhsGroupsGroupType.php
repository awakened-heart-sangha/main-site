<?php

namespace Drupal\ahs_groups\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\Routing\Route;

/**
 * Checks access for an entity based on its bundle.
 */
class AhsGroupsGroupType implements AccessInterface {

  /**
   * Restrict access based on group type.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The entity to check access for.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   */
  public function access(GroupInterface $group, Route $route) {
    $group_type = $route->getRequirement('_group_type');
    return AccessResult::allowedIf($group->bundle() === $group_type);
  }

}
