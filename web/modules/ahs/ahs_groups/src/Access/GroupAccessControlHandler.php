<?php

namespace Drupal\ahs_groups\Access;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\Access\GroupAccessControlHandler as ParentAccessControlHandler;

/**
 * Access controller for the Group entity.
 *
 * @see \Drupal\group\Entity\Group.
 */
class GroupAccessControlHandler extends ParentAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation === 'view' && !$entity->isPublished()) {
      return GroupAccessResult::allowedIfHasGroupPermission($entity, $account, 'view unpublished group')->addCacheableDependency($entity);
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
