<?php

namespace Drupal\ahs_groups\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\Routing\Route;

/**
 * Checks access for a group based on registration status.
 */
class AhsGroupsGroupRegisterable implements AccessInterface {

  /**
   * Restrict access based on group registerability.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The entity to check access for.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   */
  public function access(AccountInterface $account, GroupInterface $group, Route $route) {
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($account->id());
    return AccessResult::allowedIf($group->isRegistrationAvailableToUser($user))->addCacheableDependency($group);
  }

}
