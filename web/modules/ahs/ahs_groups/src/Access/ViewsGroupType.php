<?php

namespace Drupal\ahs_groups\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to a view based on the route's group bundle.
 *
 * Not currently used but might be useful in future.
 */
class ViewsGroupType implements AccessInterface {

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    if (!$route->hasRequirement('_ahs_groups_views_group_type')) {
      return AccessResult::neutral();
    }
    $required_type = $route->getRequirement('_ahs_groups_views_group_type');
    $group = $route_match->getParameter('group');
    if ($group instanceof GroupInterface) {
      if ($group->bundle() === $required_type) {
        return AccessResult::allowed()->addCacheableDependency($group);
      }
    }
    return AccessResult::neutral();
  }

}
