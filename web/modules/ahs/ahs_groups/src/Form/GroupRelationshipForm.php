<?php

namespace Drupal\ahs_groups\Form;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\group\Entity\Form\GroupRelationshipForm as parentGroupRelationshipForm;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\registration\Entity\Registration;
use Drupal\registration\Entity\RegistrationInterface;
use Drupal\user\UserInterface;

/**
 * Provides a form for adding/editing a group relationship.
 *
 * We modify this form in order to allow specifying a registration type
 * and creating a registration as a second step.
 *
 * @see ahs_groups_entity_type_build().
 */
class GroupRelationshipForm extends parentGroupRelationshipForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    if ($this->getPlugin()->getRelationTypeId() !== 'group_membership') {
      return $form;
    }

    if (!$this->getRegistration()) {
      $hosts = $this->getGroup()->getRegistrationHostEntities();
      if ($hosts) {
        foreach ($hosts as $host) {
          $options[$host->getEntityTypeId() . ":" . $host->id()] = $host->label();
        }
        $form['registration_host'] = [
          '#type' => 'select',
          '#title' => 'Registration type',
          '#options' => $options,
          '#empty_option' => 'None',
          '#empty_value' => '',
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $return = parent::save($form, $form_state);
    if ($this->getPlugin()->getRelationTypeId() !== 'group_membership') {
      return $return;
    }

    $registration = $this->getRegistration();
    if (!$registration) {
      $host_key = $form_state->getValue('registration_host');
      if (!empty($host_key)) {
        $host_type_id = explode(":", $host_key)[0];
        $host_id = explode(":", $host_key)[1];
        $host_storage = $this->entityTypeManager->getStorage($host_type_id);
        $host = $host_storage->load($host_id);
        /** @var \Drupal\registration\HostEntityInterface $host_wrapped */
        $host_wrapped = $this->entityTypeManager
          ->getHandler('registration', 'host_entity')
          ->createHostEntity($host);
        $registration = Registration::create([
          'entity_type_id' => $host_type_id,
          'entity_id' => $host_id,
          'type' => $host_wrapped->getRegistrationTypeBundle(),
          'user_uid' => $this->getUser()->id(),
          'count' => 1,
        ]);
        $registration->save();

        $group_relationship = $this->getEntity();
        assert($group_relationship instanceof GroupRelationshipInterface);
        $group_relationship->set('field_registration', $registration);
        $group_relationship->save();
      }
    }
    else {
      // Trigger resave of the registration in case some data needs resyncing.
      $registration->save();
    }

    $participantsUrl = Url::fromRoute('view.group_members.page_1', ['group' => $this->getGroup()->id()]);
    if ($registration && $this->areFieldsDisplayed('registration', $registration->bundle(), 'default')) {
      $options = ['query' => ['destination' => $participantsUrl->toString()]];
      $form_state->setRedirectUrl($registration->toUrl('edit-form', $options));
    }
    else {
      $form_state->setRedirectUrl($participantsUrl);
    }

    return $return;
  }

  /**
   * Checks if any configurable fields are displayed in a given view mode.
   */
  public function areFieldsDisplayed($entity_type_id, $bundle, $view_mode) {
    $entityDisplay = $this->entityTypeManager->getStorage('entity_view_display')
      ->load($entity_type_id . '.' . $bundle . '.' . $view_mode);
    if (!$entityDisplay) {
      return TRUE;
    }

    $fieldDefinitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle);
    foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
      if ($fieldDefinition instanceof FieldConfig) {
        $component = $entityDisplay->getComponent($fieldName);
        if ($component && $component['type'] !== 'hidden') {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Get the current group.
   */
  protected function getGroup() : AhsGroup {
    $group_relationship = $this->getEntity();
    assert($group_relationship instanceof GroupRelationshipInterface);
    $group = $group_relationship->getGroup();
    assert($group instanceof AhsGroup);
    return $group;
  }

  /**
   * Get the current registration.
   */
  protected function getRegistration() : RegistrationInterface|null {
    $user = $this->getUser();
    if ($user) {
      $registrations = $this->getGroup()->getRegistrations($user);
      if ($registrations) {
        $registration = reset($registrations);
        return !$registration->isCanceled() ? $registration : NULL;
      }
    }
    return NULL;
  }

  /**
   * Get the form user.
   */
  protected function getUser() : UserInterface|null {
    $user = $this->getEntity()->getEntity();
    if ($user) {
      assert($user instanceof UserInterface);
      assert(!$user->isAnonymous());
    }
    return $user;
  }

}
