<?php

namespace Drupal\ahs_groups\Form;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\Resolver\ChainStoreResolverInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\EnforcedResponseException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GroupMaterialsForm. Provides group materials form.
 */
class GroupMaterialsForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current current group.
   *
   * @var \Drupal\group\Entity\GroupInterface
   */
  protected $group;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The store.
   *
   * @var \Drupal\commerce_store\Entity\StoreInterface
   */
  protected $store;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * Constructs a new GroupMaterialsForm object.
   */
  public function __construct(MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match, Connection $database, ChainStoreResolverInterface $store_resolver, CartManagerInterface $cart_manager, CartProviderInterface $cart_provider) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->group = $route_match->getParameter('group');
    $this->database = $database;
    $this->store = $store_resolver->resolve();
    $this->cartManager = $cart_manager;
    $this->cartProvider = $cart_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('database'),
      $container->get('commerce_store.chain_store_resolver'),
      $container->get('commerce_cart.cart_manager'),
      $container->get('commerce_cart.cart_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ahs_groups_group_materials_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->group instanceof GroupInterface) {
      $this->doImmediateRedirect();
    }

    if (!$this->group->hasField('field_materials') || $this->group->get('field_materials')->isEmpty()) {
      $this->doImmediateRedirect();
    }

    $userMaterialsNeeded = $this->getUserMaterialsNeeded();
    if (empty($userMaterialsNeeded)) {
      $this->doImmediateRedirect();
    }

    $form['instruction'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => "You need to have the following materials to participate in this course. Please request only materials you do not already have. They will be posted to you",
    ];

    $materialsOptions = [];
    foreach ($userMaterialsNeeded as $material) {
      $materialsOptions[$material->id()] = $material->label();
    }
    $form['materials'] = [
      '#type' => 'checkboxes',
      '#title' => 'Materials',
      '#options' => $materialsOptions,
      '#default_value' => array_keys($materialsOptions),
      '#description' => $this->t("Please request only materials you do not already have."),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['request'] = [
      '#type' => 'submit',
      '#value' => $this->t('Request materials'),
    ];
    $form['actions']['skip'] = [
      '#type' => 'submit',
      '#value' => $this->t('Skip'),
      '#submit' => [[$this, 'submitSkip']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $materialsRequested = $values['materials'];

    // If the user unchecked all the materials, then no need to order.
    if (empty($materialsRequested)) {
      $form_state->setResponse($this->getRedirectResponse());
      return;
    }

    $cart = $this->cartProvider->getCart('ahs_free', $this->store);
    if (!$cart) {
      $cart = $this->cartProvider->createCart('ahs_free', $this->store);
    }
    else {
      $this->cartManager->emptyCart($cart);
    }
    if (!$this->currentUser()->isAnonymous()) {
      $cart->setCustomerId($this->currentUser()->id());
      $cart->setEmail($this->currentUser()->getEmail());
    }
    $cart->setData('redirect_on_completion_uri', $this->getFinalDestinationUrl());
    $cart->setData('redirect_on_completion_messages', [
      $this->getThankyouMessage(),
      "The materials you requested will be posted to you.",
    ]);
    $cart->save();

    foreach ($materialsRequested as $materialId) {
      $orderItem = OrderItem::create([
        'type' => 'default',
        'quantity' => 1,
        'unit_price' => new Price(0, 'GBP'),
        'purchased_entity' => $materialId,
        'overridden_unit_price' => TRUE,
      ]);
      $orderItem->save();
      $this->cartManager->addOrderItem($cart, $orderItem, TRUE);
    }

    // Remove the add to cart status message.
    $this->messenger()->deleteByType('status');

    $url = Url::fromRoute(
      'commerce_checkout.form',
      [
        'commerce_order' => $cart->get('order_id')->getString(),
        'step' => NULL,
      ]
    );
    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function submitSkip(array &$form, FormStateInterface $form_state) {
    $form_state->setResponse($this->getRedirectResponse());
  }

  /**
   * Helper to get user materials.
   */
  protected function getUserMaterialsNeeded() {
    $groupMaterials = $this->group->get('field_materials')->referencedEntities();
    return array_filter($groupMaterials, function ($groupMaterial) {
      return !$this->hasUserOrderedMaterial($groupMaterial);
    });
  }

  /**
   * Helper to check user orders.
   */
  protected function hasUserOrderedMaterial(PurchasableEntityInterface $material) {
    $query = $this->database->select('commerce_order', 'co');
    $query->leftjoin('commerce_order_item', 'coi', 'co.order_id = coi.order_id');
    $query->fields('co', ['order_id']);
    $query->condition('co.state', ['draft', 'canceled'], 'NOT IN');
    $query->condition('co.uid', $this->currentUser()->id());
    $query->condition('coi.purchased_entity', $material->id());
    $result = $query->distinct()->countQuery()->execute()->fetchField();
    $hasMaterial = ((int) $result) > 0;
    return $hasMaterial;
  }

  /**
   * Redirect the user immediately.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   */
  protected function doImmediateRedirect() {
    throw new EnforcedResponseException($this->getRedirectResponse());
  }

  /**
   * Get the redirect response to use.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   Redirect response.
   */
  protected function getRedirectResponse() {
    if ($thankyou = $this->getThankyouMessage()) {
      $this->messenger->addMessage($thankyou);
    }
    return new TrustedRedirectResponse($this->getFinalDestinationUrl());
  }

  /**
   * Get thankyou message to show at end of registration process.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Thanks message.
   */
  protected function getThankyouMessage() {
    if ($this->group instanceof GroupInterface) {
      return $this->t('Thankyou for registering for %group.', ['%group' => $this->group->label()]);
    }
  }

  /**
   * Get final destination uri of registration process.
   *
   * @return string
   *   Destination URL.
   */
  protected function getFinalDestinationUrl() {
    if ($this->group && $this->group->hasField('field_registration_redirect') && !$this->group->get('field_registration_redirect')->isEmpty()) {
      return $this->group->get('field_registration_redirect')->first()->getUrl()->toString(TRUE)->getGeneratedUrl();
    }
    elseif ($this->group && $this->group->access('view')) {
      return $this->group->toUrl()->toString(TRUE)->getGeneratedUrl();
    }
    else {
      return Url::fromRoute('<front')->toString(TRUE)->getGeneratedUrl();
    }
  }

  /**
   * The _title_callback for the materials form route.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to start.
   *
   * @return string
   *   The page title.
   */
  public function materialsTitle(GroupInterface $group) {
    return $this->t('Request %label materials', ['%label' => $group->label()]);
  }

}
