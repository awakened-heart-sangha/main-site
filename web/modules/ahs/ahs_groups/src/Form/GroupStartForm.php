<?php

namespace Drupal\ahs_groups\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;

/**
 * Implements a form for starting a course yourself by cloning a template.
 */
class GroupStartForm extends GroupCloneForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $message = $this->stringTranslationManager->translate("Would you like to start the '@entity' course? You will study on your own, at your own pace.", [
      '@entity' => $this->entity->label(),
    ]);
    $form['message'] = [
      '#markup' => "<p>$message</p>",
    ];

    $form = parent::buildForm($form, $form_state);

    $form['type']['#default_value'] = 'course';
    $form['type']['#access'] = FALSE;

    $form['weeks']['#access'] = FALSE;
    $form['create_sessions']['#access'] = FALSE;

    $form['uids']['#default_value'] = $this->currentUser->id();
    $form['uids']['#access'] = FALSE;

    $form['content']['#default_value'] = 'clone';
    $form['content']['#access'] = FALSE;

    $form['fields']['#access'] = FALSE;

    $form['actions']['abort']['#access'] = FALSE;
    $form['actions']['clone']['#value'] = $this->stringTranslationManager->translate('Start');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDuplicate(array $properties) {
    $entity = parent::getDuplicate($properties);

    // We can't assume the bundle is 'course' because getDuplicate is used in
    // parent::buildForm() to load form displays for different bundles.
    if ($entity->bundle() === 'course') {
      // Courses started using this form are always initially private,
      // published, self-paced, and not open for others to join.
      $entity->set('field_private', TRUE);
      $entity->set('field_self_paced', TRUE);
      $entity->set('field_registration_open', FALSE);
      $entity->set('status', TRUE);
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function addMessage(EntityInterface $cloned_entity, array $properties) {}

  /**
   * Sets a redirect on form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The cloned entity.
   */
  protected function formSetRedirect(FormStateInterface $form_state, EntityInterface $entity) {
    $form_state->setRedirect('entity.group.order_materials', ['group' => $entity->id()]);
  }

  /**
   * The _title_callback for the start form route.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to start.
   *
   * @return string
   *   The page title.
   */
  public function startTitle(GroupInterface $group) {
    return $this->t('Start %label', ['%label' => $group->label()]);
  }

}
