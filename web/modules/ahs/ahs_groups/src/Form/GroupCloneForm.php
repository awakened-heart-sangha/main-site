<?php

namespace Drupal\ahs_groups\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_clone\Event\EntityCloneEvent;
use Drupal\entity_clone\Event\EntityCloneEvents;
use Drupal\entity_clone\Form\EntityCloneForm;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements an entity Clone form for group entities.
 */
class GroupCloneForm extends EntityCloneForm {

  /**
   * The machine name of the form mode to use.
   *
   * @var string
   */
  protected $formMode = 'clone';

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('string_translation'),
      $container->get('event_dispatcher'),
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('entity_clone.settings.manager'),
      $container->get('entity_clone.service_provider'),
      $container->get('module_handler'),
      $container->get('entity.definition_update_manager')
    );

    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->entity && $this->entityTypeDefinition->hasHandlerClass('entity_clone')) {

      if ($this->entity->bundle() === 'course_template') {
        $form['type'] = [
          '#type' => 'radios',
          '#title' => $this->t('Type'),
          '#default_value' => 'course',
          '#options' => [
            'course' => $this->t('Course: for particular people or at a particular time'),
            'course_template' => $this->t('Course template: anyone eligible can start anytime'),
          ],
          '#description' => $this->t("What should this course template be cloned as?"),
        ];
      }

      if ($this->entity->bundle() === 'course_template' || $this->entity->bundle() === 'course') {
        $form['weeks'] = [
          '#type' => 'number',
          '#min' => 0,
          '#size' => 2,
          '#field_suffix' => 'weeks',
          '#title' => $this->t('Duration'),
          '#description' => $this->t("Duration of the course in weeks."),
          '#default_value' => '8',
        ];

        // Use the expected duration from the template if we have it.
        if ($this->entity->hasField('field_expected_weeks')) {
          $form['weeks']['#default_value'] = $this->entity->get('field_expected_weeks')->value;
        }
        // Else use the duration of the course if we have it.
        elseif ($this->entity->hasField('field_dates') && $sourceEnd = $this->entity->get('field_dates')->end_date) {
          $sourceStart = $this->entity->get('field_dates')->end_date;
          $seconds = $sourceEnd->getTimestamp() - $sourceStart->getTimestamp();
          $form['weeks']['#default_value'] = floor(($seconds / 86400) * 7);
        }

        $form['create_sessions'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Create sessions automatically'),
          '#description' => $this->t("Create a session for every week of the course."),
          '#default_value' => TRUE,
        ];
      }

      $form = parent::buildForm($form, $form_state);
      $accessible_elements = [
        'type',
        'message',
        'weeks',
        'create_sessions',
        'actions',
      ];
      foreach ($form as $key => $value) {
        if (is_array($value) && !in_array($key, $accessible_elements)) {
          $form[$key]['#access'] = FALSE;
        }
      }

      // Add elements of the clone view mode to the form for the source entity.
      // We use a duplicate not the original to get the values for the fields,
      // in order to centralise all the default value logic in ::getDuplicate().
      $this->attachFormDisplay($form, $this->getDuplicate(['type' => $this->entity->bundle()]));

      // If cloning as an entity of a different bundle is offered, add elements
      // form the 'clone' view mode of the offered bundles.
      if (isset($form['type'])) {
        $form['fields'][$this->entity->bundle()]['#states'] = [
          'visible' => [
            ':input[name="type"]' => ['value' => $this->entity->bundle()],
          ],
        ];
        foreach ($form['type']['#options'] as $bundleMachineName => $bundleLabel) {
          if ($bundleMachineName !== $this->entity->bundle()) {
            $duplicate = $this->getDuplicate(['type' => $bundleMachineName]);
            $this->attachFormDisplay($form, $duplicate);
            $form['fields'][$bundleMachineName]['#states'] = [
              'visible' => [
                ':input[name="type"]' => ['value' => $bundleMachineName],
              ],
            ];
          }
        }
      }

      $bundleLabel = strtolower($this->entity->type->entity->label());
      $form['uids'] = [
        '#type' => 'select2',
        '#title' => $this->t('Students'),
        '#multiple' => TRUE,
        '#autocomplete' => TRUE,
        '#target_type' => 'user',
        '#description' => $this->t("Users who will be added to the @bundle as students.", [
          '@bundle' => $bundleLabel,
        ]),
      ];

      $form['content'] = [
        '#type' => 'radios',
        '#title' => 'Content',
        '#default_value' => 'unpublish',
        '#options' => [
          'clone' => $this->t('Copy'),
          'remove' => $this->t("Don't copy"),
          'unpublish' => $this->t('Copy but unpublish'),
        ],
        '#description' => $this->t("What should happen to the content of the @bundle?", [
          '@bundle' => $bundleLabel,
        ]),
      ];

      // Show some elements only when cloning to a course,
      // not template to template.
      if (isset($form['type'])) {
        $courseOnlyElements = [
          'weeks',
          'create_sessions',
          'uids',
        ];
        foreach ($courseOnlyElements as $element) {
          if (isset($form[$element])) {
            $form[$element]['#states'] = [
              'visible' => [
                ':input[name="type"]' => ['value' => 'course'],
              ],
            ];
          }
        }
      }

    }
    return $form;
  }

  /**
   * Helper to attach form display.
   */
  protected function attachFormDisplay(array &$form, EntityInterface $entity) {
    $display = EntityFormDisplay::collectRenderDisplay($entity, $this->formMode);
    $form_state = new FormState();
    $displayForm = ['#parents' => ['fields', $entity->bundle()], '#type' => 'container'];
    $display->buildForm($entity, $displayForm, $form_state);
    $this->modifyFormDisplay($displayForm, $entity->bundle());
    $form['fields'][$entity->bundle()] = $displayForm;
  }

  /**
   * Helper to modify form dusplay.
   */
  protected function modifyFormDisplay(array &$form, $bundle) {
    $this->addVisibilityStates($form, $bundle, 'field_suggested', ['status']);
    $this->addVisibilityStates($form, $bundle, 'field_registration_open', ['status']);
    $this->addVisibilityStates($form, $bundle, 'field_registration_deadline', ['status', 'field_registration_open']);
    $this->addVisibilityStates($form, $bundle, 'field_registration_instruction', ['status', 'field_registration_open']);
    $this->addVisibilityStates($form, $bundle, 'field_registration_normal', ['status', 'field_registration_open']);
    $this->addVisibilityStates($form, $bundle, 'field_registration_redirect', [
      'status',
      'field_registration_open',
      'field_registration_normal',
    ]);
    $this->addVisibilityStates($form, $bundle, 'field_registration_url', [
      'status',
      'field_registration_open',
      ['field_registration_normal' => FALSE],
    ]);
  }

  /**
   * Helper to add form states.
   */
  protected function addVisibilityStates(array &$form, $bundle, $target, array $dependencies) {
    if (!isset($form[$target])) {
      return;
    }
    $states = [];
    foreach ($dependencies as $dependency) {
      $dependentField = $dependency;
      $positive = TRUE;
      if (is_array($dependency)) {
        $dependentField = array_keys($dependency)[0];
        $positive = array_values($dependency)[0];
      }
      if (isset($form[$dependentField])) {
        if (!empty($states)) {
          $states[] = 'and';
        }
        $states[':input[name^="fields[' . $bundle . '][' . $dependentField . ']"]'] = ['checked' => $positive];
      }
    }
    if (!empty($states)) {
      $form[$target]['#states']['visible'] = $states;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $properties = $this->getProperties($form_state);
    $duplicate = $this->getDuplicate($properties);
    if ($duplicate->hasField('field_typical_time') && $duplicate->hasField('field_private')) {
      if ($duplicate->get('field_typical_time')->isEmpty() && !(bool) $duplicate->get('field_private')->value) {
        $form_state->setError($form['fields'][$duplicate->bundle()]['field_typical_time']['widget'], $this->t('A day and time should be specified unless the @type is private.', [
          '@type' => strtolower($duplicate->type->entity->label()),
        ]));
      }
    }
  }

  /**
   * Sets a redirect on form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The cloned entity.
   */
  protected function formSetRedirect(FormStateInterface $form_state, EntityInterface $entity) {
    $form_state->setRedirect($entity->toUrl('edit-form')->getRouteName(), $entity->toUrl('edit-form')->getRouteParameters());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\entity_clone\EntityClone\EntityCloneInterface $entity_clone_handler */
    $entity_clone_handler = $this->entityTypeManager->getHandler($this->entityTypeDefinition->id(), 'entity_clone');
    $properties = $this->getProperties($form_state);
    $properties['no_suffix'] = (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting();

    $duplicate = $this->getDuplicate($properties);

    $this->eventDispatcher->dispatch(new EntityCloneEvent($this->entity, $duplicate, $properties), EntityCloneEvents::PRE_CLONE);
    $cloned_entity = $entity_clone_handler->cloneEntity($this->entity, $duplicate, $properties);
    $this->eventDispatcher->dispatch(new EntityCloneEvent($this->entity, $duplicate, $properties), EntityCloneEvents::POST_CLONE);

    $this->addMessage($cloned_entity, $properties);

    $this->formSetRedirect($form_state, $cloned_entity);
  }

  /**
   * Helper to get properties.
   */
  protected function getProperties(FormStateInterface $form_state) {
    $properties = [];
    if ($this->entityTypeManager->hasHandler($this->entityTypeDefinition->id(), 'entity_clone_form')) {
      $entity_clone_form_handler = $this->entityTypeManager->getHandler($this->entityTypeDefinition->id(), 'entity_clone_form');
    }
    if (isset($entity_clone_form_handler) && $entity_clone_form_handler) {
      $properties = $entity_clone_form_handler->getValues($form_state);
    }

    // Bring uids up a level, select2 has the values down a level.
    $properties['uids'] = $properties['uids'][0] ?? [];

    return $properties;
  }

  /**
   * Helper to get duplicate.
   */
  protected function getDuplicate(array $properties) {
    // Change the group bundle if the 'type' form element requests it.
    if (isset($properties['type']) && $properties['type'] !== $this->entity->bundle()) {
      $sourceFields = $this->entity->getFields(FALSE);
      $duplicate = $this->entityTypeManager->getStorage('group')->create(['type' => $properties['type']]);
      $excludedFields = ['id', 'uuid', 'type', 'revision_id'];
      foreach ($sourceFields as $fieldId => $fieldItems) {
        if (!in_array($fieldId, $excludedFields) && $duplicate->hasField($fieldId)) {
          $duplicate->set($fieldId, $fieldItems->getValue());
        }
      }
    }
    else {
      $duplicate = $this->entity->createDuplicate();
    }

    // Set source entity as template when cloning from template to course.
    if ($duplicate->hasField('field_template') && $this->entity->bundle() === 'course_template') {
      $duplicate->set('field_template', $this->entity);
    }

    // By default, don't clone dates.
    if ($duplicate->hasField('field_dates')) {
      $duplicate->set('field_dates', NULL);
    }
    if ($duplicate->hasField('field_typical_time')) {
      $duplicate->set('field_typical_time', NULL);
    }
    if ($duplicate->hasField('field_registration_deadline')) {
      $duplicate->set('field_registration_deadline', NULL);
    }

    // By default, don't clone leaders for courses.
    if ($duplicate->bundle() === 'course' && $duplicate->hasField('field_leaders')) {
      $duplicate->set('field_leaders', NULL);
    }

    // By default, newly cloned groups are unpublished and closed.
    if ($duplicate->hasField('field_registration_open')) {
      $duplicate->set('field_registration_open', FALSE);
    }
    $duplicate->set('status', FALSE);

    // Set values on entity using fields from
    // form mode added to GroupEntityCloneForm.
    if (isset($properties['fields'][$duplicate->bundle()])) {
      $display = EntityFormDisplay::collectRenderDisplay($duplicate, $this->formMode);
      $form_state = new FormState();
      $form_state->setValues($properties['fields'][$duplicate->bundle()]);
      $form = [];
      $display->buildForm($duplicate, $form, $form_state);
      $display->extractFormValues($duplicate, $form, $form_state);
    }

    // Provide default start and end dates as needed.
    // GroupStartForm provides a start date, and the form display may have set
    // start and end dates.
    if ($duplicate->hasField('field_dates') && empty($duplicate->get('field_dates')->end_date)) {
      $start = $duplicate->get('field_dates')->start_date;
      // Use the 'typical' time as a start date if given by form.
      // It's easier for staff to define a course by start date and duration in
      // weeks, than by start date and end date.
      if (!$start && $duplicate->hasField('field_typical_time') && !$duplicate->get('field_typical_time')->isEmpty()) {
        $start = $duplicate->get('field_typical_time')->date;
      }
      if (!$start && $duplicate->bundle() === 'course') {
        // Fallback to today as the start date for courses.
        $timestamp = $this->time->getRequestTime();
        $start = DrupalDateTime::createFromTimestamp($timestamp);
      }
      if ($start) {
        $end = $duplicate->get('field_dates')->end_date;
        if (!$end && isset($properties['weeks']) && !empty($properties['weeks'])) {
          // Subtract 1 from weeks because if the course has 6 weekly sessions
          // the gap between first and last is only the length of 5 weeks.
          $days = (($properties['weeks'] - 1) * 7);
          // Add an hour to make the course end at the end of the last session.
          $daysInterval = new \DateInterval("P{$days}DT1H");
          $end = clone $start;
          $end = $end->add($daysInterval);
        }
        if (!$end) {
          $end = $start;
        }

        $value = ['value' => $start->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)];
        if ($end) {
          $value['end_value'] = $end->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        }
        $duplicate->set('field_dates', $value);
      }
    }

    // Remove the content if requested.
    if (isset($properties['content']) && $duplicate->hasField('field_content')) {
      if ($properties['content'] === 'remove') {
        $duplicate->set('field_content', NULL);
        // Pretend the source entity has no content, in order to stop
        // ContentEntityCloneBase:cloneEntity() from cloning it.
        if ($this->entity->hasField('field_content')) {
          $this->entity->set('field_content', NULL);
        }
      }
    }

    return $duplicate;
  }

  /**
   * Helper to add message.
   */
  protected function addMessage(EntityInterface $cloned_entity, array $properties) {
    $this->messenger->addMessage($this->stringTranslationManager->translate('The entity <em>@entity (@entity_id)</em> of type <em>@type</em> was cloned.', [
      '@entity' => $this->entity->label(),
      '@entity_id' => $this->entity->id(),
      '@type' => $this->entity->getEntityTypeId(),
    ]));
  }

  /**
   * The _title_callback for the clone_form route.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to start.
   *
   * @return string
   *   The page title.
   */
  public function cloneTitle(GroupInterface $group) {
    ;
    return $this->t('Clone %label @type', [
      '%label' => $group->label(),
      '@type' => strtolower($group->type->entity->label()),
    ]);
  }

}
