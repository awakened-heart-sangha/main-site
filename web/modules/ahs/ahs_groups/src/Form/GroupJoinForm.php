<?php

namespace Drupal\ahs_groups\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Form\GroupRelationshipForm;
use Drupal\message\Entity\Message;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for joining a group.
 */
class GroupJoinForm extends GroupRelationshipForm {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Date time formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The message notify.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotify;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->time = $container->get('datetime.time');
    $form->dateFormatter = $container->get('date.formatter');
    $form->messageNotify = $container->get('message_notify.sender');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['entity_id']['#access'] = FALSE;
    $form['group_roles']['#access'] = FALSE;
    $form['path']['#access'] = FALSE;
    $form['field_participant_type']['#access'] = FALSE;

    // Force the user to give a name if they have not already.
    $user = User::load($this->currentUser()->id());
    assert($user->isAuthenticated());
    $profile = $user->get('about_profiles')->entity;
    if (empty($user->getName()) && $profile) {
      $field = $profile->get('field_name');
      if ($field->isEmpty()) {
        $settings = $field->getFieldDefinition()->getSettings();
        $components = array_filter($settings['components']);
        $form['name'] = [
          '#type' => 'name',
          '#required' => TRUE,
          '#title' => 'Your name',
        ];
        foreach (_name_translations() as $key => $title) {
          if (isset($components[$key])) {
            $form['name']['#components'][$key]['type'] = 'textfield';
            $size = !empty($settings['size'][$key]) ? $settings['size'][$key] : 60;
            $title_display = $settings['title_display'][$key] ?? 'description';
            $form['name']['#components'][$key]['title'] = Html::escape($settings['labels'][$key]);
            $form['name']['#components'][$key]['title_display'] = $title_display;
            $form['name']['#components'][$key]['size'] = $size;
            $form['name']['#components'][$key]['maxlength'] = !empty($settings['max_length'][$key]) ? $settings['max_length'][$key] : 255;
          }
          else {
            $form['name']['#components'][$key]['exclude'] = TRUE;
          }
        }
      }
    }

    // Force users to check the course sessions they can attend, purely as a
    // psychological deterrent to frivolous signup.
    $group = $this->getEntity()->getGroup();
    if ($group->bundle() === 'course') {
      $sessionsContent = $group->getRelationships('group_node:session');

      $sessionsContent = array_filter($sessionsContent, static function ($content_item) {
        return ($content_item->hasField('field_primary') && $content_item->field_primary->value);
      });

      // Filter out past sessions.
      $sessionsContent = array_filter($sessionsContent, function ($contentItem, $k) {
        $date = $contentItem->getEntity()->get('field_datetime')->date;
        return $date && $date->getTimestamp() > $this->time->getRequestTime();
      }, ARRAY_FILTER_USE_BOTH);

      if (!empty($sessionsContent)) {
        usort($sessionsContent, function ($a, $b) {
          $dateA = $a->getEntity()->get('field_datetime')->date;
          $dateB = $b->getEntity()->get('field_datetime')->date;
          $valA = $dateA ? $dateA->getTimestamp() : 0;
          $valB = $dateB ? $dateB->getTimestamp() : 0;
          return $valA <=> $valB;
        });

        $form['sessions_info'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => 'The central feature of this course is a series of scheduled online group discussions. Please tick all of the sessions that you commit to attending.',
        ];
        foreach ($sessionsContent as $sessionContent) {
          $session = $sessionContent->getEntity();
          $date = $session->get('field_datetime')->date;
          $dateFormatted = !$date ? '' : $this->dateFormatter->format($date->getTimestamp(), 'custom', 'l j M g:ia');
          $options[$session->id()] = $session->label() . ' - ' . $dateFormatted;
        }
        $form['sessions'] = [
          '#type' => 'checkboxes',
          '#title' => 'Sessions',
          '#options' => $options,
        ];
      }
    }

    // Because this form is often empty of fields, provide a simple confirmation
    // message to help UX.
    $form['message'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Please confirm you wish to register.',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Register');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Set participant type as 'student'.
    $this->entity->set('field_participant_type', '69');

    $return = parent::save($form, $form_state);

    /** @var \Drupal\group\Entity\GroupInterface $group */
    $group = $this->getEntity()->getGroup();
    $form_state->setRedirect('entity.group.order_materials', ['group' => $group->id()]);

    if ($group->bundle() === 'event') {
      $message = Message::create([
        'template' => 'event_registration_confirmation',
      ]);
      $message->set('field_group', $group);
      $message->set('arguments', [
        '@group-absolute-url' => $group->toUrl('canonical', ['absolute' => TRUE])->toString(TRUE)->getGeneratedUrl(),
      ]);
      $message->save();
      $this->messageNotify->send($message, ['mail' => $this->currentUser()->getEmail(), 'reply' => 'events@ahs.org.uk'], 'email');
    }

    if ($form_state->hasValue('name')) {
      $value = $form_state->getValue('name');
      $user = User::load($this->currentUser()->id());
      assert($user->isAuthenticated());
      $profile = $user->get('about_profiles')->entity;
      $profile->field_name[0] = [
        'given' => $value['given'],
        'family' => $value['family'],
      ];
      $profile->save();
    }

    return $return;
  }

}
