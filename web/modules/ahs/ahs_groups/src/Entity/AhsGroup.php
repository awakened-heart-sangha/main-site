<?php

namespace Drupal\ahs_groups\Entity;

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\user\UserInterface;
use Drupal\ahs_miscellaneous\EntityWithTimezoneInterface;

/**
 * Defines an ahs group entity class.
 *
 * This adds additional methods to the group entity.
 */
class AhsGroup extends Group implements EntityWithTimezoneInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The user groups manager service.
   *
   * @var \Drupal\ahs_groups\UserGroupsManager
   */
  protected $userGroupsManager;

  /**
   * The user experience evaluator service.
   *
   * @var \Drupal\ahs_training\UserExperienceEvaluator
   */
  protected $userExperienceEvaluator;

  /**
   * AhsGroup constructor.
   */
  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);
    $this->time = \Drupal::service('datetime.time');
    $this->userGroupsManager = \Drupal::service('ahs_groups.user_groups_manager');
    $this->userExperienceEvaluator = \Drupal::service('ahs_training.user_experience_evaluator');
  }

  /**
   * Helper to check eligibility conditions.
   */
  public function areEligibilityConditionsMetByUser(UserInterface $user) {
    // If there are no eligibility conditions, everyone is eligible.
    $conditions = $this->get('field_eligibility_conditions')->referencedEntities();
    if (empty($conditions)) {
      return TRUE;
    }
    return $this->userExperienceEvaluator->isAnyConditionMetByUser($conditions, $user->id(), $this->getConditionsEvaluationDate());
  }

  /**
   * Helper to check suggestion conditions.
   */
  public function areSuggestionConditionsMetByUser(UserInterface $user) {
    // If there are no suggestion conditions, then treat as unconditional.
    if (!$this->hasSuggestionConditions()) {
      return TRUE;
    }
    $conditions = $this->get('field_suggestion_conditions')->referencedEntities();
    return $this->userExperienceEvaluator->isAnyConditionMetByUser($conditions, $user->id(), $this->getConditionsEvaluationDate());
  }

  /**
   * Helper to get date.
   */
  public function getConditionsEvaluationDate() {
    if (!$date = $this->getStartDate()) {
      $date = DrupalDateTime::createFromTimestamp($this->time->getRequestTime());
    }
    return $date;
  }

  /**
   * Get the group start date, if it has one.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   A start date object, or null.
   */
  public function getStartDate() {
    return $this->hasField('field_dates') ? $this->get('field_dates')->start_date : NULL;
  }

  /**
   * Get how far in the future the group end date is.
   *
   * Negative numbers indicate the end date is in the past.
   *
   * @return bool|null
   *   How far in the future in seconds, or null if no end date.
   */
  public function getStartDateOffset() {
    $startDate = $this->getStartDate();
    if (is_null($startDate)) {
      return;
    }
    $time = $this->time->getCurrentTime();
    $offset = $startDate->getTimestamp() - $time;
    return $offset;
  }

  /**
   * Get the group end date.
   *
   * Falling back to the start date if it has no end date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   An end date object, or null.
   */
  public function getEndDate() {
    return $this->hasField('field_dates') ? $this->get('field_dates')->end_date : $this->getStartDate();
  }

  /**
   * Get how far in the future the group end date is.
   *
   * Negative numbers indicate the end date is in the past.
   *
   * @return bool|null
   *   How far in the future in seconds, or null if no end date.
   */
  public function getEndDateOffset() {
    $endDate = $this->getEndDate();
    if (is_null($endDate)) {
      return;
    }
    $time = $this->time->getCurrentTime();
    $offset = $endDate->getTimestamp() - $time;
    return $offset;
  }

  /**
   * Get the host entity wrappers for the group's registration variations.
   *
   * A group's registration variations are the commerce product variations
   * managed from the group's field_product. A 'host entity' is a wrapper
   * class around one of these, provided and used by the registration module.
   *
   * @return \Drupal\registration\HostEntityInterface[]
   *   An array of host entity wrappers for the group's registration variations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getRegistrationHostEntities() {
    $hosts = [];

    if ($this->hasField('field_product')) {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      foreach ($this->get('field_product')->referencedEntities() as $product) {

        /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product */
        foreach ($product->getVariations() as $variation) {
          $hosts[] = $this->entityTypeManager()
            ->getHandler('registration', 'host_entity')
            ->createHostEntity($variation);
        }
      }
    }

    return $hosts;
  }

  /**
   * Get the latest completed registration for the group for a user.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   (optional) The user. Defaults to the current user.
   * @param array $states
   *   (optional) The states to filter by.
   *
   * @return \Drupal\registration\Entity\RegistrationInterface|null
   *   The registrations. If multiple, the most recent completed should be first.
   */
  public function getRegistrations(AccountInterface $user = NULL, $states = []) {
    $user = $user ?? \Drupal::currentUser();
    if ($user->isAuthenticated() && $this->hasField('field_product') && $this->get('field_product')->entity) {
      $variation_ids = [];

      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      foreach ($this->get('field_product')->referencedEntities() as $product) {
        $variation_ids = array_merge($variation_ids, $product->getVariationIds());
      }

      $registration_storage = \Drupal::entityTypeManager()->getStorage('registration');
      $registration_query = $registration_storage->getQuery();
      $registration_query->accessCheck(FALSE);
      $registration_query->condition('user_uid', $user->id());
      $registration_query->condition('entity_id', $variation_ids, 'IN');
      $registration_query->condition('entity_type_id', 'commerce_product_variation');
      if ($states) {
        $registration_query->condition('state', $states, 'IN');
      }
      $registration_query->sort('completed', 'DESC');
      $registration_query->sort('changed', 'DESC');
      $registration_query->range(0, 1);
      $registration_ids = $registration_query->execute();
      return $registration_storage->loadMultiple($registration_ids) ?? [];
    }
    return [];
  }

  /**
   * Get a user-friendly label for the bundle.
   *
   * @return string
   *   A lowercase bundle label.
   */
  public function getTypeLabel() {
    if ($this->bundle() === 'course_template') {
      return 'course';
    }
    else {
      return strtolower($this->type->entity->label());
    }
  }

  /**
   * Helper to get eligibility conditions.
   */
  public function hasEligibilityConditions() {
    return $this->hasField('field_eligibility_conditions') && !$this->get('field_eligibility_conditions')->isEmpty();
  }

  /**
   * Whether the group has a special registration instruction.
   *
   * @return bool
   *   Value if the group has registration instruction.
   */
  public function hasRegistrationInstruction() {
    return $this->hasField('field_registration_instruction') && !$this->get('field_registration_instruction')->isEmpty();
  }

  /**
   * Whether the group has a special registration url.
   *
   * @return bool
   *   Value if the group has registration URL.
   */
  public function hasRegistrationUrl() {
    return $this->hasField('field_registration_url') && !$this->get('field_registration_url')->isEmpty();
  }

  /**
   * Determine if a user is a member of the group.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user.
   *
   * @return bool
   *   Whether the user is a member of the group.
   */
  public function hasMember(UserInterface $user) {
    return (bool) $this->getMember($user);
  }

  /**
   * Get the registration system in use for the group.
   *
   * * 'group' is directly joining the group using GroupJoinForm.
   * * 'registration' is using the registration and commerce_registration
   * modules.
   * * 'manual' does not automatically add the user to the group,
   * staff have to do that.
   *
   * @return string
   *   Registration type.
   */
  public function getRegistrationSystem() {
    if ($this->hasField('field_registration_system')) {
      if (!$this->get('field_registration_system')->isEmpty()) {
        return $this->get('field_registration_system')->value;
      }
    }
    if ($this->hasField('field_registration_normal')) {
      if (!$this->get('field_registration_normal')->isEmpty()) {
        return $this->get('field_registration_normal')->value ? 'group' : 'manual';
      }
    }
    return 'manual';
  }

  /**
   * Helper to get registration deadline.
   */
  protected function getRegistrationDeadline() {
    if ($this->getRegistrationSystem() === 'registration') {
      $deadlines = [];
      foreach ($this->getRegistrationHostEntities() as $host) {
        $settings = $host->getSettings();
        $close = $settings->get('close');
        // Ignore disabled variations.
        if ($settings->getSetting('status')) {
          if ($close->isEmpty()) {
            // The group has no deadline if any enabled variation has no close date.
            return NULL;
          }
          else {
            // Remember the value for easy storage,
            // and the timestamp for easy comparison.
            $deadlines[$close->date->getTimestamp()] = $close->date;
          }
        }
      }
      // Deadline is the latest close value if all enabled variations have a close date.
      return empty($deadlines) ? NULL : $deadlines[max(array_keys($deadlines))];
    }

    $hasDeadline = $this->hasField('field_registration_deadline') && !$this->get('field_registration_deadline')->isEmpty();
    return $hasDeadline && $this->isRegistrationOpen() ? $this->get('field_registration_deadline')->date : NULL;
  }

  /**
   * Whether there is a registration deadline for the group that has not passed.
   *
   * @return bool
   *   Value if the group has not a registration deadline.
   */
  public function hasRegistrationDeadlinePending() {
    return $this->getRegistrationDeadline() && !$this->hasRegistrationDeadlinePassed();
  }

  /**
   * Whether there is a registration deadline for the group that has passed.
   *
   * @return bool
   *   Value if the group has a registration deadline.
   */
  public function hasRegistrationDeadlinePassed() {
    $time = $this->time->getCurrentTime();
    $deadline = $this->getRegistrationDeadline()?->getTimestamp();
    return $deadline && $deadline < $time;
  }

  /**
   * Whether the group has any suggestions.
   */
  public function hasSuggestionConditions() {
    return $this->hasField('field_suggestion_conditions') && !$this->get('field_suggestion_conditions')->isEmpty();
  }

  /**
   * Whether the user is already engaging with this course or template.
   *
   * @return bool
   *   Value if the user is engaging.
   */
  public function hasUserAlreadyEngaged(UserInterface $user) {
    if ($this->bundle() === 'course_template') {
      return $this->userGroupsManager->hasUserEngagedWithCourseTemplate($user, $this);
    }
    return $this->userGroupsManager->isUserGroupMember($user->id(), $this);
  }

  /**
   * Whether the user already has all the experiences, if there are any.
   *
   * Regardless of experience effective date,
   * i.e. include in-progress experiences.
   *
   * @return bool
   *   Value if the user has all experiences.
   */
  public function hasUserAlreadyExperiencesGiven(UserInterface $user) {
    $experienceIds = array_column($this->get('field_experiences_given')->getValue(), 'target_id');
    if ($experienceIds && $this->userExperienceEvaluator->hasAllExperiences($experienceIds, $user->id())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is this group finished, i.e. it's end date is in the past.
   *
   * @return bool
   *   Value if this group finished.
   */
  public function isFinished() {
    $offset = $this->getEndDateOffset();
    return !is_null($offset) && $offset < 0;
  }

  /**
   * Is this group started, i.e. it's start date is in the past.
   *
   * @return bool
   *   Value if this group started.
   */
  public function isStarted() {
    $offset = $this->getStartDateOffset();
    return !is_null($offset) && $offset < 0;
  }

  /**
   * Whether the group is available to anyone, now or in the future.
   *
   * @return bool
   *   Whether the group is available.
   */
  public function isAvailable() {
    if (!$this->isPublished()) {
      return FALSE;
    }

    if ($this->isFinished()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Whether the group is available to the user.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user.
   *
   * @return bool
   *   Whether the group is open to the user.
   */
  public function isAvailableToUser(UserInterface $user) {
    $isParticipant = $this->bundle() === 'course_template' ? FALSE : $this->userGroupsManager->isUserGroupMember($user->id(), $this);
    return $this->isAvailable() && !$isParticipant && $this->areEligibilityConditionsMetByUser($user);
  }

  /**
   * Determine whether registration is now available for a group.
   *
   * The group must be published, explicitly open, not finished,
   * and not past its registration deadline.
   */
  public function isRegistrationAvailable() {
    // Registration requires the group to be published & not finished.
    if (!$this->isAvailable()) {
      return FALSE;
    }

    // Registration must be explicitly open.
    if (!$this->isRegistrationOpen()) {
      return FALSE;
    }

    // Registration is closed if there is a passed registration deadline.
    if ($this->hasRegistrationDeadlinePassed()) {
      return FALSE;
    }

    // Check capacity and other additional constraints
    // if using 'registration' module.
    if ($this->getRegistrationSystem() === 'registration') {
      foreach ($this->getRegistrationHostEntities() as $host) {
        if ($host->isAvailableForRegistration()) {
          return TRUE;
        }
      }
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Whether the user is able to register for the group.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user.
   *
   * @return bool
   *   Whether user is able to register for the group.
   */
  public function isRegistrationAvailableToUser(UserInterface $user) {
    return $this->isRegistrationAvailable() && $this->isAvailableToUser($user);
  }

  /**
   * Determine whether registration is now open for a group.
   *
   * This simply checks the boolean field field_registration_opn.
   * For most purposes use ::isRegistrationAvailable().
   */
  public function isRegistrationOpen() {
    if ($this->getRegistrationSystem() === 'registration') {
      foreach ($this->getRegistrationHostEntities() as $host) {
        if ($host->getSetting('status')) {
          return TRUE;
        }
      }
      return FALSE;
    }

    return $this->hasField('field_registration_open') && !$this->get('field_registration_open')->isEmpty() && !empty($this->get('field_registration_open')->value);
  }

  /**
   * Is the group private.
   */
  public function isPrivate() {
    return $this->hasField('field_private') && !$this->get('field_private')->isEmpty() && !empty($this->get('field_private')->value);
  }

  /**
   * Whether the group is online.
   */
  public function isOnline() {
    if ($this->bundle() === 'event') {
      return $this->hasField('field_online') && !$this->get('field_online')->isEmpty() && !empty($this->get('field_online')->value);
    }
    return TRUE;
  }


  /**
   * Is the group self-paced.
   */
  public function isSelfPaced() {
    return $this->hasField('field_self_paced') && !$this->get('field_self_paced')->isEmpty() && !empty($this->get('field_self_paced')->value);
  }

  /**
   * Is a group marked as suggested, to at least some people.
   */
  public function isSuggested() {
    return $this->hasField('field_suggested') && !$this->get('field_suggested')->isEmpty() && !empty($this->get('field_suggested')->value);
  }

  /**
   * Is a group is suitable to suggest to anyone.
   */
  public function isSuggestable() {
    // Groups must be available to be suggested.
    if (!$this->isAvailable()) {
      return FALSE;
    }

    // Groups must be explicitly suggested.
    if (!$this->isSuggested()) {
      return FALSE;
    }

    // Only future groups are suggested, not currently running groups.
    if ($this->isStarted()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Is a group suggested to a particular user.
   */
  public function isSuggestedToUser(UserInterface $user) {
    // Groups must be suggested and available.
    if (!$this->isSuggestable()) {
      return FALSE;
    }

    // There's no point in suggesting a group a user can't join.
    if (!$this->isRegistrationAvailableToUser($user)) {
      return FALSE;
    }

    // Don't suggest templates the user has already started a course from.
    // This is not covered by the experience logic below, because experiences
    // could be added to a template after the user starts it.
    if ($this->hasUserAlreadyEngaged($user)) {
      return FALSE;
    }

    // If the user already has the experiences the group gives, then it
    // should not be suggested.
    if ($this->hasUserAlreadyExperiencesGiven($user)) {
      return FALSE;
    }

    $met = $this->areSuggestionConditionsMetByUser($user);
    return $met;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);

    // Make events open for online participation by default.
    if ($values['type'] === 'event') {
      $product_starage = \Drupal::entityTypeManager()->getStorage('commerce_product');

      $product = $product_starage->create([
        'type' => 'ahs_registration',
        'title' => 'Online',
      ]);

      $values += [
        // Use normal registration by default,
        // even on automatically created entities.
        'field_registration_system' => 'group',
        // Assign a default registration variation, to give better UX if
        // the registration system is changed to 'registration' later.
        'field_product' => $product,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // Create a new variation in the preSave() method
    // in case the group has NOT been saved.
    if ($this->hasField('field_product') && $this->get('field_product')->entity) {
      $products = [];

      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      foreach ($this->get('field_product')->referencedEntities() as $product) {
        if ($product->isNew()) {
          $product->save();
        }

        if (!$product->hasVariations()) {
          $variation_starage = $this->entityTypeManager()->getStorage('commerce_product_variation');
          $variation = $variation_starage->create([
            'type' => 'ahs_registration',
            'field_registration_type' => 'online',
            'title' => $product->getTitle(),
            'price' => new Price(0, 'GBP'),
          ]);

          $product->addVariation($variation);
          $product->save();
        }

        $products[] = $product;
      }

      $this->set('field_product', $products);
    }

    // For BC, save data from the registration variations into
    // the legacy registration fields.
    if ($this->getRegistrationSystem() === 'registration') {
      $this->set('field_registration_open', $this->isRegistrationOpen());
      $deadline = $this->getRegistrationDeadline() ? $this->getRegistrationDeadline()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT) : '';
      $this->set('field_registration_deadline', $deadline);
    }
  }

  /**
   * Should eligibility and suggestions conditions be shown to students?
   *
   * Defaults to yes.
   *
   * @return bool
   *   Value of field conditions field.
   */
  public function showConditions() {
    if (!$this->hasField('field_show_conditions')) {
      return TRUE;
    }
    elseif ($this->get('field_show_conditions')->isEmpty() || is_null($this->get('field_show_conditions')->value)) {
      return TRUE;
    }
    else {
      return (bool) $this->get('field_show_conditions')->value;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function hasVariation(ProductVariationInterface $variation) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimezone(): string {
    // First try to get timezone from venue if it exists
    if ($this->hasField('field_venue') && !$this->get('field_venue')->isEmpty()) {
      $venue = $this->get('field_venue')->entity;
      if ($venue && $venue->hasField('field_timezone') && !$venue->get('field_timezone')->isEmpty()) {
        return $venue->get('field_timezone')->value;
      }
    }

    // Fallback to site default
    return \Drupal::config('system.date')->get('timezone.default');
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsWithTimezone(): array {
    return ['field_dates', 'field_typical_time'];
  }

  /**
   * {@inheritdoc}
   */
  public function willBeAttendedFromMultipleTimezones(): bool {
    return $this->isOnline() && !$this->isFinished();
  }
}
