<?php

namespace Drupal\ahs_groups\EntityClone;

use Drupal\ahs_training\Entity\TrainingRecord;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_clone\EntityClone\Content\ContentEntityCloneBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An entity clone handler for groups.
 */
class GroupEntityClone extends ContentEntityCloneBase {

  /**
   * The source entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->logger = $container->get('logger.factory')->get('ahs_groups');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function cloneEntity(EntityInterface $entity, EntityInterface $cloned_entity, array $properties = [], array &$already_cloned = []) {
    // The current user is always the new group owner.
    if (isset($this->currentUser) && $this->currentUser->isAuthenticated()) {
      $cloned_entity->setOwnerId($this->currentUser->id());
    }
    elseif (isset($properties['uids']) && is_array($properties['uids']) && count($properties['uids']) === 1) {
      $cloned_entity->setOwnerId($properties['uids'][0]);
    }

    // Clone children and save. Set the entity as class
    // property so it is available to ::fieldIsClonable().
    $this->entity = $entity;
    $properties['no_suffix'] = (int) \Drupal::service('entity_clone.settings.manager')->getExcludeClonedSetting();
    $cloned_entity = parent::cloneEntity($entity, $cloned_entity, $properties, $already_cloned);

    // Unpublish the content if requested. This must come after the parent
    // method has cloned the paragraphs, in order to not unpublish paragraphs
    // referenced from the original entity.
    if (isset($properties['content']) && $cloned_entity->hasField('field_content')) {
      if ($properties['content'] === 'unpublish') {
        $paragraphs = $cloned_entity->get('field_content')->referencedEntities();
        foreach ($paragraphs as $paragraph) {
          $paragraph->setUnpublished()->save();
        }
      }
    }

    // Create group sessions.
    if (isset($properties['create_sessions']) && $properties['create_sessions'] == TRUE) {
      if (isset($properties['weeks']) && $cloned_entity->hasField('field_typical_time') && !$cloned_entity->get('field_typical_time')->isEmpty()) {
        $weeks = (int) $properties['weeks'];
        $date = clone $cloned_entity->get('field_typical_time')->date;
        for ($week = 0; $week < $weeks; $week++) {
          $session = Node::create([
            'type' => 'session',
            'title' => "Week " . ($week + 1),
            'field_leader' => $cloned_entity->get('field_leaders')->getValue(),
            'field_session_type' => 72,
            'field_datetime' => $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
          ]);
          $session->save();
          $cloned_entity->addRelationship($session, 'group_node:session', ['field_primary' => TRUE]);
          $interval = new \DateInterval("P7D");
          $date = $date->add($interval);
        }
      }
    }

    // Add specified users as group members.
    // Leaders are added automatically when groups are saved.
    if ($cloned_entity->bundle() !== 'course_template') {
      if (isset($properties['uids']) && is_array($properties['uids'])) {
        foreach ($properties['uids'] as $uid) {
          $user = User::load($uid);
          if (!$user) {
            $this->logger->error("Could not load user $uid from students field when cloning group. Full structure: " . print_r($properties['uids'], TRUE));
          }
          else {
            $cloned_entity->addMember($user, [
              'field_participant_type',
              TrainingRecord::STUDENT_TERM_ID,
            ]);
          }
        }
      }
    }

    return $cloned_entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function setClonedEntityLabel(EntityInterface $original_entity, EntityInterface $cloned_entity, array $properties) {
    // Don't change the group title.
  }

  /**
   * Determines if a field is clonable.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return bool
   *   TRUE if the field is clonable; FALSE otherwise.
   */
  protected function fieldIsClonable(FieldDefinitionInterface $field_definition) {
    // Clonable fields must exist on both source and clone. We can't assume
    // this is true because \Drupal\ahs_groups\Form\GroupCloneForm allows
    // cloning to a different bundle.
    if (!$this->entity->hasField($field_definition->getName())) {
      return FALSE;
    }

    // Only allow specific group entity reference fields to be cloned.
    // The parent implementation will clone other reference fields like
    // field_leaders even if we have unset them on the clone.
    $clonable_field_names = [
      'field_eligibility_conditions',
      'field_suggestion_conditions',
      'field_content',
      'field_registration_variations',
      'field_product',
    ];

    $field_is_clonable = in_array($field_definition->getName(), $clonable_field_names, TRUE);
    if (($field_definition instanceof FieldConfigInterface) && $field_is_clonable) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Fetches the properties of a child entity.
   *
   * @param array $properties
   *   Properties of the clone operation.
   * @param \Drupal\Core\Field\FieldConfigInterface $field_definition
   *   The field definition.
   * @param \Drupal\Core\Entity\EntityInterface $referenced_entity
   *   The field's target entity.
   *
   * @return array
   *   Child properties.
   */
  protected function getChildProperties(array $properties, FieldConfigInterface $field_definition, EntityInterface $referenced_entity) {
    // Recursive field ids in $properties use source bundle, but are looked up
    // by cloned bundle. This is a problem because this handler allows cloning
    // to different bundle. We reset the properties to use cloned bundle.
    $field_definition_id_parts = explode(".", $field_definition->id());
    $field_definition_id_parts[1] = $this->entity->bundle();
    $source_field_definition_id = implode(".", $field_definition_id_parts);
    if (isset($properties['recursive'][$source_field_definition_id]['references'][$referenced_entity->id()])) {
      if ($field_definition_id_parts[0] === 'group' && $field_definition->id() !== $source_field_definition_id) {
        $properties['recursive'][$field_definition->id()]['references'][$referenced_entity->id()] = $properties['recursive'][$source_field_definition_id]['references'][$referenced_entity->id()];
        unset($properties['recursive'][$source_field_definition_id]['references'][$referenced_entity->id()]);
      }
    }
    return parent::getChildProperties($properties, $field_definition, $referenced_entity);
  }

}
