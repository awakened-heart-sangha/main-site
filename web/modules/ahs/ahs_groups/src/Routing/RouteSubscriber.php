<?php

namespace Drupal\ahs_groups\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // onAlterRoutes() is a method in
    // RouteSubscriberBase, wrapping alterRoutes().
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -100];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Use /manage instead of /edit for group edit.
    if ($route = $collection->get('entity.group.edit_form')) {
      $route->setPath('/group/{group}/manage');
    }
    // Customise the Group join title.
    if ($route = $collection->get('entity.group.join')) {
      $route->setDefault('_title_callback', '\Drupal\ahs_groups\Controller\GroupMembershipController::joinTitle');
      $route->setRequirement('_ahs_groups_registerable', 'TRUE');
    }

    // Customise the group clone form.
    if ($route = $collection->get('entity.group.clone_form')) {
      $route->setDefault('_form', '\Drupal\ahs_groups\Form\GroupCloneForm');
      $route->setDefault('_title_callback', '\Drupal\ahs_groups\Form\GroupCloneForm::cloneTitle');
    }
  }

}
