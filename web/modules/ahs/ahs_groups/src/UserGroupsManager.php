<?php

namespace Drupal\ahs_groups;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\ahs_training\UserExperienceEvaluator;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\user\UserInterface;

/**
 * Evaluate which users meet the conditions for which groups.
 */
class UserGroupsManager {

  /**
   * The user experience evaluator service.
   *
   * @var \Drupal\ahs_training\UserExperienceEvaluator
   */
  protected $userExperienceEvaluator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The group membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoader
   */
  protected $membershipLoader;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The groups users are already members of.
   *
   * Keyed by user id, each value is an array of group ids.
   *
   * @var array
   */
  protected $userGroupIds = [];

  /**
   * The templates referenced from groups users are members of.
   *
   * Keyed by user id, each value is an array of template group ids.
   *
   * @var array
   */
  protected $userCourseTemplateIds = [];

  /**
   * Constructs a new TrainingCondition object.
   */
  public function __construct(UserExperienceEvaluator $user_experience_evaluator, EntityTypeManagerInterface $entity_type_manager, TimeInterface $time, GroupMembershipLoaderInterface $membership_loader, Connection $database, EntityFieldManagerInterface $entity_field_manager) {
    $this->userExperienceEvaluator = $user_experience_evaluator;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->membershipLoader = $membership_loader;
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Helper to get available groups.
   */
  public function getAvailableGroupsForUser(UserInterface $user) {
    $visibleGroups = $this->getVisibleGroups();
    return array_filter($visibleGroups, function (AhsGroup $visibleGroup) use ($user) {
      return $visibleGroup->isAvailableToUser($user);
    });
  }

  /**
   * Helper to get suggestions for the group.
   */
  public function getSuggestedGroupsForUser(UserInterface $user) {
    $visibleGroups = $this->getVisibleGroups();
    return array_filter($visibleGroups, function (AhsGroup $visibleGroup) use ($user) {
      return $visibleGroup->isSuggestedToUser($user);
    });
  }

  /**
   * Helper to get participating groups.
   */
  public function getParticipatingGroupsForUser(UserInterface $user) {
    $participatingGroups = Group::loadMultiple($this->getUserGroupIds($user->id()));
    return array_filter($participatingGroups, function (AhsGroup $group) {
      return in_array($group->bundle(), ['course', 'event']) && $group->isPublished();
    });
  }

  /**
   * Get an array of groups to consider promoting.
   *
   * @return \Drupal\ahs_groups\Entity\AhsGroup[]
   *   An array of groups.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getVisibleGroups() {
    $groupQuery = $this->entityTypeManager->getStorage('group')->getQuery()->accessCheck(FALSE);

    // Only certain group types can be promoted.
    $promotedTypes = [
      'course',
      'event',
      'course_template',
    ];
    $groupQuery->condition('type', $promotedTypes, 'IN');

    // Only public published groups can be promoted.
    $groupQuery->condition('status', TRUE);
    $groupQuery->condition('field_private', FALSE);

    // Only promote groups with dates if they have not ended.
    $time = $this->time->getCurrentTime();
    $date = DrupalDateTime::createFromTimestamp($time);
    $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $date = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $orCondition = $groupQuery->orConditionGroup();
    // Be careful in case some groups have a start date but no explicit
    // end date. Templates have no start date, allow them.
    $orCondition->notExists('field_dates.value')
    // Groups that have not started are allowed.
      ->condition('field_dates.value', $date, '>');
    $andCondition = $groupQuery->andConditionGroup();
    // Groups that have an explicit end date are allowed only
    // if the end date is in the future.
    $andCondition->exists('field_dates.end_value')
      ->condition('field_dates.end_value', $date, '>');
    $orCondition->condition($andCondition);
    $groupQuery->condition($orCondition);

    $groupIds = $groupQuery->execute();
    $groups = Group::loadMultiple($groupIds);
    return $groups;
  }

  /**
   * Determine if a user is a member of a group.
   *
   * @param int $uid
   *   A user id.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   A group.
   *
   * @return bool
   *   Whether then user is a member of the group.
   */
  public function isUserGroupMember($uid, $group) {
    $userGroupIds = $this->getUserGroupIds($uid);
    return in_array($group->id(), $userGroupIds);
  }

  /**
   * Get the ids of the groups a user is a member of.
   *
   * @param int $uid
   *   The user id.
   *
   * @return array
   *   An array of group ids.
   */
  protected function getUserGroupIds($uid) {
    if (!isset($this->userGroupIds[$uid])) {
      $user = $this->entityTypeManager->getStorage('user')->load($uid);
      $this->userGroupIds[$uid] = [];
      foreach ($this->membershipLoader->loadByUser($user) as $group_membership) {
        $this->userGroupIds[$uid][] = $group_membership->getGroup()->id();
      }
    }
    return $this->userGroupIds[$uid];
  }

  /**
   * Determine if a user is a member of a group.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user entity.
   * @param \Drupal\group\Entity\GroupInterface $template
   *   The template group.
   *
   * @return bool
   *   Whether the user is a member of a group that references the template.
   */
  public function hasUserEngagedWithCourseTemplate(UserInterface $user, GroupInterface $template) {
    if ($template->bundle() !== 'course_template') {
      return FALSE;
    }
    return in_array($template->id(), $this->getUserCourseTemplateIds($user));
  }

  /**
   * Get all the course templates the user has engage with.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array
   *   Ids of course templates the user has engaged with.
   */
  protected function getUserCourseTemplateIds(UserInterface $user) {
    if (!isset($this->userCourseTemplateIds[$user->id()])) {
      // If the user is not a member of any groups, they can't have engaged
      // with any course templates.
      $userGroupIds = $this->getUserGroupIds($user->id());
      if (empty($userGroupIds)) {
        return [];
      }

      // Find groups that are the template for groups the user is a member of.
      // Groups reference their template from field_template.
      // Identify the table and column name to use in query.
      $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions('group');
      /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
      $table_mapping = $this->entityTypeManager->getStorage('group')->getTableMapping($field_storage_definitions);
      /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $field_storage_definition */
      $tableName = $table_mapping->getFieldTableName('field_template');
      $fieldStorageDefinition = $field_storage_definitions['field_template'];
      $columnName = $table_mapping->getFieldColumnName($fieldStorageDefinition, 'target_id');

      $query = $this->database->select($tableName, 'gft');
      $query->condition('gft.entity_id', $userGroupIds, 'IN');
      $query->addField('gft', $columnName);
      $this->userCourseTemplateIds[$user->id()] = $query->execute()
        ->fetchAll(\PDO::FETCH_COLUMN | \PDO::FETCH_UNIQUE, 0);
    }
    return $this->userCourseTemplateIds[$user->id()];
  }

  /**
   * Get all the courses a user has done that reference a template.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param \Drupal\group\Entity\GroupInterface $template
   *   The template.
   *
   * @return array
   *   Ids of courses.
   */
  public function getUserCourseIdsFromTemplate(UserInterface $user, GroupInterface $template) {
    // Only templates can have child course.
    if ($template->bundle() !== 'course_template') {
      return [];
    }

    // If the user is not a member of any groups, they can't have engaged
    // with any courses from templates.
    $userGroupIds = $this->getUserGroupIds($user->id());
    if (empty($userGroupIds)) {
      return [];
    }

    $courseIds = $this->entityTypeManager
      ->getStorage('group')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'course')
      ->condition('id', $userGroupIds, 'IN')
      ->condition('field_template', $template->id())
      ->execute();
    return $courseIds;
  }

}
