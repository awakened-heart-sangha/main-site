<?php

namespace Drupal\ahs_groups\Plugin\Group\RelationHandler;

use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface;
use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderTrait;

/**
 * Provides group permissions for ahs_groups_group_paragraph entities.
 */
class GroupParagraphPermissionProvider implements PermissionProviderInterface {

  use PermissionProviderTrait;

  /**
   * Constructs a new GroupParagraphPermissionProvider.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface $parent
   *   The parent permission provider.
   */
  public function __construct(PermissionProviderInterface $parent) {
    $this->parent = $parent;
  }

  /**
   * Provides a permission to view paragraphs that are public.
   */
  public function getEntityViewPrivatePermission($scope = 'any') {
    // View own is not implemented.
    if ($scope === 'any') {
      return "view any private $this->pluginId entity";
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPermissions() {
    $permissions = $this->parent->buildPermissions();

    $permissions[$this->getEntityViewPrivatePermission()] = [
      'title' => 'Entity: View any private paragraph entities',
    ];

    // Remove permissions except view & update.
    $permissions = array_filter($permissions, function ($key) {
      return strpos($key, 'view ') === 0 || strpos($key, 'update ') === 0;
    }, ARRAY_FILTER_USE_KEY);

    // Remove update own.
    unset($permissions[$this->getPermission('update', 'entity', 'own')]);

    // Remove relation permissions.
    $permissions = array_filter($permissions, function ($key) {
      return substr($key, -strlen('entity')) === 'entity';
    }, ARRAY_FILTER_USE_KEY);

    return $permissions;
  }

}
