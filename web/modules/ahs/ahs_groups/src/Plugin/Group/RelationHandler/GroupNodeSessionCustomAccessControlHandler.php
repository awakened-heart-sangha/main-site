<?php

namespace Drupal\ahs_groups\Plugin\Group\RelationHandler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Plugin\Group\RelationHandler\AccessControlInterface;
use Drupal\group\Plugin\Group\RelationHandler\AccessControlTrait;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides access control for group nodes that distinguishes any groups.
 */
class GroupNodeSessionCustomAccessControlHandler implements AccessControlInterface {

  use AccessControlTrait;

  /**
   * Constructs a new GroupNodeSessionCustomAccessControlHandler.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\AccessControlInterface $parent
   *   The parent permission provider.
   */
  public function __construct(AccessControlInterface $parent) {
    $this->parent = $parent;
  }

  /**
   * {@inheritdoc}
   */
  public function entityAccess(EntityInterface $entity, $operation, AccountInterface $account, $return_as_object = FALSE) {
    /** @var \Drupal\group\Entity\Storage\GroupRelationshipStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('group_content');
    $group_contents = $storage->loadByEntity($entity);

    // Filter out the content that does not use this plugin.
    foreach ($group_contents as $id => $group_content) {
      // @todo Shows the need for a plugin ID base field.
      $plugin_id = $group_content->getPlugin()->getPluginId();
      if ($plugin_id !== $this->pluginId) {
        unset($group_contents[$id]);
      }
    }

    // If this plugin is not being used by the entity, we have nothing to say.
    if (empty($group_contents)) {
      return AccessResult::neutral();
    }

    // We only check unpublished vs published for "view" right now. If we ever
    // start supporting other operations, we need to remove the "view" check.
    $check_published = $operation === 'view'
      && $entity->getEntityType()->entityClassImplements(EntityPublishedInterface::class);

    // Check if the account is the owner and an owner permission is supported.
    $is_owner = FALSE;
    if ($entity->getEntityType()->entityClassImplements(EntityOwnerInterface::class)) {
      $is_owner = $entity->getOwnerId() === $account->id();
    }

    // Add in the admin permission and filter out the unsupported permissions.
    $permissions = [$this->permissionProvider->getAdminPermission()];
    if (!$check_published || $entity->isPublished()) {
      $permissions[] = $this->permissionProvider->getPermission($operation, 'entity', 'any');
      $own_permission = $this->permissionProvider->getPermission($operation, 'entity', 'own');
      if ($is_owner) {
        $permissions[] = $own_permission;
      }
    }
    elseif ($check_published && !$entity->isPublished()) {
      $permissions[] = $this->permissionProvider->getPermission("$operation unpublished", 'entity', 'any');
      $own_permission = $this->permissionProvider->getPermission("$operation unpublished", 'entity', 'own');
      if ($is_owner) {
        $permissions[] = $own_permission;
      }
    }
    $permissions = array_filter($permissions);

    //
    // CHANGE 1: This is the first  customisation made to the parent method
    // foreach ($group_contents as $group_content) {
    // Consider the primary group first.
    $primaryGroup = NULL;
    foreach ($group_contents as $group_content) {
      if ($group_content->hasField('field_primary') &&
        !$group_content->get('field_primary')->isEmpty() &&
        $group_content->get('field_primary')->value
      ) {
        $primaryGroup = $group_content->getGroup();
        $result = GroupAccessResult::allowedIfHasGroupPermissions($group_content->getGroup(), $account, $permissions, 'OR');
        break;
      }
    }

    // If there is no primary primary group, or if the primary group did not
    // grant view access, allow secondary groups to grant access.
    // There should always be a primary group.
    if (!isset($result) || (!$result->isAllowed() && $operation = 'view')) {
      foreach ($group_contents as $group_content) {
        $result = GroupAccessResult::allowedIfHasGroupPermissions($group_content->getGroup(), $account, $permissions, 'OR');
        if ($result->isAllowed()) {
          break;
        }
      }
    }

    // Content whose primary group is restricted, can only be accessed
    // by people with a restricted permission.
    if ($primaryGroup && isset($result) && $result->isAllowed()) {
      $restricted = $primaryGroup->hasField('field_restricted') && $primaryGroup->field_restricted->value;
      if ($restricted) {
        // This access check must be on the group that granted access,
        // not the primary group, as the user may not be a
        // member of the primary group.
        $result = $result->andIf(GroupAccessResult::allowedIfHasGroupPermission($group_content->getGroup(), $account, 'access restricted content'));
      }
    }

    // If we did not allow access, we need to explicitly forbid access to avoid
    // other modules from granting access where Group promised the entity would
    // be inaccessible.
    if (!$result->isAllowed()) {
      $result = AccessResult::forbidden()->addCacheContexts(['user.group_permissions']);
    }

    //
    // CHANGE 2: This is the second customisation made to the parent method.
    if ($primaryGroup) {
      $result->addCacheableDependency($primaryGroup);
    }
    //
    // If there was an owner permission to check, the result needs to vary per
    // user. We also need to add the entity as a dependency because if its owner
    // changes, someone might suddenly gain or lose access.
    if (!empty($own_permission)) {
      // @todo Not necessary if admin, could boost performance here.
      $result->cachePerUser();
    }

    // If we needed to check for the owner permission or published access, we
    // need to add the entity as a dependency because the owner or publication
    // status might change.
    if (!empty($own_permission) || $check_published) {
      // @todo Not necessary if admin, could boost performance here.
      $result->addCacheableDependency($entity);
    }

    return $return_as_object ? $result : $result->isAllowed();
  }

}
