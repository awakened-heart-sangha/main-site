<?php

namespace Drupal\ahs_groups\Plugin\Group\Relation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Plugin\Group\Relation\GroupRelationBase;

/**
 * Provides a content enabler for paragraphs.
 *
 * @GroupRelationType(
 *   id = "ahs_groups_group_paragraph",
 *   label = @Translation("Group paragraph"),
 *   description = @Translation("Adds paragraphs to groups both publicly and privately."),
 *   entity_type_id = "paragraph",
 *   entity_access = TRUE,
 * )
 *
 * Because paragraphs can know their parent group directly, group-based access
 * control on paragraphs doesn't require them to be added as group content.
 * But having this paragraphs group content plugin provides a UI and storage
 * for the relevant permissions.
 *
 * @see ahs_groups_paragraph_access().
 */
class GroupParagraph extends GroupRelationBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Disable the entity cardinality field as the functionality of this module
    // relies on a cardinality of 1. We don't just hide it, though, to keep a UI
    // that's consistent with other content enabler plugins.
    $info = $this->t("This field has been disabled by the plugin to guarantee the functionality that's expected of it.");
    $form['entity_cardinality']['#disabled'] = TRUE;
    $form['entity_cardinality']['#description'] .= '<br /><em>' . $info . '</em>';

    return $form;
  }

}
