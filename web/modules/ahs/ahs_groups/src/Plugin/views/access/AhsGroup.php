<?php

namespace Drupal\ahs_groups\Plugin\views\access;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Plugin\views\access\GroupPermission;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides group access control, customised for AHS.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "ahs_groups_ahs_group",
 *   title = @Translation("AHS Group access"),
 *   help = @Translation("Access logic for AHS.")
 * )
 */
class AhsGroup extends GroupPermission {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    /** @var \Drupal\ahs_groups\Entity\AhsGroup $group */
    $group = $this->group;
    if (!empty($this->group) && !in_array($group->getRegistrationSystem(), $this->options['ahs_groups_registration_systems'])) {
      return FALSE;
    }
    return parent::access($account);
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_ahs_groups_registration_systems', implode(',', $this->options['ahs_groups_registration_systems']));
    parent::alterRouteDefinition($route);
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $selected_labels = array_intersect_key($this->getRegistrationSystems(), array_flip($this->options['ahs_groups_registration_systems']));
    $trimmed_labels = array_map(function ($string) {
      return trim(preg_replace('/\(.*?\)/', '', $string));
    }, $selected_labels);
    $summary = $this->t('Registration: @labels.', [
      '@labels' => implode(', ', $trimmed_labels),
    ]);
    $summary .= parent::summaryTitle();
    return $summary;
  }

  /**
   * Helper to get registration system.
   */
  protected function getRegistrationSystems() {
    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('group', 'event');
    return options_allowed_values($field_definitions['field_registration_system']->getFieldStorageDefinition());
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['ahs_groups_registration_systems'] = ['default' => ['registration']];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['ahs_groups_registration_systems'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Group registration system'),
      '#options' => $this->getRegistrationSystems(),
      '#default_value' => $this->options['ahs_groups_registration_systems'],
      '#description' => $this->t('This display is only shown for groups that use a specified registration system.'),
    ];
  }

}
