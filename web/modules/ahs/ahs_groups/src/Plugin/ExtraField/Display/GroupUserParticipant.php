<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\user\UserInterface;

/**
 * Extra field display plugin show to group members.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_user_participant",
 *   label = @Translation("Participant info"),
 *   description = @Translation("Displayed to users who are group participants."),
 *   bundles = {
 *     "group.event",
 *     "group.course",
 *   }
 * )
 */
class GroupUserParticipant extends GroupUserBase {

  /**
   * {@inheritdoc}
   */
  protected static function build(AhsGroup $group, UserInterface $user) {
    $build = [];
    if ($group->hasMember($user)) {
      $groupType = $group->getTypeLabel();
      $build['participant'] = [
        '#type' => 'html_tag',
        '#attributes' => ['class' => ['extra--participant']],
        '#tag' => 'p',
        '#value' => "You are registered for this $groupType.",
      ];
    }
    return $build;
  }

}
