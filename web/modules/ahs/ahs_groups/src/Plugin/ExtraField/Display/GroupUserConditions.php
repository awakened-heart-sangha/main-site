<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Extra field display plugin displaying information about group conditions.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_user_conditions",
 *   label = @Translation("User conditions"),
 *   description = @Translation("Displays eligibility and suggestion status for the user."),
 *   bundles = {
 *     "group.*",
 *   }
 * )
 */
class GroupUserConditions extends GroupUserBase {

  /**
   * {@inheritdoc}
   */
  protected static function build(AhsGroup $group, UserInterface $user) {
    $build = [];
    if ($group->isAvailable() && $group->showConditions()) {
      $build = $user->isAnonymous() ? self::buildForAnonymous($group, $user) : self::buildForUser($group, $user);
    }
    return $build;
  }

  /**
   * Helper to build anonymous section.
   */
  protected static function buildForAnonymous(AhsGroup $group, UserInterface $user) {
    $groupType = $group->getTypeLabel();
    $build = [
      'anon_conditions' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['extra--conditions', 'extra--conditions-anonymous']],

        'no_eligibility_conditions' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => "Everyone is eligible for this $groupType.",
          '#access' => !$group->hasEligibilityConditions(),
        ],

        'has_eligibility_conditions' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => "You may be eligible for this $groupType.",
          '#access' => $group->hasEligibilityConditions(),
        ],

        'no_suggestion_conditions' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => "This $groupType is suggested to everyone.",
          '#access' => !$group->hasEligibilityConditions() && ($group->isSuggested() && !$group->hasSuggestionConditions()),
        ],

        'has_suggestion_conditions' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => "This $groupType may not be suggested to you.",
          '#access' => !$group->hasEligibilityConditions() && ($group->isSuggested() && $group->hasSuggestionConditions()),
        ],

        'login' => [
          '#type' => 'link',
          '#url' => Url::fromRoute('user.login', [], ['query' => ['destination' => $group->toUrl('canonical')->toString()]]),
          '#title' => "Please login to check.",
          '#access' => $group->hasEligibilityConditions() || ($group->isSuggested() && $group->hasSuggestionConditions()),
        ],
      ],
    ];
    return $build;
  }

  /**
   * Helper to build user section.
   */
  protected static function buildForUser(AhsGroup $group, UserInterface $user) {
    $groupType = $group->getTypeLabel();
    $isGroupEligibleToUser = $group->areEligibilityConditionsMetByUser($user);
    $build = [
      'user_conditions' => [
        '#type' => 'container',
        '#access' => !$group->hasMember($user),
        '#attributes' => ['class' => ['extra--conditions', 'extra--conditions-user']],

        'eligible' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $group->hasEligibilityConditions() ? "You are eligible for this $groupType." : "Everyone is eligible for this $groupType.",
          '#access' => $isGroupEligibleToUser,
        ],

        'not_eligible' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => "You are not eligible for this $groupType.",
          '#access' => !$isGroupEligibleToUser,
        ],

        'suggested' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $group->hasSuggestionConditions() ? "This $groupType is suggested for you." : "This $groupType is suggested for everyone.",
          '#access' => $group->isSuggestedToUser($user),
        ],

        'not_suggested' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => "This $groupType is not suggested for you.",
          '#access' => $isGroupEligibleToUser && !$group->areSuggestionConditionsMetByUser($user),
        ],
      ],
    ];
    return $build;
  }

}
