<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\group\Entity\Group;
use Drupal\user\UserInterface;

/**
 * Extra field display plugin displaying information about previous courses.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_user_previous",
 *   label = @Translation("Previous engagement"),
 *   description = @Translation("Displays template courses user has previously done."),
 *   bundles = {
 *     "group.course_template",
 *   }
 * )
 */
class GroupUserPrevious extends GroupUserBase {

  /**
   * {@inheritdoc}
   */
  protected static function build(AhsGroup $group, UserInterface $user) {
    $build = [];
    $userGroupsManager = \Drupal::service('ahs_groups.user_groups_manager');
    $previous = $userGroupsManager->getUserCourseIdsFromTemplate($user, $group);
    if (!empty($previous)) {
      $courses = Group::loadMultiple($previous);
      $build['previous'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['extra--previous']],

        'message' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => 'You have already engaged in courses of this type:',
        ],

        'courses' => [
          '#type' => 'container',
        ],
      ];

      $view_builder = \Drupal::entityTypeManager()->getViewBuilder('group');
      foreach ($courses as $course) {
        $build['previous']['courses'][$course->id()] = $view_builder->view($course, 'small_teaser');
      }

      if ($group->isRegistrationAvailableToUser($user)) {
        $build['previous']['again'] = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => 'You may register to start a new course of this type.',
        ];
      }
    }
    return $build;
  }

}
