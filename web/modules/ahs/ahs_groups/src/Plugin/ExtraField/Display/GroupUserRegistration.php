<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Extra field display plugin displaying registration status and button.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_user_registration",
 *   label = @Translation("Registration"),
 *   description = @Translation("Displays registration button & info."),
 *   bundles = {
 *     "group.*",
 *   }
 * )
 */
class GroupUserRegistration extends GroupUserBase {

  /**
   * {@inheritdoc}
   */
  protected static function build(AhsGroup $group, UserInterface $user) {
    $build = $group->isRegistrationAvailable() ? static::buildOpen($group, $user) : static::buildClosed($group, $user);
    \Drupal::service('renderer')->addCacheableDependency($build, $group);
    return $build;
  }

  /**
   * Helper to build registration section.
   */
  protected static function buildOpen(AhsGroup $group, UserInterface $user) {
    $build['open'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['extra--registration', 'extra--registration-open']],
    ];

    $build['open']['registration'] = $user->isAnonymous() ? static::buildAnonymous($group, $user) : static::buildUser($group, $user);

    if ($group->isRegistrationAvailable() && $group->hasRegistrationDeadlinePending()) {
      $groupRegistrationDeadlineDate = $group->get('field_registration_deadline')->date;
      $max_age = $groupRegistrationDeadlineDate->getTimestamp() - \Drupal::time()->getCurrentTime();
      $groupRegistrationDeadline = \Drupal::service('date.formatter')->format($groupRegistrationDeadlineDate->getTimestamp(), 'long_day_with_time');
      $build['open']['deadline'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => "Registration is open until $groupRegistrationDeadline.",
      ];
      $build['open']['#cache']['max-age'] = $max_age;
    }

    return $build;
  }

  /**
   * Helper to build anonymous section.
   */
  protected static function buildAnonymous(AhsGroup $group, UserInterface $user) {
    $build = [];
    if ($group->getRegistrationSystem() !== 'manual' || $group->hasRegistrationInstruction() || $group->hasRegistrationUrl()) {
      $groupType = $group->getTypeLabel();
      $build = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        'login' => [
          '#type' => 'link',
          '#url' => Url::fromRoute('user.login', [], ['query' => ['destination' => $group->toUrl('canonical')->toString()]]),
          '#title' => "Login to register for this $groupType",
        ],
      ];
    }
    return $build;
  }

  /**
   * Helper to build user section.
   */
  protected static function buildUser(AhsGroup $group, UserInterface $user) {
    $build = [];
    if ($group->isRegistrationAvailableToUser($user)) {
      if ($group->hasRegistrationInstruction()) {
        $build = [
          'instruction' => [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $group->get('field_registration_instruction')->value,
          ],
        ];
      }
      if ($group->getRegistrationSystem() === 'registration') {
        $build += static::buildRegistrationCommerceRegistration($group, $user);
      }
      elseif ($group->getRegistrationSystem() === 'group') {
        $build += static::buildRegistrationGroup($group, $user);
      }
      elseif ($group->getRegistrationSystem() === 'manual') {
        $build += static::buildRegistrationManual($group, $user);
      }
    }
    // Caching varies between users, and on whether
    // the user is registered for the group.
    $build['#cache']['contexts'] = array_merge(
      ($build['#cache']['contexts'] ?? []),
      [
        'user',
        'user.is_group_member:' . $group->id(),
      ]
    );
    return $build;
  }

  /**
   * Provides a section with button(s) for registration users.
   */
  public static function buildRegistrationCommerceRegistration(AhsGroup $group, UserInterface $user) {
    $build = [];

    if (!$group->hasField('field_product') || !$group->get('field_product')->entity) {
      return $build;
    }

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    foreach ($group->get('field_product')->referencedEntities() as $product) {
      foreach ($product->getVariations() as $variation) {
        if ($variation->isPublished()) {
          $handler = \Drupal::entityTypeManager()->getHandler('registration', 'host_entity');
          $host_entity = $handler->createHostEntity($variation);
          $availability_result = $host_entity->isAvailableForRegistration(TRUE);

          // Skip variations that are unavailable, e.g. past deadline or over capacity.
          $variation_build = ['#type' => 'container'];
          if ($availability_result->isValid()) {
            $options = [
              'query' => [
                'source_entity_type' => 'commerce_product_variation',
                'source_entity_id' => $variation->id(),
              ],
            ];
            $variation_build['button'] = [
              '#type' => 'link',
              '#url' => Url::fromRoute('entity.webform.canonical', ['webform' => 'register'], $options),
              '#title' => t('Register for @title', ['@title' => $product->getTitle()]),
              '#attributes' => ['class' => ['btn', 'btn-primary']],
            ];
          }
          else {
            $variation_build['button'] = [
              '#type' => 'html_tag',
              '#tag' => 'p',
              '#value' => t('Register for @title', ['@title' => $product->getTitle()]),
              '#attributes' => ['class' => ['btn', 'btn-primary', 'disabled']],
            ];
            $causes = [];
            foreach ($availability_result->getViolations() as $violation) {
              $causes[] = (string) $violation->getCause();
            }
            $reason = implode(' ', $causes);
            $variation_build['unavailable'] = [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => $reason,
            ];
          }
          $build['variation_' . $variation->id()] = $variation_build;
          // Make the whole set of variations render arrays
          // depend on all variations.
          \Drupal::service('renderer')->addCacheableDependency($build, $variation);
        }
      }
    }

    return $build;
  }

  /**
   * Helper to build registration section.
   */
  protected static function buildRegistrationGroup(AhsGroup $group, UserInterface $user) {
    $build = [];
    if ($group->bundle() === 'course_template' || $group->hasPermission('join group', $user)) {
      $routeName = $group->bundle() === 'course_template' ? 'entity.group.start' : 'entity.group.join';
      $build['normal'] = [
        '#type' => 'link',
        '#url' => Url::fromRoute($routeName, ['group' => $group->id()]),
        '#title' => 'Register',
        '#attributes' => ['class' => ['btn', 'btn-primary']],
      ];
    }
    return $build;
  }

  /**
   * Helper to build registration section.
   */
  protected static function buildRegistrationManual(AhsGroup $group, UserInterface $user) {
    $build = [];
    if ($group->hasRegistrationUrl()) {
      $build['url'] = [
        '#type' => 'link',
        '#url' => $group->get('field_registration_url')->first()->getUrl(),
        '#title' => 'Register',
        '#attributes' => ['class' => ['btn', 'btn-primary']],
      ];
    }
    return $build;
  }

  /**
   * Helper to build close section.
   */
  protected static function buildClosed(AhsGroup $group, UserInterface $user) {
    // Display a closed message for registerable groups after
    // the registration deadline until the group finishes.
    $wasOpen = $group->isAvailable() && $group->isRegistrationOpen() && $group->hasRegistrationDeadlinePassed();
    // This would be confusing to people who are already participants.
    if (!$group->hasUserAlreadyEngaged($user) && !$group->isFinished() && $wasOpen) {
      $groupType = $group->getTypeLabel();
      $build['closed'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => "Registration is now closed for this $groupType.",
        '#attributes' => ['class' => ['extra--registration', 'extra--registration-closed']],
        '#cache' => [
          // Caching varies between users, and on whether
          // the user is registered for the group.
          'contexts' => [
            'user',
            'user.is_group_member:' . $group->id(),
          ],
        ],
      ];
      return $build;
    }
    return [];
  }

}
