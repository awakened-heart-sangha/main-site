<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;

/**
 * Extra field display plugin displaying information about group location.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_location",
 *   label = @Translation("Location"),
 *   description = @Translation("Displays group location, venue and/or online."),
 *   bundles = {
 *     "group.event",
 *   }
 * )
 */
class GroupLocation extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $group) {
    $build = [];
    $location = FALSE;

    $venue = FALSE;
    if ($venueEntity = $group->get('field_venue')->entity) {
      $venueLabel = (string) $venueEntity->label();
      $venue = ucfirst($venueLabel);
    }

    $online = $group->isOnline();

    if ($venue && $online) {
      $location = "$venue & online";
    }
    elseif ($venue) {
      $location = $venue;
    }
    elseif ($online) {
      $location = "Online";
    }

    if ($location) {
      $build = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $location,
      ];
    }
    return $build;
  }

}
