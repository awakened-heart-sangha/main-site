<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Drupal\views\Views;

/**
 * Extra field display plugin displaying information about group sessions.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_sessions",
 *   label = @Translation("Sessions"),
 *   description = @Translation("Displays group sessions."),
 *   bundles = {
 *     "group.course",
 *   }
 * )
 */
class GroupSessions extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $group) {
    if ($group->get('field_self_paced')->value === TRUE) {
      return;
    }
    $viewName = 'group_recordings';
    $viewDisplay = 'sessions_block';
    $view = Views::getView($viewName);
    if (!$view || !$view
      ->access($viewDisplay)) {
      return [];
    }
    return [
      'sessions' => [
        '#type' => 'view',
        '#name' => $viewName,
        '#display_id' => $viewDisplay,
        '#arguments' => [$group->id()],
      ],
    ];
  }

}
