<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\ahs_groups\Entity\AhsGroup;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Base classes for extra field display plugins.
 *
 * Plugins display personalised information about
 * a group and a user's relationship to it.
 */
class GroupUserBase extends ExtraFieldDisplayBase implements TrustedCallbackInterface {

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(...$args) {
    $this->renderer = \Drupal::service('renderer');
    parent::__construct(...$args);
  }

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $entity) {
    // Get the user for the current route, falling back to the current user.
    $user = \Drupal::routeMatch()->getParameter('user');
    if ($user) {
      $uid = $user->id();
    }
    else {
      $uid = \Drupal::routeMatch()->getRawParameter('user');
    }
    if (!$uid) {
      $uid = \Drupal::currentUser()->id();
    }

    $build[] = [
      '#lazy_builder' => [
        static::class . '::lazyBuilder',
        [
          $entity->id(),
          $uid,
        ],
      ],
      '#cache' => [
        'contexts' => [
          'url',
        ],
      ],
    ];
    return $build;
  }

  /**
   * Helper to build the display.
   */
  public static function lazyBuilder($gid, $uid) {
    $group = Group::load($gid);
    if (empty($group)) {
      throw new \Exception("Group with id $gid could not be found.");
    }
    $user = User::load($uid);
    if (empty($user)) {
      throw new \Exception("User with id $uid could not be found.");
    }
    // Wrap it in a child array because of
    // https://www.drupal.org/project/drupal/issues/2609250
    return ['build' => static::build($group, $user) ?? []];
  }

  /**
   * Helper to get build array.
   */
  protected static function build(AhsGroup $group, UserInterface $user) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['lazyBuilder'];
  }

}
