<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;

/**
 * Extra field display plugin displaying information about group type.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_type",
 *   label = @Translation("Group type"),
 *   description = @Translation("Displays basic information about the group type."),
 *   bundles = {
 *     "group.*",
 *   }
 * )
 */
class GroupType extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $group) {
    $type = FALSE;
    $build = [];

    if ($group->bundle() === 'course_template') {
      $type = 'Self-paced';
    }

    elseif ($group->bundle() === 'course') {
      $type = 'Online course';
    }

    elseif ($group->bundle() === 'event') {
      if ($term = $group->get('field_event_type')->entity) {
        $typeLabel = (string) $term->label();
        $type = ucfirst($typeLabel);
      }
      else {
        $type = 'Event';
      }
    }

    if ($type) {
      $build = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $type,
      ];
    }

    return $build;
  }

}
