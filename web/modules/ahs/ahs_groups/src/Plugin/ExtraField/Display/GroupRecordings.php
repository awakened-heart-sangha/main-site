<?php

namespace Drupal\ahs_groups\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Drupal\views\Views;

/**
 * Extra field display plugin displaying information about group sessions.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_groups_group_recordings",
 *   label = @Translation("Recordings"),
 *   description = @Translation("Displays group session recordings."),
 *   bundles = {
 *     "group.*",
 *   }
 * )
 */
class GroupRecordings extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $group) {
    $viewName = 'group_recordings';
    $viewDisplay = 'recordings_block';
    $view = Views::getView($viewName);

    // Show no recordings section if user has no access to recordings.
    if (!$view || !$view
      ->access($viewDisplay)) {
      return [];
    }

    $view->setArguments([$group->id()]);
    $view->execute($viewDisplay);
    $recordings = [];

    // If there are no recordings.
    if (count($view->result) === 0) {
      $recordings = [
        '#markup' => '',
        '#cache' => [
          'tags' => $view->getCacheTags(),
        ],
      ];
      // Respect group setting to hide section if no recordings yet.
      if (!$group->hasField('field_recordings_none_hide') || (!$group->get('field_recordings_none_hide')->isEmpty() && !empty($group->get('field_recordings_none_hide')->value))) {
        return $recordings;
      }
      // Hide recordings when group starts more than a week in future.
      if ($group->getStartDateOffset() > (60 * 60 * 24 * 7)) {
        $recordings['#cache']['max-age'] = $group->getStartDateOffset() - (60 * 60 * 24 * 7);
        return $recordings;
      }
      // Respect group setting to show message if no recordings yet.
      if ($group->hasField('field_recordings_none_note') && !$group->get('field_recordings_none_note')->isEmpty() && !empty($group->get('field_recordings_none_note')->value)) {
        unset($recordings['#markup']);
        $recordings = $recordings + [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $group->get('field_recordings_none_note')->value,
        ];
      }
    }
    // Else there are recordings so show them.
    else {
      $recordings = [
        '#type' => 'view',
        '#name' => $viewName,
        '#display_id' => $viewDisplay,
        '#arguments' => [$group->id()],
      ];
    }

    // If there is a message about recordings, always add that above recordings.
    $container = NULL;
    if ($group->hasField('field_recordings_note') && !$group->get('field_recordings_note')->isEmpty() && !empty($group->get('field_recordings_note')->value)) {
      $message = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $group->get('field_recordings_note')->value,
      ];
      $container = [
        '#type' => 'container',
        'message' => $message,
        'recordings' => $recordings,
      ];
    }

    $section = [
      'recordings_section' => [
        '#theme' => 'section_panel',
        '#id' => 'recordings',
        '#heading' => 'Recordings',
        '#body' => $container ?? $recordings,
      ],
    ];
    return $section;
  }

}
