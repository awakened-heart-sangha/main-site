<?php

namespace Drupal\ahs_groups\Controller;

use Drupal\group\Controller\GroupMembershipController as ParentGroupMembershipController;
use Drupal\group\Entity\GroupInterface;

/**
 * Customise some aspects of Group membership routes.
 */
class GroupMembershipController extends ParentGroupMembershipController {

  /**
   * The _title_callback for the join form route.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return string
   *   The page title.
   */
  public function joinTitle(GroupInterface $group) {
    return $this->t('Register for %label', ['%label' => $group->label()]);
  }

}
