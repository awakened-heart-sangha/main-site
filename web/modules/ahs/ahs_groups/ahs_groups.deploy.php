<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

use Drupal\ahs_training\Entity\TrainingRecord;
use Drupal\group\Entity\Group;
use Drupal\node\Entity\Node;

/**
 * Provide default self-paced value for existing groups.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0001_set_self_paced(&$sandbox) {
  $groupIds = \Drupal::entityTypeManager()
    ->getStorage('group')
    ->getQuery()
    ->accessCheck(FALSE)
    ->condition('type', 'course')
    ->execute();
  $groups = Group::loadMultiple($groupIds);
  foreach ($groups as $group) {
    $needsSave = FALSE;
    if ($group->hasField('field_self_paced')) {
      $group->set('field_self_paced', FALSE);
      $needsSave = TRUE;
    }

    if ($group->hasField('field_published')) {
      if ($group->get('field_published')->isEmpty()) {
        $group->set('field_self_paced', TRUE);
        $needsSave = TRUE;
      }
    }

    if ($needsSave) {
      $group->save();
    }
  }
}

/**
 * Set leaders as group participants with mentor participant type.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0002_set_leader_mentor(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('group')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', ['event', 'course'], 'IN')
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);

      $memberships = $group->getMembers();
      $studentTermId = TrainingRecord::STUDENT_TERM_ID;
      foreach ($memberships as $membership) {
        $content = $membership->getGroupRelationship();
        if ($content->get('field_participant_type')->isEmpty()) {
          $content->get('field_participant_type')->setValue([['target_id' => $studentTermId]]);
          $content->save();
        }
      }

      $mentorTermId = TrainingRecord::MENTOR_TERM_ID;
      foreach ($group->field_leaders->referencedEntities() as $leader) {
        if ($membership = $group->getMember($leader)) {
          $content = $membership->getGroupRelationship();
          if ($content->get('field_participant_type')->target_id !== $mentorTermId) {
            $content->get('field_participant_type')->setValue([['target_id' => $mentorTermId]]);
            $content->save();
          }
        }
        else {
          $group->addMember($leader, ['field_participant_type' => $mentorTermId]);
        }
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_groups')
        ->error("Error changing leader members " . $id . " " . $group->label() . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'groups fixed');
}

/**
 * Make leaders have mwntor group role.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0003_set_leader_mentor_role(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('group')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', ['event', 'course'], 'IN')
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);
      foreach ($group->field_leaders->referencedEntities() as $leader) {
        $membership = $group->getMember($leader);
        $content = $membership->getGroupRelationship();
        $content->set('group_roles', [$group->bundle() . '-mentor']);
        $content->save();
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_groups')
        ->error("Error giving leaders mentor role " . $id . " " . $group->label() . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'groups fixed');
}

/**
 * Set primary as group session default.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0004_set_primary_as_group_session_default(&$sandbox) {
  $jobsLoader = function () {
    $storage = \Drupal::entityTypeManager()->getStorage('group_content');
    return array_keys($storage->loadByPluginId('group_node:session'));
  };

  $jobProcessor = function ($id) {
    try {
      $content = \Drupal::entityTypeManager()->getStorage('group_content')->load($id);
      if ($content->hasField('field_primary') && ($content->get('field_primary')->isEmpty() || $content->get('field_primary')->value === NULL)) {
        $content->set('field_primary', TRUE);
        $content->save();
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_groups')
        ->error("Error making group node primary " . $id . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'group nodes fixed');
}

/**
 * Set show conditions to true by default, and get rid of online as a venue.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0005_show_conditions(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('group')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', ['event', 'course', 'course_template'], 'IN')
      ->execute();

  };

  $jobProcessor = function ($id) {
    try {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);
      $group->set('field_show_conditions', TRUE);
      if ($group->bundle() === 'event') {
        $venueField = $group->get('field_venue');
        if (!$venueField->isEmpty() && $venueField->target_id == 2144) {
          $group->set('field_venue', NULL);
          $group->set('field_online', TRUE);
        }
      }
      $group->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_groups')
        ->error("Error changing leader members " . $id . " " . $group->label() . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'groups fixed');
}

/**
 * Remove online venue.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0006_remove_online_venue(&$sandbox) {
  $venue = Node::load(2144);
  $venue->delete();
}

/**
 * Set show conditions to true by default, and get rid of online as a venue.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0006_recordings_none_note(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('group')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', ['event', 'course'], 'IN')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);
      $group->set('field_recordings_none_note', "No recordings are available yet.");
      $group->set('field_recordings_none_hide', TRUE);
      if ($group->getEndDateOffset() > 0 && $group->bundle() === 'event') {
        $group->set('field_recordings_none_hide', FALSE);
      }
      $group->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_groups')
        ->error("Error setting recordings none note " . $id . " " . $group->label() . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'groups fixed');
}

/**
 * Rebuild group paras content.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_groups_deploy_0007_rebuild_group_paras_content(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('group')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', ['event', 'course'], 'IN')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);
      _ahs_groups_add_sessions_from_paragraphs($group);
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_groups')
        ->error("Error rebuilding group paras content " . $id . " " . $group->label() . " : " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'groups para content fixed');
}

/**
 * Create produce entities from groups and product variations.
 */
function ahs_groups_deploy_0008_create_products(array &$sandbox) {
  $group_starage = \Drupal::entityTypeManager()->getStorage('group');
  $product_starage = \Drupal::entityTypeManager()->getStorage('commerce_product');
  $variation_starage = \Drupal::entityTypeManager()->getStorage('commerce_product_variation');

  if (!isset($sandbox['total'])) {
    if (!\Drupal::database()->schema()->tableExists('group__field_registration_variations')) {
      return;
    }

    // Get all variations.
    $query = \Drupal::database()->select('group__field_registration_variations', 'frv');
    $query->fields('frv', ['entity_id', 'field_registration_variations_target_id']);
    $results = $query->execute()->fetchAll();

    if (empty($results)) {
      return;
    }

    $sandbox['current'] = $sandbox['current_chunk'] = 0;
    $sandbox['chunks'] = array_chunk($results, 1);
    $sandbox['total'] = count($sandbox['chunks']);
  }

  $sandbox['current_chunk'] = $sandbox['chunks'][$sandbox['current']];
  if (!empty($sandbox['current_chunk'][0])) {
    $result = $sandbox['current_chunk'][0];

    $group_id = $result->entity_id;
    $variation_id = $result->field_registration_variations_target_id;

    if ($group = $group_starage->load($group_id)) {
      // Create a new product.
      $product = $product_starage->create([
        'type' => 'ahs_registration',
        'title' => 'Online',
        'field_group' => ['target_id' => $group->id()],
      ]);

      // Attach the variation to the product.
      if ($variation = $variation_starage->load($variation_id)) {
        $product->addVariation($variation);
      }

      $product->save();

      // Attach the product to the group.
      $products = $group->get('field_product')->getValue();
      $products[] = ['target_id' => $product->id()];
      $group->set('field_product', $products);

      // Clear old variation values.
      $group->set('field_registration_variations', NULL);

      // Save the group.
      $group->save();
    }
  }

  $sandbox['current']++;
  $sandbox['#finished'] = $sandbox['total'] == 0 ? 1 : ($sandbox['current'] / $sandbox['total']);
}
