<?php

/**
 * @file
 * Contains ahs_groups.post_update.php.
 */

/**
 * Update the group's status by field_published value.
 */
function ahs_groups_post_update_group_status(array &$sandbox) {
  $group_storage = \Drupal::entityTypeManager()->getStorage('group');

  // Fetching amount of data we need to process.
  // Runs only once per update.
  if (!isset($sandbox['total'])) {
    $result_count = $group_storage->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    // Write total of entities need to be processed to $sandbox.
    $sandbox['total'] = $result_count;
    // Initiate default value for current processing № of element.
    $sandbox['current'] = 0;
  }

  // Do not continue if no entities are found.
  if (empty($sandbox['total'])) {
    $sandbox['#finished'] = 1;
    return t('No groups to be processed.');
  }

  // How much entities can be processed per batch.
  $limit = 5;

  $group_ids = $group_storage->getQuery()
    ->accessCheck(FALSE)
    ->range($sandbox['current'], $limit)
    ->execute();

  // Load entities.
  $groups = $group_storage->loadMultiple($group_ids);

  foreach ($groups as $group) {
    $status_old = $group->hasField('field_published')
      ? (bool) $group->get('field_published')->value
      : TRUE;
    $group->set('status', $status_old);
    $group->save();

    // Increment currently processed entities.
    $sandbox['current']++;
  }

  $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);

  // Print some progress.
  return t('@count / @total groups processed.', ['@count' => $sandbox['current'], '@total' => $sandbox['total']]);
}
