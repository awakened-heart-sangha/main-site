<?php

namespace Drupal\ahs_news\Entity;

use Drupal\simplenews\Entity\Subscriber;
use Drupal\user\UserInterface;

/**
 * Defines the simplenews subscriber entity.
 */
class AhsSubscriber extends Subscriber {

  /**
   * {@inheritdoc}
   */
  protected function getUserSharedFields(UserInterface $user) {
    // Sync mail between subscriber and user, to allow authenticated
    // users to edit their subscriber  and automatically update their user.
    $fields = parent::getUserSharedFields($user);
    if (!in_array('mail', $fields)) {
      $fields[] = 'mail';
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function uuid() {
    // Subscriber fails to properly declare uuid entity key
    // used by parent method.
    return $this->get('uuid')->value;
  }

}
