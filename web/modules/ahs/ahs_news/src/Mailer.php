<?php

namespace Drupal\ahs_news;

use Drupal\simplenews\Mail\Mailer as parentMailer;
use Drupal\simplenews\Mail\MailInterface;

/**
 * Default Mailer. Provides base class for mailer.
 */
class Mailer extends parentMailer {

  /**
   * {@inheritdoc}
   */
  public function sendMail(MailInterface $mail) {
    // Can only intentionally re-render an entity with references 20 times
    // https://www.drupal.org/project/drupal/issues/2940605
    // Reset this protection for each email sent.
    $reflection = new \ReflectionProperty('\Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter', 'recursiveRenderDepth');
    $reflection->setAccessible(TRUE);
    $reflection->setValue(NULL, []);

    // Customise the from address.
    $issue = $mail->getIssue();
    $sub = $mail->getSubscriber();
    $cache = $this->mailCache;

    $mail_new = new MailCustom($issue, $sub, $cache);

    $result = parent::sendMail($mail_new);
    return $result;
  }

}
