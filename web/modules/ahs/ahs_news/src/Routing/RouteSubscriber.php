<?php

namespace Drupal\ahs_news\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // onAlterRoutes() is a method in RouteSubscriberBase,
    // wrapping alterRoutes().
    $events[RoutingEvents::ALTER] = 'onAlterRoutes';
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Change path from 'user/{user}/simplenews and title from 'Newsletters'.
    if ($route = $collection->get('simplenews.newsletter_subscriptions_user')) {
      $route->setPath('/user/{user}/email-news');
      $route->setDefault('_title', 'Email news');
    }

    // Change path from 'user/{user}/simplenews and title from 'Newsletters'.
    if ($route = $collection->get('simplenews.newsletter_subscriptions_arguments')) {
      $route->setDefault('_title', 'Your email news preferences');
    }

    // Node tab.
    if ($route = $collection->get('simplenews.node_tab')) {
      // Prettier url than /simplenews.
      $route->setPath('/node/{node}/email');
      // Use our customised form.
      $route->setDefault('_form', '\Drupal\ahs_news\Form\NodeTabForm');
      // Remove custom access requirement and replace it with our own.
      // In NodeTabForm we further restrict access to the part of the
      // form that controls sending.
      $route->setRequirements(['_entity_access' => 'node.update']);
      $route->addOptions(['parameters' => ['node' => ['bundle' => ['news']]]]);
    }

    // Use our custom controller for history.
    if ($route = $collection->get('history.read_node')) {
      $route->setDefault('_controller', '\Drupal\ahs_news\Controller\HistoryController::readNode');
    }
    if ($route = $collection->get('get_node_read_timestamps')) {
      $route->setDefault('_controller', '\Drupal\ahs_news\Controller\HistoryController::getNodeReadTimestamps');
    }
  }

}
