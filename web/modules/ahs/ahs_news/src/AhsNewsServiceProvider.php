<?php

namespace Drupal\ahs_news;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Don't cache simplenews, because we inject personalised views in the footer.
 */
class AhsNewsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('simplenews.mail_cache')) {
      $definition = $container->getDefinition('simplenews.mail_cache');
      $definition->setClass('Drupal\simplenews\Mail\MailCacheNone');
    }
  }

}
