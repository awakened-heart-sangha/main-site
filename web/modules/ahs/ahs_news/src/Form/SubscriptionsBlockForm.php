<?php

namespace Drupal\ahs_news\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\simplenews\Form\SubscriptionsBlockForm as parentForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add subscriptions, but show no form if no subscription needed.
 */
class SubscriptionsBlockForm extends parentForm {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // The parent simply shows a message, which is clumsy.
    $form = parent::buildForm($form, $form_state);
    $form['message']['#weight'] = -10;
    if (empty($form['mail']['widget'][0]['value']['#default_value'])) {
      // The message is duplicated by the placeholder
      // when no email already exists.
      unset($form['message']);
    }
    else {
      // Don't allow changing emails using this form.
      $form['mail']['widget']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitExtra(array $form, FormStateInterface $form_state) {
    parent::submitExtra($form, $form_state);

    // Redirect to the preferences page so they can fine tune subscriptions.
    // This works even when not logged in, using the hash.
    $subscriber = $this->entity;
    $hash = simplenews_generate_hash($subscriber->getMail(), 'manage');
    $route_parameters = [
      'snid' => $subscriber->id(),
      'timestamp' => $this->time->getRequestTime(),
      'hash' => $hash,
    ];
    $url = Url::fromRoute('simplenews.newsletter_subscriptions_arguments', $route_parameters);
    $form_state->setRedirectUrl($url);
  }

}
