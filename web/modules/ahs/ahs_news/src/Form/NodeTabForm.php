<?php

namespace Drupal\ahs_news\Form;

use Drupal\ahs_news\Plugin\simplenews\RecipientHandler\AhsRecipientHandlerMultiple;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\simplenews\Form\NodeTabForm as parentNodeTabForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure simplenews subscriptions of a user.
 */
class NodeTabForm extends parentNodeTabForm {

  /**
   * Date time formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The topics fetcher.
   *
   * @var \Drupal\ahs_discourse\TopicsFetcher
   */
  protected $topicsFetcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->topicsFetcher = $container->get('ahs_discourse.topics_fetcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $form = parent::buildForm($form, $form_state, $node);
    $form['#title'] = $this->t('<em>Send email:</em> @title', ['@title' => $node->getTitle()]);

    $form['preview'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Preview'),
      '#weight' => -10,
    ];

    $form['preview']['uid'] = [
      '#type' => 'select2',
      '#title' => 'User',
      '#multiple' => FALSE,
      '#autocomplete' => TRUE,
      '#target_type' => 'user',
      '#default_value' => $this->currentUser()->id(),
    ];

    // Close the sending test email section, as our
    // preview is a better solution.
    $form['test']['#open'] = FALSE;

    $hasPublishTime = $node->hasField('publish_on') && !empty($node->publish_on->value);
    if ($hasPublishTime) {
      $publishTime = $this->dateFormatter->format($node->publish_on->value, 'long');
    }

    $form['send']['method'] = [
      '#type' => 'item',
    ];
    if ($node->simplenews_issue->status == SIMPLENEWS_STATUS_SEND_NOT) {
      if ($hasPublishTime) {
        $form['send']['method']['#markup'] = $this->t('Mails are not currently set to be sent when this news is published on @publish_time.', [
          '@publish_time' => $publishTime,
        ]);
      }
      else {
        $form['send']['method']['#markup'] = $this->t('Mails are not currently set to be sent for this news.');
      }
    }
    elseif ($node->simplenews_issue->status == SIMPLENEWS_STATUS_SEND_PUBLISH) {
      if ($hasPublishTime) {
        $form['send']['method']['#markup'] = $this->t('Mails will be sent when this news is published (currently scheduled for @publish_time).', [
          '@publish_time' => $publishTime,
        ]);
      }
      else {
        $form['send']['method']['#markup'] = $this->t('Mails will be sent when this news is published');
      }
    }

    $sendFuture = in_array($node->simplenews_issue->status, [SIMPLENEWS_STATUS_SEND_NOT, SIMPLENEWS_STATUS_SEND_PUBLISH]);
    $newsletter = $node->simplenews_issue->target_id;
    $verb = $sendFuture ? "Send" : "Sent";
    $recipientHandler = \Drupal::service('simplenews.spool_storage')->getRecipientHandler($node, $node->simplenews_issue->getValue());
    if ($recipientHandler instanceof AhsRecipientHandlerMultiple) {
      $recipients = $recipientHandler->recipientsMessage($verb);
    }
    else {
      $recipients = $this->t("@verb to anyone subscribed to @newsletter.", [
        '@verb' => $verb,
        '@newsletter' => $newsletter,
      ]);
    }
    $form['send']['recipients'] = [
      '#type' => 'item',
      '#weight' => 0,
      '#markup' => $recipients,
    ];

    // Only users with permission to send a newsletter can initiate
    // its sending, but others can cancel it.
    if ($node->simplenews_issue->status == SIMPLENEWS_STATUS_SEND_NOT) {
      $form['send']['send']['#access'] = $this->currentUser->hasPermission('send newsletter');

      // If there are no recipients, nothing can be sent yet.
      $summary = $this->spoolStorage->issueSummary($node);
      if ($summary['count'] == 0) {
        $form['send']['send']['#disabled'] = TRUE;
        $form['send']['method']['#access'] = FALSE;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitTestMail(array &$form, FormStateInterface $form_state) {
    // Get latest help wanted topics from Sanghaspace.
    $this->topicsFetcher->syncHelpWanted();
    parent::submitTestMail($form, $form_state);
  }

  /**
   * See https://www.drupal.org/project/simplenews/issues/3266375.
   */
  public function submitStop(array &$form, FormStateInterface $form_state) {
    $issue = $form_state->get('node');
    if ($issue->simplenews_issue->status == SIMPLENEWS_STATUS_SEND_PUBLISH) {
      $issue->simplenews_issue->status = SIMPLENEWS_STATUS_SEND_NOT;
      $issue->save();
      return;
    }
    $this->spoolStorage->deleteIssue($issue);
  }

}
