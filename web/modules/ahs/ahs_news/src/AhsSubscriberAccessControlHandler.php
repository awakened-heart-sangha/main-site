<?php

namespace Drupal\ahs_news;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\simplenews\SubscriberAccessControlHandler;

/**
 * Defines the access control handler for the simplenews subscriber entity type.
 *
 * @see \Drupal\simplenews\Entity\Subscriber
 */
class AhsSubscriberAccessControlHandler extends SubscriberAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    // Allow editing email from page form for convenience. But not if not
    // logged in, if merely authenticated by hash. This is becaue we sync
    // subscriber email with user email and allowing hash control of user
    // email address is scarily insecure.
    if ($operation == 'edit' && $field_definition->getName() === 'mail') {
      if ($account->isAuthenticated() && $items && ($entity = $items->getEntity()) && ($entity->getUserId() == $account->id())) {
        return AccessResult::allowed()->addCacheableDependency($entity);
      }
      // The parent returns AccessResult::forbidden for mail,
      // which turns an orIf to forbidden.
      $parentAllowed = AccessResult::allowedIf(parent::checkFieldAccess($operation, $field_definition, $account, $items)->isAllowed());
      return AccessResult::allowedIfHasPermission($account, 'administer simplenews subscriptions')
        ->orIf($parentAllowed);
    }
    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
