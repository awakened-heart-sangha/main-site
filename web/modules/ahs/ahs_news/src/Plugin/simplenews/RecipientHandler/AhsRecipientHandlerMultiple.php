<?php

namespace Drupal\ahs_news\Plugin\simplenews\RecipientHandler;

use Drupal\Component\Utility\Random;
use Drupal\simplenews\Plugin\simplenews\RecipientHandler\RecipientHandlerEntityBase;
use Drupal\simplenews\SubscriberInterface;

/**
 * This handler sends a newsletter issue to custom field newsletters also.
 *
 * @RecipientHandler(
 *   id = "ahs_news_multiple",
 *   title = @Translation("Send to multiple newsletters")
 * )
 */
class AhsRecipientHandlerMultiple extends RecipientHandlerEntityBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $options = [];
    $descriptions = [];
    $any_count = [];
    $member_count = [];
    foreach (simplenews_newsletter_get_visible() as $newsletter) {
      $options[$newsletter->id()] = $newsletter->name;
      $descriptions[$newsletter->id()] = $newsletter->description;
      $any_count[$newsletter->id()] = $this->countSubscribers($newsletter);
      $member_count[$newsletter->id()] = $this->countSubscribers($newsletter, TRUE);
    }

    // For some unknown reason the checkboxes will not save unless
    // another value is updated at the same time.
    $random = new Random();
    $element['random'] = [
      '#type' => 'hidden',
      '#default_value' => $random->string(),
    ];

    $element['members'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send to members'),
      '#default_value' => $this->configuration['members'] ?? FALSE,
    ];
    $element['member_newsletters'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Send to members who subscribe to any of these lists'),
      '#options' => $options,
      '#default_value' => array_keys(array_filter($this->configuration['member_newsletters'] ?? [])),
      '#states' => [
        'visible' => [
          ':input[name="simplenews_issue[handler_settings][members]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['anyone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send to anyone'),
      '#default_value' => $this->configuration['anyone'] ?? FALSE,
    ];
    $element['anyone_newsletters'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Send to anyone who subscribes to any of these lists'),
      '#options' => $options,
      '#default_value' => array_keys(array_filter($this->configuration['anyone_newsletters'] ?? [])),
      '#states' => [
        'visible' => [
          ':input[name="simplenews_issue[handler_settings][anyone]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    foreach ($options as $id => $name) {
      $element['member_newsletters'][$id]['#description'] = "$descriptions[$id] ({$member_count[$id]} people)";
      $element['anyone_newsletters'][$id]['#description'] = "$descriptions[$id] ({$any_count[$id]} people)";
    }

    return $element;
  }

  public function recipientsMessage($verb) {
    // Backwards compatabilty for old configs.
    $message = '';
    if (empty($this->issue->simplenews_issue->handler_settings['anyone']) && empty($this->issue->simplenews_issue->handler_settings['members']) && !empty($this->issue->simplenews_issue->handler_settings['newsletters'])) {
      $newsletters = array_keys(array_filter($this->issue->simplenews_issue->handler_settings['newsletters'] ?? []));
      if ($newsletters) {
        $recipientsType = ($this->issue->simplenews_issue->handler_settings['members_only'] ?? NULL) ? 'members' : 'anyone';
        $message = $this->buildMessage($newsletters, $recipientsType, $verb);
      }
    }

    // New configs matching form above.
    else {
      if (!empty($this->issue->simplenews_issue->handler_settings['anyone'])) {
        $anyone_newsletters = array_keys(array_filter($this->issue->simplenews_issue->handler_settings['anyone_newsletters'] ?? []));
        if ($anyone_newsletters) {
          $recipientsType = 'anyone';
          $message .= $this->buildMessage($anyone_newsletters, $recipientsType, $verb);
        }
      }
      if (!empty($this->issue->simplenews_issue->handler_settings['members'])) {
        $member_newsletters = array_keys(array_filter($this->issue->simplenews_issue->handler_settings['member_newsletters'] ?? []));
        if ($member_newsletters) {
          $recipientsType = 'members';
          $message .= ' ' . $this->buildMessage($member_newsletters, $recipientsType, $verb);
        }
      }
    }

    $message = trim($message);
    if (empty($message)) {
      $message = $this->t("No recipients selected yet. Edit the news and select some recipients.");
    }
    return $message;
  }

  protected function buildMessage(array $newsletters, $recipientsType, $verb) {
    $newslettersString = implode(', ', $newsletters);
    if (count($newsletters) > 1) {
      $recipients = $this->t("@verb to @type subscribed to any of: @string.", [
        '@verb' => $verb,
        '@type' => $recipientsType,
        '@string' => $newslettersString,
      ]);
    }
    else {
      $recipients = $this->t("@verb to @type subscribed to @string.", [
        '@verb' => $verb,
        '@type' => $recipientsType,
        '@string' => $newslettersString,
      ]);
    }
    return $recipients;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery() {
    $query = \Drupal::entityQuery('simplenews_subscriber')
      ->condition('status', SubscriberInterface::ACTIVE)
      ->condition('subscriptions.status', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED)
      ->accessCheck(FALSE);

    // Backwards-compatability for old configs.
    if (empty($this->configuration['anyone']) && empty($this->configuration['members']) && !empty($this->configuration['newsletters'])) {
      $newsletter_ids = array_filter($this->configuration['newsletters'] ?? []) ?: [999999999999];
      $query->condition('subscriptions', array_keys($newsletter_ids), 'IN');
      if (!empty($this->configuration['members_only'])) {
        $query->condition('uid.entity:user.roles', 'member');
      }
    }
    else {
      // New configs matching form above.
      $orGroup = $query->orConditionGroup();
      $hasConditions = false;

      if (!empty($this->configuration['anyone'])) {
        $newsletter_ids = array_filter($this->configuration['anyone_newsletters'] ?? []) ?: [999999999999];
        $orGroup->condition('subscriptions', array_keys($newsletter_ids), 'IN');
        $hasConditions = true;
      }
      if (!empty($this->configuration['members'])) {
        $newsletter_ids = array_filter($this->configuration['member_newsletters'] ?? []) ?: [999999999999];
        $andGroupMembers = $query->andConditionGroup();
        $andGroupMembers->condition('subscriptions', array_keys($newsletter_ids), 'IN');
        $andGroupMembers->condition('uid.entity:user.roles', 'member', 'IN');
        $orGroup->condition($andGroupMembers);
        $hasConditions = true;
      }

      // Only add the orGroup if it has conditions
      if ($hasConditions) {
        $query->condition($orGroup);
      }
      else {
        // If no conditions are set, return no results
        $query->condition('id', 0);
      }
    }

    return $query;
  }

  /**
   * Count subscribers for a single preference.
   *
   * @param \Drupal\simplenews\NewsletterInterface $newsletter
   * @param boolean $only_members
   * @return int
   */
  protected function countSubscribers($newsletter, $only_members = FALSE) {
    $query = \Drupal::entityQuery('simplenews_subscriber')
      ->condition('status', SubscriberInterface::ACTIVE)
      ->condition('subscriptions.status', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED)
      ->accessCheck(FALSE)
      ->condition('subscriptions', $newsletter->id());

    if ($only_members) {
      $query->condition('uid.entity:user.roles', 'member');
    }

    return $query->count()->execute();
  }

}