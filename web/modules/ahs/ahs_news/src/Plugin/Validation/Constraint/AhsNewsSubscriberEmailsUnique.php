<?php

namespace Drupal\ahs_news\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a subscriber's emails are unique, considering user emails also.
 *
 * @Constraint(
 *   id = "AhsNewsSubscriberEmailsUnique",
 *   label = @Translation("Subscriber emails unique", context = "Validation")
 * )
 */
class AhsNewsSubscriberEmailsUnique extends Constraint {

  /**
   * {@inheritdoc}
   */
  public $message = 'A user already exists with the email %newemail. Please unsubscribe from all emails here, and then login with the email %newemail at https://ahs.org.uk/user/login and change the newsletter preferences for that user.';

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return '\Drupal\ahs_news\Plugin\Validation\Constraint\AhsNewsSubscriberEmailsValidator';
  }

}
