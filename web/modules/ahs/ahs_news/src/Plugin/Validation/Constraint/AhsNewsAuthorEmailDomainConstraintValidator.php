<?php

namespace Drupal\ahs_news\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Email domain constraint.
 */
class AhsNewsAuthorEmailDomainConstraintValidator extends ConstraintValidator {

  /**
   * Checks if News author's email domain is 'ahs.org.uk'.
   *
   * @param mixed $field
   *   The field/value that should be validated.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   The constraint for the validation.
   */
  public function validate($field, Constraint $constraint) {
    // Get field's parent entity to access fields of this entity.
    $entity = $field->getEntity();
    if ($entity && $entity->hasField('field_email_from')) {

      $email = $entity->field_email_from->value;

      // Display an error message, if email domain is not 'ahs.org.uk'.
      if (!str_ends_with($email, '@ahs.org.uk')) {
        $this->context->buildViolation($constraint->errorMessage)
          ->atPath('field_email_from')
          ->addViolation();
      }
    }

  }

}
