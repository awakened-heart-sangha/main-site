<?php

namespace Drupal\ahs_news\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an Email domain constraint.
 *
 * @Constraint(
 *   id = "AhsNewsAuthorEmailDomainConstraint",
 *   label = @Translation("News author's email domain constraint", context = "Validation"),
 * )
 */
class AhsNewsAuthorEmailDomainConstraint extends Constraint {
  /**
   * Constraint error message.
   *
   * @var string
   */
  public $errorMessage = 'The reply email should end with \'@ahs.org.uk\'.';

}
