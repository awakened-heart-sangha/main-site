<?php

namespace Drupal\ahs_news\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that subscriber emails are not used by any users on the site.
 */
class AhsNewsSubscriberEmailsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * User storage handler.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs a new AlternativeUserEmailsValidator.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage handler.
   */
  public function __construct(UserStorageInterface $user_storage) {
    $this->userStorage = $user_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager')->getStorage('user'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!$item = $items->first()) {
      return;
    }

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $items->getEntity();
    if ($entity->getEntityTypeId() !== 'simplenews_subscriber' || $items->getFieldDefinition()->getName() !== 'mail') {
      return;
    }

    // Find users with the same email.
    $query = $this->userStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('mail', $item->value);
    // Exclude the subscriber's user, it's OK for them to have the same email.
    if ($uid = $entity->getUserId()) {
      $query->condition('uid', $uid, '<>');
    }
    $results = $query->count()->execute();

    // There should be no users found.
    if ($results > 0) {
      $this->context->addViolation($constraint->message, [
        '%newemail' => $item->value,
      ]);
    }
  }

}
