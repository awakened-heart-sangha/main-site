<?php

namespace Drupal\ahs_news\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\Plugin\Block\SimplenewsSubscriptionBlock;

/**
 * Provides a subscription block with all available newsletters and email field.
 *
 * @Block(
 *   id = "ahs_simplenews_subscription_block",
 *   admin_label = @Translation("AHS simplenews subscription"),
 *   category = @Translation("Simplenews")
 * )
 */
class AhsSimplenewsSubscriptionBlock extends SimplenewsSubscriptionBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = parent::build();
    $form['#attributes']['class'][] = 'block-simplenews-subscription-block';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $subscribed = TRUE;
    $subscriber = Subscriber::loadByUid($this->currentUser->id(), 'create');
    $defaults = $this->configuration['default_newsletters'];
    foreach ($defaults as $default) {
      if (!$subscriber->isSubscribed($default)) {
        $subscribed = FALSE;
      }
    }

    // Hide form if using it would not subscribe to anything.
    if ($subscribed) {
      return AccessResult::forbidden('User is already subscribed to defaults.');
    }

    // Only allow users with the 'subscribe to newsletters' permission.
    return AccessResult::allowedIfHasPermission($account, 'subscribe to newsletters');
  }

}
