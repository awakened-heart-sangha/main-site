<?php

namespace Drupal\ahs_news\Plugin\views\field;

use Drupal\node\Entity\Node;
use Drupal\simplenews\Plugin\views\field\SendStatus as parentSendStatus;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to provide send status of a newsletter issue.
 *
 * Customised to return nothing if not set to send,
 * and add stating text "On publish", "Sent" and "Pending".
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ahs_news_send_status")
 */
class SendStatus extends parentSendStatus {

  /**
   * The simplenews spool storage.
   *
   * @var \Drupal\simplenews\Spool\SpoolStorageInterface
   */
  protected $simplenewsSpool;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->simplenewsSpool = $container->get('simplenews.spool_storage');
    $instance->moduleExtensionList = $container->get('extension.list.module');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $output = [];
    $node = $values->_entity;

    if ($node->hasField('simplenews_issue')) {
      $output = parent::render($values);
      $message = $this->getMessage($node);
      $status = $node->get('simplenews_issue')->status;
      $error_count = $message['error_count'] ? ' ❌{{ error_count }}' : '';

      if ($status == SIMPLENEWS_STATUS_SEND_PUBLISH) {
        $output['text'] = [
          '#type' => 'inline_template',
          '#template' => "<span title=\"{{ description }}\">On publish ({{ count }})</span>",
          '#context' => $message,
        ];
      }
      elseif ($status == SIMPLENEWS_STATUS_SEND_PENDING) {
        $output['text'] = [
          '#type' => 'inline_template',
          '#template' => "<span title=\"{{ description }}\">Pending ({{ sent_count }}/{{ count }}$error_count)</span>",
          '#context' => $message,
        ];
      }
      elseif ($status == SIMPLENEWS_STATUS_SEND_READY) {
        $output['text'] = [
          '#type' => 'inline_template',
          '#template' => "<span title=\"{{ description }}\">Sent ({{ sent_count }}/{{ count }}$error_count)</span>",
          '#context' => $message,
        ];
      }
    }

    return $output;
  }

  /**
   * Return a compiled message to display.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object.
   *
   * @return array
   *   An array containing the elements of the message to be rendered.
   */
  protected function getMessage(Node $node) {
    $status = $node->simplenews_issue->status;
    $message = $this->simplenewsSpool->issueSummary($node);

    $images = [
      SIMPLENEWS_STATUS_SEND_PENDING => 'images/sn-cron.png',
      SIMPLENEWS_STATUS_SEND_READY => 'images/sn-sent.png',
    ];
    if (isset($images[$status])) {
      $message['uri'] = $this->moduleExtensionList->getPath('simplenews') . '/' . $images[$status];
    }
    else {
      $message['uri'] = NULL;
    }

    return $message;
  }

}
