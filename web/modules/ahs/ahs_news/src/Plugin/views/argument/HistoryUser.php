<?php

namespace Drupal\ahs_news\Plugin\views\argument;

use Drupal\Core\Cache\UncacheableDependencyTrait;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides filtering of nodes to those with new to a user.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("ahs_news_history_user_timestamp")
 */
class HistoryUser extends ArgumentPluginBase {

  use UncacheableDependencyTrait;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $user = User::load($this->argument);
    if (!$user) {
      return;
    }

    // Hey, Drupal kills old history, so nodes that haven't been updated
    // since HISTORY_READ_LIMIT are bzzzzzzzt outta here!
    $limit = $this->time->getRequestTime() - HISTORY_READ_LIMIT;

    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";
    $node = $this->query->ensureTable('node_field_data', $this->relationship);

    // NULL means a history record doesn't exist. That's clearly new content.
    // Unless it's very very old content. Everything in the query is already
    // type safe cause none of it is coming from outside here.
    $this->query->addWhereExpression('AND', "($field IS NULL AND ($node.changed > (***CURRENT_TIME*** - $limit))) OR $field < $node.changed");
  }

}
