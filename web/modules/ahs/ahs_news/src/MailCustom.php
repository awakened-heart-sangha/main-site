<?php

namespace Drupal\ahs_news;

use Drupal\simplenews\Mail\MailEntity;

/**
 * Overrides getFromAddress() by address from custom field.
 */
class MailCustom extends MailEntity {

  /**
   * {@inheritdoc}
   */
  public function getFromAddress() {
    $node = $this->getIssue();
    if ($node->hasField('field_email_from') && !$node->get('field_email_from')->isEmpty()) {
      return $node->get('field_email_from')->getString();
    }
    return parent::getFromAddress();
  }

}
