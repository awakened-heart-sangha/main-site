<?php

namespace Drupal\ahs_news\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for History module routes.
 *
 * All methods except currentUser() and readNode() are only necessary because
 * historyReadMultiple hard codes the current user.
 */
class HistoryController extends ControllerBase {

  /**
   * The identified user.
   *
   * @var \Drupal\ahs_user_management\IdentifiedAccountProxy
   */
  protected $identifiedAccount;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->identifiedAccount = $container->get('ahs_user_management.identified_user');
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Returns a set of nodes' last read timestamps.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request of the page.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getNodeReadTimestamps(Request $request) {
    if ($this->identifiedAccount->isAnonymous()) {
      throw new AccessDeniedHttpException();
    }

    $nids = $request->request->get('node_ids');
    if (!isset($nids)) {
      throw new NotFoundHttpException();
    }
    return new JsonResponse($this->historyReadMultiple($nids));
  }

  /**
   * Marks a node as read by the current user right now.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request of the page.
   * @param \Drupal\node\NodeInterface $node
   *   The node whose "last read" timestamp should be updated.
   */
  public function readNode(Request $request, NodeInterface $node) {
    if ($this->identifiedAccount->isAnonymous()) {
      throw new AccessDeniedHttpException();
    }

    // Make sure we explicitly specify our current user.
    history_write($node->id(), $this->identifiedAccount);

    return new JsonResponse((int) $this->historyRead($node->id()));
  }

  /**
   * Here we use our custom currentUser() method.
   */
  protected function historyReadMultiple($nids) {
    $history = &drupal_static(__FUNCTION__, []);

    $return = [];

    $nodes_to_read = [];
    foreach ($nids as $nid) {
      if (isset($history[$nid])) {
        $return[$nid] = $history[$nid];
      }
      else {
        // Initialize value if current user has not viewed the node.
        $nodes_to_read[$nid] = 0;
      }
    }

    if (empty($nodes_to_read)) {
      return $return;
    }

    $result = $this->database->query('SELECT nid, timestamp FROM {history} WHERE uid = :uid AND nid IN ( :nids[] )', [
      ':uid' => $this->identifiedAccount->id(),
      ':nids[]' => array_keys($nodes_to_read),
    ]);
    foreach ($result as $row) {
      $nodes_to_read[$row->nid] = (int) $row->timestamp;
    }
    $history += $nodes_to_read;

    return $return + $nodes_to_read;
  }

  /**
   * Helper to get history.
   */
  protected function historyRead($nid) {
    $history = $this->historyReadMultiple([$nid]);
    return $history[$nid];
  }

}
