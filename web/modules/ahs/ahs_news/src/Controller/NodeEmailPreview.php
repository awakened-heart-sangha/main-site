<?php

namespace Drupal\ahs_news\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Random;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\node\NodeInterface;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\Mail\MailEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

/**
 * Custom node redirect controller.
 */
class NodeEmailPreview extends ControllerBase {

  /**
   * The topics fetcher.
   *
   * @var \Drupal\ahs_discourse\TopicsFetcher
   */
  protected $topicsFetcher;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $theme;

  /**
   * The theme initialization manager.
   *
   * @var \Drupal\Core\Theme\ThemeInitialization
   */
  protected $themeInitialization;

  /**
   * The current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The render service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The asset resolver service.
   *
   * @var \Drupal\Core\Asset\AssetResolverInterface
   */
  protected $assetResolver;

  /**
   * The mail cached service.
   *
   * @var \Drupal\simplenews\Mail\MailCacheInterface
   */
  protected $mailCached;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->topicsFetcher = $container->get('ahs_discourse.topics_fetcher');
    $instance->theme = $container->get('theme.manager');
    $instance->themeInitialization = $container->get('theme.initialization');
    $instance->currentRequest = $container->get('request_stack')->getCurrentRequest();
    $instance->renderer = $container->get('renderer');
    $instance->assetResolver = $container->get('asset.resolver');
    $instance->mailCached = $container->get('simplenews.mail_cache');

    return $instance;
  }

  /**
   * Helper to preview email.
   */
  public function view(NodeInterface $node, $uid) {
    // Pull latest help wanted topics from Sanghaspace.
    $this->topicsFetcher->syncHelpWanted();

    $this->theme->setActiveTheme($this->themeInitialization->initTheme('ahs_email'));
    $mail = $this->getMail($node, $uid);

    // MailManager tries to make links absolute before passing to swiftmailer.
    $body = Markup::create(Html::transformRootRelativeUrlsToAbsolute((string) $mail->getBody(), $this->currentRequest->getSchemeAndHttpHost()));

    $message = [
      'id' => 'simplenews',
      'module' => 'simplenews',
      'to' => $mail->getRecipient(),
      'headers' => [],
      'subject' => $mail->getSubject(),
      'body' => $body,
      'key' => 'node',
    ];

    $render = [
      '#theme' => 'symfony_mailer_lite_email',
      '#message' => $message,
      '#is_html' => TRUE,
    ];
    $render['#attached']['library'] = ["ahs_email/symfony_mailer_lite"];

    $rendered = $this->renderer->renderInIsolation($render);

    // Replicate the inline style rendering swiftmailer uses.
    // Process CSS from libraries.
    $assets = AttachedAssets::createFromRenderArray($render);
    $css = '';
    // Request optimization so that the CssOptimizer performs essential
    // processing such as @include.
    foreach ($this->assetResolver->getCssAssets($assets, FALSE) as $css_asset) {
      $css .= file_get_contents($css_asset['data']);
    }
    if ($css) {
      $rendered = (new CssToInlineStyles())->convert($rendered, $css);
    }

    $response = new Response();
    $response->setContent($rendered);
    return $response;
  }

  /**
   * Helper to get email entity.
   */
  protected function getMail(NodeInterface $node, $uid) {
    if (!$subscriber = Subscriber::loadByUid($uid)) {
      $randomGen = new Random();
      $mail = $randomGen->name() . "@example.com";
      $subscriber = Subscriber::create([
        'mail' => $mail,
        'uid' => $uid,
      ]);
    }
    assert($subscriber);
    $mail = new MailEntity($node, $subscriber, $this->mailCached);
    return $mail;
  }

}
