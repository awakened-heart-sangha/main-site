<?php

namespace Drupal\ahs_news\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Custom node redirect controller.
 */
class NodeSuuidController extends ControllerBase {

  /**
   * The identified user.
   *
   * @var \Drupal\ahs_user_management\IdentifiedAccountProxy
   */
  protected $identifiedAccount;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->identifiedAccount = $container->get('ahs_user_management.identified_user');
    return $instance;
  }

  /**
   * Provides redirect response for the route.
   */
  public function track(Request $request, $nid, $suuid) {
    // Track incoming node views with subscriber uuid added, then redirect
    // to node. Ideally this would be in some more generic place, but it's
    // tricky to do because of caching. Drupal core (in statistics and
    // history) uses javascript on page load to track views.
    if ($this->currentUser()->isAnonymous()) {
      $subscribers = $this->entityTypeManager()->getStorage('simplenews_subscriber')->loadByProperties(['uuid' => $suuid]);
      if ($subscribers && $subscriber = reset($subscribers)) {
        $uid = $subscriber->getUserId();
        if ($uid) {
          $this->identifiedAccount->setIdentified($uid);
        }
      }
    }

    // We deliberately don't upcast or validate the node id, leaving that to the
    // redirect. We track suuid regardless of node validity.
    $url = Url::fromRoute('entity.node.canonical', ['node' => $nid]);
    return new RedirectResponse($url->toString(TRUE)->getGeneratedUrl());
  }

}
