<?php

/**
 * @file
 * Contains ahs_training\ahs_training.views.inc.
 *
 * Provide a custom views field data that isn't tied to any other module.
 */

/**
 * Implements hook_views_data_alter().
 */
function ahs_news_views_data_alter(array &$data) {
  $data['history']['timestamp']['argument'] = [
    'title' => t('Has new content for user'),
    'id' => 'ahs_news_history_user_timestamp',
    'help' => 'Show only nodes that have new content for specified user',
  ];

  $data['node']['ahs_news_send_status'] = [
    'real field' => 'nid',
    'field' => [
      'title' => t('Send status (AHS custom)'),
      'help' => t('Send status of the newsletter.'),
      'id' => 'ahs_news_send_status',
    ],
    'group' => t('Content'),
  ];

}
