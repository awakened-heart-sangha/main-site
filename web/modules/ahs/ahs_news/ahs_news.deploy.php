<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

use Drupal\simplenews\Entity\Newsletter;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;
use Drupal\user\Entity\User;

/**
 * Subscribe existing members to newsletters.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_news_deploy_0002_add_members_to_newsletters(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->condition('roles', 'member')
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      $subscriber = Subscriber::loadByUid($user->id(), TRUE);
      $subscriber->subscribe('important', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED, 'setup');
      $subscriber->subscribe('retreats', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED, 'setup');
      $subscriber->subscribe('online_courses', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED, 'setup');
      $subscriber->subscribe('help_wanted', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED, 'setup');
      $subscriber->subscribe('local', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED, 'setup');
      $subscriber->unsubscribe('courses', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED, 'setup');
      $subscriber->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_news')
        ->error("Error subscribing member " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'members subscribed to newsletters');
}

/**
 * Implements hook_deploy_NAME().
 */
function ahs_news_deploy_0003_purge_courses_newsletter(&$sandbox) {
  $newsletter = Newsletter::create(['id' => 'courses', 'label' => 'courses']);
  $newsletter->save();
  $newsletter->delete();
}

/**
 * Subscribe existing members to community newsletter.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_news_deploy_0004_add_members_to_newsletters(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityQuery('simplenews_subscriber')
      ->accessCheck(FALSE)
      ->condition('status', SubscriberInterface::ACTIVE)
      ->condition('subscriptions.status', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED)
      ->condition('subscriptions', ['help_wanted', 'local'], 'IN')
      ->condition('uid.entity:user.roles', 'member')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $subscriber = Subscriber::load($id);
      $subscriber->subscribe('community', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED, 'update');
      $subscriber->save();
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_news')
        ->error("Error subscribing subscriber " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'members subscribed to community newsletter.');
}