<?php

/**
 * @file
 * Provide views data for decoupled_auth.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function ahs_user_management_views_data_alter(&$data) {
  $data['users']['display_name'] = [
    'title' => t('Display name'),
    'help' => t('The display name of the user.'),
    'field' => [
      'id' => 'user_display_name',
    ],
    'filter' => [
      'id' => 'user_display_name',
      'field' => 'field_name',
      'field_name' => 'field_name',
    ],
  ];

  // Use a custom filter on the name field on about profile.
  $data['profile__field_name']['ahs_user_name'] = [
    'title' => t('AHS name'),
    'help' => t('The name of the person, including nickname and Sangha name.'),
  // Add this line.
    'group' => t('Profile'),
    'filter' => [
      'id' => 'ahs_user_name',
    ],
  ];

  $data['users_field_data']['ahs_user_management_customer_address'] = [
    'title' => t('Customer address'),
    'help' => t('Show the address from the customer profile.'),
    'real field' => 'uid',
    'field' => [
      'id' => 'ahs_user_management_customer_address',
    ],
  ];
}
