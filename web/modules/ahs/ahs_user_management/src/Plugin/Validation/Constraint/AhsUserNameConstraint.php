<?php

namespace Drupal\ahs_user_management\Plugin\Validation\Constraint;

use Drupal\user\Plugin\Validation\Constraint\UserNameConstraint;

/**
 * Checks if a value is a valid user name, but only if the user is coupled.
 *
 * @Constraint(
 *   id = "AhsUserName",
 *   label = @Translation("User name", context = "Validation"),
 * )
 */
class AhsUserNameConstraint extends UserNameConstraint {}
