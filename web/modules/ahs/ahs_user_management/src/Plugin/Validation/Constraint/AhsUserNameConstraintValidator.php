<?php

namespace Drupal\ahs_user_management\Plugin\Validation\Constraint;

use Drupal\ahs_user_management\AhsUserInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\Plugin\Validation\Constraint\UserNameConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Validates the AhsUserName constraint.
 */
class AhsUserNameConstraintValidator extends UserNameConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    // Skip username validation for decoupled users.
    if ($items instanceof FieldItemListInterface) {
      $entity = $items->getEntity();
      if ($entity instanceof AhsUserInterface && $entity->isDecoupled()) {
        return;
      }
    }

    // Otherwise pass on for normal validation.
    parent::validate($items, $constraint);
  }

}
