<?php

namespace Drupal\ahs_user_management\Plugin\EntityMergeHandler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_merge\MergeContext;
use Drupal\entity_merge\Plugin\EntityMergeHandler\ProfileEntityMergeHandler;

/**
 * Custom merge handler for profiles.
 */
class AhsProfileMergeHandler extends ProfileEntityMergeHandler {

  /**
   * {@inheritdoc}
   */
  protected function getDisplayFieldsToSkip(MergeContext $context): array {
    return array_merge(parent::getDisplayFieldsToSkip($context), [
      'status',
      'revision_user',
      'revision_created',
      'created',
      'changed',
      'field_notes',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function postMerge(MergeContext $context) {
    $commentIds = $this->getCommentIds('field_notes', $context->getSecondary());
    if (!empty($commentIds)) {
      $context->addProgress('Transferring comments', 'field_notes');
    }
    $comments = $this->entityTypeManager->getStorage('comment')->loadMultiple($commentIds);
    foreach ($comments as $comment) {
      $newComment = $comment->createDuplicate();
      $newComment->set('entity_id', $context->getPrimary()->id());
      $newComment->save();
    }
  }

  /**
   * Gets comment ids for a particular entity on a particular field.
   *
   * @param string $fieldId
   *   The field id.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   An array of comment ids.
   */
  protected function getCommentIds($fieldId, EntityInterface $entity) {
    $query = $this->entityTypeManager->getStorage('comment')->getQuery();
    return $query->accessCheck(FALSE)
      ->condition('entity_type', $entity->getEntityTypeId())
      ->condition('entity_id', $entity->id())
      ->condition('field_name', $fieldId)
      ->execute();
  }

}
