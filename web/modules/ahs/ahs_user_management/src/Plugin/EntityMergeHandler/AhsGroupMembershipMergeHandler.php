<?php

namespace Drupal\ahs_user_management\Plugin\EntityMergeHandler;

use Drupal\entity_merge\Plugin\EntityMergeHandler\EntityMergeHandlerBase;

/**
 * Default entity merge handler.
 *
 * @EntityMergeHandler(
 *   id = "ahs_user_management_group_membership",
 * )
 *
 * @package Drupal\entity_merge\Plugin\EntityMergeHandler
 */
class AhsGroupMembershipMergeHandler extends EntityMergeHandlerBase {

}
