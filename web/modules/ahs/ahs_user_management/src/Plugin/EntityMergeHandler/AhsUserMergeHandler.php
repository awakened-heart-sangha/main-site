<?php

namespace Drupal\ahs_user_management\Plugin\EntityMergeHandler;

use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_merge\MergeContext;
use Drupal\entity_merge\Plugin\EntityMergeHandler\UserEntityMergeHandler;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\message\Entity\Message;
use Drupal\message_notify\MessageNotifier;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Merge handler for Ahs users.
 */
class AhsUserMergeHandler extends UserEntityMergeHandler {

  /**
   * The message notify.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotify;

  /**
   * The group membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoader
   */
  protected $membershipLoader;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, MessageNotifier $message_notify, GroupMembershipLoaderInterface $membership_loader) {
    parent::__construct($entity_type_manager, $entity_field_manager);
    $this->messageNotify = $message_notify;
    $this->membershipLoader = $membership_loader;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('message_notify.sender'),
      $container->get('group.membership_loader')
    );
  }

  /**
   * Primary user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $originalPrimary;

  /**
   * Secondary user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $originalSecondary;

  /**
   * {@inheritdoc}
   */
  protected function getDisplayFieldsToSkip(MergeContext $context): array {
    $skip = array_merge(parent::getDisplayFieldsToSkip($context), [
      'name',
      'status',
      'path',
      'access',
      'login',
      'created',
      'changed',
      'field_oldsite_id',
      'role_change',
      'customer_profiles',
    ]);

    return $skip;
  }

  /**
   * {@inheritdoc}
   */
  public function performMerge(MergeContext $context) {

    // Store the original entity state so we can consult if after merging.
    $this->originalPrimary = clone $context->getPrimary();
    $this->originalSecondary = clone $context->getSecondary();

    // Something about our profiles is triggering an early save that's
    // happening before DecoupledUserMergeHandler unsets the secondary's
    // account name.
    /** @var \Drupal\ahs_user_management\Entity\AhsUser $secondary */
    $secondary = $context->getSecondary();
    $secondary->set('name', NULL);

    // This calls ::postMerge and saves the entities.
    parent::performMerge($context);

    // When the email is nulled on the secondary in ::postMerge, it gets
    // transferred to the alternatives. But entities are only saved in
    // parent::performMerge() if there are no errors.
    if (!$context->hasErrors()) {
      $secondary->set('alternative_user_emails', NULL);
      $secondary->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postMerge(MergeContext $context) {
    /** @var \Drupal\ahs_user_management\Entity\AhsUser $primary */
    /** @var \Drupal\ahs_user_management\Entity\AhsUser $secondary */
    $primary = $context->getPrimary();
    $secondary = $context->getSecondary();

    // We can't have two users for the same account, so decouple the secondary.
    if ($primary->getAccountName() == $secondary->getAccountName()) {
      $secondary->decouple();
    }

    // Set the secondary's emails as alternatives for the primary.
    $secondaryEmail = $secondary->getEmail();
    $emails[] = $secondary->getEmail();
    $emails = $emails + array_column($secondary->get('alternative_user_emails')->getValue(), 'value');
    $secondary->setEmail(NULL);
    $secondary->set('alternative_user_emails', NULL);
    $alternatives = $primary->get('alternative_user_emails');
    foreach ($emails as $email) {
      if (!in_array($email, array_column($alternatives->getValue(), 'value'))) {
        $alternatives->appendItem(['value' => $email]);
      }
    }

    // Strip roles from secondary.
    $secondary->set('roles', NULL);

    // Leave admin notes on the users.
    $primaryId = $primary->id();
    $primaryEmail = $primary->getEmail();
    $secondaryId = $secondary->id();
    $primary->addAdminNote("<a href=\"/user/$secondaryId\">User $secondaryId</a> with email $secondaryEmail was merged into this user $primaryId whose email was $primaryEmail.", 'full_html');
    $secondary->addAdminNote("This user $secondaryId with email $secondaryEmail was merged into <a href=\"/user/$primaryId\">user $primaryId</a> whose email was $primaryEmail.", 'full_html');

    // Transfer username only if primary did not have one.
    if (empty($this->originalPrimary->getAccountName()) && $name = $this->originalSecondary->getAccountName()) {
      $primary->set('name', $name);
    }

    parent::postMerge($context);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkipRepointingReferences(): array {
    $fields_to_skip = parent::getFieldsToSkipRepointingReferences();

    // Moved from DecoupledUserMergeHandler that referred to
    // removed DecoupledAuthUser.
    $fields_to_skip['profile'][] = 'uid';
    // Commerce payment methods should only be repointed if the corresponding
    // user remote id had been moved.
    $fields_to_skip['commerce_payment_method'][] = 'uid';
    return $fields_to_skip;
  }

  /**
   * {@inheritdoc}
   */
  public function repointEntityReferences(MergeContext $context) {
    $this->mergeGroupMemberships($context);
    $this->repointMultipleProfiles($context);

    // Repoint payment methods if the gateway customer has been merged across.
    foreach ($this->getPaymentMethodIdsToRepoint($this->originalPrimary, $this->originalSecondary) as $methodId) {
      $method = PaymentMethod::load($methodId);
      $method->set('uid', $context->getPrimary()->id());
      $method->save();
    }

    parent::repointEntityReferences($context);
  }

  /**
   * Identify payment methods that can be repointed after merging.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getPaymentMethodIdsToRepoint(UserInterface $primary, UserInterface $secondary) {
    $methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties([
      'uid' => $secondary->id(),
    ]);
    $methodsToRepoint = [];
    foreach ($methods as $method) {
      $provider = $method->getPaymentGatewayId() . '|' . $method->getPaymentGatewayMode();
      $primaryRemoteId = $primary->get('commerce_remote_id')->getByProvider($provider);
      $secondaryRemoteId = $secondary->get('commerce_remote_id')->getByProvider($provider);
      if (!empty($secondaryRemoteId) && empty($primaryRemoteId)) {
        $methodsToRepoint[] = $method->id();
      }
    }
    return $methodsToRepoint;
  }

  /**
   * Merge group memberships for these users.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   The merge context.
   */
  protected function mergeGroupMemberships(MergeContext $context) {
    // Find all group memberships, merging them if they are for the same group.
    $primary = $context->getPrimary();
    $secondary = $context->getSecondary();
    $loader = $this->membershipLoader;
    $primaryMemberships = $loader->loadByUser($primary);
    $secondaryMemberships = $loader->loadByUser($secondary);
    $secondaryMembershipsKeyedByGroupId = [];
    foreach ($secondaryMemberships as $secondaryMembership) {
      $secondaryMembershipsKeyedByGroupId[$secondaryMembership->getGroup()->id()] = $secondaryMembership;

      // Manually set membership of Primary user to a group during merging.
      if (empty($primaryMemberships)) {
        $primaryMemberships[] = $secondaryMembership;
        $group = $secondaryMembership->getGroup();
        $group->addMember($primary);
        $group->save();
      }
    }
    foreach ($primaryMemberships as $primaryMembership) {
      $gid = $primaryMembership->getGroup()->id();
      if (isset($secondaryMembershipsKeyedByGroupId[$gid])) {
        $primaryContent = $primaryMembership->getGroupRelationship();
        $secondaryContent = $secondaryMembershipsKeyedByGroupId[$gid]->getGroupRelationship();
        $new_context = $context->createChildContext('group_content', $primaryContent, $secondaryContent, "group_memberships:{$primary->id()}");
        $handler = $new_context->getEntityPluginManager()
          ->createInstance('ahs_user_management_group_membership');
        $handler->performMerge($new_context);
        $secondaryContent->delete();
      }
    }
  }

  /**
   * Change ownership of profiles.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   The merge context.
   */
  protected function repointMultipleProfiles(MergeContext $context) {
    $primary = $context->getPrimary();
    $secondary = $context->getSecondary();
    $profileTypes = $this->entityTypeManager->getStorage('profile_type')->loadMultiple();
    foreach ($profileTypes as $profileType) {
      if ($profileType->allowsMultiple()) {
        // Loads published profiles only, shouldn't matter.
        $profiles = $this->entityTypeManager->getStorage('profile')->loadMultipleByUser($secondary, $profileType->id(), TRUE);
        foreach ($profiles as $profile) {
          $profile->setDefault(FALSE);
          $profile->setOwner($primary);
          $profile->save();
        }
      }
    }
  }

}
