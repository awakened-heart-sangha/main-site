<?php

namespace Drupal\ahs_user_management\Plugin\ExtraField\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\extra_field\Plugin\ExtraFieldFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A field for user registration form.
 *
 * It checks if the user should have been identifed by email.
 *
 * @ExtraFieldForm(
 *   id = "ahs_user_management_user_email_known",
 *   label = @Translation("Email known"),
 *   description = @Translation("Is the user new to the Sangha?"),
 *   bundles = {
 *     "user.user"
 *   },
 * )
 */
class UserEmailKnown extends ExtraFieldFormBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The name of the field.
   */
  const ELEMENT = 'ahs_user_management_email_known';

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * UserEmailKnown constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Defines an account interface which represents the current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array &$form, FormStateInterface $form_state) {
    if ($this->currentUser->isAnonymous()) {
      $form[self::ELEMENT] = [
        '#type' => 'radios',
        '#title' => $this->t('New to the Sangha?'),
        '#options' => [
          '1' => $this->t('Yes, this is my first online contact with the Sangha'),
          '0' => $this->t('No, one of my emails should be in the system already'),
        ],
        '#default_value' => '0',
      ];

      $form['#validate'][] = [$this, 'validateEmailKnown'];
    }

    return [];
  }

  /**
   * Field validation callback.
   *
   * @param array $form
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateEmailKnown(array $form, FormStateInterface $form_state) {
    $shouldBeAcquired = !((bool) $form_state->getValue(self::ELEMENT));
    $ahs_user_management_acquisition = $form_state->getValue('ahs_user_management_acquisition');
    if (isset($ahs_user_management_acquisition) && !$form_state->getValue('ahs_user_management_acquisition') && $shouldBeAcquired) {
      $form_state->setError($form, $this->t('None of the email addresses you entered are known to us.'));
    }
  }

}
