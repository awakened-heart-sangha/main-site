<?php

namespace Drupal\ahs_user_management\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extra field display plugin to allow staff to generate a one-time login link.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_user_management_user_one_time_login",
 *   label = @Translation("One-time login generation link"),
 *   description = @Translation("Displays a link to generate a one-time login link for a user."),
 *   bundles = {
 *     "user.*",
 *   }
 * )
 */
class UserOneTimeLogin extends ExtraFieldDisplayBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The redirect service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirect;

  /**
   * Constructs a ExtraFieldDisplayFormattedBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Defines an account interface which represents the current user.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect
   *   Provides an interface for redirect destinations.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user, RedirectDestinationInterface $redirect) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $current_user;
    $this->redirect = $redirect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('current_user'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $entity) {
    $build = [
      '#cache' => ['context' => ['user.permissions']],
    ];
    if ($this->currentUser->hasPermission('administer users')) {
      $url = Url::fromRoute('ahs_user_management.generate_login_link',
        [
          'user' => $entity->id(),
        ],
        [
          'query' => $this->redirect->getAsArray(),
        ]
      );
      $link = Link::fromTextAndUrl($this->t('Generate one-time login link'), $url);
      $link = $link->toRenderable();
      $container = [
        '#type' => 'container',
        'link' => $link,
      ];
      $build['container'] = $container;
    }
    return $build;
  }

}
