<?php

namespace Drupal\ahs_user_management\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;

/**
 * Extra field display plugin displaying information about group type.
 *
 * @ExtraFieldDisplay(
 *   id = "ahs_user_management_user_display_name",
 *   label = @Translation("Display name"),
 *   description = @Translation("Displays the user's display name."),
 *   bundles = {
 *     "user.*",
 *   }
 * )
 */
class UserDisplayName extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $user) {
    $build = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $user->getDisplayName(),
    ];
    return $build;
  }

}
