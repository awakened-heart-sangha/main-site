<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandler\ProfileReferenceMergeHandler;

/**
 * Handles merging profile fields.
 *
 * @FieldMergeHandler(
 *   id = "ahs_user_management_merge_field_handler_profile",
 *   handles_field_type = "entity_reference"
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class AhsProfileReferenceMergeHandler extends ProfileReferenceMergeHandler {

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    $field_definition = $context->getPrimaryField()->getFieldDefinition();
    $field_definition_name = $context->getPrimaryField()->getName();

    if ($field_definition->isComputed()) {
      // Change the status of profiles fields for ahs users' merging.
      switch ($field_definition_name) {
        case 'admin_profiles':
        case 'mentor_profiles':
        case 'customer_profiles':
        case 'about_profiles':
          $context->getPrimaryField()->getFieldDefinition()->setComputed(FALSE);
          break;
      }
    }

    $perform = parent::performMerge($context);

    switch ($field_definition_name) {
      case 'admin_profiles':
      case 'mentor_profiles':
      case 'customer_profiles':
      case 'about_profiles':
        $context->getPrimaryField()->getFieldDefinition()->setComputed(TRUE);
        break;
    }

    return $perform;
  }

}
