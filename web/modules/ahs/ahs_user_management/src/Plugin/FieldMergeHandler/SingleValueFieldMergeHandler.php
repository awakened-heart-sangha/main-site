<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandler\DefaultFieldMergeHandler;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;

/**
 * Merges fields with single values.
 */
class SingleValueFieldMergeHandler extends DefaultFieldMergeHandler {

  /**
   * The field property to use.
   *
   * @var string
   */
  protected $property = 'value';

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    $field_definition = $context->getPrimaryField()->getFieldDefinition();

    // If the field is computed, skip it.
    if ($field_definition->isComputed()) {
      $context->addProgress('Computed field - not attempting to merge.');
      return FieldMergeHandlerInterface::HANDLED;
    }

    $cardinality = $field_definition
      ->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality != 1) {
      throw new \Exception('The comparative field merge handler should only be used on fields with a cardinality of 1');
    }

    if (!$this->preferPrimary($context->getPrimaryField()->getValue(), $context->getSecondaryField()->getValue())) {
      $context->getParentContext()->addProgress('Merging values', $context->getFieldId(), print_r($context->getPrimaryField()->getValue(), TRUE), print_r($context->getSecondaryField()->getValue(), TRUE));
      $result = $this->mergeValues($context->getPrimaryField()->getValue(), $context->getSecondaryField()->getValue());
      $context->getParentContext()->getPrimary()->set($context->getFieldId(), $result);
    }

    return FieldMergeHandlerInterface::HANDLED;
  }

  /**
   * {@inheritdoc}
   */
  protected function preferPrimary($primaryValues, $secondaryValues) {
    if (empty($secondaryValues) || is_null($secondaryValues[0][$this->property])) {
      return TRUE;
    }
    elseif (empty($primaryValues) || is_null($primaryValues[0][$this->property])) {
      return FALSE;
    }
    else {
      // We have both a primary and a secondary item.
      $result = $this->mergeValues($primaryValues, $secondaryValues);
      $primaryValue = $primaryValues[0][$this->property];
      return $primaryValue === $result;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function mergeValues($primaryValues, $secondaryValues) {
    $secondaryValue = $secondaryValues[0][$this->property];
    if (empty($primaryValues) || is_null($primaryValues[0][$this->property])) {
      return $secondaryValue;
    }
    $primaryValue = $primaryValues[0][$this->property];
    return $this->mergeValue($primaryValue, $secondaryValue);
  }

  /**
   * {@inheritdoc}
   */
  protected function mergeValue($primaryValue, $secondaryValue) {
    return $primaryValue;
  }

  /**
   * {@inheritdoc}
   */
  protected function displayCompare($primary_value, $secondary_value, FieldDisplayContext $context) : ?bool {
    return !$this->preferPrimary($primary_value, $secondary_value);
  }

}
