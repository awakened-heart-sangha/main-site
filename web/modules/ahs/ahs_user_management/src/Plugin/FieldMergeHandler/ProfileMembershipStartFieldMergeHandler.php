<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Handles user access field.
 *
 * @FieldMergeHandler(
 *   id = "ahs_user_management_profile_membership_start_field_merge_handler",
 *   handles_entity_type = "profile",
 *   handles_field_name = "field_membership_start",
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class ProfileMembershipStartFieldMergeHandler extends SingleValueFieldMergeHandler {

  /**
   * {@inheritdoc}
   */
  protected function mergeValue($primaryValue, $secondaryValue) {
    $primaryDate = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $primaryValue);
    $secondaryDate = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $secondaryValue);
    $least = min($primaryDate->getTimeStamp(), $secondaryDate->getTimestamp());
    return DrupalDateTime::createFromTimestamp($least)->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);
  }

}
