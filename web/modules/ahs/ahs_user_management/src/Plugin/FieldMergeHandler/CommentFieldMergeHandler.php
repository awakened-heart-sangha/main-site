<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Comments FieldMergeHandler.
 *
 * @FieldMergeHandler(
 *   id = "comment",
 *   handles_field_type = "comment"
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class CommentFieldMergeHandler implements FieldMergeHandlerInterface, ContainerFactoryPluginInterface {

  /**
   * Comment storage.
   *
   * @var \Drupal\comment\CommentStorageInterface
   */
  protected $storage;

  /**
   * Construct the comment merge handler.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('comment');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    return self::HANDLED;
  }

  /**
   * {@inheritdoc}
   */
  public function displayField(FieldDisplayContext $context) : bool {
    $fieldId = $context->getFieldId();
    $field_definition = $context->getPrimaryField()
      ->getFieldDefinition();

    $primaryCommentIds = $this->getCommentIds($fieldId, $context->getParentContext()->getPrimary());
    $secondaryCommentIds = $this->getCommentIds($fieldId, $context->getParentContext()->getSecondary());

    $context->setFieldDisplayInfo('', $context->getFieldId(), [
      'comparison' => NULL,
      'label' => $field_definition->getLabel(),
      'primary' => count($primaryCommentIds),
      'secondary' => count($secondaryCommentIds),
    ]);

    return FieldMergeHandlerInterface::HANDLED;
  }

  /**
   * Gets comment ids for a particular entity on a particular field.
   *
   * @param string $fieldId
   *   The field id.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   An array of comment ids.
   */
  protected function getCommentIds($fieldId, EntityInterface $entity) {
    $query = $this->storage->getQuery();
    return $query->accessCheck(FALSE)
      ->condition('entity_type', $entity->getEntityTypeId())
      ->condition('entity_id', $entity->id())
      ->condition('field_name', $fieldId)
      ->execute();
  }

}
