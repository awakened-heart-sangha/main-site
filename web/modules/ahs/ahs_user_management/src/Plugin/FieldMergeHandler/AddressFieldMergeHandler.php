<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

use Drupal\entity_merge\Plugin\FieldMergeHandler\DefaultFieldMergeHandler;

/**
 * Handles address fields. This overrides the default address handler.
 *
 * @FieldMergeHandler(
 *   id = "ahs_user_management_merge_field_handler_address",
 *   handles_field_type = "address",
 *   weight = 50
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class AddressFieldMergeHandler extends DefaultFieldMergeHandler {

}
