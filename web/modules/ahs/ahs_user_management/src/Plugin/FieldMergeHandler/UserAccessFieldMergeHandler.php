<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

/**
 * Handles user access field.
 *
 * @FieldMergeHandler(
 *   id = "ahs_user_management_user_access_field_merge_handler",
 *   handles_entity_type = "user",
 *   handles_field_name = "access",
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class UserAccessFieldMergeHandler extends SingleValueFieldMergeHandler {

  /**
   * {@inheritdoc}
   */
  protected function mergeValue($primaryValue, $secondaryValue) {
    return max($primaryValue, $secondaryValue);
  }

}
