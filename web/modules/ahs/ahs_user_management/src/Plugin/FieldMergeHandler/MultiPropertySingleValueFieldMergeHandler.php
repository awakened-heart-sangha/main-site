<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandler\DefaultFieldMergeHandler;

/**
 * Handler for fields with a single value that can have multiple properties.
 */
class MultiPropertySingleValueFieldMergeHandler extends DefaultFieldMergeHandler {

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    $field_definition = $context->getPrimaryField()->getFieldDefinition();

    // If the field is computed, skip it.
    if ($field_definition->isComputed()) {
      $context->addProgress('Computed field - not attempting to merge.');
      return self::HANDLED;
    }

    $cardinality = $field_definition
      ->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality != 1) {
      throw new \Exception('The multi-property single value merge handler should only be used on fields with a cardinality of 1');
    }

    if ($context->getSecondaryField()->isEmpty()) {
      // Nothing to do.
    }
    elseif ($context->getPrimaryField()->isEmpty()) {
      // Use secondary.
      $context->getParentContext()->addProgress('Using value from secondary', $context->getFieldId());
      $context->getParentContext()->getPrimary()->set($context->getFieldId(), $context->getSecondaryField()->getValue());
    }
    else {
      // We have both a primary and a secondary item.
      $primaryItem = $context->getPrimaryField()->first()->getValue();
      $secondaryItem = $context->getSecondaryField()->first()->getValue();

      // Merge properties to create a merged value.
      $storage = $context->getPrimaryField()->getFieldDefinition()
        ->getFieldStorageDefinition();
      $mergedValue = [];
      foreach ($storage->getPropertyDefinitions() as $property_id => $property) {
        if ($property->isComputed()) {
          continue;
        }
        $primaryProperty = $primaryItem[$property_id] ?? NULL;
        $secondaryProperty = $secondaryItem[$property_id] ?? NULL;
        $mergedValue[$property_id] = $primaryProperty ?? $secondaryProperty;
      }

      // Set merged value on the field.
      $context->getParentContext()->addProgress('Merging properties', $context->getFieldId());
      $context->getParentContext()->getPrimary()->set($context->getFieldId(), [$mergedValue]);
    }
    return self::HANDLED;
  }

}
