<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

/**
 * Handles 'about' profile 'deceased' field.
 *
 * @FieldMergeHandler(
 *   id = "ahs_user_management_profile_deceased_field_merge_handler",
 *   handles_entity_type = "profile",
 *   handles_field_name = "field_deceased",
 * )
 */
class ProfileDeceasedFieldMergeHandler extends SingleValueFieldMergeHandler {

  /**
   * {@inheritdoc}
   */
  protected function mergeValue($primaryValue, $secondaryValue) {
    // Mark deceased if either user is marked deceased.
    return $primaryValue || $secondaryValue;
  }

}
