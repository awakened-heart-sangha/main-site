<?php

namespace Drupal\ahs_user_management\Plugin\FieldMergeHandler;

/**
 * Handles user access field.
 *
 * @FieldMergeHandler(
 *   id = "ahs_user_management_group_membership_participant_type_field_merge_handler",
 *   handles_entity_type = "group_content",
 *   handles_field_name = "field_participant_type",
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class GroupMembershipParticipantTypeFieldMergeHandler extends SingleValueFieldMergeHandler {

  /**
   * The field property to use.
   *
   * @var string
   */
  protected $property = 'target_id';

  /**
   * {@inheritdoc}
   */
  protected function mergeValue($primaryValue, $secondaryValue) {
    return max((int) $primaryValue, (int) $secondaryValue);
  }

}
