<?php

namespace Drupal\ahs_user_management\Plugin\EntityReferenceSelection;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;

/**
 * Alternative class for the UserSelection plugin, swapped in by hook.
 *
 * This alternate searches on a wider range of name-like fields.
 * But unfortunately it also disables matching the anonymous user.
 */
class AhsUserSelection extends UserSelection {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'append_email' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $form['append_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Append email to label when showing options.'),
      '#default_value' => $configuration['append_email'],
    ];
    $form += parent::buildConfigurationForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    // Never reference blocked users. The parent method adds this condition only
    // if the current user does not have the 'administer users' permission.
    $query->condition('status', 1);

    // Match against any name like column.
    if (isset($match)) {
      $words = preg_split('/ /', $match, -1, PREG_SPLIT_NO_EMPTY);
      foreach ($words as $word) {
        $group = $query
          ->orConditionGroup()
          ->condition('name', $word, $match_operator)
          ->condition('mail', $word, $match_operator)
          ->condition('field_display_name.value', $word, $match_operator);
        $query->condition($group);
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function entityQueryAlter(SelectInterface $query) {
    // Remove the blunt name condition added by parent class.
    $conditions = &$query->conditions();
    foreach ($conditions as $key => $condition) {
      if ($key !== '#conjunction' && is_string($condition['field']) && $condition['field'] === 'users_field_data.name') {
        // Remove the condition.
        unset($conditions[$key]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $options = parent::getReferenceableEntities($match, $match_operator, $limit);
    $configuration = $this->getConfiguration();
    if ($configuration['append_email'] || $this->getPluginId() === 'commerce:user') {
      foreach ($options as $bundle => $bundleOptions) {
        foreach ($bundleOptions as $id => $label) {
          $user = User::load($id);
          if ($email = $user->getEmail()) {
            $options[$bundle][$id] = "$label ($email)";
          }
        }
      }
    }
    return $options;
  }

}
