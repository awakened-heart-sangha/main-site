<?php

namespace Drupal\ahs_user_management\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\UserInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that uses different permissions depending on the current user.
 *
 * This is an alternative to the commerce_current_user argument plugin.
 * Because of https://www.drupal.org/project/drupal/issues/2778345 argument
 * validation is not properly considered for access, so inaccessible views will
 * still show up as local tasks on other pages.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "ahs_user_management_own_or_admin_permission",
 *   title = @Translation("Own or admin permission"),
 *   help = @Translation("Requires a different permission if the current user is targeted by contextual filter argument.")
 * )
 */
class OwnOrAdminPermission extends AccessPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = TRUE;

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleList;

  /**
   * Route match for the current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The operation options.
   *
   * @var array
   */
  protected $operationOptions = [
    'view' => 'View',
    'view_sensitive' => 'View sensitive',
    'update' => 'Update',
    'delete' => 'Delete',
  ];

  /**
   * Constructs a Permission object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_list
   *   The module handler.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match for the current route.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PermissionHandlerInterface $permission_handler, ModuleExtensionList $module_list, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->permissionHandler = $permission_handler;
    $this->moduleList = $module_list;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.permissions'),
      $container->get('extension.list.module'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $argument = $this->routeMatch->getRawParameter($this->options['param_name']);

    // If the argument is bad, deny access.
    if (!is_numeric($argument)) {
      return FALSE;
    }
    $user_storage = $this->entityTypeManager->getStorage('user');
    $user = $user_storage->load($argument);
    if (!$user instanceof UserInterface) {
      return FALSE;
    }

    // Maybe view targets current user.
    if ($user->id() == $account->id()) {
      if (!empty($this->options['perm'])) {
        $result = $account->hasPermission($this->options['perm']);
        return $result;
      }
      return TRUE;
    }

    // View must target another user.
    $result = FALSE;
    if (!empty($this->options['admin_operation'])) {
      $result = $user->access($this->options['admin_operation'], $account);
    }
    if (!empty($this->options['admin_permission'])) {
      $result = $result || $account->hasPermission($this->options['admin_permission']);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $arguments = [
      'own_permission' => $this->options['perm'],
      'admin_permission' => $this->options['admin_permission'],
      'admin_operation' => $this->options['admin_operation'],
      'parameter_name' => $this->options['param_name'],
    ];

    $route->setRequirement(
        '_ahs_user_management_own_or_admin_permission',
        serialize($arguments)
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = [];
    $options['param_name'] = ['default' => 'user'];
    $options['perm'] = ['default' => ''];
    $options['admin_operation'] = ['default' => ''];
    $options['admin_permission'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Build the list of all permissions grouped by module.
    $permissions = [];
    $perms = $this->permissionHandler->getPermissions();
    foreach ($perms as $perm => $perm_item) {
      $provider = $perm_item['provider'];
      $display_name = $this->moduleList->getName($provider);
      $permissions[$display_name][$perm] = strip_tags($perm_item['title']);
    }

    $form['param_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User parameter name'),
      '#empty_value' => '',
      '#default_value' => $this->options['param_name'],
      '#description' => $this->t('The name of the view parameter that contains the target user id.'),
    ];

    $form['perm'] = [
      '#type' => 'select',
      '#options' => $permissions,
      '#title' => $this->t('Own permission'),
      '#empty_value' => '',
      '#default_value' => $this->options['perm'],
      '#description' => $this->t('If the view targets the current user, then the selected permission is needed.'),
    ];

    $form['admin_operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Admin operation'),
      '#description' => $this->t('Operation access to grant access even if view targets another user.'),
      '#options' => $this->operationOptions,
      '#empty_value' => '',
      '#default_value' => $this->options['admin_operation'],
    ];

    $form['admin_permission'] = [
      '#type' => 'select',
      '#title' => $this->t('Admin permission'),
      '#description' => $this->t('Permission to grant access even if view targets another user.'),
      '#options' => $permissions,
      '#empty_value' => '',
      '#default_value' => $this->options['admin_permission'],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $permissions = $this->permissionHandler->getPermissions();
    if (isset($permissions[$this->options['perm']])) {
      $own = $permissions[$this->options['perm']]['title'];
    }
    else {
      $own = 'None';
    }

    $admin = '';
    if (!empty($this->operationOptions[$this->options['admin_operation']] ?? NULL)) {
      $admin = $this->operationOptions[$this->options['admin_operation']];
    }
    if (!empty($permissions[$this->options['admin_permission']] ?? NULL)) {
      $admin = empty($admin) ? $admin : $admin . ' & ';
      $admin .= $permissions[$this->options['admin_permission']]['title'];
    }
    if (empty($admin)) {
      $admin = 'None';
    }
    return "Own: $own; Admin: $admin";
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

}
