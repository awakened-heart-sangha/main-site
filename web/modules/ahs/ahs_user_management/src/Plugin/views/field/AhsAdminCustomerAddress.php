<?php

namespace Drupal\ahs_user_management\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides customer address field handler.
 *
 * @ViewsField("ahs_user_management_customer_address")
 */
class AhsAdminCustomerAddress extends FieldPluginBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Country manager service.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->countryManager = $container->get('country_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['profile_field'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['profile_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#default_value' => $this->options['profile_field'],
      '#required' => TRUE,
      '#options' => [
        'given_name' => $this->t('First name'),
        'additional_name' => $this->t('Middle name'),
        'family_name' => $this->t('Last name'),
        'country_code' => $this->t('Country'),
        'administrative_area' => $this->t('Administrative area'),
        'locality' => $this->t('Locality'),
        'address_line1' => $this->t('Address line 1'),
        'address_line2' => $this->t('Address line 2'),
        'postal_code' => $this->t('Postcode'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $build = [];

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getEntity($values);

    // Load all customer profiles for the user.
    $profiles = $this->entityTypeManager
      ->getStorage('profile')
      ->loadByProperties([
        'uid' => $user->id(),
        'type' => 'customer',
      ]);

    if ($profiles) {
      /** @var \Drupal\profile\Entity\ProfileInterface $profile */
      foreach ($profiles as $profile) {
        // Check if the profile is default and has an address field.
        if ($profile->get('is_default')->getString() &&
          $profile->hasField('address') &&
          !$profile->get('address')->isEmpty()
        ) {
          $address = $profile->get('address')->getValue();
          $field = $this->options['profile_field'];

          if (isset($address[0][$field])) {
            $value = $address[0][$field];

            // Replace the country code with the country name.
            if ('country_code' == $field) {
              $list = $this->countryManager->getList();

              if (isset($list[$value])) {
                $value = $list[$value]->render();
              }
            }

            $build = [
              '#markup' => $value,
            ];
            break;
          }
        }
      }
    }

    return $build;
  }

}
