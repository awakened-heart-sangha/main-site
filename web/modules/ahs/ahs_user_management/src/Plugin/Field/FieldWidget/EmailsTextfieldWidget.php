<?php

namespace Drupal\ahs_user_management\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ahs_user_management_emails_textfield' widget.
 *
 * @FieldWidget(
 *   id = "ahs_user_management_emails_textfield",
 *   label = @Translation("Emails text field"),
 *   field_types = {
 *     "email"
 *   },
 *   multiple_values = TRUE
 * )
 */
class EmailsTextfieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = [];
    foreach ($items as $item) {
      $values[] = $item->value;
    }
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => implode(', ', $values),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $text = $values['value'];
    $text = str_replace(',', ' ', $text);
    $text = str_replace(';', ' ', $text);
    $text = preg_replace('!\s+!', ' ', $text);
    $values = explode(' ', $text);
    return $values;
  }

}
