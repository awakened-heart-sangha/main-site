<?php

namespace Drupal\ahs_user_management\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access based on whether it is the current user's own content.
 *
 * Used in ahs_user_management's route subscriber to hide the user edit route
 * when looking at other users.
 */
class OwnAccessCheck implements AccessInterface {

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parameterized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    if (!$route->hasRequirement('_ahs_user_management_is_own_content')) {
      return AccessResult::neutral();
    }
    $require_own = filter_var($route->getRequirement('_ahs_user_management_is_own_content'), FILTER_VALIDATE_BOOLEAN);
    $user = $route_match->getParameters()->get('user');
    if ($user instanceof UserInterface) {
      if (($user->id() === $account->id()) === $require_own) {
        return AccessResult::allowed()->addCacheableDependency($user);
      }
    }
    return AccessResult::neutral();
  }

}
