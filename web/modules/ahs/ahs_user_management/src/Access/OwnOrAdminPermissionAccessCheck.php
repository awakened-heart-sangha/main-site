<?php

namespace Drupal\ahs_user_management\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Symfony\Component\Routing\Route;

/**
 * Access check views requiring different permission.
 *
 * This is used by the ahs_user_management_own_or_admin_permission
 * views access plugin.
 */
class OwnOrAdminPermissionAccessCheck implements AccessInterface {

  /**
   * The Entity type manager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OwnOrAdminPermissionAccessCheck object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Checks access result for a view.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parameterized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $arguments = unserialize($route->getRequirement('_ahs_user_management_own_or_admin_permission'));
    $parameter_value = $route_match->getRawParameter($arguments['parameter_name']);

    if ($account->id() === $parameter_value) {
      if (!empty($arguments['own_permission'])) {
        return AccessResult::allowedIfHasPermission($account, $arguments['own_permission'])->cachePerUser();
      }
      return AccessResult::allowed()->cachePerUser();
    }

    $user_storage = $this->entityTypeManager->getStorage('user');
    $user = $user_storage->load($parameter_value);
    if (!$user instanceof UserInterface) {
      return AccessResult::forbidden();
    }

    $result = AccessResult::neutral();
    if (!empty($arguments['admin_operation'])) {
      $result = $user->access($arguments['admin_operation'], $account, TRUE);
    }
    if (!empty($arguments['admin_permission'])) {
      $result = $result->orIf(AccessResult::allowedIfHasPermission($account, $arguments['admin_permission']));
    }

    $result = $result->cachePerUser();
    return $result;
  }

}
