<?php

namespace Drupal\ahs_user_management\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\PathValidator;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines the Me controller.
 */
class MeController extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  protected $pathValidator;

  /**
   * Constructs a UserCurrentPathsController object.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\Core\Path\PathValidator $path_validator
   *   The path validator.
   */
  public function __construct(AccountProxy $current_user, PathValidator $path_validator) {
    $this->currentUser = $current_user;
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('path.validator')
    );
  }

  /**
   * Handles wildcard (user/current/*) redirects for the current user.
   *
   * Replaces the second "current" parameter in the URL with the currently
   * logged in user and redirects to the target if the resulting path is valid.
   * Ohterwise throws a NotFoundHttpException. This is safe because the redirect
   * is handled as if the user entered the URL manually with all security
   * checks.
   */
  public function flexibleRedirect(string $arg1, string $arg2, string $arg3) {
    $path = '/user/' . $this->currentUser->id();
    if (!empty($arg1)) {
      $path .= '/' . $arg1;
    }
    if (!empty($arg2)) {
      $path .= '/' . $arg2;
    }
    if (!empty($arg3)) {
      $path .= '/' . $arg3;
    }

    $url = $this->pathValidator->getUrlIfValid($path);
    if ($url !== FALSE) {
      // Valid internal path:
      return $this->redirect($url->getRouteName(), $url->getRouteParameters(), $url->getOptions());
    }

    throw new NotFoundHttpException();
  }

}
