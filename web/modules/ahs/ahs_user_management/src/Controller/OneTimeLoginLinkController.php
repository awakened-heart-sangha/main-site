<?php

namespace Drupal\ahs_user_management\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines OneTimeLoginLinkController Class.
 *
 * @package Drupal\ahs_user_management\Controller
 */
class OneTimeLoginLinkController extends ControllerBase {

  /**
   * Date time formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs the service objects.
   */
  public function __construct(DateFormatter $date_formater) {
    $this->dateFormatter = $date_formater;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
    );
  }

  /**
   * Generates a one-time login (password reset) link for the given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user for which to generate the login link.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   A redirect to the destination, if one was provided.
   */
  public function generateLoginLink(AccountInterface $user) {
    $timeout = $this->config('user.settings')->get('password_reset_timeout');
    $url = user_pass_reset_url($user);
    $mail = $user->getEmail();
    $this->messenger()->addMessage($this->t('One-time login link created for %mail:<br/> <code>%login</code>', [
      '%mail' => $mail,
      '%login' => $url,
    ]));

    $this->messenger()->addMessage($this->t("This link is valid for %hr, and will become invalid if the user's password is updated by anyone.", [
      '%hr' => $this->dateFormatter->formatInterval($timeout),
    ]));

    if ($destination = $this->getRedirectDestination()->get()) {
      return new RedirectResponse($destination);
    }
  }

}
