<?php

namespace Drupal\ahs_user_management;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\user\UserAccessControlHandler as parentUserAccessControlHandler;

/**
 * Defines the access control handler for the user entity type.
 *
 * @see \Drupal\user\Entity\User
 */
class UserAccessControlHandler extends parentUserAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $result = parent::checkAccess($entity, $operation, $account);
    if ($result->isNeutral()) {
      if ($operation === 'update') {
        return AccessResult::allowedIfHasPermission($account, 'update user profiles');
      }
      if ($operation === 'create') {
        return AccessResult::allowedIfHasPermission($account, 'create users');
      }
      if ($operation === 'view_sensitive') {
        // People can always view their own sensitive info.
        if ($account->id() == $entity->id()) {
          return AccessResult::allowed()->cachePerUser();
        }
        $result = $this->checkSensitiveInformationAccess($account, $entity)->cachePerUser();
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    if ($operation == 'view') {
      // Anyone who can see the user can see some of their info.
      switch ($field_definition->getName()) {
        // Case 'timezone':
        // case 'user_picture':
        // Some info is sensitive.
        case 'preferred_langcode':
        case 'admin_profiles':
        case 'preferred_admin_langcode':
        case 'roles':
        case 'status':
        case 'access':
        case 'login':
          return $this->checkSensitiveFieldAccess($account, $items);

        case 'mail':
          return $this->checkMailAccess($account, $items);

        default:
          return AccessResult::allowed()->cachePerPermissions();
      }
    }
    $result = parent::checkFieldAccess($operation, $field_definition, $account, $items);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // Consider our 'create users' permission.
    return AccessResult::allowedIfHasPermissions($account, ['create users', 'administer users'], 'OR');
  }

  /**
   * Check access to sensitive fields.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account.
   * @param \Drupal\Core\Field\FieldItemListInterface|null $items
   *   Field values.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result.
   */
  protected function checkSensitiveFieldAccess(AccountInterface $account, FieldItemListInterface $items = NULL) {
    // The user might not always be available, according to
    // the signature of checkFieldAccess. When the user is available,
    // use our sensitive operation otherwise use our sensitive permission.
    if ($items && $user = $items->getEntity()) {
      return $user->access('view_sensitive', $account, TRUE);
    }
    return AccessResult::allowedIfHasPermission($account, 'view sensitive user information');
  }

  /**
   * Check access for the view_sensitive operation.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account seeking access.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The user entity to be viewed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Whether access should be granted.
   */
  protected function checkSensitiveInformationAccess(AccountInterface $account, EntityInterface $entity) {
    // Some users can see sensitive info for all other users.
    $result = AccessResult::allowedIfHasPermission($account, 'view sensitive user information');
    // Other users can only see sensitive info if a group gives them access.
    if (!$result->isAllowed()) {
      $groupsResult = $this->groupBasedAccess($account, $entity, ['view group member sensitive information']);
      $result = $result->orIf($groupsResult);
      if ($result instanceof AccessResultReasonInterface) {
        $reason = "The account must have the 'view sensitive user information' global permission, or the account must have the 'view group member sensitive information' group permission for a group that the user being accessed is a member of. ";
        $reason .= "The account does not have 'view sensitive user information'. ";
        if ($groupsResult instanceof AccessResultReasonInterface) {
          $reason .= $groupsResult->getReason();
          $result->setReason($reason);
        }
      }
    }
    return $result;
  }

  /**
   * Check access to mail field.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account.
   * @param \Drupal\Core\Field\FieldItemListInterface|null $items
   *   Field values.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result.
   */
  protected function checkMailAccess(AccountInterface $account, FieldItemListInterface $items = NULL) {
    if ($items && $user = $items->getEntity()) {
      if ($account->id() == $user->id()) {
        return AccessResult::allowed()->cachePerUser();
      }
    }
    // Adding the sensitive permission here makes it easier to setup views that show
    // emails but require the view sensitive permission to access the view.
    $result = AccessResult::allowedIfHasPermission($account, 'view user email addresses');
    // The user might not always be available, according to
    // the signature of checkFieldAccess..
    if ($result->isNeutral() && $items && $user = $items->getEntity()) {
      $group_result = $this->groupBasedAccess($account, $user, ['view group member contact information']);
      $result = $result->OrIf($group_result);
    }
    return $result->cachePerUser();
  }  

    /**
   * Check whether a group membership gives access to sensitive user info.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account seeking access.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The user to be viewed.
   * @param array $permissions
   *   The permissions to check.
   * @param string $conjunction
   *   (optional) 'AND' if all permissions are required, 'OR' in case just one.
   *   Defaults to 'AND'.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Whether access should be granted.
   */
  protected function groupBasedAccess(AccountInterface $account, EntityInterface $entity, array $permissions, $conjunction = 'AND') {
    // Only authenticated users can be group members.
    if ($entity->isAnonymous()) {
      $result = AccessResult::neutral();
      $reason = 'The user is anonymous.';
    }

    else {
      // If the user is not a member of any groups, we have nothing to say.
      $groupMemberships = \Drupal::service('group.membership_loader')->loadByUser($entity);
      $result = AccessResult::neutral();
      $reason = 'The user is not a member of any groups.';

      // Grant access if any of the user's group grants access.
      foreach ($groupMemberships as $groupMembership) {
        $reason = 'The user is not a member of any groups that have not ended more than a month ago.';
        $group = $groupMembership->getGroup();

        $endDate = $group->getEndDate();
        // If the group end date changes, access will be affected.
        $result->addCacheableDependency($group);
        // If the group ended more than a month ago, ignore it.
        $monthSeconds = 60 * 60 * 24 * 30;
        if (is_null($endDate) || ($group->getEndDateOffset() < ($monthSeconds * -1))) {
          continue;
        }

        $result = GroupAccessResult::allowedIfHasGroupPermissions($group, $account, $permissions, $conjunction);

        if ($result->isAllowed()) {
          // Our end date timestamps can sometimes be bigger values than
          // the database can store, so cap at 1 month.
          $maxAge = min($monthSeconds, $endDate->getTimestamp());
          $result->setCacheMaxAge($maxAge);
          break;
        }
        $permissions_message = implode(" $conjunction ",  $permissions);
        $reason = 'The user being accessed is a member one or more current groups ' . $group->id() . " but the account does not have the permissions '$permissions_message' for any of these groups.";
      }

      // Any added or deleted group membership may be relevant to access.
      $result = $result->addCacheTags(['group_content_list:plugin:group_membership']);
    }

    if ($result instanceof AccessResultReasonInterface) {
      $result->setReason($reason);
    }
    return $result;
  }

}
