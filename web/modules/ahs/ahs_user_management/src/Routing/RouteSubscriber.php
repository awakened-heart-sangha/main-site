<?php

namespace Drupal\ahs_user_management\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // onAlterRoutes() is a method in RouteSubscriberBase,
    // wrapping alterRoutes().
    $events[RoutingEvents::ALTER] = 'onAlterRoutes';
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Don't show the top level user edit route when admins view other users.
    if ($route = $collection->get('entity.user.edit_form')) {
      $route->setRequirement('_ahs_user_management_is_own_content', 'TRUE');
    }
    // Allow access to user profile lists even for single profile types
    // Useful for debugging extra profiles mistakenly created by API where
    // singluarity is not enforced.
    if ($route = $collection->get('profile.user_page.multiple')) {
      $requirements = $route->getRequirements();
      unset($requirements['_profile_type_multiple']);
      $route->setRequirements($requirements);
    }
  }

}
