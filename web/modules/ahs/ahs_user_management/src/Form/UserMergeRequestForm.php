<?php

namespace Drupal\ahs_user_management\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;

/**
 * Form for creating new user requests with a user specified from the route.
 */
class UserMergeRequestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "ahs_user_management_user_entity_merge_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['selection']['select'] = [
      '#type' => 'select2',
      '#title' => $this->t('Other user'),
      '#required' => TRUE,
      '#description' => $this->t('Find the other user by name or email'),
      '#autocomplete' => TRUE,
      '#target_type' => 'user',
      '#selection_settings' => [
        'append_email' => TRUE,
      ],
      '#select2' => ['width' => '40em'],
    ];

    $routeLabel = $this->getRouteUserLabel();
    $form['merge_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type of merge'),
      '#options' => [
        'to' => "Data from the other user will be merged into $routeLabel.",
        'from' => "Data from $routeLabel will be merged into the other user.",
      ],
      '#default_value' => 'to',
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['verify'] = [
      '#type' => 'submit',
      '#name' => 'verify_merge',
      '#value' => $this->t('Verify merge'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user = $this->getRouteMatch()->getParameter('user');
    if ($user->id() == $this->getOtherUserId($form_state)) {
      $form_state->setErrorByName('', "The other user must be different to current user");
    }
  }

  /**
   * Helper to get user ids.
   */
  protected function getOtherUserId(FormStateInterface $form_state) {
    return reset($form_state->getValue('select')[0]);
  }

  /**
   * Helper to get user label.
   */
  protected function getRouteUserLabel() {
    $user = $this->getRouteMatch()->getParameter('user');
    return $user->label() . " (" . $user->getEmail() . ")";
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $this->getRouteMatch()->getParameter('user');
    $other_id = $this->getOtherUserId($form_state);
    $primary_id = $user->id();
    $secondary_id = $other_id;
    if ($form_state->getValue('merge_type') === 'from') {
      $primary_id = $other_id;
      $secondary_id = $user->id();
    }

    $form_state->setRedirect('entity.entity_merge_request.verify', [
      'entity_type' => 'user',
      'primary_id' => $primary_id,
      'secondary_id' => $secondary_id,
    ]);
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return string
   *   The title for the UserMergeRequestForm.
   */
  public function getTitle(UserInterface $user) {
    return $this->t('Merge %label', ['%label' => $this->getRouteUserLabel()]);
  }

}
