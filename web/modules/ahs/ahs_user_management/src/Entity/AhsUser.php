<?php

namespace Drupal\ahs_user_management\Entity;

use Drupal\ahs_user_management\AhsUserInterface;
use Drupal\comment\Entity\Comment;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\Entity\User;

/**
 * Defines an ahs user entity class.
 *
 * This declares identified users to be authenticated,
 * which is necessary to use them with commerce.
 */
class AhsUser extends User implements AhsUserInterface {

  /**
   * Flag to indicate whether this user has decoupled authentication.
   *
   * @var bool
   */
  protected $decoupled = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);

    // Constructor values don't trigger onChange, so do it manually.
    $this->isDecoupled();
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    /** @var AhsUser[] $entities */
    parent::postLoad($storage, $entities);
    foreach ($entities as $entity) {
      $entity->isDecoupled();
    }
  }

  /**
   * {@inheritdoc}
   *
   * See https://www.drupal.org/project/decoupled_auth/issues/3045475 .
   */
  public function isAuthenticated() {
    return ($this->id() > 0);
  }

  /**
   * {@inheritdoc}
   *
   * See https://www.drupal.org/project/decoupled_auth/issues/3045470 .
   */
  public function onChange($name) {
    parent::onChange($name);
    // If name or ID have changed, recalculate the decoupled status.
    if (in_array($name, ['name', 'uid'])) {
      $this->isDecoupled();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isCoupled() {
    return !$this->decoupled;
  }

  /**
   * {@inheritdoc}
   */
  public function isDecoupled() {
    $this->decoupled = empty($this->name->value) || !($this->id() > 0);
    return $this->decoupled;
  }

  /**
   * {@inheritdoc}
   */
  public function decouple() {
    $this->decoupled = TRUE;
    $this->name = NULL;
    $this->pass = NULL;

    return $this;
  }

  /**
   * Track saves in progress to prevent recursions.
   *
   * @var array
   */
  protected static $savesInProgressUserIds = [];

  /**
   * Calculate the desired display name for this user.
   *
   * For use in hook_user_format_name_alter.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   An about profile to get name information from. Sometimes providing
   *   this explicitly is necessary to determine
   *   if an display name has changed when an entity is saved.
   * @param string $format
   *   The machine name of a configured name format from the name module.
   *
   * @return string
   *   Display name.
   */
  public function calculateDisplayName(ProfileInterface $profile = NULL, $format = 'sangha_informal_full') {
    $name = $this->getName($format, $profile);

    if (isset($name) && !empty((string) $name)) {
      $displayName = $name;
    }
    else {
      // Fallbacks if figuring out the name from the about profile
      // was not possible. Prefer first part of email as a fallback.
      $email = $this->getEmail();
      $username = $this->getAccountName();
      // If the username has a space, then use that.
      // This is a convenient hack for Behat features which set a username
      // and expect the display name to be that.
      $usernameHasASpace = !empty($username) && strpos($username, ' ');
      if ($usernameHasASpace) {
        $displayName = $username;
      }
      elseif (!empty($email)) {
        // If using email as dispay name, strip out @ and
        // everything after it for privacy.
        $displayName = substr($email, 0, strpos($email, '@'));
      }
      else {
        $displayName = $this->id();
      }
    }
    return $displayName;
  }

  /**
   * Helper to get name.
   */
  public function getName($format = 'sangha_informal_full', ProfileInterface $profile = NULL) {
    $nameComponents = $this->getNameComponents($profile) ?? [];
    // Name formatter returns renderable markup.
    $name = \Drupal::service('name.formatter')->format($nameComponents, $format);
    $name = empty((string) $name) ? NULL : html_entity_decode($name, ENT_QUOTES, 'UTF-8');
    return $name;
  }

  /**
   * Helper to get name components.
   */
  public function getNameComponents(ProfileInterface $profile = NULL) {
    $nameComponents = NULL;

    // Name the user based on their about profile.
    if (!$profile && $this->hasField('about_profiles')) {
      $profile = $this->get('about_profiles')->entity;
    }
    if ($profile) {

      // Load the firstname and lastname if set.
      $nameField = $profile->get('field_name')->get(0);
      $nameComponents = [];
      if (!is_null($nameField)) {
        $nameComponents = $nameField->getValue();
      }

      // Add in the additional 'preferred' and 'alternative'
      // component of the name field. This is a hack required
      // by various problems with the name module.
      $preferredField = $profile->getFieldDefinitions()['field_name']->getSetting('preferred_field_reference');
      $alternativeField = $profile->getFieldDefinitions()['field_name']->getSetting('alternative_field_reference');
      if (!$profile->get($preferredField)->isEmpty()) {
        $nameComponents['preferred'] = $profile->get($preferredField)
          ->getValue()[0]['value'];
      }
      if (!$profile->get($alternativeField)->isEmpty()) {
        $alternative = $profile->get($alternativeField)->getValue()[0]['value'];
        // You can use only your Sangha name if you really want by
        // making it your nickname too.
        if (!(isset($nameComponents['preferred']) && ($nameComponents['preferred'] === $alternative))) {
          $nameComponents['alternative'] = $alternative;
        }
      }
    }
    return $nameComponents;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    // Ensure that new users with duplicate emails are not created.
    // It's tempting to move this into ::preSave(), but that might interfere
    // with merging users.
    if (isset($values['mail']) && !empty($values['mail'])) {
      $duplicates = $storage->loadByProperties(['mail' => $values['mail']]);
      if (!empty($duplicates)) {
        throw new \Exception("A user with the email '{$values['mail']}' already exists.");
      }
    }
  }

  /**
   * Update the profile fields whenever the user is saved.
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // Moved from ahs_user_management.module user_presave hook.
    $module_handler = \Drupal::moduleHandler();

    // Wrap around email_registration_alter_username() so we only trigger it for
    // users that are coupled. Otherwise all users become coupled.
    if ($module_handler->moduleExists('email_registration') && isset($this->original)) {
      if ($this->isCoupled() && !$this->original->isCoupled()) {
        email_registration_alter_username($this, FALSE);
      }
    }
    // Trigger the role_delegation_user_presave hook before
    // our own roles management.
    if ($module_handler->moduleExists('role_delegation') && isset($this->original)) {
      role_delegation_user_presave($this);
    }

    // If a password is set - e.g. on a user password reset form - then ensure
    // the user is coupled. Unlike decoupled_auth, we allow decoupled users
    // to access password reset. Also, update the username every time the
    // user is saved in case a simpler username is available or can be released.
    if ($this->getPassword()) {
      // Only current members get simple usernames.
      if ($this->hasRole('member')) {
        // If the user is a member and the account name is already simple,
        // nothing to do. If the account name is not simple, maybe
        // the simple name has become available, try it.
        if ($this->getAccountName() !== $this->buildSimpleUsername()) {
          $this->setUsername($this->buildSimplestUniqueUsername($storage));
        }
      }
      else {
        // Non-members always get the id appended.
        $this->setUsername($this->buildGuaranteedUniqueUsername());
      }
    }

    // Prevent uses being simultaneously members and former members.
    if ($this->hasRole('member') && $this->hasRole('former_member')) {
      // Default to preferring member.
      $this->removeRole('former_member');
    }
    // Add former member role automatically except when merging users.
    if (isset($this->original) && $this->original->hasRole('member') && !$this->hasRole('member') && $this->isActive()) {
      $this->addRole('former_member');
    }

    // Warn about duplicate emails. These shouldn't exist,
    // this is a sanity check.
    if (!$this->isNew() && !empty($this->getEmail())) {
      $duplicateIds = $storage
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('mail', $this->getEmail())
        ->condition('status', 1)
        ->condition($this->getEntityType()->getKey('id'), $this->id(), '<>')
        ->execute();
      if (!empty($duplicateIds)) {
        $duplicateId = reset($duplicateIds);
        $duplicate = $storage->load($duplicateId);
        $name = $this->getDisplayName();
        $email = $this->getEmail();
        $id = $this->id();
        $duplicateName = $duplicate->getDisplayName();
        $duplicateId = $duplicate->id();
        \Drupal::logger('ahs_user_management')
          ->error("The email for $name ($id) has been changed to $email which was already the email for $duplicateName ($duplicateId).");
      }
    }

    // For existing users, it's better to enforce and set profiles presave,
    // to prevent the need for an extra save. New users don't have
    // an id yet so can't have profiles.
    if (!$this->isNew()) {
      $this->enforceProfiles();
      $this->reloadProfileFields();
      self::$savesInProgressUserIds[$this->id()] = $this->id();
    }

    if ($this->hasField('field_display_name')) {
      $name_components = $this->getNameComponents();
      if (isset($name_components) && !empty($name_components)) {
        $name_components = array_filter($name_components);
        $this->set('field_display_name', implode(' ', $name_components));
      }
      else {
        $this->set('field_display_name', $this->getDisplayName());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // New users don't have an id so can't have profiles until after saving and
    // user custom field_display_name isn't filled.
    // Therefore trigger a second save.
    if (!$update) {
      $this->save();
    }

    $this->handleMembership();

    if ($this->get('mail')->isEmpty()) {
      \Drupal::logger('ahs_user_management')->warning("An empty email has been saved for user " . $this->id());
    }
    else {
      /** @var \Drupal\Core\Entity\EntityConstraintViolationListInterface $violations */
      $violations = $this->get('mail')->validate();
      if ($violations->count() > 0) {
        $message = $violations[0]->getMessage();
        \Drupal::logger('ahs_user_management')->warning("Invalid email saved for user " . $this->id() . ": '" . (string) $this->getEmail() . "'. " . $message);
      }
    }
  }

  /**
   * Helper to clear profile cache.
   */
  public function reloadProfileFields() {
    // Update the field for type.
    /** @var \Drupal\profile\ProfileStorageInterface $profile_storage */
    $types = array_keys(\Drupal::service('entity_type.bundle.info')
      ->getBundleInfo('profile'));
    $profile_storage = \Drupal::entityTypeManager()->getStorage('profile');
    foreach ($types as $type) {
      $profiles = $profile_storage->loadMultipleByUser($this, $type, TRUE);
      $profile_storage->resetCache(array_keys($profiles));
      $profiles = $profile_storage->loadMultiple(array_keys($profiles));
      if (!$profiles) {
        $profiles = NULL;
      }
      $this->{$type . '_profiles'} = $profiles;
    }
  }

  /**
   * Helper to force create user profiles.
   */
  protected function enforceProfiles() {
    $types = ['admin', 'about'];
    $profile_storage = \Drupal::entityTypeManager()->getStorage('profile');
    foreach ($types as $type) {
      if ($this->{$type . '_profiles'}->isEmpty()) {
        $profile = $profile_storage->loadByUser($this, $type);
        if (empty($profile)) {
          $profile = $profile_storage->create(['uid' => $this->id(), 'type' => $type]);
          $profile->setPublished();
          $profile->save();
        }
      }
    }
  }

  /**
   * Handle the member role being given to a user.
   */
  protected function handleMembership() {
    if ($this->hasRole('member')) {
      if (is_null($this->getMembershipStartDate())) {
        $this->setMembershipStartTime();
      }
      $membershipGroup = Group::load(583);
      if ($membershipGroup && !$membershipGroup->getMember($this)) {
        $membership = [
          'field_participant_type' => ['target_id' => 75],
          'created' => $this->getMembershipStartDate()->getTimestamp(),
        ];
        $membershipGroup->addMember($this, $membership);
      }
    }
    else {
      $membershipGroup = Group::load(583);
      if ($membershipGroup && $membership = $membershipGroup->getMember($this)) {
        $content = $membership->getGroupRelationship();
        $storage = \Drupal::entityTypeManager()->getStorage('group_content');
        $storage->delete([$content]);
      }
    }
  }

  /**
   * Gets a label for the user to use in debugging and test error messages.
   *
   * @return string
   *   Debug label.
   */
  public function getDebugLabel() {
    return $this->label() . " (" . $this->id() . ")";
  }

  /**
   * Get the membership start date for the user.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   Membership start time.
   */
  public function getMembershipStartDate() {
    $adminProfile = $this->admin_profiles->entity;
    if ($adminProfile && !$adminProfile->get('field_membership_start')->isEmpty()) {
      return $adminProfile->get('field_membership_start')->date;
    }
  }

  /**
   * Set the membership start date for the user.
   *
   * @param int|null $timestamp
   *   Membership start time.
   */
  public function setMembershipStartTime(int $timestamp = NULL) {
    $adminProfile = $this->admin_profiles->entity;
    if ($adminProfile) {
      $timestamp = $timestamp ?? \Drupal::time()->getRequestTime();
      $date = DrupalDateTime::createFromTimestamp($timestamp);
      $adminProfile->set('field_membership_start', $date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT));
      $adminProfile->save();
    }
  }

  /**
   * Build the ideal simple form of username.
   */
  protected function buildSimpleUsername() {
    $name = $this->getDisplayName();
    $name = preg_replace("/[^A-Za-z ]/", '', $name);
    $name = strtolower($name);
    $name = str_replace(' ', '_', trim($name));
    return $name;
  }

  /**
   * Helper to build random name.
   */
  protected function buildGuaranteedUniqueUsername() {
    // Conceivably the user could be new without an id.
    $id = $this->id() ?? 'temp';
    $name = $this->buildSimpleUsername() . "_" . $id;
    $name = email_registration_unique_username($name, $id);
    return $name;
  }

  /**
   * Helper to build simple unique name.
   */
  protected function buildSimplestUniqueUsername(EntityStorageInterface $storage) {
    $simpleName = $this->buildSimpleUsername();
    $ids = $storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('name', $simpleName)
      ->condition('uid', $this->id(), '<>')
      ->execute();
    if (empty($ids)) {
      return $simpleName;
    }
    return $this->buildGuaranteedUniqueUsername();
  }

  /**
   * Helper to get user name.
   */
  public function getDiscourseUsername() {
    if (!empty($this->getAccountName())) {
      return $this->getAccountName();
    }
    return $this->buildGuaranteedUniqueUsername();
  }

  /**
   * Helper to add admin note.
   */
  public function addAdminNote($comment, $format = 'plain_text') {
    $comment = Comment::create([
      'uid' => \Drupal::currentUser()->id(),
      'entity_type' => 'profile',
      'entity_id' => $this->admin_profiles->target_id,
      'field_name' => 'field_notes',
      'comment_type' => 'notes',
      'subject' => '',
      'comment_body' => [
        'value' => $comment,
        'format' => $format,
      ],
      'status' => 1,
    ]);
    $comment->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Make name not required at a database level and swap the constraint.
    $constraints = $fields['name']->getConstraints();
    $constraints = ['AhsUserName' => $constraints['UserName']] + $constraints;
    unset($constraints['UserName'], $constraints['NotNull']);
    $fields['name']
      ->setRequired(FALSE)
      ->setConstraints($constraints);

    return $fields;
  }

}
