<?php

namespace Drupal\ahs_user_management;

use Drupal\user\UserInterface;

/**
 * Provides an interface defining a user entity.
 *
 * @ingroup user_api
 */
interface AhsUserInterface extends UserInterface {

  /**
   * Check whether this user is decoupled.
   *
   * @return bool
   *   Whether this user is decoupled.
   */
  public function isDecoupled();

  /**
   * Set this user to the decoupled state.
   *
   * @return AhsUserInterface
   *   The user being decoupled.
   */
  public function decouple();

  /**
   * Check whether this user is coupled.
   *
   * @return bool
   *   Whether this user is coupled.
   */
  public function isCoupled();

}
