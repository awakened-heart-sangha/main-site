<?php

namespace Drupal\ahs_user_management;

use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Identify the current user by tempstore if not logged in.
 *
 * The current_user service is implemented by the AccountProxy class.
 */
class IdentifiedAccountProxy extends AccountProxy {

  /**
   * Tempstore definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempstore;

  /**
   * AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Bool to identify.
   *
   * @var bool
   */
  protected $identified = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(EventDispatcherInterface $eventDispatcher, PrivateTempStoreFactory $tempstore, AccountProxyInterface $currentUser) {
    parent::__construct($eventDispatcher);
    $this->tempstore = $tempstore->get('ahs_user_management.identification');
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccount() {
    if (!$this->currentUser->isAnonymous()) {
      return $this->currentUser->getAccount();
    }
    // Try to identify the user from tempstore.
    if (!isset($this->account) && empty($this->id)) {
      $uid = $this->tempstore->get('uid');
      if ($uid) {
        $this->id = $uid;
      }
    }
    return parent::getAccount();
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->getAccount()->id();
  }

  /**
   * {@inheritdoc}
   */
  public function isIdentifiedNotAuthenticated() {
    return $this->currentUser->isAnonymous() && !$this->getAccount()->isAnonymous();
  }

  /**
   * Identify the current user tentatively.
   *
   * @param int $uid
   *   User id.
   */
  public function setIdentified($uid) {
    $this->tempstore->set('uid', $uid);
    $this->id = $uid;
  }

}
