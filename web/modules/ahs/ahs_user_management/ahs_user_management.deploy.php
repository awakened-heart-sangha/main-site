<?php

/**
 * @file
 * Install, update and uninstall functions for the AHS miscellaneous module.
 */

use Drupal\group\Entity\Group;
use Drupal\profile\Entity\Profile;
use Drupal\user\Entity\User;

/**
 * Set Europe/London as everyone's default timezone.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0001_london_user_timezones(&$sandbox) {
  $uids = \Drupal::entityTypeManager()->getStorage('user')
    ->getQuery()
    ->accessCheck(FALSE)
    ->condition('timezone', 'Europe/London', '<>')
    ->execute();
  $users = User::loadMultiple($uids);
  foreach ($users as $user) {
    $user->set('timezone', 'Europe/London')->save();
  }
}

/**
 * Sets a membership start date fpr any member missing one .
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0003_membership_start_dates(&$sandbox) {
  $uids = \Drupal::entityTypeManager()->getStorage('user')->getQuery()
    ->accessCheck(FALSE)
    ->sort('uid')
    ->execute();

  $last = 0;
  foreach ($uids as $id) {
    try {
      $user = User::load($id);
      if (empty($user->getCreatedTime())) {
        $user->set('created', $last);
        $user->save();
      }
      else {
        $last = $user->getCreatedTime();
      }
      $hasBeenMember = $user->hasRole('member') || $user->hasRole('former_member');
      if ($hasBeenMember && empty($user->getMembershipStartDate())) {
        $user->setMembershipStartTime($user->getCreatedTime());
        \Drupal::logger('ahs_user_management')
          ->notice("Set start time for user " . $id . " " . $user->label() . " as  " . $user->getCreatedTime());
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error setting membership start time for user " . $id . ": " . $e->getMessage());
    }
  }
}

/**
 * Sets a membership start date fpr any member missing one .
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0004_add_members_to_group(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->condition('roles', 'member')
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      $membershipGroup = Group::load(583);
      $membership = [
        'field_participant_type' => ['target_id' => 75],
        'created' => $user->getMembershipStartDate()->getTimestamp(),
      ];
      $membershipGroup->addMember($user, $membership);
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error adding to membership group user " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users added to membership group');
}

/**
 * Sets a membership start date fpr any member missing one .
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0005_empty_alternatives_if_no_mail(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      if (!$user->getEmail() && !$user->get('alternative_user_emails')->isEmpty()) {
        $user->set('alternative_user_emails', NULL);
        $user->save();
        \Drupal::logger('ahs_user_management')
          ->notice("Emptied alternative emails for user " . $id . " " . $user->label());
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error adding emptying alternative emails for user " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users cleared of alternative emails');
}

/**
 * Sets a membership start date fpr any member missing one .
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0006_move_address_to_customer_profile(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      $name = $user->getDisplayName();
      $uid = $user->id();
      $profileStorage = \Drupal::entityTypeManager()->getStorage('profile');

      $aboutProfile = $profileStorage->loadByUser($user, 'about');
      if (!$aboutProfile) {
        \Drupal::logger('ahs_user_management')->notice("User $name ($uid) has no about profile so no existing address to copy.");
        return;
      }
      $aboutAddressField = $aboutProfile->get('field_address');
      if ($aboutAddressField->isEmpty()) {
        \Drupal::logger('ahs_user_management')->notice("User $name ($uid) has no address on about profile so no existing address to copy.");
        return;
      }
      $aboutLine1 = strtolower(preg_replace("/[^A-Za-z0-9]/", '', $aboutAddressField->first()->getAddressLine1()));
      $aboutPostalCode = strtolower(preg_replace("/[^A-Za-z0-9]/", '', $aboutAddressField->first()->getPostalCode()));

      $given = NULL;
      $family = NULL;
      $aboutNameField = $aboutProfile->get('field_name');
      if (!$aboutNameField->isEmpty()) {
        $given = $aboutNameField->first()->given ?? NULL;
        $family = $aboutNameField->first()->family ?? NULL;
      }
      $telephone = NULL;
      if (!$aboutProfile->get('field_telephone_mobile')->isEmpty()) {
        $telephone = $aboutProfile->get('field_telephone_mobile')->value;
      }
      if (!$telephone && !$aboutProfile->get('field_telephone_home')->isEmpty()) {
        $telephone = $aboutProfile->get('field_telephone_home')->value;
      }

      $customerProfile = $profileStorage->loadByUser($user, 'customer');
      if ($customerProfile) {
        $customerAddressField = $customerProfile->get('address');
        if (!$customerAddressField->isEmpty()) {
          $customerLine1 = strtolower(preg_replace("/[^A-Za-z0-9]/", '', $customerAddressField->first()->getAddressLine1()));
          $customerPostalCode = strtolower(preg_replace("/[^A-Za-z0-9]/", '', $customerAddressField->first()->getPostalCode()));
          $line1Match = $aboutLine1 == $customerLine1;
          $postalCodeMatch = $aboutPostalCode == $customerPostalCode;
          if ($line1Match && $postalCodeMatch) {
            \Drupal::logger('ahs_user_management')->notice("User $name ($uid) already has customer profile with address matching about profile.");
            return;
          }
          $given = $given ?? $customerAddressField->first()->getGivenName();
          $family = $family ?? $customerAddressField->first()->getFamilyName();
        }
      }

      $newProfile = Profile::create(['type' => 'customer', 'uid' => $user->id()]);
      $newProfile->setPublished();
      $newProfile->setDefault(TRUE);
      $newProfile->address[0] = [
        'given_name' => $given,
        'family_name' => $family,
        'address_line1' => $aboutAddressField->first()->getAddressLine1(),
        'address_line2' => $aboutAddressField->first()->getAddressLine2(),
        'sorting_code' => $aboutAddressField->first()->getSortingCode(),
        'postal_code' => $aboutAddressField->first()->getPostalCode(),
        'dependent_locality' => $aboutAddressField->first()->getDependentLocality(),
        'locality' => $aboutAddressField->first()->getLocality(),
        'administrative_area' => $aboutAddressField->first()->getAdministrativeArea(),
        'country_code' => $aboutAddressField->first()->getCountryCode(),
      ];
      $newProfile->set('field_telephone', $telephone);
      $newProfile->save();
      \Drupal::logger('ahs_user_management')->notice("New customer profile created for user $name ($uid).");
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error fixing address for $name ($id): " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users with address fixed');
}

/**
 * Resave all members to fix usernames.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0007_resync_rebuild_usernames(&$sandbox) {
  $jobsLoader = function () {
    $query = \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->exists('name');
    return $query->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      $user->save();
      \Drupal::logger('ahs_user_management')->notice("Resaved user: " . $id . " " . $user->label());
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error resaving user for account name rebuild " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users saved for account name rebuilding');
}

/**
 * Resave all active users to fill field_display_name.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0008_fill_user_field_display_name(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', '1')
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);

      $violations = $user->validate();
      if ($violations->count() > 0) {
        \Drupal::logger('ahs_user_validate')->warning($user->label() . ' failed validation');
      }

      $user->save();
      \Drupal::logger('ahs_user_management')->notice("Resaved user: " . $id . " " . $user->label());
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error resaving user for filling the field_display_name " . $id . ": " . $e->getMessage());
    }
  };

  \Drupal::moduleHandler()->loadInclude('ahs_miscellaneous', 'deploy.php');
  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users saved for filling the field_display_name');
}

/**
 * Deletes spam users.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0009_delete_spam(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      if (_ahs_user_management_is_spam_user($user)) {
        $mail = $user->getEmail() ?? $user->id();
        $user->delete();
        \Drupal::logger('ahs_user_management')
          ->error("Deleted spam user " . $mail . '.');
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error deleting spam user " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users checked for spam');
}

/**
 * Deletes spam users.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0010_delete_spam(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      if (_ahs_user_management_is_spam_user($user)) {
        $mail = $user->getEmail() ?? $user->id();
        $user->delete();
        \Drupal::logger('ahs_user_management')
          ->error("Deleted spam user " . $mail . '.');
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error deleting spam user " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users checked for spam');
}

/**
 * Deletes spam users.
 *
 * Implements hook_deploy_NAME().
 */
function ahs_user_management_deploy_0011_delete_spam(&$sandbox) {
  $jobsLoader = function () {
    return \Drupal::entityTypeManager()->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->execute();
  };

  $jobProcessor = function ($id) {
    try {
      $user = User::load($id);
      if (_ahs_user_management_is_spam_user($user)) {
        $mail = $user->getEmail() ?? $user->id();
        $user->delete();
        \Drupal::logger('ahs_user_management')
          ->error("Deleted spam user " . $mail . '.');
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('ahs_user_management')
        ->error("Error deleting spam user " . $id . ": " . $e->getMessage());
    }
  };

  return ahs_miscellaneous_batch_updater($sandbox, $jobsLoader, $jobProcessor, 'users checked for spam');
}