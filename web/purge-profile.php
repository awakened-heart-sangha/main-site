<?php

\Drupal::configFactory()->getEditable('core.extension')
  ->set('profile', 'ahs')
  ->save();
drupal_flush_all_caches();

\Drupal::service('module_installer')->install(['ahs']);  


\Drupal::service('module_installer')->uninstall(['minimal']);
\Drupal::service('module_installer')->uninstall(['block_visibility_groups']);

$sc = \Drupal::keyValue('system.schema');
if ($sc->get('minimal')) {
  $sc->delete('minimal');
}
drupal_flush_all_caches();

