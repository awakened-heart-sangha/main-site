From d18fab9234e7acc7878d83c38aea20f6caff2cc0 Mon Sep 17 00:00:00 2001
From: James Glasgow <21148-jrglasgow@users.noreply.drupalcode.org>
Date: Tue, 6 Dec 2022 11:37:20 -0700
Subject: [PATCH 01/12] Issue #3325652: deprecated alter hook
 hook_field_widget_form_alter()

---
 filefield_sources.module | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/filefield_sources.module b/filefield_sources.module
index a5f6f71..3591182 100644
--- a/filefield_sources.module
+++ b/filefield_sources.module
@@ -91,11 +91,11 @@ function filefield_sources_field_widget_settings_summary_alter(&$summary, $conte
 }
 
 /**
- * Implements hook_field_widget_form_alter().
+ * Implements hook_field_widget_single_element_form_alter().
  *
  * Add file field sources widget's settings to element.
  */
-function filefield_sources_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
+function filefield_sources_field_widget_single_element_form_alter(array &$element, \Drupal\Core\Form\FormStateInterface $form_state, array $context) {
   $plugin = $context['widget'];
   if (in_array($plugin->getPluginId(), \Drupal::moduleHandler()->invokeAll('filefield_sources_widgets'))) {
     $element['#filefield_sources_settings'] = $plugin->getThirdPartySetting('filefield_sources', 'filefield_sources');
-- 
GitLab


From dfe015ac3cd209d948609e58bf1a3e8ee1715f0a Mon Sep 17 00:00:00 2001
From: James R Glasgow <james.glasgow@associates.hq.dhs.gov>
Date: Fri, 27 Jan 2023 11:58:55 -0700
Subject: [PATCH 02/12] Issue #3336268: Drupal 10 compatibility

---
 composer.json              | 2 +-
 filefield_sources.info.yml | 2 +-
 2 files changed, 2 insertions(+), 2 deletions(-)

diff --git a/composer.json b/composer.json
index f888525..2e2a0e3 100644
--- a/composer.json
+++ b/composer.json
@@ -28,7 +28,7 @@
     "license": "GPL-2.0-or-later",
     "minimum-stability": "dev",
     "require": {
-        "drupal/core": "^8 || ^9"
+        "drupal/core": "^9.2 || ^10"
     },
     "require-dev": {
         "drupal/imce": "^2.3"
diff --git a/filefield_sources.info.yml b/filefield_sources.info.yml
index fada96c..488afb8 100644
--- a/filefield_sources.info.yml
+++ b/filefield_sources.info.yml
@@ -2,7 +2,7 @@ name: File Field Sources
 type: module
 description: 'Extends File fields to allow referencing of existing files, remote files, and server files.'
 package: Fields
-core_version_requirement: ^8 || ^9
+core_version_requirement: ^9.2 || ^10
 dependencies:
   - drupal:file
   - drupal:system
-- 
GitLab


From 1130da57ddac38d8ef5ee4bad2dbb80a55d83a58 Mon Sep 17 00:00:00 2001
From: James R Glasgow <james.glasgow@associates.hq.dhs.gov>
Date: Wed, 22 Mar 2023 12:30:55 -0600
Subject: [PATCH 03/12] 3336268 updated
 src/ProxyClass/File/MimeType/ExtensionMimeTypeGuesser.php and limiting to
 Drupal 10

---
 composer.json                                  |  4 ++--
 filefield_sources.info.yml                     |  2 +-
 .../File/MimeType/ExtensionMimeTypeGuesser.php | 18 +++++++++++++-----
 3 files changed, 16 insertions(+), 8 deletions(-)

diff --git a/composer.json b/composer.json
index 2e2a0e3..a73aa09 100644
--- a/composer.json
+++ b/composer.json
@@ -28,9 +28,9 @@
     "license": "GPL-2.0-or-later",
     "minimum-stability": "dev",
     "require": {
-        "drupal/core": "^9.2 || ^10"
+        "drupal/core": "^10"
     },
     "require-dev": {
-        "drupal/imce": "^2.3"
+        "drupal/imce": "^3.0"
     }
 }
diff --git a/filefield_sources.info.yml b/filefield_sources.info.yml
index 488afb8..26a7578 100644
--- a/filefield_sources.info.yml
+++ b/filefield_sources.info.yml
@@ -2,7 +2,7 @@ name: File Field Sources
 type: module
 description: 'Extends File fields to allow referencing of existing files, remote files, and server files.'
 package: Fields
-core_version_requirement: ^9.2 || ^10
+core_version_requirement: ^10
 dependencies:
   - drupal:file
   - drupal:system
diff --git a/src/ProxyClass/File/MimeType/ExtensionMimeTypeGuesser.php b/src/ProxyClass/File/MimeType/ExtensionMimeTypeGuesser.php
index 241f728..ce53a29 100644
--- a/src/ProxyClass/File/MimeType/ExtensionMimeTypeGuesser.php
+++ b/src/ProxyClass/File/MimeType/ExtensionMimeTypeGuesser.php
@@ -1,5 +1,5 @@
 <?php
-// @codingStandardsIgnoreFile
+// phpcs:ignoreFile
 
 /**
  * This file was generated via php core/scripts/generate-proxy-class.php 'Drupal\filefield_sources\File\MimeType\ExtensionMimeTypeGuesser' "modules/contrib/filefield_sources/src".
@@ -12,7 +12,7 @@ namespace Drupal\filefield_sources\ProxyClass\File\MimeType {
      *
      * @see \Drupal\Component\ProxyBuilder
      */
-    class ExtensionMimeTypeGuesser implements \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface
+    class ExtensionMimeTypeGuesser implements \Symfony\Component\Mime\MimeTypeGuesserInterface
     {
 
         use \Drupal\Core\DependencyInjection\DependencySerializationTrait;
@@ -86,19 +86,27 @@ namespace Drupal\filefield_sources\ProxyClass\File\MimeType {
         /**
          * {@inheritdoc}
          */
-        public function guess($path)
+        public function guessMimeType($path): ?string
         {
-            return $this->lazyLoadItself()->guess($path);
+            return $this->lazyLoadItself()->guessMimeType($path);
         }
 
         /**
          * {@inheritdoc}
          */
-        public function setMapping(array $mapping = NULL)
+        public function setMapping(?array $mapping = NULL)
         {
             return $this->lazyLoadItself()->setMapping($mapping);
         }
 
+        /**
+         * {@inheritdoc}
+         */
+        public function isGuesserSupported(): bool
+        {
+            return $this->lazyLoadItself()->isGuesserSupported();
+        }
+
     }
 
 }
-- 
GitLab


From 2795573cb10b31c12785fad4e387803010bf416e Mon Sep 17 00:00:00 2001
From: James R Glasgow <james.glasgow@associates.hq.dhs.gov>
Date: Thu, 23 Mar 2023 08:02:31 -0600
Subject: [PATCH 04/12] #3336268 since symfony/mime was added in Drupal 9.2.0
 the module should be compatible with ^9.2 | ^10

---
 composer.json              | 2 +-
 filefield_sources.info.yml | 2 +-
 2 files changed, 2 insertions(+), 2 deletions(-)

diff --git a/composer.json b/composer.json
index a73aa09..498cbb6 100644
--- a/composer.json
+++ b/composer.json
@@ -28,7 +28,7 @@
     "license": "GPL-2.0-or-later",
     "minimum-stability": "dev",
     "require": {
-        "drupal/core": "^10"
+        "drupal/core": "^9.2.0 | ^10"
     },
     "require-dev": {
         "drupal/imce": "^3.0"
diff --git a/filefield_sources.info.yml b/filefield_sources.info.yml
index 26a7578..488afb8 100644
--- a/filefield_sources.info.yml
+++ b/filefield_sources.info.yml
@@ -2,7 +2,7 @@ name: File Field Sources
 type: module
 description: 'Extends File fields to allow referencing of existing files, remote files, and server files.'
 package: Fields
-core_version_requirement: ^10
+core_version_requirement: ^9.2 || ^10
 dependencies:
   - drupal:file
   - drupal:system
-- 
GitLab


From 89f0604c32b40263bc0bf37f17672ebccb57ebff Mon Sep 17 00:00:00 2001
From: Dan Denysenko <dan.denysenko@gmail.com>
Date: Tue, 15 Aug 2023 15:20:42 -0400
Subject: [PATCH 05/12] Replace the implementation based on
 file_munge_filename().

---
 filefield_sources.module | 9 +++++++--
 1 file changed, 7 insertions(+), 2 deletions(-)

diff --git a/filefield_sources.module b/filefield_sources.module
index 3591182..015d26d 100644
--- a/filefield_sources.module
+++ b/filefield_sources.module
@@ -10,6 +10,7 @@ use Drupal\Component\Utility\NestedArray;
 use Drupal\Core\Field\WidgetBase;
 use Drupal\Core\Field\WidgetInterface;
 use Drupal\Core\Field\FieldDefinitionInterface;
+use Drupal\Core\File\Event\FileUploadSanitizeNameEvent;
 use Drupal\Core\Render\Element;
 use Drupal\imce\Imce;
 use Drupal\file\Entity\File;
@@ -472,7 +473,9 @@ function filefield_sources_save_file($filepath, $validators = [], $destination =
   if (!empty($extensions)) {
     // Munge the filename to protect against possible malicious extension hiding
     // within an unknown file type (ie: filename.html.foo).
-    $file->setFilename(file_munge_filename($file->getFilename(), $extensions));
+    $event = new FileUploadSanitizeNameEvent($file->getFilename(), $extensions);
+    \Drupal::service('event_dispatcher')->dispatch($event);
+    $file->setFilename($event->getFilename());
   }
 
   // Rename potentially executable files, to help prevent exploits (i.e. will
@@ -593,7 +596,9 @@ function filefield_sources_clean_filename($filepath, $extensions) {
   if (empty($extensions)) {
     $extensions = 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp';
   }
-  $filename = file_munge_filename($filename, $extensions);
+  $event = new FileUploadSanitizeNameEvent($filename, $extensions);
+  \Drupal::service('event_dispatcher')->dispatch($event);
+  $filename = $event->getFilename();
   $directory = \Drupal::service('file_system')->dirname($filepath);
   return ($directory != '.' ? $directory . '/' : '') . $filename;
 }
-- 
GitLab


From 1c561fdd4406a3df56545a7f8aaa865683b0811e Mon Sep 17 00:00:00 2001
From: Rob Carr <3470-robcarr@users.noreply.drupalcode.org>
Date: Thu, 2 Nov 2023 16:18:21 +0200
Subject: [PATCH 06/12] Issue #3336268: Drupal 10 compatibility

---
 filefield_sources.module | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/filefield_sources.module b/filefield_sources.module
index 015d26d..2846bd0 100644
--- a/filefield_sources.module
+++ b/filefield_sources.module
@@ -446,7 +446,7 @@ function filefield_sources_save_file($filepath, $validators = [], $destination =
     'status' => FileSystemInterface::EXISTS_RENAME,
   ]);
   $file->setFilename(trim(basename($filepath), '.'));
-  $file->setMimeType(\Drupal::service('file.mime_type.guesser')->guess($file->getFilename()));
+  $file->setMimeType(\Drupal::service('file.mime_type.guesser')->guessMimeType($file->getFilename()));
   $file->setSize(filesize($filepath));
 
   $extensions = '';
-- 
GitLab


From 0f425d1031e68a2818683b4d2ccc60b5e1f19f64 Mon Sep 17 00:00:00 2001
From: Ryan Schwab <rwschwab@ucsc.edu>
Date: Mon, 11 Dec 2023 14:45:01 -0800
Subject: [PATCH 07/12] Test setUp functions must declare void as return type

---
 tests/src/Functional/EmptyValuesTest.php          | 2 +-
 tests/src/Functional/FileFieldSourcesTestBase.php | 2 +-
 tests/src/Functional/ImceSourceTest.php           | 2 +-
 tests/src/Functional/MultipleValuesTest.php       | 2 +-
 4 files changed, 4 insertions(+), 4 deletions(-)

diff --git a/tests/src/Functional/EmptyValuesTest.php b/tests/src/Functional/EmptyValuesTest.php
index 886515c..5e697af 100644
--- a/tests/src/Functional/EmptyValuesTest.php
+++ b/tests/src/Functional/EmptyValuesTest.php
@@ -23,7 +23,7 @@ class EmptyValuesTest extends FileFieldSourcesTestBase {
   /**
    * Sets up for empty values test case.
    */
-  protected function setUp() {
+  protected function setUp() : void {
     parent::setUp();
     $this->setUpImce();
   }
diff --git a/tests/src/Functional/FileFieldSourcesTestBase.php b/tests/src/Functional/FileFieldSourcesTestBase.php
index 7ad4989..e587c1c 100644
--- a/tests/src/Functional/FileFieldSourcesTestBase.php
+++ b/tests/src/Functional/FileFieldSourcesTestBase.php
@@ -42,7 +42,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
   /**
    * Sets up for file field sources test cases.
    */
-  protected function setUp() {
+  protected function setUp() : void {
     parent::setUp();
 
     // Grant "administer node form display" permission.
diff --git a/tests/src/Functional/ImceSourceTest.php b/tests/src/Functional/ImceSourceTest.php
index 7259f33..9b99dd2 100644
--- a/tests/src/Functional/ImceSourceTest.php
+++ b/tests/src/Functional/ImceSourceTest.php
@@ -19,7 +19,7 @@ class ImceSourceTest extends FileFieldSourcesTestBase {
   /**
    * Sets up for imce source test case.
    */
-  protected function setUp() {
+  protected function setUp() : void {
     parent::setUp();
     $this->setUpImce();
   }
diff --git a/tests/src/Functional/MultipleValuesTest.php b/tests/src/Functional/MultipleValuesTest.php
index fdb9082..6977919 100644
--- a/tests/src/Functional/MultipleValuesTest.php
+++ b/tests/src/Functional/MultipleValuesTest.php
@@ -56,7 +56,7 @@ class MultipleValuesTest extends FileFieldSourcesTestBase {
   /**
    * Sets up for multiple values test case.
    */
-  protected function setUp() {
+  protected function setUp() : void {
     parent::setUp();
     $this->setUpImce();
 
-- 
GitLab


From c30a0bcbf5f25a0d77aa0ca7a690409003cc5b62 Mon Sep 17 00:00:00 2001
From: Ryan Schwab <rwschwab@ucsc.edu>
Date: Tue, 12 Dec 2023 11:16:22 -0800
Subject: [PATCH 08/12] #3168858 Replace deprecated drupalPostForm() in tests.

---
 tests/src/Functional/EmptyValuesTest.php      |  3 ++-
 .../Functional/FileFieldSourcesTestBase.php   | 25 ++++++++++---------
 tests/src/Functional/MultipleValuesTest.php   |  5 ++--
 3 files changed, 18 insertions(+), 15 deletions(-)

diff --git a/tests/src/Functional/EmptyValuesTest.php b/tests/src/Functional/EmptyValuesTest.php
index 5e697af..6e26203 100644
--- a/tests/src/Functional/EmptyValuesTest.php
+++ b/tests/src/Functional/EmptyValuesTest.php
@@ -33,7 +33,8 @@ class EmptyValuesTest extends FileFieldSourcesTestBase {
    */
   public function testAllSourcesEnabled() {
     // Change allowed number of values.
-    $this->drupalPostForm('admin/structure/types/manage/' . $this->typeName . '/fields/node.' . $this->typeName . '.' . $this->fieldName . '/storage', ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED], t('Save field settings'));
+    $this->drupalGet('admin/structure/types/manage/' . $this->typeName . '/fields/node.' . $this->typeName . '.' . $this->fieldName . '/storage');
+    $this->submitForm(['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED], t('Save field settings'));
 
     $this->enableSources([
       'upload' => TRUE,
diff --git a/tests/src/Functional/FileFieldSourcesTestBase.php b/tests/src/Functional/FileFieldSourcesTestBase.php
index e587c1c..51cfa91 100644
--- a/tests/src/Functional/FileFieldSourcesTestBase.php
+++ b/tests/src/Functional/FileFieldSourcesTestBase.php
@@ -68,7 +68,8 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
       $this->grantPermissions($role, ['administer imce']);
       // Assign member profile to user's role.
       $edit["roles_profiles[$rid][public]"] = 'member';
-      $this->drupalPostForm('admin/config/media/imce', $edit, t('Save configuration'));
+      $this->drupalGet('admin/config/media/imce');
+      $this->submitForm($edit, t('Save configuration'));
     }
   }
 
@@ -101,7 +102,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
     $this->assertSession()->responseContains("File field sources: upload");
 
     // Click on the widget settings button to open the widget settings form.
-    $this->drupalPostForm(NULL, [], $this->fieldName . "_settings_edit");
+    $this->submitForm([], $this->fieldName . "_settings_edit");
 
     // Enable sources.
     $prefix = 'fields[' . $this->fieldName . '][settings_edit_form][third_party_settings][filefield_sources][filefield_sources][sources]';
@@ -109,11 +110,11 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
     foreach ($sources as $source => $enabled) {
       $edit[$prefix . '[' . $source . ']'] = $enabled ? TRUE : FALSE;
     }
-    $this->drupalPostForm(NULL, $edit, $this->fieldName . '_plugin_settings_update');
+    $this->submitForm($edit, $this->fieldName . '_plugin_settings_update');
     $this->assertSession()->responseContains("File field sources: " . implode(', ', array_keys($sources)));
 
     // Save the form to save the third party settings.
-    $this->drupalPostForm(NULL, [], t('Save'));
+    $this->submitForm([], t('Save'));
 
     $add_node = 'node/add/' . $this->typeName;
     $this->drupalGet($add_node);
@@ -216,15 +217,15 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
     $this->drupalGet($manage_display);
 
     // Click on the widget settings button to open the widget settings form.
-    $this->drupalPostForm(NULL, [], $this->fieldName . "_settings_edit");
+    $this->submitForm([], $this->fieldName . "_settings_edit");
 
     // Update settings.
     $name = 'fields[' . $this->fieldName . '][settings_edit_form][third_party_settings][filefield_sources][filefield_sources]' . "[$source_key][$key]";
     $edit = [$name => $value];
-    $this->drupalPostForm(NULL, $edit, $this->fieldName . '_plugin_settings_update');
+    $this->submitForm($edit, $this->fieldName . '_plugin_settings_update');
 
     // Save the form to save the third party settings.
-    $this->drupalPostForm(NULL, [], t('Save'));
+    $this->submitForm([], t('Save'));
   }
 
   /**
@@ -246,7 +247,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
     else {
       $edit = [];
     }
-    $this->drupalPostForm(NULL, $edit, $this->fieldName . '_' . $delta . '_attach');
+    $this->submitForm($edit, $this->fieldName . '_' . $delta . '_attach');
 
     if ($filename) {
       $this->assertFileUploaded($filename, $delta);
@@ -299,7 +300,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
     $name = $this->fieldName . '[' . $delta . '][filefield_reference][autocomplete]';
     $value = $fid ? $filename . ' [fid:' . $fid . ']' : '';
     $edit = [$name => $value];
-    $this->drupalPostForm(NULL, $edit, $this->fieldName . '_' . $delta . '_autocomplete_select');
+    $this->submitForm($edit, $this->fieldName . '_' . $delta . '_autocomplete_select');
 
     if ($filename) {
       $this->assertFileUploaded($filename, $delta);
@@ -378,7 +379,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
   public function uploadFileByRemoteSource($url = '', $filename = '', $delta = 0) {
     $name = $this->fieldName . '[' . $delta . '][filefield_remote][url]';
     $edit = [$name => $url];
-    $this->drupalPostForm(NULL, $edit, $this->fieldName . '_' . $delta . '_transfer');
+    $this->submitForm($edit, $this->fieldName . '_' . $delta . '_transfer');
 
     if ($filename) {
       $this->assertFileUploaded($filename, $delta);
@@ -406,7 +407,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
     $edit = [
       $name => $uri ? \Drupal::getContainer()->get('file_system')->realPath($uri) : '',
     ];
-    $this->drupalPostForm(NULL, $edit, $this->fieldName . '_' . $delta . '_upload_button');
+    $this->submitForm($edit, $this->fieldName . '_' . $delta . '_upload_button');
 
     if ($filename) {
       $this->assertFileUploaded($filename, $delta);
@@ -425,7 +426,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
    *   Delta in multiple values field.
    */
   public function removeFile($filename, $delta = 0) {
-    $this->drupalPostForm(NULL, [], $this->fieldName . '_' . $delta . '_remove_button');
+    $this->submitForm([], $this->fieldName . '_' . $delta . '_remove_button');
 
     // Ensure file is removed.
     $this->assertFileRemoved($filename);
diff --git a/tests/src/Functional/MultipleValuesTest.php b/tests/src/Functional/MultipleValuesTest.php
index 6977919..2a2bdb2 100644
--- a/tests/src/Functional/MultipleValuesTest.php
+++ b/tests/src/Functional/MultipleValuesTest.php
@@ -70,7 +70,8 @@ class MultipleValuesTest extends FileFieldSourcesTestBase {
     $this->temporaryFile = $this->createTemporaryFile($path);
 
     // Change allowed number of values.
-    $this->drupalPostForm('admin/structure/types/manage/' . $this->typeName . '/fields/node.' . $this->typeName . '.' . $this->fieldName . '/storage', ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED], t('Save field settings'));
+    $this->drupalGet('admin/structure/types/manage/' . $this->typeName . '/fields/node.' . $this->typeName . '.' . $this->fieldName . '/storage');
+    $this->submitForm(['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED], t('Save field settings'));
 
     $this->enableSources([
       'upload' => TRUE,
@@ -106,7 +107,7 @@ class MultipleValuesTest extends FileFieldSourcesTestBase {
   public function testUploadFilesThenSaveNode() {
     $this->uploadFiles();
 
-    $this->drupalPostForm(NULL, ['title[0][value]' => $this->randomMachineName()], t('Save'));
+    $this->submitForm(['title[0][value]' => $this->randomMachineName()], t('Save'));
 
     // Ensure all files are saved to node.
     $this->assertLink('INSTALL.txt');
-- 
GitLab


From d468aab118051bbbf77fc01eedbc488f8d169288 Mon Sep 17 00:00:00 2001
From: Ryan Schwab <rwschwab@ucsc.edu>
Date: Tue, 12 Dec 2023 11:19:23 -0800
Subject: [PATCH 09/12] #3408263 Replace deprecated Simpletest methods

---
 tests/src/Functional/AttachSourceTest.php    | 16 ++++++++++------
 tests/src/Functional/ClipboardSourceTest.php |  6 ++++--
 tests/src/Functional/ImceSourceTest.php      |  6 ++++--
 tests/src/Functional/MultipleValuesTest.php  |  8 +++++---
 tests/src/Functional/ReferenceSourceTest.php | 18 ++++++++++--------
 tests/src/Functional/RemoteSourceTest.php    |  9 ++++++---
 tests/src/Functional/UploadSourceTest.php    |  6 ++++--
 7 files changed, 43 insertions(+), 26 deletions(-)

diff --git a/tests/src/Functional/AttachSourceTest.php b/tests/src/Functional/AttachSourceTest.php
index 22b1911..8f2e0db 100644
--- a/tests/src/Functional/AttachSourceTest.php
+++ b/tests/src/Functional/AttachSourceTest.php
@@ -59,7 +59,8 @@ class AttachSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByAttachSource($file->uri, $file->filename, 0);
 
     // We can only attach one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Attach'), 'After uploading a file, "Attach" button is no longer displayed.');
+    $button = $this->xpath('//input[@value="Attach"]');
+    $this->assertEmpty($button);
 
     // Ensure file is moved.
     $this->assertFalse(is_file($file->uri), 'Source file has been removed.');
@@ -96,10 +97,11 @@ class AttachSourceTest extends FileFieldSourcesTestBase {
     $this->assertTrue($this->isOptionPresent($file->uri), 'File option is present.');
 
     // Ensure empty message is not present.
-    $this->assertNoText('There currently are no files to attach.', "Empty message is not present.");
+    $this->assertSession()->responseNotContains('There currently are no files to attach.');
 
     // Attach button is always present.
-    $this->assertFieldByXpath('//input[@type="submit"]', t('Attach'), 'Attach button is present.');
+    $button = $this->xpath('//input[@value="Attach"]');
+    $this->assertNotEmpty($button);
   }
 
   /**
@@ -130,10 +132,11 @@ class AttachSourceTest extends FileFieldSourcesTestBase {
     $this->assertFalse($this->isOptionPresent($file->uri), 'File option is not present.');
 
     // Ensure empty message is present.
-    $this->assertText('There currently are no files to attach.', "Empty message is present.");
+    $this->assertSession()->responseContains('There currently are no files to attach.');
 
     // Attach button is always present.
-    $this->assertFieldByXpath('//input[@type="submit"]', t('Attach'), 'Attach button is present.');
+    $button = $this->xpath('//input[@value="Attach"]');
+    $this->assertNotEmpty($button);
   }
 
   /**
@@ -164,7 +167,8 @@ class AttachSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByAttachSource($file->uri, $file->filename, 0);
 
     // We can only attach one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Attach'), 'After uploading a file, "Attach" button is no longer displayed.');
+    $button = $this->xpath('//input[@value="Attach"]');
+    $this->assertEmpty($button);
 
     // Ensure file is copied.
     $this->assertTrue(is_file($file->uri), 'Source file still exists.');
diff --git a/tests/src/Functional/ClipboardSourceTest.php b/tests/src/Functional/ClipboardSourceTest.php
index d8f9cd9..e925433 100644
--- a/tests/src/Functional/ClipboardSourceTest.php
+++ b/tests/src/Functional/ClipboardSourceTest.php
@@ -21,12 +21,14 @@ class ClipboardSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByClipboardSource($file->getFileUri(), $file->getFilename(), 0);
 
     // We can only upload one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Upload'), t('After uploading a file, "Upload" button is no longer displayed.'));
+    $button = $this->xpath('//input[@value="Upload"]');
+    $this->assertEmpty($button);
 
     $this->removeFile($file->getFilename(), 0);
 
     // Can upload file again.
-    $this->assertFieldByXpath('//input[@type="submit"]', t('Upload'), 'After clicking the "Remove" button, the "Upload" button is displayed.');
+    $button = $this->xpath('//input[@value="Upload"]');
+    $this->assertNotEmpty($button);
   }
 
 }
diff --git a/tests/src/Functional/ImceSourceTest.php b/tests/src/Functional/ImceSourceTest.php
index 9b99dd2..c3eaa49 100644
--- a/tests/src/Functional/ImceSourceTest.php
+++ b/tests/src/Functional/ImceSourceTest.php
@@ -36,12 +36,14 @@ class ImceSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByImceSource($file->getFileUri(), $file->getFilename(), 0);
 
     // We can only upload one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Select'), t('After uploading a file, "Select" button is no longer displayed.'));
+    $button = $this->xpath('//input[@value="Select"]');
+    $this->assertEmpty($button);
 
     $this->removeFile($file->getFilename(), 0);
 
     // Can upload file again.
-    $this->assertFieldByXpath('//input[@type="submit"]', t('Select'), 'After clicking the "Remove" button, the "Select" button is displayed.');
+    $button = $this->xpath('//input[@value="Select"]');
+    $this->assertNotEmpty($button);
   }
 
 }
diff --git a/tests/src/Functional/MultipleValuesTest.php b/tests/src/Functional/MultipleValuesTest.php
index 2a2bdb2..dc3ffd9 100644
--- a/tests/src/Functional/MultipleValuesTest.php
+++ b/tests/src/Functional/MultipleValuesTest.php
@@ -98,7 +98,8 @@ class MultipleValuesTest extends FileFieldSourcesTestBase {
     $this->removeFile($this->permanentFileEntity2->getFilename(), 0);
 
     // Ensure all files have been removed.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Remove'), 'All files have been removed.');
+    $button = $this->xpath('//input[@value="Remove"]');
+    $this->assertEmpty($button);
   }
 
   /**
@@ -128,7 +129,8 @@ class MultipleValuesTest extends FileFieldSourcesTestBase {
     $uploaded_files = 0;
 
     // Ensure no files has been uploaded.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Remove'), 'There are no file have been uploaded.');
+    $button = $this->xpath('//input[@value="Remove"]');
+    $this->assertEmpty($button);
 
     // Upload a file by 'Remote' source.
     $this->uploadFileByRemoteSource($GLOBALS['base_url'] . '/core/INSTALL.txt', 'INSTALL.txt', $uploaded_files);
@@ -156,7 +158,7 @@ class MultipleValuesTest extends FileFieldSourcesTestBase {
 
     // Ensure files have been uploaded.
     $remove_buttons = $this->xpath('//input[@type="submit" and @value="' . t('Remove') . '"]');
-    $this->assertEqual(count($remove_buttons), $uploaded_files, "There are $uploaded_files files have been uploaded.");
+    $this->assertEquals(count($remove_buttons), $uploaded_files, "There are $uploaded_files files have been uploaded.");
 
     return $uploaded_files;
   }
diff --git a/tests/src/Functional/ReferenceSourceTest.php b/tests/src/Functional/ReferenceSourceTest.php
index d992832..3ebf733 100644
--- a/tests/src/Functional/ReferenceSourceTest.php
+++ b/tests/src/Functional/ReferenceSourceTest.php
@@ -28,13 +28,15 @@ class ReferenceSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByReferenceSource($file->id(), $file->getFilename(), 0);
 
     // We can only refer one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Select'), t('After uploading a file, "Select" button is no longer displayed.'));
+    $button = $this->xpath('//input[@value="Select"]');
+    $this->assertEmpty($button);
 
     // Remove uploaded file.
     $this->removeFile($file->getFileName(), 0);
 
     // Can select file again.
-    $this->assertFieldByXpath('//input[@type="submit"]', t('Select'), 'After clicking the "Remove" button, the "Select" button is displayed.');
+    $button = $this->xpath('//input[@value="Select"]');
+    $this->assertNotEmpty($button);
   }
 
   /**
@@ -60,14 +62,14 @@ class ReferenceSourceTest extends FileFieldSourcesTestBase {
     $query = $this->findCharacterNotInString($first_character);
     $options['query']['q'] = $query;
     $autocomplete_result = Json::decode($this->drupalGet('file/reference/node/' . $this->typeName . '/' . $this->fieldName, $options));
-    $this->assertEqual($autocomplete_result, [], "No files that have name starts with '$query'");
+    $this->assertEquals($autocomplete_result, [], "No files that have name starts with '$query'");
 
     // STARTS_WITH: not empty results.
     $query = $first_character;
     $options['query']['q'] = $query;
     $autocomplete_result = Json::decode($this->drupalGet('file/reference/node/' . $this->typeName . '/' . $this->fieldName, $options + ['query' => ['q' => $query]]));
-    $this->assertEqual($autocomplete_result[0]['label'], $filename, 'Autocompletion return correct label.');
-    $this->assertEqual($autocomplete_result[0]['value'], $filename . ' [fid:' . $file->id() . ']', 'Autocompletion return correct value.');
+    $this->assertEquals($autocomplete_result[0]['label'], $filename, 'Autocompletion return correct label.');
+    $this->assertEquals($autocomplete_result[0]['value'], $filename . ' [fid:' . $file->id() . ']', 'Autocompletion return correct value.');
 
     // Switch to 'Contains' match type.
     $this->updateFilefieldSourcesSettings('source_reference', 'autocomplete', '1');
@@ -76,14 +78,14 @@ class ReferenceSourceTest extends FileFieldSourcesTestBase {
     $query = $this->findCharacterNotInString($filename);
     $options['query']['q'] = $query;
     $autocomplete_result = Json::decode($this->drupalGet('file/reference/node/' . $this->typeName . '/' . $this->fieldName, $options + ['query' => ['q' => $query]]));
-    $this->assertEqual($autocomplete_result, [], "No files that have name contains '$query'");
+    $this->assertEquals($autocomplete_result, [], "No files that have name contains '$query'");
 
     // CONTAINS: not empty results.
     $query = $second_character;
     $options['query']['q'] = $query;
     $autocomplete_result = Json::decode($this->drupalGet('file/reference/node/' . $this->typeName . '/' . $this->fieldName, $options + ['query' => ['q' => $query]]));
-    $this->assertEqual($autocomplete_result[0]['label'], $filename, 'Autocompletion return correct label.');
-    $this->assertEqual($autocomplete_result[0]['value'], $filename . ' [fid:' . $file->id() . ']', 'Autocompletion return correct value.');
+    $this->assertEquals($autocomplete_result[0]['label'], $filename, 'Autocompletion return correct label.');
+    $this->assertEquals($autocomplete_result[0]['value'], $filename . ' [fid:' . $file->id() . ']', 'Autocompletion return correct value.');
   }
 
   /**
diff --git a/tests/src/Functional/RemoteSourceTest.php b/tests/src/Functional/RemoteSourceTest.php
index 3d79072..3e8a6bd 100644
--- a/tests/src/Functional/RemoteSourceTest.php
+++ b/tests/src/Functional/RemoteSourceTest.php
@@ -23,13 +23,15 @@ class RemoteSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByRemoteSource($file_url, 'README.txt', 0);
 
     // We can only transfer one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Transfer'), t('After uploading a file, "Transfer" button is no longer displayed.'));
+    $button = $this->xpath('//input[@value="Transfer"]');
+    $this->assertEmpty($button);
 
     // Remove uploaded file.
     $this->removeFile('README.txt', 0);
 
     // Can transfer file again.
-    $this->assertFieldByXpath('//input[@type="submit"]', t('Transfer'), 'After clicking the "Remove" button, the "Transfer" button is displayed.');
+    $button = $this->xpath('//input[@value="Transfer"]');
+    $this->assertNotEmpty($button);
   }
 
   /**
@@ -47,7 +49,8 @@ class RemoteSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByRemoteSource($file_url, 'bg.txt', 0);
 
     // We can only transfer one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Transfer'), t('After uploading a file, "Transfer" button is no longer displayed.'));
+    $button = $this->xpath('//input[@value="Transfer"]');
+    $this->assertEmpty($button);
 
     // Remove uploaded file.
     $this->removeFile('bg.txt', 0);
diff --git a/tests/src/Functional/UploadSourceTest.php b/tests/src/Functional/UploadSourceTest.php
index fabbc8e..8d35fa0 100644
--- a/tests/src/Functional/UploadSourceTest.php
+++ b/tests/src/Functional/UploadSourceTest.php
@@ -45,13 +45,15 @@ class UploadSourceTest extends FileFieldSourcesTestBase {
     $this->uploadFileByUploadSource($file->getFileUri(), $file->getFilename(), 0, FALSE);
 
     // We can only upload one file on single value field.
-    $this->assertNoFieldByXPath('//input[@type="submit"]', t('Upload'), t('After uploading a file, "Upload" button is no longer displayed.'));
+    $button = $this->xpath('//input[@value="Upload"]');
+    $this->assertEmpty($button);
 
     // Remove uploaded file.
     $this->removeFile($file->getFilename(), 0);
 
     // Can upload file again.
-    $this->assertFieldByXpath('//input[@type="submit"]', t('Upload'), 'After clicking the "Remove" button, the "Upload" button is displayed.');
+    $button = $this->xpath('//input[@value="Upload"]');
+    $this->assertNotEmpty($button);
   }
 
 }
-- 
GitLab


From 1442fa308dc5094046c730cee8a38340183391f2 Mon Sep 17 00:00:00 2001
From: Ryan Schwab <rwschwab@ucsc.edu>
Date: Tue, 12 Dec 2023 12:17:14 -0800
Subject: [PATCH 10/12] Mink requires string values for radio input

---
 tests/src/Functional/AttachSourceTest.php | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/tests/src/Functional/AttachSourceTest.php b/tests/src/Functional/AttachSourceTest.php
index 8f2e0db..4203d2d 100644
--- a/tests/src/Functional/AttachSourceTest.php
+++ b/tests/src/Functional/AttachSourceTest.php
@@ -154,7 +154,7 @@ class AttachSourceTest extends FileFieldSourcesTestBase {
 
     // Change settings.
     $this->updateFilefieldSourcesSettings('source_attach', 'path', $path);
-    $this->updateFilefieldSourcesSettings('source_attach', 'absolute', FILEFIELD_SOURCE_ATTACH_ABSOLUTE);
+    $this->updateFilefieldSourcesSettings('source_attach', 'absolute', (string)FILEFIELD_SOURCE_ATTACH_ABSOLUTE);
     $this->updateFilefieldSourcesSettings('source_attach', 'attach_mode', FILEFIELD_SOURCE_ATTACH_MODE_COPY);
 
     $this->enableSources([
-- 
GitLab


From 1c17a184c2ac2872d200aabdca5e6f714089cd27 Mon Sep 17 00:00:00 2001
From: Ryan Schwab <rwschwab@ucsc.edu>
Date: Tue, 12 Dec 2023 12:36:38 -0800
Subject: [PATCH 11/12] Replace deprecated file_status_permanent constant

---
 tests/src/Functional/FileFieldSourcesTestBase.php | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

diff --git a/tests/src/Functional/FileFieldSourcesTestBase.php b/tests/src/Functional/FileFieldSourcesTestBase.php
index 51cfa91..8a8fb15 100644
--- a/tests/src/Functional/FileFieldSourcesTestBase.php
+++ b/tests/src/Functional/FileFieldSourcesTestBase.php
@@ -6,6 +6,7 @@ use Drupal\Core\File\FileSystem;
 use Drupal\Core\File\FileSystemInterface;
 use Drupal\Tests\file\Functional\FileFieldTestBase;
 use Drupal\field\Entity\FieldConfig;
+use Drupal\file\FileInterface;
 use Drupal\file\Entity\File;
 use Drupal\user\Entity\Role;
 
@@ -141,7 +142,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
   public function createPermanentFileEntity() {
     $file = $this->createTemporaryFileEntity();
     // Only permanent file can be referred.
-    $file->status = FILE_STATUS_PERMANENT;
+    $file->setPermanent();
     // Author has permission to access file.
     $file->uid = $this->adminUser->id();
     $file->save();
-- 
GitLab


From 966a3e3e55c04ac311151603b3ed43babb9203c6 Mon Sep 17 00:00:00 2001
From: Ryan Schwab <rwschwab@ucsc.edu>
Date: Tue, 12 Dec 2023 12:46:57 -0800
Subject: [PATCH 12/12] Make  protected, per
 https://www.drupal.org/node/2909426

---
 tests/src/Functional/EmptyValuesTest.php          | 2 +-
 tests/src/Functional/FileFieldSourcesTestBase.php | 2 +-
 tests/src/Functional/ImceSourceTest.php           | 2 +-
 tests/src/Functional/MultipleValuesTest.php       | 2 +-
 4 files changed, 4 insertions(+), 4 deletions(-)

diff --git a/tests/src/Functional/EmptyValuesTest.php b/tests/src/Functional/EmptyValuesTest.php
index 6e26203..c6537d4 100644
--- a/tests/src/Functional/EmptyValuesTest.php
+++ b/tests/src/Functional/EmptyValuesTest.php
@@ -18,7 +18,7 @@ class EmptyValuesTest extends FileFieldSourcesTestBase {
    *
    * @var array
    */
-  public static $modules = ['imce'];
+  protected static $modules = ['imce'];
 
   /**
    * Sets up for empty values test case.
diff --git a/tests/src/Functional/FileFieldSourcesTestBase.php b/tests/src/Functional/FileFieldSourcesTestBase.php
index 8a8fb15..4c3db7e 100644
--- a/tests/src/Functional/FileFieldSourcesTestBase.php
+++ b/tests/src/Functional/FileFieldSourcesTestBase.php
@@ -25,7 +25,7 @@ abstract class FileFieldSourcesTestBase extends FileFieldTestBase {
    *
    * @var array
    */
-  public static $modules = ['filefield_sources'];
+  protected static $modules = ['filefield_sources'];
 
   /**
    * Admin user.
diff --git a/tests/src/Functional/ImceSourceTest.php b/tests/src/Functional/ImceSourceTest.php
index c3eaa49..85c2185 100644
--- a/tests/src/Functional/ImceSourceTest.php
+++ b/tests/src/Functional/ImceSourceTest.php
@@ -14,7 +14,7 @@ class ImceSourceTest extends FileFieldSourcesTestBase {
    *
    * @var array
    */
-  public static $modules = ['imce'];
+  protected static $modules = ['imce'];
 
   /**
    * Sets up for imce source test case.
diff --git a/tests/src/Functional/MultipleValuesTest.php b/tests/src/Functional/MultipleValuesTest.php
index dc3ffd9..105d66c 100644
--- a/tests/src/Functional/MultipleValuesTest.php
+++ b/tests/src/Functional/MultipleValuesTest.php
@@ -51,7 +51,7 @@ class MultipleValuesTest extends FileFieldSourcesTestBase {
    *
    * @var array
    */
-  public static $modules = ['imce'];
+  protected static $modules = ['imce'];
 
   /**
    * Sets up for multiple values test case.
-- 
GitLab

