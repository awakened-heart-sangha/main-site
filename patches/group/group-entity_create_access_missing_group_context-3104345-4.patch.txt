diff --git a/src/Entity/Access/GroupContentAccessControlHandler.php b/src/Entity/Access/GroupContentAccessControlHandler.php
index b91396b..7ce3fda 100644
--- a/src/Entity/Access/GroupContentAccessControlHandler.php
+++ b/src/Entity/Access/GroupContentAccessControlHandler.php
@@ -2,6 +2,7 @@
 
 namespace Drupal\group\Entity\Access;
 
+use Drupal\Core\Access\AccessResult;
 use Drupal\group\Entity\GroupContentType;
 use Drupal\Core\Entity\EntityAccessControlHandler;
 use Drupal\Core\Entity\EntityInterface;
@@ -28,6 +29,11 @@ class GroupContentAccessControlHandler extends EntityAccessControlHandler {
   protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
     /** @var \Drupal\group\Entity\GroupContentTypeInterface $group_content_type */
     $group_content_type = GroupContentType::load($entity_bundle);
+
+    if (empty($context['group'])) {
+      return AccessResult::neutral('No group value set in context.');
+    }
+
     return $group_content_type->getContentPlugin()->createAccess($context['group'], $account);
   }
 
