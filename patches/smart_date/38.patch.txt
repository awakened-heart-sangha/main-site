From 73333d5f94224571fa3e9e0d069ffdb64329f8cf Mon Sep 17 00:00:00 2001
From: VladRiabovol <82464669+VladRiabovol@users.noreply.github.com>
Date: Mon, 14 Nov 2022 11:27:26 +0200
Subject: [PATCH] added changes

---
 src/SmartDateTrait.php | 7 +++++++
 1 file changed, 7 insertions(+)

diff --git a/src/SmartDateTrait.php b/src/SmartDateTrait.php
index 4d7ca16..1aeb48f 100644
--- a/src/SmartDateTrait.php
+++ b/src/SmartDateTrait.php
@@ -90,6 +90,13 @@ trait SmartDateTrait {
           $start_ts = $item->start_date->getTimestamp();
           $end_ts = $item->end_date->getTimestamp();
         }
+        if (!empty($item->start_date)) {
+          $start_ts = $end_ts = $item->start_date->getTimestamp();
+        }
+        if (!empty($item->end_date)) {
+          $end_ts = $item->end_date->getTimestamp();
+          $start_ts = $start_ts ?? $end_ts;
+        }
       }
       elseif ($field_type == 'datetime') {
         if (empty($item->date)) {
-- 
GitLab

