diff --git a/src/Plugin/AdvancedQueue/JobType/RecurringOrderClose.php b/src/Plugin/AdvancedQueue/JobType/RecurringOrderClose.php
index 66cb8b7..fa66b60 100644
--- a/src/Plugin/AdvancedQueue/JobType/RecurringOrderClose.php
+++ b/src/Plugin/AdvancedQueue/JobType/RecurringOrderClose.php
@@ -12,6 +12,7 @@ use Drupal\commerce_payment\Exception\DeclineException;
  * @AdvancedQueueJobType(
  *   id = "commerce_recurring_order_close",
  *   label = @Translation("Close recurring order"),
+ *   ensure_unique = TRUE,
  * )
  */
 class RecurringOrderClose extends RecurringJobTypeBase {
diff --git a/src/Plugin/AdvancedQueue/JobType/RecurringOrderRenew.php b/src/Plugin/AdvancedQueue/JobType/RecurringOrderRenew.php
index cec84cc..6c83488 100644
--- a/src/Plugin/AdvancedQueue/JobType/RecurringOrderRenew.php
+++ b/src/Plugin/AdvancedQueue/JobType/RecurringOrderRenew.php
@@ -11,6 +11,7 @@ use Drupal\advancedqueue\JobResult;
  * @AdvancedQueueJobType(
  *   id = "commerce_recurring_order_renew",
  *   label = @Translation("Renew recurring order"),
+ *   ensure_unique = TRUE,
  * )
  */
 class RecurringOrderRenew extends RecurringJobTypeBase {
diff --git a/tests/src/Kernel/CronTest.php b/tests/src/Kernel/CronTest.php
index 25482d9..578bfef 100644
--- a/tests/src/Kernel/CronTest.php
+++ b/tests/src/Kernel/CronTest.php
@@ -68,6 +68,14 @@ class CronTest extends RecurringKernelTestBase {
     $queue = Queue::load('commerce_recurring');
     $counts = array_filter($queue->getBackend()->countJobs());
     $this->assertEquals([Job::STATE_QUEUED => 2], $counts);
+
+    // Run cron again.
+    // Confirm that no further jobs have been queued.
+    $this->container->get('commerce_recurring.cron')->run();
+
+    $counts = array_filter($queue->getBackend()->countJobs());
+    $this->assertEquals([Job::STATE_QUEUED => 2], $counts, "Jobs are not requeued by a subsequent run of cron.");
+
     $first_job = $queue->getBackend()->claimJob();
     $this->assertArraySubset(['order_id' => $order->id()], $first_job->getPayload());
     $this->assertEquals('commerce_recurring_order_close', $first_job->getType());
