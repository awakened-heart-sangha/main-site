echo "Drupal image stem: ${DRUPAL_IMAGE}"
echo "Nginx image stem: ${NGINX_IMAGE}"
if [ "$VISUAL_REGRESSION_CONTEXT" == "reference" ]; then
  echo "Generating visual regression reference, so use image from merge request target branch"
  export DRUPAL_IMAGE="${DRUPAL_IMAGE}:$CI_MERGE_REQUEST_TARGET_BRANCH_SHA"
  export NGINX_IMAGE="${NGINX_IMAGE}:$CI_MERGE_REQUEST_TARGET_BRANCH_SHA"
elif [ -f 'image_tag.txt' ]; then
 echo "Image was built on this pipeline, so use that image"
 export DRUPAL_IMAGE="${DRUPAL_IMAGE}:$(cat image_tag.txt)"
 export NGINX_IMAGE="${NGINX_IMAGE}:$(cat image_tag.txt)"
else
 echo "No image needed to be built on this pipeline, use whatever the most recent image was"
 export DRUPAL_IMAGE="${DRUPAL_IMAGE}:latest"
 export NGINX_IMAGE="${NGINX_IMAGE}:latest"
fi
echo "Drupal image: ${DRUPAL_IMAGE}"
echo "Nginx image: ${NGINX_IMAGE}"