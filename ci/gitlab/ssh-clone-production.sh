echo "ls"
ls
echo "ssh agent"
echo "SSH_AGENT_PID before: $SSH_AGENT_PID"
echo "SSH_AUTH_SOCK before: $SSH_AUTH_SOCK"
eval $(ssh-agent -s)
echo "SSH_AGENT_PID after: $SSH_AGENT_PID"
echo "SSH_AUTH_SOCK after: $SSH_AUTH_SOCK"
echo "ssh add"
echo "PLATFORM KEY: ${PLATFORMSH_PRIVATE_KEY:0:50}"
echo "$PLATFORMSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
echo "Creating .ssh"
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "Key scanning"
ssh-keyscan -t rsa ssh.eu-2.platform.sh >> ~/.ssh/known_hosts
echo "Cloning"
platform db:dump --yes --project=sorgpc5uenxrw --environment=master -f prod.sql
ls prod.sql