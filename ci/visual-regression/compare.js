const console = require("console");
const sharp = require('sharp');
const fs = require('fs-extra');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const klawSync = require('klaw-sync');
const path = require('path');
const process = require('process');

// Comparison sensitivity is a number between 0 (very sensitive) and 1 (insensitive)
// 0.9 detects no changes
// 0.75 misses quite big changes
// 0.5 detects imperceptible text changes
const comparisonSensitivity = 0.7;

const visualRegressionOutputPath = process.env.VISUAL_REGRESSION_OUTPUT_FULL_PATH || "visual-regression";
const baseDir = process.env.PWD + "/" + visualRegressionOutputPath;
const referenceDir = baseDir + "/reference";
const testDir = baseDir + "/test";
const capturedDir = "/captured";
const resizedDir = "/resized";
//const jobId = process.env.VISUAL_REGRESSION_JOB_ID || "";
//const reportName = "visual-regression-" + jobId + ".html";

// VISUAL_REGRESSION_ARTIFACT_URL_BASE is either the gitlab ci job details url, 
// or empty if running locally.
const urlBase = process.env.VISUAL_REGRESSION_ARTIFACT_URL_BASE;
const rawUrlBase = urlBase ? urlBase + "/artifacts/raw/visual-regression/" : "";
const fileUrlBase = urlBase ? urlBase + "/artifacts/file/visual-regression/" : "";

// No leading slash on path in case gitlab url is empty and we want relative urls.
const diffPath = "diff";
const diffDir = baseDir + "/" + diffPath;
const diffUrlBase = rawUrlBase + diffPath;

const comparisonPath = "comparison";
const comparisonDir = baseDir + "/" + comparisonPath;
const comparisonUrlBase = fileUrlBase + comparisonPath;

var wait = ms => new Promise((r, j)=>setTimeout(r, ms));

launch().catch((err) => {
  console.error(err);
  process.exit(2);
});

async function launch() {
  console.log("\nSetting up directories");
  console.log("Base directory is %s", baseDir);
  fs.mkdirSync(diffDir, { recursive: true });
  fs.mkdirSync(comparisonDir, { recursive: true });
  await prepareDirectoriesForResizing(referenceDir);
  await prepareDirectoriesForResizing(testDir);

  console.log("\n Discovering screenshots");
  var referenceFiles = getFiles(referenceDir + capturedDir);
  console.log("%d reference screenshots", referenceFiles.length);
  var testFiles = getFiles(testDir + capturedDir);
  console.log("%d test screenshots", testFiles.length);
  let sharedFiles = referenceFiles.filter(x => testFiles.includes(x));
  console.log("%d screenshots in common", sharedFiles.length);
  console.log(sharedFiles);
  if (sharedFiles.length === 0) {
    throw new Error('No screenshots found');
  }

  console.log("\nResizing screenshots");
  sharedFiles.forEach(function (file, index) {
    resizeFiles(file);
  });

  //Wait for metadata on resized images to consolidate
  await wait(5000);

  console.log("\nComparing screenshots");
  var diffs = []
  for (let vv = 0; vv < sharedFiles.length; vv++) {
    let file = sharedFiles[vv];
    console.log(vv)
    let hasDiff = await compareFiles(file);
    if (hasDiff) {
      diffs.push(file)
    }
    await wait(500);
  }
  console.log("%d screenshots with differences", diffs.length);

  console.log("\nReviewings diffs");
  var lastBase = "";
  var sets = [];
  var set = new Object();
  var sizes = [];
  // Build an array of sets of the same page at different screenshot size.
  for (let index = 0; index < diffs.length; index++) {
    let diff = diffs[index];
    console.log("Reviewing " + diff);
    // Split the size out from the rest of the file name.
    // diff: testname--1000(A).png
    var name = diff.split(".")[0];
    // name: testname--1000(A)
    var base = name.split("--")[0];
    // base: testname
    var tail = name.split("--")[1];
    // tail: 1000(A)
    var size = tail.split("(")[0];
    // size: 1000
    if (!sizes.includes(size)) {
      sizes.push(size);
    }

    // Start a new set if we're moving on to a different file base
    if (lastBase !== base) {
      lastBase = base;
      if (index !== 0) {
        sets.push(set);
      }
      set = new Object();
      set["base"] = base;
    }

    // Add the current size to the current set
    if (!(size in set)) {
      set[size] = [];
    }
    set[size].push(diff);

    // Make sure the last set gets added.
    if (index === diffs.length - 1) {
      sets.push(set);
    }
  }


  console.log("\n%d screenshot sizes", sizes.length);
  console.log(sizes);
  console.log("%d pages with differences", sets.length);

  console.log("\nBuilding HTML report");
  writeReport(sets, sizes);

  if (diffs.length > 0) {
    delete sharp;
    throw new Error("Visual changes detected, see \n\n" + rawUrlBase + "visual-regression.html" + " \n\n");
  } else {
    console.log('No Visual changes detected');
    process.exit(0);
  }
}

function writeReport(sets, sizes) {

  var diffHtml = '';
  sets.forEach(function (set, index) {
    diffHtml += "\n<div class=" + '"set"' + "><p>" + set["base"] + "</p>";
    diffHtml += "\n<div class=" + '"row"' + ">";
    sizes.forEach(function (size, index) {
      var sizeHtml = '';
      if (size in set) {
        set[size].forEach(function (part, index) {
          var diffUrl = diffUrlBase + part;
          var comparisonUrl = comparisonUrlBase + part;
          var img = "\n      <img src=" + '"' + diffUrl + '">';
          var anchor = "\n    <a href=" + '"' + comparisonUrl + '">' + img + "</a>";
          sizeHtml += anchor;
        });
      }
      var sizeWrapper = "\n  <div class=" + '"shots shots-' + size + '">' + sizeHtml + "\n  </div>";
      diffHtml += sizeWrapper;
    });
    diffHtml += "\n</div>";
  });
  var template=fs.readFileSync("visual-regression.html").toString();
  var report = template.replace("{{body}}", diffHtml);
  fs.writeFileSync(baseDir + '/' + "visual-regression.html", report, {flag: "w"});
  // No idea why this doesn't work.
  //console.log("report name:" + reportName);
  //fs.writeFileSync(baseDir + '/' + reportName, report, {flag: "w"});
}

function getFiles(directory) {
  let klaws;
  try {
    klaws = klawSync(directory, {nodir: true})
  } catch (err) {
    throw err;
  }

  var files = [];
  klaws.forEach(function (klaw, index) {
    files.push(klaw.path.substring(directory.length));
  });
  return files;
}

async function prepareDirectoriesForResizing(parentDir) {
  try {
    await fs.copy(parentDir + capturedDir, parentDir + resizedDir)
    console.log(parentDir + ': Captured files copied to resized folder.')
  } catch (err) {
    console.error(err)
  }
}

async function compareFiles(file) {

  const referenceFile = referenceDir + resizedDir + file;
  const testFile = testDir + resizedDir + file;
  const testFileData = fs.readFileSync(testFile);
  const testImage = new PNG.sync.read(testFileData);
  const referenceFileData = fs.readFileSync(referenceFile);
  const referenceImage = new PNG.sync.read(referenceFileData);
  //const testImage = PNG.sync.read(fs.readFileSync(testDir + resizedDir + file));

  if (!testImage.data) {
    console.log("No test image data for " + testFile);
    return false;
  }
  if (!referenceImage.data) {
    console.log("No reference image data for " + referenceFile);
    return false;
  }

  if (referenceImage.width !== testImage.width || referenceImage.height !== testImage.height) {
    console.log('%s\n%s\nImage dimensions do not match: %dx%d vs %dx%d',
        referenceFile, testFile, referenceImage.width, referenceImage.height, testImage.width, testImage.height);
    return false;
  }

  const {width, height} = referenceImage;
  let diffImage = new PNG({width, height});

  const options = {threshold: comparisonSensitivity, aaColor: [225, 225, 225]};
  const pixelsChanged = await pixelmatch(referenceImage.data, testImage.data, diffImage.data, width, height, options);
  if (pixelsChanged > 10) {
    await writeFileWithDir(diffDir + file, PNG.sync.write(diffImage));
    await compositeDiff(file, width);
    console.log("Change detected for " + file);
    return true;
  } else {
    return false
  }
}

async function compositeDiff(file, width) {
 try{
   var dir = path.dirname(comparisonDir + file);
   fs.mkdirSync(dir, { recursive: true });

   var margin = 10;
   await sharp(diffDir + file)
       .extend({
         top: 0,
         bottom: 0,
         left: 0,
         right: ((width + margin) * 2)
       })
       .composite([
         {
           input: referenceDir + resizedDir + file,
           left: width + margin,
           top: 0
         },
         {
           input: testDir + resizedDir + file,
           left: (width + margin) * 2,
           top: 0
         },
       ])
       .toFile(comparisonDir + file)
       .catch( err => {});
 }catch (e) {
   console.log(e);
 }
}

async function resizeFiles(file) {
  var referenceImage = sharp(referenceDir + capturedDir + file);
  var testImage = sharp(testDir + capturedDir + file);

  var referenceMetadata = await referenceImage.metadata();
  var testMetadata = await testImage.metadata();

  var imageWidth = Math.min(referenceMetadata.width,testMetadata.width);
  var imageHeight = Math.min(referenceMetadata.height,testMetadata.height);

  if (referenceMetadata.width !== imageWidth || referenceMetadata.height !== imageHeight) {
    console.log('Resizing reference image %s: %dx%d to %dx%d', file, referenceMetadata.width, referenceMetadata.height, imageWidth, imageHeight);
    // Defaults to resizing by cropping not stretching
    referenceImage
        .resize(imageWidth, imageHeight)
        .withMetadata()
        .toFile(referenceDir + resizedDir + file);
  }
  if (testMetadata.width !== imageWidth || testMetadata.height !== imageHeight) {
    console.log('Resizing reference image %s: %dx%d to %dx%d', file, testMetadata.width, testMetadata.height, imageWidth, imageHeight);
    testImage
        .resize(imageWidth, imageHeight)
        .withMetadata()
        .toFile(testDir + resizedDir + file);
  }
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

async function writeFileWithDir(file, contents) {
  try{
    var dir = path.dirname(file);
    fs.mkdirSync(dir, { recursive: true });
    fs.writeFileSync(file, contents, {flag: "w"});
  }catch (e) {console.log(e);}
}

function logMemory() {
  const used = process.memoryUsage();
  for (let key in used) {
    console.log(`${key} ${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`);
  }
}
