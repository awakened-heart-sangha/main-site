cd web/themes/ahs/ahs_bootstrap
echo 'node version:'
node --version
echo 'npm version:'
npm --version
npm install || { echo 'Install npm dependencies failed' ; exit 1; }
node_modules/.bin/gulp  || { echo 'Gulp failed' ; exit 1; }
