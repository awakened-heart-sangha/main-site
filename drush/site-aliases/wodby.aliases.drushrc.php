<?php

$aliases['ahs.ahs-from-db-dump.dev'] = array (
  'root' => '/var/www/html/web',
  'uri' => 'dev.ahs-from-db-dump.ahs.wod.by',
  'remote-host' => 'node-89fe1e92-d413-4312-a6d6-3d4630c8a8fa.wod.by',
  'ssh-options' => '-p 31239',
  'remote-user' => 'www-data',
  'path-aliases' => 
  array (
    '%files' => '/var/www/html/web/sites/default/files',
  ),
  '#env-vars' => 
  array (
    'PATH' => '/home/www-data/.composer/vendor/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
  ),
);

$aliases['ahs.ahs-from-config-4.dev'] = array (
  'root' => '/var/www/html/web',
  'uri' => 'dev.ahs-from-config-4.ahs.wod.by',
  'remote-host' => 'node-89fe1e92-d413-4312-a6d6-3d4630c8a8fa.wod.by',
  'ssh-options' => '-p 31241',
  'remote-user' => 'www-data',
  'path-aliases' => 
  array (
    '%files' => '/var/www/html/web/sites/default/files',
  ),
  '#env-vars' => 
  array (
    'PATH' => '/home/www-data/.composer/vendor/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
  ),
);