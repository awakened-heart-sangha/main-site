# Awakened Heart Sangha's Drupal

The Drupal8 codebase of the Awakened Heart Sangha.
It contains:

1. A theme at web/themes/ahs/ahs_bootstrap
2. Modules at web/modules/ahs
3. A module ahs_tests containing extensive integration tests
4. Devops configration like .lando.yml and .gitlab-ci.yml
5. Composer.json & composer.lock files specifying all dependencies
6. Drupal & composer boilerplate similar to https://github.com/drupal/recommended-project.

## Lando

We mainly use (but do not require) Lando for local development:
https://thinktandem.io/lando/

There is a Lando configuration file included with this project.
You can augment or override this by creating a `.lando.local.yml` file locally,
but do not commit this to the repo.

## Connecting with the production environment
The production environment is hosted on platform.sh.
The platform.sh cli tool is installed in the Lando environment and can be
access with `lando platform`.

To connect with the production environment hosted on platform.sh, you will need this in your `.lando.local.yml`:
```
services:
  appserver:
    overrides:
      environment:
        PLATFORMSH_CLI_TOKEN: <A valid API token provided by Jonathan Shaw>
```
This is equivalent to executing `lando platform auth:api-token-login <token>`

In some cases you will also need to have provided Jonathan Shaw with an SSH key
to identify your dev machine, which is uploaded in the platform.sh admin UI.

## Installing locally

1. `git clone https://gitlab.com/awakened-heart-sangha/main-site.git ahs`
2. If using Lando, run `lando start`; otherwise add a settings.local.php file in ahs/web/sites/default and put your local database connection details there
3. `lando composer install` or `composer install`
4. Either
(a) make a fresh install of Drupal with
  `lando drupal-install` or `sh scripts/ahs/drupal-install.sh`; or
(b) if you have setup token and SSH authentication with platform.sh as described
above, you can clone production with
  `lando drupal-clone-production` or `sh scripts/ahs/drupal-clone-production.sh`
5. Install frontend dependencies and build css from scss with
   `lando build-theme` or `cd web/themes/ahs/ahs_bootstrap && npm install && node_modules/.bin/gulp`
6. You can control the characteristics of your Drupal environment by
specifying the optional "DRUPAL_ENVIRONMENT" environment variable.
Possible values include: "dev" (default in Lando), "test", and "review". Depending on
the value you choose, different files are included in settings.php.

## Debugging with PHPStorm

For enabling _xdebug_ when using lando, execute `lando xdebug-on`.
This will slow down everything, so disable it when you are not really using it by executing `lando xdebug-off`.

You might also need to edit your `.lando.local.yml`:

```
services:
  appserver:
    overrides:
      environment:
        PHP_IDE_CONFIG: "serverName=appserver"
```

More detailed instructions at https://www.drupaleasy.com/blogs/ultimike/2018/01/setting-xdebug-lando-and-phpstorm
See also https://github.com/lando/lando/issues/1668#issuecomment-507191275.

## Contributing

All discussion happens in Gitlab issues at https://gitlab.com/awakened-heart-sangha/main-site/,
and all changes should be made using gitlab merge requests. There are no
required branch or commit naming rules.
(But don't begin branch names with numbers, as that currently breaks the CI).


## Running PhpUnit tests
This project has a suite of PhpUnit tests that are not conventional blank-site tests but instead use [DrupalTestTraits](https://gitlab.com/weitzman/drupal-test-traits).
Like Behat tests, they work on a database that already has this project's configuration imported into it.

These tests are located in `web/modules/ahs/ahs_tests/tests/src/ExistingSite`.

You can run them like this:

`lando phpunit --testsuite ExistingSite --filter="SomeTestClass"`

or more concisely:

`lando test SomeTestClass`

A peculiarity of this project is that we use Behat's Gherkin syntax inside these ExistingSite tests,
in order to make the test logic clear. [Library](https://github.com/jonathanjfshaw/phpunitbehat) [Background](https://medium.com/@jonathanjfshaw/write-better-tests-by-using-behat-with-phpunit-ddb08d449b73)

To run a specific scenario from an ExistingSite test use PhpUnit's @ syntax:

`lando phpunit --testsuite ExistingSite --filter="@The name of the scenario"`

`lando test "@The name of the scenario"`

## Visual Regression tests

We run visual regression tests both explicitly on some pages
(see web/modules/ahs/ahs_tests/tests/src/ExistingSite/VisualRegression)
and implicitly at every step of every browser test
(see web/modules/ahs/ahs_tests/tests/src/ExistingSite/ExistingSiteJsBase.php).

On Gitlab visual regression results are available as an artifact for every job.

To generate reference or test screenshots use:

`lando visual-reference "@The name of the scenario'`

`lando visual-test "@The name of the scenario"`


## Continuous integration
No MR will be accepted unless it's CI pipeline passes on Gitlab. This runs the
ahs_tests test suite with both a clean database and a clone of the production
database.

@jonathanjfshaw is the only person who will accept merge requests to master.

## Review/demo sites
A platform.sh demo environment will be created for any MR if there is a slot available.
If multiple MRs are ongoing the slot is likely unavailable, talk to Jonathan
if you need one.

The review environment is rebuilt every time a new commit is pushed to its
git branch.

The database used on review environments is a clone of the production
database made when the branch was first pushed to gitlab. If you wish to
refresh this clone, this can be done by Jonathan. 

## Configuration management
Config split is not currently in use but will be; for now it's OK to commit
devel etc. to the production site as we're only in a limited alpha use.
