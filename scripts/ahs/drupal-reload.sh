echo `date`
FOLDER="${PWD##*/}";
echo "PWD: $PWD"
echo "FOLDER: $FOLDER"
if [ "$FOLDER" != 'web' ]; then
cd web;
fi;
echo "PWD: $PWD";
echo "Getting drush status..."
../vendor/bin/drush status;
echo "Deploying via drush..."
../vendor/bin/drush deploy -y --debug || { echo 'Error deploying via drush' ; exit 1; }
../vendor/bin/drush php-eval 'node_access_rebuild();' || { echo 'Error rebuilding node access' ; exit 1; }
echo `date`
