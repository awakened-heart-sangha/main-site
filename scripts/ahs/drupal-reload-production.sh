echo `date`
FOLDER="${PWD##*/}";
echo "PWD: $PWD"
echo "FOLDER: $FOLDER"
if [ "$FOLDER" != 'web' ]; then
  cd web;
fi;
echo "PWD: $PWD"

if [ -f '../prod.sql' ]; then
  echo "Database size (kb):" `du -k "../prod.sql" | cut -f1`
  echo "Emptying local database..."
  ../vendor/bin/drush sql:drop --yes  || { echo 'Database emptying failed' ; exit 1; }
  echo "Deploying dump to local..."
  ../vendor/bin/drush sql:query --file=../prod.sql  || { echo 'Database loading failed' ; exit 1; }
  echo "Proceeding to drupal-reload script..."
  sh ../scripts/ahs/drupal-reload.sh  || { echo 'Running reload script failed' ; exit 1; }
else
  echo "Database dump prod.sql not found"
  exit 1
fi;
echo `date`
