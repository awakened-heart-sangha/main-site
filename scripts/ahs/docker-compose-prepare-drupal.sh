docker info

echo 'Setting up test environment'
echo `date`
docker-compose -f docker-compose.test.yml up -d
echo `date`

echo 'Waiting for test environment to be ready'
docker-compose -f docker-compose.test.yml run php sh scripts/ahs/database-wait.sh
docker-compose -f docker-compose.test.yml run php sh -l -c "while ! nc -z mariadb 3306; do sleep 1; done"

echo 'Describing test environment (can help troubleshooting) ...'
docker-compose -f docker-compose.test.yml images
docker-compose -f docker-compose.test.yml ps
docker-compose -f docker-compose.test.yml run php ls ..
docker-compose -f docker-compose.test.yml run php ls
docker-compose -f docker-compose.test.yml run php ls vendor