echo `date`
FOLDER="${PWD##*/}";
if ! [ $FOLDER == 'web' ]; then
cd web;
fi;
echo "PWD: $PWD"
chmod 0777 sites/default/settings.php
../vendor/bin/drush status
../vendor/bin/drush site:install -y --existing-config --debug --account-pass password
../vendor/bin/drupal node:access:rebuild
# Not sure why we need to rebuild cache, but we get a weird block bug if we don't
../vendor/bin/drush cache:rebuild
echo `date`
