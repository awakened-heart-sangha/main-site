echo `date`
FOLDER="${PWD##*/}";
echo "PWD: $PWD"
echo "FOLDER: $FOLDER"
if [ "$FOLDER" != 'web' ]; then
  cd web;
fi;
echo "PWD: $PWD"

../vendor/bin/drush status
echo "Dumping production"
rm ../prod.sql
#../vendor/bin/drush sql:dump --result-file=../prod.sql --db-url=mysql://gitlab@awakened-heart-sangha:${PRODUCTION_DB_PASSWORD_GITLAB}@awakened-heart-sangha.mysql.database.azure.com/drupal --structure-tables-key=common
platform db:dump --yes --project=sorgpc5uenxrw --environment=master -f ../prod.sql
sh ../scripts/ahs/drupal-reload-production.sh
echo `date`
