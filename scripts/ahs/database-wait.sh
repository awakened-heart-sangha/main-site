echo "Attempting to connect to Drupal database ... "
TIME_WAITING=0
until mysqladmin status --connect_timeout=2 -u $DB_USER -p$DB_PASS -h $DB_HOST > /dev/null 2>&1 || [ $TIME_WAITING -gt 60 ]; do
  sleep 5;
  TIME_WAITING=$(( $TIME_WAITING + 5 ))
  echo "Attempting to connect to Drupal database ... "
done

if [ $TIME_WAITING -gt 120 ]; then
  echo "Database connection timeout"
  exit 1
fi

echo "Successfully connected to Drupal database."