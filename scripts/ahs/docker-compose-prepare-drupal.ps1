$env:VISUAL_REGRESSION_OUTPUT_SHORT_PATH = "visual-regression"
$env:TEST_OUTPUT_SHORT_PATH = "test-output"
$env:ENVIRONMENT_LABEL = "test-test"
$env:VISUAL_REGRESSION_WINDOW_WIDTH = 1100
$env:VISUAL_REGRESSION_CONTEXT = "test"
$env:VISUAL_REGRESSION_DATABASE_TYPE = "new-db"
$env:AHS_ROLLBAR_ACCESS_TOKEN = "18e0e3a589cf4db09a3c6addff315462"
Invoke-Expression 'docker-compose -f docker-compose.test.yml up -d'
Invoke-Expression 'docker-compose -f docker-compose.test.yml run php sh scripts/ahs/database-wait.sh'
$cmd = 'docker-compose -f docker-compose.test.yml run php sh -l -c "while ! nc -z mariadb 3306; do sleep 5; done"'
Invoke-Expression "& $cmd"
