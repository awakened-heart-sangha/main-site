<?xml version="1.0" encoding="UTF-8"?>
<!-- TODO set checkForUnintentionallyCoveredCode="true" once https://www.drupal.org/node/2626832 is resolved. -->
<!-- PHPUnit expects functional tests to be run with either a privileged user
 or your current system user. See core/tests/README.md and
 https://www.drupal.org/node/2116263 for details.
-->
<phpunit bootstrap="ci/phpunit/bootstrap.php"
         colors="true"
         beStrictAboutTestsThatDoNotTestAnything="true"
         beStrictAboutOutputDuringTests="true"
         beStrictAboutChangesToGlobalState="true"
         printerClass="LimeDeck\Testing\Printer"
>
    <!-- TODO set printerClass="\Drupal\Tests\Listeners\HtmlOutputPrinter" once
     https://youtrack.jetbrains.com/issue/WI-24808 is resolved. Drupal provides a
     result printer that links to the html output results for functional tests.
     Unfortunately, this breaks the output of PHPStorm's PHPUnit runner. However, if
     using the command line you can add
     - -printer="\Drupal\Tests\Listeners\HtmlOutputPrinter" to use it (note there
     should be no spaces between the hyphens).
    -->
    <php>
        <!-- Set error reporting to E_ALL. -->
        <ini name="error_reporting" value="32767"/>
        <!-- Do not limit the amount of memory tests take to run. -->
        <ini name="memory_limit" value="-1"/>
        <!-- SIMPLETEST_BASE_URL, SIMPLETEST_DB_VALUE, DTT_BASE_URL and DTT_API_URL should be set in calling envuronment -->
        <!-- Example SIMPLETEST_BASE_URL value: http://localhost -->
        <!-- Example SIMPLETEST_DB value: mysql://username:password@localhost/databasename#table_prefix -->
        <!-- env name="SIMPLETEST_DB" value="mysql://drupal8:drupal8@database/drupal8" -->
        <!-- env name="SIMPLETEST_BASE_URL: "http://ahs.lndo.site" -->
        <!-- env name="DTT_BASE_URL" value="http://ahs.lndo.site" -->
        <!-- env name="DTT_API_URL" value="http://ahs.lndo.site:9222" -->
        <!-- Example BROWSERTEST_OUTPUT_DIRECTORY value: /tmp/b2b
             Specify a temporary directory for storing debug images and html documents.
             These artifacts get copied to /sites/simpletest/browser_output by BrowserTestBase -->
        <!-- env name="BROWSERTEST_OUTPUT_DIRECTORY" value="/sites/simpletest/browser_output"/ -->
        <!-- To disable deprecation testing completely uncomment the next line. -->
        <env name="SYMFONY_DEPRECATIONS_HELPER" value="disabled"/>
        <!-- Example for changing the driver class for mink tests MINK_DRIVER_CLASS value: 'Drupal\FunctionalJavascriptTests\DrupalSelenium2Driver' -->
        <!-- Example for changing the driver args to mink tests MINK_DRIVER_ARGS value: '["http://127.0.0.1:8510"]' -->
        <!-- Example for changing the driver args to phantomjs tests MINK_DRIVER_ARGS_PHANTOMJS value: '["http://127.0.0.1:8510"]' -->
        <!-- Example for changing the driver args to webdriver tests MINK_DRIVER_ARGS_WEBDRIVER value: '["firefox", null, "http://localhost:4444/wd/hub"]' -->
        <!-- env name="DTT_MINK_DRIVER_ARGS" value='["firefox", null, "http://localhost:4444/wd/hub"]'/ -->

    </php>
    <testsuites>
        <testsuite name="unit">
            <file>./web/core/tests/TestSuites/UnitTestSuite.php</file>
        </testsuite>
        <testsuite name="kernel">
            <file>./web/core/tests/TestSuites/KernelTestSuite.php</file>
        </testsuite>
        <testsuite name="functional">
            <file>./web/core/tests/TestSuites/FunctionalTestSuite.php</file>
        </testsuite>
        <testsuite name="functional-javascript">
            <file>./web/core/tests/TestSuites/FunctionalJavascriptTestSuite.php</file>
        </testsuite>
        <testsuite name="existing-site">
            <!-- This assumes tests are namespaced as
                 `\Drupal\Tests\custom_profile_foo\ExistingSite`. -->
            <directory>./web/modules/ahs/*/tests/src/ExistingSite</directory>
            <!--<directory>./web/profiles/custom/*/tests/src/ExistingSite</directory>-->
        </testsuite>
        <!-- testsuite name="existing-site-javascript" -->
            <!-- This assumes tests are namespaced as
                 `\Drupal\Tests\custom_module_foo\ExistingSiteJavascript`. -->
            <!-- directory>./web/modules/ahs/*/tests/src/ExistingSiteJavascript</directory -->
            <!-- This assumes tests are namespaced as
                 `\Drupal\Tests\custom_profile_foo\ExistingSiteJavascript`. -->
            <!--<directory>./web/profiles/custom/*/tests/src/ExistingSiteJavascript</directory>-->
        <!-- /testsuite -->
    </testsuites>
    <listeners>
        <listener class="\Drupal\Tests\Listeners\DrupalListener">
        </listener>
        <!-- The Symfony deprecation listener has to come after the Drupal listener -->
        <listener class="Symfony\Bridge\PhpUnit\SymfonyTestsListener">
        </listener>
    </listeners>
    <!-- Filter for coverage reports. -->
    <filter>
        <whitelist>
            <directory>web/core/includes</directory>
            <directory>web/core/lib</directory>
            <directory>web/core/modules</directory>
            <directory>web/modules</directory>
            <directory>web/sites</directory>
            <!-- By definition test classes have no tests. -->
            <exclude>
                <directory suffix="Test.php">./</directory>
                <directory suffix="TestBase.php">./</directory>
            </exclude>
        </whitelist>
    </filter>
</phpunit>
